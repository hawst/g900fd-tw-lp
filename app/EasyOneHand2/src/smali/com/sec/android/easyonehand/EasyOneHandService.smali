.class public Lcom/sec/android/easyonehand/EasyOneHandService;
.super Landroid/app/Service;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;,
        Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;,
        Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static TAG:Ljava/lang/String;

.field private static mCallState:I


# instance fields
.field private final mAlarmStateListener:Landroid/content/BroadcastReceiver;

.field private mBinder:Landroid/os/Binder;

.field private final mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

.field private final mDreamServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mEOHServiceWatcher:Lcom/sec/android/easyonehand/EOHServiceWatcher;

.field private mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

.field private mHandler:Landroid/os/Handler;

.field private final mHomePauseResumeReceiver:Landroid/content/BroadcastReceiver;

.field private mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

.field private mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

.field private final mPenInsertReceiver:Landroid/content/BroadcastReceiver;

.field private mPhone:Landroid/telephony/TelephonyManager;

.field private final mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

.field private final mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

.field private mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

.field private mShutdownReceiver:Landroid/content/BroadcastReceiver;

.field private mTimeGapWhenLCDOFF:J

.field private mUsbStateReceiver:Landroid/content/BroadcastReceiver;

.field private mUserPresentReceiver:Landroid/content/BroadcastReceiver;

.field private mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

.field private mWMS:Landroid/view/IWindowManager;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "EOHService"

    sput-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    .line 41
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 55
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mBinder:Landroid/os/Binder;

    .line 69
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 75
    iput-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    .line 81
    iput-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 83
    iput-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    .line 384
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$1;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 437
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$2;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mHomePauseResumeReceiver:Landroid/content/BroadcastReceiver;

    .line 447
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$3;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mDreamServiceReceiver:Landroid/content/BroadcastReceiver;

    .line 457
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$4;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$4;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 488
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$5;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    .line 500
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$6;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    .line 514
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$7;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPenInsertReceiver:Landroid/content/BroadcastReceiver;

    .line 526
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$8;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$8;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    .line 539
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$9;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$9;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    .line 548
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$10;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$10;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    .line 563
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$11;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    .line 576
    new-instance v0, Lcom/sec/android/easyonehand/EasyOneHandService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EasyOneHandService$12;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 686
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/easyonehand/EasyOneHandService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EasyOneHandService;->closeAllWindowSafe(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/util/DisplayMetrics;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 38
    sget-boolean v0, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    return v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/easyonehand/EasyOneHandService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/sec/android/easyonehand/EasyOneHandService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EasyOneHandService;
    .param p1, "x1"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J

    return-wide p1
.end method

.method static synthetic access$900()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCallState:I

    return v0
.end method

.method static synthetic access$902(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 38
    sput p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCallState:I

    return p0
.end method

.method private canStartEasyOneHand()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 599
    const/4 v0, 0x0

    .line 600
    .local v0, "runningState":I
    sget-boolean v3, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 601
    sget-object v3, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v4, "canStartEasyOneHand() start"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    :cond_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v4, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v4, :cond_5

    .line 604
    const/4 v0, 0x1

    .line 609
    :goto_0
    if-le v0, v1, :cond_1

    .line 610
    sget-object v3, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not start EasyOneHand service : reason="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :cond_1
    sget-boolean v3, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 614
    sget-object v3, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "canStartEasyOneHand() end. runningState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    :cond_2
    const/4 v3, 0x3

    if-eq v0, v3, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_3

    const/16 v3, 0xb

    if-ne v0, v3, :cond_4

    .line 620
    :cond_3
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "any_screen_enabled"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 623
    :cond_4
    if-nez v0, :cond_6

    :goto_1
    return v1

    .line 606
    :cond_5
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->getRunningSettingState()I

    move-result v0

    goto :goto_0

    :cond_6
    move v1, v2

    .line 623
    goto :goto_1
.end method

.method private closeAllWindowSafe(Z)V
    .locals 1
    .param p1, "animation"    # Z

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    .line 297
    :cond_0
    return-void
.end method

.method private initTelephonyEventListener()V
    .locals 4

    .prologue
    .line 717
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    if-nez v1, :cond_0

    .line 718
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 720
    :try_start_0
    new-instance v1, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;Lcom/sec/android/easyonehand/EasyOneHandService$1;)V

    iput-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    .line 721
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPhone:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 726
    :cond_0
    :goto_0
    return-void

    .line 722
    :catch_0
    move-exception v0

    .line 723
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v1, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v2, "initTelephonyEventListener : Doesn\'t have the permission READ_PHONE_STATE."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerEasyOneHandWatcher()V
    .locals 3

    .prologue
    .line 648
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWMS:Landroid/view/IWindowManager;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHServiceWatcher:Lcom/sec/android/easyonehand/EOHServiceWatcher;

    invoke-interface {v1, v2}, Landroid/view/IWindowManager;->registerEasyOneHandWatcher(Landroid/sec/easyonehand/IEasyOneHandWatcher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :goto_0
    return-void

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private unregisterEasyOneHandWatcher()V
    .locals 3

    .prologue
    .line 656
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWMS:Landroid/view/IWindowManager;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHServiceWatcher:Lcom/sec/android/easyonehand/EOHServiceWatcher;

    invoke-interface {v1, v2}, Landroid/view/IWindowManager;->unregisterEasyOneHandWatcher(Landroid/sec/easyonehand/IEasyOneHandWatcher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    :goto_0
    return-void

    .line 657
    :catch_0
    move-exception v0

    .line 658
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V
    .locals 4
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 284
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception while unregisterReceiverSafe() receiver="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public forceStopService()V
    .locals 6

    .prologue
    .line 633
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 634
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.sec.android.easyonehand"

    const-string v4, "com.sec.android.easyonehand.EasyOneHandService"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.action.EASYONEHAND_SERVICE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "ForceHide"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 639
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 641
    :catch_0
    move-exception v1

    .line 642
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v4, "Exception checkSetting: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSettingsObserver()Lcom/sec/android/easyonehand/EOHSettingsObserver;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

    return-object v0
.end method

.method public manageProcessForeground(Z)V
    .locals 4
    .param p1, "start"    # Z

    .prologue
    .line 588
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 590
    .local v1, "mAm":Landroid/app/IActivityManager;
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mBinder:Landroid/os/Binder;

    if-eqz v2, :cond_0

    .line 591
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mBinder:Landroid/os/Binder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 596
    :cond_0
    :goto_0
    return-void

    .line 593
    :catch_0
    move-exception v0

    .line 594
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 370
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 375
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 376
    sget-boolean v0, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 377
    sget-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHWindowController;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 382
    :cond_1
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 88
    sget-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iput-object p0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    .line 91
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->getInstance()Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 92
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0, p0}, Lcom/sec/android/easyonehand/EOHUtils;->init(Landroid/content/Context;)V

    .line 94
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 96
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowManager:Landroid/view/WindowManager;

    .line 97
    const-string v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWMS:Landroid/view/IWindowManager;

    .line 98
    new-instance v0, Lcom/sec/android/easyonehand/EOHSettingsObserver;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/easyonehand/EOHSettingsObserver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

    .line 99
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->register()V

    .line 100
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 211
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 212
    sget-object v2, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v3, "onDestroy() "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->unregister()V

    .line 215
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterEasyOneHandWatcher()V

    .line 217
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v2, :cond_0

    .line 218
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 219
    iput-object v4, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 222
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->disable()V

    .line 224
    iput-object v4, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    .line 227
    :cond_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v2, :cond_2

    .line 228
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v2, v5}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    .line 232
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 233
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mDreamServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPenInsertReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 235
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 236
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 237
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 238
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 242
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 243
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 245
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 246
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mHomePauseResumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 249
    :cond_3
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EasyOneHandService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 251
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    if-eqz v2, :cond_4

    .line 252
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPhone:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 253
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    # invokes: Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->dispose()V
    invoke-static {v2}, Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;->access$100(Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;)V

    .line 254
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 255
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mMyPhoneStateListener:Lcom/sec/android/easyonehand/EasyOneHandService$EOHPhoneStateListener;

    .line 258
    :cond_4
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "any_screen_running"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 260
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 261
    .local v1, "startIntent":Landroid/content/Intent;
    const-string v2, "com.sec.android.easyonehand.MODE_STOP"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 264
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v2, :cond_5

    .line 265
    iget-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/easyonehand/EOHWindowController;->magnifyWindow(FFF)V

    .line 268
    :cond_5
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 270
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    .line 271
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 273
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowManager:Landroid/view/WindowManager;

    .line 274
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWMS:Landroid/view/IWindowManager;

    .line 275
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    .end local v1    # "startIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception onDestroy() e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onIdle()V
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 680
    sget-boolean v0, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 681
    sget-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v1, "onIdle()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :cond_0
    return-void
.end method

.method public onOffHook()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 672
    sget-boolean v0, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 673
    sget-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v1, "onOffHook()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_0
    return-void
.end method

.method public onRinging()V
    .locals 2

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 664
    sget-boolean v0, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 665
    sget-object v0, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v1, "onRinging()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->closeSideWindowEditMode()V

    .line 668
    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x2

    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "bForceHide":Z
    const/4 v1, 0x0

    .line 106
    .local v1, "bFromCamera":Z
    const/4 v3, 0x0

    .line 108
    .local v3, "startHandMode":I
    sget-boolean v7, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v7, :cond_0

    .line 109
    sget-object v7, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartCommand() intent="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", flags="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", startId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowController;->isAnimationRunning()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 206
    :cond_1
    :goto_0
    return v6

    .line 115
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 116
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "ForceHide"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 117
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "FromCamera"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 118
    if-nez v0, :cond_3

    if-nez v1, :cond_3

    .line 119
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "LeftHandMode"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    move v3, v5

    .line 124
    :cond_3
    :goto_1
    sget-boolean v7, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v7, :cond_4

    .line 125
    sget-object v7, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartCommand() bForceHide="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", bFromCamera="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", startHandMode="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_4
    if-nez v1, :cond_1

    .line 131
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v7, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 133
    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/sec/android/easyonehand/EasyOneHandService;->canStartEasyOneHand()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 137
    :cond_5
    if-eqz p1, :cond_6

    if-nez v0, :cond_6

    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 138
    :cond_6
    sget-boolean v7, Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z

    if-eqz v7, :cond_7

    .line 139
    sget-object v7, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartCommand() bForceHide="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", duplicated start command! or intent is null "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_7
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v7, :cond_1

    .line 143
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v7, v5}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    goto/16 :goto_0

    :cond_8
    move v3, v6

    .line 119
    goto/16 :goto_1

    .line 148
    :cond_9
    sget-object v7, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onStartCommand() : handMode="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    new-instance v7, Lcom/sec/android/easyonehand/EOHServiceWatcher;

    invoke-direct {v7}, Lcom/sec/android/easyonehand/EOHServiceWatcher;-><init>()V

    iput-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHServiceWatcher:Lcom/sec/android/easyonehand/EOHServiceWatcher;

    .line 150
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EasyOneHandService;->registerEasyOneHandWatcher()V

    .line 152
    new-instance v7, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/sec/android/easyonehand/EasyOneHandService$EOHServiceHandler;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;Lcom/sec/android/easyonehand/EasyOneHandService$1;)V

    iput-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;

    .line 154
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLockedSecure()Z

    move-result v7

    if-nez v7, :cond_c

    .line 155
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->dismissKeyguard()V

    .line 156
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setScreenLocked(Z)V

    .line 161
    :goto_2
    new-instance v7, Lcom/sec/android/easyonehand/EOHWindowController;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-direct {v7, p0, v8, v3}, Lcom/sec/android/easyonehand/EOHWindowController;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;Landroid/content/Context;I)V

    iput-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 162
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowController;->showWindow()V

    .line 164
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mSettingObserver:Lcom/sec/android/easyonehand/EOHSettingsObserver;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v7, v8}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->setWindowController(Lcom/sec/android/easyonehand/EOHWindowController;)V

    .line 166
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 167
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.SCREEN_OFF"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 168
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.SCREEN_ON"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 169
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mDreamServiceReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.DREAMING_STARTED"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 170
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPenInsertReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "com.samsung.pen.INSERT"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 171
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 172
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.USER_SWITCHED"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 173
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mUserPresentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.USER_PRESENT"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 174
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.ACTION_SHUTDOWN"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 176
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT_FROM_ALARM"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 177
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 179
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 180
    new-instance v7, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-direct {v7, p0, v8}, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;-><init>(Lcom/sec/android/easyonehand/EasyOneHandService;Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    .line 181
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mOrientationListener:Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->enable()V

    .line 184
    :cond_a
    new-instance v2, Landroid/content/IntentFilter;

    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v2, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 185
    .local v2, "pkgMngFilter":Landroid/content/IntentFilter;
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 187
    const-string v7, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 188
    const-string v7, "package"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 189
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 191
    new-instance v7, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v7, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 192
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v7, :cond_b

    .line 193
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 196
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EasyOneHandService;->initTelephonyEventListener()V

    .line 198
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "any_screen_running"

    invoke-static {v7, v8, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 200
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 201
    .local v4, "startIntent":Landroid/content/Intent;
    const-string v5, "com.sec.android.easyonehand.MODE_START"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    iget-object v5, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mContext:Landroid/content/Context;

    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v5, v4, v7}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 204
    sget-object v5, Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;

    const-string v7, "onStartCommand() successfully"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 158
    .end local v2    # "pkgMngFilter":Landroid/content/IntentFilter;
    .end local v4    # "startIntent":Landroid/content/Intent;
    :cond_c
    iget-object v7, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v8}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setScreenLocked(Z)V

    goto/16 :goto_2
.end method
