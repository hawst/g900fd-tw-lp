.class Lcom/sec/android/easyonehand/EOHMainWindow$1;
.super Ljava/lang/Object;
.source "EOHMainWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHMainWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field KeyCode:I

.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHMainWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V
    .locals 1

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090017

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 171
    const/16 v0, 0xbb

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    .line 188
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 189
    const-string v0, "EOHMainWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTouch() event="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 203
    :goto_1
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 173
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto :goto_0

    .line 174
    :cond_3
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_4

    .line 175
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto :goto_0

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_5

    .line 177
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto :goto_0

    .line 178
    :cond_5
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_6

    .line 179
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto :goto_0

    .line 180
    :cond_6
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 181
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto/16 :goto_0

    .line 182
    :cond_7
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_8

    .line 183
    const/16 v0, 0x3eb

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto/16 :goto_0

    .line 184
    :cond_8
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 185
    const/16 v0, 0x3ec

    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    goto/16 :goto_0

    .line 194
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$100(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->userActivity()V

    .line 195
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    iget v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    # invokes: Lcom/sec/android/easyonehand/EOHMainWindow;->processMakeTouchToKeyDown(Landroid/view/MotionEvent;I)Z
    invoke-static {v0, p2, v1}, Lcom/sec/android/easyonehand/EOHMainWindow;->access$200(Lcom/sec/android/easyonehand/EOHMainWindow;Landroid/view/MotionEvent;I)Z

    goto/16 :goto_1

    .line 200
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->this$0:Lcom/sec/android/easyonehand/EOHMainWindow;

    iget v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow$1;->KeyCode:I

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/easyonehand/EOHMainWindow;->processClearTouchToKeyUp(Landroid/view/MotionEvent;I)Z

    goto/16 :goto_1

    .line 191
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
