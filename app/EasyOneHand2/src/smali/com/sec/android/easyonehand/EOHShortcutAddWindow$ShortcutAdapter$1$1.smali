.class Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;
.super Landroid/view/View$DragShadowBuilder;
.source "EOHShortcutAddWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;

.field final synthetic val$dragView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;Landroid/view/View;Landroid/widget/ImageView;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 447
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;->this$2:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;

    iput-object p3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;->val$dragView:Landroid/widget/ImageView;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;->val$dragView:Landroid/widget/ImageView;

    .line 457
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 458
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 5
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 449
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;->val$dragView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 450
    .local v1, "ShadowWidth":I
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;->val$dragView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 451
    .local v0, "ShadowHeight":I
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Point;->set(II)V

    .line 452
    div-int/lit8 v2, v1, 0x2

    div-int/lit8 v3, v0, 0x2

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1$1;->this$2:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;

    iget-object v4, v4, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;->this$1:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    iget-object v4, v4, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v4}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$800(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDragViewOffset()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 453
    return-void
.end method
