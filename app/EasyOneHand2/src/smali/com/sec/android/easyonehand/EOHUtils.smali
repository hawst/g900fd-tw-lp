.class public Lcom/sec/android/easyonehand/EOHUtils;
.super Ljava/lang/Object;
.source "EOHUtils.java"


# static fields
.field static final DEBUG:Z

.field private static TAG:Ljava/lang/String;

.field private static mSingleton:Lcom/sec/android/easyonehand/EOHUtils;


# instance fields
.field private isDocomoPhonebook:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mIsUseDefaultTheme:Z

.field private mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mStatusBarMgr:Landroid/app/StatusBarManager;

.field private mSupportSideWindow:I

.field private mToast:Landroid/widget/Toast;

.field private mToastLong:Landroid/widget/Toast;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private themeBackground:Landroid/graphics/Bitmap;

.field private themeType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    const-string v0, "EOHUtils"

    sput-object v0, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    .line 54
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHUtils;->DEBUG:Z

    .line 86
    new-instance v0, Lcom/sec/android/easyonehand/EOHUtils;

    invoke-direct {v0}, Lcom/sec/android/easyonehand/EOHUtils;-><init>()V

    sput-object v0, Lcom/sec/android/easyonehand/EOHUtils;->mSingleton:Lcom/sec/android/easyonehand/EOHUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook:I

    .line 78
    iput v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mSupportSideWindow:I

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mIsUseDefaultTheme:Z

    .line 90
    return-void
.end method

.method public static dismissKeyguard()V
    .locals 3

    .prologue
    .line 286
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 288
    .local v1, "mWMS":Landroid/view/IWindowManager;
    :try_start_0
    invoke-interface {v1}, Landroid/view/IWindowManager;->dismissKeyguard()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :goto_0
    return-void

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getBoolean(Ljava/lang/String;ZI)Z
    .locals 2
    .param p1, "secureSettingKey"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z
    .param p3, "userId"    # I

    .prologue
    .line 229
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/widget/ILockSettings;->getBoolean(Ljava/lang/String;ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 231
    .end local p2    # "defaultValue":Z
    :goto_0
    return p2

    .line 230
    .restart local p2    # "defaultValue":Z
    :catch_0
    move-exception v0

    .line 231
    .local v0, "re":Landroid/os/RemoteException;
    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/easyonehand/EOHUtils;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/easyonehand/EOHUtils;->mSingleton:Lcom/sec/android/easyonehand/EOHUtils;

    return-object v0
.end method

.method private getLockSettings()Lcom/android/internal/widget/ILockSettings;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

    if-nez v0, :cond_0

    .line 222
    const-string v0, "lock_settings"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/widget/ILockSettings$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/widget/ILockSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mLockSettingsService:Lcom/android/internal/widget/ILockSettings;

    return-object v0
.end method

.method private getLong(Ljava/lang/String;JI)J
    .locals 2
    .param p1, "secureSettingKey"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J
    .param p4, "userId"    # I

    .prologue
    .line 237
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHUtils;->getLockSettings()Lcom/android/internal/widget/ILockSettings;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/android/internal/widget/ILockSettings;->getLong(Ljava/lang/String;JI)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 239
    .end local p2    # "defaultValue":J
    :goto_0
    return-wide p2

    .line 238
    .restart local p2    # "defaultValue":J
    :catch_0
    move-exception v0

    .line 239
    .local v0, "re":Landroid/os/RemoteException;
    goto :goto_0
.end method

.method public static injectKeyEvent(I)V
    .locals 14
    .param p0, "keyCode"    # I

    .prologue
    .line 111
    if-gez p0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 115
    .local v2, "now":J
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x101

    move-wide v4, v2

    move v7, p0

    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHUtils;->injectKeyEvent(Landroid/view/KeyEvent;)V

    .line 117
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x101

    move-wide v4, v2

    move v7, p0

    invoke-direct/range {v1 .. v13}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHUtils;->injectKeyEvent(Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method public static injectKeyEvent(Landroid/view/KeyEvent;)V
    .locals 3
    .param p0, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "injectKeyEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    .line 133
    return-void
.end method

.method private static isUseVibetonz()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public createShadowIcon(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "icon"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const v10, 0x3f733333    # 0.95f

    const/4 v7, 0x0

    .line 381
    if-nez p1, :cond_0

    move-object v0, v7

    .line 412
    :goto_0
    return-object v0

    .line 385
    :cond_0
    const/4 v0, 0x0

    .line 388
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v2, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 389
    .local v2, "density":I
    div-int/lit16 v8, v2, 0xa0

    mul-int/lit8 v6, v8, 0x4

    .line 391
    .local v6, "shadowPixel":I
    add-int v8, p3, v6

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 392
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 393
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 394
    .local v5, "shadowPaint":Landroid/graphics/Paint;
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 395
    .local v4, "m":Landroid/graphics/Matrix;
    const v8, 0x3f733333    # 0.95f

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 396
    int-to-float v8, p2

    int-to-float v9, p2

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    int-to-float v9, v6

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 397
    new-instance v8, Landroid/graphics/LightingColorFilter;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 398
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 399
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 400
    const/16 v8, 0x33

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 401
    invoke-virtual {v1, p1, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 402
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v1, p1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 405
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "density":I
    .end local v4    # "m":Landroid/graphics/Matrix;
    .end local v5    # "shadowPaint":Landroid/graphics/Paint;
    .end local v6    # "shadowPixel":I
    :catch_0
    move-exception v3

    .line 406
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    move-object v0, v7

    .line 407
    goto :goto_0

    .line 408
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v3

    .line 409
    .local v3, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "createShadowIcon() Exception e="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public createShadowIcon(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 374
    if-nez p1, :cond_0

    .line 375
    const/4 v0, 0x0

    .line 377
    .end local p1    # "icon":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v0

    .restart local p1    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_0
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/easyonehand/EOHUtils;->createShadowIcon(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "iconId"    # I
    .param p3, "dpi"    # I

    .prologue
    .line 477
    :try_start_0
    invoke-virtual {p1, p2, p3}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 481
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v0

    .line 478
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v1

    .line 479
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v0, 0x0

    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method getLauncherLargeIconDensity()I
    .locals 5

    .prologue
    const/16 v3, 0x140

    .line 445
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 446
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 447
    .local v0, "density":I
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v2, v4, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 449
    .local v2, "sw":I
    const/16 v4, 0x258

    if-ge v2, v4, :cond_0

    .line 470
    .end local v0    # "density":I
    :goto_0
    return v0

    .line 454
    .restart local v0    # "density":I
    :cond_0
    sparse-switch v0, :sswitch_data_0

    .line 470
    int-to-float v3, v0

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v0, v3

    goto :goto_0

    .line 456
    :sswitch_0
    const/16 v0, 0xa0

    goto :goto_0

    .line 458
    :sswitch_1
    const/16 v0, 0xf0

    goto :goto_0

    :sswitch_2
    move v0, v3

    .line 460
    goto :goto_0

    :sswitch_3
    move v0, v3

    .line 462
    goto :goto_0

    .line 464
    :sswitch_4
    const/16 v0, 0x1e0

    goto :goto_0

    .line 466
    :sswitch_5
    const/16 v0, 0x280

    goto :goto_0

    .line 454
    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_1
        0xd5 -> :sswitch_2
        0xf0 -> :sswitch_3
        0x140 -> :sswitch_4
        0x1e0 -> :sswitch_5
    .end sparse-switch
.end method

.method public hideStatusBar()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    if-nez v0, :cond_0

    .line 141
    sget-object v0, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    const-string v1, "Can\'t get a instance of StatusBarManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v2, 0x7f070000

    .line 97
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mAudioManager:Landroid/media/AudioManager;

    .line 98
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    .line 99
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mPowerManager:Landroid/os/PowerManager;

    .line 100
    const-string v0, "statusbar"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mStatusBarMgr:Landroid/app/StatusBarManager;

    .line 101
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    .line 102
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v1, 0x103012b

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToast:Landroid/widget/Toast;

    .line 105
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToastLong:Landroid/widget/Toast;

    .line 107
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_COMMON_CONFIG_CHANGEABLE_UI"

    const-string v2, "none"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->themeType:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public isCurrentUserOwner()Z
    .locals 5

    .prologue
    .line 362
    const/4 v1, 0x1

    .line 364
    .local v1, "isOwner":Z
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    const/4 v1, 0x0

    .line 370
    :cond_0
    :goto_0
    return v1

    .line 367
    :catch_0
    move-exception v0

    .line 368
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isDocomoPhonebook()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 428
    iget v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 429
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    .local v0, "contactPackage":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 432
    iput v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook:I

    .line 437
    .end local v0    # "contactPackage":Ljava/lang/String;
    :cond_0
    :goto_0
    iget v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook:I

    if-ne v3, v1, :cond_2

    :goto_1
    return v1

    .line 434
    .restart local v0    # "contactPackage":Ljava/lang/String;
    :cond_1
    iput v1, p0, Lcom/sec/android/easyonehand/EOHUtils;->isDocomoPhonebook:I

    goto :goto_0

    .end local v0    # "contactPackage":Ljava/lang/String;
    :cond_2
    move v1, v2

    .line 437
    goto :goto_1
.end method

.method public isFingerprintLockScreenEnabled()Z
    .locals 5

    .prologue
    .line 249
    const/4 v1, 0x0

    .line 251
    .local v1, "isFingerLocked":Z
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/sec/android/easyonehand/EOHUtils;->isLockFingerprintEnabled(I)Z

    move-result v1

    .line 252
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHUtils;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 253
    sget-object v2, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FingerPrint lock ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return v1

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isKeyguardHidden()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public isKeyguardLocked()Z
    .locals 3

    .prologue
    .line 196
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 199
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    const/4 v1, 0x1

    .line 204
    .end local v0    # "keyguardManager":Landroid/app/KeyguardManager;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isKeyguardLockedSecure()Z
    .locals 3

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 277
    .local v0, "keyguardManager":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    const/4 v1, 0x1

    .line 282
    .end local v0    # "keyguardManager":Landroid/app/KeyguardManager;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isLockFingerprintEnabled(I)Z
    .locals 6
    .param p1, "userId"    # I

    .prologue
    const-wide/32 v4, 0x61000

    const/4 v0, 0x0

    .line 244
    const-string v1, "lock_fingerprint_autolock"

    invoke-direct {p0, v1, v0, p1}, Lcom/sec/android/easyonehand/EOHUtils;->getBoolean(Ljava/lang/String;ZI)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "lockscreen.password_type"

    invoke-direct {p0, v1, v4, v5, p1}, Lcom/sec/android/easyonehand/EOHUtils;->getLong(Ljava/lang/String;JI)J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method isSupportSideWindow()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 172
    iget v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mSupportSideWindow:I

    if-ltz v3, :cond_1

    .line 173
    iget v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mSupportSideWindow:I

    if-ne v3, v1, :cond_0

    .line 188
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 173
    goto :goto_0

    .line 176
    :cond_1
    const-string v3, "ro.product.device"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "device":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 179
    const-string v3, "mega2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "vasta"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "lentis"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "k3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "kwifi"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "kqlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "kvolte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "kactivelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "kcat6"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "kccat6"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 182
    :cond_2
    iput v1, p0, Lcom/sec/android/easyonehand/EOHUtils;->mSupportSideWindow:I

    goto :goto_0

    .line 185
    :cond_3
    iput v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mSupportSideWindow:I

    :cond_4
    move v1, v2

    .line 188
    goto :goto_0
.end method

.method loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHUtils;->getLauncherLargeIconDensity()I

    move-result v2

    .line 550
    .local v2, "dpi":I
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v7, :cond_2

    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 553
    .local v0, "cinfo":Landroid/content/pm/ComponentInfo;
    :goto_0
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 555
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v6, 0x0

    .line 557
    .local v6, "resources":Landroid/content/res/Resources;
    :try_start_0
    iget-object v7, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v5, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 562
    :goto_1
    const/4 v1, 0x0

    .line 563
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v6, :cond_0

    .line 564
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v7, :cond_0

    .line 565
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHUtils;->themeType:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHUtils;->themeType:Ljava/lang/String;

    const-string v8, "theme"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/sec/android/easyonehand/EOHUtils;->mIsUseDefaultTheme:Z

    if-nez v7, :cond_3

    .line 566
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHUtils;->loadIconForResolveTheme(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 573
    :goto_2
    if-nez v1, :cond_0

    .line 574
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v4

    .line 575
    .local v4, "iconId":I
    if-eqz v4, :cond_0

    .line 576
    invoke-virtual {p0, v6, v4, v2}, Lcom/sec/android/easyonehand/EOHUtils;->getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 581
    .end local v4    # "iconId":I
    :cond_0
    if-nez v1, :cond_1

    .line 582
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x1080093

    invoke-virtual {p0, v7, v8, v2}, Lcom/sec/android/easyonehand/EOHUtils;->getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 585
    :cond_1
    return-object v1

    .line 550
    .end local v0    # "cinfo":Landroid/content/pm/ComponentInfo;
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "resources":Landroid/content/res/Resources;
    :cond_2
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto :goto_0

    .line 558
    .restart local v0    # "cinfo":Landroid/content/pm/ComponentInfo;
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    .restart local v6    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v3

    .line 559
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1

    .line 569
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    :cond_3
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v7, v7, Landroid/content/pm/ActivityInfo;->icon:I

    if-eqz v7, :cond_4

    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    :goto_3
    invoke-virtual {v5, v7}, Landroid/content/pm/PackageManager;->getCSCPackageItemIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_2

    :cond_4
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_3
.end method

.method loadIconForResolveTheme(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 22
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 485
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/easyonehand/EOHUtils;->getLauncherLargeIconDensity()I

    move-result v8

    .line 486
    .local v8, "dpi":I
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 489
    .local v6, "cinfo":Landroid/content/pm/ComponentInfo;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 491
    .local v15, "pm":Landroid/content/pm/PackageManager;
    const/16 v16, 0x0

    .line 493
    .local v16, "resources":Landroid/content/res/Resources;
    :try_start_0
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 498
    :goto_1
    const/4 v7, 0x0

    .line 499
    .local v7, "d":Landroid/graphics/drawable/Drawable;
    const/4 v11, 0x0

    .line 501
    .local v11, "icon":Landroid/graphics/drawable/Drawable;
    if-nez v11, :cond_0

    .line 502
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v12

    .line 503
    .local v12, "iconId":I
    if-eqz v12, :cond_0

    .line 504
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v12, v8}, Lcom/sec/android/easyonehand/EOHUtils;->getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 508
    .end local v12    # "iconId":I
    :cond_0
    if-eqz v11, :cond_3

    .line 509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 511
    :cond_1
    const/4 v4, 0x0

    .line 513
    .local v4, "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_2

    :try_start_1
    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    .end local v4    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 519
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .line 521
    .local v17, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 523
    .local v10, "height":I
    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    .line 524
    .local v14, "p":Landroid/graphics/Paint;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 525
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 526
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 528
    sget-object v18, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v10, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .local v3, "b":Landroid/graphics/Bitmap;
    move-object/from16 v18, v11

    .line 529
    check-cast v18, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 530
    .local v13, "in_bit":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 531
    .local v5, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v5, v0, v1, v2, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 532
    invoke-virtual {v5}, Landroid/graphics/Canvas;->save()I

    .line 533
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    int-to-float v0, v10

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 534
    const/high16 v18, 0x3f400000    # 0.75f

    const/high16 v19, 0x3f400000    # 0.75f

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 535
    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v13, v0, v10, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v18

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    neg-int v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v5, v0, v1, v2, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 536
    invoke-virtual {v5}, Landroid/graphics/Canvas;->restore()V

    .line 538
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "d":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v7, v0, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 542
    .end local v3    # "b":Landroid/graphics/Bitmap;
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "height":I
    .end local v13    # "in_bit":Landroid/graphics/Bitmap;
    .end local v14    # "p":Landroid/graphics/Paint;
    .end local v17    # "width":I
    .restart local v7    # "d":Landroid/graphics/drawable/Drawable;
    :cond_3
    sget-object v18, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "loadIconForResolveTheme() themeBackground="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHUtils;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", d="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", icon="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    return-object v7

    .line 486
    .end local v6    # "cinfo":Landroid/content/pm/ComponentInfo;
    .end local v7    # "d":Landroid/graphics/drawable/Drawable;
    .end local v11    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v15    # "pm":Landroid/content/pm/PackageManager;
    .end local v16    # "resources":Landroid/content/res/Resources;
    :cond_4
    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto/16 :goto_0

    .line 494
    .restart local v6    # "cinfo":Landroid/content/pm/ComponentInfo;
    .restart local v15    # "pm":Landroid/content/pm/PackageManager;
    .restart local v16    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v9

    .line 495
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 514
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7    # "d":Landroid/graphics/drawable/Drawable;
    .restart local v11    # "icon":Landroid/graphics/drawable/Drawable;
    :catch_1
    move-exception v9

    .line 515
    .local v9, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v18, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "3rd_party_icon_menu  failed! "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public performHapticFeedbackLw(I)Z
    .locals 12
    .param p1, "effectId"    # I

    .prologue
    const/16 v9, 0x9

    const-wide/16 v10, 0x32

    const/4 v8, -0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 591
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->hasVibrator()Z

    move-result v6

    if-nez v6, :cond_1

    .line 679
    :cond_0
    :goto_0
    return v5

    .line 595
    :cond_1
    sget-boolean v6, Lcom/sec/android/easyonehand/EOHUtils;->DEBUG:Z

    if-eqz v6, :cond_2

    sget-object v6, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    const-string v7, "performHapticFeedbackLw()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :cond_2
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "haptic_feedback_enabled"

    invoke-static {v6, v7, v5, v8}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-nez v6, :cond_5

    move v2, v4

    .line 602
    .local v2, "hapticsDisabled":Z
    :goto_1
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "psm_switch"

    invoke-static {v6, v7, v5, v8}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_6

    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "powersaving_switch"

    invoke-static {v6, v7, v5, v8}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_6

    move v0, v4

    .line 610
    .local v0, "PowerSavingModeEnabled":Z
    :goto_2
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "psm_haptic_feedback"

    invoke-static {v6, v7, v5, v8}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_7

    move v1, v4

    .line 613
    .local v1, "TurnOffHapticFeedbackEnabled":Z
    :goto_3
    if-eqz v0, :cond_3

    if-nez v1, :cond_4

    :cond_3
    if-eqz v2, :cond_8

    .line 614
    :cond_4
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHUtils;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 615
    sget-object v4, Lcom/sec/android/easyonehand/EOHUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "haptic disabled by policy : hapticsDisabled = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " PowerSavingModeEnabled = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " TurnOffHapticFeedbackEnabled = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v0    # "PowerSavingModeEnabled":Z
    .end local v1    # "TurnOffHapticFeedbackEnabled":Z
    .end local v2    # "hapticsDisabled":Z
    :cond_5
    move v2, v5

    .line 598
    goto :goto_1

    .restart local v2    # "hapticsDisabled":Z
    :cond_6
    move v0, v5

    .line 602
    goto :goto_2

    .restart local v0    # "PowerSavingModeEnabled":Z
    :cond_7
    move v1, v5

    .line 610
    goto :goto_3

    .line 622
    .restart local v1    # "TurnOffHapticFeedbackEnabled":Z
    :cond_8
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->isUseVibetonz()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 623
    sparse-switch p1, :sswitch_data_0

    .line 634
    if-lt p1, v9, :cond_0

    const/16 v6, 0x16

    if-gt p1, v6, :cond_0

    .line 636
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v6, Landroid/os/SystemVibrator$MagnitudeType;->TouchMagnitude:Landroid/os/SystemVibrator$MagnitudeType;

    invoke-virtual {v5, p1, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe(ILandroid/os/SystemVibrator$MagnitudeType;)V

    move v5, v4

    .line 637
    goto/16 :goto_0

    .line 629
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v6, Landroid/os/SystemVibrator$MagnitudeType;->TouchMagnitude:Landroid/os/SystemVibrator$MagnitudeType;

    invoke-virtual {v5, v9, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe(ILandroid/os/SystemVibrator$MagnitudeType;)V

    move v5, v4

    .line 631
    goto/16 :goto_0

    .line 642
    :cond_9
    new-array v3, v4, [J

    .line 643
    .local v3, "pattern":[J
    sparse-switch p1, :sswitch_data_1

    goto/16 :goto_0

    .line 649
    :sswitch_1
    aput-wide v10, v3, v5

    .line 673
    :goto_4
    array-length v6, v3

    if-ne v6, v4, :cond_a

    .line 674
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    aget-wide v8, v3, v5

    invoke-virtual {v6, v8, v9}, Landroid/os/SystemVibrator;->vibrate(J)V

    :goto_5
    move v5, v4

    .line 679
    goto/16 :goto_0

    .line 653
    :sswitch_2
    aput-wide v10, v3, v5

    goto :goto_4

    .line 656
    :sswitch_3
    const-wide/16 v6, 0x64

    aput-wide v6, v3, v5

    goto :goto_4

    .line 659
    :sswitch_4
    const-wide/16 v6, 0x1f4

    aput-wide v6, v3, v5

    goto :goto_4

    .line 665
    :sswitch_5
    const-wide/16 v6, 0x5dc

    aput-wide v6, v3, v5

    goto :goto_4

    .line 668
    :sswitch_6
    aput-wide v10, v3, v5

    goto :goto_4

    .line 676
    :cond_a
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    const/4 v6, -0x1

    invoke-virtual {v5, v3, v6}, Landroid/os/SystemVibrator;->vibrate([JI)V

    goto :goto_5

    .line 623
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x2710 -> :sswitch_0
        0x2711 -> :sswitch_0
    .end sparse-switch

    .line 643
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_1
        0x3 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_3
        0xc -> :sswitch_4
        0xd -> :sswitch_5
        0xe -> :sswitch_6
        0x10 -> :sswitch_5
        0x11 -> :sswitch_5
        0x12 -> :sswitch_5
        0x2710 -> :sswitch_1
        0x2711 -> :sswitch_1
    .end sparse-switch
.end method

.method playLongHaptic()V
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    const-wide/16 v2, 0x23

    invoke-virtual {v0, v2, v3}, Landroid/os/SystemVibrator;->vibrate(J)V

    .line 149
    return-void
.end method

.method playShortHaptic()V
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mVibrator:Landroid/os/SystemVibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Landroid/os/SystemVibrator;->vibrate(J)V

    .line 153
    return-void
.end method

.method public playSimpleKeyTone()V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/easyonehand/EOHUtils;->playSimpleKeyTone(Z)V

    .line 163
    return-void
.end method

.method public playSimpleKeyTone(Z)V
    .locals 2
    .param p1, "bforce"    # Z

    .prologue
    .line 166
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mAudioManager:Landroid/media/AudioManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 169
    :cond_1
    return-void
.end method

.method public playSystemHaptic()V
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/easyonehand/EOHUtils;->performHapticFeedbackLw(I)Z

    .line 159
    :cond_0
    return-void
.end method

.method setRippleButtonBackgroundColor(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 416
    if-nez p1, :cond_1

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 420
    :cond_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 421
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/RippleDrawable;

    .line 422
    .local v0, "d":Landroid/graphics/drawable/RippleDrawable;
    if-eqz v0, :cond_0

    .line 423
    const v2, 0x7f040004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/RippleDrawable;->setColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public showErrorToast(I)V
    .locals 8
    .param p1, "errIdd"    # I

    .prologue
    const v7, 0x7f070012

    const v4, 0x7f07000c

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 297
    const/4 v0, 0x0

    .line 299
    .local v0, "full":Ljava/lang/String;
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    .line 300
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "src":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f070014

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 302
    .local v2, "sub":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 329
    .end local v1    # "src":Ljava/lang/String;
    .end local v2    # "sub":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 331
    return-void

    .line 303
    :cond_1
    const/16 v3, 0xc

    if-ne p1, v3, :cond_2

    .line 304
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 305
    .restart local v1    # "src":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f07001c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 306
    .restart local v2    # "sub":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 307
    .end local v1    # "src":Ljava/lang/String;
    .end local v2    # "sub":Ljava/lang/String;
    :cond_2
    const/16 v3, 0x8

    if-ne p1, v3, :cond_3

    .line 308
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 309
    .restart local v1    # "src":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f07001b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 310
    .restart local v2    # "sub":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 311
    .end local v1    # "src":Ljava/lang/String;
    .end local v2    # "sub":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x3

    if-ne p1, v3, :cond_4

    .line 312
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 313
    .restart local v1    # "src":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f070017

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 314
    .restart local v2    # "sub":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315
    .end local v1    # "src":Ljava/lang/String;
    .end local v2    # "sub":Ljava/lang/String;
    :cond_4
    const/16 v3, 0xb

    if-ne p1, v3, :cond_5

    .line 316
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 317
    .restart local v1    # "src":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f070018

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 318
    .restart local v2    # "sub":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 319
    .end local v1    # "src":Ljava/lang/String;
    .end local v2    # "sub":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x5

    if-ne p1, v3, :cond_6

    .line 320
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 321
    .restart local v1    # "src":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f070016

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 322
    .restart local v2    # "sub":Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 323
    .end local v1    # "src":Ljava/lang/String;
    .end local v2    # "sub":Ljava/lang/String;
    :cond_6
    const/4 v3, 0x4

    if-ne p1, v3, :cond_7

    .line 324
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f070013

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 325
    :cond_7
    const/16 v3, 0x9

    if-ne p1, v3, :cond_0

    .line 326
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    const v4, 0x7f070011

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public showToast(I)V
    .locals 5
    .param p1, "stringId"    # I

    .prologue
    .line 340
    const/4 v0, 0x0

    .line 342
    .local v0, "full":Ljava/lang/String;
    const v2, 0x7f070004

    if-eq p1, v2, :cond_0

    const v2, 0x7f070002

    if-ne p1, v2, :cond_1

    .line 343
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToastLong:Landroid/widget/Toast;

    invoke-virtual {v2, p1}, Landroid/widget/Toast;->setText(I)V

    .line 344
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToastLong:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 359
    :goto_0
    return-void

    .line 348
    :cond_1
    const v2, 0x7f070006

    if-eq p1, v2, :cond_2

    const v2, 0x7f070007

    if-ne p1, v2, :cond_3

    .line 349
    :cond_2
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "src":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 353
    .end local v1    # "src":Ljava/lang/String;
    :cond_3
    if-nez v0, :cond_4

    .line 354
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2, p1}, Landroid/widget/Toast;->setText(I)V

    .line 358
    :goto_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 356
    :cond_4
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public userActivity()V
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHUtils;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/os/PowerManager;->userActivity(JZ)V

    .line 137
    return-void
.end method
