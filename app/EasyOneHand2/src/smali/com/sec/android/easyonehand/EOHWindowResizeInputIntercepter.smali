.class public Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;
.super Ljava/lang/Object;
.source "EOHWindowResizeInputIntercepter.java"


# static fields
.field static final DEBUG:Z

.field private static TAG:Ljava/lang/String;

.field private static mResizingViewShowing:Z


# instance fields
.field mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

.field private mPrevTouchX:I

.field private mPrevTouchY:I

.field private mResizingModeHandle:I

.field private mTouchDownX:I

.field private mTouchDownY:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    const-string v0, "EOHWindowResizeInputIntercepter"

    sput-object v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->TAG:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    .line 24
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->DEBUG:Z

    return-void
.end method

.method constructor <init>(Lcom/sec/android/easyonehand/EOHWindowController;)V
    .locals 2
    .param p1, "controller"    # Lcom/sec/android/easyonehand/EOHWindowController;

    .prologue
    const/16 v1, -0x1388

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingModeHandle:I

    .line 16
    iput v1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    .line 18
    iput v1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownY:I

    .line 20
    iput v1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    .line 22
    iput v1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    .line 29
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    .line 31
    return-void
.end method

.method private adjustResizingFrameView(IIII)V
    .locals 6
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "downX"    # I
    .param p4, "downY"    # I

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingModeHandle:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    .line 100
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    iget v1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingModeHandle:I

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/easyonehand/EOHWindowController;->adjustResizingFrameView(IIIII)V

    goto :goto_0
.end method


# virtual methods
.method public isInterceptMotionEvt()Z
    .locals 1

    .prologue
    .line 118
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    return v0
.end method

.method public onInterceptEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v7, -0x1388

    .line 34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 37
    .local v0, "action":I
    and-int/lit16 v5, v0, 0xff

    packed-switch v5, :pswitch_data_0

    .line 84
    :cond_0
    :goto_0
    iget v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingModeHandle:I

    const/16 v6, 0x64

    if-eq v5, v6, :cond_4

    .line 85
    const/4 v5, 0x1

    .line 88
    :goto_1
    return v5

    .line 40
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    .line 41
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownY:I

    .line 42
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 43
    sget-object v5, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_DOWN : ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "mResizingViewShowing = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v5}, Lcom/sec/android/easyonehand/EOHWindowController;->updateEasyOneHandWindowLayout()V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->stopIntercept()V

    .line 53
    iput v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    iput v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    iput v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownY:I

    iput v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    goto :goto_0

    .line 57
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 58
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 60
    .local v4, "y":F
    sget-boolean v5, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    if-eqz v5, :cond_0

    .line 61
    iget v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingModeHandle:I

    const/16 v6, 0x6b

    if-ne v5, v6, :cond_2

    .line 62
    iget v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    if-ne v5, v7, :cond_1

    .line 63
    float-to-int v5, v3

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    .line 64
    float-to-int v5, v4

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownY:I

    .line 66
    :cond_1
    iget v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    int-to-float v5, v5

    sub-float v5, v3, v5

    float-to-int v1, v5

    .line 67
    .local v1, "deltaX":I
    iget v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    int-to-float v5, v5

    sub-float v5, v4, v5

    float-to-int v2, v5

    .line 69
    .local v2, "deltaY":I
    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v5, v1, v2}, Lcom/sec/android/easyonehand/EOHWindowController;->moveResizingFrameView(II)V

    .line 71
    float-to-int v5, v3

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchX:I

    .line 72
    float-to-int v5, v4

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mPrevTouchY:I

    goto/16 :goto_0

    .line 74
    .end local v1    # "deltaX":I
    .end local v2    # "deltaY":I
    :cond_2
    iget v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    if-ne v5, v7, :cond_3

    .line 75
    float-to-int v5, v3

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    .line 76
    float-to-int v5, v4

    iput v5, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownY:I

    .line 78
    :cond_3
    float-to-int v5, v3

    float-to-int v6, v4

    iget v7, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownX:I

    iget v8, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mTouchDownY:I

    invoke-direct {p0, v5, v6, v7, v8}, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->adjustResizingFrameView(IIII)V

    goto/16 :goto_0

    .line 88
    .end local v3    # "x":F
    .end local v4    # "y":F
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 37
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public startIntercept(I)V
    .locals 2
    .param p1, "handleMode"    # I

    .prologue
    .line 103
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 104
    sget-object v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->TAG:Ljava/lang/String;

    const-string v1, " startIntercept "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    .line 106
    iput p1, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingModeHandle:I

    .line 107
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0, p1}, Lcom/sec/android/easyonehand/EOHWindowController;->showResizingFrameView(I)V

    .line 108
    return-void
.end method

.method public stopIntercept()V
    .locals 2

    .prologue
    .line 111
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->TAG:Ljava/lang/String;

    const-string v1, " stopIntercept "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mResizingViewShowing:Z

    .line 114
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHWindowResizeInputIntercepter;->mEOHWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->hideResizingFrameView()V

    .line 115
    return-void
.end method
