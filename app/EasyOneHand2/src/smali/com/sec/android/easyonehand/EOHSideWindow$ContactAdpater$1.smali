.class Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;
.super Ljava/lang/Object;
.source "EOHSideWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;I)V
    .locals 0

    .prologue
    .line 1050
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    iput p2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$000(Lcom/sec/android/easyonehand/EOHSideWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 1054
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mIsEditMode:Z
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$200(Lcom/sec/android/easyonehand/EOHSideWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1055
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    iget-object v0, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    iget v1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->val$position:I

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHSideWindow;->removeContactMenu(I)V

    .line 1063
    :goto_0
    return-void

    .line 1057
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    iget-object v1, v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->this$0:Lcom/sec/android/easyonehand/EOHSideWindow;

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->mItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->access$1500(Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;)Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->val$position:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    # getter for: Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->lookup:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->access$1000(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/easyonehand/EOHSideWindow;->mContactLookup:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/sec/android/easyonehand/EOHSideWindow;->access$1202(Lcom/sec/android/easyonehand/EOHSideWindow;Ljava/lang/String;)Ljava/lang/String;

    .line 1058
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater$1;->this$1:Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHSideWindow$ContactAdpater;->closeQuickContact()V

    goto :goto_0
.end method
