.class final Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;
.super Landroid/view/View;
.source "EOHBackgroundWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHBackgroundWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MagnifyBackgroundView"
.end annotation


# instance fields
.field mPaint:Landroid/graphics/Paint;

.field mTransParentRect:Landroid/graphics/Rect;

.field private mwindowAdded:Z

.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 228
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .line 229
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 222
    iput-boolean v3, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mwindowAdded:Z

    .line 226
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mTransParentRect:Landroid/graphics/Rect;

    .line 230
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mPaint:Landroid/graphics/Paint;

    .line 231
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 232
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 234
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;
    .param p1, "x1"    # Z

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->setWindowAdded(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->isWindowAdded()Z

    move-result v0

    return v0
.end method

.method private isWindowAdded()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mwindowAdded:Z

    return v0
.end method

.method private setWindowAdded(Z)V
    .locals 0
    .param p1, "added"    # Z

    .prologue
    .line 258
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mwindowAdded:Z

    .line 259
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$600(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mTransParentRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 251
    return-void

    .line 247
    :cond_0
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method public setTransparentArea(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$MagnifyBackgroundView;->mTransParentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 238
    return-void
.end method
