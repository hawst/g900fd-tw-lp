.class Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;
.super Lcom/android/internal/policy/impl/WindowOrientationListener;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyOrientationListener"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    .line 471
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {p1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/android/internal/policy/impl/WindowOrientationListener;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    .line 472
    iput-object p2, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->mContext:Landroid/content/Context;

    .line 473
    return-void
.end method


# virtual methods
.method public onProposedRotationChanged(I)V
    .locals 5
    .param p1, "rotation"    # I

    .prologue
    const/4 v4, 0x4

    .line 476
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$400()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProposedRotationChanged() : rotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 479
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$1100(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 481
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    if-nez p1, :cond_2

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EasyOneHandService$MyOrientationListener;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 485
    :cond_2
    return-void
.end method
