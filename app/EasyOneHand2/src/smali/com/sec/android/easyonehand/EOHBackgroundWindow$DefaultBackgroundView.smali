.class final Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;
.super Landroid/view/View;
.source "EOHBackgroundWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHBackgroundWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DefaultBackgroundView"
.end annotation


# instance fields
.field private mwindowAdded:Z

.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    .line 187
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->mwindowAdded:Z

    .line 188
    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->isWindowAdded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;
    .param p1, "x1"    # Z

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->setWindowAdded(Z)V

    return-void
.end method

.method private isWindowAdded()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->mwindowAdded:Z

    return v0
.end method

.method private setWindowAdded(Z)V
    .locals 0
    .param p1, "added"    # Z

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->mwindowAdded:Z

    .line 196
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mScreenRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$600(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$DefaultBackgroundView;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mBackgroundImg:Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$500(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 207
    :goto_0
    return-void

    .line 205
    :cond_0
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method
