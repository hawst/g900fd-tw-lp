.class Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;
.super Landroid/os/AsyncTask;
.source "EOHShortcutAddWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;


# direct methods
.method private constructor <init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Lcom/sec/android/easyonehand/EOHShortcutAddWindow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
    .param p2, "x1"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow$1;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->fillGridView()V

    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 116
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mMainView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$300(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f09002b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    # setter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;
    invoke-static {v1, v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$202(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Landroid/widget/GridView;)Landroid/widget/GridView;

    .line 117
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$200(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/GridView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridAdapter:Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$400(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressCircle:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$100(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ProgressTask;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressCircle:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$100(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 106
    return-void
.end method
