.class Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;
.super Ljava/lang/Object;
.source "EOHBackgroundWindow.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHBackgroundWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WindowHideHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;


# direct methods
.method private constructor <init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow;
    .param p2, "x1"    # Lcom/sec/android/easyonehand/EOHBackgroundWindow$1;

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;-><init>(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 163
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 179
    :cond_0
    :goto_0
    return v2

    .line 165
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$200(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$200(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideDefaultBg()V
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$300(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V

    goto :goto_0

    .line 172
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$200(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$200(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHBackgroundWindow$WindowHideHandler;->this$0:Lcom/sec/android/easyonehand/EOHBackgroundWindow;

    # invokes: Lcom/sec/android/easyonehand/EOHBackgroundWindow;->hideMagnifyBg()V
    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHBackgroundWindow;->access$400(Lcom/sec/android/easyonehand/EOHBackgroundWindow;)V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
