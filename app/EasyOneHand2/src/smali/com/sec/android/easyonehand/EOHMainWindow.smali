.class final Lcom/sec/android/easyonehand/EOHMainWindow;
.super Ljava/lang/Object;
.source "EOHMainWindow.java"


# static fields
.field static final DEBUG:Z


# instance fields
.field private changeViewAnimation:Landroid/animation/ValueAnimator;

.field private finishAlphaAnimation:Landroid/animation/ValueAnimator;

.field private finishScaleAnimation:Landroid/animation/ValueAnimator;

.field private isAnimationRunning:Z

.field private isCloseWithoutAnimation:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentPressedKey:I

.field private mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

.field private mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

.field private mMainDialog:Landroid/app/Dialog;

.field private mMainDialogWindow:Landroid/view/Window;

.field private mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

.field private mMainView:Landroid/view/View;

.field mMoveHandleEventListener:Landroid/view/View$OnLongClickListener;

.field mResizeHandleEventListener:Landroid/view/View$OnTouchListener;

.field mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

.field private startUpAlphaAnimation:Landroid/animation/ValueAnimator;

.field private startupScaleAnimation:Landroid/animation/ValueAnimator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/easyonehand/EOHWindowController;Landroid/content/Context;Z)V
    .locals 3
    .param p1, "controller"    # Lcom/sec/android/easyonehand/EOHWindowController;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "isLeft"    # Z

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    .line 52
    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isCloseWithoutAnimation:Z

    .line 54
    iput v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mCurrentPressedKey:I

    .line 166
    new-instance v0, Lcom/sec/android/easyonehand/EOHMainWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$1;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    .line 207
    new-instance v0, Lcom/sec/android/easyonehand/EOHMainWindow$2;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$2;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mResizeHandleEventListener:Landroid/view/View$OnTouchListener;

    .line 226
    new-instance v0, Lcom/sec/android/easyonehand/EOHMainWindow$3;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$3;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMoveHandleEventListener:Landroid/view/View$OnLongClickListener;

    .line 67
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mContext:Landroid/content/Context;

    .line 69
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 70
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 71
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mContext:Landroid/content/Context;

    if-eqz p3, :cond_0

    const v0, 0x7f030002

    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    .line 72
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    .line 74
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 75
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindow:Landroid/view/Window;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 76
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindow:Landroid/view/Window;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    .line 81
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->getInstance()Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 83
    new-instance v0, Lcom/sec/android/easyonehand/EOHInputFilter;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-direct {v0, p2, v1}, Lcom/sec/android/easyonehand/EOHInputFilter;-><init>(Landroid/content/Context;Lcom/sec/android/easyonehand/EOHWindowController;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    .line 84
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHWindowController;->setInputFilter(Lcom/sec/android/easyonehand/EOHInputFilter;)V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHMainWindow;->setButtonEventHandler()V

    .line 87
    return-void

    .line 71
    :cond_0
    const v0, 0x7f030003

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/easyonehand/EOHMainWindow;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;
    .param p1, "x1"    # F

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHMainWindow;->scaleAnimationEnd(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/easyonehand/EOHMainWindow;Landroid/view/MotionEvent;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/easyonehand/EOHMainWindow;->processMakeTouchToKeyDown(Landroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHWindowController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/easyonehand/EOHMainWindow;)Lcom/sec/android/easyonehand/EOHInputFilter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/easyonehand/EOHMainWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isCloseWithoutAnimation:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/easyonehand/EOHMainWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/easyonehand/EOHMainWindow;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/easyonehand/EOHMainWindow;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHMainWindow;
    .param p1, "x1"    # F

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHMainWindow;->scaleAnimationStart(F)V

    return-void
.end method

.method private processMakeTouchToKeyDown(Landroid/view/MotionEvent;I)Z
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "keyCode"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 236
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 237
    const-string v1, "EOHMainWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processMakeTouchToKeyDown() keyCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_0
    const/16 v1, 0x3e8

    if-ge p2, v1, :cond_1

    .line 240
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v0, :cond_2

    .line 256
    :cond_1
    :goto_0
    return v6

    .line 244
    :cond_2
    iput p2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mCurrentPressedKey:I

    .line 245
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 246
    .local v2, "now":J
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    new-instance v1, Landroid/view/KeyEvent;

    move-wide v4, v2

    move v7, p2

    move v8, v6

    move v9, v6

    invoke-direct/range {v1 .. v9}, Landroid/view/KeyEvent;-><init>(JJIIII)V

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v10, v1, v4}, Lcom/sec/android/easyonehand/EOHInputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V

    .line 249
    const/16 v1, 0xbb

    if-eq p2, v1, :cond_3

    const/4 v1, 0x4

    if-ne p2, v1, :cond_4

    .line 250
    :cond_3
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHUtils;->playSimpleKeyTone()V

    .line 253
    :cond_4
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHUtils;->playSystemHaptic()V

    move v6, v0

    .line 254
    goto :goto_0
.end method

.method private runStartUpAniMation()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 374
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v0

    .line 375
    .local v0, "currentScale":F
    iput-boolean v4, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    .line 376
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowController;->adjustWindowSizePosition()V

    .line 378
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 379
    const-string v1, "EOHMainWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "runStartUpAniMation() winInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    const/16 v2, 0x7e9

    invoke-virtual {v1, v2}, Lcom/sec/android/easyonehand/EOHWindowController;->showBackground(I)V

    .line 385
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    .line 386
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 387
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 388
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/easyonehand/EOHMainWindow$4;

    invoke-direct {v2, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$4;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 397
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const-string v2, "scale"

    new-array v3, v4, [F

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, v0

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    .line 398
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 399
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 400
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/easyonehand/EOHMainWindow$5;

    invoke-direct {v2, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$5;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 413
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/easyonehand/EOHMainWindow$6;

    invoke-direct {v2, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$6;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 420
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 421
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowController;->fillSideWidnowGridView()V

    .line 422
    return-void

    .line 385
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private scaleAnimationEnd(F)V
    .locals 5
    .param p1, "value"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 468
    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    .line 475
    :goto_0
    return-void

    .line 471
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInnerX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    sub-float v3, v4, v3

    sub-float/2addr v3, p1

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    sub-float v3, v4, v3

    div-float v0, v2, v3

    .line 472
    .local v0, "x":F
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInnerY()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    sub-float v3, v4, v3

    sub-float/2addr v3, p1

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    sub-float v3, v4, v3

    div-float v1, v2, v3

    .line 473
    .local v1, "y":F
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/easyonehand/EOHMainWindow;->magnifyWindow(FFF)V

    goto :goto_0
.end method

.method private scaleAnimationStart(F)V
    .locals 5
    .param p1, "value"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 478
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInnerX()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    sub-float v3, v4, v3

    div-float v0, v2, v3

    .line 479
    .local v0, "x":F
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInnerY()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v3

    sub-float v3, v4, v3

    div-float v1, v2, v3

    .line 480
    .local v1, "y":F
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 481
    sub-float v2, v4, p1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/easyonehand/EOHMainWindow;->magnifyWindow(FFF)V

    .line 484
    :cond_0
    return-void
.end method

.method private setButtonEventHandler()V
    .locals 7

    .prologue
    const v6, 0x7f090021

    const v5, 0x7f09001d

    const v4, 0x7f09001b

    const v2, 0x7f090017

    const v3, 0x7f090009

    .line 142
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mSoftKeyEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mResizeHandleEventListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMoveHandleEventListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v2, 0x7f090024

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->setRippleButtonBackgroundColor(Landroid/view/View;)V

    .line 164
    return-void
.end method


# virtual methods
.method public changeViewLayout()V
    .locals 3

    .prologue
    .line 524
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f030002

    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    .line 526
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHMainWindow;->setButtonEventHandler()V

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHMainWindow;->setSideMenuIconState()V

    .line 528
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    .line 530
    return-void

    .line 524
    :cond_0
    const v0, 0x7f030003

    goto :goto_0
.end method

.method public changeViewLayoutAnimation()V
    .locals 4

    .prologue
    .line 533
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    .line 534
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 535
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    .line 538
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/easyonehand/EOHMainWindow$10;

    invoke-direct {v1, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$10;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 547
    return-void

    .line 533
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public createWindowLayout()V
    .locals 6

    .prologue
    .line 487
    const/16 v2, 0x8ca

    .line 490
    .local v2, "windowType":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x148

    const/4 v4, -0x3

    invoke-direct {v0, v2, v3, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 495
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    new-instance v1, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v3, v3, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-direct {v1, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 497
    .local v1, "r":Landroid/graphics/Rect;
    iget v3, v1, Landroid/graphics/Rect;->left:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 498
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 499
    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v4, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 500
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 502
    const/16 v3, 0x33

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 503
    const/4 v3, 0x2

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 504
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x40

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 505
    const-string v3, "EasyOneHand/Controller"

    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 507
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 508
    const-string v3, "EOHMainWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createEasyOneHandWindowLayoutParams lp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :cond_0
    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    .line 511
    return-void
.end method

.method public finishWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 111
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 112
    const-string v0, "EOHMainWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090017

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 125
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    .line 126
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    .line 127
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    .line 128
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    .line 129
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    .line 131
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 132
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 133
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    .line 134
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindow:Landroid/view/Window;

    .line 136
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/easyonehand/EOHViewUnbindHelper;->unbindReferences(Landroid/view/View;)V

    .line 137
    iput-object v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    .line 138
    return-void
.end method

.method public hideWindow()V
    .locals 3

    .prologue
    .line 96
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "EOHMainWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hideWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 99
    return-void
.end method

.method public isAnimationRunning()Z
    .locals 1

    .prologue
    .line 550
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    return v0
.end method

.method public magnifyWindow(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "offsetX"    # F
    .param p3, "offsetY"    # F

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/easyonehand/EOHWindowController;->magnifyWindow(FFF)V

    .line 571
    return-void
.end method

.method public onFontLocaleChanged()V
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewLayout()V

    .line 556
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHMainWindow;->setSideMenuIconState()V

    .line 558
    :cond_0
    return-void
.end method

.method public processClearTouchToKeyUp(Landroid/view/MotionEvent;I)Z
    .locals 11
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "keyCode"    # I

    .prologue
    const/16 v7, 0x3e8

    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 262
    .local v0, "action":I
    sget-boolean v1, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 263
    const-string v1, "EOHMainWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processMakeTouchToKeyUp() keyCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", action="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    if-ne v0, v6, :cond_1

    if-lt p2, v7, :cond_1

    .line 266
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1, v6}, Lcom/sec/android/easyonehand/EOHUtils;->playSimpleKeyTone(Z)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 270
    :cond_1
    if-ne v0, v6, :cond_4

    .line 271
    if-ne p2, v7, :cond_2

    .line 272
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v1, v6}, Lcom/sec/android/easyonehand/EOHWindowController;->closeWindow(Z)V

    .line 295
    :goto_0
    return v6

    .line 276
    :cond_2
    const/16 v1, 0x3eb

    if-ne p2, v1, :cond_3

    .line 277
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/sec/android/easyonehand/EOHWindowController;->toggleSideWindow(I)V

    goto :goto_0

    .line 281
    :cond_3
    const/16 v1, 0x3ec

    if-ne p2, v1, :cond_4

    .line 282
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v1, v6}, Lcom/sec/android/easyonehand/EOHWindowController;->toggleSideWindow(I)V

    goto :goto_0

    .line 287
    :cond_4
    iget v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mCurrentPressedKey:I

    if-ne p2, v1, :cond_5

    .line 288
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 289
    .local v2, "now":J
    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mInputFilter:Lcom/sec/android/easyonehand/EOHInputFilter;

    new-instance v1, Landroid/view/KeyEvent;

    move-wide v4, v2

    move v7, p2

    move v9, v8

    invoke-direct/range {v1 .. v9}, Landroid/view/KeyEvent;-><init>(JJIIII)V

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v10, v1, v4}, Lcom/sec/android/easyonehand/EOHInputFilter;->sendInputEvent(Landroid/view/InputEvent;I)V

    .line 291
    iput v8, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mCurrentPressedKey:I

    goto :goto_0

    .end local v2    # "now":J
    :cond_5
    move v6, v8

    .line 295
    goto :goto_0
.end method

.method public runFinishAnimation()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 425
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowScale()F

    move-result v0

    .line 426
    .local v0, "currentScale":F
    iput-boolean v3, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isAnimationRunning:Z

    .line 427
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    const/16 v2, 0x7e9

    invoke-virtual {v1, v2}, Lcom/sec/android/easyonehand/EOHWindowController;->showBackground(I)V

    .line 429
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const-string v2, "scale"

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, v0

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    .line 431
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    .line 432
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 433
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 434
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/easyonehand/EOHMainWindow$7;

    invoke-direct {v2, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$7;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 442
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 443
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 445
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 446
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 447
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/easyonehand/EOHMainWindow$8;

    invoke-direct {v2, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$8;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 455
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/easyonehand/EOHMainWindow$9;

    invoke-direct {v2, p0}, Lcom/sec/android/easyonehand/EOHMainWindow$9;-><init>(Lcom/sec/android/easyonehand/EOHMainWindow;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 465
    return-void

    .line 431
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public setLandscapeMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 561
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 562
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/easyonehand/EOHMainWindow;->magnifyWindow(FFF)V

    .line 563
    return-void
.end method

.method public setPortraitMode()V
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 567
    return-void
.end method

.method public setSideMenuIconState()V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v11, 0x4

    const/4 v9, 0x0

    .line 299
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    if-eqz v7, :cond_1

    .line 300
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f09000d

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 301
    .local v0, "btnContact":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f09000f

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 303
    .local v3, "btnShortcut":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f090011

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 304
    .local v2, "btnResizeImage":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f090012

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 306
    .local v1, "btnResizeHandle":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f09000c

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 307
    .local v4, "imgBorder":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f09000e

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 308
    .local v5, "imgContact":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainView:Landroid/view/View;

    const v10, 0x7f090010

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 310
    .local v6, "imgShortcut":Landroid/widget/ImageButton;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isSideWindowShowing()Z

    move-result v7

    if-nez v7, :cond_2

    .line 311
    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 312
    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 318
    :goto_0
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHUtils;->isSupportSideWindow()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 319
    :cond_0
    invoke-virtual {v0, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 320
    invoke-virtual {v3, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 321
    invoke-virtual {v4, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 322
    invoke-virtual {v5, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 323
    invoke-virtual {v6, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 325
    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 326
    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 342
    .end local v0    # "btnContact":Landroid/widget/ImageButton;
    .end local v1    # "btnResizeHandle":Landroid/widget/ImageButton;
    .end local v2    # "btnResizeImage":Landroid/widget/ImageButton;
    .end local v3    # "btnShortcut":Landroid/widget/ImageButton;
    .end local v4    # "imgBorder":Landroid/widget/ImageButton;
    .end local v5    # "imgContact":Landroid/widget/ImageButton;
    .end local v6    # "imgShortcut":Landroid/widget/ImageButton;
    :cond_1
    :goto_1
    return-void

    .line 314
    .restart local v0    # "btnContact":Landroid/widget/ImageButton;
    .restart local v1    # "btnResizeHandle":Landroid/widget/ImageButton;
    .restart local v2    # "btnResizeImage":Landroid/widget/ImageButton;
    .restart local v3    # "btnShortcut":Landroid/widget/ImageButton;
    .restart local v4    # "imgBorder":Landroid/widget/ImageButton;
    .restart local v5    # "imgContact":Landroid/widget/ImageButton;
    .restart local v6    # "imgShortcut":Landroid/widget/ImageButton;
    :cond_2
    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 315
    invoke-virtual {v2, v11}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 328
    :cond_3
    invoke-virtual {v0, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 329
    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 330
    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 331
    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 332
    invoke-virtual {v6, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 334
    invoke-virtual {v0, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 335
    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 337
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v7

    if-ne v7, v8, :cond_4

    move v7, v8

    :goto_2
    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 338
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getSideWindowType()I

    move-result v7

    const/4 v10, 0x2

    if-ne v7, v10, :cond_5

    :goto_3
    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1

    :cond_4
    move v7, v9

    .line 337
    goto :goto_2

    :cond_5
    move v8, v9

    .line 338
    goto :goto_3
.end method

.method public setWindowRect()V
    .locals 4

    .prologue
    .line 514
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    iget-object v1, v1, Lcom/sec/android/easyonehand/EOHWindowInfo;->mMainRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 516
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 517
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 518
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 519
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 520
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindow:Landroid/view/Window;

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialogWindowAttr:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 521
    return-void
.end method

.method public showWindow()V
    .locals 3

    .prologue
    .line 102
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "EOHMainWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showWindow() callers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mMainDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 108
    :cond_1
    return-void
.end method

.method public startWindow()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->playShortHaptic()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->playSimpleKeyTone(Z)V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHMainWindow;->runStartUpAniMation()V

    .line 93
    return-void
.end method

.method public stopAllAnimations(Z)V
    .locals 2
    .param p1, "isForce"    # Z

    .prologue
    .line 346
    iput-boolean p1, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->isCloseWithoutAnimation:Z

    .line 348
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHMainWindow;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 349
    const-string v0, "EOHMainWindow"

    const-string v1, "stopAllAnimations()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startUpAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->startupScaleAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishScaleAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 363
    :cond_3
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 364
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->finishAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 367
    :cond_4
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 368
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHMainWindow;->changeViewAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 371
    :cond_5
    return-void
.end method
