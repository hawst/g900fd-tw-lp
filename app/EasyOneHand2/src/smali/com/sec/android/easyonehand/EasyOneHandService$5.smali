.class Lcom/sec/android/easyonehand/EasyOneHandService$5;
.super Landroid/content/BroadcastReceiver;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$5;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 491
    if-eqz p2, :cond_0

    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 492
    const-string v1, "connected"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 493
    .local v0, "isUsbConnected":Z
    if-eqz v0, :cond_0

    .line 494
    iget-object v1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$5;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # invokes: Lcom/sec/android/easyonehand/EasyOneHandService;->closeAllWindowSafe(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$1000(Lcom/sec/android/easyonehand/EasyOneHandService;Z)V

    .line 497
    .end local v0    # "isUsbConnected":Z
    :cond_0
    return-void
.end method
