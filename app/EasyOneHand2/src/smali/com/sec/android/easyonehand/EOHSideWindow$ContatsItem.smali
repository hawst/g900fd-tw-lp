.class Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;
.super Ljava/lang/Object;
.source "EOHSideWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHSideWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ContatsItem"
.end annotation


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private contactId:J

.field private lookup:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private num:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "lookup"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "num"    # Ljava/lang/String;
    .param p5, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1107
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->contactId:J

    .line 1108
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->lookup:Ljava/lang/String;

    .line 1109
    iput-object p4, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->num:Ljava/lang/String;

    .line 1110
    iput-object p3, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->name:Ljava/lang/String;

    .line 1111
    iput-object p5, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;

    .line 1113
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->lookup:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1095
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->bitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;

    .prologue
    .line 1095
    iget-wide v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->contactId:J

    return-wide v0
.end method


# virtual methods
.method public getContactLookup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSideWindow$ContatsItem;->lookup:Ljava/lang/String;

    return-object v0
.end method
