.class final Lcom/sec/android/easyonehand/EOHResizeFrameView;
.super Landroid/view/View;
.source "EOHResizeFrameView.java"


# static fields
.field static final DEBUG:Z


# instance fields
.field private mBounds:Landroid/graphics/Rect;

.field private mCocktailBarWidth:I

.field private mDisplayHeightPixel:I

.field private mDisplaywidthPixel:I

.field private mHandleMode:I

.field private mOriginalBound:Landroid/graphics/Rect;

.field private mResizeHandleLeft:Landroid/graphics/drawable/Drawable;

.field private mResizeHandleRight:Landroid/graphics/drawable/Drawable;

.field private mResizeImg:Landroid/graphics/drawable/Drawable;

.field private mResizeImgLeft:Landroid/graphics/drawable/Drawable;

.field private mResizeImgRight:Landroid/graphics/drawable/Drawable;

.field private mScreenRatioPivotX:F

.field private mTempBound:Landroid/graphics/Rect;

.field private mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

.field private rt:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mOriginalBound:Landroid/graphics/Rect;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mScreenRatioPivotX:F

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImgLeft:Landroid/graphics/drawable/Drawable;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeHandleLeft:Landroid/graphics/drawable/Drawable;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImgRight:Landroid/graphics/drawable/Drawable;

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeHandleRight:Landroid/graphics/drawable/Drawable;

    .line 61
    invoke-static {}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getInstance()Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->rt:Landroid/graphics/Rect;

    .line 63
    return-void
.end method

.method private adjustBoundToScreen(Landroid/graphics/Rect;)V
    .locals 7
    .param p1, "checkBound"    # Landroid/graphics/Rect;

    .prologue
    const/4 v3, 0x0

    .line 227
    sget-boolean v4, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v4, :cond_0

    const-string v4, "EOHResizeFrameView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "adjustBoundToScreen() checkBound="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_0
    iget v4, p1, Landroid/graphics/Rect;->top:I

    if-gez v4, :cond_1

    .line 230
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    iget v5, p1, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v5, v5, 0x0

    add-int/2addr v4, v5

    iput v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 231
    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 233
    :cond_1
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplayHeightPixel:I

    if-le v4, v5, :cond_2

    .line 234
    iget v4, p1, Landroid/graphics/Rect;->top:I

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplayHeightPixel:I

    sub-int/2addr v5, v6

    sub-int/2addr v4, v5

    iput v4, p1, Landroid/graphics/Rect;->top:I

    .line 235
    iget v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplayHeightPixel:I

    iput v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 238
    :cond_2
    const/4 v1, 0x0

    .line 239
    .local v1, "cocktailRight":I
    const/4 v0, 0x0

    .line 240
    .local v0, "cocktailLeft":I
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 241
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v4}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDisplayRotation()I

    move-result v2

    .line 242
    .local v2, "dispRotation":I
    const/4 v4, 0x2

    if-ne v2, v4, :cond_7

    iget v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mCocktailBarWidth:I

    .line 243
    :goto_0
    if-nez v2, :cond_8

    iget v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mCocktailBarWidth:I

    .line 246
    .end local v2    # "dispRotation":I
    :cond_3
    :goto_1
    iget v3, p1, Landroid/graphics/Rect;->left:I

    if-ge v3, v0, :cond_4

    .line 247
    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->left:I

    sub-int v4, v0, v4

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->right:I

    .line 248
    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 250
    :cond_4
    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplaywidthPixel:I

    sub-int/2addr v4, v1

    if-le v3, v4, :cond_5

    .line 251
    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplaywidthPixel:I

    sub-int/2addr v4, v5

    add-int/2addr v4, v1

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 252
    iget v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplaywidthPixel:I

    sub-int/2addr v3, v1

    iput v3, p1, Landroid/graphics/Rect;->right:I

    .line 255
    :cond_5
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v3, :cond_6

    const-string v3, "EOHResizeFrameView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "adjustBoundToScreen() checkBound="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_6
    return-void

    .restart local v2    # "dispRotation":I
    :cond_7
    move v0, v3

    .line 242
    goto :goto_0

    :cond_8
    move v1, v3

    .line 243
    goto :goto_1
.end method

.method private drawResizeFrame(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 77
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getResizeLineThickness()I

    move-result v0

    .line 78
    .local v0, "margin":I
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getResizeHandlerSize()I

    move-result v1

    .line 80
    .local v1, "resizeHandlerSize":I
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImg:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v0

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v0

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeHandleLeft:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v1

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v1

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 86
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeHandleLeft:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 98
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImg:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v0

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v0

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeHandleRight:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v1

    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v1

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeHandleRight:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private setLimitRectForCornerResize(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_2

    .line 202
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMinimumHeight()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 203
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMinimumHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 204
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMinimumWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 207
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMaximumHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 208
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMaximumHeight()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 209
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMaximumWidth()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 222
    :cond_1
    :goto_0
    return-void

    .line 211
    :cond_2
    iget v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    const/16 v1, 0x6a

    if-ne v0, v1, :cond_1

    .line 212
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMinimumHeight()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 213
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMinimumHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 214
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMinimumWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 217
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v1}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMaximumHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 218
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMaximumHeight()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 219
    iget v0, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getMaximumWidth()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method


# virtual methods
.method adjustBound(IIIII)V
    .locals 5
    .param p1, "handleMode"    # I
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "downX"    # I
    .param p5, "downY"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    .line 135
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 136
    const-string v2, "EOHResizeFrameView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adjustBound() mHandleMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", dx="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", dy="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", downX="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", downY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mOriginalBound="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mOriginalBound:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mOriginalBound:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 140
    iget v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    packed-switch v2, :pswitch_data_0

    .line 156
    :goto_0
    iget v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    const/16 v3, 0x69

    if-lt v2, v3, :cond_1

    .line 157
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->setLimitRectForCornerResize(Landroid/graphics/Rect;)V

    .line 159
    :cond_1
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    invoke-direct {p0, v2}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->adjustBoundToScreen(Landroid/graphics/Rect;)V

    .line 160
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 161
    const-string v2, "EOHResizeFrameView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adjustBound() mOriginalBound="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mOriginalBound:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mTempBound="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_2
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 163
    return-void

    .line 142
    :pswitch_0
    sub-int v0, p4, p2

    .line 143
    .local v0, "xDiff":I
    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mScreenRatioPivotX:F

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 144
    .local v1, "yDiff":I
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 145
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 149
    .end local v0    # "xDiff":I
    .end local v1    # "yDiff":I
    :pswitch_1
    sub-int v0, p2, p4

    .line 150
    .restart local v0    # "xDiff":I
    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mScreenRatioPivotX:F

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 151
    .restart local v1    # "yDiff":I
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 152
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mTempBound:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getBounds()Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 101
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "EOHResizeFrameView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBounds() mBounds="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method moveBound(II)V
    .locals 9
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/4 v6, 0x0

    .line 166
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v4

    .line 167
    .local v4, "screenHeight":I
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v5

    .line 169
    .local v5, "screenWdth":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 170
    .local v3, "mTempBound":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 171
    invoke-virtual {v3, p1, p2}, Landroid/graphics/Rect;->offset(II)V

    .line 173
    const/4 v1, 0x0

    .line 174
    .local v1, "cocktailRight":I
    const/4 v0, 0x0

    .line 175
    .local v0, "cocktailLeft":I
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isCocktailBarEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 176
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v7}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getDisplayRotation()I

    move-result v2

    .line 177
    .local v2, "dispRotation":I
    const/4 v7, 0x2

    if-ne v2, v7, :cond_4

    iget v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mCocktailBarWidth:I

    .line 178
    :goto_0
    if-nez v2, :cond_5

    iget v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mCocktailBarWidth:I

    .line 181
    .end local v2    # "dispRotation":I
    :cond_0
    :goto_1
    iget v7, v3, Landroid/graphics/Rect;->left:I

    if-ge v7, v0, :cond_6

    .line 182
    iget v7, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v0

    mul-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v7, v6}, Landroid/graphics/Rect;->offset(II)V

    .line 187
    :cond_1
    :goto_2
    iget v7, v3, Landroid/graphics/Rect;->top:I

    if-gez v7, :cond_7

    .line 188
    iget v7, v3, Landroid/graphics/Rect;->top:I

    mul-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 193
    :cond_2
    :goto_3
    sget-boolean v6, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v6, :cond_3

    .line 194
    const-string v6, "EOHResizeFrameView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "moveBound() mBounds="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mTempBound="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_3
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v6, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 198
    return-void

    .restart local v2    # "dispRotation":I
    :cond_4
    move v0, v6

    .line 177
    goto :goto_0

    :cond_5
    move v1, v6

    .line 178
    goto :goto_1

    .line 183
    .end local v2    # "dispRotation":I
    :cond_6
    iget v7, v3, Landroid/graphics/Rect;->right:I

    sub-int v8, v5, v1

    if-le v7, v8, :cond_1

    .line 184
    sub-int v7, v5, v1

    iget v8, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v7, v8

    invoke-virtual {v3, v7, v6}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_2

    .line 189
    :cond_7
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    if-le v7, v4, :cond_2

    .line 190
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v4, v7

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_3
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isLeftHandMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImgLeft:Landroid/graphics/drawable/Drawable;

    :goto_0
    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImg:Landroid/graphics/drawable/Drawable;

    .line 70
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->drawResizeFrame(Landroid/graphics/Canvas;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImg:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->rt:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 73
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mResizeImgRight:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method setWindowInfo()V
    .locals 6

    .prologue
    .line 108
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 109
    const-string v3, "EOHResizeFrameView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setWindowInfo() mWinInfo="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", callers="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-static {v5}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v1

    .line 112
    .local v1, "screenHeight":I
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    .line 113
    .local v2, "screenWdth":I
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 114
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3, v0}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getFullWindowRect(Landroid/graphics/Rect;)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 118
    int-to-float v3, v1

    int-to-float v4, v2

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mScreenRatioPivotX:F

    .line 120
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mOriginalBound:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 122
    iput v1, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplayHeightPixel:I

    .line 123
    iput v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mDisplaywidthPixel:I

    .line 125
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getCocktailBarWidth()I

    move-result v3

    iput v3, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mCocktailBarWidth:I

    .line 127
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHResizeFrameView;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 128
    const-string v3, "EOHResizeFrameView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setWindowInfo mBounds="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_1
    return-void
.end method

.method public showResizingFrameView(Lcom/sec/android/easyonehand/EOHResizeFrameView;I)V
    .locals 5
    .param p1, "view"    # Lcom/sec/android/easyonehand/EOHResizeFrameView;
    .param p2, "handleMode"    # I

    .prologue
    const/4 v4, 0x0

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHResizeFrameView;->setWindowInfo()V

    .line 261
    iput p2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mHandleMode:I

    .line 263
    const/16 v1, 0x8ca

    .line 266
    .local v1, "windowType":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x158

    const/4 v3, -0x3

    invoke-direct {v0, v1, v2, v3}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 270
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 271
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 272
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenWidth()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 273
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getScreenHeight()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 275
    const/16 v2, 0x33

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 276
    const/4 v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 277
    const-string v2, "EasyOneHand/ResizeHandle"

    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHResizeFrameView;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;

    invoke-virtual {v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2, p1, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 280
    return-void
.end method
