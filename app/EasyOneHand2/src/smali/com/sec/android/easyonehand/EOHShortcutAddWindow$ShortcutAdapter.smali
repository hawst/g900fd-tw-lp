.class Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;
.super Landroid/widget/BaseAdapter;
.source "EOHShortcutAddWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EOHShortcutAddWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ShortcutAdapter"
.end annotation


# instance fields
.field private mComponentName:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIconInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mIconLabelInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:I

.field mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mPackageManger:Landroid/content/pm/PackageManager;

.field private mResolveInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p4, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 428
    new-instance v0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter$1;-><init>(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;)V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 326
    iput-object p2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    .line 327
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 328
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mPackageManger:Landroid/content/pm/PackageManager;

    .line 329
    iput p3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mLayout:I

    .line 330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    .line 332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    .line 333
    iput-object p4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    .line 334
    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addAllItem()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 365
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    .line 366
    .local v1, "N":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_2

    .line 367
    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    new-instance v9, Landroid/content/ComponentName;

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v9, v10, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    sget-boolean v7, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v7, :cond_0

    .line 370
    const-string v8, "EOHShortcutAddWindow"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addAllItem i="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", name="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", dispName="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v10, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v7}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$600(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v8, v7}, Lcom/sec/android/easyonehand/EOHUtils;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 374
    .local v6, "orig":Landroid/graphics/drawable/Drawable;
    instance-of v7, v6, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v7, :cond_1

    .line 375
    move-object v0, v6

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    move-object v4, v0

    .line 376
    .local v4, "l":Landroid/graphics/drawable/LayerDrawable;
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 378
    .end local v4    # "l":Landroid/graphics/drawable/LayerDrawable;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v8, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mPackageManger:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 380
    .local v5, "label":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    .end local v5    # "label":Ljava/lang/String;
    .end local v6    # "orig":Landroid/graphics/drawable/Drawable;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 382
    :catch_0
    move-exception v2

    .line 383
    .local v2, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    iget-object v7, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    const-string v7, "EOHShortcutAddWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception while add icon. e="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 388
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    return-void
.end method

.method public cleanUpMemory()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 337
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 338
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 339
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 340
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 341
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 338
    .local v3, "s":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    .end local v3    # "s":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 346
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    .line 348
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 349
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    .line 351
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 352
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    .line 354
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 355
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mResolveInfo:Ljava/util/List;

    .line 357
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 358
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mContext:Landroid/content/Context;

    .line 359
    iput-object v5, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mPackageManger:Landroid/content/pm/PackageManager;

    .line 360
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # setter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mProgressCircle:Landroid/widget/ProgressBar;
    invoke-static {v4, v5}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$102(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;

    .line 362
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImageViewByIndex(I)Landroid/widget/ImageView;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 420
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$200(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 421
    .local v0, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 422
    const v1, 0x7f090028

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 425
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItem(I)Landroid/content/ComponentName;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->getItem(I)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 399
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemIndex(Landroid/view/View;)I
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 403
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$200(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/GridView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/GridView;->getCount()I

    move-result v0

    .line 404
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 405
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->this$0:Lcom/sec/android/easyonehand/EOHShortcutAddWindow;

    # getter for: Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->mGridView:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->access$200(Lcom/sec/android/easyonehand/EOHShortcutAddWindow;)Landroid/widget/GridView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 406
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 407
    const v3, 0x7f090028

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 408
    sget-boolean v3, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 409
    const-string v3, "EOHShortcutAddWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", count="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", view="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    .end local v1    # "i":I
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    :goto_1
    return v1

    .line 411
    .restart local v1    # "i":I
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_1
    const v3, 0x7f090029

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 404
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 416
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 468
    if-nez p2, :cond_0

    .line 469
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mLayout:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 474
    :cond_0
    sget-boolean v2, Lcom/sec/android/easyonehand/EOHShortcutAddWindow;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 475
    const-string v2, "EOHShortcutAddWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getView() position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mComponentName:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_1
    const v2, 0x7f090028

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 479
    .local v0, "iconImage":Landroid/widget/ImageButton;
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v2, v3, :cond_2

    .line 480
    const v2, 0x7f090029

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 482
    .local v1, "iconLabel":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconLabelInfo:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mIconInfo:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 485
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 486
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHShortcutAddWindow$ShortcutAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 489
    .end local v1    # "iconLabel":Landroid/widget/TextView;
    :cond_2
    return-object p2
.end method
