.class Lcom/sec/android/easyonehand/EasyOneHandService$1;
.super Landroid/content/BroadcastReceiver;
.source "EasyOneHandService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/easyonehand/EasyOneHandService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/easyonehand/EasyOneHandService;


# direct methods
.method constructor <init>(Lcom/sec/android/easyonehand/EasyOneHandService;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v8, 0x2

    .line 387
    if-eqz p2, :cond_3

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 388
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "SCREEN_OFF() "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    # setter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3, v4, v5}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$802(Lcom/sec/android/easyonehand/EasyOneHandService;J)J

    .line 390
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mCallState:I
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$900()I

    move-result v3

    if-ne v3, v8, :cond_0

    .line 391
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # setter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3, v10, v11}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$802(Lcom/sec/android/easyonehand/EasyOneHandService;J)J

    .line 394
    :cond_0
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->DEBUG:Z
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$400()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 395
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTimeGapWhenLCDOFF="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v5}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$800(Lcom/sec/android/easyonehand/EasyOneHandService;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :cond_1
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 398
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 399
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 400
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v8, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 434
    :cond_2
    :goto_0
    return-void

    .line 402
    :cond_3
    if-eqz p2, :cond_2

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 403
    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$500()Ljava/lang/String;

    move-result-object v3

    const-string v4, "SCREEN_ON() "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 405
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 406
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$300(Lcom/sec/android/easyonehand/EasyOneHandService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 408
    :cond_4
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 409
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 411
    .local v0, "gap":J
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$800(Lcom/sec/android/easyonehand/EasyOneHandService;)J

    move-result-wide v4

    cmp-long v3, v4, v10

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$800(Lcom/sec/android/easyonehand/EasyOneHandService;)J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 412
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->isScreenLocked()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardHidden()Z

    move-result v3

    if-nez v3, :cond_5

    .line 413
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # invokes: Lcom/sec/android/easyonehand/EasyOneHandService;->closeAllWindowSafe(Z)V
    invoke-static {v3, v9}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$1000(Lcom/sec/android/easyonehand/EasyOneHandService;Z)V

    goto/16 :goto_0

    .line 417
    :cond_5
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowInfo;->updateDisplayMatrix()V

    .line 419
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v2

    .line 420
    .local v2, "isKeyguardLocked":Z
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWinInfo:Lcom/sec/android/easyonehand/EOHWindowInfo;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$700(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowInfo;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/easyonehand/EOHWindowInfo;->setScreenLocked(Z)V

    .line 421
    if-eqz v2, :cond_7

    .line 422
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$600(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHUtils;->isFingerprintLockScreenEnabled()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 423
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # invokes: Lcom/sec/android/easyonehand/EasyOneHandService;->closeAllWindowSafe(Z)V
    invoke-static {v3, v9}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$1000(Lcom/sec/android/easyonehand/EasyOneHandService;Z)V

    goto/16 :goto_0

    .line 427
    :cond_6
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 428
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # getter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;
    invoke-static {v3}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$200(Lcom/sec/android/easyonehand/EasyOneHandService;)Lcom/sec/android/easyonehand/EOHWindowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->hideSideWindow()V

    .line 432
    .end local v0    # "gap":J
    .end local v2    # "isKeyguardLocked":Z
    :cond_7
    iget-object v3, p0, Lcom/sec/android/easyonehand/EasyOneHandService$1;->this$0:Lcom/sec/android/easyonehand/EasyOneHandService;

    # setter for: Lcom/sec/android/easyonehand/EasyOneHandService;->mTimeGapWhenLCDOFF:J
    invoke-static {v3, v10, v11}, Lcom/sec/android/easyonehand/EasyOneHandService;->access$802(Lcom/sec/android/easyonehand/EasyOneHandService;J)J

    goto/16 :goto_0
.end method
