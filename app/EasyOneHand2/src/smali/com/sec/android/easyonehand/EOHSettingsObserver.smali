.class Lcom/sec/android/easyonehand/EOHSettingsObserver;
.super Landroid/database/ContentObserver;
.source "EOHSettingsObserver.java"


# static fields
.field static final DEBUG:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

.field private mRegistered:Z

.field private final mResolver:Landroid/content/ContentResolver;

.field private mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mRegistered:Z

    .line 47
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    .line 48
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    .line 49
    invoke-static {}, Lcom/sec/android/easyonehand/EOHUtils;->getInstance()Lcom/sec/android/easyonehand/EOHUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    .line 50
    return-void
.end method

.method private getSecureSettingValue(Ljava/lang/String;)I
    .locals 3
    .param p1, "settingValue"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, -0x2

    invoke-static {v0, p1, v1, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method private isCameraRunning()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method private isConflictProcessRunning()Z
    .locals 10

    .prologue
    .line 231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 233
    .local v1, "conflictListByClassName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "com.sec.android.sidesync.kms.sink.service.SideSyncServerService"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    const-string v6, "com.sec.android.sidesync.kms.source.service.SideSyncService"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    const-string v6, "DCM"

    const-string v7, "ro.csc.sales_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 236
    const-string v6, "com.rsupport.rs.service.SupportService"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 241
    .local v0, "am":Landroid/app/ActivityManager;
    const v6, 0x7fffffff

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 242
    .local v5, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v6, v5, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 243
    iget-object v6, v5, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    .line 244
    .local v4, "s":Ljava/lang/String;
    const-string v6, "EOHSettingsObserver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t start EasyOneHand due to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    const/4 v6, 0x1

    .line 252
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "s":Ljava/lang/String;
    .end local v5    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v6

    .line 249
    :catch_0
    move-exception v2

    .line 250
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 252
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private isDayDreamRunning()Z
    .locals 4

    .prologue
    .line 216
    const-string v2, "dreams"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/service/dreams/IDreamManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/dreams/IDreamManager;

    move-result-object v0

    .line 218
    .local v0, "dreamManager":Landroid/service/dreams/IDreamManager;
    const/4 v1, 0x0

    .line 220
    .local v1, "isDreaming":Z
    :try_start_0
    invoke-interface {v0}, Landroid/service/dreams/IDreamManager;->isDreaming()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    const/4 v1, 0x1

    .line 222
    const-string v2, "EOHSettingsObserver"

    const-string v3, "Daydream isDreaming"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :cond_0
    :goto_0
    return v1

    .line 224
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private isHoverZommEnabled()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 144
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accessibility_magnifier"

    const/4 v4, 0x0

    const/4 v5, -0x2

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 150
    :cond_0
    :goto_0
    return v1

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSettingsObserver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occured on isHoverZommEnabled() e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isKioskModeRunning()Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 256
    const/4 v8, 0x0

    .line 258
    .local v8, "kioskRunning":Z
    const-string v0, "content://com.sec.knox.provider2/KioskMode"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 259
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "content://com.sec.knox.provider2/KioskMode"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 260
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 262
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 263
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 264
    const-string v0, "content://com.sec.knox.provider2/KioskMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    const/4 v8, 0x1

    .line 271
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 275
    :cond_1
    :goto_0
    return v8

    .line 268
    :catch_0
    move-exception v7

    .line 269
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private isMagnifierRunning()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 164
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "FmMagnifier"

    const/4 v4, 0x0

    const/4 v5, -0x2

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 169
    :cond_0
    :goto_0
    return v1

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSettingsObserver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occured on isHoverZommEnabled() e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isShutdown()Z
    .locals 2

    .prologue
    .line 279
    const-string v1, "sys.shutdown.requested"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "shutdown":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    const/4 v1, 0x0

    .line 284
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isSideSyncRunning()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 155
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "sidesync_sink_connect"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "sidesync_source_connect"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    :cond_0
    const/4 v0, 0x1

    .line 159
    :cond_1
    return v0
.end method

.method private isTalkBackEnabled()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 128
    const/4 v1, 0x0

    .line 131
    .local v1, "isEnableTalkback":Z
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "accessibility_enabled"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "enabled_accessibility_services"

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "(?i).*marvin.talkback.TalkBackService"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    .line 138
    :goto_0
    return v1

    :cond_0
    move v1, v3

    .line 131
    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSettingsObserver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occured on isTalkBackEnabled() e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isUsbConnected()Z
    .locals 6

    .prologue
    .line 173
    const/4 v1, 0x0

    .line 174
    .local v1, "in":Ljava/io/BufferedReader;
    const-string v3, "Unknown"

    .line 176
    .local v3, "usb_status":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/android_usb/android0/state"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    .end local v1    # "in":Ljava/io/BufferedReader;
    .local v2, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 181
    if-eqz v2, :cond_3

    .line 183
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 190
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    const-string v4, "CONFIGURED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 191
    const/4 v4, 0x1

    .line 193
    :goto_1
    return v4

    .line 184
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 186
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_0

    .line 178
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 179
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    if-eqz v1, :cond_0

    .line 183
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 184
    :catch_2
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 181
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v1, :cond_1

    .line 183
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 186
    :cond_1
    :goto_4
    throw v4

    .line 184
    :catch_3
    move-exception v0

    .line 185
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 193
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 181
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_3

    .line 178
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_2

    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :cond_3
    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_0
.end method


# virtual methods
.method checkSetting()V
    .locals 6

    .prologue
    .line 90
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    if-eqz v3, :cond_0

    .line 91
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    invoke-virtual {v3}, Lcom/sec/android/easyonehand/EOHWindowController;->changeSettingsHanlder()V

    .line 94
    :cond_0
    const-string v3, "accessibility_display_magnification_enabled"

    invoke-direct {p0, v3}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->getSecureSettingValue(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isTalkBackEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isHoverZommEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isSideSyncRunning()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isKidsHomeMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 98
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    .line 99
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.sec.android.easyonehand"

    const-string v4, "com.sec.android.easyonehand.EasyOneHandService"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.action.EASYONEHAND_SERVICE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "ForceHide"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    .line 104
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v1

    .line 107
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "EOHSettingsObserver"

    const-string v4, "Exception checkSetting: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method getRunningSettingState()I
    .locals 6

    .prologue
    const/4 v2, 0x5

    const/4 v4, 0x4

    const/4 v0, 0x2

    const/4 v1, 0x3

    const/4 v3, 0x6

    .line 288
    const-string v5, "accessibility_display_magnification_enabled"

    invoke-direct {p0, v5}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->getSecureSettingValue(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 289
    iget-object v1, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v1, v0}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    .line 331
    :goto_0
    return v0

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isMagnifierRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    .line 293
    const/16 v0, 0xc

    goto :goto_0

    .line 294
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isTalkBackEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    move v0, v1

    .line 296
    goto :goto_0

    .line 297
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isHoverZommEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 298
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    move v0, v1

    .line 299
    goto :goto_0

    .line 300
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isSideSyncRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 301
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0, v2}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    move v0, v2

    .line 302
    goto :goto_0

    .line 303
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isCameraRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 304
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    .line 305
    const/16 v0, 0x8

    goto :goto_0

    .line 306
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isConflictProcessRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v3

    .line 308
    goto :goto_0

    .line 309
    :cond_6
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->DEBUG:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isUsbConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 310
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0, v4}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    move v0, v4

    .line 311
    goto :goto_0

    .line 312
    :cond_7
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->isCurrentUserOwner()Z

    move-result v0

    if-nez v0, :cond_8

    .line 314
    const/4 v0, 0x7

    goto :goto_0

    .line 315
    :cond_8
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    invoke-virtual {v0}, Lcom/sec/android/easyonehand/EOHUtils;->isFingerprintLockScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 316
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mEOHUtils:Lcom/sec/android/easyonehand/EOHUtils;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/easyonehand/EOHUtils;->showErrorToast(I)V

    .line 317
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 318
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isKidsHomeMode()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 319
    const/16 v0, 0xa

    goto/16 :goto_0

    .line 320
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isDayDreamRunning()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v3

    .line 321
    goto/16 :goto_0

    .line 322
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v3

    .line 323
    goto/16 :goto_0

    .line 326
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->isKioskModeRunning()Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v3

    .line 327
    goto/16 :goto_0

    .line 331
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public isKidsHomeMode()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 118
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "kids_home_mode"

    const/4 v4, 0x0

    const/4 v5, -0x2

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 124
    :cond_0
    :goto_0
    return v1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EOHSettingsObserver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occured on isKidsHomeMode() e="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onChange(Z)V
    .locals 0
    .param p1, "selfChange"    # Z

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/android/easyonehand/EOHSettingsObserver;->checkSetting()V

    .line 77
    return-void
.end method

.method public register()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 53
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mRegistered:Z

    if-nez v0, :cond_1

    .line 54
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "EOHSettingsObserver"

    const-string v1, "SettingsObserver register() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mRegistered:Z

    .line 58
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "accessibility_display_magnification_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "accessibility_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "sidesync_sink_connect"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "sidesync_source_connect"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "kids_home_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 69
    :cond_1
    return-void
.end method

.method public setWindowController(Lcom/sec/android/easyonehand/EOHWindowController;)V
    .locals 0
    .param p1, "controller"    # Lcom/sec/android/easyonehand/EOHWindowController;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mWindowController:Lcom/sec/android/easyonehand/EOHWindowController;

    .line 73
    return-void
.end method

.method public unregister()V
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mRegistered:Z

    if-eqz v0, :cond_1

    .line 81
    sget-boolean v0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "EOHSettingsObserver"

    const-string v1, "SettingsObserver ununun register() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mRegistered:Z

    .line 85
    iget-object v0, p0, Lcom/sec/android/easyonehand/EOHSettingsObserver;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 87
    :cond_1
    return-void
.end method
