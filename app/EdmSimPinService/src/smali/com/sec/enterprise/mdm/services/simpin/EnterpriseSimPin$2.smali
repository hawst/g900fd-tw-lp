.class Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;
.super Landroid/os/Handler;
.source "EnterpriseSimPin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 170
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 171
    .local v0, "ar":Landroid/os/AsyncResult;
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v2, Lcom/android/internal/telephony/CommandException;

    move-object v1, v2

    .line 173
    .local v1, "ce":Lcom/android/internal/telephony/CommandException;
    :goto_0
    iget v2, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    packed-switch v2, :pswitch_data_0

    .line 209
    :goto_1
    monitor-exit p0

    return-void

    .line 171
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 176
    .restart local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    :pswitch_0
    :try_start_1
    const-string v2, "EnterpriseSimPin"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lock/unlock SIM PIN operation result - ce = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    if-nez v1, :cond_1

    .line 178
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v3, 0x0

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$502(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    .line 186
    :goto_2
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$400(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 187
    :try_start_2
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v4, 0x0

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingLockResult:Z
    invoke-static {v2, v4}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$202(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;Z)Z

    .line 188
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$400(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 189
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 170
    .end local v0    # "ar":Landroid/os/AsyncResult;
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 179
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    .restart local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    :cond_1
    :try_start_4
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v2, v3, :cond_2

    .line 180
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v3, 0x1

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$502(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_2

    .line 181
    :cond_2
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v2, v3, :cond_3

    .line 182
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v3, 0x3

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$502(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_2

    .line 184
    :cond_3
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/16 v3, 0x64

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$502(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_2

    .line 192
    :pswitch_1
    const-string v2, "EnterpriseSimPin"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "change SIM PIN operation result - ce = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    if-nez v1, :cond_4

    .line 194
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v3, 0x0

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$802(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    .line 202
    :goto_3
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChange:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$700(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 203
    :try_start_5
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v4, 0x0

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingChangeResult:Z
    invoke-static {v2, v4}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$602(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;Z)Z

    .line 204
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChange:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$700(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 205
    monitor-exit v3

    goto/16 :goto_1

    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v2

    .line 195
    :cond_4
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v2, v3, :cond_5

    .line 196
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v3, 0x1

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$802(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_3

    .line 197
    :cond_5
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v2, v3, :cond_6

    .line 198
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/4 v3, 0x3

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$802(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_3

    .line 200
    :cond_6
    iget-object v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    const/16 v3, 0x64

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v2, v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$802(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
