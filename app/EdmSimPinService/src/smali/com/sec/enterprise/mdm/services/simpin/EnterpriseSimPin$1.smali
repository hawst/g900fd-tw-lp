.class Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;
.super Landroid/app/enterprise/ISimPinPolicy$Stub;
.source "EnterpriseSimPin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    invoke-direct {p0}, Landroid/app/enterprise/ISimPinPolicy$Stub;-><init>()V

    return-void
.end method

.method private enforceUser()V
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 161
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Can only be called by system or phone user"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    return-void
.end method


# virtual methods
.method public changeSimPinCode(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "currentPinCode"    # Ljava/lang/String;
    .param p2, "newPinCode"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v8, -0x1

    .line 116
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->enforceUser()V

    .line 120
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/IccCard;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v3, v4, :cond_0

    .line 121
    const/4 v2, 0x6

    .line 150
    :goto_0
    return v2

    .line 125
    :cond_0
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/IccCard;->getIccLockEnabled()Z

    move-result v1

    .line 126
    .local v1, "isLocked":Z
    if-nez v1, :cond_1

    .line 127
    const/4 v2, 0x5

    goto :goto_0

    .line 131
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # invokes: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->getSimLockInfoResult()I
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$100(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I

    move-result v3

    if-gt v3, v5, :cond_2

    .line 132
    const/16 v2, 0x8

    goto :goto_0

    .line 136
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingChangeResult:Z
    invoke-static {v3, v5}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$602(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;Z)Z

    .line 137
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mCallbackHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$300(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x65

    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v3, p1, p2, v4}, Lcom/android/internal/telephony/IccCard;->changeIccLockPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 139
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChange:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$700(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 141
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChange:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$700(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v3

    const-wide/16 v6, 0x2710

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :goto_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$800(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I

    move-result v3

    if-eq v3, v8, :cond_3

    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$800(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I

    move-result v2

    .line 149
    .local v2, "ret":I
    :goto_2
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I
    invoke-static {v3, v8}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$802(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_0

    .line 142
    .end local v2    # "ret":I
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v3, "EnterpriseSimPin"

    const-string v5, "Exception in mChange.wait"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 145
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 148
    :cond_3
    const/16 v2, 0x64

    goto :goto_2
.end method

.method public isSimLocked()Z
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->enforceUser()V

    .line 155
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/IccCard;->getIccLockEnabled()Z

    move-result v0

    return v0
.end method

.method public setIccLockEnabled(ZLjava/lang/String;)I
    .locals 9
    .param p1, "lock"    # Z
    .param p2, "pinCode"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x64

    const/4 v5, 0x1

    const/4 v8, -0x1

    .line 73
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->enforceUser()V

    .line 77
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/IccCard;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v3, v4, :cond_0

    .line 78
    const/4 v2, 0x6

    .line 109
    :goto_0
    return v2

    .line 82
    :cond_0
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/IccCard;->getIccLockEnabled()Z

    move-result v1

    .line 83
    .local v1, "isLocked":Z
    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 84
    const/4 v2, 0x4

    goto :goto_0

    .line 85
    :cond_1
    if-nez v1, :cond_2

    if-nez p1, :cond_2

    .line 86
    const/4 v2, 0x5

    goto :goto_0

    .line 90
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # invokes: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->getSimLockInfoResult()I
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$100(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I

    move-result v3

    if-gt v3, v5, :cond_3

    .line 91
    const/16 v2, 0x8

    goto :goto_0

    .line 95
    :cond_3
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingLockResult:Z
    invoke-static {v3, v5}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$202(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;Z)Z

    .line 96
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mCallbackHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$300(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Landroid/os/Handler;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v3, p1, p2, v4}, Lcom/android/internal/telephony/IccCard;->setIccLockEnabled(ZLjava/lang/String;Landroid/os/Message;)V

    .line 98
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$400(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 100
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$400(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;

    move-result-object v3

    const-wide/16 v6, 0x2710

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :goto_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$500(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I

    move-result v3

    if-eq v3, v8, :cond_4

    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # getter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v3}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$500(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I

    move-result v2

    .line 108
    .local v2, "ret":I
    :cond_4
    iget-object v3, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;->this$0:Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    # setter for: Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I
    invoke-static {v3, v8}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->access$502(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I

    goto :goto_0

    .line 101
    .end local v2    # "ret":I
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v3, "EnterpriseSimPin"

    const-string v5, "Exception in mLock.wait"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 104
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method
