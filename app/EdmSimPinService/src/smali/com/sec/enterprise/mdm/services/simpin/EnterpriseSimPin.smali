.class public Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
.super Landroid/app/Service;
.source "EnterpriseSimPin.java"


# instance fields
.field private isWaitingChangeResult:Z

.field private isWaitingLockResult:Z

.field private final mBinder:Landroid/app/enterprise/ISimPinPolicy$Stub;

.field private mCallbackHandler:Landroid/os/Handler;

.field private mChange:Ljava/lang/Object;

.field private mChangeResult:I

.field private mLock:Ljava/lang/Object;

.field private mLockResult:I

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 46
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 51
    iput-boolean v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingLockResult:Z

    .line 52
    iput v1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLock:Ljava/lang/Object;

    .line 56
    iput-boolean v2, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingChangeResult:Z

    .line 57
    iput v1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChange:Ljava/lang/Object;

    .line 67
    new-instance v0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$1;-><init>(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mBinder:Landroid/app/enterprise/ISimPinPolicy$Stub;

    .line 166
    new-instance v0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin$2;-><init>(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)V

    iput-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mCallbackHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->getSimLockInfoResult()I

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingLockResult:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mCallbackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mLockResult:I

    return p1
.end method

.method static synthetic access$602(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->isWaitingChangeResult:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChange:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mChangeResult:I

    return p1
.end method

.method private getSimLockInfoResult()I
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/IccCard;->getIccPin1RetryCount()I

    move-result v0

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mBinder:Landroid/app/enterprise/ISimPinPolicy$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 220
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 221
    const-string v1, "EnterpriseSimPin"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :try_start_0
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->mPhone:Lcom/android/internal/telephony/Phone;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_0
    return-void

    .line 224
    :catch_0
    move-exception v0

    .line 225
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "EnterpriseSimPin"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Will stopSelf() because of this: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    invoke-virtual {p0}, Lcom/sec/enterprise/mdm/services/simpin/EnterpriseSimPin;->stopSelf()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 231
    const-string v0, "EnterpriseSimPin"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 233
    return-void
.end method
