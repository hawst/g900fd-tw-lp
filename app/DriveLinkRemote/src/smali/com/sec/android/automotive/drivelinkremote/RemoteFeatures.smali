.class public Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;
.super Ljava/lang/Object;
.source "RemoteFeatures.java"


# static fields
.field private static CARMODE_FEATURES:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static FEATURE_LAUNCHER_FOREGROUND_CHECK:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    const-string v0, "FEATURE_LAUNCHER_FOREGROUND_CHECK"

    sput-object v0, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->FEATURE_LAUNCHER_FOREGROUND_CHECK:Ljava/lang/String;

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->CARMODE_FEATURES:Ljava/util/HashMap;

    .line 12
    sget-object v0, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->CARMODE_FEATURES:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->FEATURE_LAUNCHER_FOREGROUND_CHECK:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isFeatureEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p0, "feature"    # Ljava/lang/String;

    .prologue
    .line 16
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 17
    .local v0, "isEnabled":Ljava/lang/Boolean;
    if-eqz p0, :cond_0

    sget-object v1, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->CARMODE_FEATURES:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->CARMODE_FEATURES:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18
    sget-object v1, Lcom/sec/android/automotive/drivelinkremote/RemoteFeatures;->CARMODE_FEATURES:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "isEnabled":Ljava/lang/Boolean;
    check-cast v0, Ljava/lang/Boolean;

    .line 20
    .restart local v0    # "isEnabled":Ljava/lang/Boolean;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1
.end method
