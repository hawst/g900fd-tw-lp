.class Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;
.super Landroid/app/IProcessObserver$Stub;
.source "ForgroundApplicationChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->registerObserver(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    .line 62
    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    return-object v0
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foregroundActivities"    # Z

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;

    invoke-direct {v1, p0, p1, p3}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;-><init>(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 79
    return-void
.end method

.method public onProcessDied(II)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .prologue
    .line 83
    return-void
.end method

.method public onProcessStateChanged(III)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 89
    return-void
.end method
