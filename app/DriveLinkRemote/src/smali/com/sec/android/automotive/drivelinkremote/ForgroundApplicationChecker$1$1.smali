.class Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;
.super Ljava/lang/Object;
.source "ForgroundApplicationChecker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->onForegroundActivitiesChanged(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

.field private final synthetic val$foregroundActivities:Z

.field private final synthetic val$pid:I


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;IZ)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->this$1:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

    iput p2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->val$pid:I

    iput-boolean p3, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->val$foregroundActivities:Z

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->this$1:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->this$1:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
    invoke-static {v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v1

    .line 72
    iget v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->val$pid:I

    iget-boolean v3, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->val$foregroundActivities:Z

    .line 71
    # invokes: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getPackageNameFromPID(IZ)Ljava/lang/String;
    invoke-static {v1, v2, v3}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->access$1(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;IZ)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isLauncherPackageName(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->access$2(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;Ljava/lang/String;)Z

    move-result v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->this$1:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->access$3(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1$1;->this$1:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;->access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    # getter for: Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;
    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->access$3(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    move-result-object v0

    .line 75
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;->launcherAppForgroundEvent(Z)V

    .line 77
    :cond_0
    return-void
.end method
