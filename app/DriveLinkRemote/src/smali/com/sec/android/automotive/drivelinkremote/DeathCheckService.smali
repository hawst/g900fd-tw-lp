.class public Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;
.super Landroid/app/Service;
.source "DeathCheckService.java"

# interfaces
.implements Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;
    }
.end annotation


# static fields
.field private static final DB_CAR_MODE_ON:Ljava/lang/String; = "car_mode_on"

.field private static final TAG:Ljava/lang/String; = "[DeathCheckService]"


# instance fields
.field private final mBinder:Lcom/sec/android/automotive/drivelinkremote/IDeathCheckService$Stub;

.field private mCarModeObserver:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 203
    new-instance v0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;-><init>(Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->mBinder:Lcom/sec/android/automotive/drivelinkremote/IDeathCheckService$Stub;

    .line 15
    return-void
.end method


# virtual methods
.method public finishProcess()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 128
    const-string v2, "[DeathCheckService]"

    const-string v3, "DeathCheckService will be close "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "car_mode_on"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "car_mode_on"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_setting"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v6, :cond_1

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 146
    :cond_1
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v6, :cond_3

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 157
    :goto_2
    :try_start_3
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "car_mode_blocking_system_key"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "car_mode_blocking_system_key"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 159
    const-string v2, "[DeathCheckService]"

    const-string v3, "car_mode_blocking_system_key changed to 0 by deathcheck"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 167
    :cond_2
    :goto_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 168
    .local v1, "mintent":Landroid/content/Intent;
    const-string v2, "com.sec.android.automotive.drivelink.UNREGISTER_NOTI"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    const-string v2, "com.sec.android.automotive.drivelink"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    invoke-virtual {p0, v1}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->sendBroadcast(Landroid/content/Intent;)V

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->stopSelf()V

    .line 173
    return-void

    .line 134
    .end local v1    # "mintent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 141
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_1
    move-exception v0

    .line 142
    .restart local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 149
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_3
    :try_start_4
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 151
    :catch_2
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_multi_setting"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 153
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 161
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 162
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "[DeathCheckService]"

    const-string v3, "key is not defined. force set system key block to zero."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "car_mode_blocking_system_key"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 164
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_3
.end method

.method public launcherAppForgroundEvent(Z)V
    .locals 0
    .param p1, "isLauncherAppForground"    # Z

    .prologue
    .line 264
    if-eqz p1, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->finishProcess()V

    .line 266
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->mBinder:Lcom/sec/android/automotive/drivelinkremote/IDeathCheckService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 31
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver()Z

    move-result v1

    if-nez v1, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->registerObserver(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;)V

    .line 35
    :cond_0
    const-string v1, "[DeathCheckService]"

    const-string v2, "Service is created"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    new-instance v1, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;

    invoke-direct {v1, p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;-><init>(Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;)V

    iput-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->mCarModeObserver:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "car_mode_on"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 39
    iget-object v3, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->mCarModeObserver:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;

    .line 38
    invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 42
    :try_start_0
    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Settings car_mode_on="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "car_mode_on"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "car_mode_on"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 47
    const-string v2, "car_mode_blocking_system_key"

    const/4 v3, 0x0

    .line 45
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e1":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 190
    const-string v0, "[DeathCheckService]"

    const-string v1, "Service-destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->mCarModeObserver:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->unRegisterObserver()V

    .line 194
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 195
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 179
    const-string v0, "[DeathCheckService]"

    const-string v1, "Service-started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->registerObserver(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;)V

    .line 183
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
