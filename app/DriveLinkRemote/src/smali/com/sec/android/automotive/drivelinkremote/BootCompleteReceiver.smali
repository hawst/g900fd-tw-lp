.class public Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootCompleteReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[BootCompleteReceiver]"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 15
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "action":Ljava/lang/String;
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    .line 18
    if-nez v0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    const-string v2, "[BootCompleteReceiver]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onReceive action:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 26
    const-string v3, "car_mode_blocking_system_key"

    .line 25
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 27
    const-string v2, "[BootCompleteReceiver]"

    const-string v3, "!!!!!unblock system key!!!!!!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 30
    const-string v3, "car_mode_blocking_system_key"

    const/4 v4, 0x0

    .line 29
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 42
    :cond_2
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 43
    const-string v3, "car_mode_on"

    .line 42
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v6, :cond_3

    .line 44
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 45
    const-string v3, "car_mode_on"

    const/4 v4, 0x0

    .line 44
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 52
    :cond_3
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 53
    const-string v3, "drive_link_setting"

    .line 52
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v6, :cond_4

    .line 54
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 55
    const-string v3, "drive_link_setting"

    const/4 v4, 0x0

    .line 54
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 62
    :cond_4
    :goto_3
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 63
    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    .line 62
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 64
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 65
    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    .line 64
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 32
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 33
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "[BootCompleteReceiver]"

    .line 34
    const-string v3, "key does not exist -> add key & !!!!!unblock system key!!!!!!!!!"

    .line 33
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/BootCompleteReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 36
    const-string v3, "car_mode_blocking_system_key"

    .line 35
    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 38
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 47
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_2
    move-exception v1

    .line 48
    .restart local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 57
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_3
    move-exception v1

    .line 58
    .restart local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_3
.end method
