.class public Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
.super Ljava/lang/Object;
.source "ForgroundApplicationChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "ForgroundApplicationChecker"

.field private static volatile forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;


# instance fields
.field public am:Landroid/app/IActivityManager;

.field private isRegisterObserver:Z

.field private launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mProcessObserver:Landroid/app/IProcessObserver;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mContext:Landroid/content/Context;

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mHandler:Landroid/os/Handler;

    .line 23
    iput-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 31
    iput-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver:Z

    .line 36
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mContext:Landroid/content/Context;

    .line 37
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    .line 38
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;IZ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0, p1, p2}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getPackageNameFromPID(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isLauncherPackageName(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    if-nez v0, :cond_1

    .line 42
    const-class v1, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    monitor-enter v1

    .line 43
    :try_start_0
    sget-object v0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    invoke-direct {v0, p0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    .line 42
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_1
    sget-object v0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    return-object v0

    .line 42
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getPackageNameFromPID(IZ)Ljava/lang/String;
    .locals 7
    .param p1, "pid"    # I
    .param p2, "foregroundActivities"    # Z

    .prologue
    .line 177
    const/4 v2, 0x0

    .line 179
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    invoke-interface {v4}, Landroid/app/IActivityManager;->getRunningAppProcesses()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 184
    :goto_0
    if-eqz v2, :cond_0

    .line 185
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 195
    .end local v1    # "i":I
    :cond_0
    const/4 v4, 0x0

    :goto_2
    return-object v4

    .line 180
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 187
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "i":I
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 188
    .local v3, "ri":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne p1, v4, :cond_2

    if-eqz p2, :cond_2

    .line 189
    const-string v4, "ForgroundApplicationChecker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JINSEIL forgroundAppPkgName:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    goto :goto_2

    .line 185
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private isLauncherPackageName(Ljava/lang/String;)Z
    .locals 11
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/high16 v10, 0x10000

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 137
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 138
    .local v0, "homeIntent":Landroid/content/Intent;
    const-string v7, "android.intent.action.MAIN"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v7, "android.intent.category.HOME"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    iget-object v7, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 142
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v0, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 145
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-ne v7, v8, :cond_1

    .line 146
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 147
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 148
    const-string v7, "ForgroundApplicationChecker"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Launcher pkg name:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v8

    .line 172
    :goto_0
    return v7

    :cond_0
    move v7, v9

    .line 151
    goto :goto_0

    .line 153
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_1
    invoke-virtual {v3, v0, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 155
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    .line 156
    .local v6, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v6, :cond_2

    .line 167
    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 168
    .restart local v5    # "ri":Landroid/content/pm/ResolveInfo;
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 169
    const-string v7, "ForgroundApplicationChecker"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Launcher pkg name:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v8

    .line 170
    goto :goto_0

    .line 157
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 158
    iget-object v10, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 159
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 160
    .restart local v5    # "ri":Landroid/content/pm/ResolveInfo;
    iget-object v7, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 161
    const-string v7, "ForgroundApplicationChecker"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Launcher pkg name:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v8

    .line 162
    goto :goto_0

    :cond_3
    move v7, v9

    .line 164
    goto :goto_0

    .line 156
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .restart local v5    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_5
    move v7, v9

    .line 172
    goto :goto_0
.end method


# virtual methods
.method public isRegisterObserver()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver:Z

    return v0
.end method

.method public registerObserver(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;)V
    .locals 5
    .param p1, "eventListener"    # Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    .prologue
    .line 57
    iget-boolean v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver:Z

    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 58
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver:Z

    .line 60
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    .line 62
    new-instance v2, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;

    invoke-direct {v2, p0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$1;-><init>(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;)V

    iput-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 91
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    .line 92
    const-string v2, "ForgroundApplicationChecker"

    const-string v3, "JINSEIL registerProcessObserver"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "ForgroundApplicationChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JINSEIL failed to registerProcessObserver, e = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 96
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ForgroundApplicationChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JINSEIL failed to registerProcessObserver, e = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 98
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 97
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    .line 101
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    iget-object v3, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    .line 102
    const-string v2, "ForgroundApplicationChecker"

    const-string v3, "JINSEIL registerProcessObserver"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 103
    :catch_2
    move-exception v1

    .line 105
    .local v1, "e1":Landroid/os/RemoteException;
    const-string v2, "ForgroundApplicationChecker"

    .line 106
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JINSEIL failed to registerProcessObserver, e1 = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 106
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unRegisterObserver()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 114
    iget-boolean v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver:Z

    if-eqz v1, :cond_2

    .line 115
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver:Z

    .line 116
    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    if-eqz v1, :cond_0

    .line 117
    iput-object v4, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->launcherAppForgroundEventListener:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    if-eqz v1, :cond_1

    .line 121
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->am:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V

    .line 122
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 123
    const-string v1, "ForgroundApplicationChecker"

    const-string v2, "JINSEIL unregisterProcessObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :cond_1
    :goto_0
    sget-object v1, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    if-eqz v1, :cond_2

    .line 132
    sput-object v4, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->forgroundApplicationChecker:Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    .line 134
    :cond_2
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ForgroundApplicationChecker"

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JINSEIL failed to unregisterProcessObserver, e = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 126
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
