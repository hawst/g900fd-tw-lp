.class Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;
.super Lcom/sec/android/automotive/drivelinkremote/IDeathCheckService$Stub;
.source "DeathCheckService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;


# direct methods
.method constructor <init>(Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    .line 203
    invoke-direct {p0}, Lcom/sec/android/automotive/drivelinkremote/IDeathCheckService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public start()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 207
    const-string v0, "[DeathCheckService]"

    const-string v1, "IDeathCheckService.Stub start()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->isRegisterObserver()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v0}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$1;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v0, v1}, Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker;->registerObserver(Lcom/sec/android/automotive/drivelinkremote/ForgroundApplicationChecker$LauncherAppForgroundEventListener;)V

    .line 211
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 217
    const-string v0, "[DeathCheckService]"

    const-string v1, "IDeathCheckService.Stub stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    return-void
.end method
