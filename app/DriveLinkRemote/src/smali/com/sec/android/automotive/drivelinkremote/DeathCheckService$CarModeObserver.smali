.class public Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;
.super Landroid/database/ContentObserver;
.source "DeathCheckService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CarModeObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;


# direct methods
.method public constructor <init>(Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;)V
    .locals 1

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    .line 226
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 228
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v5, 0x1

    .line 232
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 234
    const-string v2, "[DeathCheckService]"

    const-string v3, "CarModeObserver-onChange"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_setting"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 238
    const-string v2, "[DeathCheckService]"

    const-string v3, "CarModeObserver-onChange : drive_link_setting off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 243
    const-string v2, "[DeathCheckService]"

    const-string v3, "CarModeObserver-onChange : drive_link_multi_setting off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "drive_link_multi_setting"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :cond_1
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 252
    .local v1, "mintent":Landroid/content/Intent;
    const-string v2, "com.sec.android.automotive.drivelink.CAR_MODE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    const-string v2, "com.sec.android.automotive.drivelink"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v2, v1}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->sendBroadcast(Landroid/content/Intent;)V

    .line 256
    iget-object v2, p0, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService$CarModeObserver;->this$0:Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;

    invoke-virtual {v2}, Lcom/sec/android/automotive/drivelinkremote/DeathCheckService;->stopSelf()V

    .line 257
    return-void

    .line 247
    .end local v1    # "mintent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
