.class public final Lcom/sec/android/app/sysscope/engine/m;
.super Lcom/sec/android/app/sysscope/engine/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/sysscope/engine/g;-><init>()V

    return-void
.end method

.method private d()Landroid/os/Bundle;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/app/sysscope/engine/m;->b:Lcom/sec/android/app/sysscope/engine/d;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/sec/android/app/sysscope/service/f;->a:Lcom/sec/android/app/sysscope/service/f;

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sysscope/engine/m;->b:Lcom/sec/android/app/sysscope/engine/d;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/sec/android/app/sysscope/engine/d;->a(Landroid/os/Bundle;)Lcom/sec/android/app/sysscope/service/f;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sysscope/service/f;->a:Lcom/sec/android/app/sysscope/service/f;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/sysscope/engine/m;->b:Lcom/sec/android/app/sysscope/engine/d;

    invoke-interface {v1}, Lcom/sec/android/app/sysscope/engine/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v3, v4}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sysscope/engine/m;->a(Landroid/os/Bundle;)V

    const-string v3, "result"

    invoke-virtual {v0}, Lcom/sec/android/app/sysscope/service/f;->a()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "info"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/sysscope/engine/m;->d()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
