// Copyright 2012 Google Inc. All Rights Reserved.

// Fragment shader for a flat page with one texture

precision mediump float;

uniform sampler2D uTexture1;
uniform float uOpacity;

varying vec2 vTexture1Coord;

void main() {
    gl_FragColor = texture2D(uTexture1, vTexture1Coord) * uOpacity;
}
