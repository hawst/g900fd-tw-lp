// Copyright 2012 Google Inc. All Rights Reserved.

// Fragment shader for a flat page with one texture

precision mediump float;

uniform vec3 uColorBg;
uniform sampler2D uTexture1;
uniform float uOpacity;

varying vec2 vTexture1Coord;

void main() {
    vec4 col2 = texture2D(uTexture1, vTexture1Coord);
    gl_FragColor.rgb = mix(uColorBg, col2.rgb, uOpacity * col2.a);
    gl_FragColor.a = 1.0;
}
