.class public final Lcom/google/uploader/client/ClientProto$TransferHandle;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ClientProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/ClientProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransferHandle"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/uploader/client/ClientProto$TransferHandle;


# instance fields
.field private hasMetadata:Z

.field private hasScottyUploadId:Z

.field private hasUploadUrl:Z

.field private memoizedSerializedSize:I

.field private metadata_:Ljava/lang/String;

.field private scottyUploadId_:Ljava/lang/String;

.field private uploadUrl_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 342
    new-instance v0, Lcom/google/uploader/client/ClientProto$TransferHandle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle;-><init>(Z)V

    sput-object v0, Lcom/google/uploader/client/ClientProto$TransferHandle;->defaultInstance:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 343
    invoke-static {}, Lcom/google/uploader/client/ClientProto;->internalForceInit()V

    .line 344
    sget-object v0, Lcom/google/uploader/client/ClientProto$TransferHandle;->defaultInstance:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-direct {v0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->initFields()V

    .line 345
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->scottyUploadId_:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->uploadUrl_:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->metadata_:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/uploader/client/ClientProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/uploader/client/ClientProto$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->scottyUploadId_:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->uploadUrl_:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->metadata_:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lcom/google/uploader/client/ClientProto$TransferHandle;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasScottyUploadId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->scottyUploadId_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/uploader/client/ClientProto$TransferHandle;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasUploadUrl:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->uploadUrl_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/uploader/client/ClientProto$TransferHandle;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasMetadata:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->metadata_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/uploader/client/ClientProto$TransferHandle;->defaultInstance:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public static newBuilder()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 1

    .prologue
    .line 158
    # invokes: Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->create()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->access$100()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 161
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->newBuilder()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->mergeFrom(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getMetadata()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->metadata_:Ljava/lang/String;

    return-object v0
.end method

.method public getScottyUploadId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->scottyUploadId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 71
    iget v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->memoizedSerializedSize:I

    .line 72
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 88
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 74
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasScottyUploadId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getScottyUploadId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasUploadUrl()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getUploadUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_2
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasMetadata()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 84
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getMetadata()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    :cond_3
    iput v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->memoizedSerializedSize:I

    move v1, v0

    .line 88
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getUploadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->uploadUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public hasMetadata()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasMetadata:Z

    return v0
.end method

.method public hasScottyUploadId()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasScottyUploadId:Z

    return v0
.end method

.method public hasUploadUrl()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasUploadUrl:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public toBuilder()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 1

    .prologue
    .line 163
    invoke-static {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->newBuilder(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getSerializedSize()I

    .line 58
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasScottyUploadId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getScottyUploadId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasUploadUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getUploadUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasMetadata()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getMetadata()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 67
    :cond_2
    return-void
.end method
