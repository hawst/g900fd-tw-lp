.class public Lcom/google/uploader/client/UploadSettings$Builder;
.super Ljava/lang/Object;
.source "UploadSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/UploadSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private headers:Lcom/google/api/client/http/HttpHeaders;

.field private metadata:Ljava/lang/String;

.field private uploadUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/uploader/client/UploadSettings$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/UploadSettings$Builder;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/uploader/client/UploadSettings$Builder;->uploadUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/uploader/client/UploadSettings$Builder;)Lcom/google/api/client/http/HttpHeaders;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/UploadSettings$Builder;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/uploader/client/UploadSettings$Builder;->headers:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/uploader/client/UploadSettings$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/UploadSettings$Builder;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/uploader/client/UploadSettings$Builder;->metadata:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build(Ljava/lang/String;)Lcom/google/uploader/client/UploadSettings;
    .locals 2
    .param p1, "uploadUrl"    # Ljava/lang/String;

    .prologue
    .line 53
    const-string v0, "uploadUrl"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/uploader/client/UploadSettings$Builder;->uploadUrl:Ljava/lang/String;

    .line 54
    new-instance v0, Lcom/google/uploader/client/UploadSettings;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/uploader/client/UploadSettings;-><init>(Lcom/google/uploader/client/UploadSettings$Builder;Lcom/google/uploader/client/UploadSettings$1;)V

    return-object v0
.end method

.method public setHeaders(Lcom/google/api/client/http/HttpHeaders;)Lcom/google/uploader/client/UploadSettings$Builder;
    .locals 0
    .param p1, "headers"    # Lcom/google/api/client/http/HttpHeaders;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/uploader/client/UploadSettings$Builder;->headers:Lcom/google/api/client/http/HttpHeaders;

    .line 35
    return-object p0
.end method

.method public setMetadata(Ljava/lang/String;)Lcom/google/uploader/client/UploadSettings$Builder;
    .locals 0
    .param p1, "metadata"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/uploader/client/UploadSettings$Builder;->metadata:Ljava/lang/String;

    .line 44
    return-object p0
.end method
