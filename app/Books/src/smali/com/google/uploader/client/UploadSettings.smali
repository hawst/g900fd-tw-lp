.class public Lcom/google/uploader/client/UploadSettings;
.super Ljava/lang/Object;
.source "UploadSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/UploadSettings$1;,
        Lcom/google/uploader/client/UploadSettings$Builder;
    }
.end annotation


# instance fields
.field private headers:Lcom/google/api/client/http/HttpHeaders;

.field private metadata:Ljava/lang/String;

.field private uploadUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/uploader/client/UploadSettings$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/google/uploader/client/UploadSettings$Builder;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    # getter for: Lcom/google/uploader/client/UploadSettings$Builder;->uploadUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/uploader/client/UploadSettings$Builder;->access$100(Lcom/google/uploader/client/UploadSettings$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/uploader/client/UploadSettings;->uploadUrl:Ljava/lang/String;

    .line 64
    # getter for: Lcom/google/uploader/client/UploadSettings$Builder;->headers:Lcom/google/api/client/http/HttpHeaders;
    invoke-static {p1}, Lcom/google/uploader/client/UploadSettings$Builder;->access$200(Lcom/google/uploader/client/UploadSettings$Builder;)Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    iput-object v0, p0, Lcom/google/uploader/client/UploadSettings;->headers:Lcom/google/api/client/http/HttpHeaders;

    .line 65
    # getter for: Lcom/google/uploader/client/UploadSettings$Builder;->metadata:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/uploader/client/UploadSettings$Builder;->access$300(Lcom/google/uploader/client/UploadSettings$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/uploader/client/UploadSettings;->metadata:Ljava/lang/String;

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/uploader/client/UploadSettings$Builder;Lcom/google/uploader/client/UploadSettings$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/uploader/client/UploadSettings$Builder;
    .param p2, "x1"    # Lcom/google/uploader/client/UploadSettings$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/uploader/client/UploadSettings;-><init>(Lcom/google/uploader/client/UploadSettings$Builder;)V

    return-void
.end method


# virtual methods
.method public getHeaders()Lcom/google/api/client/http/HttpHeaders;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/uploader/client/UploadSettings;->headers:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method public getMetadata()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/uploader/client/UploadSettings;->metadata:Ljava/lang/String;

    return-object v0
.end method

.method public getUploadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/uploader/client/UploadSettings;->uploadUrl:Ljava/lang/String;

    return-object v0
.end method
