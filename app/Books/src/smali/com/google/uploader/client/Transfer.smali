.class public interface abstract Lcom/google/uploader/client/Transfer;
.super Ljava/lang/Object;
.source "Transfer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/Transfer$Status;
    }
.end annotation


# virtual methods
.method public abstract cancel()Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;
.end method

.method public abstract pause()Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract run()Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method
