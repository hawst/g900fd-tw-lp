.class public final Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "ClientProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/ClientProto$TransferHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/uploader/client/ClientProto$TransferHandle;",
        "Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/uploader/client/ClientProto$TransferHandle;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->create()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    invoke-direct {v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;-><init>()V

    .line 175
    .local v0, "builder":Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    new-instance v1, Lcom/google/uploader/client/ClientProto$TransferHandle;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/uploader/client/ClientProto$TransferHandle;-><init>(Lcom/google/uploader/client/ClientProto$1;)V

    iput-object v1, v0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 176
    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-static {v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->buildPartial()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 3

    .prologue
    .line 220
    iget-object v1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    if-nez v1, :cond_0

    .line 221
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 225
    .local v0, "returnMe":Lcom/google/uploader/client/ClientProto$TransferHandle;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 226
    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->clone()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->clone()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->create()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-virtual {v0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->mergeFrom(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->clone()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-virtual {v0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0, p1, p2}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0, p1, p2}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 249
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 253
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 254
    :sswitch_0
    return-object p0

    .line 259
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setScottyUploadId(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    goto :goto_0

    .line 263
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setUploadUrl(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    goto :goto_0

    .line 267
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setMetadata(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    goto :goto_0

    .line 249
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 230
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getDefaultInstance()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-object p0

    .line 231
    :cond_1
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasScottyUploadId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getScottyUploadId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setScottyUploadId(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    .line 234
    :cond_2
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasUploadUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getUploadUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setUploadUrl(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    .line 237
    :cond_3
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->hasMetadata()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getMetadata()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setMetadata(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    goto :goto_0
.end method

.method public setMetadata(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 325
    if-nez p1, :cond_0

    .line 326
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    const/4 v1, 0x1

    # setter for: Lcom/google/uploader/client/ClientProto$TransferHandle;->hasMetadata:Z
    invoke-static {v0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->access$702(Lcom/google/uploader/client/ClientProto$TransferHandle;Z)Z

    .line 329
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    # setter for: Lcom/google/uploader/client/ClientProto$TransferHandle;->metadata_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->access$802(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/lang/String;)Ljava/lang/String;

    .line 330
    return-object p0
.end method

.method public setScottyUploadId(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 283
    if-nez p1, :cond_0

    .line 284
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    const/4 v1, 0x1

    # setter for: Lcom/google/uploader/client/ClientProto$TransferHandle;->hasScottyUploadId:Z
    invoke-static {v0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->access$302(Lcom/google/uploader/client/ClientProto$TransferHandle;Z)Z

    .line 287
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    # setter for: Lcom/google/uploader/client/ClientProto$TransferHandle;->scottyUploadId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->access$402(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/lang/String;)Ljava/lang/String;

    .line 288
    return-object p0
.end method

.method public setUploadUrl(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 304
    if-nez p1, :cond_0

    .line 305
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    const/4 v1, 0x1

    # setter for: Lcom/google/uploader/client/ClientProto$TransferHandle;->hasUploadUrl:Z
    invoke-static {v0, v1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->access$502(Lcom/google/uploader/client/ClientProto$TransferHandle;Z)Z

    .line 308
    iget-object v0, p0, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->result:Lcom/google/uploader/client/ClientProto$TransferHandle;

    # setter for: Lcom/google/uploader/client/ClientProto$TransferHandle;->uploadUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->access$602(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/lang/String;)Ljava/lang/String;

    .line 309
    return-object p0
.end method
