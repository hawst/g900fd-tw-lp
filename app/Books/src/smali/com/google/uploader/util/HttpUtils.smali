.class public Lcom/google/uploader/util/HttpUtils;
.super Ljava/lang/Object;
.source "HttpUtils.java"


# direct methods
.method public static getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/api/client/http/HttpHeaders;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 12
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 13
    const/4 v2, 0x0

    .line 21
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 15
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 16
    .local v1, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    instance-of v2, v0, Ljava/lang/Iterable;

    if-eqz v2, :cond_1

    .line 17
    check-cast v0, Ljava/lang/Iterable;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 18
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_2

    .line 19
    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/uploader/util/HttpUtils;->toStringValue(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 21
    :cond_2
    invoke-static {v0}, Lcom/google/uploader/util/HttpUtils;->toStringValue(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static toStringValue(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "headerValue"    # Ljava/lang/Object;

    .prologue
    .line 25
    instance-of v0, p0, Ljava/lang/Enum;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/Enum;

    .end local p0    # "headerValue":Ljava/lang/Object;
    invoke-static {p0}, Lcom/google/api/client/util/FieldInfo;->of(Ljava/lang/Enum;)Lcom/google/api/client/util/FieldInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/FieldInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .restart local p0    # "headerValue":Ljava/lang/Object;
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
