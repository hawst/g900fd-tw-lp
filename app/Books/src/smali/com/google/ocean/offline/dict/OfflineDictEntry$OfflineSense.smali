.class public final Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "OfflineDictEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/offline/dict/OfflineDictEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OfflineSense"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;


# instance fields
.field private definition_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasPartOfSpeech:Z

.field private hasPronunciation:Z

.field private memoizedSerializedSize:I

.field private partOfSpeech_:Ljava/lang/String;

.field private pronunciation_:Ljava/lang/String;

.field private synonym_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1252
    new-instance v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 1253
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry;->internalForceInit()V

    .line 1254
    sget-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    invoke-direct {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->initFields()V

    .line 1255
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 810
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 827
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->pronunciation_:Ljava/lang/String;

    .line 834
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->partOfSpeech_:Ljava/lang/String;

    .line 840
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;

    .line 852
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;

    .line 885
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->memoizedSerializedSize:I

    .line 811
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->initFields()V

    .line 812
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/offline/dict/OfflineDictEntry$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$1;

    .prologue
    .line 807
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 813
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 827
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->pronunciation_:Ljava/lang/String;

    .line 834
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->partOfSpeech_:Ljava/lang/String;

    .line 840
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;

    .line 852
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;

    .line 885
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->memoizedSerializedSize:I

    .line 813
    return-void
.end method

.method static synthetic access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .param p1, "x1"    # Z

    .prologue
    .line 807
    iput-boolean p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPronunciation:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->pronunciation_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .param p1, "x1"    # Z

    .prologue
    .line 807
    iput-boolean p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPartOfSpeech:Z

    return p1
.end method

.method static synthetic access$2002(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->partOfSpeech_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .locals 1

    .prologue
    .line 817
    sget-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 863
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 1

    .prologue
    .line 988
    # invokes: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->access$1300()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDefinitionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;

    return-object v0
.end method

.method public getPartOfSpeech()Ljava/lang/String;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->partOfSpeech_:Ljava/lang/String;

    return-object v0
.end method

.method public getPronunciation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 829
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->pronunciation_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 887
    iget v3, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->memoizedSerializedSize:I

    .line 888
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 918
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 890
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 891
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPronunciation()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 892
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPronunciation()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 895
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPartOfSpeech()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 896
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 900
    :cond_2
    const/4 v0, 0x0

    .line 901
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getDefinitionList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 902
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 904
    goto :goto_1

    .line 905
    .end local v1    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v3, v0

    .line 906
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getDefinitionList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 909
    const/4 v0, 0x0

    .line 910
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getSynonymList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 911
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 913
    goto :goto_2

    .line 914
    .end local v1    # "element":Ljava/lang/String;
    :cond_4
    add-int/2addr v3, v0

    .line 915
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getSynonymList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 917
    iput v3, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->memoizedSerializedSize:I

    move v4, v3

    .line 918
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public getSynonymList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 855
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;

    return-object v0
.end method

.method public hasPartOfSpeech()Z
    .locals 1

    .prologue
    .line 835
    iget-boolean v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPartOfSpeech:Z

    return v0
.end method

.method public hasPronunciation()Z
    .locals 1

    .prologue
    .line 828
    iget-boolean v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPronunciation:Z

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 870
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getSerializedSize()I

    .line 871
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPronunciation()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 872
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPronunciation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 874
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPartOfSpeech()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 875
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 877
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getDefinitionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 878
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_0

    .line 880
    .end local v0    # "element":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getSynonymList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 881
    .restart local v0    # "element":Ljava/lang/String;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 883
    .end local v0    # "element":Ljava/lang/String;
    :cond_3
    return-void
.end method
