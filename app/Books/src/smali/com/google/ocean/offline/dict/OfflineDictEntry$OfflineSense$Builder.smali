.class public final Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "OfflineDictEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;",
        "Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1001
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1300()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 1

    .prologue
    .line 995
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 3

    .prologue
    .line 1004
    new-instance v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    invoke-direct {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;-><init>()V

    .line 1005
    .local v0, "builder":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    new-instance v1, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;-><init>(Lcom/google/ocean/offline/dict/OfflineDictEntry$1;)V

    iput-object v1, v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 1006
    return-object v0
.end method


# virtual methods
.method public addDefinition(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1186
    if-nez p1, :cond_0

    .line 1187
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1189
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1190
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1502(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;

    .line 1192
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1193
    return-object p0
.end method

.method public addSynonym(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1226
    if-nez p1, :cond_0

    .line 1227
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1229
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1230
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1602(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;

    .line 1232
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1233
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    .locals 3

    .prologue
    .line 1050
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    if-nez v1, :cond_0

    .line 1051
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1054
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 1055
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    iget-object v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1502(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;

    .line 1058
    :cond_1
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 1059
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    iget-object v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1602(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;

    .line 1062
    :cond_2
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 1063
    .local v0, "returnMe":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 1064
    return-object v0
.end method

.method public clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2

    .prologue
    .line 1023
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    invoke-virtual {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->mergeFrom(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 995
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 995
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 995
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .prologue
    .line 1068
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getDefaultInstance()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1087
    :cond_0
    :goto_0
    return-object p0

    .line 1069
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPronunciation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1070
    invoke-virtual {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPronunciation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->setPronunciation(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    .line 1072
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPartOfSpeech()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1073
    invoke-virtual {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->setPartOfSpeech(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    .line 1075
    :cond_3
    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1076
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1077
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1502(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;

    .line 1079
    :cond_4
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->definition_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1500(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1081
    :cond_5
    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1083
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1602(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/util/List;)Ljava/util/List;

    .line 1085
    :cond_6
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->synonym_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1600(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1095
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1096
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1100
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1101
    :sswitch_0
    return-object p0

    .line 1106
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->setPronunciation(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    goto :goto_0

    .line 1110
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->setPartOfSpeech(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    goto :goto_0

    .line 1114
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->addDefinition(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    goto :goto_0

    .line 1118
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->addSynonym(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    goto :goto_0

    .line 1096
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 995
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 995
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setPartOfSpeech(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1155
    if-nez p1, :cond_0

    .line 1156
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1158
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPartOfSpeech:Z
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1902(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Z)Z

    .line 1159
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->partOfSpeech_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$2002(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/lang/String;)Ljava/lang/String;

    .line 1160
    return-object p0
.end method

.method public setPronunciation(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1134
    if-nez p1, :cond_0

    .line 1135
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1137
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->hasPronunciation:Z
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1702(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Z)Z

    .line 1138
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->pronunciation_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->access$1802(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;Ljava/lang/String;)Ljava/lang/String;

    .line 1139
    return-object p0
.end method
