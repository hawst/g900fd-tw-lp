.class public final Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "OfflineDictEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/offline/dict/OfflineDictEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Word"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;


# instance fields
.field private derivative_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private example_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasTitle:Z

.field private memoizedSerializedSize:I

.field private sense_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;",
            ">;"
        }
    .end annotation
.end field

.field private title_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 799
    new-instance v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 800
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry;->internalForceInit()V

    .line 801
    sget-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    invoke-direct {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->initFields()V

    .line 802
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 330
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->title_:Ljava/lang/String;

    .line 336
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;

    .line 348
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;

    .line 360
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;

    .line 393
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->memoizedSerializedSize:I

    .line 314
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->initFields()V

    .line 315
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/offline/dict/OfflineDictEntry$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$1;

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 330
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->title_:Ljava/lang/String;

    .line 336
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;

    .line 348
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;

    .line 360
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;

    .line 393
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->memoizedSerializedSize:I

    .line 316
    return-void
.end method

.method static synthetic access$1002(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .param p1, "x1"    # Z

    .prologue
    .line 310
    iput-boolean p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->hasTitle:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->title_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .locals 1

    .prologue
    .line 320
    sget-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 1

    .prologue
    .line 496
    # invokes: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->access$500()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDerivativeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;

    return-object v0
.end method

.method public getExampleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;

    return-object v0
.end method

.method public getSenseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 395
    iget v3, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->memoizedSerializedSize:I

    .line 396
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 426
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 398
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 399
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->hasTitle()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 400
    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 403
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getSenseList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 404
    .local v1, "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 406
    goto :goto_1

    .line 408
    .end local v1    # "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    :cond_2
    const/4 v0, 0x0

    .line 409
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getExampleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 410
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 412
    goto :goto_2

    .line 413
    .end local v1    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v3, v0

    .line 414
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getExampleList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 417
    const/4 v0, 0x0

    .line 418
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getDerivativeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 419
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 421
    goto :goto_3

    .line 422
    .end local v1    # "element":Ljava/lang/String;
    :cond_4
    add-int/2addr v3, v0

    .line 423
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getDerivativeList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 425
    iput v3, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->memoizedSerializedSize:I

    move v4, v3

    .line 426
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasTitle()Z
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->hasTitle:Z

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getSerializedSize()I

    .line 379
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 380
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 382
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getSenseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 383
    .local v0, "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 385
    .end local v0    # "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getExampleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 386
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 388
    .end local v0    # "element":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getDerivativeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 389
    .restart local v0    # "element":Ljava/lang/String;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_2

    .line 391
    .end local v0    # "element":Ljava/lang/String;
    :cond_3
    return-void
.end method
