.class public final Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "OfflineDictEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;",
        "Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$500()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 1

    .prologue
    .line 503
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 3

    .prologue
    .line 512
    new-instance v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    invoke-direct {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;-><init>()V

    .line 513
    .local v0, "builder":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    new-instance v1, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;-><init>(Lcom/google/ocean/offline/dict/OfflineDictEntry$1;)V

    iput-object v1, v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 514
    return-object v0
.end method


# virtual methods
.method public addDerivative(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 773
    if-nez p1, :cond_0

    .line 774
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 776
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 777
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$902(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 779
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 780
    return-object p0
.end method

.method public addExample(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 733
    if-nez p1, :cond_0

    .line 734
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$802(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 739
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 740
    return-object p0
.end method

.method public addSense(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .prologue
    .line 686
    if-nez p1, :cond_0

    .line 687
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$702(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 692
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 693
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .locals 3

    .prologue
    .line 558
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    if-nez v1, :cond_0

    .line 559
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 562
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 563
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    iget-object v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$702(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 566
    :cond_1
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 567
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    iget-object v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$802(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 570
    :cond_2
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 571
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    iget-object v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$902(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 574
    :cond_3
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 575
    .local v0, "returnMe":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 576
    return-object v0
.end method

.method public clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 2

    .prologue
    .line 531
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    invoke-virtual {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->mergeFrom(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .prologue
    .line 580
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getDefaultInstance()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 602
    :cond_0
    :goto_0
    return-object p0

    .line 581
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 582
    invoke-virtual {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->setTitle(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    .line 584
    :cond_2
    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 585
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 586
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$702(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 588
    :cond_3
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->sense_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$700(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 590
    :cond_4
    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 591
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 592
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$802(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 594
    :cond_5
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->example_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$800(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 596
    :cond_6
    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 598
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$902(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/util/List;)Ljava/util/List;

    .line 600
    :cond_7
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->derivative_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$900(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 610
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 611
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 615
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 616
    :sswitch_0
    return-object p0

    .line 621
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->setTitle(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    goto :goto_0

    .line 625
    :sswitch_2
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->newBuilder()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;

    move-result-object v0

    .line 626
    .local v0, "subBuilder":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 627
    invoke-virtual {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;->buildPartial()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->addSense(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    goto :goto_0

    .line 631
    .end local v0    # "subBuilder":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense$Builder;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->addExample(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    goto :goto_0

    .line 635
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->addDerivative(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    goto :goto_0

    .line 611
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 503
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 503
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 651
    if-nez p1, :cond_0

    .line 652
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->hasTitle:Z
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$1002(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Z)Z

    .line 655
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->title_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->access$1102(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;Ljava/lang/String;)Ljava/lang/String;

    .line 656
    return-object p0
.end method
