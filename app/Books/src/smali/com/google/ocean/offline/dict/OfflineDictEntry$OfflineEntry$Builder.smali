.class public final Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "OfflineDictEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;",
        "Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->buildParsed()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->buildPartial()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 3

    .prologue
    .line 151
    new-instance v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    invoke-direct {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;-><init>()V

    .line 152
    .local v0, "builder":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    new-instance v1, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;-><init>(Lcom/google/ocean/offline/dict/OfflineDictEntry$1;)V

    iput-object v1, v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .line 153
    return-object v0
.end method


# virtual methods
.method public addWord(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .prologue
    .line 269
    if-nez p1, :cond_0

    .line 270
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$302(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;Ljava/util/List;)Ljava/util/List;

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    .locals 3

    .prologue
    .line 197
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    if-nez v1, :cond_0

    .line 198
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 202
    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    iget-object v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$302(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;Ljava/util/List;)Ljava/util/List;

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .line 206
    .local v0, "returnMe":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .line 207
    return-object v0
.end method

.method public clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 2

    .prologue
    .line 170
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    invoke-virtual {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->mergeFrom(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->clone()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    invoke-virtual {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .prologue
    .line 211
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->getDefaultInstance()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-object p0

    .line 212
    :cond_1
    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$302(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;Ljava/util/List;)Ljava/util/List;

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->result:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 227
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 231
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 232
    :sswitch_0
    return-object p0

    .line 237
    :sswitch_1
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->newBuilder()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;

    move-result-object v0

    .line 238
    .local v0, "subBuilder":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 239
    invoke-virtual {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word$Builder;->buildPartial()Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->addWord(Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    goto :goto_0

    .line 227
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method
