.class public final Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "OfflineDictEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/offline/dict/OfflineDictEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OfflineEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;


# instance fields
.field private memoizedSerializedSize:I

.field private word_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 302
    new-instance v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .line 303
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry;->internalForceInit()V

    .line 304
    sget-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    invoke-direct {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->initFields()V

    .line 305
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 30
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/offline/dict/OfflineDictEntry$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 30
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$300(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->defaultInstance:Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    .locals 1

    .prologue
    .line 135
    # invokes: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->create()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->access$100()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->newBuilder()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;

    # invokes: Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->buildParsed()Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;->access$000(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry$Builder;)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 56
    iget v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->memoizedSerializedSize:I

    .line 57
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 65
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 59
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->getWordList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 61
    .local v0, "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 63
    goto :goto_1

    .line 64
    .end local v0    # "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    :cond_1
    iput v2, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->memoizedSerializedSize:I

    move v3, v2

    .line 65
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getWord(I)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    return-object v0
.end method

.method public getWordList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->word_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->getSerializedSize()I

    .line 49
    invoke-virtual {p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->getWordList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 50
    .local v0, "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 52
    .end local v0    # "element":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    :cond_0
    return-void
.end method
