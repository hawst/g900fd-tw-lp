.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 558
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1400()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 1

    .prologue
    .line 552
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 3

    .prologue
    .line 561
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;-><init>()V

    .line 562
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 563
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .locals 3

    .prologue
    .line 607
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    if-nez v1, :cond_0

    .line 608
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 612
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 613
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 2

    .prologue
    .line 580
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v0

    return-object v0
.end method

.method public hasViewport()Z
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .prologue
    .line 617
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 621
    :cond_0
    :goto_0
    return-object p0

    .line 618
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeViewport(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 630
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 634
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 635
    :sswitch_0
    return-object p0

    .line 640
    :sswitch_1
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    .line 641
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->hasViewport()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 642
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    .line 644
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 645
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->setViewport(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    goto :goto_0

    .line 630
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeViewport(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 676
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 681
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Z)Z

    .line 682
    return-object p0

    .line 679
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    goto :goto_0
.end method

.method public setViewport(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .prologue
    .line 661
    if-nez p1, :cond_0

    .line 662
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 664
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Z)Z

    .line 665
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->access$1702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 666
    return-object p0
.end method
