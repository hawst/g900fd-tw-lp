.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 5715
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$14000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5709
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->buildParsed()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14100()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 1

    .prologue
    .line 5709
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5756
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5757
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 5760
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 3

    .prologue
    .line 5718
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;-><init>()V

    .line 5719
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .line 5720
    return-object v0
.end method


# virtual methods
.method public addAvailablePid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6358
    if-nez p1, :cond_0

    .line 6359
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6361
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 6364
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6365
    return-object p0
.end method

.method public addPage(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 5965
    if-nez p1, :cond_0

    .line 5966
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5968
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5969
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5971
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5972
    return-object p0
.end method

.method public addResource(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .prologue
    .line 6290
    if-nez p1, :cond_0

    .line 6291
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6293
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6294
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 6296
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6297
    return-object p0
.end method

.method public addResourceUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6246
    if-nez p1, :cond_0

    .line 6247
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6249
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6250
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 6252
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6253
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .locals 3

    .prologue
    .line 5764
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    if-nez v1, :cond_0

    .line 5765
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5768
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 5769
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5772
    :cond_1
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 5773
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5776
    :cond_2
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 5777
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5780
    :cond_3
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_4

    .line 5781
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5784
    :cond_4
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .line 5785
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .line 5786
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2

    .prologue
    .line 5737
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5709
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5709
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5709
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .locals 1

    .prologue
    .line 6196
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v0

    return-object v0
.end method

.method public getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .locals 1

    .prologue
    .line 6138
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v0

    return-object v0
.end method

.method public getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .locals 1

    .prologue
    .line 6101
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v0

    return-object v0
.end method

.method public hasFixedLayoutInfo()Z
    .locals 1

    .prologue
    .line 6193
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo()Z

    move-result v0

    return v0
.end method

.method public hasOfflineLicenseInfo()Z
    .locals 1

    .prologue
    .line 6135
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo()Z

    move-result v0

    return v0
.end method

.method public hasReasonContentBlocked()Z
    .locals 1

    .prologue
    .line 6098
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 5745
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFixedLayoutInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .prologue
    .line 6212
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 6214
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 6219
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6220
    return-object p0

    .line 6217
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5790
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 5845
    :cond_0
    :goto_0
    return-object p0

    .line 5791
    :cond_1
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 5792
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5793
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5795
    :cond_2
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5797
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasPrefix()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5798
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setPrefix(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5800
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasDebug()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5801
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getDebug()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setDebug(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5803
    :cond_5
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5804
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContent()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setContent(Lcom/google/protobuf/ByteString;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5806
    :cond_6
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasVolumeVersion()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5807
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getVolumeVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setVolumeVersion(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5809
    :cond_7
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContentEncrypted()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 5810
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContentEncrypted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setContentEncrypted(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5812
    :cond_8
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 5813
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeReasonContentBlocked(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5815
    :cond_9
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 5816
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeOfflineLicenseInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5818
    :cond_a
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 5819
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getStyle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setStyle(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5821
    :cond_b
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 5822
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeFixedLayoutInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5824
    :cond_c
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 5825
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 5826
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5828
    :cond_d
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5830
    :cond_e
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 5831
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 5832
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5834
    :cond_f
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5836
    :cond_10
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasCurrentPosition()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 5837
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getCurrentPosition()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setCurrentPosition(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    .line 5839
    :cond_11
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5840
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 5841
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;

    .line 5843
    :cond_12
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5853
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 5854
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 5858
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5859
    :sswitch_0
    return-object p0

    .line 5864
    :sswitch_1
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    .line 5865
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5866
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->addPage(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5870
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setDebug(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5874
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setPrefix(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5878
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setContent(Lcom/google/protobuf/ByteString;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5882
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setCurrentPosition(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5886
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->addAvailablePid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5890
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setStyle(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5894
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->addResourceUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5898
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setContentEncrypted(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5902
    :sswitch_a
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    .line 5903
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->hasReasonContentBlocked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5904
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    .line 5906
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5907
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setReasonContentBlocked(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto :goto_0

    .line 5911
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    :sswitch_b
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    .line 5912
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5913
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->addResource(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto/16 :goto_0

    .line 5917
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setVolumeVersion(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto/16 :goto_0

    .line 5921
    :sswitch_d
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    .line 5922
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->hasOfflineLicenseInfo()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5923
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    .line 5925
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5926
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setOfflineLicenseInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto/16 :goto_0

    .line 5930
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    :sswitch_e
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    .line 5931
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->hasFixedLayoutInfo()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5932
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    .line 5934
    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5935
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->setFixedLayoutInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    goto/16 :goto_0

    .line 5854
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5709
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5709
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeOfflineLicenseInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .prologue
    .line 6154
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 6156
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 6161
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6162
    return-object p0

    .line 6159
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    goto :goto_0
.end method

.method public mergeReasonContentBlocked(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .prologue
    .line 6117
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15800(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 6119
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15800(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 6124
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6125
    return-object p0

    .line 6122
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    goto :goto_0
.end method

.method public setContent(Lcom/google/protobuf/ByteString;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 6044
    if-nez p1, :cond_0

    .line 6045
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6047
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContent:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6048
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->content_:Lcom/google/protobuf/ByteString;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    .line 6049
    return-object p0
.end method

.method public setContentEncrypted(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 6086
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContentEncrypted:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6087
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->contentEncrypted_:Z
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6088
    return-object p0
.end method

.method public setCurrentPosition(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6327
    if-nez p1, :cond_0

    .line 6328
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6330
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasCurrentPosition:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6331
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->currentPosition_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;

    .line 6332
    return-object p0
.end method

.method public setDebug(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6023
    if-nez p1, :cond_0

    .line 6024
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6026
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasDebug:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6027
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->debug_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;

    .line 6028
    return-object p0
.end method

.method public setFixedLayoutInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .prologue
    .line 6199
    if-nez p1, :cond_0

    .line 6200
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6202
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6203
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 6204
    return-object p0
.end method

.method public setOfflineLicenseInfo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .prologue
    .line 6141
    if-nez p1, :cond_0

    .line 6142
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6144
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6145
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 6146
    return-object p0
.end method

.method public setPrefix(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6002
    if-nez p1, :cond_0

    .line 6003
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6005
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasPrefix:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6006
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->prefix_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$14802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;

    .line 6007
    return-object p0
.end method

.method public setReasonContentBlocked(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .prologue
    .line 6104
    if-nez p1, :cond_0

    .line 6105
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6107
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6108
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 6109
    return-object p0
.end method

.method public setStyle(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6178
    if-nez p1, :cond_0

    .line 6179
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6181
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6182
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->style_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$16202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;

    .line 6183
    return-object p0
.end method

.method public setVolumeVersion(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6065
    if-nez p1, :cond_0

    .line 6066
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6068
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasVolumeVersion:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z

    .line 6069
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->volumeVersion_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->access$15402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;

    .line 6070
    return-object p0
.end method
