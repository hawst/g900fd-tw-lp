.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3555
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$8500()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 1

    .prologue
    .line 3549
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 3

    .prologue
    .line 3558
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;-><init>()V

    .line 3559
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 3560
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    .locals 3

    .prologue
    .line 3604
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    if-nez v1, :cond_0

    .line 3605
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3608
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 3609
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 3610
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 2

    .prologue
    .line 3577
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3549
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3549
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3549
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .prologue
    .line 3614
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3621
    :cond_0
    :goto_0
    return-object p0

    .line 3615
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3616
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->setUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    .line 3618
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3619
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3629
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3630
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3634
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3635
    :sswitch_0
    return-object p0

    .line 3640
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->setUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    goto :goto_0

    .line 3644
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    goto :goto_0

    .line 3630
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3549
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3549
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setMimeType(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 3681
    if-nez p1, :cond_0

    .line 3682
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3684
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->access$8902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Z)Z

    .line 3685
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->mimeType_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->access$9002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Ljava/lang/String;)Ljava/lang/String;

    .line 3686
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 3660
    if-nez p1, :cond_0

    .line 3661
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3663
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->access$8702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Z)Z

    .line 3664
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->url_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->access$8802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Ljava/lang/String;)Ljava/lang/String;

    .line 3665
    return-object p0
.end method
