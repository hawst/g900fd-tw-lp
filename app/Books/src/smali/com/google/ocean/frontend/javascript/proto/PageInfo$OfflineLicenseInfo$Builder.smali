.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3238
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$7600()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 1

    .prologue
    .line 3232
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 3

    .prologue
    .line 3241
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;-><init>()V

    .line 3242
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 3243
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .locals 3

    .prologue
    .line 3287
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    if-nez v1, :cond_0

    .line 3288
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3291
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 3292
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 3293
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 2

    .prologue
    .line 3260
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3232
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3232
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3232
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .prologue
    .line 3297
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3307
    :cond_0
    :goto_0
    return-object p0

    .line 3298
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumAllowedOfflineDevices()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3299
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getNumAllowedOfflineDevices()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->setNumAllowedOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    .line 3301
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumOfflineDevices()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3302
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getNumOfflineDevices()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->setNumOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    .line 3304
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasJustAcquiredOfflineLicense()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3305
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getJustAcquiredOfflineLicense()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->setJustAcquiredOfflineLicense(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3315
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 3316
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3320
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3321
    :sswitch_0
    return-object p0

    .line 3326
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->setNumAllowedOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    goto :goto_0

    .line 3330
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->setNumOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    goto :goto_0

    .line 3334
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->setJustAcquiredOfflineLicense(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    goto :goto_0

    .line 3316
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3232
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3232
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setJustAcquiredOfflineLicense(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 3386
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasJustAcquiredOfflineLicense:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->access$8202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z

    .line 3387
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->justAcquiredOfflineLicense_:Z
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->access$8302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z

    .line 3388
    return-object p0
.end method

.method public setNumAllowedOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 3350
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumAllowedOfflineDevices:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->access$7802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z

    .line 3351
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numAllowedOfflineDevices_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->access$7902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;I)I

    .line 3352
    return-object p0
.end method

.method public setNumOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 3368
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumOfflineDevices:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->access$8002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z

    .line 3369
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numOfflineDevices_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->access$8102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;I)I

    .line 3370
    return-object p0
.end method
