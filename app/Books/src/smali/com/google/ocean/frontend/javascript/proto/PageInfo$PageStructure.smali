.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PageStructure"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;,
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;


# instance fields
.field private block_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;",
            ">;"
        }
    .end annotation
.end field

.field private firstWordOffset_:I

.field private hasFirstWordOffset:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2241
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 2242
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 2243
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->initFields()V

    .line 2244
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1078
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1927
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    .line 1940
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->firstWordOffset_:I

    .line 1964
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->memoizedSerializedSize:I

    .line 1079
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->initFields()V

    .line 1080
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 1075
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 1081
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1927
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    .line 1940
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->firstWordOffset_:I

    .line 1964
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->memoizedSerializedSize:I

    .line 1081
    return-void
.end method

.method static synthetic access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1075
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p1, "x1"    # Z

    .prologue
    .line 1075
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->hasFirstWordOffset:Z

    return p1
.end method

.method static synthetic access$5202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p1, "x1"    # I

    .prologue
    .line 1075
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->firstWordOffset_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1

    .prologue
    .line 1085
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 1945
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 1

    .prologue
    .line 2049
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->access$4800()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 2052
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBlock(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1934
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    return-object v0
.end method

.method public getBlockCount()I
    .locals 1

    .prologue
    .line 1932
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBlockList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;

    return-object v0
.end method

.method public getFirstWordOffset()I
    .locals 1

    .prologue
    .line 1942
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->firstWordOffset_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1966
    iget v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->memoizedSerializedSize:I

    .line 1967
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 1979
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 1969
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 1970
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .line 1971
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeGroupSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1973
    goto :goto_1

    .line 1974
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->hasFirstWordOffset()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1975
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getFirstWordOffset()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 1978
    :cond_2
    iput v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->memoizedSerializedSize:I

    move v3, v2

    .line 1979
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public hasFirstWordOffset()Z
    .locals 1

    .prologue
    .line 1941
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->hasFirstWordOffset:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 1947
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .line 1948
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 1950
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1955
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getSerializedSize()I

    .line 1956
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .line 1957
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeGroup(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 1959
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->hasFirstWordOffset()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1960
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getFirstWordOffset()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1962
    :cond_1
    return-void
.end method
