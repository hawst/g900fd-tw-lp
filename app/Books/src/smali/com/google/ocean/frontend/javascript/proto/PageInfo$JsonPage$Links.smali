.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Links"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;


# instance fields
.field private hasRegion:Z

.field private hasTargetPid:Z

.field private hasUrl:Z

.field private memoizedSerializedSize:I

.field private region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

.field private targetPid_:Ljava/lang/String;

.field private url_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4116
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 4117
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 4118
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->initFields()V

    .line 4119
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3763
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3787
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->targetPid_:Ljava/lang/String;

    .line 3794
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->url_:Ljava/lang/String;

    .line 3822
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->memoizedSerializedSize:I

    .line 3764
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->initFields()V

    .line 3765
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 3760
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 3766
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3787
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->targetPid_:Ljava/lang/String;

    .line 3794
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->url_:Ljava/lang/String;

    .line 3822
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->memoizedSerializedSize:I

    .line 3766
    return-void
.end method

.method static synthetic access$9402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .param p1, "x1"    # Z

    .prologue
    .line 3760
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion:Z

    return p1
.end method

.method static synthetic access$9500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .prologue
    .line 3760
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method static synthetic access$9502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 3760
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object p1
.end method

.method static synthetic access$9602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .param p1, "x1"    # Z

    .prologue
    .line 3760
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid:Z

    return p1
.end method

.method static synthetic access$9702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3760
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->targetPid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$9802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .param p1, "x1"    # Z

    .prologue
    .line 3760
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasUrl:Z

    return p1
.end method

.method static synthetic access$9902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3760
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->url_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .locals 1

    .prologue
    .line 3770
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 3799
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 3800
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 1

    .prologue
    .line 3911
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->access$9200()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 3782
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 3824
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->memoizedSerializedSize:I

    .line 3825
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 3841
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 3827
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 3828
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3829
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3832
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3833
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getTargetPid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3836
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3837
    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3840
    :cond_3
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->memoizedSerializedSize:I

    move v1, v0

    .line 3841
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getTargetPid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3789
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->targetPid_:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3796
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasRegion()Z
    .locals 1

    .prologue
    .line 3781
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion:Z

    return v0
.end method

.method public hasTargetPid()Z
    .locals 1

    .prologue
    .line 3788
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    .prologue
    .line 3795
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasUrl:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3802
    iget-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion:Z

    if-nez v1, :cond_1

    .line 3805
    :cond_0
    :goto_0
    return v0

    .line 3803
    :cond_1
    iget-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid:Z

    if-eqz v1, :cond_0

    .line 3804
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3805
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3810
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getSerializedSize()I

    .line 3811
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3812
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3814
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3815
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getTargetPid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 3817
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3818
    const/16 v0, 0x13

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 3820
    :cond_2
    return-void
.end method
