.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Resource"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;


# instance fields
.field private hasMimeType:Z

.field private hasUrl:Z

.field private memoizedSerializedSize:I

.field private mimeType_:Ljava/lang/String;

.field private url_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3698
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 3699
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 3700
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->initFields()V

    .line 3701
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3411
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3428
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->url_:Ljava/lang/String;

    .line 3435
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->mimeType_:Ljava/lang/String;

    .line 3457
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->memoizedSerializedSize:I

    .line 3412
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->initFields()V

    .line 3413
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 3408
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 3414
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3428
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->url_:Ljava/lang/String;

    .line 3435
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->mimeType_:Ljava/lang/String;

    .line 3457
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->memoizedSerializedSize:I

    .line 3414
    return-void
.end method

.method static synthetic access$8702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    .param p1, "x1"    # Z

    .prologue
    .line 3408
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl:Z

    return p1
.end method

.method static synthetic access$8802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3408
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->url_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$8902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    .param p1, "x1"    # Z

    .prologue
    .line 3408
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType:Z

    return p1
.end method

.method static synthetic access$9002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3408
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->mimeType_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    .locals 1

    .prologue
    .line 3418
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 3440
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    .locals 1

    .prologue
    .line 3542
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;->access$8500()Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3437
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->mimeType_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 3459
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->memoizedSerializedSize:I

    .line 3460
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 3472
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 3462
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 3463
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3464
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3467
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3468
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3471
    :cond_2
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->memoizedSerializedSize:I

    move v1, v0

    .line 3472
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3430
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasMimeType()Z
    .locals 1

    .prologue
    .line 3436
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    .prologue
    .line 3429
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 3442
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 3443
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3448
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getSerializedSize()I

    .line 3449
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3450
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 3452
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->hasMimeType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3453
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 3455
    :cond_1
    return-void
.end method
