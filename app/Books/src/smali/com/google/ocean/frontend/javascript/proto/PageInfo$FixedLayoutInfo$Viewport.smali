.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Viewport"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;


# instance fields
.field private hasHeight:Z

.field private hasInitialScale:Z

.field private hasMaximumScale:Z

.field private hasMinimumScale:Z

.field private hasWidth:Z

.field private height_:Ljava/lang/String;

.field private initialScale_:F

.field private maximumScale_:F

.field private memoizedSerializedSize:I

.field private minimumScale_:F

.field private width_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 434
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 435
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 436
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initFields()V

    .line 437
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->width_:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->height_:Ljava/lang/String;

    .line 62
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initialScale_:F

    .line 69
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->minimumScale_:F

    .line 76
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->maximumScale_:F

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->memoizedSerializedSize:I

    .line 32
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initFields()V

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->width_:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->height_:Ljava/lang/String;

    .line 62
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initialScale_:F

    .line 69
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->minimumScale_:F

    .line 76
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->maximumScale_:F

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->memoizedSerializedSize:I

    .line 34
    return-void
.end method

.method static synthetic access$1002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->minimumScale_:F

    return p1
.end method

.method static synthetic access$1102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMaximumScale:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->maximumScale_:F

    return p1
.end method

.method static synthetic access$302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasWidth:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->width_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasHeight:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->height_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasInitialScale:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initialScale_:F

    return p1
.end method

.method static synthetic access$902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMinimumScale:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 1

    .prologue
    .line 203
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->access$100()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .prologue
    .line 206
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getHeight()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->height_:Ljava/lang/String;

    return-object v0
.end method

.method public getInitialScale()F
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initialScale_:F

    return v0
.end method

.method public getMaximumScale()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->maximumScale_:F

    return v0
.end method

.method public getMinimumScale()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->minimumScale_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 108
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->memoizedSerializedSize:I

    .line 109
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 133
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 111
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 112
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasWidth()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getWidth()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasHeight()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 117
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getHeight()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 120
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasInitialScale()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 121
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getInitialScale()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 124
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMinimumScale()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 125
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getMinimumScale()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_4
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMaximumScale()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 129
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getMaximumScale()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_5
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->memoizedSerializedSize:I

    move v1, v0

    .line 133
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getWidth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->width_:Ljava/lang/String;

    return-object v0
.end method

.method public hasHeight()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasHeight:Z

    return v0
.end method

.method public hasInitialScale()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasInitialScale:Z

    return v0
.end method

.method public hasMaximumScale()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMaximumScale:Z

    return v0
.end method

.method public hasMinimumScale()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMinimumScale:Z

    return v0
.end method

.method public hasWidth()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasWidth:Z

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getSerializedSize()I

    .line 89
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getWidth()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getHeight()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasInitialScale()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getInitialScale()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 98
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMinimumScale()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getMinimumScale()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 101
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMaximumScale()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 102
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getMaximumScale()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeFloat(IF)V

    .line 104
    :cond_4
    return-void
.end method
