.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3924
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$9200()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 1

    .prologue
    .line 3918
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 3

    .prologue
    .line 3927
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;-><init>()V

    .line 3928
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 3929
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    .locals 3

    .prologue
    .line 3973
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    if-nez v1, :cond_0

    .line 3974
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3977
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 3978
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 3979
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 2

    .prologue
    .line 3946
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3918
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3918
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3918
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 4038
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    return-object v0
.end method

.method public hasRegion()Z
    .locals 1

    .prologue
    .line 4035
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .prologue
    .line 3983
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 3993
    :cond_0
    :goto_0
    return-object p0

    .line 3984
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3985
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->mergeRegion(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    .line 3987
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3988
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getTargetPid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->setTargetPid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    .line 3990
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3991
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->setUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4001
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 4002
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 4006
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4007
    :sswitch_0
    return-object p0

    .line 4012
    :sswitch_1
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 4013
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->hasRegion()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4014
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->getRegion()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 4016
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4017
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->setRegion(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    goto :goto_0

    .line 4021
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->setTargetPid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    goto :goto_0

    .line 4025
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->setUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    goto :goto_0

    .line 4002
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2a -> :sswitch_1
        0x32 -> :sswitch_2
        0x9a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3918
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3918
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeRegion(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 4054
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4056
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4061
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z

    .line 4062
    return-object p0

    .line 4059
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    goto :goto_0
.end method

.method public setRegion(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 4041
    if-nez p1, :cond_0

    .line 4042
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4044
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasRegion:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z

    .line 4045
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->region_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4046
    return-object p0
.end method

.method public setTargetPid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4078
    if-nez p1, :cond_0

    .line 4079
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4081
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasTargetPid:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z

    .line 4082
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->targetPid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Ljava/lang/String;)Ljava/lang/String;

    .line 4083
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4099
    if-nez p1, :cond_0

    .line 4100
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4102
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->hasUrl:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Z)Z

    .line 4103
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->url_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->access$9902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;Ljava/lang/String;)Ljava/lang/String;

    .line 4104
    return-object p0
.end method
