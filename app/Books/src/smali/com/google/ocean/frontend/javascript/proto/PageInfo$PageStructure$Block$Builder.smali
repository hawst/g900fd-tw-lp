.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1686
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$4000()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 1

    .prologue
    .line 1680
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 3

    .prologue
    .line 1689
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;-><init>()V

    .line 1690
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .line 1691
    return-object v0
.end method


# virtual methods
.method public addParagraph(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    .prologue
    .line 1847
    if-nez p1, :cond_0

    .line 1848
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1850
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1851
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Ljava/util/List;)Ljava/util/List;

    .line 1853
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1854
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    .locals 3

    .prologue
    .line 1735
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    if-nez v1, :cond_0

    .line 1736
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1739
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 1740
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Ljava/util/List;)Ljava/util/List;

    .line 1743
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .line 1744
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .line 1745
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 2

    .prologue
    .line 1708
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1680
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1680
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1680
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 1881
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    return-object v0
.end method

.method public hasBox()Z
    .locals 1

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasBox()Z

    move-result v0

    return v0
.end method

.method public mergeBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1899
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 1904
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasBox:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Z)Z

    .line 1905
    return-object p0

    .line 1902
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .prologue
    .line 1749
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 1762
    :cond_0
    :goto_0
    return-object p0

    .line 1750
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasAppearance()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1751
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getAppearance()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->setAppearance(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    .line 1753
    :cond_2
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1754
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1755
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Ljava/util/List;)Ljava/util/List;

    .line 1757
    :cond_3
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->paragraph_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1759
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1760
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->mergeBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1770
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 1771
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 1775
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1776
    :sswitch_0
    return-object p0

    .line 1781
    :sswitch_1
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;

    move-result-object v0

    .line 1782
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readGroup(ILcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 1783
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->addParagraph(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    goto :goto_0

    .line 1787
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->setAppearance(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    goto :goto_0

    .line 1791
    :sswitch_3
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 1792
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->hasBox()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1793
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 1795
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 1796
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->setBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    goto :goto_0

    .line 1771
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x13 -> :sswitch_1
        0x22 -> :sswitch_2
        0x4a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1680
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1680
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setAppearance(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1812
    if-nez p1, :cond_0

    .line 1813
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1815
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasAppearance:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Z)Z

    .line 1816
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->appearance_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Ljava/lang/String;)Ljava/lang/String;

    .line 1817
    return-object p0
.end method

.method public setBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 1884
    if-nez p1, :cond_0

    .line 1885
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1887
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->hasBox:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Z)Z

    .line 1888
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->access$4602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 1889
    return-object p0
.end method
