.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WordBox"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;


# instance fields
.field private baselineY_:I

.field private box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

.field private flowWithNext_:Z

.field private hasBaselineY:Z

.field private hasBox:Z

.field private hasFlowWithNext:Z

.field private hasWord:Z

.field private memoizedSerializedSize:I

.field private word_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2641
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 2642
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 2643
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->initFields()V

    .line 2644
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2252
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2269
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->word_:Ljava/lang/String;

    .line 2283
    iput-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->flowWithNext_:Z

    .line 2290
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->baselineY_:I

    .line 2321
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->memoizedSerializedSize:I

    .line 2253
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->initFields()V

    .line 2254
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 2249
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 2255
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2269
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->word_:Ljava/lang/String;

    .line 2283
    iput-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->flowWithNext_:Z

    .line 2290
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->baselineY_:I

    .line 2321
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->memoizedSerializedSize:I

    .line 2255
    return-void
.end method

.method static synthetic access$5602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Z

    .prologue
    .line 2249
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasWord:Z

    return p1
.end method

.method static synthetic access$5702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 2249
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->word_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Z

    .prologue
    .line 2249
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox:Z

    return p1
.end method

.method static synthetic access$5900(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .prologue
    .line 2249
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method static synthetic access$5902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 2249
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object p1
.end method

.method static synthetic access$6002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Z

    .prologue
    .line 2249
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasFlowWithNext:Z

    return p1
.end method

.method static synthetic access$6102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Z

    .prologue
    .line 2249
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->flowWithNext_:Z

    return p1
.end method

.method static synthetic access$6202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # Z

    .prologue
    .line 2249
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBaselineY:Z

    return p1
.end method

.method static synthetic access$6302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .param p1, "x1"    # I

    .prologue
    .line 2249
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->baselineY_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .locals 1

    .prologue
    .line 2259
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 2295
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 2296
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 1

    .prologue
    .line 2414
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->access$5400()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBaselineY()I
    .locals 1

    .prologue
    .line 2292
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->baselineY_:I

    return v0
.end method

.method public getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 2278
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method public getFlowWithNext()Z
    .locals 1

    .prologue
    .line 2285
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->flowWithNext_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 2323
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->memoizedSerializedSize:I

    .line 2324
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 2344
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 2326
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 2327
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasWord()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2328
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getWord()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2331
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2332
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2335
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasFlowWithNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2336
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getFlowWithNext()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2339
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBaselineY()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2340
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBaselineY()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2343
    :cond_4
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->memoizedSerializedSize:I

    move v1, v0

    .line 2344
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2271
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->word_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBaselineY()Z
    .locals 1

    .prologue
    .line 2291
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBaselineY:Z

    return v0
.end method

.method public hasBox()Z
    .locals 1

    .prologue
    .line 2277
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox:Z

    return v0
.end method

.method public hasFlowWithNext()Z
    .locals 1

    .prologue
    .line 2284
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasFlowWithNext:Z

    return v0
.end method

.method public hasWord()Z
    .locals 1

    .prologue
    .line 2270
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasWord:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 2298
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2306
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getSerializedSize()I

    .line 2307
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasWord()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2308
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getWord()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 2310
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2311
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2313
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasFlowWithNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2314
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getFlowWithNext()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 2316
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBaselineY()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2317
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBaselineY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2319
    :cond_3
    return-void
.end method
