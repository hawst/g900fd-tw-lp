.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Paragraph"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;


# instance fields
.field private appearance_:Ljava/lang/String;

.field private continuesFromPrevPage_:Z

.field private continuesOnNextPage_:Z

.field private hasAppearance:Z

.field private hasContinuesFromPrevPage:Z

.field private hasContinuesOnNextPage:Z

.field private memoizedSerializedSize:I

.field private wordbox_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1523
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    .line 1524
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 1525
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->initFields()V

    .line 1526
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1112
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1129
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->appearance_:Ljava/lang/String;

    .line 1135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    .line 1148
    iput-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesOnNextPage_:Z

    .line 1155
    iput-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesFromPrevPage_:Z

    .line 1185
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->memoizedSerializedSize:I

    .line 1113
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->initFields()V

    .line 1114
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 1109
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1115
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1129
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->appearance_:Ljava/lang/String;

    .line 1135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    .line 1148
    iput-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesOnNextPage_:Z

    .line 1155
    iput-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesFromPrevPage_:Z

    .line 1185
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->memoizedSerializedSize:I

    .line 1115
    return-void
.end method

.method static synthetic access$3200(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 1109
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Z

    .prologue
    .line 1109
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasAppearance:Z

    return p1
.end method

.method static synthetic access$3402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 1109
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->appearance_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Z

    .prologue
    .line 1109
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesOnNextPage:Z

    return p1
.end method

.method static synthetic access$3602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Z

    .prologue
    .line 1109
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesOnNextPage_:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Z

    .prologue
    .line 1109
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesFromPrevPage:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .param p1, "x1"    # Z

    .prologue
    .line 1109
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesFromPrevPage_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .locals 1

    .prologue
    .line 1119
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 1160
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;
    .locals 1

    .prologue
    .line 1278
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;->access$3000()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAppearance()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->appearance_:Ljava/lang/String;

    return-object v0
.end method

.method public getContinuesFromPrevPage()Z
    .locals 1

    .prologue
    .line 1157
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesFromPrevPage_:Z

    return v0
.end method

.method public getContinuesOnNextPage()Z
    .locals 1

    .prologue
    .line 1150
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->continuesOnNextPage_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1187
    iget v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->memoizedSerializedSize:I

    .line 1188
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 1208
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 1190
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 1191
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 1192
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1194
    goto :goto_1

    .line 1195
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasAppearance()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1196
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getAppearance()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1199
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesOnNextPage()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1200
    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getContinuesOnNextPage()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 1203
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesFromPrevPage()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1204
    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getContinuesFromPrevPage()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    .line 1207
    :cond_4
    iput v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->memoizedSerializedSize:I

    move v3, v2

    .line 1208
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getWordbox(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    return-object v0
.end method

.method public getWordboxCount()I
    .locals 1

    .prologue
    .line 1140
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getWordboxList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->wordbox_:Ljava/util/List;

    return-object v0
.end method

.method public hasAppearance()Z
    .locals 1

    .prologue
    .line 1130
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasAppearance:Z

    return v0
.end method

.method public hasContinuesFromPrevPage()Z
    .locals 1

    .prologue
    .line 1156
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesFromPrevPage:Z

    return v0
.end method

.method public hasContinuesOnNextPage()Z
    .locals 1

    .prologue
    .line 1149
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesOnNextPage:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    .line 1162
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 1163
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 1165
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1170
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getSerializedSize()I

    .line 1171
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 1172
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 1174
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasAppearance()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1175
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getAppearance()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 1177
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesOnNextPage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1178
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getContinuesOnNextPage()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 1180
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->hasContinuesFromPrevPage()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1181
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getContinuesFromPrevPage()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 1183
    :cond_3
    return-void
.end method
