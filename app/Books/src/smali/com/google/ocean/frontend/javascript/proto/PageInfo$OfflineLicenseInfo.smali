.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OfflineLicenseInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;


# instance fields
.field private hasJustAcquiredOfflineLicense:Z

.field private hasNumAllowedOfflineDevices:Z

.field private hasNumOfflineDevices:Z

.field private justAcquiredOfflineLicense_:Z

.field private memoizedSerializedSize:I

.field private numAllowedOfflineDevices_:I

.field private numOfflineDevices_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3400
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 3401
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 3402
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->initFields()V

    .line 3403
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3081
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3098
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numAllowedOfflineDevices_:I

    .line 3105
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numOfflineDevices_:I

    .line 3112
    iput-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->justAcquiredOfflineLicense_:Z

    .line 3136
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->memoizedSerializedSize:I

    .line 3082
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->initFields()V

    .line 3083
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 3078
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 3084
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3098
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numAllowedOfflineDevices_:I

    .line 3105
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numOfflineDevices_:I

    .line 3112
    iput-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->justAcquiredOfflineLicense_:Z

    .line 3136
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->memoizedSerializedSize:I

    .line 3084
    return-void
.end method

.method static synthetic access$7802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 3078
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumAllowedOfflineDevices:Z

    return p1
.end method

.method static synthetic access$7902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .param p1, "x1"    # I

    .prologue
    .line 3078
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numAllowedOfflineDevices_:I

    return p1
.end method

.method static synthetic access$8002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 3078
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumOfflineDevices:Z

    return p1
.end method

.method static synthetic access$8102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .param p1, "x1"    # I

    .prologue
    .line 3078
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numOfflineDevices_:I

    return p1
.end method

.method static synthetic access$8202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 3078
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasJustAcquiredOfflineLicense:Z

    return p1
.end method

.method static synthetic access$8302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 3078
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->justAcquiredOfflineLicense_:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .locals 1

    .prologue
    .line 3088
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 3117
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 1

    .prologue
    .line 3225
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->access$7600()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .prologue
    .line 3228
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getJustAcquiredOfflineLicense()Z
    .locals 1

    .prologue
    .line 3114
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->justAcquiredOfflineLicense_:Z

    return v0
.end method

.method public getNumAllowedOfflineDevices()I
    .locals 1

    .prologue
    .line 3100
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numAllowedOfflineDevices_:I

    return v0
.end method

.method public getNumOfflineDevices()I
    .locals 1

    .prologue
    .line 3107
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->numOfflineDevices_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 3138
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->memoizedSerializedSize:I

    .line 3139
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 3155
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 3141
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 3142
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumAllowedOfflineDevices()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3143
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getNumAllowedOfflineDevices()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3146
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumOfflineDevices()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3147
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getNumOfflineDevices()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3150
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasJustAcquiredOfflineLicense()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3151
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getJustAcquiredOfflineLicense()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 3154
    :cond_3
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->memoizedSerializedSize:I

    move v1, v0

    .line 3155
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasJustAcquiredOfflineLicense()Z
    .locals 1

    .prologue
    .line 3113
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasJustAcquiredOfflineLicense:Z

    return v0
.end method

.method public hasNumAllowedOfflineDevices()Z
    .locals 1

    .prologue
    .line 3099
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumAllowedOfflineDevices:Z

    return v0
.end method

.method public hasNumOfflineDevices()Z
    .locals 1

    .prologue
    .line 3106
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumOfflineDevices:Z

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3124
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getSerializedSize()I

    .line 3125
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumAllowedOfflineDevices()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3126
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getNumAllowedOfflineDevices()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3128
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasNumOfflineDevices()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3129
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getNumOfflineDevices()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3131
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->hasJustAcquiredOfflineLicense()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3132
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getJustAcquiredOfflineLicense()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3134
    :cond_2
    return-void
.end method
