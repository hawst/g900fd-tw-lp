.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 880
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1900()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 1

    .prologue
    .line 874
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 3

    .prologue
    .line 883
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;-><init>()V

    .line 884
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 885
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 3

    .prologue
    .line 929
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    if-nez v1, :cond_0

    .line 930
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 933
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 934
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 935
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 2

    .prologue
    .line 902
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 939
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 952
    :cond_0
    :goto_0
    return-object p0

    .line 940
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 941
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setX(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 943
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 944
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getY()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setY(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 946
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 947
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setW(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 949
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 950
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setH(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 960
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 961
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 965
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 966
    :sswitch_0
    return-object p0

    .line 971
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setX(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    goto :goto_0

    .line 975
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setY(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    goto :goto_0

    .line 979
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setW(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    goto :goto_0

    .line 983
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->setH(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    goto :goto_0

    .line 961
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 874
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 874
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setH(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z

    .line 1054
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->h_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I

    .line 1055
    return-object p0
.end method

.method public setW(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z

    .line 1036
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->w_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I

    .line 1037
    return-object p0
.end method

.method public setX(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 999
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z

    .line 1000
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->x_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I

    .line 1001
    return-object p0
.end method

.method public setY(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z

    .line 1018
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->y_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->access$2402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I

    .line 1019
    return-object p0
.end method
