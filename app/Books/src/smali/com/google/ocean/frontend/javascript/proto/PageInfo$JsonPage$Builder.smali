.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 4542
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$10100()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 1

    .prologue
    .line 4536
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 3

    .prologue
    .line 4545
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;-><init>()V

    .line 4546
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 4547
    return-object v0
.end method


# virtual methods
.method public addClipHighlights(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 5172
    if-nez p1, :cond_0

    .line 5173
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5175
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5176
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 5178
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5179
    return-object p0
.end method

.method public addHighlights(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 4991
    if-nez p1, :cond_0

    .line 4992
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4994
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4995
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4997
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4998
    return-object p0
.end method

.method public addLinks(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .prologue
    .line 5084
    if-nez p1, :cond_0

    .line 5085
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5087
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5088
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 5090
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5091
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .locals 3

    .prologue
    .line 4591
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    if-nez v1, :cond_0

    .line 4592
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 4595
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 4596
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4599
    :cond_1
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_2

    .line 4600
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4603
    :cond_2
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_3

    .line 4604
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4607
    :cond_3
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 4608
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 4609
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2

    .prologue
    .line 4564
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4536
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4536
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 4536
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 5300
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    return-object v0
.end method

.method public getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 5118
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    return-object v0
.end method

.method public getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1

    .prologue
    .line 5206
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    return-object v0
.end method

.method public hasCcBox()Z
    .locals 1

    .prologue
    .line 5297
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox()Z

    move-result v0

    return v0
.end method

.method public hasClipHighlight()Z
    .locals 1

    .prologue
    .line 5115
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight()Z

    move-result v0

    return v0
.end method

.method public hasStructure()Z
    .locals 1

    .prologue
    .line 5203
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure()Z

    move-result v0

    return v0
.end method

.method public mergeCcBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 5316
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5318
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 5323
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5324
    return-object p0

    .line 5321
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    goto :goto_0
.end method

.method public mergeClipHighlight(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 5134
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5136
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 5141
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5142
    return-object p0

    .line 5139
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 4613
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 4683
    :cond_0
    :goto_0
    return-object p0

    .line 4614
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4615
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getPid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setPid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4617
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSrc()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4618
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSrc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setSrc(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4620
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasImageSolution()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4621
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getImageSolution()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setImageSolution(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4623
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSnippetSrc()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4624
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSnippetSrc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setSnippetSrc(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4626
    :cond_5
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSig()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4627
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSig()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setSig(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4629
    :cond_6
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasOrder()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4630
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getOrder()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setOrder(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4632
    :cond_7
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4633
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setTitle(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4635
    :cond_8
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasFlags()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4636
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getFlags()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setFlags(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4638
    :cond_9
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 4639
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4640
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4642
    :cond_a
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 4644
    :cond_b
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasVq()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 4645
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getVq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setVq(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4647
    :cond_c
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 4648
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setContent(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4650
    :cond_d
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 4651
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 4652
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4654
    :cond_e
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 4656
    :cond_f
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 4657
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->mergeClipHighlight(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4659
    :cond_10
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 4660
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 4661
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;

    .line 4663
    :cond_11
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 4665
    :cond_12
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 4666
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->mergeStructure(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4668
    :cond_13
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasUf()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 4669
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getUf()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setUf(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4671
    :cond_14
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasH()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 4672
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getH()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setH(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4674
    :cond_15
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasW()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 4675
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getW()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setW(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4677
    :cond_16
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 4678
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->mergeCcBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    .line 4680
    :cond_17
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTextSegment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4681
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getTextSegment()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setTextSegment(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 4692
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 4696
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4697
    :sswitch_0
    return-object p0

    .line 4702
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setPid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4706
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setSrc(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4710
    :sswitch_3
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 4711
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4712
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->addHighlights(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4716
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    :sswitch_4
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;

    move-result-object v0

    .line 4717
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readGroup(ILcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4718
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->addLinks(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4722
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links$Builder;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setContent(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4726
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setFlags(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4730
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setSig(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4734
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setOrder(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4738
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setTitle(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4742
    :sswitch_a
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 4743
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->hasClipHighlight()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4744
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 4746
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4747
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setClipHighlight(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto :goto_0

    .line 4751
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    :sswitch_b
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    .line 4752
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->hasStructure()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4753
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    .line 4755
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4756
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setStructure(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4760
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setUf(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4764
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setH(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4768
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setW(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4772
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setVq(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4776
    :sswitch_10
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 4777
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4778
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->addClipHighlights(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4782
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setSnippetSrc(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4786
    :sswitch_12
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 4787
    .restart local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->hasCcBox()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4788
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 4790
    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4791
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setCcBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4795
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setTextSegment(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4799
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->setImageSolution(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    goto/16 :goto_0

    .line 4692
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x23 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x80 -> :sswitch_e
        0x92 -> :sswitch_f
        0xa2 -> :sswitch_10
        0xaa -> :sswitch_11
        0xb2 -> :sswitch_12
        0xb8 -> :sswitch_13
        0xca -> :sswitch_14
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4536
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4536
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeStructure(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 5222
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12900(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5224
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12900(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 5229
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5230
    return-object p0

    .line 5227
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    goto :goto_0
.end method

.method public setCcBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 5303
    if-nez p1, :cond_0

    .line 5304
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5306
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5307
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 5308
    return-object p0
.end method

.method public setClipHighlight(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 5121
    if-nez p1, :cond_0

    .line 5122
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5124
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5125
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 5126
    return-object p0
.end method

.method public setContent(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5049
    if-nez p1, :cond_0

    .line 5050
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5052
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasContent:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5053
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->content_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 5054
    return-object p0
.end method

.method public setFlags(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 4959
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasFlags:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4960
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->flags_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I

    .line 4961
    return-object p0
.end method

.method public setH(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 5267
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasH:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5268
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->h_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I

    .line 5269
    return-object p0
.end method

.method public setImageSolution(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4857
    if-nez p1, :cond_0

    .line 4858
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4860
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasImageSolution:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4861
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->imageSolution_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 4862
    return-object p0
.end method

.method public setOrder(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 4920
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasOrder:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4921
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->order_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I

    .line 4922
    return-object p0
.end method

.method public setPid(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4815
    if-nez p1, :cond_0

    .line 4816
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4818
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4819
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->pid_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 4820
    return-object p0
.end method

.method public setSig(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4899
    if-nez p1, :cond_0

    .line 4900
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4902
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSig:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4903
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->sig_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 4904
    return-object p0
.end method

.method public setSnippetSrc(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4878
    if-nez p1, :cond_0

    .line 4879
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4881
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSnippetSrc:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4882
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->snippetSrc_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 4883
    return-object p0
.end method

.method public setSrc(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4836
    if-nez p1, :cond_0

    .line 4837
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4839
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSrc:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4840
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->src_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$10902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 4841
    return-object p0
.end method

.method public setStructure(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 5209
    if-nez p1, :cond_0

    .line 5210
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5212
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5213
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 5214
    return-object p0
.end method

.method public setTextSegment(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 5340
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTextSegment:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5341
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->textSegment_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I

    .line 5342
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 4938
    if-nez p1, :cond_0

    .line 4939
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4941
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTitle:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 4942
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->title_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$11902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 4943
    return-object p0
.end method

.method public setUf(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5246
    if-nez p1, :cond_0

    .line 5247
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5249
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasUf:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5250
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->uf_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 5251
    return-object p0
.end method

.method public setVq(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 5028
    if-nez p1, :cond_0

    .line 5029
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5031
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasVq:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5032
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->vq_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$12302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;

    .line 5033
    return-object p0
.end method

.method public setW(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 5285
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasW:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z

    .line 5286
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->w_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->access$13502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I

    .line 5287
    return-object p0
.end method
