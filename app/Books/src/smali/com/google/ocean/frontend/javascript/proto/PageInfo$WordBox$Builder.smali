.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2427
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$5400()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 1

    .prologue
    .line 2421
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 3

    .prologue
    .line 2430
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;-><init>()V

    .line 2431
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 2432
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .locals 3

    .prologue
    .line 2476
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    if-nez v1, :cond_0

    .line 2477
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2480
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 2481
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .line 2482
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 2

    .prologue
    .line 2449
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2421
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2421
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2421
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 2569
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    return-object v0
.end method

.method public hasBox()Z
    .locals 1

    .prologue
    .line 2566
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox()Z

    move-result v0

    return v0
.end method

.method public mergeBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 2585
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5900(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2587
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5900(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 2592
    :goto_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z

    .line 2593
    return-object p0

    .line 2590
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    .prologue
    .line 2486
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2499
    :cond_0
    :goto_0
    return-object p0

    .line 2487
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasWord()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2488
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getWord()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setWord(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    .line 2490
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2491
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->mergeBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    .line 2493
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasFlowWithNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2494
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getFlowWithNext()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setFlowWithNext(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    .line 2496
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBaselineY()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2497
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBaselineY()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setBaselineY(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2507
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 2508
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 2512
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2513
    :sswitch_0
    return-object p0

    .line 2518
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setWord(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    goto :goto_0

    .line 2522
    :sswitch_2
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    .line 2523
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->hasBox()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2524
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    .line 2526
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2527
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    goto :goto_0

    .line 2531
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setFlowWithNext(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    goto :goto_0

    .line 2535
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->setBaselineY(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    goto :goto_0

    .line 2508
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2421
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2421
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setBaselineY(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 2627
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBaselineY:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$6202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z

    .line 2628
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->baselineY_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$6302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;I)I

    .line 2629
    return-object p0
.end method

.method public setBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 2572
    if-nez p1, :cond_0

    .line 2573
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2575
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasBox:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z

    .line 2576
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->box_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 2577
    return-object p0
.end method

.method public setFlowWithNext(Z)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 2609
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasFlowWithNext:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$6002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z

    .line 2610
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->flowWithNext_:Z
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$6102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z

    .line 2611
    return-object p0
.end method

.method public setWord(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 2551
    if-nez p1, :cond_0

    .line 2552
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2554
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->hasWord:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Z)Z

    .line 2555
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->word_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->access$5702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;Ljava/lang/String;)Ljava/lang/String;

    .line 2556
    return-object p0
.end method
