.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "JsonPages"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;


# instance fields
.field private availablePid_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private contentEncrypted_:Z

.field private content_:Lcom/google/protobuf/ByteString;

.field private currentPosition_:Ljava/lang/String;

.field private debug_:Ljava/lang/String;

.field private fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

.field private hasContent:Z

.field private hasContentEncrypted:Z

.field private hasCurrentPosition:Z

.field private hasDebug:Z

.field private hasFixedLayoutInfo:Z

.field private hasOfflineLicenseInfo:Z

.field private hasPrefix:Z

.field private hasReasonContentBlocked:Z

.field private hasStyle:Z

.field private hasVolumeVersion:Z

.field private memoizedSerializedSize:I

.field private offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

.field private page_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;"
        }
    .end annotation
.end field

.field private prefix_:Ljava/lang/String;

.field private reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

.field private resourceUrl_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private resource_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;",
            ">;"
        }
    .end annotation
.end field

.field private style_:Ljava/lang/String;

.field private volumeVersion_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6384
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .line 6385
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 6386
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->initFields()V

    .line 6387
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5365
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5381
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    .line 5394
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->prefix_:Ljava/lang/String;

    .line 5401
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->debug_:Ljava/lang/String;

    .line 5408
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->content_:Lcom/google/protobuf/ByteString;

    .line 5415
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->volumeVersion_:Ljava/lang/String;

    .line 5422
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->contentEncrypted_:Z

    .line 5443
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->style_:Ljava/lang/String;

    .line 5456
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;

    .line 5468
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;

    .line 5481
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->currentPosition_:Ljava/lang/String;

    .line 5487
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;

    .line 5559
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->memoizedSerializedSize:I

    .line 5366
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->initFields()V

    .line 5367
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 5362
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 5368
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5381
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    .line 5394
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->prefix_:Ljava/lang/String;

    .line 5401
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->debug_:Ljava/lang/String;

    .line 5408
    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->content_:Lcom/google/protobuf/ByteString;

    .line 5415
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->volumeVersion_:Ljava/lang/String;

    .line 5422
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->contentEncrypted_:Z

    .line 5443
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->style_:Ljava/lang/String;

    .line 5456
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;

    .line 5468
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;

    .line 5481
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->currentPosition_:Ljava/lang/String;

    .line 5487
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;

    .line 5559
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->memoizedSerializedSize:I

    .line 5368
    return-void
.end method

.method static synthetic access$14300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14600(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$14702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasPrefix:Z

    return p1
.end method

.method static synthetic access$14802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->prefix_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$14902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasDebug:Z

    return p1
.end method

.method static synthetic access$15002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->debug_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$15102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContent:Z

    return p1
.end method

.method static synthetic access$15202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Lcom/google/protobuf/ByteString;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->content_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$15302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasVolumeVersion:Z

    return p1
.end method

.method static synthetic access$15402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->volumeVersion_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$15502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContentEncrypted:Z

    return p1
.end method

.method static synthetic access$15602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->contentEncrypted_:Z

    return p1
.end method

.method static synthetic access$15702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked:Z

    return p1
.end method

.method static synthetic access$15800(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    return-object v0
.end method

.method static synthetic access$15802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    return-object p1
.end method

.method static synthetic access$15902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo:Z

    return p1
.end method

.method static synthetic access$16000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    return-object v0
.end method

.method static synthetic access$16002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    return-object p1
.end method

.method static synthetic access$16102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle:Z

    return p1
.end method

.method static synthetic access$16202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->style_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$16302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo:Z

    return p1
.end method

.method static synthetic access$16400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    .prologue
    .line 5362
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    return-object v0
.end method

.method static synthetic access$16402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    return-object p1
.end method

.method static synthetic access$16502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Z

    .prologue
    .line 5362
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasCurrentPosition:Z

    return p1
.end method

.method static synthetic access$16602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 5362
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->currentPosition_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .locals 1

    .prologue
    .line 5372
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 5498
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 5499
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    .line 5500
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 5501
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    .locals 1

    .prologue
    .line 5702
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->access$14100()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5660
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;

    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->buildParsed()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;->access$14000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages$Builder;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAvailablePidList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5490
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->availablePid_:Ljava/util/List;

    return-object v0
.end method

.method public getContent()Lcom/google/protobuf/ByteString;
    .locals 1

    .prologue
    .line 5410
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->content_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public getContentEncrypted()Z
    .locals 1

    .prologue
    .line 5424
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->contentEncrypted_:Z

    return v0
.end method

.method public getCurrentPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5483
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->currentPosition_:Ljava/lang/String;

    return-object v0
.end method

.method public getDebug()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5403
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->debug_:Ljava/lang/String;

    return-object v0
.end method

.method public getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .locals 1

    .prologue
    .line 5452
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->fixedLayoutInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    return-object v0
.end method

.method public getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;
    .locals 1

    .prologue
    .line 5438
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->offlineLicenseInfo_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    return-object v0
.end method

.method public getPage(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 5388
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 5386
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5384
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->page_:Ljava/util/List;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5396
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->prefix_:Ljava/lang/String;

    return-object v0
.end method

.method public getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .locals 1

    .prologue
    .line 5431
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->reasonContentBlocked_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    return-object v0
.end method

.method public getResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5471
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resource_:Ljava/util/List;

    return-object v0
.end method

.method public getResourceUrlList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5459
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->resourceUrl_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 5561
    iget v3, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->memoizedSerializedSize:I

    .line 5562
    .local v3, "size":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    .line 5632
    .end local v3    # "size":I
    .local v4, "size":I
    :goto_0
    return v4

    .line 5564
    .end local v4    # "size":I
    .restart local v3    # "size":I
    :cond_0
    const/4 v3, 0x0

    .line 5565
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPageList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 5566
    .local v1, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    const/4 v5, 0x1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5568
    goto :goto_1

    .line 5569
    .end local v1    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasDebug()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 5570
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getDebug()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5573
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasPrefix()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 5574
    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPrefix()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5577
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContent()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 5578
    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContent()Lcom/google/protobuf/ByteString;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5581
    :cond_4
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasCurrentPosition()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 5582
    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getCurrentPosition()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5586
    :cond_5
    const/4 v0, 0x0

    .line 5587
    .local v0, "dataSize":I
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getAvailablePidList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 5588
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 5590
    goto :goto_2

    .line 5591
    .end local v1    # "element":Ljava/lang/String;
    :cond_6
    add-int/2addr v3, v0

    .line 5592
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getAvailablePidList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 5594
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 5595
    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getStyle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5599
    :cond_7
    const/4 v0, 0x0

    .line 5600
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceUrlList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 5601
    .restart local v1    # "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v0, v5

    .line 5603
    goto :goto_3

    .line 5604
    .end local v1    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v3, v0

    .line 5605
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceUrlList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    .line 5607
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContentEncrypted()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 5608
    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContentEncrypted()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v3, v5

    .line 5611
    :cond_9
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 5612
    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5615
    :cond_a
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 5616
    .local v1, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    const/16 v5, 0xb

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5618
    goto :goto_4

    .line 5619
    .end local v1    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    :cond_b
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasVolumeVersion()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 5620
    const/16 v5, 0xc

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getVolumeVersion()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5623
    :cond_c
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 5624
    const/16 v5, 0xd

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5627
    :cond_d
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 5628
    const/16 v5, 0xe

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    .line 5631
    :cond_e
    iput v3, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->memoizedSerializedSize:I

    move v4, v3

    .line 5632
    .end local v3    # "size":I
    .restart local v4    # "size":I
    goto/16 :goto_0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5445
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->style_:Ljava/lang/String;

    return-object v0
.end method

.method public getVolumeVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5417
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->volumeVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public hasContent()Z
    .locals 1

    .prologue
    .line 5409
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContent:Z

    return v0
.end method

.method public hasContentEncrypted()Z
    .locals 1

    .prologue
    .line 5423
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContentEncrypted:Z

    return v0
.end method

.method public hasCurrentPosition()Z
    .locals 1

    .prologue
    .line 5482
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasCurrentPosition:Z

    return v0
.end method

.method public hasDebug()Z
    .locals 1

    .prologue
    .line 5402
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasDebug:Z

    return v0
.end method

.method public hasFixedLayoutInfo()Z
    .locals 1

    .prologue
    .line 5451
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo:Z

    return v0
.end method

.method public hasOfflineLicenseInfo()Z
    .locals 1

    .prologue
    .line 5437
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo:Z

    return v0
.end method

.method public hasPrefix()Z
    .locals 1

    .prologue
    .line 5395
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasPrefix:Z

    return v0
.end method

.method public hasReasonContentBlocked()Z
    .locals 1

    .prologue
    .line 5430
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked:Z

    return v0
.end method

.method public hasStyle()Z
    .locals 1

    .prologue
    .line 5444
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle:Z

    return v0
.end method

.method public hasVolumeVersion()Z
    .locals 1

    .prologue
    .line 5416
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasVolumeVersion:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 5503
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPageList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 5504
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 5509
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    :goto_0
    return v2

    .line 5506
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 5507
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 5509
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5514
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getSerializedSize()I

    .line 5515
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPageList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 5516
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 5518
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasDebug()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5519
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getDebug()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 5521
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasPrefix()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5522
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 5524
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5525
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContent()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5527
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasCurrentPosition()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5528
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getCurrentPosition()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 5530
    :cond_4
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getAvailablePidList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5531
    .local v0, "element":Ljava/lang/String;
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_1

    .line 5533
    .end local v0    # "element":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasStyle()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5534
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getStyle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 5536
    :cond_6
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceUrlList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5537
    .restart local v0    # "element":Ljava/lang/String;
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    goto :goto_2

    .line 5539
    .end local v0    # "element":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasContentEncrypted()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 5540
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getContentEncrypted()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 5542
    :cond_8
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasReasonContentBlocked()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 5543
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getReasonContentBlocked()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 5545
    :cond_9
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getResourceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;

    .line 5546
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_3

    .line 5548
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$Resource;
    :cond_a
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasVolumeVersion()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 5549
    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getVolumeVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 5551
    :cond_b
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasOfflineLicenseInfo()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 5552
    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getOfflineLicenseInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OfflineLicenseInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 5554
    :cond_c
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->hasFixedLayoutInfo()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 5555
    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPages;->getFixedLayoutInfo()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 5557
    :cond_d
    return-void
.end method
