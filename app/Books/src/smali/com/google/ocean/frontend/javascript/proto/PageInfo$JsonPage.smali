.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "JsonPage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;,
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;


# instance fields
.field private ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

.field private clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

.field private clipHighlights_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;",
            ">;"
        }
    .end annotation
.end field

.field private content_:Ljava/lang/String;

.field private flags_:I

.field private h_:I

.field private hasCcBox:Z

.field private hasClipHighlight:Z

.field private hasContent:Z

.field private hasFlags:Z

.field private hasH:Z

.field private hasImageSolution:Z

.field private hasOrder:Z

.field private hasPid:Z

.field private hasSig:Z

.field private hasSnippetSrc:Z

.field private hasSrc:Z

.field private hasStructure:Z

.field private hasTextSegment:Z

.field private hasTitle:Z

.field private hasUf:Z

.field private hasVq:Z

.field private hasW:Z

.field private highlights_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;",
            ">;"
        }
    .end annotation
.end field

.field private imageSolution_:Ljava/lang/String;

.field private links_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I

.field private order_:I

.field private pid_:Ljava/lang/String;

.field private sig_:Ljava/lang/String;

.field private snippetSrc_:Ljava/lang/String;

.field private src_:Ljava/lang/String;

.field private structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

.field private textSegment_:I

.field private title_:Ljava/lang/String;

.field private uf_:Ljava/lang/String;

.field private vq_:Ljava/lang/String;

.field private w_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5354
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 5355
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 5356
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->initFields()V

    .line 5357
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3709
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4127
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->pid_:Ljava/lang/String;

    .line 4134
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->src_:Ljava/lang/String;

    .line 4141
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->imageSolution_:Ljava/lang/String;

    .line 4148
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->snippetSrc_:Ljava/lang/String;

    .line 4155
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->sig_:Ljava/lang/String;

    .line 4162
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->order_:I

    .line 4169
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->title_:Ljava/lang/String;

    .line 4176
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->flags_:I

    .line 4182
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;

    .line 4195
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->vq_:Ljava/lang/String;

    .line 4202
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->content_:Ljava/lang/String;

    .line 4208
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;

    .line 4227
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;

    .line 4247
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->uf_:Ljava/lang/String;

    .line 4254
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->h_:I

    .line 4261
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->w_:I

    .line 4275
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->textSegment_:I

    .line 4372
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->memoizedSerializedSize:I

    .line 3710
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->initFields()V

    .line 3711
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 3706
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3712
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4127
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->pid_:Ljava/lang/String;

    .line 4134
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->src_:Ljava/lang/String;

    .line 4141
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->imageSolution_:Ljava/lang/String;

    .line 4148
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->snippetSrc_:Ljava/lang/String;

    .line 4155
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->sig_:Ljava/lang/String;

    .line 4162
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->order_:I

    .line 4169
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->title_:Ljava/lang/String;

    .line 4176
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->flags_:I

    .line 4182
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;

    .line 4195
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->vq_:Ljava/lang/String;

    .line 4202
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->content_:Ljava/lang/String;

    .line 4208
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;

    .line 4227
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;

    .line 4247
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->uf_:Ljava/lang/String;

    .line 4254
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->h_:I

    .line 4261
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->w_:I

    .line 4275
    iput v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->textSegment_:I

    .line 4372
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->memoizedSerializedSize:I

    .line 3712
    return-void
.end method

.method static synthetic access$10300(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 3706
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$10400(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 3706
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$10500(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 3706
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$10602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid:Z

    return p1
.end method

.method static synthetic access$10702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->pid_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$10802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSrc:Z

    return p1
.end method

.method static synthetic access$10902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->src_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$11002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasImageSolution:Z

    return p1
.end method

.method static synthetic access$11102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->imageSolution_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$11202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSnippetSrc:Z

    return p1
.end method

.method static synthetic access$11302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->snippetSrc_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$11402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSig:Z

    return p1
.end method

.method static synthetic access$11502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->sig_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$11602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasOrder:Z

    return p1
.end method

.method static synthetic access$11702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # I

    .prologue
    .line 3706
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->order_:I

    return p1
.end method

.method static synthetic access$11802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTitle:Z

    return p1
.end method

.method static synthetic access$11902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->title_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$12002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasFlags:Z

    return p1
.end method

.method static synthetic access$12102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # I

    .prologue
    .line 3706
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->flags_:I

    return p1
.end method

.method static synthetic access$12202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasVq:Z

    return p1
.end method

.method static synthetic access$12302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->vq_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$12402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasContent:Z

    return p1
.end method

.method static synthetic access$12502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->content_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$12602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight:Z

    return p1
.end method

.method static synthetic access$12700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 3706
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method static synthetic access$12702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object p1
.end method

.method static synthetic access$12802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure:Z

    return p1
.end method

.method static synthetic access$12900(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 3706
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    return-object v0
.end method

.method static synthetic access$12902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    return-object p1
.end method

.method static synthetic access$13002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasUf:Z

    return p1
.end method

.method static synthetic access$13102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->uf_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$13202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasH:Z

    return p1
.end method

.method static synthetic access$13302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # I

    .prologue
    .line 3706
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->h_:I

    return p1
.end method

.method static synthetic access$13402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasW:Z

    return p1
.end method

.method static synthetic access$13502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # I

    .prologue
    .line 3706
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->w_:I

    return p1
.end method

.method static synthetic access$13602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox:Z

    return p1
.end method

.method static synthetic access$13700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 3706
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method static synthetic access$13702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 3706
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object p1
.end method

.method static synthetic access$13802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # Z

    .prologue
    .line 3706
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTextSegment:Z

    return p1
.end method

.method static synthetic access$13902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "x1"    # I

    .prologue
    .line 3706
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->textSegment_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .locals 1

    .prologue
    .line 3716
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 4280
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4281
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 4282
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4283
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    .locals 1

    .prologue
    .line 4529
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;->access$10100()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 4270
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->ccBox_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method public getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 4223
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlight_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method public getClipHighlightsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4230
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->clipHighlights_:Ljava/util/List;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4204
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->content_:Ljava/lang/String;

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 4178
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->flags_:I

    return v0
.end method

.method public getH()I
    .locals 1

    .prologue
    .line 4256
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->h_:I

    return v0
.end method

.method public getHighlightsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4185
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->highlights_:Ljava/util/List;

    return-object v0
.end method

.method public getImageSolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4143
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->imageSolution_:Ljava/lang/String;

    return-object v0
.end method

.method public getLinksList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4211
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->links_:Ljava/util/List;

    return-object v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 4164
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->order_:I

    return v0
.end method

.method public getPid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4129
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->pid_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 4374
    iget v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->memoizedSerializedSize:I

    .line 4375
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 4459
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 4377
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 4378
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4379
    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getPid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4382
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSrc()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4383
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSrc()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4386
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getHighlightsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4387
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4389
    goto :goto_1

    .line 4390
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getLinksList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 4391
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeGroupSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4393
    goto :goto_2

    .line 4394
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    :cond_4
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasContent()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4395
    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getContent()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4398
    :cond_5
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasFlags()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 4399
    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getFlags()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4402
    :cond_6
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSig()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 4403
    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSig()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4406
    :cond_7
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasOrder()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 4407
    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getOrder()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4410
    :cond_8
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 4411
    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4414
    :cond_9
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 4415
    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4418
    :cond_a
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 4419
    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4422
    :cond_b
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasUf()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 4423
    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getUf()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4426
    :cond_c
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasH()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 4427
    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getH()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4430
    :cond_d
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasW()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 4431
    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getW()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4434
    :cond_e
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasVq()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 4435
    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getVq()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4438
    :cond_f
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlightsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4439
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    const/16 v4, 0x14

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4441
    goto :goto_3

    .line 4442
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_10
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSnippetSrc()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 4443
    const/16 v4, 0x15

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSnippetSrc()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4446
    :cond_11
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 4447
    const/16 v4, 0x16

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4450
    :cond_12
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTextSegment()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 4451
    const/16 v4, 0x17

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getTextSegment()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 4454
    :cond_13
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasImageSolution()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 4455
    const/16 v4, 0x19

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getImageSolution()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 4458
    :cond_14
    iput v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->memoizedSerializedSize:I

    move v3, v2

    .line 4459
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto/16 :goto_0
.end method

.method public getSig()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4157
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->sig_:Ljava/lang/String;

    return-object v0
.end method

.method public getSnippetSrc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4150
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->snippetSrc_:Ljava/lang/String;

    return-object v0
.end method

.method public getSrc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4136
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->src_:Ljava/lang/String;

    return-object v0
.end method

.method public getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1

    .prologue
    .line 4242
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->structure_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    return-object v0
.end method

.method public getTextSegment()I
    .locals 1

    .prologue
    .line 4277
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->textSegment_:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4171
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getUf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4249
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->uf_:Ljava/lang/String;

    return-object v0
.end method

.method public getVq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4197
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->vq_:Ljava/lang/String;

    return-object v0
.end method

.method public getW()I
    .locals 1

    .prologue
    .line 4263
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->w_:I

    return v0
.end method

.method public hasCcBox()Z
    .locals 1

    .prologue
    .line 4269
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox:Z

    return v0
.end method

.method public hasClipHighlight()Z
    .locals 1

    .prologue
    .line 4222
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight:Z

    return v0
.end method

.method public hasContent()Z
    .locals 1

    .prologue
    .line 4203
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasContent:Z

    return v0
.end method

.method public hasFlags()Z
    .locals 1

    .prologue
    .line 4177
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasFlags:Z

    return v0
.end method

.method public hasH()Z
    .locals 1

    .prologue
    .line 4255
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasH:Z

    return v0
.end method

.method public hasImageSolution()Z
    .locals 1

    .prologue
    .line 4142
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasImageSolution:Z

    return v0
.end method

.method public hasOrder()Z
    .locals 1

    .prologue
    .line 4163
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasOrder:Z

    return v0
.end method

.method public hasPid()Z
    .locals 1

    .prologue
    .line 4128
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid:Z

    return v0
.end method

.method public hasSig()Z
    .locals 1

    .prologue
    .line 4156
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSig:Z

    return v0
.end method

.method public hasSnippetSrc()Z
    .locals 1

    .prologue
    .line 4149
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSnippetSrc:Z

    return v0
.end method

.method public hasSrc()Z
    .locals 1

    .prologue
    .line 4135
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSrc:Z

    return v0
.end method

.method public hasStructure()Z
    .locals 1

    .prologue
    .line 4241
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure:Z

    return v0
.end method

.method public hasTextSegment()Z
    .locals 1

    .prologue
    .line 4276
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTextSegment:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    .prologue
    .line 4170
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTitle:Z

    return v0
.end method

.method public hasUf()Z
    .locals 1

    .prologue
    .line 4248
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasUf:Z

    return v0
.end method

.method public hasVq()Z
    .locals 1

    .prologue
    .line 4196
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasVq:Z

    return v0
.end method

.method public hasW()Z
    .locals 1

    .prologue
    .line 4262
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasW:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 4285
    iget-boolean v3, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid:Z

    if-nez v3, :cond_1

    .line 4304
    :cond_0
    :goto_0
    return v2

    .line 4286
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getHighlightsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4287
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 4289
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getLinksList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 4290
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    .line 4292
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    :cond_5
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 4293
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4295
    :cond_6
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlightsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4296
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_7

    goto :goto_0

    .line 4298
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_8
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 4299
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4301
    :cond_9
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 4302
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4304
    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4309
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSerializedSize()I

    .line 4310
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasPid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4311
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getPid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4313
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSrc()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4314
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSrc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4316
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getHighlightsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4317
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 4319
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getLinksList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;

    .line 4320
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeGroup(ILcom/google/protobuf/MessageLite;)V

    goto :goto_1

    .line 4322
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage$Links;
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4323
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getContent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4325
    :cond_4
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasFlags()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 4326
    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getFlags()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 4328
    :cond_5
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSig()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4329
    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSig()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4331
    :cond_6
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasOrder()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 4332
    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getOrder()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 4334
    :cond_7
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4335
    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4337
    :cond_8
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasClipHighlight()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 4338
    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlight()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 4340
    :cond_9
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasStructure()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 4341
    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 4343
    :cond_a
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasUf()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 4344
    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getUf()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4346
    :cond_b
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasH()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 4347
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getH()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 4349
    :cond_c
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasW()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 4350
    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getW()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 4352
    :cond_d
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasVq()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 4353
    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getVq()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4355
    :cond_e
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getClipHighlightsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 4356
    .local v0, "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    const/16 v2, 0x14

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_2

    .line 4358
    .end local v0    # "element":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_f
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasSnippetSrc()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 4359
    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getSnippetSrc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4361
    :cond_10
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasCcBox()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 4362
    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getCcBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 4364
    :cond_11
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasTextSegment()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 4365
    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getTextSegment()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 4367
    :cond_12
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->hasImageSolution()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 4368
    const/16 v2, 0x19

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getImageSolution()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 4370
    :cond_13
    return-void
.end method
