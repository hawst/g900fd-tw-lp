.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FixedLayoutInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;,
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;


# instance fields
.field private hasViewport:Z

.field private memoizedSerializedSize:I

.field private viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 694
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .line 695
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 696
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->initFields()V

    .line 697
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 464
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 464
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$1602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .locals 1
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 450
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 451
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 1

    .prologue
    .line 545
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->access$1400()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;

    .prologue
    .line 548
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 466
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->memoizedSerializedSize:I

    .line 467
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 475
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 469
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 470
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 471
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 474
    :cond_1
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->memoizedSerializedSize:I

    move v1, v0

    .line 475
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->viewport_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    return-object v0
.end method

.method public hasViewport()Z
    .locals 1

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport:Z

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getSerializedSize()I

    .line 459
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->hasViewport()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo;->getViewport()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 462
    :cond_0
    return-void
.end method
