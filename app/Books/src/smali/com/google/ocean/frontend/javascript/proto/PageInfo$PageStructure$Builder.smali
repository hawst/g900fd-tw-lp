.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2062
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$4800()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 1

    .prologue
    .line 2056
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 3

    .prologue
    .line 2065
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;-><init>()V

    .line 2066
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 2067
    return-object v0
.end method


# virtual methods
.method public addBlock(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    .prologue
    .line 2190
    if-nez p1, :cond_0

    .line 2191
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2193
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2194
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Ljava/util/List;)Ljava/util/List;

    .line 2196
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2197
    return-object p0
.end method

.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 3

    .prologue
    .line 2111
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    if-nez v1, :cond_0

    .line 2112
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2115
    :cond_0
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 2116
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    iget-object v2, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Ljava/util/List;)Ljava/util/List;

    .line 2119
    :cond_1
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 2120
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 2121
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 2

    .prologue
    .line 2084
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2056
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2056
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2056
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 2125
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2135
    :cond_0
    :goto_0
    return-object p0

    .line 2126
    :cond_1
    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2127
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2128
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Ljava/util/List;)Ljava/util/List;

    .line 2130
    :cond_2
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->block_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5000(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2132
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->hasFirstWordOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2133
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getFirstWordOffset()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->setFirstWordOffset(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2143
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 2144
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 2148
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2149
    :sswitch_0
    return-object p0

    .line 2154
    :sswitch_1
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;

    move-result-object v0

    .line 2155
    .local v0, "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readGroup(ILcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2156
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;->buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->addBlock(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    goto :goto_0

    .line 2160
    .end local v0    # "subBuilder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Builder;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->setFirstWordOffset(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    goto :goto_0

    .line 2144
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x30 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2056
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2056
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setFirstWordOffset(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 2227
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->hasFirstWordOffset:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Z)Z

    .line 2228
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->firstWordOffset_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->access$5202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;I)I

    .line 2229
    return-object p0
.end method
