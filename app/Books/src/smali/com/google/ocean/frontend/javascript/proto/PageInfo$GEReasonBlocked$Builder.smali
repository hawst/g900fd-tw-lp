.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2873
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$6500()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 1

    .prologue
    .line 2867
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 3

    .prologue
    .line 2876
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;-><init>()V

    .line 2877
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 2878
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .locals 3

    .prologue
    .line 2922
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    if-nez v1, :cond_0

    .line 2923
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2926
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 2927
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 2928
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 2

    .prologue
    .line 2895
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2867
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .prologue
    .line 2932
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 2945
    :cond_0
    :goto_0
    return-object p0

    .line 2933
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasReason()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2934
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getReason()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setReason(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    .line 2936
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedOfflineDevices()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2937
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getNumAllowedOfflineDevices()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setNumAllowedOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    .line 2939
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedConcurrentAccesses()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2940
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getNumAllowedConcurrentAccesses()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setNumAllowedConcurrentAccesses(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    .line 2942
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasOrderUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2943
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getOrderUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setOrderUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2953
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 2954
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 2958
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2959
    :sswitch_0
    return-object p0

    .line 2964
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 2965
    .local v0, "rawValue":I
    invoke-static {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;->valueOf(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    move-result-object v2

    .line 2966
    .local v2, "value":Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;
    if-eqz v2, :cond_0

    .line 2967
    invoke-virtual {p0, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setReason(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    goto :goto_0

    .line 2972
    .end local v0    # "rawValue":I
    .end local v2    # "value":Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setNumAllowedOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    goto :goto_0

    .line 2976
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setNumAllowedConcurrentAccesses(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    goto :goto_0

    .line 2980
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->setOrderUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    goto :goto_0

    .line 2954
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2867
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2867
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setNumAllowedConcurrentAccesses(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 3035
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedConcurrentAccesses:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$7102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z

    .line 3036
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedConcurrentAccesses_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$7202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;I)I

    .line 3037
    return-object p0
.end method

.method public setNumAllowedOfflineDevices(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 3017
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedOfflineDevices:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$6902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z

    .line 3018
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedOfflineDevices_:I
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$7002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;I)I

    .line 3019
    return-object p0
.end method

.method public setOrderUrl(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 3053
    if-nez p1, :cond_0

    .line 3054
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3056
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasOrderUrl:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$7302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z

    .line 3057
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->orderUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$7402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Ljava/lang/String;)Ljava/lang/String;

    .line 3058
    return-object p0
.end method

.method public setReason(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    .prologue
    .line 2996
    if-nez p1, :cond_0

    .line 2997
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2999
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasReason:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$6702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z

    .line 3000
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->reason_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->access$6802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    .line 3001
    return-object p0
.end method
