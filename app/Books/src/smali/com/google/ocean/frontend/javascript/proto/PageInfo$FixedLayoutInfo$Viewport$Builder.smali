.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 1

    .prologue
    .line 210
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 3

    .prologue
    .line 219
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;-><init>()V

    .line 220
    .local v0, "builder":Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    new-instance v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;-><init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V

    iput-object v1, v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 221
    return-object v0
.end method


# virtual methods
.method public buildPartial()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    .locals 3

    .prologue
    .line 265
    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    if-nez v1, :cond_0

    .line 266
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 270
    .local v0, "returnMe":Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .line 271
    return-object v0
.end method

.method public clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->clone()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    .prologue
    .line 275
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 291
    :cond_0
    :goto_0
    return-object p0

    .line 276
    :cond_1
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 277
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getWidth()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setWidth(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    .line 279
    :cond_2
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 280
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getHeight()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setHeight(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    .line 282
    :cond_3
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasInitialScale()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 283
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getInitialScale()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setInitialScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    .line 285
    :cond_4
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMinimumScale()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 286
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getMinimumScale()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setMinimumScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    .line 288
    :cond_5
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMaximumScale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->getMaximumScale()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setMaximumScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 300
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 304
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 305
    :sswitch_0
    return-object p0

    .line 310
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setWidth(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    goto :goto_0

    .line 314
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setHeight(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    goto :goto_0

    .line 318
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setInitialScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    goto :goto_0

    .line 322
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setMinimumScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    goto :goto_0

    .line 326
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->setMaximumScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    goto :goto_0

    .line 300
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0, p1, p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setHeight(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 363
    if-nez p1, :cond_0

    .line 364
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasHeight:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z

    .line 367
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->height_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Ljava/lang/String;)Ljava/lang/String;

    .line 368
    return-object p0
.end method

.method public setInitialScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasInitialScale:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z

    .line 385
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->initialScale_:F
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;F)F

    .line 386
    return-object p0
.end method

.method public setMaximumScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMaximumScale:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$1102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z

    .line 421
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->maximumScale_:F
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$1202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;F)F

    .line 422
    return-object p0
.end method

.method public setMinimumScale(F)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasMinimumScale:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z

    .line 403
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->minimumScale_:F
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$1002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;F)F

    .line 404
    return-object p0
.end method

.method public setWidth(Ljava/lang/String;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 342
    if-nez p1, :cond_0

    .line 343
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    const/4 v1, 0x1

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->hasWidth:Z
    invoke-static {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Z)Z

    .line 346
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport$Builder;->result:Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;

    # setter for: Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->width_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;->access$402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$FixedLayoutInfo$Viewport;Ljava/lang/String;)Ljava/lang/String;

    .line 347
    return-object p0
.end method
