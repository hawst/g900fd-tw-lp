.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GEReasonBlocked"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;,
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;


# instance fields
.field private hasNumAllowedConcurrentAccesses:Z

.field private hasNumAllowedOfflineDevices:Z

.field private hasOrderUrl:Z

.field private hasReason:Z

.field private memoizedSerializedSize:I

.field private numAllowedConcurrentAccesses_:I

.field private numAllowedOfflineDevices_:I

.field private orderUrl_:Ljava/lang/String;

.field private reason_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3070
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .line 3071
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 3072
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->initFields()V

    .line 3073
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2652
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2725
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedOfflineDevices_:I

    .line 2732
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedConcurrentAccesses_:I

    .line 2739
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->orderUrl_:Ljava/lang/String;

    .line 2767
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->memoizedSerializedSize:I

    .line 2653
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->initFields()V

    .line 2654
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 2649
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 2655
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2725
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedOfflineDevices_:I

    .line 2732
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedConcurrentAccesses_:I

    .line 2739
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->orderUrl_:Ljava/lang/String;

    .line 2767
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->memoizedSerializedSize:I

    .line 2655
    return-void
.end method

.method static synthetic access$6702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # Z

    .prologue
    .line 2649
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasReason:Z

    return p1
.end method

.method static synthetic access$6802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    .prologue
    .line 2649
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->reason_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    return-object p1
.end method

.method static synthetic access$6902(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # Z

    .prologue
    .line 2649
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedOfflineDevices:Z

    return p1
.end method

.method static synthetic access$7002(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # I

    .prologue
    .line 2649
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedOfflineDevices_:I

    return p1
.end method

.method static synthetic access$7102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # Z

    .prologue
    .line 2649
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedConcurrentAccesses:Z

    return p1
.end method

.method static synthetic access$7202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # I

    .prologue
    .line 2649
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedConcurrentAccesses_:I

    return p1
.end method

.method static synthetic access$7302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # Z

    .prologue
    .line 2649
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasOrderUrl:Z

    return p1
.end method

.method static synthetic access$7402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 2649
    iput-object p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->orderUrl_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;
    .locals 1

    .prologue
    .line 2659
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 2744
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;->NOT_BLOCKED:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    iput-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->reason_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    .line 2745
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 1

    .prologue
    .line 2860
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->access$6500()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;

    .prologue
    .line 2863
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getNumAllowedConcurrentAccesses()I
    .locals 1

    .prologue
    .line 2734
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedConcurrentAccesses_:I

    return v0
.end method

.method public getNumAllowedOfflineDevices()I
    .locals 1

    .prologue
    .line 2727
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->numAllowedOfflineDevices_:I

    return v0
.end method

.method public getOrderUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->orderUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;
    .locals 1

    .prologue
    .line 2720
    iget-object v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->reason_:Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 2769
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->memoizedSerializedSize:I

    .line 2770
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 2790
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 2772
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 2773
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasReason()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2774
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getReason()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2777
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedOfflineDevices()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2778
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getNumAllowedOfflineDevices()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2781
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedConcurrentAccesses()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2782
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getNumAllowedConcurrentAccesses()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2785
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasOrderUrl()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2786
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getOrderUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2789
    :cond_4
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->memoizedSerializedSize:I

    move v1, v0

    .line 2790
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public hasNumAllowedConcurrentAccesses()Z
    .locals 1

    .prologue
    .line 2733
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedConcurrentAccesses:Z

    return v0
.end method

.method public hasNumAllowedOfflineDevices()Z
    .locals 1

    .prologue
    .line 2726
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedOfflineDevices:Z

    return v0
.end method

.method public hasOrderUrl()Z
    .locals 1

    .prologue
    .line 2740
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasOrderUrl:Z

    return v0
.end method

.method public hasReason()Z
    .locals 1

    .prologue
    .line 2719
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasReason:Z

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2752
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getSerializedSize()I

    .line 2753
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasReason()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2754
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getReason()Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked$ReasonBlocked;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 2756
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedOfflineDevices()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2757
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getNumAllowedOfflineDevices()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2759
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasNumAllowedConcurrentAccesses()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2760
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getNumAllowedConcurrentAccesses()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2762
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->hasOrderUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2763
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$GEReasonBlocked;->getOrderUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 2765
    :cond_3
    return-void
.end method
