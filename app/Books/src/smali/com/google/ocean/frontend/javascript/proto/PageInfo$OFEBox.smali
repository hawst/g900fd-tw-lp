.class public final Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "PageInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ocean/frontend/javascript/proto/PageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OFEBox"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;


# instance fields
.field private h_:I

.field private hasH:Z

.field private hasW:Z

.field private hasX:Z

.field private hasY:Z

.field private memoizedSerializedSize:I

.field private w_:I

.field private x_:I

.field private y_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1067
    new-instance v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;-><init>(Z)V

    sput-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .line 1068
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo;->internalForceInit()V

    .line 1069
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    invoke-direct {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->initFields()V

    .line 1070
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 705
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 722
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->x_:I

    .line 729
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->y_:I

    .line 736
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->w_:I

    .line 743
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->h_:I

    .line 774
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->memoizedSerializedSize:I

    .line 706
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->initFields()V

    .line 707
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$1;

    .prologue
    .line 702
    invoke-direct {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    const/4 v0, 0x0

    .line 708
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 722
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->x_:I

    .line 729
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->y_:I

    .line 736
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->w_:I

    .line 743
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->h_:I

    .line 774
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->memoizedSerializedSize:I

    .line 708
    return-void
.end method

.method static synthetic access$2102(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # Z

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX:Z

    return p1
.end method

.method static synthetic access$2202(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # I

    .prologue
    .line 702
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->x_:I

    return p1
.end method

.method static synthetic access$2302(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # Z

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # I

    .prologue
    .line 702
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->y_:I

    return p1
.end method

.method static synthetic access$2502(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # Z

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW:Z

    return p1
.end method

.method static synthetic access$2602(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # I

    .prologue
    .line 702
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->w_:I

    return p1
.end method

.method static synthetic access$2702(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # Z

    .prologue
    .line 702
    iput-boolean p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH:Z

    return p1
.end method

.method static synthetic access$2802(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x1"    # I

    .prologue
    .line 702
    iput p1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->h_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .locals 1

    .prologue
    .line 712
    sget-object v0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->defaultInstance:Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public static newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 1

    .prologue
    .line 867
    # invokes: Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->create()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->access$1900()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;
    .locals 1
    .param p0, "prototype"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 870
    invoke-static {}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->newBuilder()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;->mergeFrom(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getH()I
    .locals 1

    .prologue
    .line 745
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->h_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 776
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->memoizedSerializedSize:I

    .line 777
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 797
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 779
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 780
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 781
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 784
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 785
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getY()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 788
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 789
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 792
    :cond_3
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 793
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 796
    :cond_4
    iput v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->memoizedSerializedSize:I

    move v1, v0

    .line 797
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getW()I
    .locals 1

    .prologue
    .line 738
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->w_:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 724
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->x_:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 731
    iget v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->y_:I

    return v0
.end method

.method public hasH()Z
    .locals 1

    .prologue
    .line 744
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH:Z

    return v0
.end method

.method public hasW()Z
    .locals 1

    .prologue
    .line 737
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW:Z

    return v0
.end method

.method public hasX()Z
    .locals 1

    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX:Z

    return v0
.end method

.method public hasY()Z
    .locals 1

    .prologue
    .line 730
    iget-boolean v0, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 750
    iget-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX:Z

    if-nez v1, :cond_1

    .line 754
    :cond_0
    :goto_0
    return v0

    .line 751
    :cond_1
    iget-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY:Z

    if-eqz v1, :cond_0

    .line 752
    iget-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW:Z

    if-eqz v1, :cond_0

    .line 753
    iget-boolean v1, p0, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH:Z

    if-eqz v1, :cond_0

    .line 754
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 759
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getSerializedSize()I

    .line 760
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasX()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 761
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 763
    :cond_0
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasY()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 764
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 766
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasW()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 767
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 769
    :cond_2
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->hasH()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 770
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 772
    :cond_3
    return-void
.end method
