.class public interface abstract Lcom/google/android/nfcprovision/ISchoolOwnedService;
.super Ljava/lang/Object;
.source "ISchoolOwnedService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/nfcprovision/ISchoolOwnedService$Stub;
    }
.end annotation


# virtual methods
.method public abstract isSchoolOwned()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
