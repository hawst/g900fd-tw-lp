.class public Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/AppDataSearchClient$1;,
        Lcom/google/android/gms/appdatasearch/AppDataSearchClient$b;,
        Lcom/google/android/gms/appdatasearch/AppDataSearchClient$a;
    }
.end annotation


# instance fields
.field private final BM:Landroid/os/ConditionVariable;

.field private BN:Lcom/google/android/gms/common/ConnectionResult;

.field private final BO:Lcom/google/android/gms/internal/hi;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BM:Landroid/os/ConditionVariable;

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/internal/hi;

    new-instance v1, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$a;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$a;-><init>(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;Lcom/google/android/gms/appdatasearch/AppDataSearchClient$1;)V

    new-instance v2, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$b;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient$b;-><init>(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;Lcom/google/android/gms/appdatasearch/AppDataSearchClient$1;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/internal/hi;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;)Landroid/os/ConditionVariable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BM:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/appdatasearch/AppDataSearchClient;Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/ConnectionResult;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Lcom/google/android/gms/common/ConnectionResult;

    return-object p1
.end method

.method private f(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    const-string v1, "com.google.android.gms"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    return-void
.end method

.method public static verifyContentProviderClient(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-ne v0, v1, :cond_1

    const-string v0, "AppDataSearchClient"

    const-string v1, "verifyContentProviderClient: caller is current process"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.gms"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v1, v0, :cond_2

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calling UID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not Google Play Services."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Google Play Services not installed"

    invoke-direct {v1, v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calling package problem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public connect()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BM:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->connect()V

    return-void
.end method

.method public connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;
    .locals 3
    .param p1, "timeout"    # J

    .prologue
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connect()V

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BM:Landroid/os/ConditionVariable;

    invoke-virtual {v0, p1, p2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->disconnect()V

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BN:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/common/ConnectionResult;->LP:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0
.end method

.method public disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->disconnect()V

    return-void
.end method

.method public getCorpusStatus(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 3
    .param p1, "corpusName"    # Ljava/lang/String;

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/internal/hj;->m(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "Get corpus status failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->isConnected()Z

    move-result v0

    return v0
.end method

.method public queryGlobalSearch(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "numResults"    # I
    .param p4, "spec"    # Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/hj;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "Query failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerCorpus(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
    .locals 3
    .param p1, "corpusInfo"    # Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    .prologue
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->contentProviderUri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->f(Landroid/net/Uri;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/internal/hj;->b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "Register corpus failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerGlobalSearchApplication(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Z
    .locals 3
    .param p1, "appInfo"    # Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->aq(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object p1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/hj;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "Register UniversalSearchableAppInfo failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestIndexing(Ljava/lang/String;J)Z
    .locals 8
    .param p1, "corpusName"    # Ljava/lang/String;
    .param p2, "sequenceNumber"    # J

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/internal/hj;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "Request indexing failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRegisteredCorpora(Ljava/util/Collection;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "corpora":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;>;"
    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/internal/hj;->at(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    new-instance v4, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->name:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "AppDataSearchClient"

    const-string v3, "Getting corpora failed."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v2, v1

    :cond_0
    return v2

    :cond_1
    const/4 v0, 0x1

    array-length v5, v3

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v3, v2

    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0, v6}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->unregisterCorpus(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    move v0, v1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->registerCorpus(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    move v2, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3
.end method

.method public unregisterCorpus(Ljava/lang/String;)Z
    .locals 9
    .param p1, "corpusName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->BO:Lcom/google/android/gms/internal/hi;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/hi;->getSearchService()Lcom/google/android/gms/internal/hj;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Lcom/google/android/gms/internal/hj;->n(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    const-string v2, "content_provider_uris"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "success"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    move v2, v0

    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_2

    aget-boolean v4, v3, v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_1

    :goto_2
    return v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "AppDataSearchClient"

    const-string v3, "Unregister corpus failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_2
.end method
