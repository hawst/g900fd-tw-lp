.class public abstract Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelper;
.super Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;
.source "AppDataSearchDbOpenHelper.java"

# interfaces
.implements Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mTableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I
    .param p5, "tableStorageSpecs"    # [Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    .param p6, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 37
    invoke-direct/range {p0 .. p6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V

    .line 38
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelper;->mTableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelper;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v0

    return-object v0
.end method

.method public getTableMappings()[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelper;->mTableStorageSpecs:[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    return-object v0
.end method
