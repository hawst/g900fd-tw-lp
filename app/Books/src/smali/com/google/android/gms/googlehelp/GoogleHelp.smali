.class public final Lcom/google/android/gms/googlehelp/GoogleHelp;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/googlehelp/GoogleHelp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field alA:Z

.field alB:Ljava/lang/String;

.field alC:Ljava/lang/String;

.field alD:Landroid/net/Uri;

.field alE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field alF:I

.field alG:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/OfflineSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field aln:Ljava/lang/String;

.field alo:Landroid/accounts/Account;

.field alp:Landroid/os/Bundle;

.field alq:Z

.field alr:Z

.field als:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field alt:Ljava/lang/String;

.field alu:Ljava/lang/String;

.field alv:Landroid/os/Bundle;

.field alw:Landroid/graphics/Bitmap;

.field alx:[B

.field aly:I

.field alz:I

.field final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/googlehelp/a;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIIZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;)V
    .locals 1
    .param p1, "versionCode"    # I
    .param p2, "helpCenterContext"    # Ljava/lang/String;
    .param p3, "googleAccount"    # Landroid/accounts/Account;
    .param p4, "psdBundle"    # Landroid/os/Bundle;
    .param p5, "searchEnabled"    # Z
    .param p6, "metricsReportingEnabled"    # Z
    .param p8, "sendToEmailAddress"    # Ljava/lang/String;
    .param p9, "emailSubject"    # Ljava/lang/String;
    .param p10, "feedbackPsdBundle"    # Landroid/os/Bundle;
    .param p11, "backupScreenshot"    # Landroid/graphics/Bitmap;
    .param p12, "screenshotBytes"    # [B
    .param p13, "screenshotWidth"    # I
    .param p14, "screenshotHeight"    # I
    .param p15, "supportContentApiAuthEnabled"    # Z
    .param p16, "apiKey"    # Ljava/lang/String;
    .param p17, "apiDebugOption"    # Ljava/lang/String;
    .param p18, "fallbackSupportUri"    # Landroid/net/Uri;
    .param p20, "helpActivityTheme"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/accounts/Account;",
            "Landroid/os/Bundle;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Landroid/graphics/Bitmap;",
            "[BIIZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/OfflineSuggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "supportPhoneNumbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p19, "overflowMenuItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;>;"
    .local p21, "offlineSuggestions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/googlehelp/OfflineSuggestion;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aln:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alo:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alp:Landroid/os/Bundle;

    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alq:Z

    iput-boolean p6, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alr:Z

    iput-object p7, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->als:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alt:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alu:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alv:Landroid/os/Bundle;

    iput-object p11, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alw:Landroid/graphics/Bitmap;

    iput-object p12, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alx:[B

    iput p13, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aly:I

    iput p14, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alz:I

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alA:Z

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alB:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alC:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alD:Landroid/net/Uri;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alE:Ljava/util/List;

    move/from16 v0, p20

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alF:I

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alG:Ljava/util/List;

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Z)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 4

    const/4 v3, 0x0

    const/high16 v2, 0x40000

    const/16 v0, 0x3c

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/f;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    array-length v1, v0

    if-gt v1, v2, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alx:[B

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aly:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alz:I

    iput-object v3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alw:Landroid/graphics/Bitmap;

    :goto_0
    return-object p0

    :cond_0
    if-eqz v0, :cond_1

    array-length v0, v0

    if-le v0, v2, :cond_1

    if-nez p2, :cond_1

    const/high16 v0, 0x100000

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->b(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;Z)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object p0

    goto :goto_0

    :cond_1
    const-string v0, "GOOGLEHELP_GoogleHelp"

    const-string v1, "The bytes of the compressed jpeg is too large."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alx:[B

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->c(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alw:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static b(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 6

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-double v2, v2

    int-to-double v4, p1

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v0

    double-to-int v2, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-double v4, v3

    div-double v0, v4, v0

    double-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {p0, v2, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static c(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v1, v4

    int-to-double v4, v1

    const-wide/high16 v6, 0x4110000000000000L    # 262144.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v4, v2

    double-to-int v1, v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-double v4, v4

    div-double v2, v4, v2

    double-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 22
    .param p0, "helpcenterContext"    # Ljava/lang/String;

    .prologue
    new-instance v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    const/16 v20, 0x0

    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v2, p0

    invoke-direct/range {v0 .. v21}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIIZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public addAdditionalOverflowMenuItem(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 2
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alE:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;-><init>(ILjava/lang/String;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public buildHelpIntent()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->buildHelpIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getFallbackSupportUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alD:Landroid/net/Uri;

    return-object v0
.end method

.method public setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "fallbackSupportUri"    # Landroid/net/Uri;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alD:Landroid/net/Uri;

    return-object p0
.end method

.method public setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "googleAccount"    # Landroid/accounts/Account;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alo:Landroid/accounts/Account;

    return-object p0
.end method

.method public setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 1
    .param p1, "screenshot"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    if-nez p1, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alw:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->alx:[B

    .end local p0    # "this":Lcom/google/android/gms/googlehelp/GoogleHelp;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/google/android/gms/googlehelp/GoogleHelp;
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;Z)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object p0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/a;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/os/Parcel;I)V

    return-void
.end method
