.class public Lcom/google/android/gms/internal/an;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/google/android/gms/internal/gy;Lcom/google/android/gms/internal/gp;)Lcom/google/android/gms/internal/am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/internal/gy;",
            "Lcom/google/android/gms/internal/gp",
            "<",
            "Lcom/google/android/gms/internal/am;",
            ">;)",
            "Lcom/google/android/gms/internal/am;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/internal/ao;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/ao;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gy;)V

    new-instance v1, Lcom/google/android/gms/internal/an$2;

    invoke-direct {v1, p0, p3, v0}, Lcom/google/android/gms/internal/an$2;-><init>(Lcom/google/android/gms/internal/an;Lcom/google/android/gms/internal/gp;Lcom/google/android/gms/internal/am;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/am;->a(Lcom/google/android/gms/internal/am$a;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/google/android/gms/internal/gy;Ljava/lang/String;)Ljava/util/concurrent/Future;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/gms/internal/gy;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/gms/internal/am;",
            ">;"
        }
    .end annotation

    new-instance v4, Lcom/google/android/gms/internal/gp;

    invoke-direct {v4}, Lcom/google/android/gms/internal/gp;-><init>()V

    sget-object v6, Lcom/google/android/gms/internal/gw;->wB:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/internal/an$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/an$1;-><init>(Lcom/google/android/gms/internal/an;Landroid/content/Context;Lcom/google/android/gms/internal/gy;Lcom/google/android/gms/internal/gp;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v4
.end method
