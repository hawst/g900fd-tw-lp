.class public abstract Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;
.super Landroid/widget/BaseAdapter;
.source "BufferAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEntry:",
        "Ljava/lang/Object;",
        "TView:",
        "Landroid/view/View;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field private mBuffer:Lcom/google/android/gms/common/data/DataBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/data/DataBuffer",
            "<TTEntry;>;"
        }
    .end annotation
.end field

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLayoutId:I


# virtual methods
.method public abstract bindView(Landroid/view/View;ILjava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTView;ITTEntry;)V"
        }
    .end annotation
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;, "Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter<TTEntry;TTView;>;"
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->mBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->mBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataBuffer;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 57
    .local p0, "this":Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;, "Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter<TTEntry;TTView;>;"
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->mBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/DataBuffer;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;, "Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter<TTEntry;TTView;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 74
    .local p0, "this":Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;, "Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter<TTEntry;TTView;>;"
    if-eqz p2, :cond_0

    .line 75
    move-object v0, p2

    .line 80
    .local v0, "view":Landroid/view/View;, "TTView;"
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->mBuffer:Lcom/google/android/gms/common/data/DataBuffer;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/data/DataBuffer;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->bindView(Landroid/view/View;ILjava/lang/Object;)V

    .line 82
    return-object v0

    .line 77
    .end local v0    # "view":Landroid/view/View;, "TTView;"
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->mLayoutId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "view":Landroid/view/View;, "TTView;"
    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;, "Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter<TTEntry;TTView;>;"
    invoke-virtual {p0}, Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
