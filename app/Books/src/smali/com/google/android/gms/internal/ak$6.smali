.class Lcom/google/android/gms/internal/ak$6;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/cd;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/ak;->b(Lcom/google/android/gms/internal/am;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic mW:Lcom/google/android/gms/internal/ak;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/ak;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ak$6;->mW:Lcom/google/android/gms/internal/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/ha;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/ha;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/ak$6;->mW:Lcom/google/android/gms/internal/ak;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/ak;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received request to untrack: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/ak$6;->mW:Lcom/google/android/gms/internal/ak;

    invoke-static {v1}, Lcom/google/android/gms/internal/ak;->b(Lcom/google/android/gms/internal/ak;)Lcom/google/android/gms/internal/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/internal/ai;->aC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/gx;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/ak$6;->mW:Lcom/google/android/gms/internal/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ak;->destroy()V

    goto :goto_0
.end method
