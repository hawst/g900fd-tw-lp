.class public Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
.super Ljava/lang/Object;
.source "CorpusTableMapping.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    }
.end annotation


# instance fields
.field protected final mCondition:Ljava/lang/String;

.field protected final mCorpusName:Ljava/lang/String;

.field protected final mCreatedTimestampMsSpec:Ljava/lang/String;

.field protected final mDataSourceName:Ljava/lang/String;

.field protected final mIsUriColumnUnique:Z

.field protected final mScore:Ljava/lang/String;

.field protected final mSectionNameToColumnName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final mTableName:Ljava/lang/String;

.field protected final mUriCol:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "corpusName"    # Ljava/lang/String;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "uriCol"    # Ljava/lang/String;
    .param p4, "score"    # Ljava/lang/String;
    .param p5, "createdTimestampMsSpec"    # Ljava/lang/String;
    .param p7, "condition"    # Ljava/lang/String;
    .param p8, "viewNameOrNull"    # Ljava/lang/String;
    .param p9, "isUriColumnUnique"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p6, "sectionNamesToColumnNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    if-nez p3, :cond_0

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "A URI column must be specified for table "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mCorpusName:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mTableName:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mUriCol:Ljava/lang/String;

    .line 55
    if-nez p4, :cond_1

    const-string p4, "0"

    .end local p4    # "score":Ljava/lang/String;
    :cond_1
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mScore:Ljava/lang/String;

    .line 56
    if-nez p5, :cond_2

    const-string p5, "0"

    .end local p5    # "createdTimestampMsSpec":Ljava/lang/String;
    :cond_2
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mCreatedTimestampMsSpec:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p6}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mSectionNameToColumnName:Ljava/util/Map;

    .line 60
    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mCondition:Ljava/lang/String;

    .line 61
    if-nez p8, :cond_3

    iget-object p8, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mTableName:Ljava/lang/String;

    .end local p8    # "viewNameOrNull":Ljava/lang/String;
    :cond_3
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mDataSourceName:Ljava/lang/String;

    .line 62
    iput-boolean p9, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mIsUriColumnUnique:Z

    .line 63
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "corpusName"    # Ljava/lang/String;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "uriCol"    # Ljava/lang/String;
    .param p4, "score"    # Ljava/lang/String;
    .param p5, "createdTimestampMsSpec"    # Ljava/lang/String;
    .param p7, "condition"    # Ljava/lang/String;
    .param p8, "isUriColumnUnique"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p6, "sectionNamesToColumnNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 44
    return-void
.end method


# virtual methods
.method public getCondition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mCondition:Ljava/lang/String;

    return-object v0
.end method

.method public getCorpusName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mCorpusName:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedTimestampMsSpec()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mCreatedTimestampMsSpec:Ljava/lang/String;

    return-object v0
.end method

.method public getDataSourceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mDataSourceName:Ljava/lang/String;

    return-object v0
.end method

.method public getScoreSpec()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mScore:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionToColumnNameMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mSectionNameToColumnName:Ljava/util/Map;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mTableName:Ljava/lang/String;

    return-object v0
.end method

.method public getUriColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mUriCol:Ljava/lang/String;

    return-object v0
.end method

.method public isUriColumnUnique()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->mIsUriColumnUnique:Z

    return v0
.end method
