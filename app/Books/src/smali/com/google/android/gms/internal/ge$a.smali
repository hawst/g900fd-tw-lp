.class public final Lcom/google/android/gms/internal/ge$a;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/ge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final errorCode:I

.field public final lK:Lcom/google/android/gms/internal/bd;

.field public final vo:Lorg/json/JSONObject;

.field public final vp:Lcom/google/android/gms/internal/cr;

.field public final vr:J

.field public final vs:J

.field public final vu:Lcom/google/android/gms/internal/fn;

.field public final vv:Lcom/google/android/gms/internal/fp;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/fn;Lcom/google/android/gms/internal/fp;Lcom/google/android/gms/internal/cr;Lcom/google/android/gms/internal/bd;IJJLorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ge$a;->vu:Lcom/google/android/gms/internal/fn;

    iput-object p2, p0, Lcom/google/android/gms/internal/ge$a;->vv:Lcom/google/android/gms/internal/fp;

    iput-object p3, p0, Lcom/google/android/gms/internal/ge$a;->vp:Lcom/google/android/gms/internal/cr;

    iput-object p4, p0, Lcom/google/android/gms/internal/ge$a;->lK:Lcom/google/android/gms/internal/bd;

    iput p5, p0, Lcom/google/android/gms/internal/ge$a;->errorCode:I

    iput-wide p6, p0, Lcom/google/android/gms/internal/ge$a;->vr:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/ge$a;->vs:J

    iput-object p10, p0, Lcom/google/android/gms/internal/ge$a;->vo:Lorg/json/JSONObject;

    return-void
.end method
