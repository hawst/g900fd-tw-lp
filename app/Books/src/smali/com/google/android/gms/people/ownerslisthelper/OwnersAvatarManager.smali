.class public Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;
.super Ljava/lang/Object;
.source "OwnersAvatarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;
    }
.end annotation


# static fields
.field private static sPlaceholder:Landroid/graphics/Bitmap;


# instance fields
.field public final mClient:Lcom/google/android/gms/people/PeopleClient;

.field private mClosed:Z

.field public final mContext:Landroid/content/Context;

.field private final mImages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequests:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private enqueueRequest(Landroid/widget/ImageView;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;)V
    .locals 2
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "req"    # Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->removePendingRequest(Landroid/widget/ImageView;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0}, Lcom/google/android/gms/people/PeopleClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 98
    const-string v0, "AvatarManager"

    const-string v1, "Client not connected."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->processNextRequest()V

    goto :goto_0
.end method

.method protected static getPlaceholder(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 180
    sget-object v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->sPlaceholder:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 181
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/people/ownerslisthelper/R$drawable;->avatar_placeholder:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/ownerslisthelper/ImageUtils;->frameBitmapInCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->sPlaceholder:Landroid/graphics/Bitmap;

    .line 184
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->sPlaceholder:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private processNextRequest()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->load()V

    goto :goto_0
.end method


# virtual methods
.method public loadOwnerAvatar(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;
    .param p4, "avatarSize"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mImages:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mImages:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;-><init>(Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->enqueueRequest(Landroid/widget/ImageView;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;)V

    goto :goto_0
.end method

.method public onImageLoaded(Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;)V
    .locals 6
    .param p1, "status"    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2, "pfd"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "req"    # Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;

    .prologue
    .line 136
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v3, p3}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v3, p3}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 142
    iget-boolean v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClosed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_4

    .line 165
    if-eqz p2, :cond_0

    .line 167
    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 172
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClosed:Z

    if-nez v3, :cond_1

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->processNextRequest()V

    .line 176
    :cond_1
    :goto_1
    return-void

    .line 165
    :cond_2
    if-eqz p2, :cond_3

    .line 167
    :try_start_2
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 172
    :cond_3
    :goto_2
    iget-boolean v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClosed:Z

    if-nez v3, :cond_1

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->processNextRequest()V

    goto :goto_1

    .line 168
    :catch_0
    move-exception v1

    .line 169
    .local v1, "ignore":Ljava/io/IOException;
    const-string v3, "OwnersAvatarManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 168
    .end local v1    # "ignore":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 169
    .restart local v1    # "ignore":Ljava/io/IOException;
    const-string v3, "OwnersAvatarManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    .end local v1    # "ignore":Ljava/io/IOException;
    :cond_4
    :try_start_3
    iget-object v2, p3, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->view:Landroid/widget/ImageView;

    .line 146
    .local v2, "view":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    if-eq v3, p3, :cond_6

    .line 165
    if-eqz p2, :cond_5

    .line 167
    :try_start_4
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 172
    :cond_5
    :goto_3
    iget-boolean v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClosed:Z

    if-nez v3, :cond_1

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->processNextRequest()V

    goto :goto_1

    .line 168
    :catch_2
    move-exception v1

    .line 169
    .restart local v1    # "ignore":Ljava/io/IOException;
    const-string v3, "OwnersAvatarManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 151
    .end local v1    # "ignore":Ljava/io/IOException;
    :cond_6
    :try_start_5
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_7

    if-nez p2, :cond_8

    .line 152
    :cond_7
    const-string v3, "AvatarManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Avatar loaded: status="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  pfd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_8
    if-nez p2, :cond_a

    .line 157
    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->getPlaceholder(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 162
    .local v0, "circleImg":Landroid/graphics/Bitmap;
    :goto_4
    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mImages:Ljava/util/HashMap;

    iget-object v4, p3, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->accountName:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 165
    if-eqz p2, :cond_9

    .line 167
    :try_start_6
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 172
    :cond_9
    :goto_5
    iget-boolean v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClosed:Z

    if-nez v3, :cond_1

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->processNextRequest()V

    goto/16 :goto_1

    .line 159
    .end local v0    # "circleImg":Landroid/graphics/Bitmap;
    :cond_a
    :try_start_7
    invoke-static {p2}, Lcom/google/android/gms/people/PeopleClientUtil;->decodeFileDescriptor(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/ownerslisthelper/ImageUtils;->frameBitmapInCircle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .restart local v0    # "circleImg":Landroid/graphics/Bitmap;
    goto :goto_4

    .line 168
    :catch_3
    move-exception v1

    .line 169
    .restart local v1    # "ignore":Ljava/io/IOException;
    const-string v3, "OwnersAvatarManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 165
    .end local v0    # "circleImg":Landroid/graphics/Bitmap;
    .end local v1    # "ignore":Ljava/io/IOException;
    .end local v2    # "view":Landroid/widget/ImageView;
    :catchall_0
    move-exception v3

    if-eqz p2, :cond_b

    .line 167
    :try_start_8
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 172
    :cond_b
    :goto_6
    iget-boolean v4, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClosed:Z

    if-nez v4, :cond_c

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->processNextRequest()V

    :cond_c
    throw v3

    .line 168
    :catch_4
    move-exception v1

    .line 169
    .restart local v1    # "ignore":Ljava/io/IOException;
    const-string v4, "OwnersAvatarManager"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method public removePendingRequest(Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 114
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;

    iget-object v1, v1, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->view:Landroid/widget/ImageView;

    if-ne v1, p1, :cond_0

    .line 118
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mRequests:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 120
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    return-void
.end method
