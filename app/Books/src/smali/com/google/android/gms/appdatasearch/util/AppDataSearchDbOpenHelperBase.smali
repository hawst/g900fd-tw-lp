.class public abstract Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "AppDataSearchDbOpenHelperBase.java"

# interfaces
.implements Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;,
        Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;
    }
.end annotation


# static fields
.field private static final VERSION:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;


# instance fields
.field private final mListener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

.field private final mTableMappings:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->VERSION:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I
    .param p5, "tableMappings"    # [Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p6, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 70
    invoke-static {p5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->copyTableMappings([Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mTableMappings:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 71
    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mListener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .line 72
    return-void
.end method

.method private static copyTableMappings([Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 3
    .param p0, "tableMappings"    # [Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    const/4 v2, 0x0

    .line 91
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    .line 92
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Must provide at least 1 CorpusTableMapping"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_1
    array-length v1, p0

    new-array v0, v1, [Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .line 97
    .local v0, "result":[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    return-object v0
.end method

.method private static createAndPopulateSequenceTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "sequenceTableName"    # Ljava/lang/String;

    .prologue
    .line 205
    invoke-static {p0, p2}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE TABLE ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "seqno"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "action_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "docid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER UNIQUE ON CONFLICT REPLACE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "action_type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "docid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getUriColumn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getDataSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method private createSequenceTableAndTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 198
    invoke-static {p2}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "sequenceTableName":Ljava/lang/String;
    invoke-static {p1, p2, v0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createAndPopulateSequenceTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;)V

    .line 200
    invoke-static {p1, p2, v0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method private static createTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;)V
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "sequenceTableName"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getInsertTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getDeleteTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getUpdateTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method private static createTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "sequenceTableName"    # Ljava/lang/String;
    .param p3, "insertTriggerName"    # Ljava/lang/String;
    .param p4, "deleteTriggerName"    # Ljava/lang/String;
    .param p5, "updateTriggerName"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getUriColumn()Ljava/lang/String;

    move-result-object v4

    .line 231
    .local v4, "uriColumn":Ljava/lang/String;
    invoke-static {p0, p3}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTrigger(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 232
    invoke-static {p0, p5}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTrigger(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 233
    invoke-static {p0, p4}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTrigger(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 235
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "INSERT INTO ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "action_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "docid"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " VALUES (%s,%s);"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 239
    .local v2, "insertFormat":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new.["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "insertAdd":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "old.["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "insertDel":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getTableName()Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, "tableName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CREATE TRIGGER ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AFTER INSERT ON ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] FOR EACH ROW BEGIN "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " END"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 254
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CREATE TRIGGER ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AFTER DELETE ON ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] FOR EACH ROW BEGIN "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " END"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 259
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CREATE TRIGGER ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AFTER UPDATE ON ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] FOR EACH ROW BEGIN "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " END"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method private ensureCurrentSequenceTablesAfterUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 158
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 159
    .local v9, "wantedTables":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 160
    .local v10, "wantedTriggers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mTableMappings:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .local v0, "arr$":[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v6, v0, v3

    .line 161
    .local v6, "table":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    invoke-static {v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-static {v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getInsertTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    invoke-static {v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getUpdateTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-static {v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getDeleteTriggerName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 167
    .end local v6    # "table":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getExistingSequenceTables(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v1

    .line 169
    .local v1, "existingTables":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 170
    .local v5, "sequenceTableName":Ljava/lang/String;
    invoke-interface {v9, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 172
    invoke-static {p1, v5}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_1

    .line 176
    .end local v5    # "sequenceTableName":Ljava/lang/String;
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getExistingTriggers(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v2

    .line 178
    .local v2, "existingTriggers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 179
    .local v8, "triggerName":Ljava/lang/String;
    invoke-interface {v10, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 181
    invoke-static {p1, v8}, Lcom/google/android/gms/appdatasearch/util/MasterTableHelper;->dropTrigger(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_2

    .line 185
    .end local v8    # "triggerName":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mTableMappings:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    array-length v4, v0

    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_3
    if-ge v3, v4, :cond_6

    aget-object v7, v0, v3

    .line 186
    .local v7, "tableMapping":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    invoke-static {v7}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v5

    .line 187
    .restart local v5    # "sequenceTableName":Ljava/lang/String;
    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 188
    invoke-static {p1, v7, v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createAndPopulateSequenceTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;)V

    .line 193
    :cond_5
    invoke-static {p1, v7, v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;Ljava/lang/String;)V

    .line 185
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 195
    .end local v5    # "sequenceTableName":Ljava/lang/String;
    .end local v7    # "tableMapping":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    :cond_6
    return-void
.end method


# virtual methods
.method public cleanSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;J)V
    .locals 8
    .param p1, "tableSpec"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "lastCommittedSeqno"    # J

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 379
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v2

    .line 382
    .local v2, "sequenceTableName":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 384
    :try_start_0
    const-string v3, "seqno < ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 387
    .local v1, "numDeleted":I
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 391
    return-void

    .line 389
    .end local v1    # "numDeleted":I
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method protected abstract doOnCreate(Landroid/database/sqlite/SQLiteDatabase;)V
.end method

.method protected doOnOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 106
    return-void
.end method

.method protected abstract doOnUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
.end method

.method public getMaxSeqno(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)J
    .locals 7
    .param p1, "tableSpec"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 351
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v4

    .line 353
    .local v4, "sequenceTableName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SELECT MAX(seqno) FROM ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 357
    .local v0, "cursor":Landroid/database/Cursor;
    const-wide/16 v2, 0x0

    .line 359
    .local v2, "maxSeqno":J
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 360
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 363
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 366
    return-wide v2

    .line 363
    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v5
.end method

.method public getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mTableMappings:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    return-object v0
.end method

.method protected final notifyTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z
    .locals 1
    .param p1, "tableSpec"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mListener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mListener:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    invoke-interface {v0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;->onTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    move-result v0

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->doOnCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 112
    sget-object v4, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->VERSION:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;

    invoke-virtual {v4, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->create(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->mTableMappings:[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .local v0, "arr$":[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 114
    .local v3, "tableMapping":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createSequenceTableAndTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    .end local v3    # "tableMapping":Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    :cond_0
    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->doOnOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 133
    sget-object v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->VERSION:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->upgradeIfRequired(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->ensureCurrentSequenceTablesAfterUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 136
    :cond_0
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 120
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->doOnUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 122
    sget-object v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->VERSION:Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$Version;->upgradeIfRequired(Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 127
    invoke-direct {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->ensureCurrentSequenceTablesAfterUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 128
    return-void
.end method

.method public querySequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;
    .locals 18
    .param p1, "tableMapping"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "lastSeqNo"    # J
    .param p4, "limit"    # J

    .prologue
    .line 275
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getDataSourceName()Ljava/lang/String;

    move-result-object v3

    .line 276
    .local v3, "dataSourceName":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase$NamesHelper;->getSequenceTableName(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Ljava/lang/String;

    move-result-object v11

    .line 277
    .local v11, "sequenceTableName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getUriColumn()Ljava/lang/String;

    move-result-object v12

    .line 278
    .local v12, "uriColumn":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "].["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 290
    .local v13, "uriField":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    const/16 v15, 0x400

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "SELECT seqno AS seqno,CASE WHEN ["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "].["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "action_type"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] = \'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' AND "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " IS NOT NULL THEN \'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "add"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " ELSE \'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "del"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " END AS "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "action"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "docid"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " AS "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "uri"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getScoreSpec()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " AS doc_score,"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCreatedTimestampMsSpec()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " AS created_timestamp"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 303
    .local v7, "sb":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getSectionToColumnNameMap()Ljava/util/Map;

    move-result-object v10

    .line 304
    .local v10, "sectionsToColumns":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 305
    .local v9, "sectionToColumn":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 306
    .local v8, "sectionColumnName":Ljava/lang/String;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static {v14}, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncColumns;->getColumnNameForSection(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 307
    .local v6, "returnColumnName":Ljava/lang/String;
    const-string v14, ",["

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "].["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] AS "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 311
    .end local v6    # "returnColumnName":Ljava/lang/String;
    .end local v8    # "sectionColumnName":Ljava/lang/String;
    .end local v9    # "sectionToColumn":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    const-string v14, " FROM ["

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " LEFT OUTER JOIN ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ON ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "].["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "docid"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " WHERE "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "seqno"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " > "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, p2

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCondition()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1

    .line 319
    const-string v14, " AND ("

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " IS NULL"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string v14, " OR ("

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->getCondition()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "))"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;->isUriColumnUnique()Z

    move-result v14

    if-nez v14, :cond_2

    .line 327
    const-string v14, " GROUP BY "

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "seqno"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    :cond_2
    const-string v14, " ORDER BY "

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "seqno"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " LIMIT "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, p4

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 334
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v4, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 338
    .local v2, "cursor":Landroid/database/Cursor;
    return-object v2
.end method

.method public queryTagsTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;
    .locals 3
    .param p1, "tableSpec"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
    .param p2, "lastSeqNo"    # J
    .param p4, "limit"    # J

    .prologue
    .line 344
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncColumns;->TAGS_QUERY_COLUMN_NAMES:Ljava/util/List;

    sget-object v2, Lcom/google/android/gms/appdatasearch/SyncContentProviderHelper$SyncColumns;->TAGS_QUERY_COLUMN_NAMES:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    return-object v1
.end method

.method public recreateSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V
    .locals 2
    .param p1, "tableMapping"    # Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 148
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 150
    :try_start_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelperBase;->createSequenceTableAndTriggers(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V

    .line 151
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 155
    return-void

    .line 153
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method
