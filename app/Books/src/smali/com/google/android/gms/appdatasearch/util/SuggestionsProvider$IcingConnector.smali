.class Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;
.super Ljava/lang/Object;
.source "SuggestionsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IcingConnector"
.end annotation


# static fields
.field private static final CONNECT_TIMEOUT_MILLIS:J

.field private static final DISCONNECT_TIMEOUT_MILLIS:J


# instance fields
.field private final mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

.field private final mDisconnect:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 114
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->CONNECT_TIMEOUT_MILLIS:J

    .line 115
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->DISCONNECT_TIMEOUT_MILLIS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector$1;-><init>(Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mDisconnect:Ljava/lang/Runnable;

    .line 132
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mLock:Ljava/lang/Object;

    .line 137
    new-instance v0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-direct {v0, p1}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    .line 138
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mHandler:Landroid/os/Handler;

    .line 139
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;)Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    return-object v0
.end method

.method private connectLocked()Z
    .locals 6

    .prologue
    .line 157
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mDisconnect:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    sget-wide v2, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->CONNECT_TIMEOUT_MILLIS:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 160
    .local v0, "result":Lcom/google/android/gms/common/ConnectionResult;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    :cond_0
    const-string v1, "SuggestionsProvider"

    const-string v2, "Could not connect to AppDataSearchClient."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v1, 0x0

    .line 169
    .end local v0    # "result":Lcom/google/android/gms/common/ConnectionResult;
    :goto_0
    return v1

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mDisconnect:Ljava/lang/Runnable;

    sget-wide v4, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->DISCONNECT_TIMEOUT_MILLIS:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 169
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getSuggestions(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "limit"    # I

    .prologue
    const/4 v1, 0x0

    .line 142
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 143
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->connectLocked()Z

    move-result v3

    if-nez v3, :cond_0

    monitor-exit v2

    move-object v0, v1

    .line 151
    :goto_0
    return-object v0

    .line 144
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/SuggestionsProvider$IcingConnector;->mClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v4, p2, v5}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->queryGlobalSearch(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    .line 146
    .local v0, "suggestions":Lcom/google/android/gms/appdatasearch/SearchResults;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->hasError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 147
    const-string v3, "SuggestionsProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while query for suggestions: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->getErrorMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 151
    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 152
    .end local v0    # "suggestions":Lcom/google/android/gms/appdatasearch/SearchResults;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
