.class public Lcom/google/android/gms/people/PeopleClient;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/people/PeopleClient$OnImageLoadedListener;,
        Lcom/google/android/gms/people/PeopleClient$OnDataChangedListener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final auM:Lcom/google/android/gms/internal/pl;


# virtual methods
.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->auM:Lcom/google/android/gms/internal/pl;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/pl;->isConnected()Z

    move-result v0

    return v0
.end method

.method public loadOwnerAvatar(Lcom/google/android/gms/people/PeopleClient$OnImageLoadedListener;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p1, "listener"    # Lcom/google/android/gms/people/PeopleClient$OnImageLoadedListener;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;
    .param p4, "avatarSize"    # I
    .param p5, "avatarOptions"    # I

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->auM:Lcom/google/android/gms/internal/pl;

    new-instance v1, Lcom/google/android/gms/people/PeopleClient$11;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/people/PeopleClient$11;-><init>(Lcom/google/android/gms/people/PeopleClient;Lcom/google/android/gms/people/PeopleClient$OnImageLoadedListener;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/pl;->b(Lcom/google/android/gms/common/api/BaseImplementation$b;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/j;

    return-void
.end method
