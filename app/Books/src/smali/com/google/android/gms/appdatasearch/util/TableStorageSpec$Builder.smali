.class public final Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
.super Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
.source "TableStorageSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mAllowShortcuts:Z

.field private mGlobalSearchSpecs:[I

.field private final mSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTrimmable:Z

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;-><init>()V

    .line 134
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A valid tableName must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->table(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mSections:Ljava/util/List;

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mTrimmable:Z

    .line 140
    return-void
.end method


# virtual methods
.method public addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 4
    .param p1, "globalSearchSection"    # Ljava/lang/String;
    .param p2, "templateResourceId"    # I

    .prologue
    .line 227
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mGlobalSearchSpecs:[I

    if-nez v1, :cond_0

    .line 228
    invoke-static {}, Lcom/google/android/gms/appdatasearch/GlobalSearchSections;->getSectionsCount()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mGlobalSearchSpecs:[I

    .line 230
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/appdatasearch/GlobalSearchSections;->getSectionId(Ljava/lang/String;)I

    move-result v0

    .line 231
    .local v0, "id":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 232
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid global search section: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 235
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mGlobalSearchSpecs:[I

    aget v1, v1, v0

    if-eqz v1, :cond_2

    .line 236
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Table spec already contains mapping for global search section "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 240
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mGlobalSearchSpecs:[I

    aput p2, v1, v0

    .line 241
    return-object p0
.end method

.method public addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "sectionInfo"    # Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    .prologue
    .line 199
    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->columnForSection(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mSections:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    return-object p0
.end method

.method public build()Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    .locals 13

    .prologue
    .line 259
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mTableName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mUriCol:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mScore:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mCreatedTimestampMsSpec:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mVersion:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mSections:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mSectionToColumnNameMap:Ljava/util/Map;

    iget-object v8, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mCondition:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mGlobalSearchSpecs:[I

    iget-boolean v10, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mTrimmable:Z

    iget-boolean v11, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mAllowShortcuts:Z

    iget-boolean v12, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mIsUriColumnUnique:Z

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;[IZZZ)V

    return-object v0
.end method

.method public bridge synthetic score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 0
    .param p1, "scoreSpec"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-super {p0, p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    .line 157
    return-object p0
.end method

.method public bridge synthetic uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Z

    .prologue
    .line 119
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    return-object v0
.end method

.method public uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 0
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    .line 145
    return-object p0
.end method

.method public uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 0
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "isUnique"    # Z

    .prologue
    .line 150
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;->uriColumn(Ljava/lang/String;Z)Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping$Builder;

    .line 151
    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->mVersion:Ljava/lang/String;

    .line 168
    return-object p0
.end method
