.class public Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private BV:Ljava/lang/String;

.field private BY:Ljava/lang/String;

.field private CX:Landroid/net/Uri;

.field private final CY:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private CZ:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

.field private Ca:Landroid/accounts/Account;

.field private Da:Z

.field private Db:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->mName:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CY:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addSections(Ljava/lang/Iterable;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;)",
            "Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;"
        }
    .end annotation

    .prologue
    .local p1, "sections":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CY:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public build()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 10

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->BV:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CX:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CY:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CY:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CZ:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v6, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->Da:Z

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->Ca:Landroid/accounts/Account;

    iget-object v8, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->Db:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v9, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->BY:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;ZLandroid/accounts/Account;Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Ljava/lang/String;)V

    return-object v0
.end method

.method public contentProviderUri(Landroid/net/Uri;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;
    .locals 0
    .param p1, "cpUri"    # Landroid/net/Uri;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CX:Landroid/net/Uri;

    return-object p0
.end method

.method public globalSearchConfig(Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;
    .locals 0
    .param p1, "gscc"    # Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->CZ:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    return-object p0
.end method

.method public trimmable(Z)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;
    .locals 0
    .param p1, "trimmable"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->Da:Z

    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->BV:Ljava/lang/String;

    return-object p0
.end method
