.class public interface abstract Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
.super Ljava/lang/Object;
.source "AppDataSearchDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;
    }
.end annotation


# virtual methods
.method public abstract cleanSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;J)V
.end method

.method public abstract getMaxSeqno(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)J
.end method

.method public abstract getTableMappings()[Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
.end method

.method public abstract querySequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;
.end method

.method public abstract queryTagsTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;JJ)Landroid/database/Cursor;
.end method

.method public abstract recreateSequenceTable(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)V
.end method
