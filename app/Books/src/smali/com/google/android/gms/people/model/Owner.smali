.class public interface abstract Lcom/google/android/gms/people/model/Owner;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAccountName()Ljava/lang/String;
.end method

.method public abstract getAvatarUrl()Ljava/lang/String;
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getPlusPageId()Ljava/lang/String;
.end method
