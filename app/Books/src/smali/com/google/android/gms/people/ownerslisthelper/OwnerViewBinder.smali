.class final Lcom/google/android/gms/people/ownerslisthelper/OwnerViewBinder;
.super Ljava/lang/Object;
.source "OwnerViewBinder.java"


# direct methods
.method public static bindView(Landroid/view/View;Lcom/google/android/gms/people/model/Owner;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;Z)Landroid/view/View;
    .locals 8
    .param p0, "view"    # Landroid/view/View;
    .param p1, "owner"    # Lcom/google/android/gms/people/model/Owner;
    .param p2, "avatarLoader"    # Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;
    .param p3, "viewHolderItemCreator"    # Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;
    .param p4, "decorator"    # Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;
    .param p5, "isSelectedOwner"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_4

    .line 19
    invoke-interface {p3, p0, p5}, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;->createViewHolderItem(Landroid/view/View;Z)Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;

    move-result-object v1

    .line 20
    .local v1, "item":Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;
    invoke-virtual {p0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 24
    :goto_0
    const/4 v0, 0x0

    .line 25
    .local v0, "avatarUrl":Ljava/lang/String;
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->avatar:Landroid/widget/ImageView;

    if-eqz v4, :cond_0

    .line 26
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->avatar:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27
    invoke-interface {p1}, Lcom/google/android/gms/people/model/Owner;->getAvatarUrl()Ljava/lang/String;

    move-result-object v0

    .line 28
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 29
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->avatar:Landroid/widget/ImageView;

    invoke-virtual {p2, v4}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->removePendingRequest(Landroid/widget/ImageView;)V

    .line 30
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->avatar:Landroid/widget/ImageView;

    invoke-interface {p1}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, Lcom/google/android/gms/people/model/Owner;->getPlusPageId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v4, v5, v6, v2}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->loadOwnerAvatar(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 39
    :cond_0
    :goto_1
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->accountDisplayName:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 40
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 41
    iget-object v2, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->accountDisplayName:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 49
    :cond_1
    :goto_2
    iget-object v2, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->address:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 50
    iget-object v2, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->address:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_2
    if-eqz p4, :cond_3

    .line 53
    invoke-interface {p4, v1, p1, p5}, Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;->decorateView(Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;Lcom/google/android/gms/people/model/Owner;Z)V

    .line 55
    :cond_3
    return-object p0

    .line 22
    .end local v0    # "avatarUrl":Ljava/lang/String;
    .end local v1    # "item":Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;
    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;

    .restart local v1    # "item":Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;
    goto :goto_0

    .line 33
    .restart local v0    # "avatarUrl":Ljava/lang/String;
    :cond_5
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->avatar:Landroid/widget/ImageView;

    invoke-virtual {p2, v4}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->removePendingRequest(Landroid/widget/ImageView;)V

    .line 35
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->avatar:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->getPlaceholder(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 43
    :cond_6
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->accountDisplayName:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 44
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->accountDisplayName:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/google/android/gms/people/model/Owner;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v4, v1, Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItem;->accountDisplayName:Landroid/widget/TextView;

    if-eqz p5, :cond_7

    :goto_3
    invoke-virtual {v4, v7, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_2

    :cond_7
    move v2, v3

    goto :goto_3
.end method
