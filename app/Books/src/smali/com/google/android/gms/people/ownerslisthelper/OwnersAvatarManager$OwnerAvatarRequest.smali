.class final Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;
.super Ljava/lang/Object;
.source "OwnersAvatarManager.java"

# interfaces
.implements Lcom/google/android/gms/people/PeopleClient$OnImageLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OwnerAvatarRequest"
.end annotation


# instance fields
.field public final accountName:Ljava/lang/String;

.field public final avatarSize:I

.field public final pageId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

.field public final view:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p2, "view"    # Landroid/widget/ImageView;
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "pageId"    # Ljava/lang/String;
    .param p5, "avatarSize"    # I

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->this$0:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->view:Landroid/widget/ImageView;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->accountName:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->pageId:Ljava/lang/String;

    .line 52
    iput p5, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->avatarSize:I

    .line 53
    return-void
.end method


# virtual methods
.method public load()V
    .locals 6

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->this$0:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

    iget-object v0, v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->mClient:Lcom/google/android/gms/people/PeopleClient;

    iget-object v2, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->accountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->pageId:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->avatarSize:I

    const/4 v5, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/PeopleClient;->loadOwnerAvatar(Lcom/google/android/gms/people/PeopleClient$OnImageLoadedListener;Ljava/lang/String;Ljava/lang/String;II)V

    .line 58
    return-void
.end method

.method public onImageLoaded(Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V
    .locals 1
    .param p1, "status"    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2, "pfd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;->this$0:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

    invoke-virtual {v0, p1, p2, p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;->onImageLoaded(Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager$OwnerAvatarRequest;)V

    .line 63
    return-void
.end method
