.class public Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView;
.super Landroid/widget/ExpandableListView;
.source "OwnersExpandableListView.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView$OnOwnerSelectedListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView$OnOwnerSelectedListener;


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 3
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v0

    .line 39
    .local v0, "adapter":Landroid/widget/ExpandableListAdapter;
    instance-of v1, v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 40
    check-cast v1, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;

    invoke-virtual {v1, p4}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->setSelectedOwner(I)V

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView;->mListener:Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView$OnOwnerSelectedListener;

    if-eqz v1, :cond_1

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView;->mListener:Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView$OnOwnerSelectedListener;

    check-cast v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;

    .end local v0    # "adapter":Landroid/widget/ExpandableListAdapter;
    invoke-virtual {v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->getSelectedOwner()Lcom/google/android/gms/people/model/Owner;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListView$OnOwnerSelectedListener;->onOwnerSelected(Lcom/google/android/gms/people/model/Owner;)V

    .line 45
    :cond_1
    const/4 v1, 0x1

    return v1
.end method
