.class public abstract Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;
.super Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;
.source "AppDataSearchProvider.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;-><init>()V

    return-void
.end method

.method private createCorpusInfo(Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 7
    .param p1, "table"    # Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .prologue
    .line 175
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string v6, "content"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->getContentProviderAuthority()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "appdatasearch"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getCorpusName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 182
    .local v0, "contentProviderUri":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getSections()Ljava/util/List;

    move-result-object v4

    .line 184
    .local v4, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;>;"
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getGlobalSearchSectionTemplates()[I

    move-result-object v3

    .line 185
    .local v3, "globalSearchTemplateMapping":[I
    const/4 v1, 0x0

    .line 186
    .local v1, "features":[Lcom/google/android/gms/appdatasearch/Feature;
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getAllowShortcuts()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 187
    const/4 v5, 0x1

    new-array v1, v5, [Lcom/google/android/gms/appdatasearch/Feature;

    .end local v1    # "features":[Lcom/google/android/gms/appdatasearch/Feature;
    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusFeature;->allowShortcuts()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v6

    aput-object v6, v1, v5

    .line 189
    .restart local v1    # "features":[Lcom/google/android/gms/appdatasearch/Feature;
    :cond_0
    if-eqz v3, :cond_1

    new-instance v2, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;-><init>([I[Lcom/google/android/gms/appdatasearch/Feature;)V

    .line 193
    .local v2, "globalSearchCorpusConfig":Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getCorpusName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->builder(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getVersion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->version(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->contentProviderUri(Landroid/net/Uri;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->addSections(Ljava/lang/Iterable;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->globalSearchConfig(Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getTrimmable()Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->trimmable(Z)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v5

    return-object v5

    .line 189
    .end local v2    # "globalSearchCorpusConfig":Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private registerCorporaAsync()V
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;-><init>(Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 128
    return-void
.end method


# virtual methods
.method protected abstract createDataManager(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;
.end method

.method protected final createDatabase(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->createDataManager(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;

    move-result-object v0

    return-object v0
.end method

.method public final getDataManager()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->getDatabase()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;

    return-object v0
.end method

.method protected abstract getGlobalSearchableAppInfo()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProviderBase;->onCreate()Z

    move-result v1

    .line 66
    .local v1, "superSuccess":Z
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->shouldRegisterCorporaOnCreate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 68
    .local v0, "availability":I
    if-nez v0, :cond_1

    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->registerCorporaAsync()V

    .line 75
    .end local v0    # "availability":I
    :cond_0
    :goto_0
    return v1

    .line 71
    .restart local v0    # "availability":I
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->onError(I)V

    goto :goto_0
.end method

.method protected final registerCorpora([Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)I
    .locals 9
    .param p1, "tablesArray"    # [Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .prologue
    .line 142
    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 145
    .local v5, "tables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;>;"
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->getDataManager()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 146
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Not all tables in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " were registered "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "when the AppDataSearchProvider was created."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 142
    .end local v5    # "tables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;>;"
    :cond_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    goto :goto_0

    .line 150
    .restart local v5    # "tables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;>;"
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 151
    .local v1, "corpora":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .line 152
    .local v4, "table":Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    invoke-direct {p0, v4}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->createCorpusInfo(Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 154
    .end local v4    # "table":Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->getGlobalSearchableAppInfo()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    .line 156
    .local v0, "appInfo":Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    new-instance v2, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;

    invoke-direct {v2, p0, v1, v0, v5}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;-><init>(Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;Ljava/util/Set;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Ljava/util/List;)V

    .line 171
    .local v2, "function":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Integer;>;"
    const-string v6, "registerCorpora"

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p0, v2, v6, v7}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->executeWithConnection(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    return v6
.end method

.method protected shouldRegisterCorporaOnCreate()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method
