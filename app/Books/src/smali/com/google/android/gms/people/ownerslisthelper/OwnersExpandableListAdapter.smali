.class public Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;
.super Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter;
.source "OwnersExpandableListAdapter.java"

# interfaces
.implements Landroid/widget/ExpandableListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$1;,
        Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/people/ownerslisthelper/BufferAdapter",
        "<",
        "Lcom/google/android/gms/people/model/Owner;",
        "Landroid/view/ViewGroup;",
        ">;",
        "Landroid/widget/ExpandableListAdapter;"
    }
.end annotation


# static fields
.field protected static final DEFAULT_ACCOUNT_LAYOUT:I

.field protected static final GROUP_WRAPPER:I


# instance fields
.field private mAccountLayout:I

.field private mAccounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/people/model/Owner;",
            ">;"
        }
    .end annotation
.end field

.field private mAvatarLoader:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

.field private mSelectedOwnerId:Ljava/lang/String;

.field protected mViewDecorator:Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;

.field protected mViewHolderItemCreator:Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/google/android/gms/people/ownerslisthelper/R$layout;->account_item_view:I

    sput v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->DEFAULT_ACCOUNT_LAYOUT:I

    .line 25
    sget v0, Lcom/google/android/gms/people/ownerslisthelper/R$layout;->account_expandable_group_wrapper:I

    sput v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->GROUP_WRAPPER:I

    return-void
.end method

.method private setSelectedOwner(Lcom/google/android/gms/people/model/Owner;Z)V
    .locals 2
    .param p1, "selectedAccount"    # Lcom/google/android/gms/people/model/Owner;
    .param p2, "notify"    # Z

    .prologue
    .line 166
    if-nez p1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 172
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 176
    iput-object p1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwnerId:Ljava/lang/String;

    .line 179
    :cond_4
    if-eqz p2, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method protected addContent(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0
    .param p1, "contentArea"    # Landroid/view/ViewGroup;
    .param p2, "content"    # Landroid/view/View;

    .prologue
    .line 255
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 256
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 257
    return-void
.end method

.method public bridge synthetic bindView(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "x0":Landroid/view/View;
    check-cast p3, Lcom/google/android/gms/people/model/Owner;

    .end local p3    # "x2":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->bindView(Landroid/view/ViewGroup;ILcom/google/android/gms/people/model/Owner;)V

    return-void
.end method

.method public bindView(Landroid/view/ViewGroup;ILcom/google/android/gms/people/model/Owner;)V
    .locals 0
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "pos"    # I
    .param p3, "owner"    # Lcom/google/android/gms/people/model/Owner;

    .prologue
    .line 287
    return-void
.end method

.method protected createWrapper(Landroid/view/View;IZ)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "groupPosition"    # I
    .param p3, "isExpanded"    # Z

    .prologue
    const/4 v3, 0x0

    .line 235
    if-nez p1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->GROUP_WRAPPER:I

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 237
    new-instance v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;

    invoke-direct {v0, v3}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;-><init>(Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$1;)V

    .line 238
    .local v0, "item":Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;
    sget v1, Lcom/google/android/gms/people/ownerslisthelper/R$id;->expander_icon:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    # setter for: Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->expanderIcon:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->access$102(Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 239
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 243
    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->getChildrenCount(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 244
    # getter for: Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->expanderIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->access$100(Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    :goto_1
    return-object p1

    .line 241
    .end local v0    # "item":Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;

    .restart local v0    # "item":Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;
    goto :goto_0

    .line 247
    :cond_1
    # getter for: Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->expanderIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->access$100(Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 248
    # getter for: Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->expanderIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;->access$100(Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter$ExpanderViewHolderItem;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz p3, :cond_2

    sget v1, Lcom/google/android/gms/people/ownerslisthelper/R$drawable;->expander_close_holo_light:I

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_2
    sget v1, Lcom/google/android/gms/people/ownerslisthelper/R$drawable;->expander_open_holo_light:I

    goto :goto_2
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 215
    if-gez p1, :cond_0

    .line 216
    const/4 p1, 0x0

    .line 218
    :cond_0
    if-gez p2, :cond_1

    .line 219
    const/4 p2, 0x0

    .line 221
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/Owner;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/Owner;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 277
    if-nez p4, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccountLayout:I

    invoke-virtual {v0, v1, p5, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/model/Owner;

    iget-object v2, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAvatarLoader:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mViewHolderItemCreator:Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;

    iget-object v4, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mViewDecorator:Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;

    move-object v0, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/ownerslisthelper/OwnerViewBinder;->bindView(Landroid/view/View;Lcom/google/android/gms/people/model/Owner;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccounts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCombinedChildId(JJ)J
    .locals 5
    .param p1, "groupId"    # J
    .param p3, "childId"    # J

    .prologue
    .line 300
    const-wide/high16 v0, -0x8000000000000000L

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v2, p1

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    const-wide/16 v2, -0x1

    and-long/2addr v2, p3

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public getCombinedGroupId(J)J
    .locals 3
    .param p1, "groupId"    # J

    .prologue
    .line 309
    const-wide/32 v0, 0x7fffffff

    and-long/2addr v0, p1

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    return-wide v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 206
    if-gez p1, :cond_0

    .line 207
    const/4 p1, 0x0

    .line 209
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/Owner;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 262
    invoke-virtual {p0, p3, p1, p2}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->createWrapper(Landroid/view/View;IZ)Landroid/view/View;

    move-result-object p3

    .line 263
    sget v1, Lcom/google/android/gms/people/ownerslisthelper/R$id;->content_wrapper:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 264
    .local v6, "contentArea":Landroid/view/ViewGroup;
    invoke-virtual {v6, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 265
    .local v0, "content":Landroid/view/View;
    if-nez v0, :cond_0

    .line 266
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAccountLayout:I

    invoke-virtual {v1, v2, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 268
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    iget-object v2, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mAvatarLoader:Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;

    iget-object v3, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mViewHolderItemCreator:Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;

    iget-object v4, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mViewDecorator:Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/ownerslisthelper/OwnerViewBinder;->bindView(Landroid/view/View;Lcom/google/android/gms/people/model/Owner;Lcom/google/android/gms/people/ownerslisthelper/OwnersAvatarManager;Lcom/google/android/gms/people/ownerslisthelper/ViewHolderItemCreator;Lcom/google/android/gms/people/ownerslisthelper/ViewDecorator;Z)Landroid/view/View;

    move-result-object v0

    .line 270
    invoke-virtual {p0, v6, v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->addContent(Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 271
    return-object p3
.end method

.method public getSelectedOwner()Lcom/google/android/gms/people/model/Owner;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->mSelectedOwner:Lcom/google/android/gms/people/model/Owner;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 291
    const/4 v0, 0x1

    return v0
.end method

.method public onGroupCollapsed(I)V
    .locals 0
    .param p1, "groupPosition"    # I

    .prologue
    .line 315
    return-void
.end method

.method public onGroupExpanded(I)V
    .locals 0
    .param p1, "groupPosition"    # I

    .prologue
    .line 320
    return-void
.end method

.method public setSelectedOwner(I)V
    .locals 2
    .param p1, "selectedAccountPos"    # I

    .prologue
    .line 132
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/Owner;

    .line 133
    .local v0, "selectedAccount":Lcom/google/android/gms/people/model/Owner;
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->setSelectedOwner(Lcom/google/android/gms/people/model/Owner;)V

    .line 134
    return-void
.end method

.method public setSelectedOwner(Lcom/google/android/gms/people/model/Owner;)V
    .locals 1
    .param p1, "selectedAccount"    # Lcom/google/android/gms/people/model/Owner;

    .prologue
    .line 162
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/ownerslisthelper/OwnersExpandableListAdapter;->setSelectedOwner(Lcom/google/android/gms/people/model/Owner;Z)V

    .line 163
    return-void
.end method
