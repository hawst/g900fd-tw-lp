.class public final Lcom/google/android/gms/internal/fy;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation


# instance fields
.field private mg:Lcom/google/android/gms/internal/ha;

.field private final mz:Ljava/lang/Object;

.field private uq:Ljava/lang/String;

.field private ur:Lcom/google/android/gms/internal/gp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gp",
            "<",
            "Lcom/google/android/gms/internal/ga;",
            ">;"
        }
    .end annotation
.end field

.field public final us:Lcom/google/android/gms/internal/cd;

.field public final ut:Lcom/google/android/gms/internal/cd;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fy;->mz:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/internal/gp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gp;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fy;->ur:Lcom/google/android/gms/internal/gp;

    new-instance v0, Lcom/google/android/gms/internal/fy$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/fy$1;-><init>(Lcom/google/android/gms/internal/fy;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fy;->us:Lcom/google/android/gms/internal/cd;

    new-instance v0, Lcom/google/android/gms/internal/fy$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/fy$2;-><init>(Lcom/google/android/gms/internal/fy;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fy;->ut:Lcom/google/android/gms/internal/cd;

    iput-object p1, p0, Lcom/google/android/gms/internal/fy;->uq:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/fy;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fy;->mz:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/fy;)Lcom/google/android/gms/internal/gp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fy;->ur:Lcom/google/android/gms/internal/gp;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/fy;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fy;->uq:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public b(Lcom/google/android/gms/internal/ha;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/fy;->mg:Lcom/google/android/gms/internal/ha;

    return-void
.end method

.method public cJ()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/gms/internal/ga;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/fy;->ur:Lcom/google/android/gms/internal/gp;

    return-object v0
.end method
