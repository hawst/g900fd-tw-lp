.class public final Lcom/google/android/gms/internal/ai;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fe;
.end annotation


# instance fields
.field private final mv:Ljava/lang/String;

.field private final mw:Lorg/json/JSONObject;

.field private final mx:Ljava/lang/String;

.field private final my:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/internal/gy;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p2, Lcom/google/android/gms/internal/gy;->wC:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/ai;->my:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/ai;->mw:Lorg/json/JSONObject;

    iput-object p1, p0, Lcom/google/android/gms/internal/ai;->mx:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/ai;->mv:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public aA()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ai;->my:Ljava/lang/String;

    return-object v0
.end method

.method public aB()Lorg/json/JSONObject;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ai;->mw:Lorg/json/JSONObject;

    return-object v0
.end method

.method public aC()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ai;->mx:Ljava/lang/String;

    return-object v0
.end method

.method public az()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ai;->mv:Ljava/lang/String;

    return-object v0
.end method
