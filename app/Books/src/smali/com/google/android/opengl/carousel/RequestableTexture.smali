.class Lcom/google/android/opengl/carousel/RequestableTexture;
.super Ljava/lang/Object;
.source "RequestableTexture.java"


# static fields
.field private static sTextureIds:[I


# instance fields
.field private mHeight:I

.field private mTextureChangeTime:J

.field private mTextureId:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/opengl/carousel/RequestableTexture;->sTextureIds:[I

    return-void
.end method


# virtual methods
.method public deleteTextureId()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    iget v0, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureId:I

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Lcom/google/android/opengl/carousel/RequestableTexture;->sTextureIds:[I

    iget v1, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureId:I

    aput v1, v0, v2

    .line 37
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/opengl/carousel/RequestableTexture;->sTextureIds:[I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 38
    iput v2, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureId:I

    .line 40
    :cond_0
    return-void
.end method

.method public getChangeTime()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureChangeTime:J

    return-wide v0
.end method

.method public getTextureId()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureId:I

    return v0
.end method

.method public height()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mHeight:I

    return v0
.end method

.method public isTextureIdAllocated()Z
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureId:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/RequestableTexture;->deleteTextureId()V

    .line 65
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mTextureId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mTextureId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public width()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/opengl/carousel/RequestableTexture;->mWidth:I

    return v0
.end method
