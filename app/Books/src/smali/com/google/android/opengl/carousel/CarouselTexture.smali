.class public Lcom/google/android/opengl/carousel/CarouselTexture;
.super Ljava/lang/Object;
.source "CarouselTexture.java"


# instance fields
.field public mBackgroundId0:I

.field public mBackgroundId1:I

.field public mCardSingleDetailId:I

.field public mCardSingleId:I

.field public mDefaultId:I

.field public mDefaultLineId:I

.field public mDetailLoadingId:I

.field public mEmptyId:I

.field public mGlowingId:I

.field public mIds:[I

.field public mLoadingId:I

.field private mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;


# direct methods
.method public static setDefaultNPOTTextureState()V
    .locals 4

    .prologue
    const v3, 0x812f

    const v2, 0x46180400    # 9729.0f

    const/16 v1, 0xde1

    .line 112
    const/16 v0, 0x2801

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 114
    const/16 v0, 0x2800

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 118
    const/16 v0, 0x2802

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 120
    const/16 v0, 0x2803

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 122
    return-void
.end method


# virtual methods
.method public loadTexture()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/16 v4, 0xde1

    const/4 v3, 0x0

    .line 48
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    invoke-static {v5, v1, v3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 51
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->printIntArray([I)V

    .line 54
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    aget v1, v1, v3

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mLoadingId:I

    .line 55
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mDetailLoadingId:I

    .line 56
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId0:I

    .line 57
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId1:I

    .line 58
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x4

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mDefaultLineId:I

    .line 59
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mDefaultId:I

    .line 60
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mCardSingleId:I

    .line 61
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/4 v2, 0x7

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mCardSingleDetailId:I

    .line 62
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/16 v2, 0x8

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mGlowingId:I

    .line 63
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    const/16 v2, 0x9

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mEmptyId:I

    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 67
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mIds:[I

    aget v1, v1, v0

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 68
    invoke-static {}, Lcom/google/android/opengl/carousel/CarouselTexture;->setDefaultNPOTTextureState()V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselTexture;->mDefaultId:I

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 74
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v3, v1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 76
    :cond_1
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 77
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselTexture;->mLoadingId:I

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 78
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v3, v1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 81
    :cond_2
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDetailLoadingBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    .line 82
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mDetailLoadingId:I

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 83
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDetailLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v3, v1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 86
    :cond_3
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultLineBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 87
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mDefaultLineId:I

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 88
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultLineBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v3, v1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 91
    :cond_4
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mGlowingBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_5

    .line 92
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mGlowingId:I

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 93
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mGlowingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v3, v1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 96
    :cond_5
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mEmptyBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 97
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mEmptyId:I

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 98
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mEmptyBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v3, v1, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 101
    :cond_6
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 102
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    invoke-virtual {v1}, Lcom/google/android/opengl/carousel/Card;->initCardTexture()V

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 105
    :cond_7
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselTexture;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselScene;->mBackground:Lcom/google/android/opengl/carousel/Background;

    invoke-virtual {v1}, Lcom/google/android/opengl/carousel/Background;->uploadBitmap()V

    .line 108
    const-string v1, "loadTexture"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 109
    return-void
.end method
