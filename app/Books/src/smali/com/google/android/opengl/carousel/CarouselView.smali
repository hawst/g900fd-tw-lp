.class public Lcom/google/android/opengl/carousel/CarouselView;
.super Lcom/google/android/opengl/common/GLSurfaceView;
.source "CarouselView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/opengl/carousel/CarouselView$DrawListener;,
        Lcom/google/android/opengl/carousel/CarouselView$FpsInfo;
    }
.end annotation


# instance fields
.field private mAccessibilityEnabled:Z

.field private mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

.field private mCoverView:Landroid/view/View;

.field private mDebugKeyMode:Z

.field private mDrawListener:Lcom/google/android/opengl/carousel/CarouselView$DrawListener;

.field private mHandleHover:Z

.field private mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field private mScene:Lcom/google/android/opengl/carousel/CarouselScene;

.field private mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

.field private mTracking:Z


# direct methods
.method static synthetic access$000(Lcom/google/android/opengl/carousel/CarouselView;)Lcom/google/android/opengl/carousel/CarouselScene;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/opengl/carousel/CarouselView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/opengl/carousel/CarouselView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/opengl/carousel/CarouselView;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselView;->sendHoverEvent()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/opengl/carousel/CarouselView;)Lcom/google/android/opengl/carousel/CarouselSetting;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/opengl/carousel/CarouselView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    return-object v0
.end method

.method private static desiredKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 403
    sparse-switch p0, :sswitch_data_0

    .line 412
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 410
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 403
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method private focusOnFirstVisibleCard(I)V
    .locals 5
    .param p1, "direction"    # I

    .prologue
    .line 1658
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getRealtimeCarouselRotationAngle()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1660
    .local v1, "firstColumnLocation":I
    const/4 v2, 0x0

    .line 1661
    .local v2, "offset":I
    const/16 v3, 0x21

    if-ne p1, v3, :cond_0

    .line 1663
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    add-int/lit8 v2, v3, -0x1

    .line 1665
    :cond_0
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    mul-int/2addr v3, v1

    add-int v0, v3, v2

    .line 1666
    .local v0, "firstCardLocation":I
    new-instance v3, Lcom/google/android/opengl/carousel/CarouselView$26;

    invoke-direct {v3, p0, v0}, Lcom/google/android/opengl/carousel/CarouselView$26;-><init>(Lcom/google/android/opengl/carousel/CarouselView;I)V

    invoke-virtual {p0, v3}, Lcom/google/android/opengl/carousel/CarouselView;->queueEvent(Ljava/lang/Runnable;)V

    .line 1672
    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselView;->sendFocusedEvent(I)V

    .line 1673
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 1674
    return-void
.end method

.method public static isAccessibilityEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1694
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 1696
    .local v1, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    .line 1697
    .local v0, "accessibilityEnabled":Z
    return v0
.end method

.method private keyDownForMultiRow(II)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "focusCard"    # I

    .prologue
    const/4 v5, 0x0

    .line 493
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    .line 494
    .local v4, "rowCount":I
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getCardCount()I

    move-result v0

    .line 495
    .local v0, "cardCount":I
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v6}, Lcom/google/android/opengl/carousel/CarouselScene;->getCarouselRotationPosition()I

    move-result v1

    .line 496
    .local v1, "firstColumn":I
    const/4 v3, 0x0

    .line 497
    .local v3, "nextFocus":I
    const/4 v2, 0x0

    .line 499
    .local v2, "nextColumn":I
    packed-switch p1, :pswitch_data_0

    .line 541
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v5, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->setFocusedItem(I)V

    .line 542
    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselView;->sendFocusedEvent(I)V

    .line 544
    const/4 v5, 0x1

    :cond_1
    return v5

    .line 501
    :pswitch_0
    add-int/lit8 v6, v0, -0x1

    if-eq p2, v6, :cond_1

    .line 504
    add-int v3, p2, v4

    .line 505
    add-int/lit8 v5, v0, -0x1

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 506
    div-int v2, v3, v4

    .line 507
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    add-int/2addr v5, v1

    if-lt v2, v5, :cond_0

    .line 509
    invoke-direct {p0, v2}, Lcom/google/android/opengl/carousel/CarouselView;->rotateCarouselForVisibleCards(I)V

    goto :goto_0

    .line 513
    :pswitch_1
    if-eqz p2, :cond_1

    .line 516
    sub-int v3, p2, v4

    .line 517
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 518
    div-int v2, v3, v4

    .line 519
    if-ge v2, v1, :cond_0

    .line 520
    invoke-virtual {p0, v2}, Lcom/google/android/opengl/carousel/CarouselView;->rotateCarousel(I)V

    goto :goto_0

    .line 524
    :pswitch_2
    rem-int v6, p2, v4

    add-int/lit8 v7, v4, -0x1

    if-eq v6, v7, :cond_1

    .line 528
    add-int/lit8 v3, p2, 0x1

    .line 529
    add-int/lit8 v5, v0, -0x1

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 530
    goto :goto_0

    .line 532
    :pswitch_3
    rem-int v6, p2, v4

    if-eqz v6, :cond_1

    .line 536
    add-int/lit8 v3, p2, -0x1

    .line 537
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_0

    .line 499
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private keyDownForSingleRow(II)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "focusCard"    # I

    .prologue
    const/4 v2, 0x0

    .line 444
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mDpadHorizontal:Z

    if-eqz v3, :cond_1

    .line 445
    const/16 v3, 0x13

    if-eq p1, v3, :cond_0

    const/16 v3, 0x14

    if-ne p1, v3, :cond_2

    .line 487
    :cond_0
    :goto_0
    return v2

    .line 449
    :cond_1
    const/16 v3, 0x15

    if-eq p1, v3, :cond_0

    const/16 v3, 0x16

    if-eq p1, v3, :cond_0

    .line 454
    :cond_2
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v3}, Lcom/google/android/opengl/carousel/CarouselScene;->getCarouselRotationPosition()I

    move-result v0

    .line 455
    .local v0, "firstCard":I
    const/4 v1, 0x0

    .line 456
    .local v1, "nextFocus":I
    packed-switch p1, :pswitch_data_0

    .line 485
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v2, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->setFocusedItem(I)V

    .line 486
    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselView;->sendFocusedEvent(I)V

    .line 487
    const/4 v2, 0x1

    goto :goto_0

    .line 459
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getCardCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge p2, v3, :cond_0

    .line 464
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getCardCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 465
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    add-int/2addr v2, v0

    if-lt v1, v2, :cond_3

    .line 467
    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselView;->rotateCarouselForVisibleCards(I)V

    goto :goto_1

    .line 472
    :pswitch_1
    if-eqz p2, :cond_0

    .line 477
    add-int/lit8 v3, p2, -0x1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 478
    if-ge v1, v0, :cond_3

    .line 479
    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselView;->rotateCarousel(I)V

    goto :goto_1

    .line 456
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private onKeyDownInDebugMode(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 583
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDisplayLabel()C

    move-result v1

    .line 584
    .local v1, "keyChar":C
    const/4 v2, 0x1

    .line 586
    .local v2, "validKey":Z
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    .line 588
    .local v0, "camera":Lcom/google/android/opengl/carousel/GLCamera;
    sparse-switch v1, :sswitch_data_0

    .line 647
    const/4 v2, 0x0

    .line 651
    :goto_0
    if-eqz v2, :cond_0

    .line 653
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 657
    :cond_0
    return v2

    .line 590
    :sswitch_0
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->forward()V

    goto :goto_0

    .line 593
    :sswitch_1
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->backward()V

    goto :goto_0

    .line 596
    :sswitch_2
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->left()V

    goto :goto_0

    .line 599
    :sswitch_3
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->right()V

    goto :goto_0

    .line 602
    :sswitch_4
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->up()V

    goto :goto_0

    .line 605
    :sswitch_5
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->down()V

    goto :goto_0

    .line 608
    :sswitch_6
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->lookLeft()V

    goto :goto_0

    .line 611
    :sswitch_7
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->lookRight()V

    goto :goto_0

    .line 614
    :sswitch_8
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->lookForward()V

    goto :goto_0

    .line 617
    :sswitch_9
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->lookBackward()V

    goto :goto_0

    .line 620
    :sswitch_a
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->lookUp()V

    goto :goto_0

    .line 623
    :sswitch_b
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->lookDown()V

    goto :goto_0

    .line 626
    :sswitch_c
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->worldRotateLeft()V

    goto :goto_0

    .line 629
    :sswitch_d
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->worldRotateRight()V

    goto :goto_0

    .line 632
    :sswitch_e
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->reset()V

    goto :goto_0

    .line 635
    :sswitch_f
    const-string v3, "CarouselView"

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->printParameters()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 638
    :sswitch_10
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mDividAngle:F

    sub-float/2addr v4, v5

    iput v4, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    goto :goto_0

    .line 641
    :sswitch_11
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mDividAngle:F

    add-float/2addr v4, v5

    iput v4, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    goto :goto_0

    .line 644
    :sswitch_12
    const/4 v3, 0x0

    const/16 v4, 0x2ee

    const/4 v5, 0x1

    const/high16 v6, 0x41300000    # 11.0f

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/google/android/opengl/carousel/CarouselView;->setCarouselRotationAngle(FIIF)V

    goto :goto_0

    .line 588
    nop

    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_10
        0x2e -> :sswitch_11
        0x41 -> :sswitch_2
        0x44 -> :sswitch_3
        0x48 -> :sswitch_12
        0x49 -> :sswitch_8
        0x4a -> :sswitch_6
        0x4b -> :sswitch_9
        0x4c -> :sswitch_7
        0x4d -> :sswitch_b
        0x50 -> :sswitch_f
        0x51 -> :sswitch_4
        0x52 -> :sswitch_e
        0x53 -> :sswitch_1
        0x54 -> :sswitch_c
        0x55 -> :sswitch_a
        0x57 -> :sswitch_0
        0x59 -> :sswitch_d
        0x5a -> :sswitch_5
    .end sparse-switch
.end method

.method private rotateCarouselForVisibleCards(I)V
    .locals 1
    .param p1, "lastVisibleColumn"    # I

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    add-int/lit8 v0, v0, -0x1

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/carousel/CarouselView;->rotateCarousel(I)V

    .line 569
    return-void
.end method

.method private sendAccessibilityEventForItem(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "itemId"    # I

    .prologue
    .line 366
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/opengl/carousel/CarouselView;->mAccessibilityEnabled:Z

    if-eqz v2, :cond_0

    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 375
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v2, p2}, Lcom/google/android/opengl/carousel/CarouselCallback;->getTalkBackText(I)Ljava/lang/String;

    move-result-object v1

    .line 376
    .local v1, "label":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    invoke-virtual {p0, v0}, Lcom/google/android/opengl/carousel/CarouselView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method private sendFocusedEvent(I)V
    .locals 1
    .param p1, "focusId"    # I

    .prologue
    .line 361
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lcom/google/android/opengl/carousel/CarouselView;->sendAccessibilityEventForItem(II)V

    .line 362
    return-void
.end method

.method private sendHoverEvent()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 342
    const/4 v2, -0x1

    .line 343
    .local v2, "hoveredId":I
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v3}, Lcom/google/android/opengl/carousel/CarouselScene;->getHoverCard()I

    move-result v0

    .line 344
    .local v0, "hoverCard":I
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v3}, Lcom/google/android/opengl/carousel/CarouselScene;->getHoverDetail()I

    move-result v1

    .line 345
    .local v1, "hoverDetail":I
    if-eq v0, v4, :cond_2

    .line 346
    move v2, v0

    .line 351
    :cond_0
    :goto_0
    if-eq v2, v4, :cond_1

    .line 352
    const/4 v3, 0x4

    invoke-direct {p0, v3, v2}, Lcom/google/android/opengl/carousel/CarouselView;->sendAccessibilityEventForItem(II)V

    .line 354
    :cond_1
    return-void

    .line 347
    :cond_2
    if-eq v1, v4, :cond_0

    .line 348
    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public getCardCount()I
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    if-nez v0, :cond_0

    .line 577
    const/4 v0, 0x0

    .line 579
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v0

    goto :goto_0
.end method

.method public getCoverView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1705
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCoverView:Landroid/view/View;

    return-object v0
.end method

.method public getDrawListener()Lcom/google/android/opengl/carousel/CarouselView$DrawListener;
    .locals 1

    .prologue
    .line 1734
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mDrawListener:Lcom/google/android/opengl/carousel/CarouselView$DrawListener;

    return-object v0
.end method

.method public getDrawingRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "outRect"    # Landroid/graphics/Rect;

    .prologue
    const v2, 0x3e4ccccd    # 0.2f

    .line 1503
    invoke-super {p0, p1}, Lcom/google/android/opengl/common/GLSurfaceView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1504
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 1505
    iget v0, p1, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 1506
    iget v0, p1, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1507
    iget v0, p1, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1508
    return-void
.end method

.method public getRealtimeCarouselRotationAngle()F
    .locals 1

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/CarouselScene;->getRealtimeCarouselRotationAngle()F

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/android/opengl/common/GLSurfaceView;->onDetachedFromWindow()V

    .line 175
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v0}, Lcom/google/android/opengl/carousel/CarouselCallback;->onDetachedFromWindow()V

    .line 178
    :cond_0
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1643
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/opengl/common/GLSurfaceView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1644
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 1649
    invoke-direct {p0, p2}, Lcom/google/android/opengl/carousel/CarouselView;->focusOnFirstVisibleCard(I)V

    .line 1654
    :goto_0
    return-void

    .line 1651
    :cond_0
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->setFocusedItem(I)V

    .line 1652
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 302
    if-nez p1, :cond_0

    const/4 v3, 0x0

    .line 338
    :goto_0
    return v3

    .line 304
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mHandleHover:Z

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    .line 305
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 306
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 307
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 312
    .local v2, "y":F
    packed-switch v0, :pswitch_data_0

    .line 338
    .end local v0    # "action":I
    .end local v1    # "x":F
    .end local v2    # "y":F
    :cond_1
    :goto_1
    :pswitch_0
    invoke-super {p0, p1}, Lcom/google/android/opengl/common/GLSurfaceView;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto :goto_0

    .line 317
    .restart local v0    # "action":I
    .restart local v1    # "x":F
    .restart local v2    # "y":F
    :pswitch_1
    new-instance v3, Lcom/google/android/opengl/carousel/CarouselView$2;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/opengl/carousel/CarouselView$2;-><init>(Lcom/google/android/opengl/carousel/CarouselView;FF)V

    invoke-virtual {p0, v3}, Lcom/google/android/opengl/carousel/CarouselView;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 328
    :pswitch_2
    new-instance v3, Lcom/google/android/opengl/carousel/CarouselView$3;

    invoke-direct {v3, p0}, Lcom/google/android/opengl/carousel/CarouselView$3;-><init>(Lcom/google/android/opengl/carousel/CarouselView;)V

    invoke-virtual {p0, v3}, Lcom/google/android/opengl/carousel/CarouselView;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 312
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 417
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mDebugKeyMode:Z

    if-eqz v3, :cond_1

    .line 418
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselView;->onKeyDownInDebugMode(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 439
    :cond_0
    :goto_0
    return v1

    .line 421
    :cond_1
    invoke-static {p1}, Lcom/google/android/opengl/carousel/CarouselView;->desiredKey(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 425
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v3}, Lcom/google/android/opengl/carousel/CarouselScene;->getFocusedItem()I

    move-result v0

    .line 426
    .local v0, "focusCard":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    .line 427
    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselView;->focusOnFirstVisibleCard(I)V

    move v1, v2

    .line 428
    goto :goto_0

    .line 431
    :cond_2
    const/4 v1, 0x0

    .line 432
    .local v1, "handled":Z
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    if-ne v3, v2, :cond_3

    .line 433
    invoke-direct {p0, p1, v0}, Lcom/google/android/opengl/carousel/CarouselView;->keyDownForSingleRow(II)Z

    move-result v1

    .line 438
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    goto :goto_0

    .line 435
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/google/android/opengl/carousel/CarouselView;->keyDownForMultiRow(II)Z

    move-result v1

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 385
    sparse-switch p1, :sswitch_data_0

    .line 396
    invoke-super {p0, p1, p2}, Lcom/google/android/opengl/common/GLSurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 399
    :goto_0
    return v1

    .line 388
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v1}, Lcom/google/android/opengl/carousel/CarouselScene;->getFocusedItem()I

    move-result v0

    .line 389
    .local v0, "focusCard":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-eqz v1, :cond_0

    .line 391
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselView;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v1, v0}, Lcom/google/android/opengl/carousel/CarouselCallback;->onCardSelected(I)Z

    .line 399
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 385
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/opengl/carousel/CarouselView;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mAccessibilityEnabled:Z

    .line 183
    invoke-super {p0}, Lcom/google/android/opengl/common/GLSurfaceView;->onResume()V

    .line 184
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/CarouselScene;->clearSelection()V

    .line 185
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 271
    if-nez p1, :cond_0

    .line 297
    :goto_0
    return v3

    .line 273
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 274
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 275
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 277
    .local v2, "y":F
    packed-switch v0, :pswitch_data_0

    .line 296
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 297
    invoke-super {p0, p1}, Lcom/google/android/opengl/common/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto :goto_0

    .line 279
    :pswitch_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mTracking:Z

    .line 280
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/google/android/opengl/carousel/CarouselScene;->setFocusedItem(I)V

    .line 281
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->doStart(FF)V

    goto :goto_1

    .line 285
    :pswitch_1
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mTracking:Z

    if-eqz v3, :cond_1

    .line 286
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->doMotion(FF)V

    goto :goto_1

    .line 291
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v4, v1, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->doStop(FF)V

    .line 292
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselView;->mTracking:Z

    goto :goto_1

    .line 277
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public rotateCarousel(I)V
    .locals 1
    .param p1, "toColumn"    # I

    .prologue
    .line 552
    new-instance v0, Lcom/google/android/opengl/carousel/CarouselView$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/opengl/carousel/CarouselView$4;-><init>(Lcom/google/android/opengl/carousel/CarouselView;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/carousel/CarouselView;->queueEvent(Ljava/lang/Runnable;)V

    .line 565
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 999
    new-instance v0, Lcom/google/android/opengl/common/Float4;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    int-to-float v3, v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/opengl/common/Float4;-><init>(FFFF)V

    .line 1001
    .local v0, "tmp":Lcom/google/android/opengl/common/Float4;
    const/high16 v1, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/common/Float4;->times(F)V

    .line 1002
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselView;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselScene;->mBackground:Lcom/google/android/opengl/carousel/Background;

    iput-object v0, v1, Lcom/google/android/opengl/carousel/Background;->mColor:Lcom/google/android/opengl/common/Float4;

    .line 1003
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 1004
    return-void
.end method

.method public setCarouselRotationAngle(FIIF)V
    .locals 6
    .param p1, "endAngle"    # F
    .param p2, "milliseconds"    # I
    .param p3, "interpolationMode"    # I
    .param p4, "maxAnimatedArc"    # F

    .prologue
    .line 1209
    new-instance v0, Lcom/google/android/opengl/carousel/CarouselView$16;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/opengl/carousel/CarouselView$16;-><init>(Lcom/google/android/opengl/carousel/CarouselView;FIIF)V

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/carousel/CarouselView;->queueEvent(Ljava/lang/Runnable;)V

    .line 1216
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 1217
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 264
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/opengl/common/GLSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 265
    const-string v0, "CarouselView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "format is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 267
    return-void
.end method
