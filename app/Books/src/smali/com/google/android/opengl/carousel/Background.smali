.class Lcom/google/android/opengl/carousel/Background;
.super Ljava/lang/Object;
.source "Background.java"


# static fields
.field private static sProjection:[F

.field private static sVertices:[F


# instance fields
.field public mBitmap0:Landroid/graphics/Bitmap;

.field public mBitmap1:Landroid/graphics/Bitmap;

.field private mBitmapUploaded:Z

.field public mColor:Lcom/google/android/opengl/common/Float4;

.field private final mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field private final mScene:Lcom/google/android/opengl/carousel/CarouselScene;

.field private mTimeStamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x10

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Background;->sProjection:[F

    .line 25
    const/16 v0, 0xc

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Background;->sVertices:[F

    return-void
.end method

.method private getAnimatedAlpha(JJ)F
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "currentTime"    # J

    .prologue
    .line 49
    sub-long v4, p3, p1

    long-to-double v2, v4

    .line 50
    .local v2, "timeElapsed":D
    iget-object v4, p0, Lcom/google/android/opengl/carousel/Background;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-wide v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mBackgroundTransitionDuration:J

    long-to-double v4, v4

    div-double v0, v2, v4

    .line 51
    .local v0, "alpha":D
    const/high16 v4, 0x3f800000    # 1.0f

    double-to-float v5, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    return v4
.end method


# virtual methods
.method public draw(J)Z
    .locals 23
    .param p1, "currentTime"    # J

    .prologue
    .line 60
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    .line 61
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    .line 63
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    if-nez v4, :cond_2

    .line 64
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mColor:Lcom/google/android/opengl/common/Float4;

    iget v4, v4, Lcom/google/android/opengl/common/Float4;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/opengl/carousel/Background;->mColor:Lcom/google/android/opengl/common/Float4;

    iget v5, v5, Lcom/google/android/opengl/common/Float4;->y:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mColor:Lcom/google/android/opengl/common/Float4;

    iget v6, v6, Lcom/google/android/opengl/common/Float4;->z:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Background;->mColor:Lcom/google/android/opengl/common/Float4;

    iget v7, v7, Lcom/google/android/opengl/common/Float4;->w:F

    invoke-static {v4, v5, v6, v7}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 66
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmapUploaded:Z

    .line 67
    const/16 v4, 0x4000

    invoke-static {v4}, Landroid/opengl/GLES20;->glClear(I)V

    .line 69
    const/16 v19, 0x0

    .line 136
    :goto_0
    return v19

    .line 72
    :cond_2
    const/16 v19, 0x0

    .line 74
    .local v19, "stillAnimating":Z
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/opengl/carousel/Background;->mTimeStamp:J

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v4, v5, v1, v2}, Lcom/google/android/opengl/carousel/Background;->getAnimatedAlpha(JJ)F

    move-result v17

    .line 75
    .local v17, "animatedAlpha":F
    const v4, 0x3f7d70a4    # 0.99f

    cmpl-float v4, v17, v4

    if-gtz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    if-nez v4, :cond_7

    .line 76
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v4, v5}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 77
    const v4, 0x84c0

    invoke-static {v4}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 78
    const/16 v4, 0xde1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId0:I

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 79
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmapUploaded:Z

    if-nez v4, :cond_4

    .line 80
    const/16 v4, 0xde1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 103
    :cond_4
    :goto_1
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmapUploaded:Z

    .line 105
    sget-object v4, Lcom/google/android/opengl/carousel/Background;->sProjection:[F

    const/4 v5, 0x0

    const/high16 v6, -0x40800000    # -1.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, -0x40800000    # -1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static/range {v4 .. v11}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 107
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    sget-object v5, Lcom/google/android/opengl/carousel/Background;->sProjection:[F

    invoke-virtual {v4, v5}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setMVPUniform([F)V

    .line 108
    const/4 v4, 0x1

    const/4 v5, 0x2

    const/16 v6, 0x1406

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/opengl/carousel/Background;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v9, v9, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    invoke-static/range {v4 .. v9}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 112
    const v21, -0x4080068e    # -0.9999f

    .line 113
    .local v21, "z":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v4, Lcom/google/android/opengl/carousel/CarouselScene;->mVerticesData:[F

    move-object/from16 v20, v0

    .line 115
    .local v20, "vData":[F
    sget-object v4, Lcom/google/android/opengl/carousel/Background;->sVertices:[F

    const/4 v5, 0x0

    aget v5, v20, v5

    const/4 v6, 0x1

    aget v6, v20, v6

    const v7, -0x4080068e    # -0.9999f

    const/4 v8, 0x3

    aget v8, v20, v8

    const/4 v9, 0x4

    aget v9, v20, v9

    const v10, -0x4080068e    # -0.9999f

    const/4 v11, 0x6

    aget v11, v20, v11

    const/4 v12, 0x7

    aget v12, v20, v12

    const v13, -0x4080068e    # -0.9999f

    const/16 v14, 0x9

    aget v14, v20, v14

    const/16 v15, 0xa

    aget v15, v20, v15

    const v16, -0x4080068e    # -0.9999f

    invoke-static/range {v4 .. v16}, Lcom/google/android/opengl/carousel/GL2Helper;->setVector12f([FFFFFFFFFFFFF)V

    .line 120
    sget-object v4, Lcom/google/android/opengl/carousel/Background;->sVertices:[F

    invoke-static {v4}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuad([F)Z

    .line 123
    :try_start_0
    const-string v4, "drawBackground"

    invoke-static {v4}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 124
    :catch_0
    move-exception v18

    .line 125
    .local v18, "e":Ljava/lang/RuntimeException;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_5

    .line 126
    const-string v4, "Background"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed while trying to allocate texture id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v6, v6, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId0:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const-string v4, "Background"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Texture size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " * "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_6

    .line 130
    const-string v4, "Background"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed while trying to allocate texture id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v6, v6, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string v4, "Background"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Texture size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " * "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_6
    throw v18

    .line 83
    .end local v18    # "e":Ljava/lang/RuntimeException;
    .end local v20    # "vData":[F
    .end local v21    # "z":F
    :cond_7
    const/16 v19, 0x1

    .line 84
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-virtual {v4}, Lcom/google/android/opengl/carousel/CarouselRenderer;->requestRedraw()V

    .line 85
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v4, v5}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 87
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setFadeAmount(F)V

    .line 89
    const v4, 0x84c0

    invoke-static {v4}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 90
    const/16 v4, 0xde1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId1:I

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 91
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmapUploaded:Z

    if-nez v4, :cond_8

    .line 92
    const/16 v4, 0xde1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap1:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 96
    :cond_8
    const v4, 0x84c1

    invoke-static {v4}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 97
    const/16 v4, 0xde1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/opengl/carousel/Background;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselTexture;->mBackgroundId0:I

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 98
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/opengl/carousel/Background;->mBitmapUploaded:Z

    if-nez v4, :cond_4

    .line 99
    const/16 v4, 0xde1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/opengl/carousel/Background;->mBitmap0:Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto/16 :goto_1
.end method

.method uploadBitmap()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/Background;->mBitmapUploaded:Z

    .line 46
    return-void
.end method
