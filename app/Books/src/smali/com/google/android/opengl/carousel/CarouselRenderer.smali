.class public Lcom/google/android/opengl/carousel/CarouselRenderer;
.super Ljava/lang/Object;
.source "CarouselRenderer.java"

# interfaces
.implements Lcom/google/android/opengl/common/GLSurfaceView$Renderer;
.implements Lcom/google/android/opengl/common/IFpsRenderer;


# instance fields
.field public mAspect:F

.field public mAutoRotation:Z

.field public mCamera:Lcom/google/android/opengl/carousel/GLCamera;

.field public mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

.field public mColorProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field public mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field mDisplayFps:Z

.field private mFadeAmoundLoc:I

.field final mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

.field private mFrameCount:I

.field public mHeight:I

.field public mIsPortrait:Z

.field public mMultiTexHighlightProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field public mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field public mProjMatrix:[F

.field public mScene:Lcom/google/android/opengl/carousel/CarouselScene;

.field public mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

.field public mTexHighlightProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field public mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field public mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

.field public mVPMatrix:[F

.field private mVerboseMode:Z

.field public mView:Lcom/google/android/opengl/carousel/CarouselView;

.field public mWidth:I


# direct methods
.method private hideCoverView()V
    .locals 4

    .prologue
    .line 211
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mView:Lcom/google/android/opengl/carousel/CarouselView;

    invoke-virtual {v2}, Lcom/google/android/opengl/carousel/CarouselView;->getCoverView()Landroid/view/View;

    move-result-object v0

    .line 212
    .local v0, "cover":Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mView:Lcom/google/android/opengl/carousel/CarouselView;

    invoke-virtual {v2}, Lcom/google/android/opengl/carousel/CarouselView;->getDrawListener()Lcom/google/android/opengl/carousel/CarouselView$DrawListener;

    move-result-object v1

    .line 213
    .local v1, "listener":Lcom/google/android/opengl/carousel/CarouselView$DrawListener;
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 223
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mView:Lcom/google/android/opengl/carousel/CarouselView;

    new-instance v3, Lcom/google/android/opengl/carousel/CarouselRenderer$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer$1;-><init>(Lcom/google/android/opengl/carousel/CarouselRenderer;Landroid/view/View;Lcom/google/android/opengl/carousel/CarouselView$DrawListener;)V

    invoke-virtual {v2, v3}, Lcom/google/android/opengl/carousel/CarouselView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setGlParameter()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 309
    const/16 v0, 0xcf5

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    .line 310
    const/16 v0, 0xbd0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 311
    const/16 v0, 0x303

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 312
    invoke-static {v1, v1, v1, v1}, Landroid/opengl/GLES20;->glColorMask(ZZZZ)V

    .line 314
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 315
    const/16 v0, 0x405

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 318
    const-string v0, "setGlParameter"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method private setUniform()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 337
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 339
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 340
    invoke-static {v3}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 341
    invoke-static {v4}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 342
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uTexture"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 343
    .local v0, "sampleLoc":I
    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 345
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 346
    invoke-static {v3}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 347
    invoke-static {v4}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 349
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uTexture"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 350
    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 352
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uTexture1"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 353
    invoke-static {v0, v4}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 355
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uFadeAmount"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFadeAmoundLoc:I

    .line 356
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFadeAmoundLoc:I

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 358
    const-string v1, "setUniform"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 359
    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 103
    iget-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mDisplayFps:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVerboseMode:Z

    if-eqz v1, :cond_1

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v1}, Lcom/google/android/opengl/common/FpsDisplay;->preDraw()V

    .line 106
    :cond_1
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFrameCount:I

    .line 107
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFrameCount:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 108
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->hideCoverView()V

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v1}, Lcom/google/android/opengl/carousel/CarouselScene;->draw()Z

    move-result v0

    .line 112
    .local v0, "stillAnimating":Z
    iget-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mDisplayFps:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVerboseMode:Z

    if-eqz v1, :cond_4

    .line 115
    :cond_3
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v1}, Lcom/google/android/opengl/common/FpsDisplay;->postDraw()V

    .line 118
    :cond_4
    if-eqz v0, :cond_5

    .line 119
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mView:Lcom/google/android/opengl/carousel/CarouselView;

    invoke-virtual {v1}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 121
    :cond_5
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 6
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-static {v0, v0, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 144
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFrameCount:I

    .line 145
    iput p2, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    .line 146
    iput p3, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    .line 148
    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mAspect:F

    .line 149
    if-le p3, p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    .line 151
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v0, :cond_3

    .line 152
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-double v2, v1

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    sget v4, Lcom/google/android/opengl/carousel/CarouselSetting;->TRAJECTORY_X_OFFSET:I

    sub-int/2addr v1, v4

    int-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mTrajectoryAngle:F

    .line 163
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->updatePerspective()V

    .line 164
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mDisplayFps:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVerboseMode:Z

    if-eqz v0, :cond_2

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/opengl/common/FpsDisplay;->updateDimension(II)V

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/CarouselScene;->updateCarouselRotationAngle()V

    .line 169
    return-void

    .line 155
    :cond_3
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-double v2, v1

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mTrajectoryAngle:F

    goto :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setGlParameter()V

    .line 177
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mColorProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const-string v1, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vColor = aColor;\n}\n"

    const-string v2, "precision mediump float;\nvarying vec4 vColor;\nvoid main() {\n  gl_FragColor = vColor;\n}\n"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/opengl/carousel/GLProgram;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const-string v1, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  gl_PointSize = 1.0;\n  vTextureCoord = aTextureCoord;\n  vColor = aColor;\n}\n"

    const-string v2, "precision mediump float;\nvarying vec2 vTextureCoord;uniform sampler2D uTexture;void main() {  vec2 t0 = vTextureCoord.xy;  vec4 col = texture2D(uTexture, t0);  gl_FragColor = col; }\n"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/opengl/carousel/GLProgram;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const-string v1, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  gl_PointSize = 1.0;\n  vTextureCoord = aTextureCoord;\n  vColor = aColor;\n}\n"

    const-string v2, "precision mediump float;\nvarying vec2 vTextureCoord;uniform sampler2D uTexture;uniform sampler2D uTexture1;uniform float uFadeAmount;void main() {  vec2 t0 = vTextureCoord.xy;  vec4 col = texture2D(uTexture, t0);  vec4 col2 = texture2D(uTexture1, t0);  gl_FragColor = mix(col, col2, uFadeAmount);}\n"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/opengl/carousel/GLProgram;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexHighlightProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const-string v1, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  gl_PointSize = 1.0;\n  vTextureCoord = aTextureCoord;\n  vColor = aColor;\n}\n"

    const-string v2, "precision mediump float;\nvarying vec2 vTextureCoord;uniform sampler2D uTexture;uniform vec4 uHighlightColor;void main() {  vec2 t0 = vTextureCoord.xy;  vec4 col = texture2D(uTexture, t0);  gl_FragColor.rgb = mix(col.rgb, uHighlightColor.rgb, uHighlightColor.a);   gl_FragColor.rgb *= col.a;  gl_FragColor.a = col.a;}\n"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/opengl/carousel/GLProgram;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexHighlightProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const-string v1, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  gl_PointSize = 1.0;\n  vTextureCoord = aTextureCoord;\n  vColor = aColor;\n}\n"

    const-string v2, "precision mediump float;\nvarying vec2 vTextureCoord;uniform sampler2D uTexture;uniform sampler2D uTexture1;uniform float uFadeAmount;uniform vec4 uHighlightColor;void main() {  vec2 t0 = vTextureCoord.xy;  vec4 col = texture2D(uTexture, t0);  vec4 col2 = texture2D(uTexture1, t0);  vec4 col3 = mix(col, col2, uFadeAmount);  gl_FragColor.rgb = mix(col3.rgb, uHighlightColor.rgb, uHighlightColor.a);   gl_FragColor.rgb *= col.a;  gl_FragColor.a = col3.a;}\n"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/opengl/carousel/GLProgram;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mColorProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLProgram;->validProgram()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLProgram;->validProgram()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLProgram;->validProgram()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Failed to create GLProgram in onSurfaceCreated, exiting.."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/CarouselTexture;->loadTexture()V

    .line 194
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setUniform()V

    .line 196
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    invoke-interface {v0}, Lcom/google/android/opengl/carousel/ICardRenderer;->onSurfaceCreated()V

    .line 201
    :cond_2
    const-string v0, "CarouselRenderer"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "userdebug"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVerboseMode:Z

    .line 204
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mDisplayFps:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVerboseMode:Z

    if-eqz v0, :cond_5

    .line 205
    :cond_4
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v0}, Lcom/google/android/opengl/common/FpsDisplay;->onSurfaceCreated()V

    .line 207
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->requestRedraw()V

    .line 208
    return-void

    .line 201
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestRedraw()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mView:Lcom/google/android/opengl/carousel/CarouselView;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/CarouselView;->requestRender()V

    .line 137
    return-void
.end method

.method public setFadeAmount(F)V
    .locals 2
    .param p1, "fade"    # F

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    if-ne v0, v1, :cond_0

    .line 293
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mFadeAmoundLoc:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 294
    const-string v0, "setFadeAmount"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 296
    :cond_0
    return-void
.end method

.method public setHighlight(FFFF)V
    .locals 3
    .param p1, "red"    # F
    .param p2, "green"    # F
    .param p3, "blue"    # F
    .param p4, "alpha"    # F

    .prologue
    .line 299
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uHighlightColor"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 301
    .local v0, "colorLoc":I
    invoke-static {v0, p1, p2, p3, p4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 302
    const-string v1, "setHighlight"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method public setMVPUniform([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 288
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v0, v0, Lcom/google/android/opengl/carousel/GLProgram;->muMVPMatHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 289
    return-void
.end method

.method public updatePerspective()V
    .locals 5

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mProjMatrix:[F

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLCamera;->mFovy:F

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mAspect:F

    const v3, 0x3dcccccd    # 0.1f

    const/high16 v4, 0x42c80000    # 100.0f

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/opengl/carousel/GL2Helper;->loadPerspectiveMatrix([FFFFF)V

    .line 231
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 232
    return-void
.end method

.method public useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V
    .locals 1
    .param p1, "p"    # Lcom/google/android/opengl/carousel/GLProgram;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    if-eq v0, p1, :cond_0

    .line 251
    iput-object p1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 252
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v0, v0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 253
    const-string v0, "glUseProgram"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 255
    :cond_0
    return-void
.end method

.method public useTextureProgram()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 269
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    if-eq v1, v2, :cond_0

    .line 270
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iput-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 271
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v1}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 272
    const-string v1, "glUseProgram"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 273
    invoke-static {v3}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 274
    const/4 v1, 0x1

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 275
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uTexture"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 276
    .local v0, "sampleLoc":I
    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 277
    const-string v1, "setUniform"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 280
    .end local v0    # "sampleLoc":I
    :cond_0
    return-void
.end method
