.class public Lcom/google/android/opengl/carousel/GLProgram;
.super Ljava/lang/Object;
.source "GLProgram.java"


# instance fields
.field public mFShader:I

.field public mProgram:I

.field public mVShader:I

.field public maColorHandle:I

.field public maNormalHandle:I

.field public maPosHandle:I

.field public maTexHandle:I

.field public muMVPMatHandle:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->maPosHandle:I

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->maTexHandle:I

    .line 30
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->maColorHandle:I

    .line 31
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->maNormalHandle:I

    .line 38
    return-void
.end method

.method private bindHandles()V
    .locals 3

    .prologue
    .line 146
    iget v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const/4 v1, 0x0

    const-string v2, "aPosition"

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 147
    iget v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const/4 v1, 0x1

    const-string v2, "aTextureCoord"

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 148
    iget v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const/4 v1, 0x2

    const-string v2, "aColor"

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 149
    iget v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const/4 v1, 0x3

    const-string v2, "aNormal"

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 150
    const-string v0, "bindHandles"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method private getHandles()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 155
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "aPosition"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maPosHandle:I

    .line 156
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maPosHandle:I

    if-eqz v1, :cond_0

    .line 157
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not get attrib location for aPosition"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    :cond_0
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "aTextureCoord"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maTexHandle:I

    .line 161
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maTexHandle:I

    if-ne v1, v3, :cond_1

    .line 166
    :cond_1
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "aColor"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maColorHandle:I

    .line 167
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maColorHandle:I

    if-ne v1, v3, :cond_2

    .line 171
    :cond_2
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "aNormal"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maNormalHandle:I

    .line 172
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->maNormalHandle:I

    if-ne v1, v3, :cond_3

    .line 176
    :cond_3
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uMVPMatrix"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->muMVPMatHandle:I

    .line 177
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->muMVPMatHandle:I

    if-ne v1, v3, :cond_4

    .line 179
    const-string v1, "GLProgram"

    const-string v2, "No attrib handle for muMVPMatHandle."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_4
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const-string v2, "uTexture"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 184
    .local v0, "sampleLoc":I
    const-string v1, "getHandles"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 185
    const-string v1, "GLProgram"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handles: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->maPosHandle:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->maTexHandle:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->maColorHandle:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->muMVPMatHandle:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    return-void
.end method

.method public static getShaderFromRes(Landroid/content/Context;I)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 102
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 103
    .local v2, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 104
    .local v0, "br":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 105
    .local v3, "readLine":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .local v4, "sb":Ljava/lang/StringBuilder;
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 108
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v5

    .line 115
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 116
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 121
    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 117
    :catch_1
    move-exception v1

    .line 118
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "GLProgram"

    const-string v6, "Can\'t close file"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private loadShader(ILjava/lang/String;)I
    .locals 5
    .param p1, "shaderType"    # I
    .param p2, "source"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 125
    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 126
    .local v1, "shader":I
    if-eqz v1, :cond_0

    .line 127
    invoke-static {v1, p2}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 128
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 129
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 130
    .local v0, "compiled":[I
    const v2, 0x8b81

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 131
    aget v2, v0, v3

    if-nez v2, :cond_0

    .line 132
    const-string v2, "GLProgram"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not compile shader "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-string v2, "GLProgram"

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 135
    const/4 v1, 0x0

    .line 138
    .end local v0    # "compiled":[I
    :cond_0
    const-string v2, "loadShader"

    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 139
    return v1
.end method


# virtual methods
.method public createProgram(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "vertexSource"    # Ljava/lang/String;
    .param p2, "fragmentSource"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 58
    const v2, 0x8b31

    invoke-direct {p0, v2, p1}, Lcom/google/android/opengl/carousel/GLProgram;->loadShader(ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mVShader:I

    .line 59
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mVShader:I

    if-nez v2, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    const v2, 0x8b30

    invoke-direct {p0, v2, p2}, Lcom/google/android/opengl/carousel/GLProgram;->loadShader(ILjava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mFShader:I

    .line 64
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mFShader:I

    if-eqz v2, :cond_0

    .line 68
    const-string v2, "GLProgram"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The shaders are: vertex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/opengl/carousel/GLProgram;->mVShader:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fragment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/opengl/carousel/GLProgram;->mFShader:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v2

    iput v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    .line 70
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    if-eqz v2, :cond_2

    .line 71
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->mVShader:I

    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 72
    const-string v2, "glAttachShader"

    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 74
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->mFShader:I

    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 75
    const-string v2, "glAttachShader"

    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 77
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/GLProgram;->bindHandles()V

    .line 78
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v2}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 79
    new-array v0, v5, [I

    .line 80
    .local v0, "linkStatus":[I
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    const v3, 0x8b82

    invoke-static {v2, v3, v0, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 81
    aget v2, v0, v1

    if-eq v2, v5, :cond_3

    .line 82
    const-string v2, "GLProgram"

    const-string v3, "Could not link mProgram: "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const-string v2, "GLProgram"

    iget v3, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v3}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget v2, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 85
    iput v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    .line 92
    .end local v0    # "linkStatus":[I
    :cond_2
    :goto_1
    iget v1, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    goto/16 :goto_0

    .line 87
    .restart local v0    # "linkStatus":[I
    :cond_3
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/GLProgram;->getHandles()V

    goto :goto_1
.end method

.method public validProgram()Z
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
