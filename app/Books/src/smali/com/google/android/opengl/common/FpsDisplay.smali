.class public Lcom/google/android/opengl/common/FpsDisplay;
.super Ljava/lang/Object;
.source "FpsDisplay.java"


# static fields
.field private static FPS_FRAMES:I


# instance fields
.field private final mCanvas:Landroid/graphics/Canvas;

.field private mDisplayString:Ljava/lang/String;

.field public mEnableFpsLog:Z

.field public mFPS:F

.field private final mFpsBitmap:Landroid/graphics/Bitmap;

.field private final mFpsLog:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/opengl/carousel/CarouselView$FpsInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mFrameCount:I

.field private mFrameStart:J

.field private mInitFinished:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRenderTimeHistory:[J

.field private final mRenderer:Lcom/google/android/opengl/common/IFpsRenderer;

.field private mTextureId:I

.field private mTimeStart:J

.field private final mVPMatrix:[F

.field private final mVertexData:[F

.field private mWindowHeight:I

.field private mWindowWidth:I

.field private textUpdated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0x32

    sput v0, Lcom/google/android/opengl/common/FpsDisplay;->FPS_FRAMES:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/opengl/common/IFpsRenderer;)V
    .locals 8
    .param p1, "renderer"    # Lcom/google/android/opengl/common/IFpsRenderer;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mEnableFpsLog:Z

    .line 35
    const/16 v0, 0x190

    const/16 v1, 0x28

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsBitmap:Landroid/graphics/Bitmap;

    .line 37
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mPaint:Landroid/graphics/Paint;

    .line 38
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mCanvas:Landroid/graphics/Canvas;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mDisplayString:Ljava/lang/String;

    .line 40
    iput-boolean v4, p0, Lcom/google/android/opengl/common/FpsDisplay;->textUpdated:Z

    .line 42
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mVertexData:[F

    .line 46
    iput v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTextureId:I

    .line 48
    iput v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mWindowWidth:I

    .line 49
    iput v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mWindowHeight:I

    .line 50
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mVPMatrix:[F

    .line 53
    iput-wide v6, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    .line 55
    iput-wide v6, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameStart:J

    .line 57
    iput-boolean v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mInitFinished:Z

    .line 61
    iput v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameCount:I

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFPS:F

    .line 65
    sget v0, Lcom/google/android/opengl/common/FpsDisplay;->FPS_FRAMES:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mRenderTimeHistory:[J

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsLog:Ljava/util/ArrayList;

    .line 71
    iput-object p1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mRenderer:Lcom/google/android/opengl/common/IFpsRenderer;

    .line 72
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    .line 73
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 74
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 76
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 77
    return-void

    .line 42
    nop

    :array_0
    .array-data 4
        0x0
        0x42200000    # 40.0f
        0x40400000    # 3.0f
        0x0
        0x0
        0x0
        0x0
        0x40400000    # 3.0f
        0x0
        0x3f800000    # 1.0f
        0x43c80000    # 400.0f
        0x42200000    # 40.0f
        0x40400000    # 3.0f
        0x3f800000    # 1.0f
        0x0
        0x43c80000    # 400.0f
        0x0
        0x40400000    # 3.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private onDrawFrame()V
    .locals 8

    .prologue
    const/16 v7, 0xde1

    const/16 v6, 0xbe2

    const/4 v5, 0x0

    .line 86
    invoke-direct {p0}, Lcom/google/android/opengl/common/FpsDisplay;->updateProjection()V

    .line 88
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mRenderer:Lcom/google/android/opengl/common/IFpsRenderer;

    invoke-interface {v0}, Lcom/google/android/opengl/common/IFpsRenderer;->useTextureProgram()V

    .line 89
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mRenderer:Lcom/google/android/opengl/common/IFpsRenderer;

    iget-object v1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mVPMatrix:[F

    invoke-interface {v0, v1}, Lcom/google/android/opengl/common/IFpsRenderer;->setMVPUniform([F)V

    .line 90
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 91
    iget v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTextureId:I

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 92
    iget-boolean v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->textUpdated:Z

    if-eqz v0, :cond_0

    .line 93
    iput-boolean v5, p0, Lcom/google/android/opengl/common/FpsDisplay;->textUpdated:Z

    .line 94
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v5}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 96
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mDisplayString:Ljava/lang/String;

    const/4 v2, 0x0

    const/high16 v3, 0x41a00000    # 20.0f

    iget-object v4, p0, Lcom/google/android/opengl/common/FpsDisplay;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsBitmap:Landroid/graphics/Bitmap;

    invoke-static {v7, v5, v0, v5}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 100
    :cond_0
    invoke-static {v6}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mVertexData:[F

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuadTexCoords([F)Z

    .line 102
    invoke-static {v6}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 103
    return-void
.end method

.method private updateFPS()V
    .locals 20

    .prologue
    .line 177
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    .line 178
    .local v14, "time":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameStart:J

    move-wide/from16 v16, v0

    sub-long v10, v14, v16

    .line 179
    .local v10, "renderTime":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameCount:I

    move/from16 v16, v0

    sget v17, Lcom/google/android/opengl/common/FpsDisplay;->FPS_FRAMES:I

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mRenderTimeHistory:[J

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameCount:I

    move/from16 v17, v0

    aput-wide v10, v16, v17

    .line 181
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameCount:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/opengl/common/FpsDisplay;->mFrameCount:I

    .line 209
    :goto_0
    return-void

    .line 183
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    move-wide/from16 v16, v0

    cmp-long v16, v14, v16

    if-nez v16, :cond_1

    .line 184
    const/high16 v16, 0x42700000    # 60.0f

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/opengl/common/FpsDisplay;->mFPS:F

    .line 188
    :goto_1
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/opengl/common/FpsDisplay;->mFrameCount:I

    .line 189
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    .line 191
    const-wide/16 v4, 0x0

    .line 192
    .local v4, "avgRenderTime":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/opengl/common/FpsDisplay;->mRenderTimeHistory:[J

    .local v2, "arr$":[J
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_2
    if-ge v7, v8, :cond_2

    aget-wide v12, v2, v7

    .line 193
    .local v12, "t":J
    long-to-double v0, v12

    move-wide/from16 v16, v0

    add-double v4, v4, v16

    .line 192
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 186
    .end local v2    # "arr$":[J
    .end local v4    # "avgRenderTime":D
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v12    # "t":J
    :cond_1
    sget v16, Lcom/google/android/opengl/common/FpsDisplay;->FPS_FRAMES:I

    move/from16 v0, v16

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    move-wide/from16 v18, v0

    sub-long v18, v14, v18

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v17, v0

    div-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/opengl/common/FpsDisplay;->mFPS:F

    goto :goto_1

    .line 195
    .restart local v2    # "arr$":[J
    .restart local v4    # "avgRenderTime":D
    .restart local v7    # "i$":I
    .restart local v8    # "len$":I
    :cond_2
    sget v16, Lcom/google/android/opengl/common/FpsDisplay;->FPS_FRAMES:I

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v4, v4, v16

    .line 196
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mEnableFpsLog:Z

    move/from16 v16, v0

    if-eqz v16, :cond_4

    .line 197
    new-instance v3, Lcom/google/android/opengl/carousel/CarouselView$FpsInfo;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFPS:F

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/android/opengl/carousel/CarouselView$FpsInfo;-><init>(DD)V

    .line 198
    .local v3, "fpsInfo":Lcom/google/android/opengl/carousel/CarouselView$FpsInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsLog:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    const/16 v17, 0x100

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_3

    .line 199
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    const/16 v16, 0x40

    move/from16 v0, v16

    if-ge v6, v0, :cond_3

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsLog:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 199
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 203
    .end local v6    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFpsLog:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    .end local v3    # "fpsInfo":Lcom/google/android/opengl/carousel/CarouselView$FpsInfo;
    :cond_4
    const-string v16, "CPU Time: %.2f ms| FPS:%.1f"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/common/FpsDisplay;->mFPS:F

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 207
    .local v9, "logStr":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/opengl/common/FpsDisplay;->setDisplayString(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private updateProjection()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->mVPMatrix:[F

    const/4 v1, 0x0

    iget v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->mWindowWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/opengl/common/FpsDisplay;->mWindowHeight:I

    int-to-float v5, v4

    const/high16 v6, 0x41200000    # 10.0f

    const/high16 v7, -0x3ee00000    # -10.0f

    move v4, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 125
    return-void
.end method


# virtual methods
.method public onSurfaceCreated()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 132
    new-array v0, v3, [I

    .line 133
    .local v0, "textures":[I
    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 134
    aget v1, v0, v1

    iput v1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTextureId:I

    .line 135
    const v1, 0x84c0

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 136
    const/16 v1, 0xde1

    iget v2, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTextureId:I

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 137
    invoke-static {}, Lcom/google/android/opengl/carousel/GL2Helper;->setDefaultNPOTTextureState()V

    .line 138
    iput-boolean v3, p0, Lcom/google/android/opengl/common/FpsDisplay;->textUpdated:Z

    .line 139
    return-void
.end method

.method public postDraw()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/android/opengl/common/FpsDisplay;->updateFPS()V

    .line 160
    invoke-direct {p0}, Lcom/google/android/opengl/common/FpsDisplay;->onDrawFrame()V

    .line 161
    return-void
.end method

.method public preDraw()V
    .locals 6

    .prologue
    .line 145
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameStart:J

    .line 146
    iget-boolean v1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mInitFinished:Z

    if-nez v1, :cond_0

    .line 147
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mInitFinished:Z

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Initialization time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameStart:J

    iget-wide v4, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "logStr":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/android/opengl/common/FpsDisplay;->setDisplayString(Ljava/lang/String;)V

    .line 150
    iget-wide v2, p0, Lcom/google/android/opengl/common/FpsDisplay;->mFrameStart:J

    iput-wide v2, p0, Lcom/google/android/opengl/common/FpsDisplay;->mTimeStart:J

    .line 153
    .end local v0    # "logStr":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setDisplayString(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mDisplayString:Ljava/lang/String;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/opengl/common/FpsDisplay;->textUpdated:Z

    .line 108
    return-void
.end method

.method public updateDimension(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/google/android/opengl/common/FpsDisplay;->mWindowWidth:I

    .line 120
    iput p2, p0, Lcom/google/android/opengl/common/FpsDisplay;->mWindowHeight:I

    .line 121
    return-void
.end method
