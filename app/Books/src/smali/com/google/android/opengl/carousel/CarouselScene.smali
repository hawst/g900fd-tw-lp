.class Lcom/google/android/opengl/carousel/CarouselScene;
.super Ljava/lang/Object;
.source "CarouselScene.java"


# static fields
.field private static sProjection:[F

.field private static sRotationTemp:[F


# instance fields
.field mAnimatedSelection:I

.field private mAnimating:Z

.field private mAutoscrollDuration:D

.field private mAutoscrollInterpolationMode:I

.field private mAutoscrollStartAngle:F

.field private mAutoscrollStartTime:D

.field private mAutoscrollStopAngle:F

.field public mBackground:Lcom/google/android/opengl/carousel/Background;

.field private mBias:F

.field public mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

.field mCards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/opengl/carousel/Card;",
            ">;"
        }
    .end annotation
.end field

.field mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

.field mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

.field public mCarouselRotationAngle:F

.field mDetailFadeRate:F

.field mEnableSelection:Z

.field mEndAngle:F

.field private mFocusedItem:I

.field private mHitAngle:F

.field private mHoverCard:I

.field private mHoverDetail:I

.field mInScaleAnimation:Z

.field private mIsAutoScrolling:Z

.field private mIsDragging:Z

.field private mLastAngle:F

.field private mLastPosition:Landroid/graphics/PointF;

.field private mLastStopTime:J

.field private mLastTime:J

.field private mMass:F

.field mOnLongPress:Z

.field private mOverscroll:Z

.field private mOverscrollBias:F

.field private mReleaseTime:J

.field public mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field public mRotationAngle:F

.field mSelectedDetail:I

.field private mSelectionRadius:F

.field private mSelectionVelocityThreshold:F

.field public mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

.field private mStopTimeThreshold:J

.field mTexCoord:Ljava/nio/FloatBuffer;

.field private mTiltAngle:F

.field private mTouchBias:F

.field private mTouchPosition:Landroid/graphics/PointF;

.field private mTouchTime:J

.field private mVelocity:F

.field private mVelocityHistory:[F

.field private mVelocityHistoryCount:I

.field private mVelocityThreshold:F

.field private mVertices:Ljava/nio/FloatBuffer;

.field mVerticesData:[F

.field mWedgeAngle:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/16 v0, 0x10

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sProjection:[F

    .line 147
    const/16 v0, 0x20

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sRotationTemp:[F

    return-void
.end method

.method private carouselRotationAngleToRadians(F)F
    .locals 2
    .param p1, "carouselRotationAngle"    # F

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private computeAverageVelocityFromHistory()F
    .locals 5

    .prologue
    .line 943
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    if-lez v3, :cond_1

    .line 944
    const/16 v3, 0xa

    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 945
    .local v0, "count":I
    const/4 v2, 0x0

    .line 946
    .local v2, "vsum":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 947
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    aget v3, v3, v1

    add-float/2addr v2, v3

    .line 946
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 949
    :cond_0
    int-to-float v3, v0

    div-float v3, v2, v3

    .line 951
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "vsum":F
    :goto_1
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private cullCards()I
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 371
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mPrefetchCardCount:I

    div-int/lit8 v5, v10, 0x2

    .line 372
    .local v5, "prefetchCardCountPerSide":I
    const/4 v4, 0x0

    .line 373
    .local v4, "portraitCullOffset":F
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v10, v10, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v10, :cond_0

    .line 374
    const v10, 0x3e99999a    # 0.3f

    add-float/2addr v4, v10

    .line 378
    :cond_0
    const v10, -0x4019999a    # -1.8f

    sub-float/2addr v10, v4

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v8

    .line 379
    .local v8, "thetaFirst":F
    neg-int v10, v5

    int-to-float v10, v10

    sub-float/2addr v10, v4

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v6

    .line 380
    .local v6, "textureFirst":F
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    int-to-float v10, v10

    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v10, v11

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v9

    .line 381
    .local v9, "thetaLast":F
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    add-int/lit8 v10, v10, -0x1

    add-int/2addr v10, v5

    int-to-float v10, v10

    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v7

    .line 383
    .local v7, "textureLast":F
    const/4 v1, 0x0

    .line 385
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v2, v10, :cond_8

    .line 387
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 388
    .local v0, "card":Lcom/google/android/opengl/carousel/Card;
    iget-object v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    if-lez v10, :cond_7

    .line 391
    int-to-float v10, v2

    invoke-virtual {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v3

    .line 392
    .local v3, "p":F
    cmpl-float v10, v3, v8

    if-ltz v10, :cond_1

    cmpg-float v10, v3, v9

    if-ltz v10, :cond_2

    :cond_1
    cmpg-float v10, v3, v8

    if-gtz v10, :cond_5

    cmpl-float v10, v3, v9

    if-lez v10, :cond_5

    .line 393
    :cond_2
    iput-boolean v12, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    .line 394
    add-int/lit8 v1, v1, 0x1

    .line 398
    :goto_1
    cmpl-float v10, v3, v6

    if-ltz v10, :cond_3

    cmpg-float v10, v3, v7

    if-ltz v10, :cond_4

    :cond_3
    cmpg-float v10, v3, v6

    if-gtz v10, :cond_6

    cmpl-float v10, v3, v7

    if-lez v10, :cond_6

    .line 400
    :cond_4
    iput-boolean v12, v0, Lcom/google/android/opengl/carousel/Card;->mPrefetchTexture:Z

    .line 385
    .end local v3    # "p":F
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 396
    .restart local v3    # "p":F
    :cond_5
    iput-boolean v13, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    goto :goto_1

    .line 402
    :cond_6
    iput-boolean v13, v0, Lcom/google/android/opengl/carousel/Card;->mPrefetchTexture:Z

    goto :goto_2

    .line 406
    .end local v3    # "p":F
    :cond_7
    iput-boolean v12, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    .line 407
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 411
    .end local v0    # "card":Lcom/google/android/opengl/carousel/Card;
    :cond_8
    return v1
.end method

.method private deltaTimeInSeconds(J)F
    .locals 5
    .param p1, "current"    # J

    .prologue
    .line 1010
    iget-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1011
    iget-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 1013
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doAutoscroll(F)Z
    .locals 12
    .param p1, "currentTime"    # F

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 1561
    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    .line 1590
    :goto_0
    return v4

    .line 1565
    :cond_0
    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_1

    .line 1566
    float-to-double v6, p1

    iput-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1569
    :cond_1
    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    add-double v0, v6, v8

    .line 1571
    .local v0, "interpolationEndTime":D
    float-to-double v6, p1

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    sub-double/2addr v6, v8

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    div-double/2addr v6, v8

    double-to-float v3, v6

    .line 1572
    .local v3, "timePos":F
    cmpl-float v6, v3, v10

    if-lez v6, :cond_2

    .line 1573
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1576
    :cond_2
    move v2, v3

    .line 1577
    .local v2, "lambda":F
    iget v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    if-ne v6, v5, :cond_4

    .line 1578
    sub-float v6, v10, v3

    sub-float v7, v10, v3

    mul-float/2addr v6, v7

    sub-float v2, v10, v6

    .line 1584
    :cond_3
    :goto_1
    iget v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    mul-float/2addr v6, v2

    float-to-double v6, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    float-to-double v10, v2

    sub-double/2addr v8, v10

    iget v10, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    float-to-double v10, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v6, v6

    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 1586
    float-to-double v6, p1

    cmpl-double v6, v6, v0

    if-lez v6, :cond_5

    .line 1587
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->stopAutoscroll()V

    goto :goto_0

    .line 1579
    :cond_4
    iget v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 1581
    mul-float v6, v3, v3

    const/high16 v7, 0x40400000    # 3.0f

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v3

    sub-float/2addr v7, v8

    mul-float v2, v6, v7

    goto :goto_1

    :cond_5
    move v4, v5

    .line 1590
    goto :goto_0
.end method

.method private doPhysics(F)Z
    .locals 18
    .param p1, "dt"    # F

    .prologue
    .line 1084
    const v9, 0x3c23d70a    # 0.01f

    .line 1085
    .local v9, "minStepTime":F
    cmpl-float v15, p1, v9

    if-lez v15, :cond_0

    div-float v15, p1, v9

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v15

    add-int/lit8 v4, v15, 0x1

    .line 1087
    .local v4, "N":I
    :goto_0
    const/4 v15, 0x5

    invoke-static {v4, v15}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1088
    int-to-float v15, v4

    div-float p1, p1, v15

    .line 1090
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 1091
    .local v11, "oldBias":F
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v4, :cond_2

    .line 1093
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v15, v15, Lcom/google/android/opengl/carousel/CarouselSetting;->mFrictionCoeff:F

    neg-float v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    move/from16 v16, v0

    mul-float v2, v15, v16

    .line 1096
    .local v2, "Ff":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v15, v15, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    move/from16 v16, v0

    add-float v13, v15, v16

    .line 1097
    .local v13, "theta":F
    const v15, 0x40c90fdb

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v6, v15, v16

    .line 1098
    .local v6, "dtheta":F
    div-float v12, v13, v6

    .line 1101
    .local v12, "position":F
    float-to-double v0, v12

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->floor(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    sub-float v7, v12, v15

    .line 1103
    .local v7, "fraction":F
    const/high16 v15, 0x3f000000    # 0.5f

    cmpl-float v15, v7, v15

    if-lez v15, :cond_1

    .line 1104
    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v7

    neg-float v14, v15

    .line 1108
    .local v14, "x":F
    :goto_2
    const/high16 v15, -0x80000000

    mul-float v3, v15, v14

    .line 1111
    .local v3, "Fr":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mMass:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    add-float v16, v2, v3

    mul-float v16, v16, p1

    add-float v10, v15, v16

    .line 1112
    .local v10, "momentum":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mMass:F

    div-float v15, v10, v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 1114
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    move/from16 v16, v0

    mul-float v16, v16, p1

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 1091
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 1085
    .end local v2    # "Ff":F
    .end local v3    # "Fr":F
    .end local v4    # "N":I
    .end local v6    # "dtheta":F
    .end local v7    # "fraction":F
    .end local v8    # "i":I
    .end local v10    # "momentum":F
    .end local v11    # "oldBias":F
    .end local v12    # "position":F
    .end local v13    # "theta":F
    .end local v14    # "x":F
    :cond_0
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1106
    .restart local v2    # "Ff":F
    .restart local v4    # "N":I
    .restart local v6    # "dtheta":F
    .restart local v7    # "fraction":F
    .restart local v8    # "i":I
    .restart local v11    # "oldBias":F
    .restart local v12    # "position":F
    .restart local v13    # "theta":F
    :cond_1
    move v14, v7

    .restart local v14    # "x":F
    goto :goto_2

    .line 1117
    .end local v2    # "Ff":F
    .end local v6    # "dtheta":F
    .end local v7    # "fraction":F
    .end local v12    # "position":F
    .end local v13    # "theta":F
    .end local v14    # "x":F
    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    sub-float/2addr v15, v11

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1129
    .local v5, "deltaBias":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityThreshold:F

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/opengl/carousel/CarouselScene;->isInMotion(F)Z

    move-result v15

    return v15
.end method

.method private doSelection(FF)I
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 655
    new-instance v0, Lcom/google/android/opengl/carousel/Ray;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-direct {v0, v2}, Lcom/google/android/opengl/carousel/Ray;-><init>(Lcom/google/android/opengl/carousel/CarouselRenderer;)V

    .line 656
    .local v0, "ray":Lcom/google/android/opengl/carousel/Ray;
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    invoke-virtual {v0, v2, p1, p2}, Lcom/google/android/opengl/carousel/Ray;->makeRayForPixelAt(Lcom/google/android/opengl/carousel/GLCamera;FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 657
    const v2, 0x7cf0bdc2    # 1.0E37f

    iput v2, v0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 658
    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectGeometry(Lcom/google/android/opengl/carousel/Ray;)I

    move-result v1

    .line 662
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private dragFunction(FF)F
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    .line 1018
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->hitAngle(FF)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1019
    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    sub-float v1, v2, v3

    .line 1021
    .local v1, "result":F
    float-to-double v2, v1

    const-wide v4, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 1022
    float-to-double v2, v1

    add-double/2addr v2, v6

    double-to-float v1, v2

    .line 1026
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    iput v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    .line 1042
    :goto_1
    return v1

    .line 1023
    :cond_1
    float-to-double v2, v1

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 1024
    float-to-double v2, v1

    sub-double/2addr v2, v6

    double-to-float v1, v2

    goto :goto_0

    .line 1029
    .end local v1    # "result":F
    :cond_2
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragFactor:F

    .line 1030
    .local v0, "factor":F
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 1031
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->projectedDelta(FF)F

    move-result v2

    mul-float/2addr v0, v2

    .line 1040
    :goto_2
    const v2, 0x40490fdb    # (float)Math.PI

    mul-float v1, v0, v2

    .restart local v1    # "result":F
    goto :goto_1

    .line 1033
    .end local v1    # "result":F
    :cond_3
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v2, :cond_4

    .line 1034
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, p2

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    mul-float/2addr v0, v2

    goto :goto_2

    .line 1036
    :cond_4
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float v2, p1, v2

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v0, v2

    goto :goto_2
.end method

.method private drawCards(J)Z
    .locals 13
    .param p1, "currentTime"    # J

    .prologue
    .line 532
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    .line 533
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleSlotCount:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEndAngle:F

    .line 534
    const/4 v12, 0x0

    .line 536
    .local v12, "stillAnimating":Z
    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 538
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->setCardPosition()V

    .line 539
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    invoke-interface {v0}, Lcom/google/android/opengl/carousel/ICardRenderer;->beforeDrawAllCards()V

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .local v10, "i":I
    :goto_0
    if-ltz v10, :cond_8

    .line 544
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/opengl/carousel/Card;

    .line 545
    .local v7, "card":Lcom/google/android/opengl/carousel/Card;
    iget-boolean v0, v7, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    if-nez v0, :cond_2

    .line 543
    :cond_1
    :goto_1
    add-int/lit8 v10, v10, -0x1

    goto :goto_0

    .line 548
    :cond_2
    iget-object v0, v7, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/RequestableTexture;->getChangeTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedAlpha(JJ)F

    move-result v6

    .line 550
    .local v6, "animatedAlpha":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v6, v0

    if-gez v0, :cond_3

    .line 551
    const/4 v12, 0x1

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mRezInCardCount:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 557
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEndAngle:F

    int-to-float v1, v10

    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    div-float v11, v0, v1

    .line 558
    .local v11, "positionAlpha":F
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mRezInCardCount:F

    div-float v1, v11, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 564
    :goto_2
    const/high16 v0, 0x3f800000    # 1.0f

    mul-float v1, v6, v11

    invoke-virtual {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->getFadeOutLeftAlpha(I)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 567
    .local v8, "fadeAmount":F
    iget-object v0, v7, Lcom/google/android/opengl/carousel/Card;->mMMatrix:[F

    iget v1, v7, Lcom/google/android/opengl/carousel/Card;->mId:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->getMatrixForCard([FIZZ)Z

    move-result v0

    or-int/2addr v12, v0

    .line 570
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    if-ne v10, v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mScaleSelectedCard:Z

    if-nez v0, :cond_5

    .line 571
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedGlowingForSelected()F

    move-result v9

    .line 578
    .local v9, "glowAlpha":F
    :goto_3
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFrameSelectedCard:Z

    if-eqz v0, :cond_7

    const/high16 v0, -0x40800000    # -1.0f

    :goto_4
    invoke-virtual {v7, v8, v0}, Lcom/google/android/opengl/carousel/Card;->draw(FF)V

    .line 581
    float-to-double v0, v9

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFrameSelectedCard:Z

    if-eqz v0, :cond_1

    .line 582
    invoke-virtual {v7, v9}, Lcom/google/android/opengl/carousel/Card;->drawGlowing(F)V

    goto :goto_1

    .line 560
    .end local v8    # "fadeAmount":F
    .end local v9    # "glowAlpha":F
    .end local v11    # "positionAlpha":F
    :cond_4
    const/high16 v11, 0x3f800000    # 1.0f

    .restart local v11    # "positionAlpha":F
    goto :goto_2

    .line 572
    .restart local v8    # "fadeAmount":F
    :cond_5
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    if-ne v10, v0, :cond_6

    .line 573
    const/high16 v9, 0x3f800000    # 1.0f

    .restart local v9    # "glowAlpha":F
    goto :goto_3

    .line 575
    .end local v9    # "glowAlpha":F
    :cond_6
    const/high16 v9, -0x40800000    # -1.0f

    .restart local v9    # "glowAlpha":F
    goto :goto_3

    :cond_7
    move v0, v9

    .line 578
    goto :goto_4

    .line 586
    .end local v6    # "animatedAlpha":F
    .end local v7    # "card":Lcom/google/android/opengl/carousel/Card;
    .end local v8    # "fadeAmount":F
    .end local v9    # "glowAlpha":F
    .end local v11    # "positionAlpha":F
    :cond_8
    const-string v0, "drawCards"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 587
    return v12
.end method

.method private drawDetails(J)Z
    .locals 11
    .param p1, "currentTime"    # J

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mShowDetails:Z

    if-nez v0, :cond_0

    .line 598
    const/4 v10, 0x0

    .line 623
    :goto_0
    return v10

    .line 600
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTexCoord:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 602
    const/4 v10, 0x0

    .line 605
    .local v10, "stillAnimating":Z
    sget-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sProjection:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v3, v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v5, v5

    const/4 v6, 0x0

    const/high16 v7, 0x41a00000    # 20.0f

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 608
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 609
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 610
    const/16 v0, 0xde1

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselTexture;->mDetailLoadingId:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 612
    const/4 v8, 0x0

    .line 613
    .local v8, "card":Lcom/google/android/opengl/carousel/Card;
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v9, v0, -0x1

    .local v9, "i":I
    :goto_1
    if-ltz v9, :cond_2

    .line 614
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    iget-boolean v0, v0, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    if-nez v0, :cond_1

    .line 613
    :goto_2
    add-int/lit8 v9, v9, -0x1

    goto :goto_1

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "card":Lcom/google/android/opengl/carousel/Card;
    check-cast v8, Lcom/google/android/opengl/carousel/Card;

    .line 618
    .restart local v8    # "card":Lcom/google/android/opengl/carousel/Card;
    sget-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sProjection:[F

    invoke-virtual {v8, p1, p2, v0}, Lcom/google/android/opengl/carousel/Card;->drawDetails(J[F)Z

    move-result v0

    or-int/2addr v10, v0

    goto :goto_2

    .line 621
    :cond_2
    const-string v0, "drawDetails"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAnimatedGlowingForSelected()F
    .locals 8

    .prologue
    .line 988
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    sub-long v0, v4, v6

    .line 990
    .local v0, "dt":J
    const-wide/16 v4, 0xc8

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 991
    long-to-float v3, v0

    const/high16 v4, 0x43480000    # 200.0f

    div-float v2, v3, v4

    .line 995
    .local v2, "fraction":F
    :goto_0
    return v2

    .line 993
    .end local v2    # "fraction":F
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    .restart local v2    # "fraction":F
    goto :goto_0
.end method

.method private getAnimatedScaleForSelected(Lcom/google/android/opengl/common/Float3;)Z
    .locals 10
    .param p1, "scale"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 957
    const/4 v2, 0x0

    .line 958
    .local v2, "fraction":F
    const/4 v5, 0x0

    .line 959
    .local v5, "stillAnimating":Z
    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    if-eqz v6, :cond_5

    .line 961
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x7d

    sub-long v0, v6, v8

    .line 962
    .local v0, "dt":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-nez v6, :cond_1

    :cond_0
    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    if-eqz v6, :cond_2

    .line 963
    :cond_1
    long-to-float v6, v0

    const/high16 v7, 0x43480000    # 200.0f

    div-float v3, v6, v7

    .line 964
    .local v3, "s":F
    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v3, v6, v7}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v2

    .line 965
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    .line 967
    .end local v3    # "s":F
    :cond_2
    const-wide/16 v6, 0xc8

    cmp-long v6, v0, v6

    if-gez v6, :cond_4

    const/4 v5, 0x1

    .line 980
    .end local v0    # "dt":J
    :cond_3
    :goto_0
    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3e4ccccd    # 0.2f

    mul-float/2addr v7, v2

    add-float v4, v6, v7

    .line 981
    .local v4, "scaleFactor":F
    invoke-virtual {p1, v4, v4, v4}, Lcom/google/android/opengl/common/Float3;->set(FFF)V

    .line 983
    return v5

    .line 967
    .end local v4    # "scaleFactor":F
    .restart local v0    # "dt":J
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 968
    .end local v0    # "dt":J
    :cond_5
    iget-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    if-eqz v6, :cond_3

    .line 970
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    sub-long v0, v6, v8

    .line 971
    .restart local v0    # "dt":J
    const-wide/16 v6, 0xc8

    cmp-long v6, v0, v6

    if-gez v6, :cond_6

    .line 972
    const/high16 v6, 0x3f800000    # 1.0f

    long-to-float v7, v0

    const/high16 v8, 0x43480000    # 200.0f

    div-float/2addr v7, v8

    sub-float v3, v6, v7

    .line 973
    .restart local v3    # "s":F
    const/4 v6, 0x0

    invoke-static {v3, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 974
    const/4 v5, 0x1

    .line 975
    goto :goto_0

    .line 976
    .end local v3    # "s":F
    :cond_6
    const/4 v2, 0x0

    .line 977
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mInScaleAnimation:Z

    goto :goto_0
.end method

.method private getCardTiltAngle(I)F
    .locals 7
    .param p1, "i"    # I

    .prologue
    const/4 v6, 0x0

    .line 1279
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    .line 1280
    .local v1, "rowCount":I
    div-int/2addr p1, v1

    .line 1281
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v5

    add-int/2addr v5, v1

    add-int/lit8 v5, v5, -0x1

    div-int v4, v5, v1

    .line 1282
    .local v4, "totalSlots":I
    const/high16 v3, 0x40a00000    # 5.0f

    .line 1283
    .local v3, "tiltSlotNumber":F
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    div-float v0, v5, v3

    .line 1284
    .local v0, "deltaTilt":F
    const/4 v2, 0x0

    .line 1285
    .local v2, "tiltAngle":F
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    int-to-float v5, p1

    cmpg-float v5, v5, v3

    if-gez v5, :cond_1

    .line 1287
    int-to-float v5, p1

    sub-float v5, v3, v5

    mul-float v2, v0, v5

    .line 1291
    :cond_0
    :goto_0
    return v2

    .line 1288
    :cond_1
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    int-to-float v5, p1

    int-to-float v6, v4

    sub-float/2addr v6, v3

    cmpl-float v5, v5, v6

    if-lez v5, :cond_0

    .line 1289
    sub-int v5, p1, v4

    int-to-float v5, v5

    add-float/2addr v5, v3

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    mul-float v2, v0, v5

    goto :goto_0
.end method

.method private getSwayAngleForVelocity(FZ)F
    .locals 4
    .param p1, "v"    # F
    .param p2, "enableSway"    # Z

    .prologue
    .line 1604
    const/4 v1, 0x0

    .line 1606
    .local v1, "sway":F
    if-eqz p2, :cond_0

    .line 1608
    const v0, 0x40060a92

    .line 1609
    .local v0, "range":F
    neg-float v2, p1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mSwaySensitivity:F

    mul-float/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->logistic(F)F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    mul-float v1, v0, v2

    .line 1612
    .end local v0    # "range":F
    :cond_0
    return v1
.end method

.method private getVerticalOffsetForCard(I)F
    .locals 8
    .param p1, "i"    # I

    .prologue
    const/4 v7, 0x1

    .line 1299
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    .line 1300
    .local v1, "rowCount":I
    if-ne v1, v7, :cond_0

    .line 1302
    const/4 v5, 0x0

    .line 1316
    :goto_0
    return v5

    .line 1304
    :cond_0
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowSpacing:F

    .line 1305
    .local v3, "rowSpacing":F
    sget-object v5, Lcom/google/android/opengl/carousel/Card;->mVerticesData:[F

    const/16 v6, 0xa

    aget v5, v5, v6

    sget-object v6, Lcom/google/android/opengl/carousel/Card;->mVerticesData:[F

    aget v6, v6, v7

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultCardMatrix:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    mul-float v0, v5, v6

    .line 1307
    .local v0, "cardHeight":F
    int-to-float v5, v1

    add-float v6, v0, v3

    mul-float/2addr v5, v6

    sub-float v4, v5, v3

    .line 1308
    .local v4, "totalHeight":F
    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mFirstCardTop:Z

    if-eqz v5, :cond_1

    .line 1309
    rem-int v5, p1, v1

    sub-int v5, v1, v5

    add-int/lit8 p1, v5, -0x1

    .line 1314
    :goto_1
    int-to-float v5, p1

    add-float v6, v0, v3

    mul-float v2, v5, v6

    .line 1316
    .local v2, "rowOffset":F
    sub-float v5, v0, v4

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v5, v2

    goto :goto_0

    .line 1311
    .end local v2    # "rowOffset":F
    :cond_1
    rem-int/2addr p1, v1

    goto :goto_1
.end method

.method private hitAngle(FF)Z
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v2, 0x1

    .line 1342
    new-instance v1, Lcom/google/android/opengl/carousel/Ray;

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-direct {v1, v3}, Lcom/google/android/opengl/carousel/Ray;-><init>(Lcom/google/android/opengl/carousel/CarouselRenderer;)V

    .line 1343
    .local v1, "ray":Lcom/google/android/opengl/carousel/Ray;
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCamera:Lcom/google/android/opengl/carousel/GLCamera;

    invoke-virtual {v1, v3, p1, p2}, Lcom/google/android/opengl/carousel/Ray;->makeRayForPixelAt(Lcom/google/android/opengl/carousel/GLCamera;FF)Z

    .line 1345
    const v3, 0x7cf0bdc2    # 1.0E37f

    iput v3, v1, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 1346
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    if-ne v3, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

    invoke-virtual {v1, v3}, Lcom/google/android/opengl/carousel/Ray;->rayPlaneIntersect(Lcom/google/android/opengl/carousel/Plane;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1348
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    iget-object v3, v1, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0, v3}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 1349
    .local v0, "direction":Lcom/google/android/opengl/common/Float3;
    iget v3, v1, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    invoke-virtual {v0, v3}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 1350
    iget-object v3, v1, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v0, v3}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 1352
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselPlane:Lcom/google/android/opengl/carousel/Plane;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/Plane;->mPoint:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v0, v3}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 1354
    iget v3, v0, Lcom/google/android/opengl/common/Float3;->x:F

    float-to-double v4, v3

    iget v3, v0, Lcom/google/android/opengl/common/Float3;->z:F

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    .line 1368
    .end local v0    # "direction":Lcom/google/android/opengl/common/Float3;
    :goto_0
    return v2

    .line 1356
    :cond_0
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

    invoke-virtual {v1, v3}, Lcom/google/android/opengl/carousel/Ray;->rayCylinderIntersect(Lcom/google/android/opengl/carousel/Cylinder;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1359
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    iget-object v3, v1, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0, v3}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 1360
    .restart local v0    # "direction":Lcom/google/android/opengl/common/Float3;
    iget v3, v1, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    invoke-virtual {v0, v3}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 1361
    iget-object v3, v1, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v0, v3}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 1363
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselCylinder:Lcom/google/android/opengl/carousel/Cylinder;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/Cylinder;->mCenter:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v0, v3}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 1365
    iget v3, v0, Lcom/google/android/opengl/common/Float3;->x:F

    float-to-double v4, v3

    iget v3, v0, Lcom/google/android/opengl/common/Float3;->z:F

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v3, v4

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    goto :goto_0

    .line 1368
    .end local v0    # "direction":Lcom/google/android/opengl/common/Float3;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I
    .locals 9
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "tapCoordinates"    # Lcom/google/android/opengl/common/Float2;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 676
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mDetailsSelectable:Z

    if-eqz v6, :cond_2

    .line 678
    const/4 v1, 0x0

    .local v1, "id":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 679
    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 680
    .local v0, "card":Lcom/google/android/opengl/carousel/Card;
    iget-boolean v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailVisible:Z

    if-nez v6, :cond_1

    .line 678
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 683
    :cond_1
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v7

    iget v2, v6, Lcom/google/android/opengl/common/Float2;->x:F

    .line 684
    .local v2, "x0":F
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v7

    iget v4, v6, Lcom/google/android/opengl/common/Float2;->y:F

    .line 685
    .local v4, "y0":F
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v8

    iget v3, v6, Lcom/google/android/opengl/common/Float2;->x:F

    .line 686
    .local v3, "x1":F
    iget-object v6, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    aget-object v6, v6, v8

    iget v5, v6, Lcom/google/android/opengl/common/Float2;->y:F

    .line 687
    .local v5, "y1":F
    cmpl-float v6, p1, v2

    if-ltz v6, :cond_0

    cmpg-float v6, p1, v3

    if-gtz v6, :cond_0

    cmpl-float v6, p2, v4

    if-ltz v6, :cond_0

    cmpg-float v6, p2, v5

    if-gtz v6, :cond_0

    .line 688
    sub-float v6, p1, v2

    sub-float v7, p2, v4

    invoke-virtual {p3, v6, v7}, Lcom/google/android/opengl/common/Float2;->set(FF)V

    .line 693
    .end local v0    # "card":Lcom/google/android/opengl/carousel/Card;
    .end local v1    # "id":I
    .end local v2    # "x0":F
    .end local v3    # "x1":F
    .end local v4    # "y0":F
    .end local v5    # "y1":F
    :goto_1
    return v1

    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private intersectGeometry(Lcom/google/android/opengl/carousel/Ray;)I
    .locals 13
    .param p1, "ray"    # Lcom/google/android/opengl/carousel/Ray;

    .prologue
    .line 697
    const/4 v7, -0x1

    .line 699
    .local v7, "hit":I
    const/4 v8, 0x0

    .local v8, "id":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_4

    .line 700
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/Card;->mVisible:Z

    if-eqz v1, :cond_3

    .line 701
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/opengl/carousel/Card;

    .line 702
    .local v6, "card":Lcom/google/android/opengl/carousel/Card;
    iget-object v2, v6, Lcom/google/android/opengl/carousel/Card;->mMMatrix:[F

    .line 703
    .local v2, "matrix":[F
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/google/android/opengl/common/Float3;->getArray(I)[Lcom/google/android/opengl/common/Float3;

    move-result-object v11

    .line 706
    .local v11, "p":[Lcom/google/android/opengl/common/Float3;
    const/4 v12, 0x0

    .local v12, "vertex":I
    :goto_1
    const/4 v1, 0x4

    if-ge v12, v1, :cond_1

    .line 707
    invoke-virtual {v6, v12}, Lcom/google/android/opengl/carousel/Card;->getVertexCoord(I)[F

    move-result-object v4

    .line 708
    .local v4, "cardVertices":[F
    const/4 v1, 0x4

    new-array v0, v1, [F

    .line 709
    .local v0, "tmp":[F
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 711
    const/4 v1, 0x3

    aget v1, v0, v1

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 712
    aget-object v1, v11, v12

    const/4 v3, 0x0

    aget v3, v0, v3

    iput v3, v1, Lcom/google/android/opengl/common/Float3;->x:F

    .line 713
    aget-object v1, v11, v12

    const/4 v3, 0x1

    aget v3, v0, v3

    iput v3, v1, Lcom/google/android/opengl/common/Float3;->y:F

    .line 714
    aget-object v1, v11, v12

    const/4 v3, 0x2

    aget v3, v0, v3

    iput v3, v1, Lcom/google/android/opengl/common/Float3;->z:F

    .line 715
    aget-object v1, v11, v12

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v5, 0x3

    aget v5, v0, v5

    div-float/2addr v3, v5

    invoke-virtual {v1, v3}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 706
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 717
    :cond_0
    const-string v1, "CarouselScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad w coord: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 722
    .end local v0    # "tmp":[F
    .end local v4    # "cardVertices":[F
    :cond_1
    const/4 v1, 0x0

    aget-object v1, v11, v1

    const/4 v3, 0x1

    aget-object v3, v11, v3

    const/4 v5, 0x2

    aget-object v5, v11, v5

    invoke-virtual {p1, v1, v3, v5}, Lcom/google/android/opengl/carousel/Ray;->rayTriangleIntersect(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Z

    move-result v9

    .line 723
    .local v9, "is1":Z
    const/4 v1, 0x2

    aget-object v1, v11, v1

    const/4 v3, 0x3

    aget-object v3, v11, v3

    const/4 v5, 0x0

    aget-object v5, v11, v5

    invoke-virtual {p1, v1, v3, v5}, Lcom/google/android/opengl/carousel/Ray;->rayTriangleIntersect(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Z

    move-result v10

    .line 724
    .local v10, "is2":Z
    if-nez v9, :cond_2

    if-eqz v10, :cond_3

    .line 725
    :cond_2
    move v7, v8

    .line 699
    .end local v2    # "matrix":[F
    .end local v6    # "card":Lcom/google/android/opengl/carousel/Card;
    .end local v9    # "is1":Z
    .end local v10    # "is2":Z
    .end local v11    # "p":[Lcom/google/android/opengl/common/Float3;
    .end local v12    # "vertex":I
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 730
    :cond_4
    return v7
.end method

.method private isInMotion(F)Z
    .locals 1
    .param p1, "threshold"    # F

    .prologue
    .line 1134
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logistic(F)F
    .locals 4
    .param p1, "t"    # F

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1600
    neg-float v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method

.method private maximumBias()F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1148
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    div-int v0, v1, v2

    .line 1149
    .local v0, "totalSlots":I
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    if-lez v1, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_0
.end method

.method private minimumBias()F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1140
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    div-int v0, v1, v2

    .line 1141
    .local v0, "totalSlots":I
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    sub-int v1, v0, v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    neg-float v1, v1

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v1

    goto :goto_0
.end method

.method private projectedDelta(FF)F
    .locals 9
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v8, 0x40400000    # 3.0f

    .line 1052
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, p2

    float-to-double v4, v3

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float v3, p1, v3

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v1, v4

    .line 1053
    .local v1, "screenAngle":F
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, p2

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float v4, p1, v4

    invoke-static {v3, v4}, Landroid/graphics/PointF;->length(FF)F

    move-result v2

    .line 1054
    .local v2, "screenDelta":F
    float-to-double v4, v2

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mTrajectoryAngle:F

    sub-float v3, v1, v3

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v0, v4

    .line 1055
    .local v0, "delta":F
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v4, v4

    invoke-static {v3, v4}, Landroid/graphics/PointF;->length(FF)F

    move-result v3

    div-float/2addr v0, v3

    .line 1056
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v3, :cond_0

    .line 1058
    div-float/2addr v0, v8

    .line 1061
    :cond_0
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mEnableBoostArea:Z

    if-eqz v3, :cond_1

    .line 1063
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    add-int/lit8 v3, v3, -0x78

    int-to-float v3, v3

    cmpl-float v3, p1, v3

    if-lez v3, :cond_1

    const/high16 v3, 0x43960000    # 300.0f

    cmpg-float v3, p2, v3

    if-gez v3, :cond_1

    .line 1064
    mul-float/2addr v0, v8

    .line 1068
    :cond_1
    return v0
.end method

.method private radiansToCarouselRotationAngle(F)F
    .locals 6
    .param p1, "angle"    # F

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    int-to-float v0, v0

    neg-float v1, p1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-double v2, v1

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v2, v4

    double-to-float v1, v2

    mul-float/2addr v0, v1

    return v0
.end method

.method private rotateM([FIFFFF)V
    .locals 9
    .param p1, "m"    # [F
    .param p2, "mOffset"    # I
    .param p3, "a"    # F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "z"    # F

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x10

    .line 1332
    sget-object v0, Lcom/google/android/opengl/carousel/CarouselScene;->sRotationTemp:[F

    .local v0, "temp":[F
    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 1333
    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    move-object v2, v0

    move v3, v8

    move-object v4, p1

    move v5, p2

    move-object v6, v0

    move v7, v1

    .line 1334
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1335
    invoke-static {v0, v8, p1, p2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1336
    return-void
.end method

.method private setCardPosition()V
    .locals 8

    .prologue
    const v3, 0x8892

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 347
    const/4 v6, 0x0

    .line 348
    .local v6, "USE_VBO":Z
    if-nez v6, :cond_0

    .line 349
    const/4 v1, 0x3

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVertices:Ljava/nio/FloatBuffer;

    move v3, v0

    move v4, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 360
    :goto_0
    return-void

    .line 354
    :cond_0
    new-array v7, v1, [I

    .line 355
    .local v7, "vboIds":[I
    invoke-static {v1, v7, v0}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 356
    aget v0, v7, v0

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 357
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVerticesData:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVertices:Ljava/nio/FloatBuffer;

    const v2, 0x88e4

    invoke-static {v3, v0, v1, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    goto :goto_0
.end method

.method private slotAngle(F)F
    .locals 4
    .param p1, "p"    # F

    .prologue
    .line 1214
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->wedgeAngle(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 1215
    .local v0, "angle":F
    return v0
.end method

.method private stopAutoscroll()V
    .locals 2

    .prologue
    .line 1551
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    .line 1552
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1553
    return-void
.end method

.method private tiltOverscroll()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 922
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 924
    const/4 v2, 0x0

    .line 938
    :goto_0
    return v2

    .line 928
    :cond_0
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    sub-float v0, v3, v4

    .line 930
    .local v0, "deltaBias":F
    const v3, 0x3ec90fdb

    div-float v3, v0, v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mTiltMaximumAngle:F

    mul-float v1, v3, v4

    .line 932
    .local v1, "tiltAngle":F
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mFillDirection:I

    if-ne v3, v2, :cond_1

    .line 933
    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    goto :goto_0

    .line 935
    :cond_1
    neg-float v3, v1

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    goto :goto_0
.end method

.method private updateCardResources(J)V
    .locals 5
    .param p1, "currentTime"    # J

    .prologue
    .line 423
    const/4 v2, 0x1

    .line 424
    .local v2, "requestLargeTexture":Z
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mTextureVelocityThreshold:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 426
    const/4 v2, 0x0

    .line 431
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 432
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/opengl/carousel/Card;

    .line 433
    .local v0, "card":Lcom/google/android/opengl/carousel/Card;
    iget-boolean v3, v0, Lcom/google/android/opengl/carousel/Card;->mPrefetchTexture:Z

    if-eqz v3, :cond_1

    .line 434
    invoke-virtual {v0, v2}, Lcom/google/android/opengl/carousel/Card;->requestTexture(Z)V

    .line 431
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 436
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/Card;->initCardTexture()V

    goto :goto_1

    .line 439
    .end local v0    # "card":Lcom/google/android/opengl/carousel/Card;
    :cond_2
    return-void
.end method

.method private updateNextPosition(J)Z
    .locals 9
    .param p1, "currentTime"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 1158
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->deltaTimeInSeconds(J)F

    move-result v0

    .line 1159
    .local v0, "dt":F
    cmpg-float v7, v0, v8

    if-gtz v7, :cond_0

    .line 1200
    :goto_0
    return v5

    .line 1162
    :cond_0
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->maximumBias()F

    move-result v1

    .line 1163
    .local v1, "firstBias":F
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->minimumBias()F

    move-result v2

    .line 1165
    .local v2, "lastBias":F
    const/4 v4, 0x0

    .line 1168
    .local v4, "stillAnimating":Z
    iget-boolean v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    if-eqz v7, :cond_4

    .line 1169
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    const v7, 0x3c23d70a    # 0.01f

    cmpl-float v5, v5, v7

    if-lez v5, :cond_2

    .line 1170
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mTiltMaximumAngle:F

    mul-float/2addr v6, v0

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 1171
    const/4 v4, 0x1

    .line 1193
    :cond_1
    :goto_1
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-static {v5, v2, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v3

    .line 1195
    .local v3, "newbias":F
    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    move v5, v4

    .line 1200
    goto :goto_0

    .line 1172
    .end local v3    # "newbias":F
    :cond_2
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    const v7, -0x43dc28f6    # -0.01f

    cmpg-float v5, v5, v7

    if-gez v5, :cond_3

    .line 1173
    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mTiltMaximumAngle:F

    mul-float/2addr v6, v0

    add-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 1174
    const/4 v4, 0x1

    goto :goto_1

    .line 1176
    :cond_3
    iput-boolean v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 1177
    iput v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 1178
    iput v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    goto :goto_1

    .line 1180
    :cond_4
    iget-boolean v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    if-eqz v7, :cond_5

    .line 1181
    long-to-float v5, p1

    invoke-direct {p0, v5}, Lcom/google/android/opengl/carousel/CarouselScene;->doAutoscroll(F)Z

    move-result v4

    goto :goto_1

    .line 1183
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->doPhysics(F)Z

    move-result v4

    .line 1184
    iget v7, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_6

    :goto_2
    iput-boolean v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 1185
    iget-boolean v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    if-eqz v5, :cond_1

    .line 1186
    iput v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 1187
    const/4 v4, 0x1

    goto :goto_1

    :cond_6
    move v5, v6

    .line 1184
    goto :goto_2
.end method

.method private wedgeAngle(F)F
    .locals 4
    .param p1, "cards"    # F

    .prologue
    .line 1073
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p1

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSlotCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method cancelHover()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 800
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 801
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 802
    return-void
.end method

.method cardAngle(F)F
    .locals 3
    .param p1, "p"    # F

    .prologue
    .line 1207
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mRowCount:I

    int-to-float v2, v2

    div-float v2, p1, v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->slotAngle(F)F

    move-result v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    add-float v0, v1, v2

    .line 1208
    .local v0, "angle":F
    return v0
.end method

.method public clearSelection()V
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 222
    return-void
.end method

.method doHover(FF)Z
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v5, -0x1

    .line 775
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v0

    .line 776
    .local v0, "hoverCard":I
    new-instance v3, Lcom/google/android/opengl/common/Float2;

    invoke-direct {v3}, Lcom/google/android/opengl/common/Float2;-><init>()V

    .line 777
    .local v3, "point":Lcom/google/android/opengl/common/Float2;
    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I

    move-result v1

    .line 778
    .local v1, "hoverDetail":I
    const/4 v2, 0x0

    .line 779
    .local v2, "hoverOccur":Z
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    if-eq v0, v4, :cond_0

    .line 780
    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 781
    if-eq v0, v5, :cond_0

    .line 782
    const/4 v2, 0x1

    .line 783
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 786
    :cond_0
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    if-eq v1, v4, :cond_1

    .line 787
    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    .line 788
    if-eq v1, v5, :cond_1

    .line 789
    const/4 v2, 0x1

    .line 790
    iput v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    .line 793
    :cond_1
    return v2
.end method

.method public doMotion(FF)V
    .locals 16
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 874
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOnLongPress:Z

    if-eqz v13, :cond_0

    .line 919
    :goto_0
    return-void

    .line 880
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/carousel/CarouselScene;->maximumBias()F

    move-result v9

    .line 881
    .local v9, "highBias":F
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/carousel/CarouselScene;->minimumBias()F

    move-result v11

    .line 883
    .local v11, "lowBias":F
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 884
    .local v2, "currentTime":J
    invoke-direct/range {p0 .. p2}, Lcom/google/android/opengl/carousel/CarouselScene;->dragFunction(FF)F

    move-result v6

    .line 885
    .local v6, "deltaOmega":F
    move v5, v6

    .line 886
    .local v5, "deltaBias":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v13, v13, Lcom/google/android/opengl/carousel/CarouselSetting;->mAntiJitter:Z

    if-eqz v13, :cond_1

    .line 887
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-nez v13, :cond_1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v13

    const v14, 0x3951b717    # 2.0E-4f

    cmpl-float v13, v13, v14

    if-lez v13, :cond_1

    .line 888
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v13, v13, Lcom/google/android/opengl/carousel/CarouselSetting;->mMaxDeltaBias:F

    neg-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v14, v14, Lcom/google/android/opengl/carousel/CarouselSetting;->mMaxDeltaBias:F

    invoke-static {v6, v13, v14}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v5

    .line 892
    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    add-float/2addr v13, v5

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    .line 893
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    const v14, 0x3ec90fdb

    sub-float v14, v11, v14

    const v15, 0x3ec90fdb

    add-float/2addr v15, v9

    invoke-static {v13, v14, v15}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    .line 896
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    invoke-static {v13, v11, v9}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 897
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/carousel/CarouselScene;->tiltOverscroll()Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 899
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    sub-float v13, p1, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    iget v14, v14, Landroid/graphics/PointF;->y:F

    sub-float v14, p2, v14

    invoke-direct {v4, v13, v14}, Landroid/graphics/PointF;-><init>(FF)V

    .line 900
    .local v4, "delta":Landroid/graphics/PointF;
    iget v13, v4, Landroid/graphics/PointF;->x:F

    iget v14, v4, Landroid/graphics/PointF;->x:F

    mul-float/2addr v13, v14

    iget v14, v4, Landroid/graphics/PointF;->y:F

    iget v15, v4, Landroid/graphics/PointF;->y:F

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    float-to-double v14, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    double-to-float v7, v14

    .line 901
    .local v7, "distance":F
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectionRadius:F

    cmpg-float v13, v7, v13

    if-gez v13, :cond_3

    const/4 v10, 0x1

    .line 902
    .local v10, "inside":Z
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    and-int/2addr v13, v10

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 903
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 905
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->deltaTimeInSeconds(J)F

    move-result v8

    .line 906
    .local v8, "dt":F
    const/4 v13, 0x0

    cmpl-float v13, v8, v13

    if-lez v13, :cond_2

    .line 907
    div-float v13, v6, v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v14, v14, Lcom/google/android/opengl/carousel/CarouselSetting;->mAccelerationFactor:F

    mul-float/2addr v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v14, v14, Lcom/google/android/opengl/carousel/CarouselSetting;->mAccelerationRatio:F

    mul-float v12, v13, v14

    .line 908
    .local v12, "v":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v13, v13, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    neg-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v14, v14, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    invoke-static {v12, v13, v14}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v12

    .line 909
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    rem-int/lit8 v14, v14, 0xa

    aput v12, v13, v14

    .line 910
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    .line 913
    .end local v12    # "v":F
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/carousel/CarouselScene;->computeAverageVelocityFromHistory()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 914
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    goto/16 :goto_0

    .line 901
    .end local v8    # "dt":F
    .end local v10    # "inside":Z
    :cond_3
    const/4 v10, 0x0

    goto :goto_1
.end method

.method public doStart(FF)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 734
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 735
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchPosition:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastPosition:Landroid/graphics/PointF;

    invoke-virtual {v1, v4}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 737
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->hitAngle(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 738
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHitAngle:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    .line 744
    :goto_0
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectionVelocityThreshold:F

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->isInMotion(F)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 746
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 747
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistory:[F

    aput v6, v1, v3

    .line 748
    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    .line 751
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    .line 752
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    .line 753
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchBias:F

    .line 755
    iput-boolean v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    .line 757
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    .line 758
    new-instance v0, Lcom/google/android/opengl/common/Float2;

    invoke-direct {v0}, Lcom/google/android/opengl/common/Float2;-><init>()V

    .line 759
    .local v0, "point":Lcom/google/android/opengl/common/Float2;
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I

    move-result v1

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectedDetail:I

    .line 761
    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 762
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTiltAngle:F

    .line 763
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    iput v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscrollBias:F

    .line 764
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 765
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->stopAutoscroll()V

    .line 766
    return-void

    .line 740
    .end local v0    # "point":Lcom/google/android/opengl/common/Float2;
    :cond_0
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastAngle:F

    goto :goto_0

    :cond_1
    move v1, v3

    .line 744
    goto :goto_1
.end method

.method public doStop(FF)V
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v9, 0x1

    const/4 v5, -0x1

    const/4 v8, 0x0

    .line 806
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v4, :cond_0

    .line 841
    :goto_0
    return-void

    .line 808
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 810
    .local v0, "currentTime":J
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mReleaseTime:J

    .line 811
    iput-boolean v9, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 812
    iget-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-eqz v4, :cond_4

    .line 813
    const/4 v3, -0x1

    .line 814
    .local v3, "selection":I
    new-instance v2, Lcom/google/android/opengl/common/Float2;

    invoke-direct {v2}, Lcom/google/android/opengl/common/Float2;-><init>()V

    .line 815
    .local v2, "point":Lcom/google/android/opengl/common/Float2;
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->intersectDetailTexture(FFLcom/google/android/opengl/common/Float2;)I

    move-result v3

    if-eq v3, v5, :cond_3

    .line 817
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    iget v5, v2, Lcom/google/android/opengl/common/Float2;->x:F

    float-to-int v5, v5

    iget v6, v2, Lcom/google/android/opengl/common/Float2;->y:F

    float-to-int v6, v6

    invoke-interface {v4, v3, v5, v6}, Lcom/google/android/opengl/carousel/CarouselCallback;->onDetailSelected(III)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    .line 825
    :cond_1
    :goto_1
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 838
    .end local v2    # "point":Lcom/google/android/opengl/common/Float2;
    .end local v3    # "selection":I
    :cond_2
    :goto_2
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOnLongPress:Z

    .line 839
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    .line 840
    iput-boolean v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    goto :goto_0

    .line 819
    .restart local v2    # "point":Lcom/google/android/opengl/common/Float2;
    .restart local v3    # "selection":I
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/carousel/CarouselScene;->doSelection(FF)I

    move-result v3

    if-eq v3, v5, :cond_1

    .line 822
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->sendAnimationFinished()V

    .line 823
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v4, v3}, Lcom/google/android/opengl/carousel/CarouselCallback;->onCardSelected(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    goto :goto_1

    .line 827
    .end local v2    # "point":Lcom/google/android/opengl/common/Float2;
    .end local v3    # "selection":I
    :cond_4
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastStopTime:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastStopTime:J

    sub-long v4, v0, v4

    iget-wide v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mStopTimeThreshold:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 828
    iput v8, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityHistoryCount:I

    .line 830
    :cond_5
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->computeAverageVelocityFromHistory()F

    move-result v4

    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 831
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    neg-float v5, v5

    iget-object v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselSetting;->mVelocityUpLimit:F

    invoke-static {v4, v5, v6}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v4

    iput v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 833
    iget v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocityThreshold:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    .line 834
    iput-boolean v9, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    goto :goto_2
.end method

.method public draw()Z
    .locals 9

    .prologue
    const/16 v8, 0xbe2

    const/4 v3, 0x0

    .line 236
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 238
    .local v0, "currentTime":J
    iget-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mTouchTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0xc8

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    const/4 v2, 0x1

    .line 240
    .local v2, "stillAnimating":Z
    :goto_0
    invoke-static {v8}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 241
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBackground:Lcom/google/android/opengl/carousel/Background;

    invoke-virtual {v4, v0, v1}, Lcom/google/android/opengl/carousel/Background;->draw(J)Z

    .line 243
    invoke-static {v8}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 245
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v4, :cond_2

    .line 278
    :cond_0
    :goto_1
    return v3

    .end local v2    # "stillAnimating":Z
    :cond_1
    move v2, v3

    .line 238
    goto :goto_0

    .line 247
    .restart local v2    # "stillAnimating":Z
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 250
    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mAutoRotation:Z

    if-eqz v3, :cond_3

    .line 251
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    float-to-double v4, v3

    const-wide v6, 0x3f60624dd2f1a9fcL    # 0.002

    sub-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRotationAngle:F

    .line 254
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsDragging:Z

    if-nez v3, :cond_4

    .line 255
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->updateNextPosition(J)Z

    move-result v2

    .line 258
    :cond_4
    iput-wide v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mLastTime:J

    .line 260
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->cullCards()I

    .line 261
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->updateCardResources(J)V

    .line 263
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->drawCards(J)Z

    move-result v3

    or-int/2addr v2, v3

    .line 264
    invoke-direct {p0, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->drawDetails(J)Z

    move-result v3

    or-int/2addr v2, v3

    .line 266
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    if-eq v2, v3, :cond_5

    .line 267
    if-eqz v2, :cond_6

    .line 269
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->sendAnimationStarted()V

    .line 274
    :goto_2
    iput-boolean v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 277
    :cond_5
    const-string v3, "CarouselScene.draw"

    invoke-static {v3}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 278
    iget-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    goto :goto_1

    .line 272
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->sendAnimationFinished()V

    goto :goto_2
.end method

.method getAnimatedAlpha(JJ)F
    .locals 7
    .param p1, "startTime"    # J
    .param p3, "currentTime"    # J

    .prologue
    .line 633
    sub-long v4, p3, p1

    long-to-double v2, v4

    .line 634
    .local v2, "timeElapsed":D
    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-wide v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mFadeInDuration:J

    long-to-double v4, v4

    div-double v0, v2, v4

    .line 635
    .local v0, "alpha":D
    const/high16 v4, 0x3f800000    # 1.0f

    double-to-float v5, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    return v4
.end method

.method getCardCount()I
    .locals 1

    .prologue
    .line 1620
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCarouselRotationPosition()I
    .locals 2

    .prologue
    .line 331
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    .line 332
    .local v0, "f":F
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method getFadeOutLeftAlpha(I)F
    .locals 5
    .param p1, "i"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 642
    int-to-float v1, p1

    invoke-virtual {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v0

    .line 643
    .local v0, "angle":F
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mFadeOutLeftAngle:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 644
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/android/opengl/carousel/Card;->mFadeWithEmptyTexture:Z

    .line 645
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    sub-float v1, v0, v1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mFadeOutLeftAngle:F

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 649
    :goto_0
    return v1

    .line 648
    :cond_0
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/opengl/carousel/Card;

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/google/android/opengl/carousel/Card;->mFadeWithEmptyTexture:Z

    move v1, v2

    .line 649
    goto :goto_0
.end method

.method public getFocusedItem()I
    .locals 1

    .prologue
    .line 1439
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    return v0
.end method

.method public getHoverCard()I
    .locals 1

    .prologue
    .line 1447
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverCard:I

    return v0
.end method

.method public getHoverDetail()I
    .locals 1

    .prologue
    .line 1451
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mHoverDetail:I

    return v0
.end method

.method getMatrixForCard([FIZZ)Z
    .locals 15
    .param p1, "matrix"    # [F
    .param p2, "i"    # I
    .param p3, "enableSway"    # Z
    .param p4, "enableCardMatrix"    # Z

    .prologue
    .line 1235
    move/from16 v0, p2

    int-to-float v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v13

    .line 1236
    .local v13, "theta":F
    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    move/from16 v0, p3

    invoke-direct {p0, v2, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->getSwayAngleForVelocity(FZ)F

    move-result v12

    .line 1237
    .local v12, "swayAngle":F
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1238
    const/4 v4, 0x0

    float-to-double v2, v13

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v5, v2

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/opengl/carousel/CarouselScene;->rotateM([FIFFFF)V

    .line 1239
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mRadius:F

    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->getVerticalOffsetForCard(I)F

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1241
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardTiltAngle(I)F

    move-result v14

    .line 1242
    .local v14, "tiltAngle":F
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardRotation:F

    add-float/2addr v2, v12

    add-float v9, v2, v14

    .line 1243
    .local v9, "rotation":F
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardFaceTangent:Z

    if-nez v2, :cond_0

    .line 1244
    sub-float/2addr v9, v13

    .line 1247
    :cond_0
    const/4 v4, 0x0

    float-to-double v2, v9

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v5, v2

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/opengl/carousel/CarouselScene;->rotateM([FIFFFF)V

    .line 1248
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-boolean v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mIsPortrait:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mSpecialRotationInPortrait:Z

    if-eqz v2, :cond_1

    .line 1251
    const/4 v3, 0x0

    const/high16 v4, -0x3eb00000    # -13.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1252
    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1255
    :cond_1
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardXYScale:Lcom/google/android/opengl/common/Float2;

    iget v3, v3, Lcom/google/android/opengl/common/Float2;->x:F

    iget-object v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardXYScale:Lcom/google/android/opengl/common/Float2;

    iget v4, v4, Lcom/google/android/opengl/common/Float2;->y:F

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4, v5}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1256
    const/4 v11, 0x0

    .line 1257
    .local v11, "stillAnimating":Z
    iget v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimatedSelection:I

    move/from16 v0, p2

    if-ne v0, v2, :cond_2

    .line 1258
    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v2, v2, Lcom/google/android/opengl/carousel/CarouselSetting;->mScaleSelectedCard:Z

    if-eqz v2, :cond_2

    .line 1259
    invoke-static {}, Lcom/google/android/opengl/common/Float3;->getUnit()Lcom/google/android/opengl/common/Float3;

    move-result-object v10

    .line 1260
    .local v10, "scale":Lcom/google/android/opengl/common/Float3;
    invoke-direct {p0, v10}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedScaleForSelected(Lcom/google/android/opengl/common/Float3;)Z

    move-result v11

    .line 1261
    const/4 v2, 0x0

    iget v3, v10, Lcom/google/android/opengl/common/Float3;->x:F

    iget v4, v10, Lcom/google/android/opengl/common/Float3;->y:F

    iget v5, v10, Lcom/google/android/opengl/common/Float3;->z:F

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4, v5}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1265
    .end local v10    # "scale":Lcom/google/android/opengl/common/Float3;
    :cond_2
    if-eqz p4, :cond_3

    .line 1266
    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCards:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/opengl/carousel/Card;

    iget-object v6, v2, Lcom/google/android/opengl/carousel/Card;->mClientMatrix:[F

    const/4 v7, 0x0

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1269
    :cond_3
    return v11
.end method

.method public getRealtimeCarouselRotationAngle()F
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    return v0
.end method

.method public sendAnimationFinished()V
    .locals 2

    .prologue
    .line 304
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v1, :cond_0

    .line 314
    :goto_0
    return-void

    .line 310
    :cond_0
    iget v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    .line 313
    .local v0, "angle":F
    iget-object v1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v1, v0}, Lcom/google/android/opengl/carousel/CarouselCallback;->onAnimationFinished(F)V

    goto :goto_0
.end method

.method sendAnimationStarted()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    invoke-interface {v0}, Lcom/google/android/opengl/carousel/CarouselCallback;->onAnimationStarted()V

    .line 298
    :cond_0
    return-void
.end method

.method public setCarouselRotationAngle(F)V
    .locals 2
    .param p1, "angle"    # F

    .prologue
    .line 1466
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->getCardCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselRotationAngle:F

    .line 1467
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->updateCarouselRotationAngle()V

    .line 1468
    return-void
.end method

.method setCarouselRotationAngle(FIIF)V
    .locals 7
    .param p1, "endAngle"    # F
    .param p2, "milliseconds"    # I
    .param p3, "interpolationMode"    # I
    .param p4, "maxAnimatedArc"    # F

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1515
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    invoke-direct {p0, v3}, Lcom/google/android/opengl/carousel/CarouselScene;->radiansToCarouselRotationAngle(F)F

    move-result v0

    .line 1517
    .local v0, "actualStart":F
    cmpl-float v3, p4, v6

    if-lez v3, :cond_0

    .line 1519
    cmpg-float v3, v0, p1

    if-gtz v3, :cond_1

    .line 1520
    sub-float v3, p1, p4

    cmpg-float v3, v0, v3

    if-gez v3, :cond_0

    .line 1521
    sub-float v0, p1, p4

    .line 1530
    :cond_0
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAnimating:Z

    .line 1531
    iput-boolean v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mIsAutoScrolling:Z

    .line 1532
    int-to-double v4, p2

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollDuration:D

    .line 1533
    iput p3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollInterpolationMode:I

    .line 1534
    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->carouselRotationAngleToRadians(F)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    .line 1535
    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/CarouselScene;->carouselRotationAngleToRadians(F)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    .line 1538
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->maximumBias()F

    move-result v1

    .line 1539
    .local v1, "highBias":F
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/CarouselScene;->minimumBias()F

    move-result v2

    .line 1540
    .local v2, "lowBias":F
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    invoke-static {v3, v2, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartAngle:F

    .line 1541
    iget v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    invoke-static {v3, v2, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStopAngle:F

    .line 1544
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mAutoscrollStartTime:D

    .line 1545
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mOverscroll:Z

    .line 1546
    iput v6, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mVelocity:F

    .line 1547
    return-void

    .line 1524
    .end local v1    # "highBias":F
    .end local v2    # "lowBias":F
    :cond_1
    add-float v3, p1, p4

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    .line 1525
    add-float v0, p1, p4

    goto :goto_0
.end method

.method public setFocusedItem(I)V
    .locals 0
    .param p1, "focusedItem"    # I

    .prologue
    .line 1443
    iput p1, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mFocusedItem:I

    .line 1444
    return-void
.end method

.method updateCarouselRotationAngle()V
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mCarouselRotationAngle:F

    invoke-direct {p0, v0}, Lcom/google/android/opengl/carousel/CarouselScene;->carouselRotationAngleToRadians(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/opengl/carousel/CarouselScene;->mBias:F

    .line 289
    return-void
.end method
