.class public Lcom/google/android/opengl/carousel/Card;
.super Ljava/lang/Object;
.source "Card.java"


# static fields
.field private static FADE_THRESHOLD:F

.field public static final mVerticesData:[F

.field private static sGlowMatrix:[F

.field private static sMatrix:[F

.field private static sModelMatrix:[F

.field private static sOneVertex:[F

.field private static sOtherScreenLeft:[F

.field private static sOtherScreenRight:[F

.field private static sScreenCoord:[F

.field private static sScreenCoordLeft:[F

.field private static sScreenCoordRight:[F

.field private static sVertices:[F


# instance fields
.field public mClientMatrix:[F

.field public mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

.field public mDetailTextureOffset:Lcom/google/android/opengl/common/Float2;

.field public mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

.field public mDetailTextureState:I

.field public mDetailVisible:Z

.field mFadeWithEmptyTexture:Z

.field public mId:I

.field public mMMatrix:[F

.field private mMVPMatrix:[F

.field public mPrefetchTexture:Z

.field private mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field private mScene:Lcom/google/android/opengl/carousel/CarouselScene;

.field private mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

.field public mTextureState:I

.field public mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

.field public mVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/16 v2, 0x10

    const/4 v1, 0x4

    .line 29
    new-array v0, v3, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->mVerticesData:[F

    .line 68
    const v0, 0x3f333333    # 0.7f

    sput v0, Lcom/google/android/opengl/carousel/Card;->FADE_THRESHOLD:F

    .line 75
    new-array v0, v2, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sMatrix:[F

    .line 76
    new-array v0, v2, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sModelMatrix:[F

    .line 77
    new-array v0, v3, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sVertices:[F

    .line 78
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sScreenCoord:[F

    .line 79
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sScreenCoordLeft:[F

    .line 80
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sScreenCoordRight:[F

    .line 81
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sOtherScreenLeft:[F

    .line 82
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sOtherScreenRight:[F

    .line 83
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sOneVertex:[F

    .line 84
    new-array v0, v2, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sGlowMatrix:[F

    return-void

    .line 29
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private bindFadingTexture()V
    .locals 2

    .prologue
    const/16 v1, 0xde1

    .line 184
    iget-boolean v0, p0, Lcom/google/android/opengl/carousel/Card;->mFadeWithEmptyTexture:Z

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselTexture;->mEmptyId:I

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselTexture;->mLoadingId:I

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_0
.end method

.method private drawRuler([F)V
    .locals 20
    .param p1, "screenCoord"    # [F

    .prologue
    .line 411
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v14, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDetailAlignment:I

    .line 412
    .local v14, "detailTextureAlignment":I
    const/high16 v19, 0x40a00000    # 5.0f

    .line 414
    .local v19, "yPadding":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mDefaultLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v0, v1

    move/from16 v16, v0

    .line 419
    .local v16, "lineWidth":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v1, v1, Lcom/google/android/opengl/carousel/CarouselSetting;->mMusicPortraitOfLabel:Z

    if-eqz v1, :cond_0

    .line 420
    const/4 v1, 0x1

    aget v1, p1, v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mPortriatRulerHeight:F

    add-float v18, v1, v4

    .line 421
    .local v18, "rulerTop":F
    const/4 v1, 0x1

    aget v17, p1, v1

    .line 429
    .local v17, "rulerBottom":F
    :goto_0
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v15, v16, v1

    .line 430
    .local v15, "halfWidth":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v1, v1, Lcom/google/android/opengl/common/Float2;->x:F

    const/4 v4, 0x0

    aget v4, p1, v4

    add-float/2addr v1, v4

    sub-float v2, v1, v15

    .line 431
    .local v2, "x0":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v1, v1, Lcom/google/android/opengl/common/Float2;->x:F

    const/4 v4, 0x0

    aget v4, p1, v4

    add-float/2addr v1, v4

    add-float v5, v1, v15

    .line 432
    .local v5, "x1":F
    add-float v3, v17, v19

    .line 433
    .local v3, "y0":F
    sub-float v1, v18, v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v4, v4, Lcom/google/android/opengl/common/Float2;->y:F

    sub-float v9, v1, v4

    .line 435
    .local v9, "y1":F
    const v1, 0x84c0

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 436
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    if-ne v1, v4, :cond_2

    .line 437
    const/16 v1, 0xde1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselTexture;->mDefaultLineId:I

    invoke-static {v1, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 444
    :goto_1
    sget-object v1, Lcom/google/android/opengl/carousel/Card;->sVertices:[F

    const/4 v4, 0x2

    aget v4, p1, v4

    const/4 v6, 0x2

    aget v7, p1, v6

    const/4 v6, 0x2

    aget v10, p1, v6

    const/4 v6, 0x2

    aget v13, p1, v6

    move v6, v3

    move v8, v5

    move v11, v2

    move v12, v9

    invoke-static/range {v1 .. v13}, Lcom/google/android/opengl/carousel/GL2Helper;->setVector12f([FFFFFFFFFFFFF)V

    .line 447
    sget-object v1, Lcom/google/android/opengl/carousel/Card;->sVertices:[F

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuad([F)Z

    .line 448
    return-void

    .line 422
    .end local v2    # "x0":F
    .end local v3    # "y0":F
    .end local v5    # "x1":F
    .end local v9    # "y1":F
    .end local v15    # "halfWidth":F
    .end local v17    # "rulerBottom":F
    .end local v18    # "rulerTop":F
    :cond_0
    and-int/lit8 v1, v14, 0x10

    if-lez v1, :cond_1

    .line 423
    const/4 v1, 0x1

    aget v18, p1, v1

    .line 424
    .restart local v18    # "rulerTop":F
    const/16 v17, 0x0

    .restart local v17    # "rulerBottom":F
    goto :goto_0

    .line 426
    .end local v17    # "rulerBottom":F
    .end local v18    # "rulerTop":F
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v0, v1

    move/from16 v18, v0

    .line 427
    .restart local v18    # "rulerTop":F
    const/4 v1, 0x1

    aget v17, p1, v1

    .restart local v17    # "rulerBottom":F
    goto :goto_0

    .line 439
    .restart local v2    # "x0":F
    .restart local v3    # "y0":F
    .restart local v5    # "x1":F
    .restart local v9    # "y1":F
    .restart local v15    # "halfWidth":F
    :cond_2
    const/16 v1, 0xde1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselTexture;->mDetailLoadingId:I

    invoke-static {v1, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 440
    const v1, 0x84c1

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 441
    const/16 v1, 0xde1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v4, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselTexture;->mDefaultLineId:I

    invoke-static {v1, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_1
.end method

.method private getBlendedAlpha(F)F
    .locals 8
    .param p1, "animatedAlpha"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 388
    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselSetting;->mVisibleDetailCount:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget v6, v6, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    mul-float/2addr v5, v6

    add-float v3, v4, v5

    .line 390
    .local v3, "startDetailFadeAngle":F
    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget v4, v4, Lcom/google/android/opengl/carousel/CarouselScene;->mDetailFadeRate:F

    iget-object v5, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget v5, v5, Lcom/google/android/opengl/carousel/CarouselScene;->mWedgeAngle:F

    mul-float/2addr v4, v5

    add-float v1, v3, v4

    .line 392
    .local v1, "endDetailFadeAngle":F
    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget v5, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/google/android/opengl/carousel/CarouselScene;->cardAngle(F)F

    move-result v4

    sub-float v4, v1, v4

    sub-float v5, v1, v3

    div-float v2, v4, v5

    .line 395
    .local v2, "positionAlpha":F
    const/4 v4, 0x0

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 396
    invoke-static {v7, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 398
    mul-float v4, p1, v2

    iget-object v5, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget v6, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-virtual {v5, v6}, Lcom/google/android/opengl/carousel/CarouselScene;->getFadeOutLeftAlpha(I)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v7, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 401
    .local v0, "blendedAlpha":F
    return v0
.end method

.method private removeAllTextures(Z)V
    .locals 3
    .param p1, "clientInvalidate"    # Z

    .prologue
    const/4 v2, 0x0

    .line 488
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/RequestableTexture;->release()V

    .line 489
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/RequestableTexture;->release()V

    .line 491
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v0, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    iget v0, p0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    if-eqz v0, :cond_3

    .line 494
    if-eqz p1, :cond_2

    .line 495
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/CarouselCallback;->onInvalidateTexture(I)V

    .line 497
    :cond_2
    iput v2, p0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    .line 500
    :cond_3
    iget v0, p0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureState:I

    if-eqz v0, :cond_0

    .line 501
    if-eqz p1, :cond_4

    .line 502
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/CarouselCallback;->onInvalidateDetailTexture(I)V

    .line 505
    :cond_4
    iput v2, p0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureState:I

    goto :goto_0
.end method

.method private setupShaders(ZF)V
    .locals 8
    .param p1, "doHighlight"    # Z
    .param p2, "fadeAmount"    # F

    .prologue
    const v7, 0x84c0

    const/16 v6, 0xde1

    const/4 v3, 0x0

    .line 112
    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexHighlightProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 114
    .local v2, "singleTex":Lcom/google/android/opengl/carousel/GLProgram;
    :goto_0
    if-eqz p1, :cond_1

    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexHighlightProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 117
    .local v1, "multiTex":Lcom/google/android/opengl/carousel/GLProgram;
    :goto_1
    iget v4, p0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/google/android/opengl/carousel/RequestableTexture;->isTextureIdAllocated()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    .line 119
    .local v0, "loaded":Z
    :goto_2
    if-nez v0, :cond_3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    aget-object v3, v4, v3

    invoke-virtual {v3}, Lcom/google/android/opengl/carousel/RequestableTexture;->isTextureIdAllocated()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 121
    iget-object v3, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-virtual {v3, v2}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 122
    invoke-static {v7}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/Card;->getTextureId()I

    move-result v3

    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 145
    :goto_3
    return-void

    .line 112
    .end local v0    # "loaded":Z
    .end local v1    # "multiTex":Lcom/google/android/opengl/carousel/GLProgram;
    .end local v2    # "singleTex":Lcom/google/android/opengl/carousel/GLProgram;
    :cond_0
    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    goto :goto_0

    .line 114
    .restart local v2    # "singleTex":Lcom/google/android/opengl/carousel/GLProgram;
    :cond_1
    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v4, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    goto :goto_1

    .restart local v1    # "multiTex":Lcom/google/android/opengl/carousel/GLProgram;
    :cond_2
    move v0, v3

    .line 117
    goto :goto_2

    .line 124
    .restart local v0    # "loaded":Z
    :cond_3
    sget v3, Lcom/google/android/opengl/carousel/Card;->FADE_THRESHOLD:F

    cmpl-float v3, p2, v3

    if-gtz v3, :cond_4

    const v3, 0x3c23d70a    # 0.01f

    cmpg-float v3, p2, v3

    if-ltz v3, :cond_4

    if-nez v0, :cond_6

    .line 126
    :cond_4
    iget-object v3, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-virtual {v3, v2}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 127
    invoke-static {v7}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 130
    if-eqz v0, :cond_5

    sget v3, Lcom/google/android/opengl/carousel/Card;->FADE_THRESHOLD:F

    cmpl-float v3, p2, v3

    if-lez v3, :cond_5

    .line 131
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/Card;->getTextureId()I

    move-result v3

    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_3

    .line 133
    :cond_5
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/Card;->bindFadingTexture()V

    goto :goto_3

    .line 137
    :cond_6
    iget-object v3, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-virtual {v3, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 138
    invoke-static {v7}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/opengl/carousel/Card;->bindFadingTexture()V

    .line 141
    const v3, 0x84c1

    invoke-static {v3}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 142
    iget-object v3, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    invoke-virtual {v3, p2}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setFadeAmount(F)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/Card;->getTextureId()I

    move-result v3

    invoke-static {v6, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_3
.end method


# virtual methods
.method public draw(FF)V
    .locals 8
    .param p1, "fadeAmount"    # F
    .param p2, "highlightAlpha"    # F

    .prologue
    const v7, 0x3e99999a    # 0.3f

    const/4 v1, 0x0

    .line 153
    invoke-direct {p0, v1, p1}, Lcom/google/android/opengl/carousel/Card;->setupShaders(ZF)V

    .line 154
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mMVPMatrix:[F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVPMatrix:[F

    iget-object v4, p0, Lcom/google/android/opengl/carousel/Card;->mMMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 155
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/Card;->mMVPMatrix:[F

    invoke-virtual {v0, v2}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setMVPUniform([F)V

    .line 156
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/ICardRenderer;->draw(I)V

    .line 159
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    .line 160
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/opengl/carousel/Card;->setupShaders(ZF)V

    .line 161
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, p0, Lcom/google/android/opengl/carousel/Card;->mMVPMatrix:[F

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setMVPUniform([F)V

    .line 165
    mul-float v0, p2, v7

    add-float v6, v7, v0

    .line 168
    .local v6, "adjustedAlpha":F
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    const v1, 0x3e4ccccd    # 0.2f

    const v2, 0x3f35b5b6

    const v3, 0x3f65e5e6

    invoke-virtual {v0, v1, v2, v3, v6}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setHighlight(FFFF)V

    .line 171
    .end local v6    # "adjustedAlpha":F
    :cond_0
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/ICardRenderer;->drawHighlightable(I)V

    .line 176
    :goto_0
    const-string v0, "Card.draw"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 177
    return-void

    .line 173
    :cond_1
    const/4 v0, 0x6

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_0
.end method

.method public drawDetails(J[F)Z
    .locals 35
    .param p1, "currentTime"    # J
    .param p3, "projection"    # [F

    .prologue
    .line 222
    const/16 v30, 0x0

    .line 223
    .local v30, "stillAnimating":Z
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailVisible:Z

    .line 226
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v10, 0x1

    aget-object v7, v7, v10

    invoke-virtual {v7}, Lcom/google/android/opengl/carousel/RequestableTexture;->isTextureIdAllocated()Z

    move-result v7

    if-nez v7, :cond_0

    .line 227
    const/4 v7, 0x0

    .line 379
    :goto_0
    return v7

    .line 232
    :cond_0
    sget-object v2, Lcom/google/android/opengl/carousel/Card;->sMatrix:[F

    .line 233
    .local v2, "matrix":[F
    monitor-enter v2

    .line 235
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    sget-object v10, Lcom/google/android/opengl/carousel/Card;->sModelMatrix:[F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/opengl/carousel/Card;->mId:I

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7, v10, v12, v13, v14}, Lcom/google/android/opengl/carousel/CarouselScene;->getMatrixForCard([FIZZ)Z

    .line 236
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v4, v7, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVPMatrix:[F

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/opengl/carousel/Card;->sModelMatrix:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 239
    sget-object v29, Lcom/google/android/opengl/carousel/Card;->sScreenCoord:[F

    .line 241
    .local v29, "screenCoord":[F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v0, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mDetailAlignment:I

    move/from16 v23, v0

    .line 242
    .local v23, "detailTextureAlignment":I
    and-int/lit8 v7, v23, 0x10

    if-lez v7, :cond_2

    .line 243
    const/16 v25, 0x0

    .line 244
    .local v25, "indexLeft":I
    const/16 v26, 0x1

    .line 250
    .local v26, "indexRight":I
    :goto_1
    sget-object v3, Lcom/google/android/opengl/carousel/Card;->sScreenCoordLeft:[F

    .line 251
    .local v3, "screenCoordLeft":[F
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/Card;->getVertexCoord(I)[F

    move-result-object v7

    const/4 v8, 0x0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 252
    sget-object v4, Lcom/google/android/opengl/carousel/Card;->sScreenCoordRight:[F

    .line 253
    .local v4, "screenCoordRight":[F
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/Card;->getVertexCoord(I)[F

    move-result-object v8

    const/4 v9, 0x0

    move-object v6, v2

    invoke-static/range {v4 .. v9}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 255
    const/4 v7, 0x3

    aget v7, v3, v7

    const/4 v10, 0x0

    cmpl-float v7, v7, v10

    if-eqz v7, :cond_1

    const/4 v7, 0x3

    aget v7, v4, v7

    const/4 v10, 0x0

    cmpl-float v7, v7, v10

    if-nez v7, :cond_3

    .line 257
    :cond_1
    const-string v7, "Card"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Bad transform: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    const/4 v7, 0x0

    monitor-exit v2

    goto/16 :goto_0

    .line 377
    .end local v3    # "screenCoordLeft":[F
    .end local v4    # "screenCoordRight":[F
    .end local v23    # "detailTextureAlignment":I
    .end local v25    # "indexLeft":I
    .end local v26    # "indexRight":I
    .end local v29    # "screenCoord":[F
    :catchall_0
    move-exception v7

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 246
    .restart local v23    # "detailTextureAlignment":I
    .restart local v29    # "screenCoord":[F
    :cond_2
    const/16 v25, 0x3

    .line 247
    .restart local v25    # "indexLeft":I
    const/16 v26, 0x2

    .restart local v26    # "indexRight":I
    goto :goto_1

    .line 261
    .restart local v3    # "screenCoordLeft":[F
    .restart local v4    # "screenCoordRight":[F
    :cond_3
    and-int/lit8 v7, v23, 0x1

    if-lez v7, :cond_4

    .line 263
    and-int/lit8 v7, v23, 0x10

    if-lez v7, :cond_9

    .line 264
    const/16 v25, 0x3

    .line 265
    const/16 v26, 0x2

    .line 270
    :goto_2
    :try_start_1
    sget-object v5, Lcom/google/android/opengl/carousel/Card;->sOtherScreenLeft:[F

    .line 271
    .local v5, "otherScreenLeft":[F
    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/Card;->getVertexCoord(I)[F

    move-result-object v9

    const/4 v10, 0x0

    move-object v7, v2

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 272
    sget-object v6, Lcom/google/android/opengl/carousel/Card;->sOtherScreenRight:[F

    .line 273
    .local v6, "otherScreenRight":[F
    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/carousel/Card;->getVertexCoord(I)[F

    move-result-object v10

    const/4 v11, 0x0

    move-object v8, v2

    invoke-static/range {v6 .. v11}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 275
    const/4 v7, 0x1

    const/4 v10, 0x1

    const/4 v12, 0x1

    aget v12, v3, v12

    const/4 v13, 0x1

    aget v13, v4, v13

    add-float/2addr v12, v13

    const/4 v13, 0x1

    aget v13, v5, v13

    add-float/2addr v12, v13

    const/4 v13, 0x1

    aget v13, v6, v13

    add-float/2addr v12, v13

    const/high16 v13, 0x40800000    # 4.0f

    div-float/2addr v12, v13

    aput v12, v3, v10

    aput v12, v4, v7

    .line 278
    .end local v5    # "otherScreenLeft":[F
    .end local v6    # "otherScreenRight":[F
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v7, v7, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v0, v7

    move/from16 v34, v0

    .line 279
    .local v34, "width":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v7, v7, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v0, v7

    move/from16 v24, v0

    .line 280
    .local v24, "height":F
    move/from16 v0, v34

    move/from16 v1, v24

    invoke-static {v3, v0, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->convertNormalizedToPixelCoordinates([FFF)Z

    .line 281
    move/from16 v0, v34

    move/from16 v1, v24

    invoke-static {v4, v0, v1}, Lcom/google/android/opengl/carousel/GL2Helper;->convertNormalizedToPixelCoordinates([FFF)Z

    .line 283
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v10, 0x1

    aget-object v7, v7, v10

    invoke-virtual {v7}, Lcom/google/android/opengl/carousel/RequestableTexture;->width()I

    move-result v7

    int-to-float v0, v7

    move/from16 v33, v0

    .line 284
    .local v33, "textureWidth":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v10, 0x1

    aget-object v7, v7, v10

    invoke-virtual {v7}, Lcom/google/android/opengl/carousel/RequestableTexture;->height()I

    move-result v7

    int-to-float v0, v7

    move/from16 v31, v0

    .line 286
    .local v31, "textureHeight":F
    move-object/from16 v29, v3

    .line 287
    and-int/lit8 v7, v23, 0x10

    if-lez v7, :cond_a

    .line 288
    const/4 v7, 0x1

    const/4 v10, 0x1

    aget v10, v3, v10

    const/4 v12, 0x1

    aget v12, v4, v12

    invoke-static {v10, v12}, Ljava/lang/Math;->min(FF)F

    move-result v10

    aput v10, v29, v7

    .line 292
    :cond_5
    :goto_3
    move/from16 v0, v23

    and-int/lit16 v7, v0, 0x100

    if-lez v7, :cond_6

    .line 293
    const/4 v7, 0x0

    aget v10, v29, v7

    const/4 v12, 0x0

    aget v12, v4, v12

    const/4 v13, 0x0

    aget v13, v3, v13

    sub-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v33, v13

    sub-float/2addr v12, v13

    add-float/2addr v10, v12

    aput v10, v29, v7

    .line 298
    :cond_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mMusicPortraitOfLabel:Z

    if-eqz v7, :cond_7

    .line 299
    const v7, 0x3dcccccd    # 0.1f

    invoke-static {v3, v4, v7}, Lcom/google/android/opengl/carousel/GL2Helper;->proportionPoint([F[FF)[F

    move-result-object v29

    .line 304
    :cond_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v12, 0x1

    aget-object v10, v10, v12

    invoke-virtual {v10}, Lcom/google/android/opengl/carousel/RequestableTexture;->getChangeTime()J

    move-result-wide v12

    move-wide/from16 v0, p1

    invoke-virtual {v7, v12, v13, v0, v1}, Lcom/google/android/opengl/carousel/CarouselScene;->getAnimatedAlpha(JJ)F

    move-result v20

    .line 306
    .local v20, "animatedAlpha":F
    const/high16 v7, 0x3f800000    # 1.0f

    cmpg-float v7, v20, v7

    if-gez v7, :cond_8

    .line 307
    const/16 v30, 0x1

    .line 310
    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/opengl/carousel/Card;->getBlendedAlpha(F)F

    move-result v21

    .line 311
    .local v21, "blendedAlpha":F
    const/4 v7, 0x0

    cmpl-float v7, v21, v7

    if-nez v7, :cond_b

    .line 312
    const/4 v7, 0x0

    monitor-exit v2

    goto/16 :goto_0

    .line 267
    .end local v20    # "animatedAlpha":F
    .end local v21    # "blendedAlpha":F
    .end local v24    # "height":F
    .end local v31    # "textureHeight":F
    .end local v33    # "textureWidth":F
    .end local v34    # "width":F
    :cond_9
    const/16 v25, 0x0

    .line 268
    const/16 v26, 0x1

    goto/16 :goto_2

    .line 289
    .restart local v24    # "height":F
    .restart local v31    # "textureHeight":F
    .restart local v33    # "textureWidth":F
    .restart local v34    # "width":F
    :cond_a
    and-int/lit8 v7, v23, 0x1

    if-lez v7, :cond_5

    .line 290
    const/4 v7, 0x1

    aget v10, v29, v7

    float-to-double v12, v10

    move/from16 v0, v31

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    div-double v16, v16, v18

    sub-double v12, v12, v16

    double-to-float v10, v12

    aput v10, v29, v7

    goto :goto_3

    .line 315
    .restart local v20    # "animatedAlpha":F
    .restart local v21    # "blendedAlpha":F
    :cond_b
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailVisible:Z

    .line 317
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mDrawRuler:Z

    if-eqz v7, :cond_c

    .line 318
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/google/android/opengl/carousel/Card;->drawRuler([F)V

    .line 322
    :cond_c
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureOffset:Lcom/google/android/opengl/common/Float2;

    iget v0, v7, Lcom/google/android/opengl/common/Float2;->x:F

    move/from16 v27, v0

    .line 323
    .local v27, "offx":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureOffset:Lcom/google/android/opengl/common/Float2;

    iget v7, v7, Lcom/google/android/opengl/common/Float2;->y:F

    neg-float v0, v7

    move/from16 v28, v0

    .line 325
    .local v28, "offy":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mMusicPortraitOfLabel:Z

    if-eqz v7, :cond_e

    .line 326
    const/4 v7, 0x1

    aget v7, v29, v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mPortriatRulerHeight:F

    add-float v32, v7, v10

    .line 335
    .local v32, "textureTop":F
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mMusicPortraitOfLabel:Z

    if-eqz v7, :cond_10

    .line 336
    const/4 v7, 0x0

    aget v7, v29, v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v10, v10, Lcom/google/android/opengl/common/Float2;->x:F

    sub-float/2addr v7, v10

    sub-float v7, v7, v27

    sub-float v8, v7, v33

    .line 337
    .local v8, "x0":F
    const/4 v7, 0x1

    aget v7, v29, v7

    add-float v7, v7, v28

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v10, v10, Lcom/google/android/opengl/common/Float2;->y:F

    sub-float/2addr v7, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v10, v10, Lcom/google/android/opengl/carousel/CarouselSetting;->mPortriatRulerHeight:F

    add-float/2addr v7, v10

    sub-float v9, v7, v31

    .line 344
    .local v9, "y0":F
    :goto_5
    add-float v11, v8, v33

    .line 345
    .local v11, "x1":F
    add-float v15, v9, v31

    .line 347
    .local v15, "y1":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    const/4 v10, 0x0

    aget-object v7, v7, v10

    iput v8, v7, Lcom/google/android/opengl/common/Float2;->x:F

    .line 348
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    const/4 v10, 0x0

    aget-object v7, v7, v10

    sub-float v10, v24, v15

    iput v10, v7, Lcom/google/android/opengl/common/Float2;->y:F

    .line 349
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    const/4 v10, 0x1

    aget-object v7, v7, v10

    iput v11, v7, Lcom/google/android/opengl/common/Float2;->x:F

    .line 350
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailTexturePosition:[Lcom/google/android/opengl/common/Float2;

    const/4 v10, 0x1

    aget-object v7, v7, v10

    sub-float v10, v24, v9

    iput v10, v7, Lcom/google/android/opengl/common/Float2;->y:F

    .line 352
    sget-object v7, Lcom/google/android/opengl/carousel/Card;->sVertices:[F

    const/4 v10, 0x2

    aget v10, v29, v10

    const/4 v12, 0x2

    aget v13, v29, v12

    const/4 v12, 0x2

    aget v16, v29, v12

    const/4 v12, 0x2

    aget v19, v29, v12

    move v12, v9

    move v14, v11

    move/from16 v17, v8

    move/from16 v18, v15

    invoke-static/range {v7 .. v19}, Lcom/google/android/opengl/carousel/GL2Helper;->setVector12f([FFFFFFFFFFFFF)V

    .line 356
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-boolean v7, v7, Lcom/google/android/opengl/carousel/CarouselSetting;->mHighlightDetail:Z

    if-eqz v7, :cond_d

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-boolean v7, v7, Lcom/google/android/opengl/carousel/CarouselScene;->mEnableSelection:Z

    if-eqz v7, :cond_d

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget v7, v7, Lcom/google/android/opengl/carousel/CarouselScene;->mSelectedDetail:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/opengl/carousel/Card;->mId:I

    if-ne v7, v10, :cond_d

    .line 358
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v10, v10, Lcom/google/android/opengl/carousel/CarouselRenderer;->mColorProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v7, v10}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 359
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setMVPUniform([F)V

    .line 360
    sget-object v22, Lcom/google/android/opengl/carousel/CarouselSetting;->LABEL_HIGHLIGHT_COLOR:Lcom/google/android/opengl/common/Float4;

    .line 361
    .local v22, "color":Lcom/google/android/opengl/common/Float4;
    const/4 v7, 0x2

    move-object/from16 v0, v22

    iget v10, v0, Lcom/google/android/opengl/common/Float4;->x:F

    move-object/from16 v0, v22

    iget v12, v0, Lcom/google/android/opengl/common/Float4;->y:F

    move-object/from16 v0, v22

    iget v13, v0, Lcom/google/android/opengl/common/Float4;->z:F

    move-object/from16 v0, v22

    iget v14, v0, Lcom/google/android/opengl/common/Float4;->w:F

    invoke-static {v7, v10, v12, v13, v14}, Landroid/opengl/GLES20;->glVertexAttrib4f(IFFFF)V

    .line 362
    sget-object v7, Lcom/google/android/opengl/carousel/Card;->sVertices:[F

    invoke-static {v7}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuad([F)Z

    .line 365
    .end local v22    # "color":Lcom/google/android/opengl/common/Float4;
    :cond_d
    const v7, 0x3f7d70a4    # 0.99f

    cmpl-float v7, v21, v7

    if-lez v7, :cond_11

    .line 366
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v10, v10, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v7, v10}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 373
    :goto_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setMVPUniform([F)V

    .line 374
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/opengl/carousel/Card;->setGlDetailTexture()V

    .line 376
    sget-object v7, Lcom/google/android/opengl/carousel/Card;->sVertices:[F

    invoke-static {v7}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuad([F)Z

    .line 377
    monitor-exit v2

    move/from16 v7, v30

    .line 379
    goto/16 :goto_0

    .line 327
    .end local v8    # "x0":F
    .end local v9    # "y0":F
    .end local v11    # "x1":F
    .end local v15    # "y1":F
    .end local v32    # "textureTop":F
    :cond_e
    and-int/lit8 v7, v23, 0x2

    if-lez v7, :cond_f

    .line 328
    move/from16 v32, v24

    .restart local v32    # "textureTop":F
    goto/16 :goto_4

    .line 330
    .end local v32    # "textureTop":F
    :cond_f
    const/4 v7, 0x1

    aget v32, v29, v7

    .restart local v32    # "textureTop":F
    goto/16 :goto_4

    .line 340
    :cond_10
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v7, v7, Lcom/google/android/opengl/common/Float2;->x:F

    const/4 v10, 0x0

    aget v10, v29, v10

    add-float/2addr v7, v10

    add-float v8, v7, v27

    .line 341
    .restart local v8    # "x0":F
    add-float v7, v32, v28

    sub-float v7, v7, v31

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v10, v10, Lcom/google/android/opengl/common/Float2;->y:F

    sub-float v9, v7, v10

    .line 342
    .restart local v9    # "y0":F
    add-float v7, v32, v28

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mDetailLineOffset:Lcom/google/android/opengl/common/Float2;

    iget v10, v10, Lcom/google/android/opengl/common/Float2;->y:F

    sub-float/2addr v7, v10

    goto/16 :goto_5

    .line 368
    .restart local v11    # "x1":F
    .restart local v15    # "y1":F
    :cond_11
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v10, v10, Lcom/google/android/opengl/carousel/CarouselRenderer;->mMultiTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v7, v10}, Lcom/google/android/opengl/carousel/CarouselRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 370
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setFadeAmount(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6
.end method

.method public drawGlowing(F)V
    .locals 7
    .param p1, "glowAlpha"    # F

    .prologue
    const/4 v1, 0x0

    .line 195
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mGlowingBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 198
    const/16 v0, 0xde1

    iget-object v2, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v2, v2, Lcom/google/android/opengl/carousel/CarouselTexture;->mGlowingId:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 200
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mMMatrix:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    sput-object v0, Lcom/google/android/opengl/carousel/Card;->sGlowMatrix:[F

    .line 201
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v6, v0, Lcom/google/android/opengl/carousel/CarouselSetting;->mCardGlowScale:F

    .line 202
    .local v6, "scale":F
    sget-object v0, Lcom/google/android/opengl/carousel/Card;->sGlowMatrix:[F

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v6, v6, v2}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 203
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mMVPMatrix:[F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVPMatrix:[F

    sget-object v4, Lcom/google/android/opengl/carousel/Card;->sGlowMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 204
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, p0, Lcom/google/android/opengl/carousel/Card;->mMVPMatrix:[F

    invoke-virtual {v0, v2}, Lcom/google/android/opengl/carousel/CarouselRenderer;->setMVPUniform([F)V

    .line 206
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCardRenderer:Lcom/google/android/opengl/carousel/ICardRenderer;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/ICardRenderer;->drawGlowingBorder(I)V

    .line 212
    :goto_1
    const-string v0, "Card.drawGlowing"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    goto :goto_0

    .line 209
    :cond_2
    const/4 v0, 0x6

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_1
.end method

.method public getDetailTextureId()I
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/RequestableTexture;->getTextureId()I

    move-result v0

    return v0
.end method

.method public getTextureId()I
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mTextures:[Lcom/google/android/opengl/carousel/RequestableTexture;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/opengl/carousel/RequestableTexture;->getTextureId()I

    move-result v0

    return v0
.end method

.method public getVertexCoord(I)[F
    .locals 5
    .param p1, "vertex"    # I

    .prologue
    const/4 v4, 0x3

    .line 102
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 103
    sget-object v1, Lcom/google/android/opengl/carousel/Card;->sOneVertex:[F

    sget-object v2, Lcom/google/android/opengl/carousel/Card;->mVerticesData:[F

    mul-int/lit8 v3, p1, 0x3

    add-int/2addr v3, v0

    aget v2, v2, v3

    aput v2, v1, v0

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    sget-object v1, Lcom/google/android/opengl/carousel/Card;->sOneVertex:[F

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v1, v4

    .line 107
    sget-object v1, Lcom/google/android/opengl/carousel/Card;->sOneVertex:[F

    return-object v1
.end method

.method public initCardTexture()V
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/carousel/Card;->initCardTexture(Z)V

    .line 453
    return-void
.end method

.method public initCardTexture(Z)V
    .locals 0
    .param p1, "clientInvalidate"    # Z

    .prologue
    .line 461
    invoke-direct {p0, p1}, Lcom/google/android/opengl/carousel/Card;->removeAllTextures(Z)V

    .line 462
    return-void
.end method

.method public requestTexture(Z)V
    .locals 3
    .param p1, "requestLargeTexture"    # Z

    .prologue
    const/4 v2, 0x1

    .line 468
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    if-nez v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    if-nez v0, :cond_2

    .line 472
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/CarouselCallback;->onRequestTexture(I)V

    .line 473
    iput v2, p0, Lcom/google/android/opengl/carousel/Card;->mTextureState:I

    .line 476
    :cond_2
    iget v0, p0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureState:I

    if-nez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselScene;->mCallback:Lcom/google/android/opengl/carousel/CarouselCallback;

    iget v1, p0, Lcom/google/android/opengl/carousel/Card;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/opengl/carousel/CarouselCallback;->onRequestDetailTexture(I)V

    .line 478
    iput v2, p0, Lcom/google/android/opengl/carousel/Card;->mDetailTextureState:I

    goto :goto_0
.end method

.method setGlDetailTexture()V
    .locals 4

    .prologue
    const v3, 0x84c0

    const/16 v2, 0xde1

    .line 524
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget-object v1, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v1, v1, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    if-ne v0, v1, :cond_0

    .line 525
    invoke-static {v3}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 526
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/Card;->getDetailTextureId()I

    move-result v0

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 533
    :goto_0
    return-void

    .line 528
    :cond_0
    invoke-static {v3}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 529
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Card;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mTexture:Lcom/google/android/opengl/carousel/CarouselTexture;

    iget v0, v0, Lcom/google/android/opengl/carousel/CarouselTexture;->mDetailLoadingId:I

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 530
    const v0, 0x84c1

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 531
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/Card;->getDetailTextureId()I

    move-result v0

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_0
.end method
