.class Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
.super Ljava/lang/Object;
.source "GLSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/opengl/common/GLSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GLThreadManager"
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

.field private mGLESDriverCheckComplete:Z

.field private mGLESVersion:I

.field private mGLESVersionCheckComplete:Z

.field private mLimitedGLESContexts:Z

.field private mMultipleGLESContextsAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1787
    const-string v0, "GLThreadManager"

    sput-object v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/opengl/common/GLSurfaceView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/opengl/common/GLSurfaceView$1;

    .prologue
    .line 1786
    invoke-direct {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;-><init>()V

    return-void
.end method

.method private checkGLESVersion()V
    .locals 3

    .prologue
    const/high16 v2, 0x20000

    const/4 v1, 0x1

    .line 1873
    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESVersionCheckComplete:Z

    if-nez v0, :cond_1

    .line 1877
    iput v2, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESVersion:I

    .line 1878
    iget v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESVersion:I

    if-lt v0, v2, :cond_0

    .line 1879
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    .line 1885
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESVersionCheckComplete:Z

    .line 1887
    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1851
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESDriverCheckComplete:Z

    if-nez v3, :cond_3

    .line 1852
    invoke-direct {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->checkGLESVersion()V

    .line 1853
    const/16 v3, 0x1f01

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 1854
    .local v0, "renderer":Ljava/lang/String;
    iget v3, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESVersion:I

    const/high16 v4, 0x20000

    if-ge v3, v4, :cond_0

    .line 1855
    const-string v3, "Q3Dimension MSM7500 "

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v2

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    .line 1857
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1860
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    if-eqz v3, :cond_1

    const-string v3, "Adreno"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mLimitedGLESContexts:Z

    .line 1868
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mGLESDriverCheckComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1870
    .end local v0    # "renderer":Ljava/lang/String;
    :cond_3
    monitor-exit p0

    return-void

    .restart local v0    # "renderer":Ljava/lang/String;
    :cond_4
    move v3, v1

    .line 1855
    goto :goto_0

    .line 1851
    .end local v0    # "renderer":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public releaseEglContextLocked(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V
    .locals 1
    .param p1, "thread"    # Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    if-ne v0, p1, :cond_0

    .line 1833
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    .line 1835
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1836
    return-void
.end method

.method public declared-synchronized shouldReleaseEGLContextWhenPausing()Z
    .locals 1

    .prologue
    .line 1842
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mLimitedGLESContexts:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized shouldTerminateEGLWhenPausing()Z
    .locals 1

    .prologue
    .line 1846
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->checkGLESVersion()V

    .line 1847
    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mMultipleGLESContextsAllowed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized threadExiting(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V
    .locals 1
    .param p1, "thread"    # Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    .prologue
    .line 1793
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    # setter for: Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z
    invoke-static {p1, v0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->access$1102(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;Z)Z

    .line 1794
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    if-ne v0, p1, :cond_0

    .line 1795
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    .line 1797
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1798
    monitor-exit p0

    return-void

    .line 1793
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public tryAcquireEglContextLocked(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)Z
    .locals 2
    .param p1, "thread"    # Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    .prologue
    const/4 v0, 0x1

    .line 1808
    iget-object v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    if-nez v1, :cond_2

    .line 1809
    :cond_0
    iput-object p1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    .line 1810
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1824
    :cond_1
    :goto_0
    return v0

    .line 1813
    :cond_2
    invoke-direct {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->checkGLESVersion()V

    .line 1814
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    if-nez v1, :cond_1

    .line 1821
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    if-eqz v0, :cond_3

    .line 1822
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->mEglOwner:Lcom/google/android/opengl/common/GLSurfaceView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->requestReleaseEglContextLocked()V

    .line 1824
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
