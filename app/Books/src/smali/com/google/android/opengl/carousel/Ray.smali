.class Lcom/google/android/opengl/carousel/Ray;
.super Ljava/lang/Object;
.source "Ray.java"


# static fields
.field public static sDebugRay:Z


# instance fields
.field public mBestTime:F

.field mDirection:Lcom/google/android/opengl/common/Float3;

.field mPosition:Lcom/google/android/opengl/common/Float3;

.field public mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field private mScene:Lcom/google/android/opengl/carousel/CarouselScene;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/opengl/carousel/Ray;->sDebugRay:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/opengl/carousel/CarouselRenderer;)V
    .locals 1
    .param p1, "renderer"    # Lcom/google/android/opengl/carousel/CarouselRenderer;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0}, Lcom/google/android/opengl/common/Float3;-><init>()V

    iput-object v0, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    .line 21
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0}, Lcom/google/android/opengl/common/Float3;-><init>()V

    iput-object v0, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 30
    iput-object p1, p0, Lcom/google/android/opengl/carousel/Ray;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    .line 31
    iget-object v0, p0, Lcom/google/android/opengl/carousel/Ray;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iput-object v0, p0, Lcom/google/android/opengl/carousel/Ray;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    .line 32
    return-void
.end method


# virtual methods
.method public makeRayForPixelAt(Lcom/google/android/opengl/carousel/GLCamera;FF)Z
    .locals 12
    .param p1, "camera"    # Lcom/google/android/opengl/carousel/GLCamera;
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 39
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v8, v8, Lcom/google/android/opengl/carousel/CarouselRenderer;->mWidth:I

    int-to-float v8, v8

    div-float v6, p2, v8

    .line 40
    .local v6, "u":F
    const/high16 v8, 0x3f800000    # 1.0f

    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v9, v9, Lcom/google/android/opengl/carousel/CarouselRenderer;->mHeight:I

    int-to-float v9, v9

    div-float v9, p3, v9

    sub-float v7, v8, v9

    .line 42
    .local v7, "v":F
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget v0, v8, Lcom/google/android/opengl/carousel/CarouselRenderer;->mAspect:F

    .line 43
    .local v0, "aspect":F
    iget v8, p1, Lcom/google/android/opengl/carousel/GLCamera;->mFovy:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    double-to-float v3, v8

    .line 44
    .local v3, "halfFovy":F
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    float-to-double v10, v3

    invoke-static {v10, v11}, Ljava/lang/Math;->tan(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-float v4, v8

    .line 45
    .local v4, "tanfov2":F
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget-object v9, p1, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    invoke-virtual {v8, v9}, Lcom/google/android/opengl/common/Float3;->set([F)V

    .line 46
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget-object v9, p1, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    invoke-virtual {v8, v9}, Lcom/google/android/opengl/common/Float3;->set([F)V

    .line 47
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8, v9}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 48
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8}, Lcom/google/android/opengl/common/Float3;->normalize()V

    .line 50
    new-instance v1, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v1}, Lcom/google/android/opengl/common/Float3;-><init>()V

    .line 51
    .local v1, "du":Lcom/google/android/opengl/common/Float3;
    new-instance v5, Lcom/google/android/opengl/common/Float3;

    iget-object v8, p1, Lcom/google/android/opengl/carousel/GLCamera;->mUp:[F

    invoke-direct {v5, v8}, Lcom/google/android/opengl/common/Float3;-><init>([F)V

    .line 52
    .local v5, "tmp":Lcom/google/android/opengl/common/Float3;
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8, v5, v1}, Lcom/google/android/opengl/common/Float3;->cross(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 53
    invoke-virtual {v1}, Lcom/google/android/opengl/common/Float3;->normalize()V

    .line 54
    invoke-virtual {v1, v4}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 56
    new-instance v2, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v2}, Lcom/google/android/opengl/common/Float3;-><init>()V

    .line 57
    .local v2, "dv":Lcom/google/android/opengl/common/Float3;
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v1, v8, v2}, Lcom/google/android/opengl/common/Float3;->cross(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    move-result-object v2

    .line 58
    invoke-virtual {v2}, Lcom/google/android/opengl/common/Float3;->normalize()V

    .line 59
    invoke-virtual {v2, v4}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 61
    invoke-virtual {v1, v0}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 63
    invoke-virtual {v5, v1}, Lcom/google/android/opengl/common/Float3;->set(Lcom/google/android/opengl/common/Float3;)V

    .line 64
    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v5, v8}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 65
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8, v5}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 66
    invoke-virtual {v5, v2}, Lcom/google/android/opengl/common/Float3;->set(Lcom/google/android/opengl/common/Float3;)V

    .line 67
    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v5, v8}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 68
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8, v5}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 70
    invoke-virtual {v5, v1}, Lcom/google/android/opengl/common/Float3;->set(Lcom/google/android/opengl/common/Float3;)V

    .line 71
    invoke-virtual {v5, v6}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 72
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8, v5}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 73
    invoke-virtual {v5, v2}, Lcom/google/android/opengl/common/Float3;->set(Lcom/google/android/opengl/common/Float3;)V

    .line 74
    invoke-virtual {v5, v7}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 75
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8, v5}, Lcom/google/android/opengl/common/Float3;->add(Lcom/google/android/opengl/common/Float3;)V

    .line 76
    iget-object v8, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v8}, Lcom/google/android/opengl/common/Float3;->normalize()V

    .line 78
    sget-boolean v8, Lcom/google/android/opengl/carousel/Ray;->sDebugRay:Z

    if-eqz v8, :cond_0

    .line 79
    const-string v8, "Ray"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ray position : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const-string v8, "Ray"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ray direction:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    const/4 v8, 0x1

    return v8
.end method

.method public rayCylinderIntersect(Lcom/google/android/opengl/carousel/Cylinder;)Z
    .locals 14
    .param p1, "cylinder"    # Lcom/google/android/opengl/carousel/Cylinder;

    .prologue
    const/4 v8, 0x1

    const/high16 v13, 0x40000000    # 2.0f

    const/4 v7, 0x0

    const/4 v12, 0x0

    .line 153
    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v9, v9, Lcom/google/android/opengl/common/Float3;->x:F

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v10, v10, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v10, v10, Lcom/google/android/opengl/common/Float3;->z:F

    iget-object v11, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v11, v11, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 154
    .local v0, "A":F
    const v9, 0x358637bd    # 1.0E-6f

    cmpg-float v9, v0, v9

    if-gez v9, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v7

    .line 158
    :cond_1
    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v9, v9, Lcom/google/android/opengl/common/Float3;->x:F

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget v10, v10, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget v10, v10, Lcom/google/android/opengl/common/Float3;->z:F

    iget-object v11, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget v11, v11, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    mul-float v1, v13, v9

    .line 159
    .local v1, "B":F
    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget v9, v9, Lcom/google/android/opengl/common/Float3;->x:F

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget v10, v10, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget v10, v10, Lcom/google/android/opengl/common/Float3;->z:F

    iget-object v11, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget v11, v11, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Lcom/google/android/opengl/carousel/Cylinder;->mRadius:F

    iget v11, p1, Lcom/google/android/opengl/carousel/Cylinder;->mRadius:F

    mul-float/2addr v10, v11

    sub-float v2, v9, v10

    .line 161
    .local v2, "C":F
    mul-float v9, v1, v1

    const/high16 v10, 0x40800000    # 4.0f

    mul-float/2addr v10, v0

    mul-float/2addr v10, v2

    sub-float v4, v9, v10

    .line 163
    .local v4, "disc":F
    cmpg-float v9, v4, v12

    if-ltz v9, :cond_0

    .line 165
    float-to-double v10, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    .line 166
    mul-float v3, v13, v0

    .line 169
    .local v3, "denom":F
    neg-float v9, v1

    sub-float/2addr v9, v4

    div-float v5, v9, v3

    .line 170
    .local v5, "t1":F
    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v9, v9, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v9, v9, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v10, 0x3

    if-ne v9, v10, :cond_2

    cmpl-float v9, v5, v12

    if-lez v9, :cond_2

    iget v9, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    cmpg-float v9, v5, v9

    if-gez v9, :cond_2

    .line 172
    iput v5, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    move v7, v8

    .line 173
    goto :goto_0

    .line 177
    :cond_2
    neg-float v9, v1

    add-float/2addr v9, v4

    div-float v6, v9, v3

    .line 178
    .local v6, "t2":F
    iget-object v9, p0, Lcom/google/android/opengl/carousel/Ray;->mScene:Lcom/google/android/opengl/carousel/CarouselScene;

    iget-object v9, v9, Lcom/google/android/opengl/carousel/CarouselScene;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v9, v9, Lcom/google/android/opengl/carousel/CarouselSetting;->mDragModel:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    cmpl-float v9, v6, v12

    if-lez v9, :cond_0

    iget v9, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    cmpg-float v9, v6, v9

    if-gez v9, :cond_0

    .line 180
    iput v6, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    move v7, v8

    .line 181
    goto :goto_0
.end method

.method public rayPlaneIntersect(Lcom/google/android/opengl/carousel/Plane;)Z
    .locals 5
    .param p1, "plane"    # Lcom/google/android/opengl/carousel/Plane;

    .prologue
    .line 134
    iget-object v2, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    iget-object v3, p1, Lcom/google/android/opengl/carousel/Plane;->mNormal:Lcom/google/android/opengl/common/Float3;

    invoke-static {v2, v3}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v0

    .line 135
    .local v0, "denom":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 136
    iget v2, p1, Lcom/google/android/opengl/carousel/Plane;->mConstant:F

    iget-object v3, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    iget-object v4, p1, Lcom/google/android/opengl/carousel/Plane;->mNormal:Lcom/google/android/opengl/common/Float3;

    invoke-static {v3, v4}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v3

    add-float/2addr v2, v3

    neg-float v2, v2

    div-float v1, v2, v0

    .line 137
    .local v1, "t":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 138
    iput v1, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 139
    const/4 v2, 0x1

    .line 142
    .end local v1    # "t":F
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public rayTriangleIntersect(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Z
    .locals 12
    .param p1, "p0"    # Lcom/google/android/opengl/common/Float3;
    .param p2, "p1"    # Lcom/google/android/opengl/common/Float3;
    .param p3, "p2"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 91
    const/4 v7, 0x0

    .line 93
    .local v7, "tmin":F
    new-instance v3, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v3, p2}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 94
    .local v3, "e1":Lcom/google/android/opengl/common/Float3;
    invoke-virtual {v3, p1}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 95
    new-instance v4, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v4, p3}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 96
    .local v4, "e2":Lcom/google/android/opengl/common/Float3;
    invoke-virtual {v4, p1}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 98
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0}, Lcom/google/android/opengl/common/Float3;-><init>()V

    .line 99
    .local v0, "cross":Lcom/google/android/opengl/common/Float3;
    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-virtual {v10, v4, v0}, Lcom/google/android/opengl/common/Float3;->cross(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 100
    invoke-static {v0, v3}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v2

    .line 102
    .local v2, "div":F
    const/4 v10, 0x0

    cmpl-float v10, v2, v10

    if-nez v10, :cond_0

    .line 103
    const/4 v10, 0x0

    .line 126
    :goto_0
    return v10

    .line 105
    :cond_0
    new-instance v1, Lcom/google/android/opengl/common/Float3;

    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mPosition:Lcom/google/android/opengl/common/Float3;

    invoke-direct {v1, v10}, Lcom/google/android/opengl/common/Float3;-><init>(Lcom/google/android/opengl/common/Float3;)V

    .line 106
    .local v1, "d":Lcom/google/android/opengl/common/Float3;
    invoke-virtual {v1, p1}, Lcom/google/android/opengl/common/Float3;->minus(Lcom/google/android/opengl/common/Float3;)V

    .line 108
    const/high16 v10, 0x3f800000    # 1.0f

    div-float v5, v10, v2

    .line 110
    .local v5, "invDiv":F
    invoke-static {v1, v0}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v10

    mul-float v8, v10, v5

    .line 111
    .local v8, "u":F
    const/4 v10, 0x0

    cmpg-float v10, v8, v10

    if-ltz v10, :cond_1

    const/high16 v10, 0x3f800000    # 1.0f

    cmpl-float v10, v8, v10

    if-lez v10, :cond_2

    .line 112
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 114
    :cond_2
    invoke-virtual {v1, v3, v0}, Lcom/google/android/opengl/common/Float3;->cross(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 115
    iget-object v10, p0, Lcom/google/android/opengl/carousel/Ray;->mDirection:Lcom/google/android/opengl/common/Float3;

    invoke-static {v10, v0}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v10

    mul-float v9, v10, v5

    .line 117
    .local v9, "v":F
    const/4 v10, 0x0

    cmpg-float v10, v9, v10

    if-ltz v10, :cond_3

    add-float v10, v8, v9

    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v10, v10, v11

    if-lez v10, :cond_4

    .line 118
    :cond_3
    const/4 v10, 0x0

    goto :goto_0

    .line 120
    :cond_4
    invoke-static {v4, v0}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v10

    mul-float v6, v10, v5

    .line 121
    .local v6, "t":F
    cmpg-float v10, v6, v7

    if-ltz v10, :cond_5

    iget v10, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    cmpl-float v10, v6, v10

    if-lez v10, :cond_6

    .line 122
    :cond_5
    const/4 v10, 0x0

    goto :goto_0

    .line 124
    :cond_6
    iput v6, p0, Lcom/google/android/opengl/carousel/Ray;->mBestTime:F

    .line 126
    const/4 v10, 0x1

    goto :goto_0
.end method
