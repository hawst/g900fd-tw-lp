.class Lcom/google/android/opengl/carousel/CarouselSetting;
.super Ljava/lang/Object;
.source "CarouselSetting.java"


# static fields
.field static final DEFAULT_AT:[F

.field static final DEFAULT_AT_PORTRAIT:[F

.field static final DEFAULT_EYE:[F

.field static final DEFAULT_EYE_PORTRAIT:[F

.field static final DEFAULT_UP:[F

.field static final DEFAULT_UP_PORTRAIT:[F

.field static final LABEL_HIGHLIGHT_COLOR:Lcom/google/android/opengl/common/Float4;

.field static TRAJECTORY_X_OFFSET:I


# instance fields
.field mAccelerationFactor:F

.field mAccelerationRatio:F

.field mAntiJitter:Z

.field mBackgroundTransitionDuration:J

.field mCardFaceTangent:Z

.field mCardGlowScale:F

.field mCardRotation:F

.field mCardXYScale:Lcom/google/android/opengl/common/Float2;

.field mDefaultBitmap:Landroid/graphics/Bitmap;

.field mDefaultCardMatrix:[F

.field mDefaultLineBitmap:Landroid/graphics/Bitmap;

.field mDetailAlignment:I

.field mDetailLoadingBitmap:Landroid/graphics/Bitmap;

.field mDetailsSelectable:Z

.field mDividAngle:F

.field mDpadHorizontal:Z

.field mDpadSmoothScrollInterpolationMode:I

.field mDpadSmoothScrollMaxArc:F

.field mDpadSmoothScrollTime:I

.field mDragFactor:F

.field mDragModel:I

.field mDrawRuler:Z

.field mEmptyBitmap:Landroid/graphics/Bitmap;

.field mEnableBoostArea:Z

.field mFadeInDuration:J

.field mFadeOutLeftAngle:F

.field mFillDirection:I

.field mFirstCardTop:Z

.field mFrameSelectedCard:Z

.field mFrictionCoeff:F

.field mGlowingBitmap:Landroid/graphics/Bitmap;

.field mHighlightDetail:Z

.field mLoadingBitmap:Landroid/graphics/Bitmap;

.field mMaxDeltaBias:F

.field mMusicPortraitOfLabel:Z

.field mPortriatRulerHeight:F

.field mPrefetchCardCount:I

.field mRadius:F

.field mRezInCardCount:F

.field mRowCount:I

.field mRowSpacing:F

.field mScaleSelectedCard:Z

.field mShowDetails:Z

.field mSlotCount:I

.field mSpecialRotationInPortrait:Z

.field mStartAngle:F

.field mSwaySensitivity:F

.field mTextureVelocityThreshold:F

.field mTiltMaximumAngle:F

.field mTrajectoryAngle:F

.field mVelocityUpLimit:F

.field mVisibleDetailCount:I

.field mVisibleSlotCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const v2, 0x3f19999a    # 0.6f

    const/4 v1, 0x3

    .line 20
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->DEFAULT_EYE:[F

    .line 21
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->DEFAULT_AT:[F

    .line 22
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->DEFAULT_UP:[F

    .line 24
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->DEFAULT_EYE_PORTRAIT:[F

    .line 25
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->DEFAULT_AT_PORTRAIT:[F

    .line 26
    new-array v0, v1, [F

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->DEFAULT_UP_PORTRAIT:[F

    .line 52
    const/16 v0, 0x64

    sput v0, Lcom/google/android/opengl/carousel/CarouselSetting;->TRAJECTORY_X_OFFSET:I

    .line 98
    new-instance v0, Lcom/google/android/opengl/common/Float4;

    invoke-direct {v0, v2, v2, v2, v2}, Lcom/google/android/opengl/common/Float4;-><init>(FFFF)V

    sput-object v0, Lcom/google/android/opengl/carousel/CarouselSetting;->LABEL_HIGHLIGHT_COLOR:Lcom/google/android/opengl/common/Float4;

    return-void

    .line 20
    :array_0
    .array-data 4
        0x41a64467
        0x4010980b
        0x4187da86
    .end array-data

    .line 21
    :array_1
    .array-data 4
        0x416964c3    # 14.5871f
        -0x3fd6110a
        -0x4043d417
    .end array-data

    .line 22
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 24
    :array_3
    .array-data 4
        0x42a0a986
        0x40819874    # 4.04986f
        0x41838be1
    .end array-data

    .line 25
    :array_4
    .array-data 4
        0x4294a745
        -0x3eaed35b
        -0x3e8a9100
    .end array-data

    .line 26
    :array_5
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
