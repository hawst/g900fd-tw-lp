.class public final Lcom/google/android/opengl/common/Float3;
.super Ljava/lang/Object;
.source "Float3.java"


# instance fields
.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "initX"    # F
    .param p2, "initY"    # F
    .param p3, "initZ"    # F

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 22
    iput p2, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 23
    iput p3, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/android/opengl/common/Float3;)V
    .locals 0
    .param p1, "b"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-virtual {p0, p1}, Lcom/google/android/opengl/common/Float3;->set(Lcom/google/android/opengl/common/Float3;)V

    .line 18
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "farray"    # [F

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p0, p1}, Lcom/google/android/opengl/common/Float3;->set([F)V

    .line 28
    return-void
.end method

.method public static dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F
    .locals 3
    .param p0, "a"    # Lcom/google/android/opengl/common/Float3;
    .param p1, "b"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/common/Float3;->y:F

    iget v2, p1, Lcom/google/android/opengl/common/Float3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/common/Float3;->z:F

    iget v2, p1, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public static getArray(I)[Lcom/google/android/opengl/common/Float3;
    .locals 3
    .param p0, "count"    # I

    .prologue
    .line 56
    new-array v1, p0, [Lcom/google/android/opengl/common/Float3;

    .line 57
    .local v1, "r":[Lcom/google/android/opengl/common/Float3;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p0, :cond_0

    .line 58
    new-instance v2, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v2}, Lcom/google/android/opengl/common/Float3;-><init>()V

    aput-object v2, v1, v0

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-object v1
.end method

.method public static getUnit()Lcom/google/android/opengl/common/Float3;
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 64
    new-instance v0, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/opengl/common/Float3;-><init>(FFF)V

    return-object v0
.end method


# virtual methods
.method public add(Lcom/google/android/opengl/common/Float3;)V
    .locals 2
    .param p1, "b"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 80
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 81
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 82
    return-void
.end method

.method public cross(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;
    .locals 3
    .param p1, "b"    # Lcom/google/android/opengl/common/Float3;
    .param p2, "out"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/common/Float3;->z:F

    iget v2, p1, Lcom/google/android/opengl/common/Float3;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/opengl/common/Float3;->x:F

    .line 105
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/common/Float3;->x:F

    iget v2, p1, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/opengl/common/Float3;->y:F

    .line 106
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->y:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/opengl/common/Float3;->y:F

    iget v2, p1, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/google/android/opengl/common/Float3;->z:F

    .line 107
    return-object p2
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 127
    invoke-static {p0, p0}, Lcom/google/android/opengl/common/Float3;->dot(Lcom/google/android/opengl/common/Float3;Lcom/google/android/opengl/common/Float3;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public minus(Lcom/google/android/opengl/common/Float3;)V
    .locals 2
    .param p1, "b"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 74
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 75
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    iget v1, p1, Lcom/google/android/opengl/common/Float3;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 76
    return-void
.end method

.method public negate()V
    .locals 1

    .prologue
    .line 98
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/common/Float3;->times(F)V

    .line 99
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/opengl/common/Float3;->getLength()F

    move-result v0

    .line 86
    .local v0, "length":F
    iget v1, p0, Lcom/google/android/opengl/common/Float3;->x:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 87
    iget v1, p0, Lcom/google/android/opengl/common/Float3;->y:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 88
    iget v1, p0, Lcom/google/android/opengl/common/Float3;->z:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 89
    return-void
.end method

.method public set(FFF)V
    .locals 0
    .param p1, "ix"    # F
    .param p2, "iy"    # F
    .param p3, "iz"    # F

    .prologue
    .line 39
    iput p1, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 40
    iput p2, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 41
    iput p3, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 42
    return-void
.end method

.method public set(Lcom/google/android/opengl/common/Float3;)V
    .locals 1
    .param p1, "a"    # Lcom/google/android/opengl/common/Float3;

    .prologue
    .line 32
    iget v0, p1, Lcom/google/android/opengl/common/Float3;->x:F

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 33
    iget v0, p1, Lcom/google/android/opengl/common/Float3;->y:F

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 34
    iget v0, p1, Lcom/google/android/opengl/common/Float3;->z:F

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 35
    return-void
.end method

.method public set([F)V
    .locals 2
    .param p1, "farray"    # [F

    .prologue
    .line 46
    array-length v0, p1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expecting length 3"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 50
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 51
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 52
    return-void
.end method

.method public times(F)V
    .locals 1
    .param p1, "t"    # F

    .prologue
    .line 92
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->x:F

    .line 93
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->y:F

    .line 94
    iget v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/opengl/common/Float3;->z:F

    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x20

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Float3: ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 112
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget v1, p0, Lcom/google/android/opengl/common/Float3;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    iget v1, p0, Lcom/google/android/opengl/common/Float3;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 116
    iget v1, p0, Lcom/google/android/opengl/common/Float3;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, "] Length: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/opengl/common/Float3;->getLength()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
