.class Lcom/google/android/opengl/carousel/GLCamera;
.super Ljava/lang/Object;
.source "GLCamera.java"


# instance fields
.field private mDefaultEye:[F

.field private mDefaultLookAt:[F

.field mEye:[F

.field mFovy:F

.field mLookat:[F

.field private mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

.field mUp:[F

.field private mVMatrix:[F

.field private mWorldMatrix:[F

.field private moveStep:F

.field private rotStep:F


# virtual methods
.method backward()V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    const/4 v1, 0x2

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 91
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 92
    return-void
.end method

.method down()V
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    const/4 v1, 0x1

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    .line 111
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 112
    return-void
.end method

.method forward()V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    const/4 v1, 0x2

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    .line 86
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 87
    return-void
.end method

.method left()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    .line 96
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 97
    return-void
.end method

.method lookBackward()V
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    const/4 v1, 0x2

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 141
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 142
    return-void
.end method

.method lookDown()V
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    const/4 v1, 0x1

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    .line 131
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 132
    return-void
.end method

.method lookForward()V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    const/4 v1, 0x2

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    .line 136
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 137
    return-void
.end method

.method lookLeft()V
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    sub-float/2addr v2, v3

    aput v2, v0, v1

    .line 116
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 117
    return-void
.end method

.method lookRight()V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 121
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 122
    return-void
.end method

.method lookUp()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    const/4 v1, 0x1

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 126
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 127
    return-void
.end method

.method printParameters()Ljava/lang/String;
    .locals 8

    .prologue
    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x64

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 156
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v3, "==== Current Camera Parameters ====\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Eye: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    invoke-static {v4}, Lcom/google/android/opengl/carousel/GL2Helper;->getStringForFloatArray([F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Lookat: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    invoke-static {v4}, Lcom/google/android/opengl/carousel/GL2Helper;->getStringForFloatArray([F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Up: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/opengl/carousel/GLCamera;->mUp:[F

    invoke-static {v4}, Lcom/google/android/opengl/carousel/GL2Helper;->getStringForFloatArray([F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    iget-object v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v3, v3, Lcom/google/android/opengl/carousel/CarouselRenderer;->mSetting:Lcom/google/android/opengl/carousel/CarouselSetting;

    iget v2, v3, Lcom/google/android/opengl/carousel/CarouselSetting;->mStartAngle:F

    .line 161
    .local v2, "startAngle":F
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v2, v3

    float-to-double v4, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v4, v6

    double-to-float v0, v4

    .line 162
    .local v0, "r":F
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Start Angle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " * 2 * PI\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method reset()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mDefaultEye:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    .line 70
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mDefaultLookAt:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    .line 71
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mWorldMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 73
    return-void
.end method

.method right()V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 101
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 102
    return-void
.end method

.method setCamera()V
    .locals 11

    .prologue
    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mVMatrix:[F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    aget v2, v2, v1

    iget-object v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    aget v3, v3, v7

    iget-object v4, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    aget v4, v4, v9

    iget-object v5, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    aget v5, v5, v1

    iget-object v6, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    aget v6, v6, v7

    iget-object v7, p0, Lcom/google/android/opengl/carousel/GLCamera;->mLookat:[F

    aget v7, v7, v9

    const/high16 v9, 0x3f800000    # 1.0f

    move v10, v8

    invoke-static/range {v0 .. v10}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 62
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v0, v0, Lcom/google/android/opengl/carousel/CarouselRenderer;->mVPMatrix:[F

    iget-object v2, p0, Lcom/google/android/opengl/carousel/GLCamera;->mRenderer:Lcom/google/android/opengl/carousel/CarouselRenderer;

    iget-object v2, v2, Lcom/google/android/opengl/carousel/CarouselRenderer;->mProjMatrix:[F

    iget-object v4, p0, Lcom/google/android/opengl/carousel/GLCamera;->mVMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 63
    return-void
.end method

.method up()V
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mEye:[F

    const/4 v1, 0x1

    aget v2, v0, v1

    iget v3, p0, Lcom/google/android/opengl/carousel/GLCamera;->moveStep:F

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 106
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 107
    return-void
.end method

.method worldRotateLeft()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 145
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mWorldMatrix:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/opengl/carousel/GLCamera;->rotStep:F

    neg-float v2, v2

    const/high16 v4, 0x3f800000    # 1.0f

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 147
    return-void
.end method

.method worldRotateRight()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/opengl/carousel/GLCamera;->mWorldMatrix:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/opengl/carousel/GLCamera;->rotStep:F

    const/high16 v4, 0x3f800000    # 1.0f

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/opengl/carousel/GLCamera;->setCamera()V

    .line 152
    return-void
.end method
