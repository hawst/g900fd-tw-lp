.class Lcom/google/android/opengl/common/GLSurfaceView$GLThread;
.super Ljava/lang/Thread;
.source "GLSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/opengl/common/GLSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GLThread"
.end annotation


# instance fields
.field private mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mExited:Z

.field private mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/opengl/common/GLSurfaceView;",
            ">;"
        }
    .end annotation
.end field

.field private mHasSurface:Z

.field private mHaveEglContext:Z

.field private mHaveEglSurface:Z

.field private mHeight:I

.field private mPaused:Z

.field private mRenderComplete:Z

.field private mRenderMode:I

.field private mRequestPaused:Z

.field private mRequestRender:Z

.field private mShouldExit:Z

.field private mShouldReleaseEglContext:Z

.field private mSizeChanged:Z

.field private mSurfaceIsBad:Z

.field private mWaitingForSurface:Z

.field private mWidth:I


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/opengl/common/GLSurfaceView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "glSurfaceViewWeakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/google/android/opengl/common/GLSurfaceView;>;"
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1216
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1730
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    .line 1731
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSizeChanged:Z

    .line 1217
    iput v2, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWidth:I

    .line 1218
    iput v2, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHeight:I

    .line 1219
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestRender:Z

    .line 1220
    iput v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderMode:I

    .line 1221
    iput-object p1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;

    .line 1222
    return-void
.end method

.method static synthetic access$1102(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/opengl/common/GLSurfaceView$GLThread;
    .param p1, "x1"    # Z

    .prologue
    .line 1214
    iput-boolean p1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z

    return p1
.end method

.method private guardedRun()V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1263
    new-instance v18, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    .line 1264
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    .line 1265
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    .line 1267
    const/4 v8, 0x0

    .line 1268
    .local v8, "gl":Ljavax/microedition/khronos/opengles/GL10;
    const/4 v3, 0x0

    .line 1269
    .local v3, "createEglContext":Z
    const/4 v4, 0x0

    .line 1270
    .local v4, "createEglSurface":Z
    const/4 v5, 0x0

    .line 1271
    .local v5, "createGlInterface":Z
    const/4 v10, 0x0

    .line 1272
    .local v10, "lostEglContext":Z
    const/4 v12, 0x0

    .line 1273
    .local v12, "sizeChanged":Z
    const/16 v17, 0x0

    .line 1274
    .local v17, "wantRenderNotification":Z
    const/4 v6, 0x0

    .line 1275
    .local v6, "doRenderNotification":Z
    const/4 v2, 0x0

    .line 1276
    .local v2, "askedToReleaseEglContext":Z
    const/16 v16, 0x0

    .line 1277
    .local v16, "w":I
    const/4 v9, 0x0

    .line 1278
    .local v9, "h":I
    const/4 v7, 0x0

    .line 1281
    .local v7, "event":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    :try_start_0
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v19

    monitor-enter v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1283
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mShouldExit:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    .line 1284
    monitor-exit v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1538
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v19

    monitor-enter v19

    .line 1539
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglSurfaceLocked()V

    .line 1540
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglContextLocked()V

    .line 1541
    monitor-exit v19

    return-void

    :catchall_0
    move-exception v18

    monitor-exit v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v18

    .line 1287
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_2

    .line 1288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Ljava/lang/Runnable;

    move-object v7, v0

    .line 1441
    :goto_2
    monitor-exit v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1443
    if-eqz v7, :cond_13

    .line 1444
    :try_start_4
    invoke-interface {v7}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1445
    const/4 v7, 0x0

    .line 1446
    goto :goto_0

    .line 1293
    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestPaused:Z

    move/from16 v20, v0

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    .line 1294
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestPaused:Z

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z

    .line 1295
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    .line 1302
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mShouldReleaseEglContext:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    .line 1306
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglSurfaceLocked()V

    .line 1307
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglContextLocked()V

    .line 1308
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mShouldReleaseEglContext:Z

    .line 1309
    const/4 v2, 0x1

    .line 1313
    :cond_4
    if-eqz v10, :cond_5

    .line 1314
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglSurfaceLocked()V

    .line 1315
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglContextLocked()V

    .line 1316
    const/4 v10, 0x0

    .line 1320
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    move/from16 v18, v0

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z

    move/from16 v18, v0

    if-eqz v18, :cond_8

    .line 1324
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglSurfaceLocked()V

    .line 1325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/opengl/common/GLSurfaceView;

    .line 1326
    .local v15, "view":Lcom/google/android/opengl/common/GLSurfaceView;
    if-nez v15, :cond_10

    const/4 v11, 0x0

    .line 1328
    .local v11, "preserveEglContextOnPause":Z
    :goto_3
    if-eqz v11, :cond_6

    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->shouldReleaseEGLContextWhenPausing()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1329
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglContextLocked()V

    .line 1334
    :cond_7
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->shouldTerminateEGLWhenPausing()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->finish()V

    .line 1343
    .end local v11    # "preserveEglContextOnPause":Z
    .end local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHasSurface:Z

    move/from16 v18, v0

    if-nez v18, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWaitingForSurface:Z

    move/from16 v18, v0

    if-nez v18, :cond_a

    .line 1347
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    .line 1348
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglSurfaceLocked()V

    .line 1350
    :cond_9
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWaitingForSurface:Z

    .line 1351
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSurfaceIsBad:Z

    .line 1352
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    .line 1356
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHasSurface:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWaitingForSurface:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 1360
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWaitingForSurface:Z

    .line 1361
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    .line 1364
    :cond_b
    if-eqz v6, :cond_c

    .line 1368
    const/16 v17, 0x0

    .line 1369
    const/4 v6, 0x0

    .line 1370
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderComplete:Z

    .line 1371
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    .line 1375
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->readyToDraw()Z

    move-result v18

    if-eqz v18, :cond_12

    .line 1378
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    move/from16 v18, v0

    if-nez v18, :cond_d

    .line 1379
    if-eqz v2, :cond_11

    .line 1380
    const/4 v2, 0x0

    .line 1395
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    move/from16 v18, v0

    if-nez v18, :cond_e

    .line 1396
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    .line 1397
    const/4 v4, 0x1

    .line 1398
    const/4 v5, 0x1

    .line 1399
    const/4 v12, 0x1

    .line 1402
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    move/from16 v18, v0

    if-eqz v18, :cond_12

    .line 1403
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSizeChanged:Z

    move/from16 v18, v0

    if-eqz v18, :cond_f

    .line 1404
    const/4 v12, 0x1

    .line 1405
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWidth:I

    move/from16 v16, v0

    .line 1406
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHeight:I

    .line 1407
    const/16 v17, 0x1

    .line 1415
    const/4 v4, 0x1

    .line 1417
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSizeChanged:Z

    .line 1419
    :cond_f
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestRender:Z

    .line 1420
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_2

    .line 1441
    :catchall_1
    move-exception v18

    monitor-exit v19
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v18
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1538
    :catchall_2
    move-exception v18

    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v19

    monitor-enter v19

    .line 1539
    :try_start_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglSurfaceLocked()V

    .line 1540
    invoke-direct/range {p0 .. p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->stopEglContextLocked()V

    .line 1541
    monitor-exit v19
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    throw v18

    .line 1326
    .restart local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    :cond_10
    :try_start_8
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->mPreserveEGLContextOnPause:Z
    invoke-static {v15}, Lcom/google/android/opengl/common/GLSurfaceView;->access$900(Lcom/google/android/opengl/common/GLSurfaceView;)Z

    move-result v11

    goto/16 :goto_3

    .line 1381
    .end local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    :cond_11
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->tryAcquireEglContextLocked(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v18

    if-eqz v18, :cond_d

    .line 1383
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->start()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1388
    const/16 v18, 0x1

    :try_start_a
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    .line 1389
    const/4 v3, 0x1

    .line 1391
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_4

    .line 1384
    :catch_0
    move-exception v14

    .line 1385
    .local v14, "t":Ljava/lang/RuntimeException;
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->releaseEglContextLocked(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V

    .line 1386
    throw v14

    .line 1439
    .end local v14    # "t":Ljava/lang/RuntimeException;
    :cond_12
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 1449
    :cond_13
    if-eqz v4, :cond_16

    .line 1453
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->createSurface()Z

    move-result v18

    if-nez v18, :cond_15

    .line 1454
    const-string v18, "GLThread"

    const/16 v19, 0x6

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_14

    .line 1455
    const-string v18, "GLThread"

    const-string v19, "Failed to create surface"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    :cond_14
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v19

    monitor-enter v19
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1459
    const/16 v18, 0x1

    :try_start_c
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSurfaceIsBad:Z

    .line 1460
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    .line 1461
    monitor-exit v19

    goto/16 :goto_0

    :catchall_3
    move-exception v18

    monitor-exit v19
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v18

    .line 1464
    :cond_15
    const/4 v4, 0x0

    .line 1467
    :cond_16
    if-eqz v5, :cond_17

    .line 1468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->createGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    move-object v8, v0

    .line 1470
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1471
    const/4 v5, 0x0

    .line 1474
    :cond_17
    if-eqz v3, :cond_19

    .line 1478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/opengl/common/GLSurfaceView;

    .line 1479
    .restart local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    if-eqz v15, :cond_18

    .line 1480
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->mRenderer:Lcom/google/android/opengl/common/GLSurfaceView$Renderer;
    invoke-static {v15}, Lcom/google/android/opengl/common/GLSurfaceView;->access$1000(Lcom/google/android/opengl/common/GLSurfaceView;)Lcom/google/android/opengl/common/GLSurfaceView$Renderer;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v8, v1}, Lcom/google/android/opengl/common/GLSurfaceView$Renderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1482
    :cond_18
    const/4 v3, 0x0

    .line 1485
    .end local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    :cond_19
    if-eqz v12, :cond_1b

    .line 1489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/opengl/common/GLSurfaceView;

    .line 1490
    .restart local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    if-eqz v15, :cond_1a

    .line 1491
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->mRenderer:Lcom/google/android/opengl/common/GLSurfaceView$Renderer;
    invoke-static {v15}, Lcom/google/android/opengl/common/GLSurfaceView;->access$1000(Lcom/google/android/opengl/common/GLSurfaceView;)Lcom/google/android/opengl/common/GLSurfaceView$Renderer;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-interface {v0, v8, v1, v9}, Lcom/google/android/opengl/common/GLSurfaceView$Renderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1493
    :cond_1a
    const/4 v12, 0x0

    .line 1500
    .end local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mGLSurfaceViewWeakRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/opengl/common/GLSurfaceView;

    .line 1501
    .restart local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    if-eqz v15, :cond_1c

    .line 1502
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->mRenderer:Lcom/google/android/opengl/common/GLSurfaceView$Renderer;
    invoke-static {v15}, Lcom/google/android/opengl/common/GLSurfaceView;->access$1000(Lcom/google/android/opengl/common/GLSurfaceView;)Lcom/google/android/opengl/common/GLSurfaceView$Renderer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Lcom/google/android/opengl/common/GLSurfaceView$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1505
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->swap()I

    move-result v13

    .line 1506
    .local v13, "swapError":I
    sparse-switch v13, :sswitch_data_0

    .line 1520
    const-string v18, "GLThread"

    const-string v19, "eglSwapBuffers"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v13}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->logEglErrorAsWarning(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1522
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v19

    monitor-enter v19
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1523
    const/16 v18, 0x1

    :try_start_e
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSurfaceIsBad:Z

    .line 1524
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->notifyAll()V

    .line 1525
    monitor-exit v19

    .line 1529
    :goto_5
    :sswitch_0
    if-eqz v17, :cond_0

    .line 1530
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 1513
    :sswitch_1
    const/4 v10, 0x1

    .line 1514
    goto :goto_5

    .line 1525
    :catchall_4
    move-exception v18

    monitor-exit v19
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :try_start_f
    throw v18
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 1541
    .end local v13    # "swapError":I
    .end local v15    # "view":Lcom/google/android/opengl/common/GLSurfaceView;
    :catchall_5
    move-exception v18

    :try_start_10
    monitor-exit v19
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    throw v18

    .line 1506
    nop

    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method

.method private readyToDraw()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1550
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHasSurface:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSurfaceIsBad:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWidth:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHeight:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestRender:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderMode:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private stopEglContextLocked()V
    .locals 1

    .prologue
    .line 1256
    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    .line 1257
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    invoke-virtual {v0}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->finish()V

    .line 1258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    .line 1259
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->releaseEglContextLocked(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V

    .line 1261
    :cond_0
    return-void
.end method

.method private stopEglSurfaceLocked()V
    .locals 1

    .prologue
    .line 1245
    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    .line 1246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    .line 1247
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEglHelper:Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;

    invoke-virtual {v0}, Lcom/google/android/opengl/common/GLSurfaceView$EglHelper;->destroySurface()V

    .line 1249
    :cond_0
    return-void
.end method


# virtual methods
.method public ableToDraw()Z
    .locals 1

    .prologue
    .line 1546
    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->readyToDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRenderMode()I
    .locals 2

    .prologue
    .line 1566
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1567
    :try_start_0
    iget v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderMode:I

    monitor-exit v1

    return v0

    .line 1568
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1613
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1617
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestPaused:Z

    .line 1618
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1619
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1624
    :try_start_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1625
    :catch_0
    move-exception v0

    .line 1626
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1629
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1630
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 1633
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1637
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestPaused:Z

    .line 1638
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestRender:Z

    .line 1639
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderComplete:Z

    .line 1640
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1641
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1646
    :try_start_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1647
    :catch_0
    move-exception v0

    .line 1648
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1651
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1652
    return-void
.end method

.method public onWindowResize(II)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1655
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1656
    :try_start_0
    iput p1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWidth:I

    .line 1657
    iput p2, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHeight:I

    .line 1658
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mSizeChanged:Z

    .line 1659
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestRender:Z

    .line 1660
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderComplete:Z

    .line 1661
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1665
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mPaused:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderComplete:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->ableToDraw()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1670
    :try_start_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1671
    :catch_0
    move-exception v0

    .line 1672
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1675
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1676
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 1704
    if-nez p1, :cond_0

    .line 1705
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1707
    :cond_0
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1708
    :try_start_0
    iget-object v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1709
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1710
    monitor-exit v1

    .line 1711
    return-void

    .line 1710
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public requestExitAndWait()V
    .locals 3

    .prologue
    .line 1681
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1682
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mShouldExit:Z

    .line 1683
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1684
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1686
    :try_start_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1687
    :catch_0
    move-exception v0

    .line 1688
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1691
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1692
    return-void
.end method

.method public requestReleaseEglContextLocked()V
    .locals 1

    .prologue
    .line 1695
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mShouldReleaseEglContext:Z

    .line 1696
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1697
    return-void
.end method

.method public requestRender()V
    .locals 2

    .prologue
    .line 1572
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1573
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRequestRender:Z

    .line 1574
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1575
    monitor-exit v1

    .line 1576
    return-void

    .line 1575
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GLThread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->setName(Ljava/lang/String;)V

    .line 1232
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->guardedRun()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1236
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->threadExiting(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V

    .line 1238
    :goto_0
    return-void

    .line 1233
    :catch_0
    move-exception v0

    .line 1236
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->threadExiting(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;->threadExiting(Lcom/google/android/opengl/common/GLSurfaceView$GLThread;)V

    throw v0
.end method

.method public setRenderMode(I)V
    .locals 2
    .param p1, "renderMode"    # I

    .prologue
    .line 1556
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1557
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1559
    :cond_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1560
    :try_start_0
    iput p1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mRenderMode:I

    .line 1561
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1562
    monitor-exit v1

    .line 1563
    return-void

    .line 1562
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public surfaceCreated()V
    .locals 3

    .prologue
    .line 1579
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1583
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHasSurface:Z

    .line 1584
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1585
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWaitingForSurface:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1587
    :try_start_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1588
    :catch_0
    move-exception v0

    .line 1589
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1592
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1593
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 3

    .prologue
    .line 1596
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v2

    monitor-enter v2

    .line 1600
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mHasSurface:Z

    .line 1601
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1602
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mWaitingForSurface:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/opengl/common/GLSurfaceView$GLThread;->mExited:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1604
    :try_start_1
    # getter for: Lcom/google/android/opengl/common/GLSurfaceView;->sGLThreadManager:Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;
    invoke-static {}, Lcom/google/android/opengl/common/GLSurfaceView;->access$800()Lcom/google/android/opengl/common/GLSurfaceView$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1605
    :catch_0
    move-exception v0

    .line 1606
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1609
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1610
    return-void
.end method
