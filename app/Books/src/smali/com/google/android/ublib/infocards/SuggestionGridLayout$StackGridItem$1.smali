.class Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->bringToFrontAndCollapse(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1286
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    iput-object p2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->val$v:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1291
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->val$v:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1292
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    iget-boolean v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v0, :cond_0

    .line 1293
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    .line 1297
    :goto_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->updateItemBackgrounds()V

    .line 1298
    return-void

    .line 1295
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;->this$1:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->requestLayout()V

    goto :goto_0
.end method
