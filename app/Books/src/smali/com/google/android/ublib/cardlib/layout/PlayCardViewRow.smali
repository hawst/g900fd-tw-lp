.class public Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.source "PlayCardViewRow.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method private measureThumbnailSpanningHeight(I)V
    .locals 8
    .param p1, "heightMeasureSpec"    # I

    .prologue
    .line 21
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 23
    .local v0, "availableHeight":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;->getPaddingTop()I

    move-result v2

    .line 24
    .local v2, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;->getPaddingBottom()I

    move-result v1

    .line 26
    .local v1, "paddingBottom":I
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 28
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v6, v0, v2

    sub-int/2addr v6, v1

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v6, v7

    .line 30
    .local v3, "thumbnailHeight":I
    int-to-float v6, v3

    iget v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;->mThumbnailAspectRatio:F

    div-float/2addr v6, v7

    float-to-int v5, v6

    .line 31
    .local v5, "thumbnailWidth":I
    iput v5, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 32
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 43
    invoke-super/range {p0 .. p5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onLayout(ZIIII)V

    .line 44
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 36
    invoke-direct {p0, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewRow;->measureThumbnailSpanningHeight(I)V

    .line 38
    invoke-super {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onMeasure(II)V

    .line 39
    return-void
.end method
