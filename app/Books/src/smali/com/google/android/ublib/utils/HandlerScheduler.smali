.class public Lcom/google/android/ublib/utils/HandlerScheduler;
.super Ljava/lang/Object;
.source "HandlerScheduler.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Scheduler;


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/ublib/utils/HandlerScheduler;-><init>(Landroid/os/Handler;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/ublib/utils/HandlerScheduler;->mHandler:Landroid/os/Handler;

    .line 19
    return-void
.end method


# virtual methods
.method public getTime()J
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public schedule(Ljava/lang/Runnable;J)V
    .locals 4
    .param p1, "task"    # Ljava/lang/Runnable;
    .param p2, "delayMillis"    # J

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/ublib/utils/HandlerScheduler;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/ublib/utils/HandlerScheduler;->getTime()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-virtual {v0, p1, p0, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 24
    return-void
.end method
