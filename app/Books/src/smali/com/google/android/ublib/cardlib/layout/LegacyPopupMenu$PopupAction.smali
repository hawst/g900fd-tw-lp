.class Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;
.super Ljava/lang/Object;
.source "LegacyPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PopupAction"
.end annotation


# instance fields
.field private final mActionListener:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

.field private mCachedCustomRow:Landroid/view/View;

.field private final mCustomView:Landroid/view/View;

.field private final mIsEnabled:Z

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "customView"    # Landroid/view/View;
    .param p4, "actionListener"    # Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mTitle:Ljava/lang/String;

    .line 64
    iput-boolean p2, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mIsEnabled:Z

    .line 65
    iput-object p3, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;

    .line 66
    iput-object p4, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mIsEnabled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mTitle:Ljava/lang/String;

    return-object v0
.end method
