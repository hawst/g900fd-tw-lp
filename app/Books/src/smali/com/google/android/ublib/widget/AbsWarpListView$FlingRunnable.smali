.class Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;
.super Ljava/lang/Object;
.source "AbsWarpListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/AbsWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlingRunnable"
.end annotation


# instance fields
.field private isKilled:Z

.field private final mFinalOffset:I

.field private mScrollOffset:I

.field private final mScroller:Landroid/widget/Scroller;

.field private final mScrollerEndOffset:I

.field final synthetic this$0:Lcom/google/android/ublib/widget/AbsWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 12
    .param p2, "distance"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1173
    iput-object p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167
    new-instance v0, Landroid/widget/Scroller;

    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    .line 1174
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1300(Lcom/google/android/ublib/widget/AbsWarpListView;)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->setFriction(F)V

    .line 1176
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v11

    .line 1177
    .local v11, "centerPos":I
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v10

    .line 1179
    .local v10, "centerOff":I
    invoke-virtual {p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(I)V

    .line 1181
    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mSnapMode:I
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1400(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v0

    if-ne v0, v5, :cond_0

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterAlignOffset(I)I
    invoke-static {p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1500(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    move-result v9

    .line 1184
    .local v9, "addDistance":I
    :goto_0
    invoke-virtual {p1, v9}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(I)V

    .line 1186
    add-int v0, p2, v9

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mFinalOffset:I

    .line 1187
    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$200(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v0

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I
    invoke-static {p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$802(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1188
    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$300(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v0

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I
    invoke-static {p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$902(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1189
    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1800(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v0

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingCenterPosition:I
    invoke-static {p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1702(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1191
    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V
    invoke-static {p1, v11, v10}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1900(Lcom/google/android/ublib/widget/AbsWarpListView;II)V

    .line 1192
    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->updateViewVisibility()V
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2000(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 1194
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mFinalOffset:I

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->getFlingVelocityForDistance(I)I
    invoke-static {p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2100(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    move-result v3

    .line 1195
    .local v3, "velocity":I
    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->getFlingDistanceForVelocity(I)I
    invoke-static {p1, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2200(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollerEndOffset:I

    .line 1197
    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->setScrollState(I)V
    invoke-static {p1, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2300(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    .line 1198
    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z
    invoke-static {p1, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1102(Lcom/google/android/ublib/widget/AbsWarpListView;Z)Z

    .line 1199
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v2, v1

    move v4, v1

    move v7, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1201
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->run()V

    .line 1202
    return-void

    .line 1181
    .end local v3    # "velocity":I
    .end local v9    # "addDistance":I
    :cond_0
    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mSnapMode:I
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1400(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v0

    if-ne v0, v4, :cond_1

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->getLeftAlignOffset(I)I
    invoke-static {p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1600(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    move-result v9

    goto :goto_0

    :cond_1
    move v9, v1

    goto :goto_0
.end method

.method static synthetic access$1200(Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;)Landroid/widget/Scroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method


# virtual methods
.method getProgress()F
    .locals 2

    .prologue
    .line 1253
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollOffset:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mFinalOffset:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method kill(Z)V
    .locals 4
    .param p1, "toNotify"    # Z

    .prologue
    const/4 v3, 0x0

    .line 1239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->isKilled:Z

    .line 1240
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I
    invoke-static {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$400(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I
    invoke-static {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$500(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 1241
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I
    invoke-static {v0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$402(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1242
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    const/4 v1, -0x1

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I
    invoke-static {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$502(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1243
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v2

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1900(Lcom/google/android/ublib/widget/AbsWarpListView;II)V

    .line 1244
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->updateViewVisibility()V
    invoke-static {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2000(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 1247
    :cond_0
    if-eqz p1, :cond_1

    .line 1248
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->setScrollState(I)V
    invoke-static {v0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2300(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    .line 1250
    :cond_1
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 1206
    iget-boolean v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->isKilled:Z

    if-eqz v2, :cond_0

    .line 1236
    :goto_0
    return-void

    .line 1209
    :cond_0
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    .line 1210
    .local v0, "isAnimating":Z
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    .line 1212
    .local v1, "newOffset":I
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollerEndOffset:I

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mFinalOffset:I

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollerEndOffset:I

    if-eqz v2, :cond_1

    .line 1218
    int-to-long v2, v1

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mFinalOffset:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollerEndOffset:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 1221
    :cond_1
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollOffset:I

    sub-int v3, v1, v3

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(I)V

    .line 1222
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->reportScroll()V
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2400(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 1223
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScrollOffset:I

    .line 1224
    if-eqz v0, :cond_2

    .line 1225
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-static {v2, p0}, Lcom/google/android/ublib/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1227
    :cond_2
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    new-instance v3, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable$1;

    invoke-direct {v3, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable$1;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;)V

    invoke-static {v2, v3}, Lcom/google/android/ublib/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
