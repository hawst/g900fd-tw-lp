.class Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;
.super Ljava/lang/Object;
.source "AbsWarpListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/AbsWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemClickListener"
.end annotation


# instance fields
.field private final mPosition:I

.field final synthetic this$0:Lcom/google/android/ublib/widget/AbsWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 0
    .param p2, "position"    # I

    .prologue
    .line 541
    iput-object p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542
    iput p2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->mPosition:I

    .line 543
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 547
    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    .line 548
    .local v0, "listener":Landroid/widget/AdapterView$OnItemClickListener;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z
    invoke-static {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1100(Lcom/google/android/ublib/widget/AbsWarpListView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 549
    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->mPosition:I

    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$000(Lcom/google/android/ublib/widget/AbsWarpListView;)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v2

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;->mPosition:I

    invoke-interface {v2, v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemId(I)J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 552
    :cond_0
    return-void
.end method
