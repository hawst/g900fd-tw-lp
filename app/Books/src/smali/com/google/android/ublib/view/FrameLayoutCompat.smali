.class public Lcom/google/android/ublib/view/FrameLayoutCompat;
.super Landroid/widget/FrameLayout;
.source "FrameLayoutCompat.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method


# virtual methods
.method public onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 32
    sub-int v9, p4, p2

    .line 33
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/google/android/ublib/view/FrameLayoutCompat;->getChildCount()I

    move-result v6

    .line 34
    .local v6, "childCount":I
    const/4 v7, 0x0

    .local v7, "childIndex":I
    :goto_0
    if-ge v7, v6, :cond_1

    .line 35
    invoke-virtual {p0, v7}, Lcom/google/android/ublib/view/FrameLayoutCompat;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 36
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout$LayoutParams;

    .line 38
    .local v8, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget v0, v8, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x7

    const/4 v4, 0x5

    if-ne v0, v4, :cond_0

    .line 39
    iget v0, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v0, v4

    iget v4, v8, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v0, v4

    sub-int v2, v9, v0

    .line 44
    .local v2, "childLeft":I
    :goto_1
    iget v3, v8, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 45
    .local v3, "childTop":I
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    .line 34
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 42
    .end local v2    # "childLeft":I
    .end local v3    # "childTop":I
    :cond_0
    iget v2, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .restart local v2    # "childLeft":I
    goto :goto_1

    .line 48
    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "childLeft":I
    .end local v8    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    return-void
.end method
