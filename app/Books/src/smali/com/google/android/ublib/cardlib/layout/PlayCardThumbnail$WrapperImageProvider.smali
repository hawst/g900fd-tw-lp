.class Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;
.super Ljava/lang/Object;
.source "PlayCardThumbnail.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperImageProvider"
.end annotation


# instance fields
.field mProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

.field final synthetic this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V
    .locals 0
    .param p2, "provider"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->mProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    .line 171
    return-void
.end method


# virtual methods
.method public cancelFetch()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->mProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider;->cancelFetch()V

    .line 184
    return-void
.end method

.method public fetchImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V
    .locals 3
    .param p1, "imageUri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "listener"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mRequestingImage:Z
    invoke-static {v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->access$002(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Z)Z

    .line 177
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->mProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    new-instance v1, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-direct {v1, v2, p3}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;-><init>(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider;->fetchImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;->this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mRequestingImage:Z
    invoke-static {v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->access$002(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Z)Z

    .line 179
    return-void
.end method
