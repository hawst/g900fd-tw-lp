.class Lcom/google/android/ublib/widget/VerticalWarpListView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "VerticalWarpListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/VerticalWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/widget/VerticalWarpListView;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    iget-object v3, v3, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    iget-object v3, v3, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    iget-object v1, v1, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    .line 84
    .local v0, "maxVelocity":I
    neg-int v1, v0

    int-to-float v1, v1

    invoke-static {p4, v1}, Ljava/lang/Math;->max(FF)F

    move-result p4

    .line 85
    int-to-float v1, v0

    invoke-static {p4, v1}, Ljava/lang/Math;->min(FF)F

    move-result p4

    .line 87
    iget-object v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    neg-float v2, p4

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/google/android/ublib/widget/VerticalWarpListView;->fling(I)V

    .line 88
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 43
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    invoke-virtual {v3, v2}, Lcom/google/android/ublib/widget/VerticalWarpListView;->stopFling(Z)V

    .line 45
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    iget-object v3, v3, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    int-to-float v0, v3

    .line 47
    .local v0, "touchSlop":F
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrolling:Z
    invoke-static {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$000(Lcom/google/android/ublib/widget/VerticalWarpListView;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mDownX:F
    invoke-static {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$100(Lcom/google/android/ublib/widget/VerticalWarpListView;)F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v0

    if-lez v3, :cond_0

    .line 48
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->registerDrag()V

    .line 49
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # setter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrollBlocked:Z
    invoke-static {v3, v2}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$202(Lcom/google/android/ublib/widget/VerticalWarpListView;Z)Z

    .line 52
    :cond_0
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrollBlocked:Z
    invoke-static {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$200(Lcom/google/android/ublib/widget/VerticalWarpListView;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->needsScrollFilter()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 64
    :cond_1
    :goto_0
    return v1

    .line 56
    :cond_2
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mDownY:F
    invoke-static {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$300(Lcom/google/android/ublib/widget/VerticalWarpListView;)F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v0

    if-lez v3, :cond_3

    .line 57
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # setter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrolling:Z
    invoke-static {v3, v2}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$002(Lcom/google/android/ublib/widget/VerticalWarpListView;Z)Z

    .line 60
    :cond_3
    iget-object v3, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrolling:Z
    invoke-static {v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->access$000(Lcom/google/android/ublib/widget/VerticalWarpListView;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    iget-object v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    float-to-int v3, p4

    invoke-virtual {v1, v3}, Lcom/google/android/ublib/widget/VerticalWarpListView;->drag(I)V

    move v1, v2

    .line 62
    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/VerticalWarpListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/VerticalWarpListView;->stopFling(Z)V

    .line 70
    return-void
.end method
