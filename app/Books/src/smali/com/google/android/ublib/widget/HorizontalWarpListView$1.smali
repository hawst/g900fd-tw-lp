.class Lcom/google/android/ublib/widget/HorizontalWarpListView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HorizontalWarpListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/HorizontalWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/widget/HorizontalWarpListView;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v3, v3, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v3, v3, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v1, v1, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    .line 84
    .local v0, "maxVelocity":I
    neg-int v1, v0

    int-to-float v1, v1

    invoke-static {p3, v1}, Ljava/lang/Math;->max(FF)F

    move-result p3

    .line 85
    int-to-float v1, v0

    invoke-static {p3, v1}, Ljava/lang/Math;->min(FF)F

    move-result p3

    .line 87
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z
    invoke-static {v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$400(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, p3

    :goto_1
    float-to-int v1, v1

    invoke-virtual {v2, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->fling(I)V

    .line 88
    const/4 v1, 0x1

    goto :goto_0

    .line 87
    :cond_2
    neg-float v1, p3

    goto :goto_1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v1, 0x1

    .line 44
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->stopFling(Z)V

    .line 46
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v2, v2, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    int-to-float v0, v2

    .line 48
    .local v0, "touchSlop":F
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrolling:Z
    invoke-static {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$000(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mDownY:F
    invoke-static {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$100(Lcom/google/android/ublib/widget/HorizontalWarpListView;)F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # setter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrollBlocked:Z
    invoke-static {v2, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$202(Lcom/google/android/ublib/widget/HorizontalWarpListView;Z)Z

    .line 50
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->registerDrag()V

    .line 53
    :cond_0
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrollBlocked:Z
    invoke-static {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$200(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->needsScrollFilter()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 54
    const/4 v1, 0x0

    .line 64
    .end local p3    # "distanceX":F
    :cond_1
    :goto_0
    return v1

    .line 57
    .restart local p3    # "distanceX":F
    :cond_2
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mDownX:F
    invoke-static {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$300(Lcom/google/android/ublib/widget/HorizontalWarpListView;)F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v0

    if-lez v2, :cond_3

    .line 58
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # setter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrolling:Z
    invoke-static {v2, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$002(Lcom/google/android/ublib/widget/HorizontalWarpListView;Z)Z

    .line 61
    :cond_3
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrolling:Z
    invoke-static {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$000(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v3, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    # getter for: Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z
    invoke-static {v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->access$400(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z

    move-result v3

    if-eqz v3, :cond_4

    neg-float p3, p3

    .end local p3    # "distanceX":F
    :cond_4
    float-to-int v3, p3

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->drag(I)V

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;->this$0:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->stopFling(Z)V

    .line 70
    return-void
.end method
