.class public Lcom/google/android/ublib/view/JellybeanSystemUi;
.super Ljava/lang/Object;
.source "JellybeanSystemUi.java"

# interfaces
.implements Lcom/google/android/ublib/view/SystemUi;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/view/JellybeanSystemUi$1;,
        Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;
    }
.end annotation


# instance fields
.field private mLayoutFlags:I

.field private mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

.field private final mListenerWrapper:Landroid/view/View$OnSystemUiVisibilityChangeListener;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;-><init>(Lcom/google/android/ublib/view/JellybeanSystemUi;Lcom/google/android/ublib/view/JellybeanSystemUi$1;)V

    iput-object v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mListenerWrapper:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    .line 24
    iput-object p1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    .line 25
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/ublib/view/JellybeanSystemUi;)Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/view/JellybeanSystemUi;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    return-object v0
.end method

.method private turnStatusBarOff()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mLayoutFlags:I

    invoke-virtual {p0}, Lcom/google/android/ublib/view/JellybeanSystemUi;->getFlagsToHideSystemBars()I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 42
    return-void
.end method

.method private turnStatusBarOn()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mLayoutFlags:I

    or-int/lit8 v1, v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public copyFlagsTo(Lcom/google/android/ublib/view/SystemUi;)V
    .locals 2
    .param p1, "flagReceiver"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    .line 99
    check-cast p1, Lcom/google/android/ublib/view/JellybeanSystemUi;

    .end local p1    # "flagReceiver":Lcom/google/android/ublib/view/SystemUi;
    iget-object v0, p1, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 101
    return-void
.end method

.method protected getFlagsToHideSystemBars()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x5

    return v0
.end method

.method protected getFullscreenLayoutFlags()I
    .locals 1

    .prologue
    .line 87
    const/16 v0, 0x500

    return v0
.end method

.method public setOnSystemUiVisibilityChangeListener(Lcom/google/android/ublib/view/SystemUi$VisibilityListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    .line 60
    if-eqz p1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mListenerWrapper:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    .line 65
    .local v0, "listenerWrapper":Landroid/view/View$OnSystemUiVisibilityChangeListener;
    :goto_0
    iget-object v1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 66
    return-void

    .line 63
    .end local v0    # "listenerWrapper":Landroid/view/View$OnSystemUiVisibilityChangeListener;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "listenerWrapper":Landroid/view/View$OnSystemUiVisibilityChangeListener;
    goto :goto_0
.end method

.method public setSystemUiVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 29
    if-eqz p1, :cond_0

    .line 30
    invoke-direct {p0}, Lcom/google/android/ublib/view/JellybeanSystemUi;->turnStatusBarOn()V

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-direct {p0}, Lcom/google/android/ublib/view/JellybeanSystemUi;->turnStatusBarOff()V

    goto :goto_0
.end method

.method public setViewFullscreen(Z)V
    .locals 2
    .param p1, "fullScreen"    # Z

    .prologue
    .line 46
    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/ublib/view/JellybeanSystemUi;->getFullscreenLayoutFlags()I

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mLayoutFlags:I

    .line 51
    :goto_0
    iget-object v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mLayoutFlags:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 52
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ublib/view/JellybeanSystemUi;->mLayoutFlags:I

    goto :goto_0
.end method
