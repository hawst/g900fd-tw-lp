.class public Lcom/google/android/ublib/utils/WrappedIoException;
.super Ljava/io/IOException;
.source "WrappedIoException.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/google/android/ublib/utils/WrappedIoException;->toCauseString(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0, p2}, Lcom/google/android/ublib/utils/WrappedIoException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/ublib/utils/WrappedIoException;->toCauseString(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/utils/WrappedIoException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 29
    return-void
.end method

.method public static maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;
    .locals 1
    .param p0, "e"    # Ljava/lang/Exception;

    .prologue
    .line 36
    instance-of v0, p0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 37
    check-cast p0, Ljava/io/IOException;

    .line 39
    .end local p0    # "e":Ljava/lang/Exception;
    :goto_0
    return-object p0

    .restart local p0    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v0, Lcom/google/android/ublib/utils/WrappedIoException;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/Throwable;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private static toCauseString(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "cause"    # Ljava/lang/Throwable;
    .param p1, "fallback"    # Ljava/lang/String;

    .prologue
    .line 50
    if-eqz p0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .end local p1    # "fallback":Ljava/lang/String;
    :cond_0
    return-object p1
.end method
