.class public Lcom/google/android/ublib/view/RelativeLayoutCompat;
.super Landroid/widget/RelativeLayout;
.source "RelativeLayoutCompat.java"


# instance fields
.field private mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 35
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutCompat;->mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutCompat;->mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;->onLayoutChange(Landroid/view/View;IIII)V

    .line 39
    :cond_0
    return-void
.end method
