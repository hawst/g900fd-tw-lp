.class Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/infocards/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimpleGridItem"
.end annotation


# instance fields
.field mDismissTrail:Landroid/view/View;

.field mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

.field final synthetic this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

.field view:Landroid/view/View;

.field views:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;Lcom/google/android/ublib/infocards/DismissTrailFactory;)V
    .locals 1
    .param p2, "v"    # Landroid/view/View;
    .param p3, "dismissTrailFactory"    # Lcom/google/android/ublib/infocards/DismissTrailFactory;

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    .line 1007
    iput-object p2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    .line 1008
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1009
    iput-object p3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    .line 1010
    return-void
.end method


# virtual methods
.method public getGridLayoutParams()Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    return-object v0
.end method

.method public getMeasuredHeight()I
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getMeasuredWidth()I
    .locals 1

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public getViews()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic getViews()Ljava/util/List;
    .locals 1

    .prologue
    .line 1000
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->getViews()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public gridLayout(IIII)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    .line 1022
    return-void
.end method

.method public gridMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 1018
    return-void
.end method

.method public isGone()Z
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeExtraViews()V
    .locals 0

    .prologue
    .line 1067
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1038
    return-void
.end method

.method public shouldShowDismissTrail()Z
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    invoke-interface {v0}, Lcom/google/android/ublib/infocards/DismissTrailFactory;->shouldShowDismissTrail()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showDismissTrail()V
    .locals 6

    .prologue
    .line 1055
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    invoke-interface {v2}, Lcom/google/android/ublib/infocards/DismissTrailFactory;->createDismissTrail()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    .line 1056
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 1057
    .local v1, "viewLayoutParams":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iget v4, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    iget-boolean v5, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;IZ)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1200(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;IZ)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v0

    .line 1059
    .local v0, "layoutParams":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    sget-object v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->FADE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v2, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 1060
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1301(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1061
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    .line 1062
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1063
    return-void
.end method
