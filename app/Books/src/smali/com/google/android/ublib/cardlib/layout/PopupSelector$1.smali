.class Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;
.super Ljava/lang/Object;
.source "PopupSelector.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/cardlib/layout/PopupSelector;->setupList(Landroid/view/View;)Landroid/widget/ListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;->this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "list":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-gez p3, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;->this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->dismiss()V

    .line 220
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;->this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    # getter for: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
    invoke-static {v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$500(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;->this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    # getter for: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
    invoke-static {v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$500(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->onSelect(I)V

    goto :goto_0
.end method
