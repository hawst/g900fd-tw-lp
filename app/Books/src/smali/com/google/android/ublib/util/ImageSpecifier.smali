.class public Lcom/google/android/ublib/util/ImageSpecifier;
.super Ljava/lang/Object;
.source "ImageSpecifier.java"


# instance fields
.field public constraints:Lcom/google/android/ublib/util/ImageConstraints;

.field public uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    .line 20
    iput-object p2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 36
    instance-of v2, p1, Lcom/google/android/ublib/util/ImageSpecifier;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/google/android/ublib/util/ImageSpecifier;

    .line 38
    .local v0, "other":Lcom/google/android/ublib/util/ImageSpecifier;
    iget-object v2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    iget-object v3, v0, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    iget-object v3, v0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 40
    .end local v0    # "other":Lcom/google/android/ublib/util/ImageSpecifier;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 25
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "uri"

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "constraints"

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
