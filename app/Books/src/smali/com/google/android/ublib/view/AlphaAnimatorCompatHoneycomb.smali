.class public Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;
.super Ljava/lang/Object;
.source "AlphaAnimatorCompatHoneycomb.java"

# interfaces
.implements Lcom/google/android/ublib/view/AnimatorCompat;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;
    }
.end annotation


# instance fields
.field private final mAnimator:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/view/View;FFILcom/google/android/ublib/view/AnimatorCompat$Interpolation;Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "fromAlpha"    # F
    .param p3, "toAlpha"    # F
    .param p4, "durationMillis"    # I
    .param p5, "interpolation"    # Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;
    .param p6, "listener"    # Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    .line 28
    .local v0, "startAlpha":F
    const-string v1, "AlphaAnimHC"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    const-string v1, "AlphaAnimHC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting animation from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    :cond_0
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v3, 0x1

    aput p3, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;->mAnimator:Landroid/animation/ObjectAnimator;

    .line 33
    iget-object v1, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;->mAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v2, p4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 34
    sget-object v1, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->LINEAR:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    if-ne p5, v1, :cond_1

    .line 35
    iget-object v1, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;->mAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 38
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;->mAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;

    invoke-direct {v2, p0, p6}, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;-><init>(Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 39
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 49
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 44
    return-void
.end method
