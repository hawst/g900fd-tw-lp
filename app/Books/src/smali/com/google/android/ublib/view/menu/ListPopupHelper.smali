.class public Lcom/google/android/ublib/view/menu/ListPopupHelper;
.super Ljava/lang/Object;
.source "ListPopupHelper.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;
    }
.end annotation


# instance fields
.field private mAnchorView:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private mMeasureParent:Landroid/view/ViewGroup;

.field private mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

.field private final mPopupMaxWidth:I

.field private final mSource:Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;

.field private mTreeObserver:Landroid/view/ViewTreeObserver;


# direct methods
.method private measureContentWidth(Landroid/widget/ListAdapter;)I
    .locals 10
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v8, 0x0

    .line 162
    const/4 v6, 0x0

    .line 163
    .local v6, "width":I
    const/4 v4, 0x0

    .line 164
    .local v4, "itemView":Landroid/view/View;
    const/4 v3, 0x0

    .line 165
    .local v3, "itemType":I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 167
    .local v7, "widthMeasureSpec":I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 169
    .local v1, "heightMeasureSpec":I
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 170
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 171
    invoke-interface {p1, v2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v5

    .line 181
    .local v5, "positionType":I
    const/4 v4, 0x0

    .line 182
    iget-object v8, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mMeasureParent:Landroid/view/ViewGroup;

    if-nez v8, :cond_0

    .line 183
    new-instance v8, Landroid/widget/FrameLayout;

    iget-object v9, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mMeasureParent:Landroid/view/ViewGroup;

    .line 185
    :cond_0
    iget-object v8, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mMeasureParent:Landroid/view/ViewGroup;

    invoke-interface {p1, v2, v4, v8}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 186
    invoke-virtual {v4, v7, v1}, Landroid/view/View;->measure(II)V

    .line 187
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 170
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    .end local v5    # "positionType":I
    :cond_1
    return v6
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->dismiss()V

    .line 129
    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    iput-object v1, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    .line 133
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mSource:Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;

    invoke-interface {v0}, Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;->onClose()V

    .line 134
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 137
    iput-object v1, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    .line 140
    :cond_1
    return-void
.end method

.method public onGlobalLayout()V
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mAnchorView:Landroid/view/View;

    .line 196
    .local v0, "anchor":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v1

    if-nez v1, :cond_2

    .line 197
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->dismiss()V

    .line 203
    .end local v0    # "anchor":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 198
    .restart local v0    # "anchor":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    iget-object v1, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/ListPopupWindow;->show()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mSource:Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;

    invoke-interface {v0, p3}, Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;->onItemClick(I)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->dismiss()V

    .line 150
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 153
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->dismiss()V

    .line 157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tryShow()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 99
    new-instance v4, Lcom/google/android/ublib/widget/ListPopupWindow;

    iget-object v5, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {}, Lcom/google/android/ublib/view/MiscViewUtils;->getPopupMenuStyle()I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/ublib/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    .line 100
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4, p0}, Lcom/google/android/ublib/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 101
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4, p0}, Lcom/google/android/ublib/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 103
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mSource:Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;

    invoke-interface {v4}, Lcom/google/android/ublib/view/menu/ListPopupHelper$ListPopupDelegate;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 104
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4, v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 105
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4, v3}, Lcom/google/android/ublib/widget/ListPopupWindow;->setModal(Z)V

    .line 107
    iget-object v2, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mAnchorView:Landroid/view/View;

    .line 108
    .local v2, "anchor":Landroid/view/View;
    if-eqz v2, :cond_2

    .line 109
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    if-nez v4, :cond_0

    move v1, v3

    .line 110
    .local v1, "addGlobalListener":Z
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    .line 111
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mTreeObserver:Landroid/view/ViewTreeObserver;

    invoke-virtual {v4, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 113
    :cond_1
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4, v2}, Lcom/google/android/ublib/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 118
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-direct {p0, v0}, Lcom/google/android/ublib/view/menu/ListPopupHelper;->measureContentWidth(Landroid/widget/ListAdapter;)I

    move-result v5

    iget v6, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopupMaxWidth:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/ublib/widget/ListPopupWindow;->setContentWidth(I)V

    .line 119
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/google/android/ublib/widget/ListPopupWindow;->setInputMethodMode(I)V

    .line 120
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/ListPopupWindow;->show()V

    .line 121
    iget-object v4, p0, Lcom/google/android/ublib/view/menu/ListPopupHelper;->mPopup:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 122
    .end local v1    # "addGlobalListener":Z
    :goto_0
    return v3

    :cond_2
    move v3, v1

    .line 115
    goto :goto_0
.end method
