.class Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;
.super Ljava/lang/Object;
.source "AlphaAnimatorCompatHoneycomb.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimatorListenerWrapper"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

.field final synthetic this$0:Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;)V
    .locals 0
    .param p2, "listener"    # Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;->this$0:Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;->mListener:Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

    .line 55
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 66
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;->mListener:Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

    invoke-interface {v0}, Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;->onAnimationEnd()V

    .line 63
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 69
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb$AnimatorListenerWrapper;->mListener:Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

    invoke-interface {v0}, Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;->onAnimationStart()V

    .line 59
    return-void
.end method
