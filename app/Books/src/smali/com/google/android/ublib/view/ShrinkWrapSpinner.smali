.class public Lcom/google/android/ublib/view/ShrinkWrapSpinner;
.super Landroid/widget/Spinner;
.source "ShrinkWrapSpinner.java"


# instance fields
.field private mCurrentlyMeasuring:Z


# direct methods
.method private createProxyAdapter()Landroid/widget/SpinnerAdapter;
    .locals 4

    .prologue
    .line 70
    invoke-super {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    .line 71
    .local v2, "superAdapter":Landroid/widget/SpinnerAdapter;
    invoke-virtual {p0}, Lcom/google/android/ublib/view/ShrinkWrapSpinner;->getSelectedItemPosition()I

    move-result v1

    .line 72
    .local v1, "selectedItemPosition":I
    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 74
    .local v0, "sameView":Landroid/view/View;
    new-instance v3, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;

    invoke-direct {v3, p0, v0, v2, v1}, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;-><init>(Lcom/google/android/ublib/view/ShrinkWrapSpinner;Landroid/view/View;Landroid/widget/SpinnerAdapter;I)V

    return-object v3
.end method


# virtual methods
.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/android/ublib/view/ShrinkWrapSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/SpinnerAdapter;
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner;->mCurrentlyMeasuring:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/google/android/ublib/view/ShrinkWrapSpinner;->createProxyAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner;->mCurrentlyMeasuring:Z

    .line 56
    invoke-super {p0, p1, p2}, Landroid/widget/Spinner;->onMeasure(II)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner;->mCurrentlyMeasuring:Z

    .line 58
    return-void
.end method
