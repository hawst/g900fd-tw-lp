.class Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;
.super Ljava/lang/Object;
.source "Consumers.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/utils/Consumers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WeakWrapperConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;, "Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer<TT;>;"
    .local p1, "inner":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;->mReference:Ljava/lang/ref/WeakReference;

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumers$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/utils/Consumer;
    .param p2, "x1"    # Lcom/google/android/ublib/utils/Consumers$1;

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;, "Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;-><init>(Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;, "Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer<TT;>;"
    .local p1, "t":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;->mReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/utils/Consumer;

    .line 32
    .local v0, "inner":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    if-eqz v0, :cond_0

    .line 33
    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 35
    :cond_0
    return-void
.end method
