.class public Lcom/google/android/ublib/view/AlphaAnimatorCompat;
.super Ljava/lang/Object;
.source "AlphaAnimatorCompat.java"


# direct methods
.method public static makeAlphaAnimation(Landroid/view/View;FFILcom/google/android/ublib/view/AnimatorCompat$Interpolation;Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;)Lcom/google/android/ublib/view/AnimatorCompat;
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "fromAlpha"    # F
    .param p2, "toAlpha"    # F
    .param p3, "durationMillis"    # I
    .param p4, "interpolation"    # Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;
    .param p5, "listener"    # Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ublib/view/AlphaAnimatorCompatHoneycomb;-><init>(Landroid/view/View;FFILcom/google/android/ublib/view/AnimatorCompat$Interpolation;Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;)V

    return-object v0
.end method
