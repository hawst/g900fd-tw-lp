.class public Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;
.super Landroid/widget/FrameLayout;
.source "PlayCardThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;,
        Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;
    }
.end annotation


# instance fields
.field private mAnimationEnabled:Z

.field private mRequestingImage:Z

.field private mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

.field private mThumbnailMaxHeight:I

.field private mThumbnailMaxWidth:I

.field private final mVisibilityController:Lcom/google/android/ublib/view/FadeAnimationController;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    iput v2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxWidth:I

    .line 50
    iput v2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxHeight:I

    .line 52
    new-instance v0, Lcom/google/android/ublib/view/FadeAnimationController;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mVisibilityController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 53
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mVisibilityController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    .line 54
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mRequestingImage:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->onImageStatusChanged(Z)V

    return-void
.end method

.method private debugLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 207
    const-string v0, "PlayCardThumbnail"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    return-void
.end method

.method private onImageStatusChanged(Z)V
    .locals 2
    .param p1, "available"    # Z

    .prologue
    .line 155
    const-string v0, "PlayCardThumbnail"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changing visibility to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->debugLog(Ljava/lang/String;)V

    .line 159
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mAnimationEnabled:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mRequestingImage:Z

    if-nez v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mVisibilityController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mVisibilityController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    goto :goto_0
.end method

.method private updatePadding()V
    .locals 2

    .prologue
    .line 117
    iget v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxWidth:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxHeight:I

    if-nez v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 125
    .local v0, "thumbnailLp":Landroid/view/ViewGroup$LayoutParams;
    iget v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxWidth:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 126
    iget v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 127
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;Z)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/ublib/cardlib/model/Document;
    .param p2, "imageProvider"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    .param p3, "scrolling"    # Z

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->updatePadding()V

    .line 103
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getThumbnailURI()Landroid/net/Uri;

    move-result-object v0

    .line 104
    .local v0, "imageUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getArtUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->onImageStatusChanged(Z)V

    .line 108
    :cond_0
    if-eqz p3, :cond_1

    .line 109
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->clearArtwork()V

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    new-instance v2, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;

    invoke-direct {v2, p0, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageProvider;-><init>(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setArtURI(Landroid/net/Uri;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V

    goto :goto_0
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public hasCoverImage()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->hasImage()Z

    move-result v0

    return v0
.end method

.method public maybeLoadCoverImage()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->maybeLoadImage()V

    .line 216
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 60
    const v0, 0x7f0e00cb

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    .line 61
    return-void
.end method

.method public onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 132
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 133
    const-string v0, "PlayCardThumbnail"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "allowed height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requested height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->debugLog(Ljava/lang/String;)V

    .line 137
    :cond_0
    return-void
.end method

.method public setCoverAnimationEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mAnimationEnabled:Z

    if-ne v0, p1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mAnimationEnabled:Z

    goto :goto_0
.end method

.method public setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V
    .locals 1
    .param p1, "fillStyle"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 69
    return-void
.end method

.method public setOptimizedForScrollingEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnail:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setOptimizedForScrollingEnabled(Z)V

    .line 80
    return-void
.end method

.method public setThumbnailMetrics(II)V
    .locals 0
    .param p1, "thumbnailMaxWidth"    # I
    .param p2, "thumbnailMaxHeight"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxWidth:I

    .line 92
    iput p2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->mThumbnailMaxHeight:I

    .line 93
    return-void
.end method
