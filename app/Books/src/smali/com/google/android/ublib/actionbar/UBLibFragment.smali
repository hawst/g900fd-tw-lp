.class public Lcom/google/android/ublib/actionbar/UBLibFragment;
.super Landroid/support/v4/app/Fragment;
.source "UBLibFragment.java"


# instance fields
.field private mHasOptionsMenu:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->invalidateOptionsMenu()V

    .line 37
    return-void
.end method


# virtual methods
.method public setHasOptionsMenu(Z)V
    .locals 0
    .param p1, "hasMenu"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/ublib/actionbar/UBLibFragment;->mHasOptionsMenu:Z

    .line 31
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 32
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->invalidateOptionsMenu()V

    .line 33
    return-void
.end method

.method protected startActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;
    .locals 2
    .param p1, "callback"    # Landroid/support/v7/view/ActionMode$Callback;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 49
    .local v0, "activity":Landroid/app/Activity;
    instance-of v1, v0, Landroid/support/v7/app/ActionBarActivity;

    if-eqz v1, :cond_0

    .line 50
    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarActivity;->startSupportActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v1

    .line 52
    :goto_0
    return-object v1

    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
