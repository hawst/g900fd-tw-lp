.class public Lcom/google/android/ublib/utils/Runnables;
.super Ljava/lang/Object;
.source "Runnables.java"


# direct methods
.method private static createWeakReferenceTask(Ljava/lang/ref/Reference;Lcom/google/android/ublib/utils/Consumer;)Ljava/lang/Runnable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/ref/Reference",
            "<TT;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)",
            "Ljava/lang/Runnable;"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "reference":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<TT;>;"
    .local p1, "task":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    new-instance v0, Lcom/google/android/ublib/utils/Runnables$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/ublib/utils/Runnables$1;-><init>(Ljava/lang/ref/Reference;Lcom/google/android/ublib/utils/Consumer;)V

    return-object v0
.end method

.method public static withWeakReference(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Ljava/lang/Runnable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)",
            "Ljava/lang/Runnable;"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "object":Ljava/lang/Object;, "TT;"
    .local p1, "task":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, p1}, Lcom/google/android/ublib/utils/Runnables;->createWeakReferenceTask(Ljava/lang/ref/Reference;Lcom/google/android/ublib/utils/Consumer;)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method
