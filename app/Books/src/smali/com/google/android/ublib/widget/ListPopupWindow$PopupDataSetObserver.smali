.class Lcom/google/android/ublib/widget/ListPopupWindow$PopupDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "ListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/widget/ListPopupWindow;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/widget/ListPopupWindow;)V
    .locals 0

    .prologue
    .line 1366
    iput-object p1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupDataSetObserver;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/widget/ListPopupWindow;Lcom/google/android/ublib/widget/ListPopupWindow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/widget/ListPopupWindow;
    .param p2, "x1"    # Lcom/google/android/ublib/widget/ListPopupWindow$1;

    .prologue
    .line 1366
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/ListPopupWindow$PopupDataSetObserver;-><init>(Lcom/google/android/ublib/widget/ListPopupWindow;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 1369
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupDataSetObserver;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1371
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupDataSetObserver;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->show()V

    .line 1373
    :cond_0
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupDataSetObserver;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->dismiss()V

    .line 1378
    return-void
.end method
