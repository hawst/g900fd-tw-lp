.class public Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
.super Ljava/lang/Object;
.source "LegacyPopupMenu.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;,
        Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;
    }
.end annotation


# instance fields
.field private final mAllowCustomView:Z

.field private final mAnchor:Landroid/view/View;

.field private final mBounds:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private final mPopupActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupSelector:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

.field private final mSystemUi:Lcom/google/android/ublib/view/SystemUi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZLcom/google/android/ublib/view/SystemUi;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;
    .param p3, "bounds"    # Landroid/view/View;
    .param p4, "allowCustomView"    # Z
    .param p5, "systemUi"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    .line 180
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mAnchor:Landroid/view/View;

    .line 181
    iput-object p3, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mBounds:Landroid/view/View;

    .line 182
    iput-boolean p4, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mAllowCustomView:Z

    .line 183
    iput-object p5, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    .line 184
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;
    .param p3, "systemUi"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    .line 166
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZLcom/google/android/ublib/view/SystemUi;)V

    .line 167
    return-void
.end method


# virtual methods
.method public addMenuItem(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "customView"    # Landroid/view/View;
    .param p4, "onActionSelectedListener"    # Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    new-instance v1, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;-><init>(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    return-void
.end method

.method public addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "onActionSelectedListener"    # Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    new-instance v1, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2, p3}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;-><init>(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->dismiss()V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .line 211
    :cond_0
    return-void
.end method

.method public show()V
    .locals 7

    .prologue
    .line 200
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mAnchor:Landroid/view/View;

    new-instance v3, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mAllowCustomView:Z

    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupActions:Ljava/util/List;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;-><init>(Landroid/content/Context;ZLjava/util/List;)V

    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mBounds:Landroid/view/View;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;Lcom/google/android/ublib/view/SystemUi;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .line 203
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->mPopupSelector:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->show()V

    .line 204
    return-void
.end method
