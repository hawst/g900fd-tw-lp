.class public interface abstract Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;
.super Ljava/lang/Object;
.source "PlayCardMenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
    }
.end annotation


# virtual methods
.method public abstract addMenuEntries(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract showPopupMenu(Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
.end method
