.class public interface abstract Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
.super Ljava/lang/Object;
.source "PlayCardImageProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;,
        Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    }
.end annotation


# virtual methods
.method public abstract cancelFetch()V
.end method

.method public abstract fetchImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V
.end method
