.class public Lcom/google/android/ublib/cardlib/PlayCardClusterView;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/ublib/cardlib/model/Document;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# instance fields
.field private mContent:Landroid/view/ViewGroup;

.field private mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

.field private final mHeaderContentOverlap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeaderContentOverlap:I

    .line 47
    return-void
.end method


# virtual methods
.method public createContent()V
    .locals 1

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;->createContent()V

    .line 95
    return-void
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 162
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;->getColumnCount()I

    move-result v0

    return v0
.end method

.method public hasCards()Z
    .locals 1

    .prologue
    .line 82
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideHeader()V
    .locals 2

    .prologue
    .line 98
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 99
    return-void
.end method

.method public inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 90
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    invoke-interface {v0, p1, p2}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;->inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V

    .line 91
    return-void
.end method

.method public onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V
    .locals 1
    .param p2, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p3, "cardMetadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            ")V"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    .local p1, "newDocList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;->onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V

    .line 155
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 53
    const v0, 0x7f0e00ee

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContent:Landroid/view/ViewGroup;

    .line 55
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContent:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    .line 56
    const v0, 0x7f0e00da

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    .line 57
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    const/4 v7, 0x0

    .line 135
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getWidth()I

    move-result v3

    .line 136
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getPaddingTop()I

    move-result v2

    .line 137
    .local v2, "paddingTop":I
    move v0, v2

    .line 139
    .local v0, "contentY":I
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 140
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v1

    .line 141
    .local v1, "headerHeight":I
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    add-int v5, v2, v1

    invoke-virtual {v4, v7, v2, v3, v5}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->layout(IIII)V

    .line 144
    iget v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeaderContentOverlap:I

    sub-int v4, v1, v4

    add-int/2addr v0, v4

    .line 146
    .end local v1    # "headerHeight":I
    :cond_0
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v7, v0, v3, v5}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 147
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 116
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 118
    .local v0, "availableWidth":I
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {v3, p1, p2}, Landroid/view/ViewGroup;->measure(II)V

    .line 119
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContent:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getPaddingBottom()I

    move-result v4

    add-int v2, v3, v4

    .line 122
    .local v2, "height":I
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 123
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->measure(II)V

    .line 124
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v1

    .line 127
    .local v1, "headerHeight":I
    iget v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeaderContentOverlap:I

    sub-int v3, v1, v3

    add-int/2addr v2, v3

    .line 130
    .end local v1    # "headerHeight":I
    :cond_0
    invoke-virtual {p0, v0, v2}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setMeasuredDimension(II)V

    .line 131
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "columnCount"    # I

    .prologue
    .line 158
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;->setColumnCount(I)V

    .line 159
    return-void
.end method

.method public setHeaderContentDescription(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentDescription"    # Ljava/lang/String;

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V
    .locals 8
    .param p1, "metadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "imageProviderFactory"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;
    .param p6, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p7, "bindingsProvider"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    .local p2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p4, "clickCallback":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mContentController:Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;->setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 67
    return-void
.end method

.method public setMoreClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 111
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V

    .line 112
    return-void
.end method

.method public showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "titleMain"    # Ljava/lang/String;
    .param p2, "titleSecondary"    # Ljava/lang/String;
    .param p3, "more"    # Ljava/lang/String;

    .prologue
    .line 102
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->mHeader:Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 104
    return-void
.end method
