.class public abstract Lcom/google/android/ublib/actionbar/UBLibActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "UBLibActivity.java"


# instance fields
.field private mActivityDestroyed:Z

.field private mActivityResumed:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ublib/actionbar/UBLibActivity;->mActivityDestroyed:Z

    return-void
.end method


# virtual methods
.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/android/ublib/actionbar/UBLibActivity;->mActivityDestroyed:Z

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/actionbar/UBLibActivity;->mActivityDestroyed:Z

    .line 82
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 83
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/actionbar/UBLibActivity;->mActivityResumed:Z

    .line 55
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPostResume()V

    .line 56
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStop()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ublib/actionbar/UBLibActivity;->mActivityResumed:Z

    .line 62
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x1

    .line 49
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    return-void
.end method
