.class public Lcom/google/android/ublib/widget/VerticalWarpListView;
.super Lcom/google/android/ublib/widget/AbsWarpListView;
.source "VerticalWarpListView.java"


# instance fields
.field private mDownX:F

.field private mDownY:F

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private final mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mScrollBlocked:Z

.field private mScrolling:Z

.field private final mTmpRect:Landroid/graphics/Rect;

.field private final mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field protected final mViewConfig:Landroid/view/ViewConfiguration;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;-><init>(Landroid/content/Context;)V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    .line 24
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    .line 38
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/ublib/widget/VerticalWarpListView$1;

    invoke-direct {v2, p0}, Lcom/google/android/ublib/widget/VerticalWarpListView$1;-><init>(Lcom/google/android/ublib/widget/VerticalWarpListView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    .line 24
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    .line 38
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/ublib/widget/VerticalWarpListView$1;

    invoke-direct {v2, p0}, Lcom/google/android/ublib/widget/VerticalWarpListView$1;-><init>(Lcom/google/android/ublib/widget/VerticalWarpListView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/ublib/widget/AbsWarpListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    .line 24
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    .line 38
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/ublib/widget/VerticalWarpListView$1;

    invoke-direct {v2, p0}, Lcom/google/android/ublib/widget/VerticalWarpListView$1;-><init>(Lcom/google/android/ublib/widget/VerticalWarpListView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/widget/VerticalWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/VerticalWarpListView;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrolling:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/ublib/widget/VerticalWarpListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/VerticalWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrolling:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/ublib/widget/VerticalWarpListView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/VerticalWarpListView;

    .prologue
    .line 19
    iget v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mDownX:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/ublib/widget/VerticalWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/VerticalWarpListView;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrollBlocked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/ublib/widget/VerticalWarpListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/VerticalWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrollBlocked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/ublib/widget/VerticalWarpListView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/VerticalWarpListView;

    .prologue
    .line 19
    iget v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mDownY:F

    return v0
.end method

.method private getMeasureSpec(II)I
    .locals 2
    .param p1, "size"    # I
    .param p2, "parent"    # I

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 163
    packed-switch p1, :pswitch_data_0

    .line 169
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_0
    return v0

    .line 165
    :pswitch_0
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    .line 167
    :pswitch_1
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 188
    invoke-super {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 191
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 197
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 198
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 199
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 203
    :cond_1
    return-void
.end method

.method protected getLeadingEdge()Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1

    .prologue
    .line 133
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method protected getMaxOffset()I
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getHeight()I

    move-result v0

    return v0
.end method

.method protected getTrailingEdge()Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1

    .prologue
    .line 139
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method public getViewPosition(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {p1, v0}, Lcom/google/android/ublib/view/ViewCompat;->getHitRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public getViewSize(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {p1, v0}, Lcom/google/android/ublib/view/ViewCompat;->getHitRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method protected layoutView(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 176
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getWidth()I

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getMeasureSpec(II)I

    move-result v4

    iget v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getHeight()I

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getMeasureSpec(II)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/view/View;->measure(II)V

    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .local v3, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 181
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/VerticalWarpListView;->getWidth()I

    move-result v4

    sub-int/2addr v4, v3

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mViewAlignment:F

    mul-float/2addr v4, v5

    float-to-int v1, v4

    .line 183
    .local v1, "left":I
    const/16 v4, -0x2000

    add-int v5, v1, v3

    add-int/lit16 v6, v0, -0x2000

    invoke-virtual {p1, v1, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 184
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/VerticalWarpListView;->handleTouchEvent(Landroid/view/MotionEvent;)V

    .line 146
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 147
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mDownX:F

    .line 148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mDownY:F

    .line 149
    iput-boolean v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrollBlocked:Z

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrolling:Z

    .line 151
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mScrollBlocked:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 157
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/VerticalWarpListView;->handleTouchEvent(Landroid/view/MotionEvent;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method protected positionView(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "off"    # I

    .prologue
    .line 107
    iget-object v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {p1, v1}, Lcom/google/android/ublib/view/ViewCompat;->getHitRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 108
    iget-object v1, p0, Lcom/google/android/ublib/widget/VerticalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v0, p2, v1

    .line 109
    .local v0, "change":I
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    int-to-float v2, v0

    add-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 112
    :cond_0
    return-void
.end method
