.class public Lcom/google/android/ublib/util/ImageConstraints;
.super Ljava/lang/Object;
.source "ImageConstraints.java"


# instance fields
.field public height:Ljava/lang/Integer;

.field public width:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "width"    # Ljava/lang/Integer;
    .param p2, "height"    # Ljava/lang/Integer;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    .line 22
    iput-object p2, p0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    .line 23
    return-void
.end method

.method public static fixedWidth(Ljava/lang/Integer;)Lcom/google/android/ublib/util/ImageConstraints;
    .locals 2
    .param p0, "width"    # Ljava/lang/Integer;

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/ublib/util/ImageConstraints;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/ublib/util/ImageConstraints;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 51
    instance-of v2, p1, Lcom/google/android/ublib/util/ImageConstraints;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 52
    check-cast v0, Lcom/google/android/ublib/util/ImageConstraints;

    .line 53
    .local v0, "other":Lcom/google/android/ublib/util/ImageConstraints;
    iget-object v2, p0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    iget-object v3, v0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    iget-object v3, v0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 55
    .end local v0    # "other":Lcom/google/android/ublib/util/ImageConstraints;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public satisfiesConstraints(II)Z
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, p1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "width"

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "height"

    iget-object v2, p0, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
