.class public Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterViewContent.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/ublib/cardlib/model/Document;",
        ">",
        "Landroid/view/ViewGroup;",
        "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mBindingsProvider:Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;

.field private mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private mColumnCount:I

.field private final mDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

.field private mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

.field private mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    .line 55
    return-void
.end method


# virtual methods
.method protected bindCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/model/Document;)V
    .locals 6
    .param p1, "playCard"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    .local p2, "book":Lcom/google/android/ublib/cardlib/model/Document;, "TT;"
    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;->createImageProvider()Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 92
    return-void
.end method

.method public createContent()V
    .locals 4

    .prologue
    .line 117
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    const/4 v2, 0x0

    .local v2, "tileIndex":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 118
    invoke-virtual {p0, v2}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 119
    .local v0, "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/ublib/cardlib/model/Document;

    move-object v1, v3

    .line 121
    .local v1, "doc":Lcom/google/android/ublib/cardlib/model/Document;, "TT;"
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/model/Document;Z)V

    .line 117
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "doc":Lcom/google/android/ublib/cardlib/model/Document;, "TT;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 123
    .end local v0    # "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    :cond_1
    return-void
.end method

.method protected getCardClickCallback()Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    return-object v0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 211
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    iget v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mColumnCount:I

    return v0
.end method

.method protected getDocs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    return-object v0
.end method

.method public getMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 82
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    return-object v0
.end method

.method public inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 104
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 105
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .local v2, "tileIndex":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 106
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v4, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v3

    .line 107
    .local v3, "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {p1, v4, v1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;->getCard(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    move-result-object v0

    .line 109
    .local v0, "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 110
    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getFillStyle()Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 111
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->addView(Landroid/view/View;)V

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    .end local v0    # "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v3    # "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_0
    return-void
.end method

.method public onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V
    .locals 0
    .param p2, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p3, "cardMetaData"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            ")V"
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    .local p1, "newDocList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 177
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getWidth()I

    move-result v1

    .line 178
    .local v1, "availableWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getWidth()I

    move-result v9

    .line 179
    .local v9, "columns":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v15

    sub-int v15, v1, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v16

    sub-int v15, v15, v16

    div-int v8, v15, v9

    .line 180
    .local v8, "cellSize":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getPaddingTop()I

    move-result v11

    .line 181
    .local v11, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v10

    .line 183
    .local v10, "paddingLeft":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v12

    .line 184
    .local v12, "tileCount":I
    const/4 v13, 0x0

    .local v13, "tileIndex":I
    :goto_0
    if-ge v13, v12, :cond_1

    .line 185
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15, v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v14

    .line 186
    .local v14, "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v14}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getXStart()I

    move-result v6

    .line 187
    .local v6, "cardXStart":I
    invoke-virtual {v14}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getYStart()I

    move-result v7

    .line 188
    .local v7, "cardYStart":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 190
    .local v2, "card":Landroid/view/View;
    if-nez v2, :cond_0

    .line 191
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v15, v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->removeTileMetadata(I)V

    .line 184
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 195
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 196
    .local v3, "cardHeight":I
    mul-int v15, v8, v6

    add-int v4, v10, v15

    .line 197
    .local v4, "cardLeft":I
    mul-int v15, v3, v7

    add-int v5, v11, v15

    .line 199
    .local v5, "cardTop":I
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    add-int/2addr v15, v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    add-int v16, v16, v5

    move/from16 v0, v16

    invoke-virtual {v2, v4, v5, v15, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 202
    .end local v2    # "card":Landroid/view/View;
    .end local v3    # "cardHeight":I
    .end local v4    # "cardLeft":I
    .end local v5    # "cardTop":I
    .end local v6    # "cardXStart":I
    .end local v7    # "cardYStart":I
    .end local v14    # "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 147
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 151
    .local v0, "availableWidth":I
    iget-object v13, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getWidth()I

    move-result v6

    .line 152
    .local v6, "columns":I
    iget-object v13, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getHeight()I

    move-result v8

    .line 153
    .local v8, "rows":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v13

    sub-int v13, v0, v13

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v14

    sub-int/2addr v13, v14

    div-int v5, v13, v6

    .line 154
    .local v5, "cellSize":I
    const/4 v9, 0x0

    .line 156
    .local v9, "targetHeight":I
    iget-object v13, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileCount()I

    move-result v10

    .line 157
    .local v10, "tileCount":I
    const/4 v11, 0x0

    .local v11, "tileIndex":I
    :goto_0
    if-ge v11, v10, :cond_1

    .line 158
    iget-object v13, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v13, v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v12

    .line 159
    .local v12, "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v12}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v2

    .line 160
    .local v2, "cardHSpan":I
    invoke-virtual {v12}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v3

    .line 161
    .local v3, "cardVSpan":I
    invoke-virtual {p0, v11}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 162
    .local v1, "card":Landroid/view/View;
    if-nez v1, :cond_0

    .line 157
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 165
    :cond_0
    mul-int v4, v5, v2

    .line 166
    .local v4, "cardWidth":I
    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    const/4 v14, 0x0

    invoke-virtual {v1, v13, v14}, Landroid/view/View;->measure(II)V

    .line 168
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 169
    .local v7, "measuredHeight":I
    div-int v13, v7, v3

    invoke-static {v13, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    goto :goto_1

    .line 172
    .end local v1    # "card":Landroid/view/View;
    .end local v2    # "cardHSpan":I
    .end local v3    # "cardVSpan":I
    .end local v4    # "cardWidth":I
    .end local v7    # "measuredHeight":I
    .end local v12    # "tileMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_1
    mul-int v13, v9, v8

    invoke-virtual {p0, v0, v13}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->setMeasuredDimension(II)V

    .line 173
    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 206
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    iput p1, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mColumnCount:I

    .line 207
    return-void
.end method

.method protected setDocuments(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    .local p1, "clusterDocs":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 75
    if-eqz p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mDocs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 78
    :cond_0
    return-void
.end method

.method public setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V
    .locals 0
    .param p1, "metadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .param p3, "cardsContextMenuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "imageProviderFactory"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;
    .param p6, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p7, "binder"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    .local p2, "clusterDocs":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p4, "clickCallback":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback<TT;>;"
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    .line 65
    invoke-virtual {p0, p2}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->setDocuments(Ljava/util/List;)V

    .line 66
    iput-object p3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 67
    iput-object p4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    .line 68
    iput-object p5, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

    .line 69
    iput-object p6, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    .line 70
    iput-object p7, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mBindingsProvider:Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;

    .line 71
    return-void
.end method

.method protected setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/model/Document;Z)V
    .locals 6
    .param p1, "playCard"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p3, "addView"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView;",
            "TT;Z)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p0, "this":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<TT;>;"
    .local p2, "doc":Lcom/google/android/ublib/cardlib/model/Document;, "TT;"
    if-nez p2, :cond_1

    .line 131
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bindNoDocument()V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mCardsContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mImageProviderFactory:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;->createImageProvider()Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    iget-object v5, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mBindingsProvider:Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 136
    new-instance v0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->mCardClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    invoke-direct {v0, v1, p2, v2}, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;-><init>(Landroid/content/Context;Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;)V

    invoke-virtual {p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    if-eqz p3, :cond_0

    .line 139
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
