.class public Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;,
        Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    }
.end annotation


# static fields
.field public static final CARD_LARGE:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_LARGEMINUS_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_LARGE_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUM:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUMPLUS:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUMPLUS_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUM_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_ROW:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_2x1:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_2x1_WRAPPED:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_WRAPPED:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field private final mHeight:I

.field private final mTiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const v6, 0x3fb872b0    # 1.441f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v3, 0x4

    .line 99
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040043

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1_WRAPPED:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 102
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040043

    invoke-direct {v0, v1, v4, v4, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_WRAPPED:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 105
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040043

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v7, v4, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_2x1:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 108
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040043

    const/4 v2, 0x3

    invoke-direct {v0, v1, v7, v2, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 111
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040040

    invoke-direct {v0, v1, v3, v7, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 114
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040041

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 117
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04003e

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 120
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040043

    invoke-direct {v0, v1, v7, v3, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 123
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040040

    invoke-direct {v0, v1, v3, v7, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 126
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040041

    invoke-direct {v0, v1, v3, v3, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 129
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04003f

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 132
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04003e

    const/16 v2, 0x8

    invoke-direct {v0, v1, v3, v2, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_LARGE_16x9:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 135
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040042

    invoke-direct {v0, v1, v4, v4, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->CARD_ROW:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    .line 163
    iput p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mWidth:I

    .line 164
    iput p2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mHeight:I

    .line 165
    return-void
.end method


# virtual methods
.method public addTile(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .locals 2
    .param p1, "cardMetadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p2, "xStart"    # I
    .param p3, "yStart"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    new-instance v1, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;-><init>(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    return-object p0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mHeight:I

    return v0
.end method

.method public getTileCount()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTileMetadata(I)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
    .locals 1
    .param p1, "tileIndex"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mWidth:I

    return v0
.end method

.method public removeTileMetadata(I)V
    .locals 1
    .param p1, "tileIndex"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 190
    return-void
.end method
