.class Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
.super Ljava/lang/Object;
.source "AbsWarpListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/AbsWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewInfo"
.end annotation


# instance fields
.field mPosition:I

.field final mView:Landroid/view/View;

.field mViewId:J

.field mVisible:Z

.field final synthetic this$0:Lcom/google/android/ublib/widget/AbsWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView;Landroid/view/View;I)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mVisible:Z

    .line 225
    iput-object p2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    .line 226
    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    invoke-static {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$000(Lcom/google/android/ublib/widget/AbsWarpListView;)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mViewId:J

    .line 227
    iput p3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    .line 228
    return-void
.end method


# virtual methods
.method canSpare()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 251
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$200(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    if-lt v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$300(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    if-gt v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$400(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    if-lt v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$500(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    if-le v2, v3, :cond_1

    .line 273
    :cond_0
    :goto_0
    return v0

    .line 256
    :cond_1
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$000(Lcom/google/android/ublib/widget/AbsWarpListView;)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    invoke-interface {v2, v3, v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->isViewPinned(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 260
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mConserveViews:Z
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$600(Lcom/google/android/ublib/widget/AbsWarpListView;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 261
    goto :goto_0

    .line 264
    :cond_2
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->isViewVisible(Landroid/view/View;)Z
    invoke-static {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$100(Lcom/google/android/ublib/widget/AbsWarpListView;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 268
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$700(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$800(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    if-lt v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$900(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    if-le v2, v3, :cond_0

    :cond_3
    move v0, v1

    .line 273
    goto :goto_0
.end method

.method setVisible(Z)V
    .locals 4
    .param p1, "isVisible"    # Z

    .prologue
    .line 231
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->isViewVisible(Landroid/view/View;)Z
    invoke-static {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$100(Lcom/google/android/ublib/widget/AbsWarpListView;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->positionView(Landroid/view/View;I)V

    .line 235
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mVisible:Z

    if-eq p1, v0, :cond_1

    .line 236
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mVisible:Z

    .line 237
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    invoke-static {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$000(Lcom/google/android/ublib/widget/AbsWarpListView;)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mPosition:I

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->setViewVisible(Landroid/view/View;IZ)V

    .line 239
    :cond_1
    return-void
.end method
