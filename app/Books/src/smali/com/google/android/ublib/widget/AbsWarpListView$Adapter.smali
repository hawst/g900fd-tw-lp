.class public interface abstract Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
.super Ljava/lang/Object;
.source "AbsWarpListView.java"

# interfaces
.implements Landroid/widget/Adapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/AbsWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Adapter"
.end annotation


# virtual methods
.method public abstract getNegativeCount()I
.end method

.method public abstract getViewSize(I)I
.end method

.method public abstract isViewPinned(Landroid/view/View;I)Z
.end method

.method public abstract preload(I)V
.end method

.method public abstract setViewVisible(Landroid/view/View;IZ)V
.end method

.method public abstract transformView(Landroid/view/View;I)V
.end method
