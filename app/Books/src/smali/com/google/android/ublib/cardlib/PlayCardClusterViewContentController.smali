.class public interface abstract Lcom/google/android/ublib/cardlib/PlayCardClusterViewContentController;
.super Ljava/lang/Object;
.source "PlayCardClusterViewContentController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/ublib/cardlib/model/Document;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract createContent()V
.end method

.method public abstract getColumnCount()I
.end method

.method public abstract inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V
.end method

.method public abstract onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            ")V"
        }
    .end annotation
.end method

.method public abstract setColumnCount(I)V
.end method

.method public abstract setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;",
            "Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;",
            ")V"
        }
    .end annotation
.end method
