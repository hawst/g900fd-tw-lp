.class Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;
.super Ljava/lang/Object;
.source "PlayCardArtImageView.java"

# interfaces
.implements Landroid/support/v7/graphics/Palette$PaletteAsyncListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->maybeSetBackground(Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

.field final synthetic val$imageUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;->this$0:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    iput-object p2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;->val$imageUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenerated(Landroid/support/v7/graphics/Palette;)V
    .locals 4
    .param p1, "palette"    # Landroid/support/v7/graphics/Palette;

    .prologue
    .line 504
    invoke-static {p1}, Lcom/google/android/ublib/utils/PaletteUtils;->getRepresentativeColor(Landroid/support/v7/graphics/Palette;)I

    move-result v0

    .line 505
    .local v0, "pixelValue":I
    # getter for: Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->sCoverUriToBackgroundColor:Landroid/support/v4/util/LruCache;
    invoke-static {}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->access$200()Landroid/support/v4/util/LruCache;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;->val$imageUri:Landroid/net/Uri;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;->val$imageUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;->this$0:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    # getter for: Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->access$300(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;->this$0:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setBackgroundColor(I)V

    .line 509
    :cond_0
    return-void
.end method
