.class Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;
.super Landroid/widget/ListView;
.source "ListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DropDownListView"
.end annotation


# instance fields
.field private final mHijackFocus:Z

.field private mListSelectionHidden:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hijackFocus"    # Z

    .prologue
    .line 1196
    const/4 v0, 0x0

    const v1, 0x7f01007a

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1197
    iput-boolean p2, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    .line 1199
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->setCacheColorHint(I)V

    .line 1200
    return-void
.end method

.method static synthetic access$502(Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;
    .param p1, "x1"    # Z

    .prologue
    .line 1153
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mListSelectionHidden:Z

    return p1
.end method

.method private measureScrapChild(Landroid/view/View;II)V
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "widthMeasureSpec"    # I

    .prologue
    const/4 v6, 0x0

    .line 1343
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 1344
    .local v3, "p":Landroid/view/ViewGroup$LayoutParams;
    if-nez v3, :cond_0

    .line 1345
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 1346
    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1353
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getListPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getListPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p3, v4, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 1355
    .local v1, "childWidthSpec":I
    iget v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1357
    .local v2, "lpHeight":I
    if-lez v2, :cond_1

    .line 1358
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1362
    .local v0, "childHeightSpec":I
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 1363
    return-void

    .line 1360
    .end local v0    # "childHeightSpec":I
    :cond_1
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .restart local v0    # "childHeightSpec":I
    goto :goto_0
.end method


# virtual methods
.method public hasFocus()Z
    .locals 1

    .prologue
    .line 1253
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWindowFocus()Z
    .locals 1

    .prologue
    .line 1233
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 1

    .prologue
    .line 1243
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInTouchMode()Z
    .locals 1

    .prologue
    .line 1223
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mHijackFocus:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->mListSelectionHidden:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final measureHeightOfChildrenCopy(IIIII)I
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxHeight"    # I
    .param p5, "disallowPartialChildPosition"    # I

    .prologue
    .line 1286
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 1287
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-nez v0, :cond_1

    .line 1288
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getListPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getListPaddingBottom()I

    move-result v7

    add-int v4, v6, v7

    .line 1337
    :cond_0
    :goto_0
    return v4

    .line 1292
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getListPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getListPaddingBottom()I

    move-result v7

    add-int v5, v6, v7

    .line 1293
    .local v5, "returnedHeight":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getDividerHeight()I

    move-result v6

    if-lez v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getDividerHeight()I

    move-result v2

    .line 1297
    .local v2, "dividerHeight":I
    :goto_1
    const/4 v4, 0x0

    .line 1302
    .local v4, "prevHeightWithoutPartialChild":I
    const/4 v6, -0x1

    if-ne p3, v6, :cond_2

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    add-int/lit8 p3, v6, -0x1

    .line 1307
    :cond_2
    move v3, p2

    .local v3, "i":I
    :goto_2
    if-gt v3, p3, :cond_8

    .line 1308
    const/4 v6, 0x0

    invoke-interface {v0, v3, v6, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1310
    .local v1, "child":Landroid/view/View;
    invoke-direct {p0, v1, v3, p1}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->measureScrapChild(Landroid/view/View;II)V

    .line 1312
    if-lez v3, :cond_3

    .line 1314
    add-int/2addr v5, v2

    .line 1317
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 1319
    if-lt v5, p4, :cond_6

    .line 1322
    if-ltz p5, :cond_4

    if-le v3, p5, :cond_4

    if-lez v4, :cond_4

    if-ne v5, p4, :cond_0

    :cond_4
    move v4, p4

    goto :goto_0

    .line 1293
    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "dividerHeight":I
    .end local v3    # "i":I
    .end local v4    # "prevHeightWithoutPartialChild":I
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 1330
    .restart local v1    # "child":Landroid/view/View;
    .restart local v2    # "dividerHeight":I
    .restart local v3    # "i":I
    .restart local v4    # "prevHeightWithoutPartialChild":I
    :cond_6
    if-ltz p5, :cond_7

    if-lt v3, p5, :cond_7

    .line 1331
    move v4, v5

    .line 1307
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v1    # "child":Landroid/view/View;
    :cond_8
    move v4, v5

    .line 1337
    goto :goto_0
.end method
