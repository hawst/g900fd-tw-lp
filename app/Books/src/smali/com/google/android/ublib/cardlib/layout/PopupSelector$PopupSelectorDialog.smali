.class Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;
.super Landroid/app/Dialog;
.source "PopupSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/PopupSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupSelectorDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/cardlib/layout/PopupSelector;Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/View;)V
    .locals 15
    .param p2, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p3, "anchorView"    # Landroid/view/View;
    .param p4, "boundsView"    # Landroid/view/View;

    .prologue
    .line 46
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->this$0:Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .line 47
    # getter for: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$000(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)Landroid/content/Context;

    move-result-object v11

    const v12, 0x7f0a0030

    invoke-direct {p0, v11, v12}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 48
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->getViewRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 49
    .local v1, "anchorRect":Landroid/graphics/Rect;
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->getViewRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    .line 51
    .local v2, "boundsRect":Landroid/graphics/Rect;
    const v11, 0x7f0400a1

    invoke-virtual {p0, v11}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->setContentView(I)V

    .line 53
    # invokes: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->calculateScreenWidth()I
    invoke-static/range {p1 .. p1}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$100(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)I

    move-result v9

    .line 55
    .local v9, "screenWidth":I
    const v11, 0x7f0e0194

    invoke-virtual {p0, v11}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 57
    .local v6, "mainView":Landroid/view/View;
    # invokes: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->getBackgroundResId()I
    invoke-static/range {p1 .. p1}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$200(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)I

    move-result v11

    invoke-virtual {v6, v11}, Landroid/view/View;->setBackgroundResource(I)V

    .line 59
    move-object/from16 v0, p1

    # invokes: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->setupList(Landroid/view/View;)Landroid/widget/ListView;
    invoke-static {v0, v6}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$300(Lcom/google/android/ublib/cardlib/layout/PopupSelector;Landroid/view/View;)Landroid/widget/ListView;

    move-result-object v4

    .line 61
    .local v4, "list":Landroid/widget/ListView;
    move-object/from16 v0, p1

    # invokes: Lcom/google/android/ublib/cardlib/layout/PopupSelector;->getMaxContentWidth(Landroid/widget/ListView;I)I
    invoke-static {v0, v4, v9}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->access$400(Lcom/google/android/ublib/cardlib/layout/PopupSelector;Landroid/widget/ListView;I)I

    move-result v3

    .line 63
    .local v3, "contentWidth":I
    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    const/4 v12, -0x2

    invoke-virtual {v6, v11, v12}, Landroid/view/View;->measure(II)V

    .line 65
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    invoke-virtual {v6, v11, v12, v13, v14}, Landroid/view/View;->layout(IIII)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->getWindow()Landroid/view/Window;

    move-result-object v10

    .line 68
    .local v10, "window":Landroid/view/Window;
    invoke-virtual {v10}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 69
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    iput v11, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 70
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    iput v11, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 71
    iget v11, v1, Landroid/graphics/Rect;->left:I

    iput v11, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 72
    iget v11, v1, Landroid/graphics/Rect;->bottom:I

    iput v11, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 74
    if-eqz v2, :cond_1

    .line 76
    iget v11, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v12, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v11, v12

    iget v12, v2, Landroid/graphics/Rect;->bottom:I

    if-le v11, v12, :cond_0

    .line 77
    iget v11, v1, Landroid/graphics/Rect;->top:I

    iget v12, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int v8, v11, v12

    .line 78
    .local v8, "proposedY":I
    iget v11, v2, Landroid/graphics/Rect;->top:I

    if-lt v8, v11, :cond_0

    .line 79
    iput v8, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 80
    const v11, 0x7f0a0031

    invoke-virtual {v10, v11}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 83
    .end local v8    # "proposedY":I
    :cond_0
    iget v11, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v12, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v11, v12

    iget v12, v2, Landroid/graphics/Rect;->right:I

    if-le v11, v12, :cond_1

    .line 84
    iget v11, v1, Landroid/graphics/Rect;->right:I

    iget v12, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v11, v12

    .line 85
    .local v7, "proposedX":I
    iget v11, v2, Landroid/graphics/Rect;->left:I

    if-lt v7, v11, :cond_1

    .line 86
    iput v7, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 92
    .end local v7    # "proposedX":I
    :cond_1
    const/16 v11, 0x33

    iput v11, v5, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 94
    iget v11, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v12, 0x20100

    or-int/2addr v11, v12

    iput v11, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 96
    invoke-virtual {v10, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 97
    return-void
.end method

.method private getViewRect(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 100
    if-nez p1, :cond_0

    .line 101
    const/4 v1, 0x0

    .line 105
    :goto_0
    return-object v1

    .line 103
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 104
    .local v0, "location":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 105
    new-instance v1, Landroid/graphics/Rect;

    aget v2, v0, v4

    aget v3, v0, v6

    aget v4, v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    aget v5, v0, v6

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 121
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 128
    .end local v0    # "handled":Z
    :goto_0
    return v0

    .line 124
    .restart local v0    # "handled":Z
    :cond_0
    const/16 v1, 0x13

    if-ne p1, v1, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->cancel()V

    .line 126
    const/4 v0, 0x1

    goto :goto_0

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 111
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;->cancel()V

    .line 113
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
