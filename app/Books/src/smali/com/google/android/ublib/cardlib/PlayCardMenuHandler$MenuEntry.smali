.class public abstract Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
.super Ljava/lang/Object;
.source "PlayCardMenuHandler.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MenuEntry"
.end annotation


# instance fields
.field public final isEnabled:Z

.field public final menuId:I

.field public final menuTitle:Ljava/lang/String;

.field public final runAsync:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "async"    # Z

    .prologue
    .line 63
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;ZZ)V

    .line 64
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZZ)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "async"    # Z
    .param p4, "enabled"    # Z

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;->menuId:I

    .line 56
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;->menuTitle:Ljava/lang/String;

    .line 57
    iput-boolean p3, p0, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;->runAsync:Z

    .line 58
    iput-boolean p4, p0, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;->isEnabled:Z

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;IIZ)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "id"    # I
    .param p3, "titleId"    # I
    .param p4, "async"    # Z

    .prologue
    .line 68
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;Z)V

    .line 69
    return-void
.end method
