.class final Lcom/google/android/ublib/utils/Runnables$1;
.super Ljava/lang/Object;
.source "Runnables.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/utils/Runnables;->createWeakReferenceTask(Ljava/lang/ref/Reference;Lcom/google/android/ublib/utils/Consumer;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$reference:Ljava/lang/ref/Reference;

.field final synthetic val$task:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Ljava/lang/ref/Reference;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/ublib/utils/Runnables$1;->val$reference:Ljava/lang/ref/Reference;

    iput-object p2, p0, Lcom/google/android/ublib/utils/Runnables$1;->val$task:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 23
    iget-object v1, p0, Lcom/google/android/ublib/utils/Runnables$1;->val$reference:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 24
    .local v0, "object":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    .line 25
    iget-object v1, p0, Lcom/google/android/ublib/utils/Runnables$1;->val$task:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 27
    :cond_0
    return-void
.end method
