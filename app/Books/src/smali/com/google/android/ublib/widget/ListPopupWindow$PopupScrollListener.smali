.class Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupScrollListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/widget/ListPopupWindow;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/widget/ListPopupWindow;)V
    .locals 0

    .prologue
    .line 1414
    iput-object p1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/widget/ListPopupWindow;Lcom/google/android/ublib/widget/ListPopupWindow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/widget/ListPopupWindow;
    .param p2, "x1"    # Lcom/google/android/ublib/widget/ListPopupWindow$1;

    .prologue
    .line 1414
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;-><init>(Lcom/google/android/ublib/widget/ListPopupWindow;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 1418
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 1421
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->isInputMethodNotNeeded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$700(Lcom/google/android/ublib/widget/ListPopupWindow;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1423
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$900(Lcom/google/android/ublib/widget/ListPopupWindow;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mResizePopupRunnable:Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;
    invoke-static {v1}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$800(Lcom/google/android/ublib/widget/ListPopupWindow;)Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1424
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$PopupScrollListener;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mResizePopupRunnable:Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$800(Lcom/google/android/ublib/widget/ListPopupWindow;)Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->run()V

    .line 1426
    :cond_0
    return-void
.end method
