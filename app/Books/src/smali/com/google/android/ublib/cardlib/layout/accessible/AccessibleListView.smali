.class public Lcom/google/android/ublib/cardlib/layout/accessible/AccessibleListView;
.super Landroid/widget/ListView;
.source "AccessibleListView.java"


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    .line 41
    .local v1, "populated":Z
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/accessible/AccessibleListView;->getSelectedView()Landroid/view/View;

    move-result-object v3

    .line 42
    .local v3, "selectedView":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 44
    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "focusOwner":Landroid/view/View;
    if-nez v0, :cond_0

    move v2, v1

    .line 55
    .end local v0    # "focusOwner":Landroid/view/View;
    .end local v1    # "populated":Z
    .local v2, "populated":Z
    :goto_0
    return v2

    .line 50
    .end local v2    # "populated":Z
    .restart local v0    # "focusOwner":Landroid/view/View;
    .restart local v1    # "populated":Z
    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 52
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    .end local v0    # "focusOwner":Landroid/view/View;
    :cond_1
    move v2, v1

    .line 55
    .end local v1    # "populated":Z
    .restart local v2    # "populated":Z
    goto :goto_0
.end method
