.class public final enum Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;
.super Ljava/lang/Enum;
.source "AnimatorCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/view/AnimatorCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Interpolation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

.field public static final enum DEFAULT:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

.field public static final enum LINEAR:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->DEFAULT:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    .line 27
    new-instance v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    const-string v1, "LINEAR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->LINEAR:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    sget-object v1, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->DEFAULT:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->LINEAR:Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->$VALUES:[Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    return-object v0
.end method

.method public static values()[Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->$VALUES:[Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    invoke-virtual {v0}, [Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/ublib/view/AnimatorCompat$Interpolation;

    return-object v0
.end method
