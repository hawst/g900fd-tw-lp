.class public Lcom/google/android/ublib/view/HoneycombSystemUi;
.super Ljava/lang/Object;
.source "HoneycombSystemUi.java"

# interfaces
.implements Lcom/google/android/ublib/view/SystemUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/view/HoneycombSystemUi$1;,
        Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

.field private final mListenerWrapper:Landroid/view/View$OnSystemUiVisibilityChangeListener;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;-><init>(Lcom/google/android/ublib/view/HoneycombSystemUi;Lcom/google/android/ublib/view/HoneycombSystemUi$1;)V

    iput-object v0, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mListenerWrapper:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    .line 20
    iput-object p1, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mView:Landroid/view/View;

    .line 21
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/ublib/view/HoneycombSystemUi;)Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/view/HoneycombSystemUi;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    return-object v0
.end method


# virtual methods
.method public copyFlagsTo(Lcom/google/android/ublib/view/SystemUi;)V
    .locals 2
    .param p1, "flagReceiver"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    .line 46
    check-cast p1, Lcom/google/android/ublib/view/HoneycombSystemUi;

    .end local p1    # "flagReceiver":Lcom/google/android/ublib/view/SystemUi;
    iget-object v0, p1, Lcom/google/android/ublib/view/HoneycombSystemUi;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 48
    return-void
.end method

.method public setOnSystemUiVisibilityChangeListener(Lcom/google/android/ublib/view/SystemUi$VisibilityListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    .line 29
    if-eqz p1, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mListenerWrapper:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    .line 34
    .local v0, "listenerWrapper":Landroid/view/View$OnSystemUiVisibilityChangeListener;
    :goto_0
    iget-object v1, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 35
    return-void

    .line 32
    .end local v0    # "listenerWrapper":Landroid/view/View$OnSystemUiVisibilityChangeListener;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "listenerWrapper":Landroid/view/View$OnSystemUiVisibilityChangeListener;
    goto :goto_0
.end method

.method public setSystemUiVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 40
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 41
    .local v0, "visibilityType":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/ublib/view/HoneycombSystemUi;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 42
    return-void

    .line 40
    .end local v0    # "visibilityType":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setViewFullscreen(Z)V
    .locals 0
    .param p1, "fullScreen"    # Z

    .prologue
    .line 64
    return-void
.end method
