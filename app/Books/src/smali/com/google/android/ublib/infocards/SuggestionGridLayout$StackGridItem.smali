.class Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/infocards/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StackGridItem"
.end annotation


# instance fields
.field public bottom:I

.field public left:I

.field mChildHeight:I

.field mCollapsedCardsBottom:I

.field mCollapsedCardsTop:I

.field final mDensity:F

.field mDismissTrail:Landroid/view/View;

.field mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

.field mExpanded:Z

.field mExpandedHeader:Landroid/view/View;

.field final mExpandedOverlapAmount:I

.field mLp:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

.field mMeasuredHeight:I

.field mMeasuredWidth:I

.field mShowingDismissTrail:Z

.field mTag:Ljava/lang/Object;

.field mTmpClipRect:Landroid/graphics/Rect;

.field mTopItemBeingDragged:Z

.field mTotalCollapsedSpacing:I

.field mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public right:I

.field final synthetic this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

.field public top:I


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/content/Context;Ljava/util/List;ILcom/google/android/ublib/infocards/DismissTrailFactory;Landroid/view/View;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "column"    # I
    .param p5, "dismissTrailFactory"    # Lcom/google/android/ublib/infocards/DismissTrailFactory;
    .param p6, "expandedHeader"    # Landroid/view/View;
    .param p7, "tag"    # Ljava/lang/Object;
    .param p8, "viewClickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;I",
            "Lcom/google/android/ublib/infocards/DismissTrailFactory;",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1142
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1098
    iput-boolean v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mShowingDismissTrail:Z

    .line 1102
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    .line 1111
    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    .line 1118
    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mCollapsedCardsTop:I

    .line 1119
    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mCollapsedCardsBottom:I

    .line 1135
    iput-boolean v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    .line 1136
    iput-boolean v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTopItemBeingDragged:Z

    .line 1143
    iput-object p5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    .line 1144
    iput-object p3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    .line 1145
    iput-object p7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTag:Ljava/lang/Object;

    .line 1146
    new-instance v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v5, v4, p4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;-><init>(III)V

    iput-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mLp:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 1148
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDensity:F

    .line 1154
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, p8

    .line 1156
    .local v1, "listener":Landroid/view/View$OnClickListener;
    :goto_0
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1157
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1154
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Landroid/view/View$OnClickListener;
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    new-instance v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;

    const/4 v3, 0x0

    invoke-direct {v1, p0, p8, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;Landroid/view/View$OnClickListener;Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;)V

    goto :goto_0

    .line 1160
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "listener":Landroid/view/View$OnClickListener;
    :cond_1
    invoke-virtual {p0, p6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setHeader(Landroid/view/View;)V

    .line 1161
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->updateTotalCollapsedSpacing()V

    .line 1162
    iget v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDensity:F

    const/high16 v4, 0x42be0000    # 95.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->updateItemBackgrounds()V

    .line 1165
    invoke-virtual {p0, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    .line 1166
    return-void
.end method

.method private getTallestChildHeight(II)I
    .locals 4
    .param p1, "childWidthSpec"    # I
    .param p2, "childHeightSpec"    # I

    .prologue
    .line 1437
    const/4 v2, 0x0

    .line 1438
    .local v2, "maxHeight":I
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1439
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 1440
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1441
    goto :goto_0

    .line 1442
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return v2
.end method


# virtual methods
.method public bringToFrontAndCollapse(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1268
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isTopCard(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1269
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v0, :cond_0

    .line 1270
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    .line 1300
    :cond_0
    :goto_0
    return-void

    .line 1275
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 1278
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1279
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1282
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->invalidate()V

    .line 1286
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    new-instance v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem$1;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;Landroid/view/View;)V

    const-wide/16 v2, 0x46

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1303
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 1316
    :cond_0
    return-void

    .line 1304
    :cond_1
    const/4 v5, 0x0

    .line 1305
    .local v5, "lastOffset":I
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1307
    .local v0, "baseChild":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    .line 1308
    .local v2, "childCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 1309
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1310
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int v6, v7, v8

    .line 1311
    .local v6, "verticalOffset":I
    sub-int v3, v6, v5

    .line 1312
    .local v3, "delta":I
    const/4 v7, 0x0

    int-to-float v8, v3

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1313
    invoke-virtual {v1, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1314
    move v5, v6

    .line 1308
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public enableAnimation(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 1328
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 1329
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 1330
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1331
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 1332
    .local v4, "vglp":Landroid/view/ViewGroup$LayoutParams;
    instance-of v5, v4, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    if-eqz v5, :cond_0

    move-object v3, v4

    .line 1333
    check-cast v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 1334
    .local v3, "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    if-eqz p1, :cond_1

    sget-object v5, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_1
    iput-object v5, v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 1329
    .end local v3    # "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1334
    .restart local v3    # "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_1
    sget-object v5, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_1

    .line 1337
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .end local v4    # "vglp":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    return-void
.end method

.method getChildClipRect(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 8
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 1190
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1191
    .local v3, "left":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v5

    .line 1192
    .local v5, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    .line 1193
    .local v4, "right":I
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1195
    .local v0, "bottom":I
    iget-object v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1196
    .local v2, "index":I
    iget-object v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_0

    .line 1197
    iget-object v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    add-int/lit8 v7, v2, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1198
    .local v1, "childAbove":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    add-int v0, v6, v7

    .line 1200
    .end local v1    # "childAbove":Landroid/view/View;
    :cond_0
    iget-object v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v3, v5, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1201
    iget-object v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    return-object v6
.end method

.method public getGridLayoutParams()Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mLp:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    return-object v0
.end method

.method public getMeasuredHeight()I
    .locals 1

    .prologue
    .line 1515
    iget v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mMeasuredHeight:I

    return v0
.end method

.method public getMeasuredWidth()I
    .locals 1

    .prologue
    .line 1510
    iget v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mMeasuredWidth:I

    return v0
.end method

.method public getViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1496
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    return-object v0
.end method

.method public gridLayout(IIII)V
    .locals 6
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 1359
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->onLayout(ZIIII)V

    .line 1360
    return-void
.end method

.method public gridMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1354
    invoke-virtual {p0, p1, p2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->onMeasure(II)V

    .line 1355
    return-void
.end method

.method isCollapsible()Z
    .locals 2

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 1500
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    return v0
.end method

.method public isGone()Z
    .locals 1

    .prologue
    .line 1521
    const/4 v0, 0x0

    return v0
.end method

.method public isTopCard(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1570
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 1446
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v3

    .line 1447
    .local v3, "childCount":I
    iget v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    .line 1448
    .local v8, "verticalOffset":I
    move/from16 v0, p2

    iput v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->left:I

    .line 1449
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->right:I

    .line 1450
    move/from16 v0, p3

    iput v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->top:I

    .line 1451
    move/from16 v0, p5

    iput v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->bottom:I

    .line 1452
    iget v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->top:I

    .line 1455
    .local v4, "currentTop":I
    iget-boolean v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz v9, :cond_0

    .line 1456
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 1457
    .local v5, "headerHeight":I
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    iget v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->left:I

    iget v11, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->right:I

    add-int v12, v4, v5

    invoke-virtual {v9, v10, v4, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 1458
    add-int/2addr v4, v5

    .line 1463
    .end local v5    # "headerHeight":I
    :cond_0
    iput v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mCollapsedCardsTop:I

    .line 1464
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v3, :cond_5

    .line 1465
    iget-boolean v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-nez v9, :cond_1

    .line 1467
    const/4 v9, 0x1

    if-ne v3, v9, :cond_3

    .line 1468
    const/4 v7, 0x0

    .line 1472
    .local v7, "s":F
    :goto_1
    float-to-double v10, v7

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-float v7, v10

    .line 1473
    iget v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    int-to-float v9, v9

    mul-float/2addr v9, v7

    float-to-int v8, v9

    .line 1474
    iget v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->top:I

    add-int v4, v9, v8

    .line 1477
    .end local v7    # "s":F
    :cond_1
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1478
    .local v2, "child":Landroid/view/View;
    add-int/lit8 v9, v3, -0x1

    if-ge v6, v9, :cond_4

    .line 1479
    iget v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->left:I

    iget v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->right:I

    iget v11, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    add-int/2addr v11, v4

    invoke-virtual {v2, v9, v4, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 1488
    :goto_2
    iget-boolean v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v9, :cond_2

    .line 1489
    add-int/2addr v4, v8

    .line 1464
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1470
    .end local v2    # "child":Landroid/view/View;
    :cond_3
    const/high16 v9, 0x3f800000    # 1.0f

    int-to-float v10, v6

    mul-float/2addr v9, v10

    add-int/lit8 v10, v3, -0x1

    int-to-float v10, v10

    div-float v7, v9, v10

    .restart local v7    # "s":F
    goto :goto_1

    .line 1483
    .end local v7    # "s":F
    .restart local v2    # "child":Landroid/view/View;
    :cond_4
    iput v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mCollapsedCardsBottom:I

    .line 1486
    iget v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->left:I

    iget v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->right:I

    iget v11, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->bottom:I

    invoke-virtual {v2, v9, v4, v10, v11}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 1492
    .end local v2    # "child":Landroid/view/View;
    :cond_5
    return-void
.end method

.method public onMeasure(II)V
    .locals 16
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1363
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 1364
    .local v7, "heightMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 1365
    .local v8, "heightSize":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 1368
    .local v12, "width":I
    move/from16 v4, p1

    .line 1370
    .local v4, "childWidthSpec":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    .line 1371
    .local v2, "childCount":I
    if-nez v2, :cond_0

    .line 1372
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setMeasuredDimension(II)V

    .line 1434
    :goto_0
    return-void

    .line 1377
    :cond_0
    const/4 v5, 0x0

    .line 1378
    .local v5, "headerHeight":I
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz v13, :cond_1

    .line 1379
    const/high16 v13, 0x40000000    # 2.0f

    if-ne v7, v13, :cond_3

    .line 1381
    const/high16 v13, -0x80000000

    invoke-static {v8, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1383
    .local v11, "refHeightSpec":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v13, v4, v11}, Landroid/view/View;->measure(II)V

    .line 1384
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 1400
    .end local v11    # "refHeightSpec":I
    :cond_1
    :goto_1
    const/4 v6, 0x0

    .line 1401
    .local v6, "height":I
    const/high16 v13, 0x40000000    # 2.0f

    if-ne v7, v13, :cond_5

    .line 1402
    sub-int v6, v8, v5

    .line 1403
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    sub-int v13, v8, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    .line 1424
    :cond_2
    :goto_2
    add-int/2addr v6, v5

    .line 1426
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1428
    .local v3, "childHeightSpec":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    if-ge v9, v2, :cond_b

    .line 1429
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1430
    .local v1, "child":Landroid/view/View;
    move/from16 v0, p1

    invoke-virtual {v1, v0, v3}, Landroid/view/View;->measure(II)V

    .line 1428
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 1385
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "childHeightSpec":I
    .end local v6    # "height":I
    .end local v9    # "i":I
    :cond_3
    const/high16 v13, -0x80000000

    if-ne v7, v13, :cond_4

    .line 1386
    const/high16 v13, -0x80000000

    invoke-static {v8, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1388
    .restart local v11    # "refHeightSpec":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v13, v4, v11}, Landroid/view/View;->measure(II)V

    .line 1389
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 1390
    goto :goto_1

    .end local v11    # "refHeightSpec":I
    :cond_4
    if-nez v7, :cond_1

    .line 1391
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1392
    .restart local v11    # "refHeightSpec":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v13, v4, v11}, Landroid/view/View;->measure(II)V

    .line 1393
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    goto :goto_1

    .line 1404
    .end local v11    # "refHeightSpec":I
    .restart local v6    # "height":I
    :cond_5
    const/high16 v13, -0x80000000

    if-ne v7, v13, :cond_8

    .line 1405
    sub-int v13, v8, v5

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    add-int/lit8 v15, v2, -0x1

    mul-int/2addr v14, v15

    sub-int v10, v13, v14

    .line 1407
    .local v10, "maxHeight":I
    const/high16 v13, -0x80000000

    invoke-static {v10, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1408
    .restart local v11    # "refHeightSpec":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getTallestChildHeight(II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    .line 1409
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v13, :cond_6

    .line 1410
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    add-int/lit8 v15, v2, -0x1

    mul-int/2addr v14, v15

    add-int v6, v13, v14

    goto/16 :goto_2

    .line 1412
    :cond_6
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    const/4 v13, 0x1

    if-le v2, v13, :cond_7

    const/4 v13, 0x1

    :goto_4
    mul-int/2addr v13, v15

    add-int v6, v14, v13

    goto/16 :goto_2

    :cond_7
    const/4 v13, 0x0

    goto :goto_4

    .line 1414
    .end local v10    # "maxHeight":I
    .end local v11    # "refHeightSpec":I
    :cond_8
    if-nez v7, :cond_2

    .line 1415
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 1416
    .restart local v11    # "refHeightSpec":I
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getTallestChildHeight(II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    .line 1417
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v13, :cond_9

    .line 1418
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    add-int/lit8 v15, v2, -0x1

    mul-int/2addr v14, v15

    add-int v6, v13, v14

    goto/16 :goto_2

    .line 1420
    :cond_9
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    const/4 v13, 0x1

    if-le v2, v13, :cond_a

    const/4 v13, 0x1

    :goto_5
    mul-int/2addr v13, v15

    add-int v6, v14, v13

    goto/16 :goto_2

    :cond_a
    const/4 v13, 0x0

    goto :goto_5

    .line 1433
    .end local v11    # "refHeightSpec":I
    .restart local v3    # "childHeightSpec":I
    .restart local v9    # "i":I
    :cond_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setMeasuredDimension(II)V

    goto/16 :goto_0
.end method

.method public removeExtraViews()V
    .locals 2

    .prologue
    .line 1563
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1564
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$2001(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V

    .line 1565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    .line 1567
    :cond_0
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1233
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1234
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1235
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    .line 1237
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->updateItemBackgrounds()V

    .line 1238
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->updateTotalCollapsedSpacing()V

    .line 1239
    return-void
.end method

.method setExpanded(Z)V
    .locals 2
    .param p1, "expanded"    # Z

    .prologue
    .line 1205
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1206
    const/4 p1, 0x1

    .line 1208
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eq p1, v0, :cond_2

    .line 1209
    iput-boolean p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    .line 1210
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1211
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1213
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->requestLayout()V

    .line 1215
    :cond_2
    return-void

    .line 1211
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method

.method setHeader(Landroid/view/View;)V
    .locals 2
    .param p1, "header"    # Landroid/view/View;

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1501(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V

    .line 1175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    .line 1178
    :cond_0
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    .line 1179
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1180
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1181
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpandedHeader:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1601(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V

    .line 1184
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v0, :cond_2

    .line 1185
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->invalidate()V

    .line 1187
    :cond_2
    return-void
.end method

.method setMeasuredDimension(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1504
    iput p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mMeasuredWidth:I

    .line 1505
    iput p2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mMeasuredHeight:I

    .line 1506
    return-void
.end method

.method public setVisibility(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 1319
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1320
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 1321
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1322
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1320
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1324
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public shouldShowDismissTrail()Z
    .locals 1

    .prologue
    .line 1526
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mShowingDismissTrail:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    invoke-interface {v0}, Lcom/google/android/ublib/infocards/DismissTrailFactory;->shouldShowDismissTrail()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showDismissTrail()V
    .locals 3

    .prologue
    .line 1533
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/ublib/infocards/DismissTrailFactory;

    invoke-interface {v1}, Lcom/google/android/ublib/infocards/DismissTrailFactory;->createDismissTrail()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    .line 1534
    new-instance v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mLp:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    invoke-direct {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1535
    .local v0, "layoutParams":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1701(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1538
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    .line 1541
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1542
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->updateTotalCollapsedSpacing()V

    .line 1544
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mShowingDismissTrail:Z

    .line 1545
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->fadeIn(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1800(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V

    .line 1546
    return-void
.end method

.method public updateItemBackgrounds()V
    .locals 0

    .prologue
    .line 1264
    return-void
.end method

.method updateTotalCollapsedSpacing()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1224
    iget v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mDensity:F

    const/high16 v3, 0x42e60000    # 115.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    .line 1226
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1227
    .local v0, "nViews":I
    int-to-float v2, v0

    mul-float/2addr v2, v4

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1228
    .local v1, "r":F
    iget v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float v3, v4, v1

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    .line 1229
    return-void
.end method
