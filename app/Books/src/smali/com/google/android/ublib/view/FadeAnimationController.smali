.class public Lcom/google/android/ublib/view/FadeAnimationController;
.super Ljava/lang/Object;
.source "FadeAnimationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;
    }
.end annotation


# instance fields
.field private mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;

.field private final mInvisibleValue:I

.field private mShowViewBeforeFadingIn:Z

.field private final mView:Landroid/view/View;

.field private mVisible:Z

.field private final mVisibleAlpha:F


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 66
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "invisibleValue"    # I

    .prologue
    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;IF)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/view/View;IF)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "invisibleValue"    # I
    .param p3, "visibleAlpha"    # F

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mShowViewBeforeFadingIn:Z

    .line 55
    iput-object p1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;

    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    .line 57
    iput p2, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mInvisibleValue:I

    .line 58
    iput p3, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisibleAlpha:F

    .line 59
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/ublib/view/FadeAnimationController;Lcom/google/android/ublib/view/AnimatorCompat;)Lcom/google/android/ublib/view/AnimatorCompat;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/view/FadeAnimationController;
    .param p1, "x1"    # Lcom/google/android/ublib/view/AnimatorCompat;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/ublib/view/FadeAnimationController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/view/FadeAnimationController;

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mInvisibleValue:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/ublib/view/FadeAnimationController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/view/FadeAnimationController;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public getVisible()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    return v0
.end method

.method public setVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 93
    const/16 v0, 0x12c

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZILcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    .line 94
    return-void
.end method

.method public setVisible(ZILcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V
    .locals 6
    .param p1, "visible"    # Z
    .param p2, "durationMillis"    # I
    .param p3, "listener"    # Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    .prologue
    const/4 v2, 0x0

    .line 102
    iget-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    if-eq p1, v0, :cond_6

    .line 103
    iput-boolean p1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    .line 108
    if-eqz p1, :cond_0

    .line 109
    iget-object v3, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mShowViewBeforeFadingIn:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 112
    :cond_0
    if-eqz p1, :cond_5

    move v1, v2

    .line 113
    .local v1, "fromAlpha":F
    :goto_1
    if-eqz p1, :cond_1

    iget v2, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisibleAlpha:F

    .line 115
    .local v2, "toAlpha":F
    :cond_1
    new-instance v5, Lcom/google/android/ublib/view/FadeAnimationController$1;

    invoke-direct {v5, p0, p1, p3}, Lcom/google/android/ublib/view/FadeAnimationController$1;-><init>(Lcom/google/android/ublib/view/FadeAnimationController;ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    .line 140
    .local v5, "animatorListener":Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;

    invoke-interface {v0}, Lcom/google/android/ublib/view/AnimatorCompat;->cancel()V

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;

    const/4 v4, 0x0

    move v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/ublib/view/AlphaAnimatorCompat;->makeAlphaAnimation(Landroid/view/View;FFILcom/google/android/ublib/view/AnimatorCompat$Interpolation;Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;)Lcom/google/android/ublib/view/AnimatorCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;

    .line 153
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;

    invoke-interface {v0}, Lcom/google/android/ublib/view/AnimatorCompat;->start()V

    .line 159
    .end local v1    # "fromAlpha":F
    .end local v2    # "toAlpha":F
    .end local v5    # "animatorListener":Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;
    :cond_3
    :goto_2
    return-void

    .line 109
    :cond_4
    const/4 v0, 0x4

    goto :goto_0

    .line 112
    :cond_5
    iget v1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisibleAlpha:F

    goto :goto_1

    .line 155
    :cond_6
    const-string v0, "FadeAnimController"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 156
    const-string v0, "FadeAnimController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVisible() was a no-op because visible is already "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public setVisible(ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "listener"    # Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    .prologue
    .line 97
    const/16 v0, 0x12c

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZILcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    .line 98
    return-void
.end method

.method public setVisibleNoAnim(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 83
    iget-boolean v1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    if-eq p1, v1, :cond_0

    .line 84
    iput-boolean p1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    .line 86
    iget-object v2, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisible:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 87
    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mVisibleAlpha:F

    .line 88
    .local v0, "alpha":F
    :goto_1
    iget-object v1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 90
    .end local v0    # "alpha":F
    :cond_0
    return-void

    .line 86
    :cond_1
    iget v1, p0, Lcom/google/android/ublib/view/FadeAnimationController;->mInvisibleValue:I

    goto :goto_0

    .line 87
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
