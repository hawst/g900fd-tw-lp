.class public Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;
.super Ljava/lang/Object;
.source "DocumentClickHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/ublib/cardlib/model/Document;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDoc:Lcom/google/android/ublib/cardlib/model/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler<TT;>;"
    .local p2, "doc":Lcom/google/android/ublib/cardlib/model/Document;, "TT;"
    .local p3, "callback":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mContext:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mDoc:Lcom/google/android/ublib/cardlib/model/Document;

    .line 24
    iput-object p3, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    .line 25
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler<TT;>;"
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler;->mDoc:Lcom/google/android/ublib/cardlib/model/Document;

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;->onDocumentClick(Landroid/content/Context;Ljava/lang/Object;Landroid/view/View;)V

    .line 32
    :cond_0
    return-void
.end method
