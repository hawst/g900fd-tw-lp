.class public Lcom/google/android/ublib/widget/HorizontalWarpListView;
.super Lcom/google/android/ublib/widget/AbsWarpListView;
.source "HorizontalWarpListView.java"


# instance fields
.field private mDownX:F

.field private mDownY:F

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private final mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mRTL:Z

.field private mScrollBlocked:Z

.field private mScrolling:Z

.field private final mTmpRect:Landroid/graphics/Rect;

.field private final mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field protected final mViewConfig:Landroid/view/ViewConfiguration;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    .line 23
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    .line 39
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;

    invoke-direct {v2, p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;-><init>(Lcom/google/android/ublib/widget/HorizontalWarpListView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    .line 23
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    .line 39
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;

    invoke-direct {v2, p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;-><init>(Lcom/google/android/ublib/widget/HorizontalWarpListView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/ublib/widget/AbsWarpListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewConfig:Landroid/view/ViewConfiguration;

    .line 23
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    .line 39
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;

    invoke-direct {v2, p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView$1;-><init>(Lcom/google/android/ublib/widget/HorizontalWarpListView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrolling:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/ublib/widget/HorizontalWarpListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrolling:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/ublib/widget/HorizontalWarpListView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mDownY:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrollBlocked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/ublib/widget/HorizontalWarpListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrollBlocked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/ublib/widget/HorizontalWarpListView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mDownX:F

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/ublib/widget/HorizontalWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z

    return v0
.end method

.method private getMeasureSpec(II)I
    .locals 2
    .param p1, "size"    # I
    .param p2, "parent"    # I

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 181
    packed-switch p1, :pswitch_data_0

    .line 187
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_0
    return v0

    .line 183
    :pswitch_0
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    .line 185
    :pswitch_1
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 205
    invoke-super {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 208
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 209
    const/high16 v0, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 210
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 211
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 214
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 218
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 219
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 220
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 221
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 222
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 226
    :cond_2
    return-void
.end method

.method protected getLeadingEdge()Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1

    .prologue
    .line 151
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 152
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_0
.end method

.method protected getMaxOffset()I
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getWidth()I

    move-result v0

    return v0
.end method

.method protected getTrailingEdge()Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1

    .prologue
    .line 157
    invoke-static {p0}, Lcom/google/android/ublib/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 158
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mLeadingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTrailingEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_0
.end method

.method public getViewPosition(Landroid/view/View;)I
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {p1, v0}, Lcom/google/android/ublib/view/ViewCompat;->getHitRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getMaxOffset()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    .line 145
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    goto :goto_0
.end method

.method public getViewSize(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {p1, v0}, Lcom/google/android/ublib/view/ViewCompat;->getHitRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public isRTL()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z

    return v0
.end method

.method protected layoutView(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 193
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 194
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getWidth()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getMeasureSpec(II)I

    move-result v3

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getHeight()I

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getMeasureSpec(II)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/view/View;->measure(II)V

    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 198
    .local v2, "viewHeight":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getHeight()I

    move-result v3

    sub-int/2addr v3, v2

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mViewAlignment:F

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 200
    .local v1, "top":I
    const/16 v3, -0x2000

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/lit16 v4, v4, -0x2000

    add-int v5, v1, v2

    invoke-virtual {p1, v3, v1, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 201
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 163
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->handleTouchEvent(Landroid/view/MotionEvent;)V

    .line 164
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 165
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mDownX:F

    .line 166
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mDownY:F

    .line 167
    iput-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrollBlocked:Z

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrolling:Z

    .line 169
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mScrollBlocked:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 175
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->handleTouchEvent(Landroid/view/MotionEvent;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 177
    const/4 v0, 0x1

    return v0
.end method

.method protected positionView(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "off"    # I

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {p1, v1}, Lcom/google/android/ublib/view/ViewCompat;->getHitRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isRTL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getMaxOffset()I

    move-result v1

    sub-int/2addr v1, p2

    iget-object v2, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int p2, v1, v2

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v0, p2, v1

    .line 122
    .local v0, "change":I
    if-eqz v0, :cond_1

    .line 123
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    int-to-float v2, v0

    add-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 125
    :cond_1
    return-void
.end method

.method public setRTL(Z)V
    .locals 0
    .param p1, "isRTL"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/HorizontalWarpListView;->mRTL:Z

    .line 111
    return-void
.end method
