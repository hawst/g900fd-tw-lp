.class public Lcom/google/android/ublib/cardlib/layout/PopupMenuFactory;
.super Ljava/lang/Object;
.source "PopupMenuFactory.java"


# direct methods
.method public static getInstance(Landroid/content/Context;Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "anchor"    # Landroid/view/View;
    .param p2, "systemUi"    # Lcom/google/android/ublib/view/SystemUi;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 31
    if-eqz p2, :cond_0

    .line 32
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)V

    .line 34
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;

    invoke-direct {v0, p0, p1}, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method
