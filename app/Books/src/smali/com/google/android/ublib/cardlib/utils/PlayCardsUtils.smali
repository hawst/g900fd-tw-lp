.class public Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;
.super Ljava/lang/Object;
.source "PlayCardsUtils.java"


# direct methods
.method public static final getCardsGridColumnCount(Landroid/content/res/Resources;IIII)I
    .locals 5
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "minColumnWidth"    # I
    .param p2, "extraOuter"    # I
    .param p3, "extraInner"    # I
    .param p4, "minColumnCount"    # I

    .prologue
    .line 18
    const/high16 v3, 0x7f0d0000

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 19
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 20
    .local v2, "displayWidth":I
    mul-int/lit8 v3, p2, 0x2

    sub-int v3, v2, v3

    mul-int/lit8 v4, p3, 0x2

    add-int/2addr v4, p1

    div-int v1, v3, v4

    .line 21
    .local v1, "count":I
    mul-int v3, v1, p3

    add-int/2addr v3, p2

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v2, v3

    div-int v0, v3, v1

    .line 22
    .local v0, "cellSize":I
    const/4 v3, 0x5

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v3, p4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 24
    .end local v0    # "cellSize":I
    .end local v1    # "count":I
    .end local v2    # "displayWidth":I
    :goto_0
    return v3

    :cond_0
    const v3, 0x7f0c0003

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    goto :goto_0
.end method

.method public static getHorizontalMargins(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 34
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    return v1
.end method

.method public static final getLayoutHeight(Landroid/view/View;)I
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {p0}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getVerticalMargins(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static final getLayoutWidth(Landroid/view/View;)I
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {p0}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getHorizontalMargins(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static getVerticalMargins(Landroid/view/View;)I
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 39
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    return v1
.end method

.method public static includeInLayout(Landroid/view/View;)Z
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 29
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
