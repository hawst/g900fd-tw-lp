.class Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;
.super Ljava/lang/Object;
.source "JellybeanSystemUi.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/view/JellybeanSystemUi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerWrapper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/view/JellybeanSystemUi;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/view/JellybeanSystemUi;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/JellybeanSystemUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/view/JellybeanSystemUi;Lcom/google/android/ublib/view/JellybeanSystemUi$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/view/JellybeanSystemUi;
    .param p2, "x1"    # Lcom/google/android/ublib/view/JellybeanSystemUi$1;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;-><init>(Lcom/google/android/ublib/view/JellybeanSystemUi;)V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 71
    iget-object v2, p0, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/JellybeanSystemUi;

    # getter for: Lcom/google/android/ublib/view/JellybeanSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    invoke-static {v2}, Lcom/google/android/ublib/view/JellybeanSystemUi;->access$100(Lcom/google/android/ublib/view/JellybeanSystemUi;)Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/JellybeanSystemUi;

    invoke-virtual {v2}, Lcom/google/android/ublib/view/JellybeanSystemUi;->getFlagsToHideSystemBars()I

    move-result v0

    .line 74
    .local v0, "hidingMask":I
    and-int v2, v0, p1

    if-nez v2, :cond_1

    const/4 v1, 0x1

    .line 75
    .local v1, "systemUiVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/ublib/view/JellybeanSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/JellybeanSystemUi;

    # getter for: Lcom/google/android/ublib/view/JellybeanSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    invoke-static {v2}, Lcom/google/android/ublib/view/JellybeanSystemUi;->access$100(Lcom/google/android/ublib/view/JellybeanSystemUi;)Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/ublib/view/SystemUi$VisibilityListener;->onSystemUiVisibilityChanged(Z)V

    .line 77
    .end local v0    # "hidingMask":I
    .end local v1    # "systemUiVisible":Z
    :cond_0
    return-void

    .line 74
    .restart local v0    # "hidingMask":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
