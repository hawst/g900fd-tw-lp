.class public Lcom/google/android/ublib/cardlib/PlayCardArtImageView;
.super Landroid/widget/ImageView;
.source "PlayCardArtImageView.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;
    }
.end annotation


# static fields
.field private static DEFAULT_ASPECT_RATIO:F

.field private static sCoverUriToBackgroundColor:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAspectRatio:F

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDesiredImageHeight:I

.field private mDesiredImageWidth:I

.field private final mFetchImageTask:Ljava/lang/Runnable;

.field private mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

.field private final mFinishSettingArtImageTask:Ljava/lang/Runnable;

.field private mHasImage:Z

.field private mImageHeight:I

.field private mImageProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

.field private mImageUri:Landroid/net/Uri;

.field private mImageWidth:I

.field private mMeasuring:Z

.field private mOnReadyListener:Ljava/lang/Runnable;

.field private mOptimizedForScrollingEnabled:Z

.field private mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    const v0, 0x3fb872b0    # 1.441f

    sput v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->DEFAULT_ASPECT_RATIO:F

    .line 478
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->sCoverUriToBackgroundColor:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    iput-boolean v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mHasImage:Z

    .line 69
    iput-boolean v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mOptimizedForScrollingEnabled:Z

    .line 102
    sget-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 103
    sget v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->DEFAULT_ASPECT_RATIO:F

    iput v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mAspectRatio:F

    .line 116
    new-instance v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$1;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$1;-><init>(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;)V

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFetchImageTask:Ljava/lang/Runnable;

    .line 251
    new-instance v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$2;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$2;-><init>(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;)V

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFinishSettingArtImageTask:Ljava/lang/Runnable;

    .line 131
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->fetchImageFromProvider()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->finishSettingArtImage()V

    return-void
.end method

.method static synthetic access$200()Landroid/support/v4/util/LruCache;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->sCoverUriToBackgroundColor:Landroid/support/v4/util/LruCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method private clearArtwork(Z)V
    .locals 4
    .param p1, "requestLayout"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 174
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFetchImageTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 175
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider;->cancelFetch()V

    .line 178
    :cond_0
    iput-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    .line 179
    iput-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 180
    const-string v0, "PlayCardArtImageView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    const-string v0, "clearing bitmap"

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->debugLog(Ljava/lang/String;)V

    .line 183
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 184
    invoke-static {p0, v2}, Lcom/google/android/ublib/view/ViewCompat;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 185
    iput v3, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    .line 186
    iput v3, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    .line 187
    iput-boolean v3, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mHasImage:Z

    .line 195
    if-eqz p1, :cond_2

    .line 196
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->requestLayout()V

    .line 198
    :cond_2
    return-void
.end method

.method private computeValidDimensionsFrom(II)Z
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v0, 0x1

    .line 271
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v1, v2, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 273
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    .line 290
    :goto_0
    return v0

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_HEIGHT:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v1, v2, :cond_1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 277
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    goto :goto_0

    .line 280
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 281
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    .line 282
    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    invoke-direct {p0, v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getHeightForWidth(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    goto :goto_0

    .line 284
    :cond_2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 285
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    .line 286
    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    invoke-direct {p0, v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getWidthForHeight(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    goto :goto_0

    .line 290
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private debugLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 535
    const-string v0, "PlayCardArtImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    return-void
.end method

.method private fetchImageFromProvider()V
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider;->cancelFetch()V

    .line 517
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    if-eqz v0, :cond_1

    .line 518
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v1, v1, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v2, v2, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider;->fetchImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    const-string v0, "PlayCardArtImageView"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    const-string v0, "PlayCardArtImageView"

    const-string v1, "Missing requested image specifier in PlayCardArtImageView"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private finishSettingArtImage()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mOnReadyListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mOnReadyListener:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->maybeShowImage()V

    goto :goto_0
.end method

.method private getBorderColor(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 468
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    return v0
.end method

.method private getHeightForWidth(I)I
    .locals 2
    .param p1, "width"    # I

    .prologue
    .line 300
    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mAspectRatio:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private getWidthForHeight(I)I
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 295
    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mAspectRatio:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private hasValidSize()Z
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    if-gtz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_HEIGHT:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    if-gtz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    if-lez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeImageConstraints()Lcom/google/android/ublib/util/ImageConstraints;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 414
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v1, v2, :cond_1

    .line 415
    iget v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/ublib/util/ImageConstraints;->fixedWidth(Ljava/lang/Integer;)Lcom/google/android/ublib/util/ImageConstraints;

    move-result-object v0

    .line 434
    :cond_0
    :goto_0
    return-object v0

    .line 416
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_HEIGHT:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-eq v1, v2, :cond_0

    .line 421
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v1, v2, :cond_0

    .line 422
    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    if-lez v1, :cond_2

    .line 423
    new-instance v0, Lcom/google/android/ublib/util/ImageConstraints;

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    invoke-direct {p0, v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getHeightForWidth(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/ublib/util/ImageConstraints;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0

    .line 425
    :cond_2
    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    if-lez v1, :cond_3

    .line 426
    new-instance v0, Lcom/google/android/ublib/util/ImageConstraints;

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    invoke-direct {p0, v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getWidthForHeight(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/ublib/util/ImageConstraints;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0

    .line 429
    :cond_3
    const-string v1, "PlayCardArtImageView"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 430
    const-string v1, "PlayCardArtImageView"

    const-string v2, "Tried to make image constraints with no valid dimensions"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private makeImageSpecifier()Lcom/google/android/ublib/util/ImageSpecifier;
    .locals 3

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->hasValidSize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    new-instance v0, Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->makeImageConstraints()Lcom/google/android/ublib/util/ImageConstraints;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/ublib/util/ImageSpecifier;-><init>(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;)V

    .line 410
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeSetBackground(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    .line 490
    .local v0, "imageUri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 513
    :goto_0
    return-void

    .line 495
    :cond_0
    sget-object v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->sCoverUriToBackgroundColor:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 496
    .local v1, "pixelColor":Ljava/lang/Integer;
    if-eqz v1, :cond_1

    .line 497
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 500
    :cond_1
    new-instance v2, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;

    invoke-direct {v2, p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$3;-><init>(Lcom/google/android/ublib/cardlib/PlayCardArtImageView;Landroid/net/Uri;)V

    invoke-static {p1, v2}, Landroid/support/v7/graphics/Palette;->generateAsync(Landroid/graphics/Bitmap;Landroid/support/v7/graphics/Palette$PaletteAsyncListener;)Landroid/os/AsyncTask;

    .line 512
    invoke-direct {p0, p1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getBorderColor(Landroid/graphics/Bitmap;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private maybeShowImage()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 456
    const-string v0, "PlayCardArtImageView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    const-string v0, "setting bitmap"

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->debugLog(Ljava/lang/String;)V

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 460
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->maybeSetBackground(Landroid/graphics/Bitmap;)V

    .line 461
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mHasImage:Z

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 464
    :cond_1
    return-void
.end method

.method private setArtImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x3

    .line 219
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    .line 220
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    .line 221
    const-string v0, "PlayCardArtImageView"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received Image W"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",H"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->debugLog(Ljava/lang/String;)V

    .line 225
    :cond_0
    const-string v0, "PlayCardArtImageView"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v0, v0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    if-eqz v0, :cond_1

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requested "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v1, v1, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", received "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->debugLog(Ljava/lang/String;)V

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v0, v0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v0, v0, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    iget v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    iget v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/util/ImageConstraints;->satisfiesConstraints(II)Z

    move-result v0

    if-nez v0, :cond_2

    .line 234
    const-string v0, "PlayCardArtImageView"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    const-string v0, "PlayCardArtImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requesting image with constraints "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v2, v2, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but received width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_2
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 243
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mMeasuring:Z

    if-nez v0, :cond_3

    .line 244
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->finishSettingArtImage()V

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_3
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFinishSettingArtImageTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public clearArtwork()V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->clearArtwork(Z)V

    .line 202
    return-void
.end method

.method public getArtUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 0

    .prologue
    .line 539
    return-object p0
.end method

.method public hasImage()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mHasImage:Z

    return v0
.end method

.method public maybeLoadImage()V
    .locals 4

    .prologue
    .line 372
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->makeImageSpecifier()Lcom/google/android/ublib/util/ImageSpecifier;

    move-result-object v0

    .line 373
    .local v0, "spec":Lcom/google/android/ublib/util/ImageSpecifier;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 374
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    .line 375
    const-string v1, "PlayCardArtImageView"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requesting image with constraints "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v2, v2, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->debugLog(Ljava/lang/String;)V

    .line 382
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mOptimizedForScrollingEnabled:Z

    if-eqz v1, :cond_2

    .line 385
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFetchImageTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 390
    :cond_1
    :goto_0
    return-void

    .line 387
    :cond_2
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->fetchImageFromProvider()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->clearArtwork(Z)V

    .line 136
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 137
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mRequestedImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    .line 148
    return-void
.end method

.method public onImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setArtImage(Landroid/graphics/Bitmap;)V

    .line 142
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 305
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 306
    .local v3, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 311
    .local v0, "heightSize":I
    iget v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageWidth:I

    if-lez v6, :cond_0

    iget v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mDesiredImageHeight:I

    if-gtz v6, :cond_1

    .line 313
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->computeValidDimensionsFrom(II)Z

    .line 324
    :cond_1
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mMeasuring:Z

    .line 325
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->maybeLoadImage()V

    .line 326
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mMeasuring:Z

    .line 328
    iget v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    if-lez v6, :cond_2

    iget v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v7, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v6, v7, :cond_5

    .line 333
    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    if-ne v6, v10, :cond_4

    .line 334
    move v2, p1

    .line 335
    .local v2, "outWidthMeasureSpec":I
    invoke-direct {p0, v3}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getHeightForWidth(I)I

    move-result v6

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 355
    .local v1, "outHeightMeasureSpec":I
    :goto_0
    const-string v6, "PlayCardArtImageView"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 356
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    invoke-virtual {v7}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " A"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mAspectRatio:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " W"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",H"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " -->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " W\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",H\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->debugLog(Ljava/lang/String;)V

    .line 364
    :cond_3
    invoke-super {p0, v2, v1}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 365
    return-void

    .line 339
    .end local v1    # "outHeightMeasureSpec":I
    .end local v2    # "outWidthMeasureSpec":I
    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getWidthForHeight(I)I

    move-result v6

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 341
    .restart local v2    # "outWidthMeasureSpec":I
    move v1, p2

    .restart local v1    # "outHeightMeasureSpec":I
    goto :goto_0

    .line 343
    .end local v1    # "outHeightMeasureSpec":I
    .end local v2    # "outWidthMeasureSpec":I
    :cond_5
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v7, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    if-ne v6, v7, :cond_6

    .line 344
    int-to-double v6, v3

    iget v8, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    int-to-double v8, v8

    div-double v4, v6, v8

    .line 345
    .local v4, "scale":D
    move v2, p1

    .line 346
    .restart local v2    # "outWidthMeasureSpec":I
    iget v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    int-to-double v6, v6

    mul-double/2addr v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 348
    .restart local v1    # "outHeightMeasureSpec":I
    goto/16 :goto_0

    .line 349
    .end local v1    # "outHeightMeasureSpec":I
    .end local v2    # "outWidthMeasureSpec":I
    .end local v4    # "scale":D
    :cond_6
    int-to-double v6, v0

    iget v8, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageHeight:I

    int-to-double v8, v8

    div-double v4, v6, v8

    .line 350
    .restart local v4    # "scale":D
    iget v6, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageWidth:I

    int-to-double v6, v6

    mul-double/2addr v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 352
    .restart local v2    # "outWidthMeasureSpec":I
    move v1, p2

    .restart local v1    # "outHeightMeasureSpec":I
    goto/16 :goto_0
.end method

.method public setArtURI(Landroid/net/Uri;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V
    .locals 1
    .param p1, "imageURI"    # Landroid/net/Uri;
    .param p2, "imageProvider"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->clearArtwork(Z)V

    .line 207
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageUri:Landroid/net/Uri;

    .line 208
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mImageProvider:Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    .line 210
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->maybeLoadImage()V

    .line 212
    :cond_0
    return-void
.end method

.method public setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V
    .locals 0
    .param p1, "fillStyle"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 152
    return-void
.end method

.method public setOptimizedForScrollingEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->mOptimizedForScrollingEnabled:Z

    .line 164
    return-void
.end method
