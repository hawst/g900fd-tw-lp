.class public Lcom/google/android/ublib/cardlib/layout/PopupSelector;
.super Ljava/lang/Object;
.source "PopupSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;
    }
.end annotation


# instance fields
.field private final mAnchorView:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private final mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

.field private mMeasureParent:Landroid/view/ViewGroup;

.field private mPopupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;Lcom/google/android/ublib/view/SystemUi;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;
    .param p3, "listAdapter"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
    .param p4, "callingWindowSystemUi"    # Lcom/google/android/ublib/view/SystemUi;
    .param p5, "boundsView"    # Landroid/view/View;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mAnchorView:Landroid/view/View;

    .line 153
    iput-object p3, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    .line 154
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    .line 156
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 163
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    if-eqz p4, :cond_0

    .line 164
    invoke-direct {p0, v0, p4}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->setupPopupWindow(Landroid/view/LayoutInflater;Lcom/google/android/ublib/view/SystemUi;)V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-direct {p0, v0, p2, p5}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->setupDialog(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->calculateScreenWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->getBackgroundResId()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/ublib/cardlib/layout/PopupSelector;Landroid/view/View;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PopupSelector;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->setupList(Landroid/view/View;)Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/ublib/cardlib/layout/PopupSelector;Landroid/widget/ListView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PopupSelector;
    .param p1, "x1"    # Landroid/widget/ListView;
    .param p2, "x2"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->getMaxContentWidth(Landroid/widget/ListView;I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PopupSelector;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    return-object v0
.end method

.method private calculateScreenWidth()I
    .locals 4

    .prologue
    .line 262
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 263
    .local v0, "displayFrameRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mAnchorView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 264
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int v1, v2, v3

    .line 265
    .local v1, "screenWidth":I
    return v1
.end method

.method private getBackgroundResId()I
    .locals 4

    .prologue
    .line 302
    iget-object v2, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/google/android/ublib/R$styleable;->AppTheme:[I

    invoke-virtual {v2, v3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 303
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 304
    .local v1, "backgroundResId":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 305
    return v1
.end method

.method private getMaxContentWidth(Landroid/widget/ListView;I)I
    .locals 4
    .param p1, "list"    # Landroid/widget/ListView;
    .param p2, "screenWidth"    # I

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->measureContentWidth()I

    move-result v1

    .line 256
    .local v1, "listContentWidth":I
    invoke-virtual {p1}, Landroid/widget/ListView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p1}, Landroid/widget/ListView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    mul-int/lit8 v3, p2, 0x4

    div-int/lit8 v3, v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 258
    .local v0, "contentWidth":I
    return v0
.end method

.method private measureContentWidth()I
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 276
    const/4 v6, 0x0

    .line 277
    .local v6, "width":I
    const/4 v4, 0x0

    .line 278
    .local v4, "itemView":Landroid/view/View;
    const/4 v3, 0x0

    .line 279
    .local v3, "itemType":I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 281
    .local v7, "widthMeasureSpec":I
    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 283
    .local v1, "heightMeasureSpec":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getCount()I

    move-result v0

    .line 284
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 285
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v8, v2}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItemViewType(I)I

    move-result v5

    .line 286
    .local v5, "positionType":I
    if-eq v5, v3, :cond_0

    .line 287
    move v3, v5

    .line 288
    const/4 v4, 0x0

    .line 290
    :cond_0
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mMeasureParent:Landroid/view/ViewGroup;

    if-nez v8, :cond_1

    .line 291
    new-instance v8, Landroid/widget/FrameLayout;

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mMeasureParent:Landroid/view/ViewGroup;

    .line 293
    :cond_1
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mMeasureParent:Landroid/view/ViewGroup;

    invoke-virtual {v8, v2, v4, v9}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 294
    invoke-virtual {v4, v7, v1}, Landroid/view/View;->measure(II)V

    .line 295
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 284
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 298
    .end local v5    # "positionType":I
    :cond_2
    return v6
.end method

.method private setupDialog(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p2, "anchorView"    # Landroid/view/View;
    .param p3, "boundsView"    # Landroid/view/View;

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$PopupSelectorDialog;-><init>(Lcom/google/android/ublib/cardlib/layout/PopupSelector;Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mDialog:Landroid/app/Dialog;

    .line 173
    return-void
.end method

.method private setupList(Landroid/view/View;)Landroid/widget/ListView;
    .locals 2
    .param p1, "mainView"    # Landroid/view/View;

    .prologue
    .line 205
    const v1, 0x7f0e0195

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 206
    .local v0, "list":Landroid/widget/ListView;
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mListAdapter:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 209
    :cond_0
    new-instance v1, Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;

    invoke-direct {v1, p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector$1;-><init>(Lcom/google/android/ublib/cardlib/layout/PopupSelector;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 225
    return-object v0
.end method

.method private setupPopupWindow(Landroid/view/LayoutInflater;Lcom/google/android/ublib/view/SystemUi;)V
    .locals 8
    .param p1, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p2, "callingWindowSystemUi"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x2

    .line 178
    const v4, 0x7f0400a1

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;

    .line 181
    .local v1, "mainView":Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->calculateScreenWidth()I

    move-result v3

    .line 183
    .local v3, "screenWidth":I
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->getBackgroundResId()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->setBackgroundResource(I)V

    .line 185
    new-instance v4, Landroid/widget/PopupWindow;

    invoke-direct {v4, v1, v6, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 188
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->setPopupWindow(Landroid/widget/PopupWindow;)V

    .line 190
    invoke-static {v1}, Lcom/google/android/ublib/view/SystemUiUtils;->makeSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v2

    .line 191
    .local v2, "popupSystemUi":Lcom/google/android/ublib/view/SystemUi;
    invoke-interface {p2, v2}, Lcom/google/android/ublib/view/SystemUi;->copyFlagsTo(Lcom/google/android/ublib/view/SystemUi;)V

    .line 194
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v7}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 195
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const v6, 0x106000d

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 196
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v7}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 198
    invoke-direct {p0, v1}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->setupList(Landroid/view/View;)Landroid/widget/ListView;

    move-result-object v0

    .line 200
    .local v0, "list":Landroid/widget/ListView;
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-direct {p0, v0, v3}, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->getMaxContentWidth(Landroid/widget/ListView;I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 202
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->update()V

    .line 240
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PopupSelector;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method
