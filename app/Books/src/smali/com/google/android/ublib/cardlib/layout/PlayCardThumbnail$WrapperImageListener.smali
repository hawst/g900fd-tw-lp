.class Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;
.super Ljava/lang/Object;
.source "PlayCardThumbnail.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperImageListener"
.end annotation


# instance fields
.field mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

.field final synthetic this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;->this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .line 192
    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;->onError(Ljava/lang/Throwable;)V

    .line 203
    return-void
.end method

.method public onImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;->this$0:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->onImageStatusChanged(Z)V
    invoke-static {v0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->access$100(Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;Z)V

    .line 197
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail$WrapperImageListener;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;->onImage(Landroid/graphics/Bitmap;)V

    .line 198
    return-void
.end method
