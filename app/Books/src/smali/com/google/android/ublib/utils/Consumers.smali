.class public Lcom/google/android/ublib/utils/Consumers;
.super Ljava/lang/Object;
.source "Consumers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;
    }
.end annotation


# static fields
.field private static NOOP:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/ublib/utils/Consumers$1;

    invoke-direct {v0}, Lcom/google/android/ublib/utils/Consumers$1;-><init>()V

    sput-object v0, Lcom/google/android/ublib/utils/Consumers;->NOOP:Lcom/google/android/ublib/utils/Consumer;

    return-void
.end method

.method public static deliverOnThread(Lcom/google/android/ublib/utils/Consumer;Ljava/util/concurrent/Executor;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    new-instance v0, Lcom/google/android/ublib/utils/Consumers$2;

    invoke-direct {v0, p1, p0}, Lcom/google/android/ublib/utils/Consumers$2;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/ublib/utils/Consumer;)V

    return-object v0
.end method

.method public static deliverOnThreadOrNull(Lcom/google/android/ublib/utils/Consumer;Ljava/util/concurrent/Executor;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 81
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    if-eqz p0, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/ublib/utils/Consumers;->deliverOnThread(Lcom/google/android/ublib/utils/Consumer;Ljava/util/concurrent/Executor;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/ublib/utils/Consumers;->deliverOnThreadOrNull(Lcom/google/android/ublib/utils/Consumer;Ljava/util/concurrent/Executor;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    return-object v0
.end method

.method public static getNoopConsumer()Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/ublib/utils/Consumers;->NOOP:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method public static weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "inner":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    new-instance v0, Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/ublib/utils/Consumers$WeakWrapperConsumer;-><init>(Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumers$1;)V

    return-object v0
.end method
