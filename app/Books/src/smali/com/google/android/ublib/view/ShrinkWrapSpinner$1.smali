.class Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;
.super Landroid/widget/BaseAdapter;
.source "ShrinkWrapSpinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/view/ShrinkWrapSpinner;->createProxyAdapter()Landroid/widget/SpinnerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/view/ShrinkWrapSpinner;

.field final synthetic val$sameView:Landroid/view/View;

.field final synthetic val$selectedItemPosition:I

.field final synthetic val$superAdapter:Landroid/widget/SpinnerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/view/ShrinkWrapSpinner;Landroid/view/View;Landroid/widget/SpinnerAdapter;I)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->this$0:Lcom/google/android/ublib/view/ShrinkWrapSpinner;

    iput-object p2, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$sameView:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$superAdapter:Landroid/widget/SpinnerAdapter;

    iput p4, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$selectedItemPosition:I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$selectedItemPosition:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$superAdapter:Landroid/widget/SpinnerAdapter;

    iget v1, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$selectedItemPosition:I

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$superAdapter:Landroid/widget/SpinnerAdapter;

    iget v1, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$selectedItemPosition:I

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/ublib/view/ShrinkWrapSpinner$1;->val$sameView:Landroid/view/View;

    return-object v0
.end method
