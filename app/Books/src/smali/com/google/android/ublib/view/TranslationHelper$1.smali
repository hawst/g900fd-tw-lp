.class Lcom/google/android/ublib/view/TranslationHelper$1;
.super Ljava/lang/Object;
.source "TranslationHelper.java"

# interfaces
.implements Lcom/google/android/ublib/view/TranslationHelper$Translatable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/view/TranslationHelper;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/view/TranslationHelper;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/ublib/view/TranslationHelper$1;->this$0:Lcom/google/android/ublib/view/TranslationHelper;

    iput-object p2, p0, Lcom/google/android/ublib/view/TranslationHelper$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTranslationX()F
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/ublib/view/TranslationHelper$1;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    return v0
.end method

.method public getTranslationY()F
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/ublib/view/TranslationHelper$1;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    return v0
.end method

.method public setTranslationX(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/ublib/view/TranslationHelper$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 139
    return-void
.end method

.method public setTranslationY(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/ublib/view/TranslationHelper$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 143
    return-void
.end method
