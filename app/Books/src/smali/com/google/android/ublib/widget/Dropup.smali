.class public Lcom/google/android/ublib/widget/Dropup;
.super Landroid/app/Dialog;
.source "Dropup.java"


# instance fields
.field private final mAnchorX:I

.field private final mAnchorY:I

.field private final mContent:Landroid/view/View;

.field private final mContentPaddingBottom:I

.field private final mMaxHeight:I

.field private final mMaxWidth:I


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/high16 v5, -0x80000000

    .line 62
    iget-object v2, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/widget/Dropup;->setContentView(Landroid/view/View;)V

    .line 65
    iget-object v2, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    iget v3, p0, Lcom/google/android/ublib/widget/Dropup;->mMaxWidth:I

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v4, p0, Lcom/google/android/ublib/widget/Dropup;->mMaxHeight:I

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 67
    iget-object v2, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/Dropup;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 70
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 71
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v2, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 72
    iget-object v2, p0, Lcom/google/android/ublib/widget/Dropup;->mContent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 73
    iget v2, p0, Lcom/google/android/ublib/widget/Dropup;->mAnchorX:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 74
    iget v2, p0, Lcom/google/android/ublib/widget/Dropup;->mAnchorY:I

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/ublib/widget/Dropup;->mContentPaddingBottom:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 76
    const/16 v2, 0x33

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 78
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, 0x20100

    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 80
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 81
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 95
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 102
    .end local v0    # "handled":Z
    :goto_0
    return v0

    .line 98
    .restart local v0    # "handled":Z
    :cond_0
    const/16 v1, 0x13

    if-ne p1, v1, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/Dropup;->cancel()V

    .line 100
    const/4 v0, 0x1

    goto :goto_0

    .line 102
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/Dropup;->cancel()V

    .line 87
    const/4 v0, 0x1

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
