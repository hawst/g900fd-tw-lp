.class public Lcom/google/android/ublib/utils/AccessibilityUtils;
.super Ljava/lang/Object;
.source "AccessibilityUtils.java"


# direct methods
.method public static announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "textToAnnounce"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-static {}, Lcom/google/android/ublib/utils/AccessibilityUtils;->getAnnouncementEventType()I

    move-result v0

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/ublib/utils/AccessibilityUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Landroid/view/View;ILjava/lang/String;)V

    .line 108
    return-void
.end method

.method private static getAnnouncementEventType()I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 121
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x4000

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static getHoverEnterEventType()I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 147
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x8000

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static sendAccessibilityEventWithText(Landroid/content/Context;Landroid/view/View;ILjava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "type"    # I
    .param p3, "textToAnnounce"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string v2, "accessibility"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 78
    .local v1, "manager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 100
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 83
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 90
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 93
    if-eqz p1, :cond_1

    .line 95
    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;)V

    .line 99
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public static sendHoverEvent(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 130
    invoke-static {}, Lcom/google/android/ublib/utils/AccessibilityUtils;->getHoverEnterEventType()I

    move-result v0

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/ublib/utils/AccessibilityUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Landroid/view/View;ILjava/lang/String;)V

    .line 132
    return-void
.end method

.method public static setImportantForAccessibility(Landroid/view/View;Z)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "important"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 157
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 160
    .local v0, "mode":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 162
    .end local v0    # "mode":I
    :cond_0
    return-void

    .line 158
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
