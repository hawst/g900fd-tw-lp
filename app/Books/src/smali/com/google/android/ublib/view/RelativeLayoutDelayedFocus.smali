.class public Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;
.super Landroid/widget/RelativeLayout;
.source "RelativeLayoutDelayedFocus.java"


# instance fields
.field private mPopupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 28
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 37
    iget-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 41
    iget-object v0, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->update()V

    .line 43
    :cond_0
    return-void
.end method

.method public setPopupWindow(Landroid/widget/PopupWindow;)V
    .locals 0
    .param p1, "window"    # Landroid/widget/PopupWindow;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/ublib/view/RelativeLayoutDelayedFocus;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 32
    return-void
.end method
