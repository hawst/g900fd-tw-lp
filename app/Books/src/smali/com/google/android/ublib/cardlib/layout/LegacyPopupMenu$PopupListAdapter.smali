.class public Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LegacyPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PopupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAllowCustomView:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "allowCustomView"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p3, "popupActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;>;"
    const v0, 0x7f040045

    const v1, 0x7f0e00f4

    invoke-direct {p0, p1, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 87
    iput-boolean p2, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->mAllowCustomView:Z

    .line 88
    return-void
.end method

.method private static removeViewFromParent(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 115
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 118
    :cond_0
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    .line 100
    .local v0, "popupAction":Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;
    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, -0x2

    .line 123
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    .line 125
    .local v4, "popupAction":Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;
    iget-boolean v8, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->mAllowCustomView:Z

    if-eqz v8, :cond_2

    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 126
    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$200(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v8

    if-nez v8, :cond_0

    .line 127
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 128
    .local v1, "infl":Landroid/view/LayoutInflater;
    const v8, 0x7f040044

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 130
    .local v5, "row":Landroid/view/View;
    const v8, 0x7f0e00f4

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 131
    .local v6, "textView":Landroid/widget/TextView;
    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->toString()Ljava/lang/String;

    move-result-object v7

    .line 133
    .local v7, "title":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 134
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    :goto_0
    const v8, 0x1020002

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 141
    .local v0, "holder":Landroid/view/ViewGroup;
    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->removeViewFromParent(Landroid/view/View;)V

    .line 143
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 146
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v8, 0x15

    iput v8, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 148
    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCustomView:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v0, v8, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    # setter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;
    invoke-static {v4, v5}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$202(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;Landroid/view/View;)Landroid/view/View;

    .line 152
    .end local v0    # "holder":Landroid/view/ViewGroup;
    .end local v1    # "infl":Landroid/view/LayoutInflater;
    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "row":Landroid/view/View;
    .end local v6    # "textView":Landroid/widget/TextView;
    .end local v7    # "title":Ljava/lang/String;
    :cond_0
    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mCachedCustomRow:Landroid/view/View;
    invoke-static {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$200(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Landroid/view/View;

    move-result-object v2

    .line 155
    :goto_1
    return-object v2

    .line 136
    .restart local v1    # "infl":Landroid/view/LayoutInflater;
    .restart local v5    # "row":Landroid/view/View;
    .restart local v6    # "textView":Landroid/widget/TextView;
    .restart local v7    # "title":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 154
    .end local v1    # "infl":Landroid/view/LayoutInflater;
    .end local v5    # "row":Landroid/view/View;
    .end local v6    # "textView":Landroid/widget/TextView;
    .end local v7    # "title":Ljava/lang/String;
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 155
    .local v2, "itemView":Landroid/view/View;
    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->mAllowCustomView:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getViewTypeCount()I

    move-result v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mIsEnabled:Z
    invoke-static {v0}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$100(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Z

    move-result v0

    return v0
.end method

.method public onSelect(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;

    # getter for: Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    invoke-static {v0}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;->access$300(Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu$PopupAction;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;->onActionSelected()V

    .line 162
    return-void
.end method
