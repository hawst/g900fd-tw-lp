.class public interface abstract Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
.super Ljava/lang/Object;
.source "PlayCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OptionalActionCallback"
.end annotation


# virtual methods
.method public abstract getActionText(Lcom/google/android/ublib/cardlib/model/Document;)Ljava/lang/String;
.end method

.method public abstract performAction(Lcom/google/android/ublib/cardlib/model/Document;)V
.end method
