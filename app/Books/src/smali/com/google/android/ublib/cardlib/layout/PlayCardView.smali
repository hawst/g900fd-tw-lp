.class public Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.super Landroid/widget/FrameLayout;
.source "PlayCardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;,
        Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;,
        Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    }
.end annotation


# instance fields
.field protected mAccessibilityOverlay:Landroid/view/View;

.field protected mAction:Landroid/widget/TextView;

.field protected mActionTouchArea:Landroid/view/View;

.field protected mDescription:Landroid/widget/TextView;

.field protected mDetailsView:Landroid/view/ViewGroup;

.field private mDocument:Lcom/google/android/ublib/cardlib/model/Document;

.field protected mLoadingIndicator:Landroid/view/View;

.field private mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

.field protected mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

.field protected mPrice:Landroid/widget/TextView;

.field protected mRatingBar:Landroid/widget/RatingBar;

.field protected mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

.field protected mReason2:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

.field protected mReasonSeparator:Landroid/view/View;

.field protected mSubtitle:Landroid/widget/TextView;

.field protected mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

.field protected mThumbnailAspectRatio:F

.field protected mTitle:Landroid/widget/TextView;

.field protected mUnavailableCardOpacity:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 113
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0c0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mUnavailableCardOpacity:F

    .line 114
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p1, "x1"    # Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    return-object p1
.end method

.method private computeAvailabilityOpacity()V
    .locals 2

    .prologue
    .line 309
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/ublib/cardlib/model/Document;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 310
    .local v0, "isAvailable":Z
    :goto_0
    if-eqz v0, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setAlpha(F)V

    .line 311
    return-void

    .line 309
    .end local v0    # "isAvailable":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 310
    .restart local v0    # "isAvailable":Z
    :cond_1
    iget v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mUnavailableCardOpacity:F

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V
    .locals 7
    .param p1, "doc"    # Lcom/google/android/ublib/cardlib/model/Document;
    .param p2, "contextMenuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p3, "imageProvider"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    .param p4, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p5, "bindingsProvider"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;

    .prologue
    .line 178
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;ZLcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 179
    return-void
.end method

.method public bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;ZLcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/ublib/cardlib/model/Document;
    .param p2, "contextMenuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p3, "imageProvider"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    .param p4, "scrolling"    # Z
    .param p5, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p6, "bindingsProvider"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    .line 189
    if-eqz p1, :cond_e

    .line 190
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 191
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 193
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    .line 201
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v3

    .line 202
    .local v3, "subtitle":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    if-nez v3, :cond_1

    const-string v3, ""

    .end local v3    # "subtitle":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    :cond_2
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v5, p1, p3, p4}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->bind(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;Z)V

    .line 208
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    if-eqz v5, :cond_3

    .line 209
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 213
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->updatePrice()V

    .line 215
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mAction:Landroid/widget/TextView;

    if-eqz v5, :cond_4

    .line 216
    invoke-interface {p5, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;->getActionText(Lcom/google/android/ublib/cardlib/model/Document;)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "actionText":Ljava/lang/String;
    if-eqz v0, :cond_c

    .line 218
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mAction:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mAction:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mActionTouchArea:Landroid/view/View;

    if-eqz v5, :cond_4

    .line 222
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mActionTouchArea:Landroid/view/View;

    new-instance v6, Lcom/google/android/ublib/cardlib/layout/PlayCardView$1;

    invoke-direct {v6, p0, p5, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView$1;-><init>(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/model/Document;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mActionTouchArea:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 236
    .end local v0    # "actionText":Ljava/lang/String;
    :cond_4
    :goto_1
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    if-eqz v5, :cond_5

    .line 237
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getReason1()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 238
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getReason1()Ljava/lang/String;

    move-result-object v2

    .line 240
    .local v2, "reason1":Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v2, v6, v7}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->bind(Ljava/lang/CharSequence;Landroid/net/Uri;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V

    .line 245
    .end local v2    # "reason1":Ljava/lang/CharSequence;
    :cond_5
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    if-eqz v5, :cond_6

    .line 249
    :cond_6
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v5, :cond_7

    .line 250
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getDescription()Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "description":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 252
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    .end local v1    # "description":Ljava/lang/String;
    :cond_7
    :goto_2
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    if-eqz v5, :cond_8

    .line 260
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->setVisibility(I)V

    .line 262
    if-eqz p2, :cond_8

    .line 263
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    new-instance v6, Lcom/google/android/ublib/cardlib/layout/PlayCardView$2;

    invoke-direct {v6, p0, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView$2;-><init>(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;)V

    invoke-virtual {v5, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    :cond_8
    if-eqz p6, :cond_9

    .line 274
    invoke-interface {p6, p1, p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;->applyBindings(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView;)V

    .line 277
    :cond_9
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mLoadingIndicator:Landroid/view/View;

    if-eqz v5, :cond_a

    .line 278
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mLoadingIndicator:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 281
    :cond_a
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->computeAvailabilityOpacity()V

    .line 282
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 286
    :goto_3
    return-void

    .line 196
    .restart local v4    # "title":Ljava/lang/String;
    :cond_b
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 232
    .end local v4    # "title":Ljava/lang/String;
    .restart local v0    # "actionText":Ljava/lang/String;
    :cond_c
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mAction:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 255
    .end local v0    # "actionText":Ljava/lang/String;
    .restart local v1    # "description":Ljava/lang/String;
    :cond_d
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 284
    .end local v1    # "description":Ljava/lang/String;
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->bindNoDocument()V

    goto :goto_3
.end method

.method public bindNoDocument()V
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setVisibility(I)V

    .line 351
    return-void
.end method

.method public getDocument()Lcom/google/android/ublib/cardlib/model/Document;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    return-object v0
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 427
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCoverImage()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->hasCoverImage()Z

    move-result v0

    return v0
.end method

.method public maybeLoadCoverImage()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->maybeLoadCoverImage()V

    .line 432
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 138
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;->dismiss()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    .line 142
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 120
    const v0, 0x7f0e00ca

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    .line 121
    const v0, 0x7f0e00cd

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mTitle:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f0e00ce

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mSubtitle:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f0e00ef

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    .line 124
    const v0, 0x7f0e00f0

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    .line 125
    const v0, 0x7f0e00f3

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mReason2:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    .line 126
    const v0, 0x7f0e00f1

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mReasonSeparator:Landroid/view/View;

    .line 127
    const v0, 0x7f0e00e1

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    .line 128
    const v0, 0x7f0e00e0

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mPrice:Landroid/widget/TextView;

    .line 129
    const v0, 0x7f0e00cf

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDescription:Landroid/widget/TextView;

    .line 130
    const v0, 0x7f0e00f2

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mLoadingIndicator:Landroid/view/View;

    .line 131
    const v0, 0x7f0e00d4

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    .line 132
    const v0, 0x7f0e00cc

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    .line 133
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingLeft()I

    move-result v1

    .line 386
    .local v1, "childLeft":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingTop()I

    move-result v3

    .line 387
    .local v3, "thumbnailTop":I
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v1

    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v4, v1, v3, v5, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->layout(IIII)V

    .line 390
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-static {v4}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 391
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 392
    .local v2, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getBottom()I

    move-result v4

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v0, v4, v5

    .line 393
    .local v0, "cardViewTop":I
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v1

    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 397
    .end local v0    # "cardViewTop":I
    .end local v2    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    iget-object v4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v4, v1, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 402
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 356
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 357
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingLeft()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingRight()I

    move-result v8

    add-int v4, v7, v8

    .line 358
    .local v4, "horizontalPadding":I
    sub-int v0, v6, v4

    .line 359
    .local v0, "availableChildWidth":I
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 361
    .local v2, "fullWidthChildWidthSpec":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7, v2, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->measure(II)V

    .line 362
    const-string v7, "PlayCardView"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 363
    const-string v7, "PlayCardView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Thumbnail width="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", height="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_0
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-static {v7}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 369
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v7, v2, p2}, Landroid/view/ViewGroup;->measure(II)V

    .line 370
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 371
    .local v5, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v7

    iget v8, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v7, v8

    iget v8, v5, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v1, v7, v8

    .line 376
    .end local v5    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .local v1, "cardViewHeight":I
    :goto_0
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingTop()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getPaddingBottom()I

    move-result v8

    add-int v3, v7, v8

    .line 379
    .local v3, "height":I
    invoke-virtual {p0, v6, v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setMeasuredDimension(II)V

    .line 380
    return-void

    .line 373
    .end local v1    # "cardViewHeight":I
    .end local v3    # "height":I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "cardViewHeight":I
    goto :goto_0
.end method

.method public setCoverAnimationEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->setCoverAnimationEnabled(Z)V

    .line 154
    return-void
.end method

.method public setOptimizedForScrollingEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->setOptimizedForScrollingEnabled(Z)V

    .line 158
    return-void
.end method

.method public setThumbnailAspectRatio(F)V
    .locals 0
    .param p1, "thumbnailAspectRatio"    # F

    .prologue
    .line 145
    iput p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnailAspectRatio:F

    .line 146
    return-void
.end method

.method public setThumbnailFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V
    .locals 1
    .param p1, "fillStyle"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 150
    return-void
.end method

.method public updateAvailability(Z)V
    .locals 1
    .param p1, "isAvailable"    # Z

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/model/Document;->setIsAvailable(Z)V

    .line 291
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->computeAvailabilityOpacity()V

    .line 293
    :cond_0
    return-void
.end method

.method public updatePrice()V
    .locals 3

    .prologue
    .line 296
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mPrice:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mDocument:Lcom/google/android/ublib/cardlib/model/Document;

    invoke-virtual {v1}, Lcom/google/android/ublib/cardlib/model/Document;->getPrice()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "price":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 299
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mPrice:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mPrice:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    .end local v0    # "price":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 302
    .restart local v0    # "price":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->mPrice:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
