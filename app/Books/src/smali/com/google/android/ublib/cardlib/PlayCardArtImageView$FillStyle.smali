.class public final enum Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;
.super Ljava/lang/Enum;
.source "PlayCardArtImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/PlayCardArtImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FillStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

.field public static final enum FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

.field public static final enum FILL_TO_HEIGHT:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

.field public static final enum FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    const-string v1, "FILL_TO_WIDTH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 45
    new-instance v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    const-string v1, "FILL_TO_HEIGHT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_HEIGHT:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 50
    new-instance v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    const-string v1, "FILL_TO_ASPECT_RATIO"

    invoke-direct {v0, v1, v4}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 35
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_HEIGHT:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->$VALUES:[Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    return-object v0
.end method

.method public static values()[Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->$VALUES:[Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    invoke-virtual {v0}, [Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    return-object v0
.end method
