.class public abstract Lcom/google/android/ublib/widget/AbsWarpListView;
.super Landroid/widget/AdapterView;
.source "AbsWarpListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;,
        Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;,
        Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;,
        Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;,
        Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;",
        ">;"
    }
.end annotation


# static fields
.field private static final sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;


# instance fields
.field private mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

.field private mCenterLockPosition:I

.field private mCenterOffset:I

.field private mCenterPosition:I

.field private mConserveViews:Z

.field private mDragged:Z

.field private mEnterWarpPosition:I

.field private mExitWarpPosition:I

.field private mFirstVisible:I

.field private mFlingCenterPosition:I

.field private mFlingFirstVisible:I

.field private mFlingLastVisible:I

.field private mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

.field private mInvalidateOffset:I

.field private mInvalidatePending:Z

.field private mInvalidateRunnable:Ljava/lang/Runnable;

.field private mLastVisible:I

.field private mLeadingGap:I

.field private mMaxFlingViews:I

.field private mNeedsScrollFilter:Z

.field private mObserver:Landroid/database/DataSetObserver;

.field private final mOnScrollListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollFriction:F

.field private mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

.field private mScrollState:I

.field private final mSearchScroller:Landroid/widget/Scroller;

.field private mSnapMode:I

.field private mStasis:Z

.field private mTrailingGap:I

.field protected mViewAlignment:F

.field private final mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 121
    const-class v0, Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    aput-object v3, v1, v2

    new-instance v2, Lcom/google/android/ublib/widget/AbsWarpListView$1;

    invoke-direct {v2}, Lcom/google/android/ublib/widget/AbsWarpListView$1;-><init>()V

    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    sput-object v0, Lcom/google/android/ublib/widget/AbsWarpListView;->sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v3, 0x7fffffff

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 294
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    .line 131
    sget-object v0, Lcom/google/android/ublib/widget/AbsWarpListView;->sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 132
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mOnScrollListeners:Ljava/util/Set;

    .line 136
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 137
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 140
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I

    .line 141
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I

    .line 142
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingCenterPosition:I

    .line 145
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 146
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    .line 149
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    .line 172
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    .line 175
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    .line 178
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mNeedsScrollFilter:Z

    .line 187
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    .line 188
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    .line 191
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViewAlignment:F

    .line 201
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    .line 203
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    .line 278
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    .line 281
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$2;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mObserver:Landroid/database/DataSetObserver;

    .line 568
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 1305
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$3;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$3;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateRunnable:Ljava/lang/Runnable;

    .line 295
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const v3, 0x7fffffff

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 298
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 131
    sget-object v0, Lcom/google/android/ublib/widget/AbsWarpListView;->sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 132
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mOnScrollListeners:Ljava/util/Set;

    .line 136
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 137
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 140
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I

    .line 141
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I

    .line 142
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingCenterPosition:I

    .line 145
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 146
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    .line 149
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    .line 172
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    .line 175
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    .line 178
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mNeedsScrollFilter:Z

    .line 187
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    .line 188
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    .line 191
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViewAlignment:F

    .line 201
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    .line 203
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    .line 278
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    .line 281
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$2;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mObserver:Landroid/database/DataSetObserver;

    .line 568
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 1305
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$3;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$3;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateRunnable:Ljava/lang/Runnable;

    .line 299
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const v3, 0x7fffffff

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 302
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    sget-object v0, Lcom/google/android/ublib/widget/AbsWarpListView;->sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 132
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mOnScrollListeners:Ljava/util/Set;

    .line 136
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 137
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 140
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I

    .line 141
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I

    .line 142
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingCenterPosition:I

    .line 145
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 146
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    .line 149
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    .line 172
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    .line 175
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    .line 178
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mNeedsScrollFilter:Z

    .line 187
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    .line 188
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    .line 191
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViewAlignment:F

    .line 201
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    .line 203
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    .line 278
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    .line 281
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$2;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mObserver:Landroid/database/DataSetObserver;

    .line 568
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 1305
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$3;

    invoke-direct {v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$3;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateRunnable:Ljava/lang/Runnable;

    .line 303
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/widget/AbsWarpListView;)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/ublib/widget/AbsWarpListView;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->isViewVisible(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/ublib/widget/AbsWarpListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->reload(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/ublib/widget/AbsWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/ublib/widget/AbsWarpListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/android/ublib/widget/AbsWarpListView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSnapMode:I

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterAlignOffset(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeftAlignOffset(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1702(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingCenterPosition:I

    return p1
.end method

.method static synthetic access$1800(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/ublib/widget/AbsWarpListView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->updateViewVisibility()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFlingVelocityForDistance(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFlingDistanceForVelocity(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setScrollState(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->reportScroll()V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/ublib/widget/AbsWarpListView;)Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/google/android/ublib/widget/AbsWarpListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidatePending:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I

    return v0
.end method

.method static synthetic access$2702(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I

    return p1
.end method

.method static synthetic access$2800(Lcom/google/android/ublib/widget/AbsWarpListView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->isVisiblePosition(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 47
    invoke-static {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->nextInvalidateOffset(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->scheduleInvalidate(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/ublib/widget/AbsWarpListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mConserveViews:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/ublib/widget/AbsWarpListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/ublib/widget/AbsWarpListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I

    return p1
.end method

.method private correctOverScroll(I)V
    .locals 6
    .param p1, "scrollAmount"    # I

    .prologue
    .line 876
    const/4 v1, 0x0

    .line 877
    .local v1, "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v0

    .line 878
    .local v0, "count":I
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v2

    .line 880
    .local v2, "negCount":I
    add-int v4, v0, v2

    if-nez v4, :cond_1

    .line 922
    :cond_0
    :goto_0
    return-void

    .line 884
    :cond_1
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    iget v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-le v4, v5, :cond_2

    .line 885
    if-gez p1, :cond_5

    .line 886
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingEdge()Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v1

    .line 887
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollToBegin()V

    .line 894
    :cond_2
    :goto_1
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    add-int/lit8 v5, v0, -0x1

    if-ne v4, v5, :cond_3

    .line 895
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    invoke-virtual {p0, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v3

    .line 896
    .local v3, "view":Landroid/view/View;
    invoke-virtual {p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewPosition(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getTrailingGap()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 897
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getTrailingEdge()Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v1

    .line 898
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollToEnd()V

    .line 903
    .end local v3    # "view":Landroid/view/View;
    :cond_3
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    neg-int v5, v2

    if-ne v4, v5, :cond_4

    .line 904
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    invoke-virtual {p0, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v3

    .line 905
    .restart local v3    # "view":Landroid/view/View;
    invoke-virtual {p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewPosition(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingGap()I

    move-result v5

    if-le v4, v5, :cond_4

    .line 906
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingEdge()Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v1

    .line 907
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollToBegin()V

    .line 911
    .end local v3    # "view":Landroid/view/View;
    :cond_4
    if-eqz v1, :cond_0

    .line 912
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getScrollState()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 919
    :pswitch_0
    int-to-float v4, p1

    invoke-virtual {v1, v4}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    goto :goto_0

    .line 889
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getTrailingEdge()Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v1

    .line 890
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollToEnd()V

    goto :goto_1

    .line 914
    :pswitch_1
    invoke-virtual {v1}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    if-eqz v4, :cond_0

    .line 915
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->mScroller:Landroid/widget/Scroller;
    invoke-static {v4}, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->access$1200(Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;)Landroid/widget/Scroller;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v4}, Landroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    goto/16 :goto_0

    .line 912
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private flingByDistance(I)V
    .locals 2
    .param p1, "distance"    # I

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v1

    add-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 1281
    :goto_0
    return-void

    .line 1279
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 1280
    new-instance v0, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    invoke-direct {v0, p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    iput-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    goto :goto_0
.end method

.method private getCenterAlignOffset(I)I
    .locals 9
    .param p1, "scrollDistance"    # I

    .prologue
    .line 1024
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v7

    div-int/lit8 v6, v7, 0x2

    .line 1025
    .local v6, "viewCenter":I
    const v4, 0x7fffffff

    .local v4, "offsetForward":I
    const v3, 0x7fffffff

    .line 1026
    .local v3, "offsetBack":I
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .local v2, "i":I
    :goto_0
    iget v7, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-gt v2, v7, :cond_1

    .line 1027
    invoke-virtual {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v5

    .line 1028
    .local v5, "view":Landroid/view/View;
    invoke-virtual {p0, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewPosition(Landroid/view/View;)I

    move-result v7

    invoke-virtual {p0, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int v0, v7, v8

    .line 1029
    .local v0, "centerPos":I
    sub-int v1, v0, v6

    .line 1031
    .local v1, "distance":I
    if-lez v1, :cond_0

    .line 1032
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1026
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1034
    :cond_0
    neg-int v7, v1

    invoke-static {v7, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1

    .line 1038
    .end local v0    # "centerPos":I
    .end local v1    # "distance":I
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    mul-int/lit8 v7, v3, 0x3

    if-lt v4, v7, :cond_3

    .line 1039
    neg-int v4, v3

    .line 1046
    .end local v4    # "offsetForward":I
    :cond_2
    :goto_2
    return v4

    .line 1042
    .restart local v4    # "offsetForward":I
    :cond_3
    mul-int/lit8 v7, v4, 0x3

    if-ge v3, v7, :cond_2

    .line 1046
    if-gtz p1, :cond_2

    neg-int v4, v3

    goto :goto_2
.end method

.method private getDistance(IIII)I
    .locals 6
    .param p1, "position1"    # I
    .param p2, "offset1"    # I
    .param p3, "position2"    # I
    .param p4, "offset2"    # I

    .prologue
    const/4 v5, 0x1

    .line 1071
    invoke-static {p1, p3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1072
    .local v3, "minPosition":I
    invoke-static {p1, p3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1073
    .local v2, "maxPosition":I
    const/4 v0, 0x0

    .line 1075
    .local v0, "distance":I
    invoke-direct {p0, v3, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1076
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 1075
    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v1

    goto :goto_0

    .line 1079
    :cond_0
    if-le p1, p3, :cond_1

    .line 1080
    neg-int v0, v0

    .line 1083
    :cond_1
    add-int v4, v0, p2

    sub-int/2addr v4, p4

    return v4
.end method

.method private getFlingDistanceForVelocity(I)I
    .locals 9
    .param p1, "velocity"    # I

    .prologue
    const/4 v1, 0x0

    .line 1087
    const/16 v0, -0x7fff

    const/16 v2, 0x7fff

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 1091
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1092
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v2, v1

    move v3, p1

    move v4, v1

    move v7, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1093
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    return v0
.end method

.method private getFlingVelocityForDistance(I)I
    .locals 6
    .param p1, "distance"    # I

    .prologue
    .line 1104
    const/4 v2, 0x0

    .line 1105
    .local v2, "minVelocity":I
    const/16 v1, 0x7fff

    .line 1106
    .local v1, "maxVelocity":I
    const/4 v3, 0x1

    .line 1108
    .local v3, "sign":I
    if-gez p1, :cond_0

    .line 1109
    neg-int p1, p1

    .line 1110
    const/4 v3, -0x1

    .line 1113
    :cond_0
    :goto_0
    if-le v1, v2, :cond_3

    .line 1114
    add-int v5, v1, v2

    div-int/lit8 v4, v5, 0x2

    .line 1115
    .local v4, "velocity":I
    invoke-direct {p0, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFlingDistanceForVelocity(I)I

    move-result v0

    .line 1117
    .local v0, "curDistance":I
    if-ne v0, p1, :cond_1

    .line 1118
    mul-int v5, v3, v4

    .line 1126
    .end local v0    # "curDistance":I
    .end local v4    # "velocity":I
    :goto_1
    return v5

    .line 1119
    .restart local v0    # "curDistance":I
    .restart local v4    # "velocity":I
    :cond_1
    if-ge v0, p1, :cond_2

    .line 1120
    add-int/lit8 v2, v4, 0x1

    goto :goto_0

    .line 1122
    :cond_2
    add-int/lit8 v1, v4, -0x1

    goto :goto_0

    .line 1126
    .end local v0    # "curDistance":I
    .end local v4    # "velocity":I
    :cond_3
    add-int v5, v2, v1

    mul-int/2addr v5, v3

    div-int/lit8 v5, v5, 0x2

    goto :goto_1
.end method

.method private getLeftAlignOffset(I)I
    .locals 4
    .param p1, "scrollDistance"    # I

    .prologue
    .line 996
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-le v2, v3, :cond_1

    .line 997
    const/4 v0, 0x0

    .line 1016
    :cond_0
    :goto_0
    return v0

    .line 1000
    :cond_1
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewOffset(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingGap()I

    move-result v3

    sub-int v0, v2, v3

    .line 1002
    .local v0, "offsetBack":I
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-eq v2, v3, :cond_0

    .line 1006
    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewOffset(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingGap()I

    move-result v3

    sub-int v1, v2, v3

    .line 1008
    .local v1, "offsetForward":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    if-gt v2, v3, :cond_0

    .line 1012
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    if-le v2, v3, :cond_2

    move v0, v1

    .line 1013
    goto :goto_0

    .line 1016
    :cond_2
    if-lez p1, :cond_3

    .end local v1    # "offsetForward":I
    :goto_1
    move v0, v1

    goto :goto_0

    .restart local v1    # "offsetForward":I
    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method private getScrollListenerArray()[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    .locals 3

    .prologue
    .line 578
    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    if-nez v1, :cond_0

    .line 579
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mOnScrollListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-array v1, v1, [Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    iput-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 582
    :cond_0
    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mOnScrollListeners:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 583
    .local v0, "listeners":[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 584
    return-object v0
.end method

.method private getSpareView(Landroid/support/v4/util/SparseArrayCompat;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 614
    .local p1, "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    invoke-virtual {p1}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    .line 615
    .local v1, "viewCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 616
    invoke-virtual {p1, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    .line 617
    .local v2, "viewInfo":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    invoke-virtual {v2}, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->canSpare()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 618
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->setVisible(Z)V

    .line 619
    invoke-virtual {p1, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    .line 620
    iget-object v3, v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    .line 623
    .end local v2    # "viewInfo":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    :goto_1
    return-object v3

    .line 615
    .restart local v2    # "viewInfo":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 623
    .end local v2    # "viewInfo":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private invalidateGaps()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    .line 399
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSnapMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 401
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLeadingGap:I

    .line 402
    iput v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mTrailingGap:I

    .line 404
    :cond_0
    return-void
.end method

.method private isViewVisible(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 646
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewPosition(Landroid/view/View;)I

    move-result v1

    .line 647
    .local v1, "position":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v0

    .line 648
    .local v0, "maxOffset":I
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v1

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isVisiblePosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1295
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static nextInvalidateOffset(I)I
    .locals 1
    .param p0, "offset"    # I

    .prologue
    .line 1291
    if-ltz p0, :cond_0

    neg-int v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    return v0

    :cond_0
    neg-int v0, p0

    goto :goto_0
.end method

.method private placeView(III)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "offset"    # I
    .param p3, "size"    # I

    .prologue
    .line 722
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v0

    .line 723
    .local v0, "maxOffset":I
    if-ge p2, v0, :cond_0

    add-int v1, p2, p3

    if-lez v1, :cond_0

    .line 724
    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 725
    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 726
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->positionView(Landroid/view/View;I)V

    .line 728
    :cond_0
    return-void
.end method

.method private populate(II)V
    .locals 12
    .param p1, "viewPos"    # I
    .param p2, "viewOff"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x1

    .line 737
    iget-boolean v9, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    if-eqz v9, :cond_1

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 741
    :cond_1
    iget-object v9, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v9}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v1

    .line 742
    .local v1, "count":I
    iget-object v9, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v9}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v4

    .line 743
    .local v4, "negCount":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v3

    .line 745
    .local v3, "maxOffset":I
    add-int v9, v1, v4

    if-eqz v9, :cond_2

    if-nez v3, :cond_3

    .line 746
    :cond_2
    const/4 v9, 0x0

    iput v9, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 747
    iput v10, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    goto :goto_0

    .line 751
    :cond_3
    neg-int v9, v4

    invoke-static {p1, v9}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 752
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 754
    iput v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 755
    neg-int v9, v4

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 756
    const v9, 0x7fffffff

    iput v9, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 758
    div-int/lit8 v7, v3, 0x2

    .line 759
    .local v7, "viewCenter":I
    const v0, 0x7fffffff

    .line 761
    .local v0, "centerDistance":I
    move v5, p2

    .line 765
    .local v5, "off":I
    add-int/lit8 v9, p1, -0x1

    invoke-direct {p0, v9, v10}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v6

    .local v6, "pos":I
    :goto_1
    neg-int v9, v4

    if-lt v6, v9, :cond_4

    .line 766
    invoke-virtual {p0, v6}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v8

    .line 768
    .local v8, "viewSize":I
    add-int v9, v5, v8

    if-gtz v9, :cond_6

    .line 789
    .end local v8    # "viewSize":I
    :cond_4
    move v5, p2

    .line 791
    invoke-direct {p0, p1, v11}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v6

    :goto_2
    if-ge v5, v3, :cond_0

    if-ge v6, v1, :cond_0

    .line 792
    invoke-virtual {p0, v6}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v8

    .line 794
    .restart local v8    # "viewSize":I
    invoke-direct {p0, v6, v5, v8}, Lcom/google/android/ublib/widget/AbsWarpListView;->placeView(III)V

    .line 796
    div-int/lit8 v9, v8, 0x2

    add-int/2addr v9, v5

    sub-int/2addr v9, v7

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 797
    .local v2, "distance":I
    if-ge v2, v0, :cond_5

    .line 798
    iput v6, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 799
    iput v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    .line 800
    move v0, v2

    .line 803
    :cond_5
    add-int/2addr v5, v8

    .line 791
    add-int/lit8 v9, v6, 0x1

    invoke-direct {p0, v9, v11}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v6

    goto :goto_2

    .line 772
    .end local v2    # "distance":I
    :cond_6
    sub-int/2addr v5, v8

    .line 774
    if-le v5, v3, :cond_8

    .line 765
    :cond_7
    :goto_3
    add-int/lit8 v9, v6, -0x1

    invoke-direct {p0, v9, v10}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v6

    goto :goto_1

    .line 778
    :cond_8
    invoke-direct {p0, v6, v5, v8}, Lcom/google/android/ublib/widget/AbsWarpListView;->placeView(III)V

    .line 780
    div-int/lit8 v9, v8, 0x2

    add-int/2addr v9, v5

    sub-int/2addr v9, v7

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 781
    .restart local v2    # "distance":I
    if-ge v2, v0, :cond_7

    .line 782
    iput v6, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 783
    iput v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    .line 784
    move v0, v2

    goto :goto_3
.end method

.method private releaseScrollListenerArray([Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;)V
    .locals 2
    .param p1, "listeners"    # [Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .prologue
    .line 588
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    array-length v0, v0

    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 593
    :goto_0
    return-void

    .line 591
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 592
    iput-object p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollListenerArray:[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    goto :goto_0
.end method

.method private reload(Z)V
    .locals 11
    .param p1, "newAdapter"    # Z

    .prologue
    const/4 v10, 0x0

    .line 459
    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v8}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewTypeCount()I

    move-result v6

    .line 461
    .local v6, "typeCount":I
    if-nez p1, :cond_0

    if-gtz v6, :cond_1

    .line 462
    :cond_0
    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 463
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->removeAllViewsInLayout()V

    .line 464
    iput v10, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    .line 465
    iput v10, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    .line 466
    iput v10, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 467
    const/4 v8, -0x1

    iput v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 468
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 469
    iput v10, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    .line 471
    const/4 v5, 0x0

    .local v5, "type":I
    :goto_0
    if-ge v5, v6, :cond_3

    .line 472
    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    new-instance v9, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v9}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 474
    .end local v5    # "type":I
    :cond_1
    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v8}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->hasStableIds()Z

    move-result v8

    if-nez v8, :cond_3

    .line 475
    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/support/v4/util/SparseArrayCompat;

    .line 476
    .local v7, "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v7}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v8

    if-ge v0, v8, :cond_2

    .line 477
    invoke-virtual {v7, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    .line 478
    .local v2, "info":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    invoke-virtual {v2, v10}, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->setVisible(Z)V

    .line 479
    const-wide/high16 v8, -0x8000000000000000L

    iput-wide v8, v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mViewId:J

    .line 476
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 484
    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    .end local v7    # "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    :cond_3
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->invalidateGaps()V

    .line 486
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v4

    .line 487
    .local v4, "position":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v3

    .line 489
    .local v3, "offset":I
    iget v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    const v9, 0x7fffffff

    if-eq v8, v9, :cond_4

    .line 490
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    .line 491
    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v8}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v8

    neg-int v8, v8

    if-lt v4, v8, :cond_5

    iget-object v8, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v8}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v8

    if-ge v4, v8, :cond_5

    .line 492
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v8

    invoke-virtual {p0, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v3, v8, 0x2

    .line 498
    :cond_4
    :goto_2
    invoke-virtual {p0, v4, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->position(II)V

    .line 499
    return-void

    .line 494
    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private reportScroll()V
    .locals 7

    .prologue
    .line 1389
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getScrollListenerArray()[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    move-result-object v2

    .line 1391
    .local v2, "listeners":[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 1392
    aget-object v1, v2, v0

    .line 1393
    .local v1, "listener":Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    if-nez v1, :cond_1

    .line 1400
    .end local v1    # "listener":Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->releaseScrollListenerArray([Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;)V

    .line 1401
    return-void

    .line 1396
    .restart local v1    # "listener":Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    :cond_1
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    iget v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v5}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v6}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v6

    add-int/2addr v5, v6

    invoke-interface {v1, p0, v3, v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;->onScroll(Lcom/google/android/ublib/widget/AbsWarpListView;III)V

    .line 1391
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private resetAnimation(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 652
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 653
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotationX(F)V

    .line 654
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotationY(F)V

    .line 655
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    .line 656
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 657
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 658
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 659
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 660
    return-void
.end method

.method private scheduleInvalidate(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 1299
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidatePending:Z

    if-nez v0, :cond_0

    .line 1300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidatePending:Z

    .line 1301
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateRunnable:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1303
    :cond_0
    return-void
.end method

.method private scrollBy(IZ)V
    .locals 6
    .param p1, "amount"    # I
    .param p2, "fromUser"    # Z

    .prologue
    .line 826
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v2

    .line 827
    .local v2, "count":I
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v3

    .line 828
    .local v3, "negCount":I
    add-int v4, v2, v3

    if-nez v4, :cond_1

    .line 829
    const/4 v4, 0x0

    iput v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    .line 830
    const/4 v4, -0x1

    iput v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    .line 831
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->updateViewVisibility()V

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 835
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v0

    .line 836
    .local v0, "center":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v4

    sub-int v1, v4, p1

    .line 838
    .local v1, "centerOff":I
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 839
    const v4, 0x7fffffff

    iput v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    .line 842
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V

    .line 843
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->correctOverScroll(I)V

    .line 845
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->updateViewVisibility()V

    .line 847
    if-lez p1, :cond_3

    .line 848
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    add-int/lit8 v5, v2, -0x1

    if-eq v4, v5, :cond_0

    .line 849
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iget v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    add-int/lit8 v5, v5, 0x1

    invoke-interface {v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->preload(I)V

    goto :goto_0

    .line 852
    :cond_3
    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    neg-int v5, v3

    if-eq v4, v5, :cond_0

    .line 853
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iget v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->preload(I)V

    goto :goto_0
.end method

.method private scrollToBegin()V
    .locals 2

    .prologue
    .line 859
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 860
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingGap()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V

    .line 861
    return-void
.end method

.method private scrollToEnd()V
    .locals 4

    .prologue
    .line 864
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 865
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v2}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 866
    .local v0, "lastPos":I
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v1

    .line 867
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getTrailingGap()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, v0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V

    .line 868
    return-void
.end method

.method private setScrollState(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 596
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    if-eq p1, v3, :cond_1

    .line 597
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    .line 599
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getScrollListenerArray()[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    move-result-object v2

    .line 601
    .local v2, "listeners":[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 602
    aget-object v1, v2, v0

    .line 603
    .local v1, "listener":Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    if-nez v1, :cond_2

    .line 609
    .end local v1    # "listener":Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->releaseScrollListenerArray([Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;)V

    .line 611
    .end local v0    # "i":I
    .end local v2    # "listeners":[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    :cond_1
    return-void

    .line 606
    .restart local v0    # "i":I
    .restart local v1    # "listener":Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    .restart local v2    # "listeners":[Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;
    :cond_2
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    invoke-interface {v1, p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;->onScrollStateChanged(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    .line 601
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private updateViewVisibility()V
    .locals 7

    .prologue
    .line 703
    iget-object v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v4/util/SparseArrayCompat;

    .line 704
    .local v4, "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    invoke-virtual {v4}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v3

    .line 705
    .local v3, "viewCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 706
    invoke-virtual {v4, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v2

    .line 707
    .local v2, "pos":I
    invoke-virtual {v4, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    iget v6, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    if-lt v2, v6, :cond_1

    iget v6, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-gt v2, v6, :cond_1

    const/4 v6, 0x1

    :goto_1
    invoke-virtual {v5, v6}, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->setVisible(Z)V

    .line 705
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 707
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 710
    .end local v0    # "i":I
    .end local v2    # "pos":I
    .end local v3    # "viewCount":I
    .end local v4    # "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    :cond_2
    return-void
.end method

.method private warp(II)I
    .locals 1
    .param p1, "pos"    # I
    .param p2, "dir"    # I

    .prologue
    .line 1060
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    if-lt p1, v0, :cond_1

    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    if-gt p1, v0, :cond_1

    .line 1061
    if-ltz p2, :cond_0

    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    add-int/lit8 v0, v0, 0x1

    .line 1063
    :goto_0
    return v0

    .line 1061
    :cond_0
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    move v0, p1

    .line 1063
    goto :goto_0
.end method


# virtual methods
.method public centerPosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 454
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v0

    .line 455
    .local v0, "size":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->position(II)V

    .line 456
    return-void
.end method

.method protected drag(I)V
    .locals 2
    .param p1, "amount"    # I

    .prologue
    const/4 v1, 0x1

    .line 809
    iput-boolean v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    .line 810
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    if-nez v0, :cond_0

    .line 811
    invoke-direct {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setScrollState(I)V

    .line 813
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(I)V

    .line 814
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->reportScroll()V

    .line 815
    return-void
.end method

.method protected fling(I)V
    .locals 1
    .param p1, "velocity"    # I

    .prologue
    .line 1284
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFlingDistanceForVelocity(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingByDistance(I)V

    .line 1285
    return-void
.end method

.method public flingToCenter(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 1161
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v1

    .line 1162
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v2

    invoke-virtual {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x2

    .line 1163
    .local v0, "offset":I
    invoke-virtual {p0, p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingToPosition(II)V

    .line 1164
    return-void
.end method

.method public flingToPosition(II)V
    .locals 6
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    const/4 v5, 0x0

    .line 1135
    invoke-virtual {p0, v5}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 1137
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v1

    .line 1138
    .local v1, "centerPosition":I
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v0

    .line 1140
    .local v0, "centerOffset":I
    sub-int v3, p1, v1

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    if-le v3, v4, :cond_0

    .line 1141
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    .line 1142
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    .line 1151
    :goto_0
    invoke-direct {p0, v1, v0, p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->getDistance(IIII)I

    move-result v2

    .line 1153
    .local v2, "distance":I
    invoke-direct {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->flingByDistance(I)V

    .line 1154
    return-void

    .line 1143
    .end local v2    # "distance":I
    :cond_0
    sub-int v3, v1, p1

    iget v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    if-le v3, v4, :cond_1

    .line 1144
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p1

    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    .line 1145
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mMaxFlingViews:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    goto :goto_0

    .line 1147
    :cond_1
    iput v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mEnterWarpPosition:I

    .line 1148
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mExitWarpPosition:I

    goto :goto_0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    sget-object v1, Lcom/google/android/ublib/widget/AbsWarpListView;->sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    goto :goto_0
.end method

.method public getCenterOffset()I
    .locals 1

    .prologue
    .line 973
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterOffset:I

    return v0
.end method

.method public getCenterPosition()I
    .locals 1

    .prologue
    .line 969
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    return v0
.end method

.method public getFinalCenterPosition()I
    .locals 2

    .prologue
    .line 960
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingCenterPosition:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I

    goto :goto_0
.end method

.method public getFinalFirstVisiblePosition()I
    .locals 2

    .prologue
    .line 948
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingFirstVisible:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    goto :goto_0
.end method

.method public getFinalLastVisiblePosition()I
    .locals 2

    .prologue
    .line 954
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingLastVisible:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    goto :goto_0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 938
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 943
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    return v0
.end method

.method protected abstract getLeadingEdge()Landroid/support/v4/widget/EdgeEffectCompat;
.end method

.method public getLeadingGap()I
    .locals 5

    .prologue
    .line 359
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLeadingGap:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v2

    .line 361
    .local v2, "maxOffset":I
    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v3

    neg-int v0, v3

    .line 362
    .local v0, "firstPos":I
    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 363
    .local v1, "lastPos":I
    if-le v0, v1, :cond_1

    const/4 v3, 0x0

    :goto_0
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLeadingGap:I

    .line 366
    .end local v0    # "firstPos":I
    .end local v1    # "lastPos":I
    .end local v2    # "maxOffset":I
    :cond_0
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLeadingGap:I

    return v3

    .line 363
    .restart local v0    # "firstPos":I
    .restart local v1    # "lastPos":I
    .restart local v2    # "maxOffset":I
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v3

    sub-int v3, v2, v3

    div-int/lit8 v3, v3, 0x2

    goto :goto_0
.end method

.method protected abstract getMaxOffset()I
.end method

.method public getNextVisiblePosition(I)I
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 1050
    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->warp(II)I

    move-result v0

    return v0
.end method

.method public getScrollFriction()F
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    return v0
.end method

.method public getScrollProgress()F
    .locals 1

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->getProgress()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollState()I
    .locals 1

    .prologue
    .line 565
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1337
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract getTrailingEdge()Landroid/support/v4/widget/EdgeEffectCompat;
.end method

.method public getTrailingGap()I
    .locals 5

    .prologue
    .line 370
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mTrailingGap:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getMaxOffset()I

    move-result v2

    .line 372
    .local v2, "maxOffset":I
    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v3

    neg-int v0, v3

    .line 373
    .local v0, "firstPos":I
    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 374
    .local v1, "lastPos":I
    if-le v0, v1, :cond_1

    const/4 v3, 0x0

    :goto_0
    iput v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mTrailingGap:I

    .line 376
    .end local v0    # "firstPos":I
    .end local v1    # "lastPos":I
    .end local v2    # "maxOffset":I
    :cond_0
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mTrailingGap:I

    return v3

    .line 374
    .restart local v0    # "firstPos":I
    .restart local v1    # "lastPos":I
    .restart local v2    # "maxOffset":I
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSizeAt(I)I

    move-result v3

    sub-int v3, v2, v3

    div-int/lit8 v3, v3, 0x2

    goto :goto_0
.end method

.method public getView(I)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 667
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewTypeCount()I

    move-result v4

    if-nez v4, :cond_0

    .line 699
    :goto_0
    return-object v1

    .line 670
    :cond_0
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v5, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemViewType(I)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/util/SparseArrayCompat;

    .line 671
    .local v3, "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    invoke-virtual {v3, p1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    .line 672
    .local v2, "viewInfo":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    if-nez v2, :cond_6

    .line 673
    .local v1, "view":Landroid/view/View;
    :goto_1
    if-eqz v1, :cond_1

    iget-wide v4, v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mViewId:J

    iget-object v6, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v6, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemId(I)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 674
    :cond_1
    if-nez v1, :cond_7

    invoke-direct {p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getSpareView(Landroid/support/v4/util/SparseArrayCompat;)Landroid/view/View;

    move-result-object v0

    .line 676
    .local v0, "spareView":Landroid/view/View;
    :goto_2
    if-eqz v0, :cond_2

    .line 677
    invoke-direct {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->resetAnimation(Landroid/view/View;)V

    .line 680
    :cond_2
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4, p1, v0, p0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 682
    if-eq v1, v0, :cond_4

    .line 683
    if-eqz v0, :cond_3

    .line 684
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->removeViewInLayout(Landroid/view/View;)V

    .line 686
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getChildCount()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0, v1, v4, v5, v6}, Lcom/google/android/ublib/widget/AbsWarpListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 689
    :cond_4
    new-instance v4, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    invoke-direct {v4, p0, v1, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;Landroid/view/View;I)V

    invoke-virtual {v3, p1, v4}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 690
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->layoutView(Landroid/view/View;)V

    .line 691
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 693
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 694
    new-instance v4, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;

    invoke-direct {v4, p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 698
    .end local v0    # "spareView":Landroid/view/View;
    :cond_5
    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4, v1, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->transformView(Landroid/view/View;I)V

    goto :goto_0

    .line 672
    .end local v1    # "view":Landroid/view/View;
    :cond_6
    iget-object v1, v2, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    goto :goto_1

    .restart local v1    # "view":Landroid/view/View;
    :cond_7
    move-object v0, v1

    .line 674
    goto :goto_2
.end method

.method public getViewOffset(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 977
    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-gt p1, v1, :cond_0

    .line 978
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v0

    .line 979
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewPosition(Landroid/view/View;)I

    move-result v1

    .line 981
    .end local v0    # "view":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingGap()I

    move-result v1

    goto :goto_0
.end method

.method public abstract getViewPosition(Landroid/view/View;)I
.end method

.method public abstract getViewSize(Landroid/view/View;)I
.end method

.method public getViewSizeAt(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 713
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v2, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewSize(I)I

    move-result v1

    .line 714
    .local v1, "viewSize":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 715
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v0

    .line 716
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewSize(Landroid/view/View;)I

    move-result v1

    .line 718
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    return v1
.end method

.method public getWeakOnScrollListeners()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mOnScrollListeners:Ljava/util/Set;

    return-object v0
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1346
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1360
    :goto_0
    :pswitch_0
    return-void

    .line 1348
    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    goto :goto_0

    .line 1352
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeadingEdge()Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 1353
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getTrailingEdge()Landroid/support/v4/widget/EdgeEffectCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 1354
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1355
    invoke-direct {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->setScrollState(I)V

    .line 1357
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    goto :goto_0

    .line 1346
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1330
    iput v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I

    .line 1331
    invoke-direct {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->scheduleInvalidate(I)V

    .line 1332
    return-void
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 965
    iget v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract layoutView(Landroid/view/View;)V
.end method

.method public needsScrollFilter()Z
    .locals 1

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mNeedsScrollFilter:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1405
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 1406
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->setScrollState(I)V

    .line 1407
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    .line 1408
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 926
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 927
    if-eqz p1, :cond_1

    .line 928
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 929
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 930
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->layoutView(Landroid/view/View;)V

    .line 928
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 932
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->reload(Z)V

    .line 934
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public position(II)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    const/high16 v4, -0x80000000

    const/4 v3, 0x0

    .line 411
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V

    .line 413
    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    if-nez v1, :cond_0

    .line 414
    invoke-direct {p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->correctOverScroll(I)V

    .line 416
    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSnapMode:I

    packed-switch v1, :pswitch_data_0

    .line 446
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->updateViewVisibility()V

    .line 447
    invoke-direct {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->reportScroll()V

    .line 448
    return-void

    .line 418
    :pswitch_0
    invoke-direct {p0, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterAlignOffset(I)I

    move-result v0

    .line 419
    .local v0, "correctOff":I
    if-eqz v0, :cond_0

    .line 420
    invoke-direct {p0, v0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(IZ)V

    goto :goto_0

    .line 425
    .end local v0    # "correctOff":I
    :pswitch_1
    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    iget v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-le v1, v2, :cond_0

    .line 435
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLeftAlignOffset(I)I

    move-result v0

    .line 437
    .restart local v0    # "correctOff":I
    if-eqz v0, :cond_0

    .line 438
    invoke-direct {p0, v0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(IZ)V

    goto :goto_0

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected abstract positionView(Landroid/view/View;I)V
.end method

.method public rebuildView(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 1372
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1386
    :cond_0
    :goto_0
    return-void

    .line 1375
    :cond_1
    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v4, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemViewType(I)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/util/SparseArrayCompat;

    .line 1376
    .local v2, "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    invoke-virtual {v2, p1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    .line 1377
    .local v0, "viewInfo":Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;
    if-eqz v0, :cond_0

    .line 1378
    iget-object v3, v0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getViewPosition(Landroid/view/View;)I

    move-result v1

    .line 1379
    .local v1, "viewPosition":I
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, v0, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mViewId:J

    .line 1380
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->setVisible(Z)V

    .line 1381
    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollState:I

    if-nez v3, :cond_0

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFirstVisible:I

    if-lt p1, v3, :cond_0

    iget v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLastVisible:I

    if-gt p1, v3, :cond_0

    .line 1383
    invoke-virtual {p0, p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->position(II)V

    goto :goto_0
.end method

.method protected registerDrag()V
    .locals 1

    .prologue
    .line 1363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mDragged:Z

    .line 1364
    return-void
.end method

.method public requestLayout()V
    .locals 0

    .prologue
    .line 1417
    return-void
.end method

.method public scrollBy(I)V
    .locals 1
    .param p1, "amount"    # I

    .prologue
    .line 822
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->scrollBy(IZ)V

    .line 823
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 47
    check-cast p1, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    return-void
.end method

.method public setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .prologue
    .line 503
    if-nez p1, :cond_0

    .line 504
    sget-object p1, Lcom/google/android/ublib/widget/AbsWarpListView;->sNullAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    if-ne v0, p1, :cond_1

    .line 516
    :goto_0
    return-void

    .line 511
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling()V

    .line 512
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 513
    iput-object p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 514
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 515
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->reload(Z)V

    goto :goto_0
.end method

.method public setCenterLockPosition(I)V
    .locals 0
    .param p1, "centerLockPosition"    # I

    .prologue
    .line 383
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterLockPosition:I

    .line 384
    return-void
.end method

.method public setGaps(II)V
    .locals 0
    .param p1, "leadingGap"    # I
    .param p2, "trailingGap"    # I

    .prologue
    .line 354
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mLeadingGap:I

    .line 355
    iput p2, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mTrailingGap:I

    .line 356
    return-void
.end method

.method public setNeedsScrollFilter(Z)V
    .locals 0
    .param p1, "needsScrollFilter"    # Z

    .prologue
    .line 333
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mNeedsScrollFilter:Z

    .line 334
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 7
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v5

    if-ne p1, v5, :cond_1

    .line 536
    :cond_0
    return-void

    .line 523
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 525
    iget-object v5, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v4/util/SparseArrayCompat;

    .line 526
    .local v4, "views":Landroid/support/v4/util/SparseArrayCompat;, "Landroid/support/v4/util/SparseArrayCompat<Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;>;"
    invoke-virtual {v4}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v3

    .line 527
    .local v3, "viewCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 528
    invoke-virtual {v4, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;

    iget-object v2, v5, Lcom/google/android/ublib/widget/AbsWarpListView$ViewInfo;->mView:Landroid/view/View;

    .line 529
    .local v2, "view":Landroid/view/View;
    if-nez p1, :cond_3

    .line 530
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 527
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 532
    :cond_3
    new-instance v5, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;

    invoke-virtual {v4, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v6

    invoke-direct {v5, p0, v6}, Lcom/google/android/ublib/widget/AbsWarpListView$ItemClickListener;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public setScrollFriction(F)V
    .locals 2
    .param p1, "friction"    # F

    .prologue
    .line 310
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    .line 311
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSearchScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mScrollFriction:F

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFriction(F)V

    .line 312
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 1342
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->centerPosition(I)V

    .line 1343
    return-void
.end method

.method public setSnapMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 390
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mSnapMode:I

    .line 391
    return-void
.end method

.method public setStasis(Z)V
    .locals 2
    .param p1, "isStasis"    # Z

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    if-ne p1, v0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    .line 345
    iget-boolean v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mStasis:Z

    if-nez v0, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterOffset()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->populate(II)V

    goto :goto_0
.end method

.method public setViewAlignment(F)V
    .locals 0
    .param p1, "position"    # F

    .prologue
    .line 323
    iput p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mViewAlignment:F

    .line 324
    return-void
.end method

.method public stopFling()V
    .locals 1

    .prologue
    .line 1272
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->stopFling(Z)V

    .line 1273
    return-void
.end method

.method protected stopFling(Z)V
    .locals 2
    .param p1, "toNotify"    # Z

    .prologue
    .line 1264
    iget-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    if-eqz v1, :cond_0

    .line 1265
    iget-object v0, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    .line 1266
    .local v0, "runnable":Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/ublib/widget/AbsWarpListView;->mFlingRunnable:Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;

    .line 1267
    invoke-virtual {v0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;->kill(Z)V

    .line 1269
    .end local v0    # "runnable":Lcom/google/android/ublib/widget/AbsWarpListView$FlingRunnable;
    :cond_0
    return-void
.end method
