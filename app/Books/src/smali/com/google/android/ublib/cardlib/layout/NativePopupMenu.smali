.class public Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;
.super Ljava/lang/Object;
.source "NativePopupMenu.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final mAnchorView:Landroid/view/View;

.field private final mPopupMenu:Landroid/widget/PopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mAnchorView:Landroid/view/View;

    .line 35
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-direct {v0, p1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mAnchorView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "onActionSelectedListener"    # Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;

    .prologue
    const/4 v2, 0x0

    .line 41
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v2, v2, v2, p1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 42
    .local v0, "menuItem":Landroid/view/MenuItem;
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 43
    new-instance v1, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu$1;

    invoke-direct {v1, p0, p3}, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu$1;-><init>(Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 52
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 70
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mAnchorView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 57
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v1, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu$2;

    invoke-direct {v1, p0}, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu$2;-><init>(Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/NativePopupMenu;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 65
    return-void
.end method
