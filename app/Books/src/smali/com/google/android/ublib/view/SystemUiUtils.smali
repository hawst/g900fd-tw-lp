.class public Lcom/google/android/ublib/view/SystemUiUtils;
.super Ljava/lang/Object;
.source "SystemUiUtils.java"


# static fields
.field private static sImmersiveModeDisabled:Z


# direct methods
.method private static createUserManager(Landroid/content/Context;)Landroid/os/UserManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 77
    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    return-object v0
.end method

.method public static makeSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;
    .locals 8
    .param p0, "view"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 31
    const/4 v2, 0x1

    .line 34
    .local v2, "immersiveMode":Z
    :try_start_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-ne v4, v5, :cond_0

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/ublib/view/SystemUiUtils;->createUserManager(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v3

    .line 45
    .local v3, "um":Landroid/os/UserManager;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 46
    .local v1, "handle":Landroid/os/UserHandle;
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v3, v1}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 47
    const/4 v2, 0x0

    .line 48
    sget-boolean v4, Lcom/google/android/ublib/view/SystemUiUtils;->sImmersiveModeDisabled:Z

    if-nez v4, :cond_0

    const-string v4, "SystemUiUtils"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49
    const-string v4, "SystemUiUtils"

    const-string v5, "Disabling immersive mode for KitKat secondary user"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const/4 v4, 0x1

    sput-boolean v4, Lcom/google/android/ublib/view/SystemUiUtils;->sImmersiveModeDisabled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v3    # "um":Landroid/os/UserManager;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 64
    new-instance v4, Lcom/google/android/ublib/view/KeyLimePieSystemUi;

    invoke-direct {v4, p0}, Lcom/google/android/ublib/view/KeyLimePieSystemUi;-><init>(Landroid/view/View;)V

    .line 71
    .end local v2    # "immersiveMode":Z
    :goto_1
    return-object v4

    .line 54
    .restart local v2    # "immersiveMode":Z
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SystemUiUtils"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 58
    const-string v4, "SystemUiUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to determine user type in KitKat: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 66
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Lcom/google/android/ublib/view/JellybeanSystemUi;

    invoke-direct {v4, p0}, Lcom/google/android/ublib/view/JellybeanSystemUi;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 68
    .end local v2    # "immersiveMode":Z
    :cond_2
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 69
    new-instance v4, Lcom/google/android/ublib/view/JellybeanSystemUi;

    invoke-direct {v4, p0}, Lcom/google/android/ublib/view/JellybeanSystemUi;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 71
    :cond_3
    new-instance v4, Lcom/google/android/ublib/view/HoneycombSystemUi;

    invoke-direct {v4, p0}, Lcom/google/android/ublib/view/HoneycombSystemUi;-><init>(Landroid/view/View;)V

    goto :goto_1
.end method
