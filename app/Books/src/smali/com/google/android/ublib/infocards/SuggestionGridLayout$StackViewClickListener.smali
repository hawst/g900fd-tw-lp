.class Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/infocards/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StackViewClickListener"
.end annotation


# instance fields
.field private final mDelegateListener:Landroid/view/View$OnClickListener;

.field private final mStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "stack"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .param p2, "delegateListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1583
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 1584
    iput-object p2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mDelegateListener:Landroid/view/View$OnClickListener;

    .line 1585
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;Landroid/view/View$OnClickListener;Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .param p2, "x1"    # Landroid/view/View$OnClickListener;
    .param p3, "x2"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;

    .prologue
    .line 1578
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isTopCard(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1592
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    iget-boolean v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v0, :cond_1

    .line 1593
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    .line 1598
    :cond_0
    :goto_0
    return-void

    .line 1594
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mDelegateListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 1595
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;->mDelegateListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
