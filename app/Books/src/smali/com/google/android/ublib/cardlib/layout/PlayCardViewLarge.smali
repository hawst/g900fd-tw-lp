.class public Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.source "PlayCardViewLarge.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 25
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 27
    .local v0, "availableWidth":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;->getPaddingLeft()I

    move-result v1

    .line 28
    .local v1, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;->getPaddingRight()I

    move-result v2

    .line 30
    .local v2, "paddingRight":I
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 32
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v6, v0, v1

    sub-int/2addr v6, v2

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v6, v7

    .line 34
    .local v5, "thumbnailWidth":I
    iget v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;->mThumbnailAspectRatio:F

    int-to-float v7, v5

    mul-float/2addr v6, v7

    float-to-int v3, v6

    .line 35
    .local v3, "thumbnailHeight":I
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iput v3, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 37
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewLarge;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6, v5, v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->setThumbnailMetrics(II)V

    .line 39
    invoke-super {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onMeasure(II)V

    .line 40
    return-void
.end method
