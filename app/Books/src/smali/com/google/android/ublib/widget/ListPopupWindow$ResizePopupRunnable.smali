.class Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResizePopupRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/widget/ListPopupWindow;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/widget/ListPopupWindow;)V
    .locals 0

    .prologue
    .line 1387
    iput-object p1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/widget/ListPopupWindow;Lcom/google/android/ublib/widget/ListPopupWindow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/widget/ListPopupWindow;
    .param p2, "x1"    # Lcom/google/android/ublib/widget/ListPopupWindow$1;

    .prologue
    .line 1387
    invoke-direct {p0, p1}, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;-><init>(Lcom/google/android/ublib/widget/ListPopupWindow;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mDropDownList:Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$600(Lcom/google/android/ublib/widget/ListPopupWindow;)Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mDropDownList:Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$600(Lcom/google/android/ublib/widget/ListPopupWindow;)Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mDropDownList:Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;
    invoke-static {v1}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$600(Lcom/google/android/ublib/widget/ListPopupWindow;)Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getChildCount()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mDropDownList:Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$600(Lcom/google/android/ublib/widget/ListPopupWindow;)Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow$DropDownListView;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    iget v1, v1, Lcom/google/android/ublib/widget/ListPopupWindow;->mListItemExpandMaximum:I

    if-gt v0, v1, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    # getter for: Lcom/google/android/ublib/widget/ListPopupWindow;->mPopup:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->access$700(Lcom/google/android/ublib/widget/ListPopupWindow;)Landroid/widget/PopupWindow;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 1392
    iget-object v0, p0, Lcom/google/android/ublib/widget/ListPopupWindow$ResizePopupRunnable;->this$0:Lcom/google/android/ublib/widget/ListPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/ListPopupWindow;->show()V

    .line 1394
    :cond_0
    return-void
.end method
