.class public Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
.super Ljava/lang/Object;
.source "PlayCardHeap.java"


# instance fields
.field private final mHeap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;->mHeap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getCard(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .locals 3
    .param p1, "cardMetadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Utils;->ensureOnMainThread()V

    .line 71
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;->mHeap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 72
    .local v0, "available":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/cardlib/layout/PlayCardView;>;"
    if-nez v0, :cond_0

    .line 73
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;->mHeap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v1

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 79
    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    goto :goto_0
.end method
