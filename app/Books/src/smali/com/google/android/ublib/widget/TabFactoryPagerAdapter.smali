.class public Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "TabFactoryPagerAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentPrimary:I

.field private final mPageTitles:[Ljava/lang/String;

.field private final mTabFactories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/widget/TabFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 5
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/widget/TabFactory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "factories":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/widget/TabFactory;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 32
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mCurrentPrimary:I

    .line 36
    iput-object p1, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mTabFactories:Ljava/util/List;

    .line 37
    iput-object p2, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mContext:Landroid/content/Context;

    .line 38
    iget-object v3, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mTabFactories:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 39
    .local v1, "numTabs":I
    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mPageTitles:[Ljava/lang/String;

    .line 40
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 41
    .local v2, "resources":Landroid/content/res/Resources;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 42
    iget-object v4, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mPageTitles:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mTabFactories:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/ublib/widget/TabFactory;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/TabFactory;->getTitleResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v0

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 93
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/View;
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 113
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mTabFactories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mPageTitles:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 58
    iget-object v1, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mTabFactories:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/widget/TabFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/ublib/widget/TabFactory;->createView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, "view":Landroid/view/View;
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    .line 60
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 53
    if-ne p2, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1, "state"    # Landroid/os/Parcelable;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 104
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/PagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 72
    iget v3, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mCurrentPrimary:I

    if-eq p2, v3, :cond_1

    .line 73
    iput p2, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mCurrentPrimary:I

    .line 74
    iget-object v3, p0, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;->mTabFactories:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/widget/TabFactory;

    .line 75
    .local v2, "factory":Lcom/google/android/ublib/widget/TabFactory;
    instance-of v3, p3, Landroid/view/View;

    if-eqz v3, :cond_2

    .line 76
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {v2, p3}, Lcom/google/android/ublib/widget/TabFactory;->onGainingPrimaryStatus(Landroid/view/View;)V

    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 85
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v2}, Lcom/google/android/ublib/widget/TabFactory;->getTitleResourceId()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "description":Ljava/lang/String;
    const v3, 0x7f0f0008

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, p1, v3}, Lcom/google/android/ublib/utils/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 89
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "description":Ljava/lang/String;
    .end local v2    # "factory":Lcom/google/android/ublib/widget/TabFactory;
    :cond_1
    return-void

    .line 78
    .restart local v2    # "factory":Lcom/google/android/ublib/widget/TabFactory;
    .restart local p3    # "object":Ljava/lang/Object;
    :cond_2
    const-string v3, "TabFactoryPA"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    const-string v3, "TabFactoryPA"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t find view set as primary: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 109
    return-void
.end method
