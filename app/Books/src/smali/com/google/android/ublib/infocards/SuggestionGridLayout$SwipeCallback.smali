.class Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/ublib/infocards/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/infocards/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwipeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)V
    .locals 0

    .prologue
    .line 803
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p2, "x1"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;

    .prologue
    .line 803
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)V

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 860
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    .line 861
    const/4 v1, 0x0

    .line 868
    :goto_0
    return v1

    .line 864
    :cond_0
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$300(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 865
    .local v0, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    if-ne v1, p1, :cond_1

    .line 866
    const/4 v1, 0x1

    goto :goto_0

    .line 868
    :cond_1
    invoke-interface {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    goto :goto_0
.end method

.method dragEnd(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 955
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 956
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->enableAnimation(Z)V

    .line 957
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setVisibility(I)V

    .line 958
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->enableAnimation(Z)V

    .line 959
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 960
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 961
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v1, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$402(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 968
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # setter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v1, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$102(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Z)Z

    .line 971
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->invalidate()V

    .line 972
    return-void

    .line 963
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$300(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 964
    .local v0, "gi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    if-eqz v0, :cond_0

    .line 965
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x0

    const/high16 v11, 0x3f000000    # 0.5f

    .line 810
    iget-object v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v10}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$100(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-object v5, v9

    .line 854
    :cond_0
    :goto_0
    return-object v5

    .line 812
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    add-float/2addr v10, v11

    float-to-int v7, v10

    .line 813
    .local v7, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    add-float/2addr v10, v11

    float-to-int v8, v10

    .line 815
    .local v8, "y":I
    iget-object v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v10}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 816
    .local v0, "count":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_4

    .line 817
    iget-object v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v10, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 818
    .local v5, "v":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_3

    .line 816
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 821
    :cond_3
    iget-object v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$200(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 822
    iget-object v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$200(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 823
    iget-object v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-static {v10, v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$300(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v2

    .line 824
    .local v2, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    if-eqz v2, :cond_2

    .line 826
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v10

    iget-boolean v10, v10, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    if-eqz v10, :cond_2

    .line 830
    instance-of v9, v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;

    if-nez v9, :cond_0

    move-object v4, v2

    .line 833
    check-cast v4, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 834
    .local v4, "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-virtual {v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isExpanded()Z

    move-result v9

    if-nez v9, :cond_0

    .line 840
    invoke-virtual {v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getViews()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 841
    .local v6, "viewIndex":I
    invoke-virtual {v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getViews()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v3

    .line 842
    .local v3, "numViews":I
    add-int/lit8 v9, v3, -0x1

    if-eq v6, v9, :cond_0

    .line 846
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # setter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v9, v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$402(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 847
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setupDragImage(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)V
    invoke-static {v9, v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$500(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)V

    .line 848
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v5

    goto :goto_0

    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v3    # "numViews":I
    .end local v4    # "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .end local v5    # "v":Landroid/view/View;
    .end local v6    # "viewIndex":I
    :cond_4
    move-object v5, v9

    .line 854
    goto :goto_0
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 874
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 877
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 879
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setVisibility(I)V

    .line 880
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 885
    :goto_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # setter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v0, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$102(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Z)Z

    .line 886
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->invalidate()V

    .line 887
    return-void

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-static {v1, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$300(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 883
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 893
    const/4 v1, 0x0

    .line 894
    .local v1, "dismissTrail":Landroid/view/View;
    const/4 v3, 0x0

    .line 896
    .local v3, "gridItem":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v5

    if-ne p1, v5, :cond_3

    .line 897
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 899
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getViews()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 900
    .local v2, "dismissedChildren":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 902
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    move-result-object v3

    .line 904
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-static {v5, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$402(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 906
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 907
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 908
    .local v0, "child":Landroid/view/View;
    invoke-interface {v3, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    .line 909
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v5, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$801(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V

    .line 906
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 913
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$900(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 914
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$900(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    invoke-interface {v5, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;->onViewsDismissed(Ljava/util/ArrayList;)V

    .line 930
    .end local v2    # "dismissedChildren":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4    # "i":I
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z
    invoke-static {v5, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$102(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Z)Z

    .line 934
    invoke-interface {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 935
    invoke-interface {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->shouldShowDismissTrail()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 936
    invoke-interface {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->showDismissTrail()V

    .line 942
    :cond_2
    :goto_2
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->invalidate()V

    .line 943
    return-void

    .line 917
    :cond_3
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-static {v5, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$300(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v3

    .line 918
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 920
    invoke-interface {v3, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    .line 921
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # invokes: Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    invoke-static {v5, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1001(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V

    .line 924
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$900(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 925
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 926
    .restart local v2    # "dismissedChildren":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$900(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    move-result-object v5

    invoke-interface {v5, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;->onViewsDismissed(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 938
    .end local v2    # "dismissedChildren":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :cond_4
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->this$0:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    # getter for: Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->access$1100(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 947
    return-void
.end method

.method public onSnapBackCompleted(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 951
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;->dragEnd(Landroid/view/View;)V

    .line 952
    return-void
.end method
