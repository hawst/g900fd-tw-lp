.class public Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/infocards/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;
    }
.end annotation


# instance fields
.field public animationIndex:I

.field public appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

.field public canDismiss:Z

.field public canDrag:Z

.field public column:I

.field public disappearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

.field public noPadding:Z

.field public spanAllColumns:Z


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "column"    # I

    .prologue
    const/4 v0, 0x1

    .line 678
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 655
    iput-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 656
    iput-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 657
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 679
    iput p3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    .line 680
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 683
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 655
    iput-boolean v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 656
    iput-boolean v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 657
    iput-boolean v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 685
    sget-object v1, Lcom/google/android/ublib/R$styleable;->SuggestionGridLayout_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 687
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    .line 688
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 690
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 692
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 696
    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->getAnimationType(Landroid/content/res/TypedArray;ILcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 701
    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->getAnimationType(Landroid/content/res/TypedArray;ILcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 704
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 705
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1, "other"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v3, 0x1

    .line 663
    const/4 v1, -0x1

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 655
    iput-boolean v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 656
    iput-boolean v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 657
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 664
    instance-of v1, p1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 665
    check-cast v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 666
    .local v0, "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iget v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    iput v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    .line 667
    iget-boolean v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    .line 668
    iget-boolean v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 669
    iget-boolean v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 670
    iget-boolean v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 671
    iget-object v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 672
    iget-object v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 673
    iget v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->animationIndex:I

    iput v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->animationIndex:I

    .line 675
    .end local v0    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private getAnimationType(Landroid/content/res/TypedArray;ILcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;
    .locals 2
    .param p1, "a"    # Landroid/content/res/TypedArray;
    .param p2, "id"    # I
    .param p3, "defaultType"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .prologue
    .line 708
    const/4 v1, -0x1

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 709
    .local v0, "index":I
    if-ltz v0, :cond_0

    invoke-static {}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->values()[Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    move-result-object v1

    aget-object p3, v1, v0

    .end local p3    # "defaultType":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;
    :cond_0
    return-object p3
.end method
