.class public interface abstract Lcom/google/android/ublib/view/SystemUi;
.super Ljava/lang/Object;
.source "SystemUi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    }
.end annotation


# virtual methods
.method public abstract copyFlagsTo(Lcom/google/android/ublib/view/SystemUi;)V
.end method

.method public abstract setOnSystemUiVisibilityChangeListener(Lcom/google/android/ublib/view/SystemUi$VisibilityListener;)V
.end method

.method public abstract setSystemUiVisible(Z)V
.end method

.method public abstract setViewFullscreen(Z)V
.end method
