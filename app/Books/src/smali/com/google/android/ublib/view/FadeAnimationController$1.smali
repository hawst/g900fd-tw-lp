.class Lcom/google/android/ublib/view/FadeAnimationController$1;
.super Ljava/lang/Object;
.source "FadeAnimationController.java"

# interfaces
.implements Lcom/google/android/ublib/view/AnimatorCompat$AnimatorListenerCompat;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZILcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/view/FadeAnimationController;

.field final synthetic val$listener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/google/android/ublib/view/FadeAnimationController;ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->this$0:Lcom/google/android/ublib/view/FadeAnimationController;

    iput-boolean p2, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$visible:Z

    iput-object p3, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$listener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->this$0:Lcom/google/android/ublib/view/FadeAnimationController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/ublib/view/FadeAnimationController;->mAnimation:Lcom/google/android/ublib/view/AnimatorCompat;
    invoke-static {v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->access$002(Lcom/google/android/ublib/view/FadeAnimationController;Lcom/google/android/ublib/view/AnimatorCompat;)Lcom/google/android/ublib/view/AnimatorCompat;

    .line 120
    iget-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$visible:Z

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->this$0:Lcom/google/android/ublib/view/FadeAnimationController;

    # getter for: Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/ublib/view/FadeAnimationController;->access$200(Lcom/google/android/ublib/view/FadeAnimationController;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->this$0:Lcom/google/android/ublib/view/FadeAnimationController;

    # getter for: Lcom/google/android/ublib/view/FadeAnimationController;->mInvisibleValue:I
    invoke-static {v1}, Lcom/google/android/ublib/view/FadeAnimationController;->access$100(Lcom/google/android/ublib/view/FadeAnimationController;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$listener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$listener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    invoke-interface {v0}, Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;->onVisibilityChangeEnd()V

    .line 126
    :cond_1
    return-void
.end method

.method public onAnimationStart()V
    .locals 2

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$visible:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->this$0:Lcom/google/android/ublib/view/FadeAnimationController;

    # getter for: Lcom/google/android/ublib/view/FadeAnimationController;->mView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/ublib/view/FadeAnimationController;->access$200(Lcom/google/android/ublib/view/FadeAnimationController;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$listener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/google/android/ublib/view/FadeAnimationController$1;->val$listener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    invoke-interface {v0}, Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;->onVisibilityChangeBegin()V

    .line 136
    :cond_1
    return-void
.end method
