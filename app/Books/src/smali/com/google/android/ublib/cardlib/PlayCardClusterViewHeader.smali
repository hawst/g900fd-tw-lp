.class public Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;
.super Landroid/widget/LinearLayout;
.source "PlayCardClusterViewHeader.java"


# instance fields
.field private mContent:Landroid/view/View;

.field private final mMinHeight:I

.field private mMoreView:Landroid/widget/TextView;

.field private mTitleMain:Landroid/widget/TextView;

.field private mTitleSecondary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    sget-object v1, Lcom/google/android/ublib/R$styleable;->PlayCardClusterViewHeader:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMinHeight:I

    .line 46
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 53
    const v0, 0x7f0e00db

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mContent:Landroid/view/View;

    .line 54
    const v0, 0x7f0e00dc

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f0e00dd

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f0e00df

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    .line 58
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getWidth()I

    move-result v6

    .line 127
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getHeight()I

    move-result v0

    .line 129
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getPaddingTop()I

    move-result v4

    .line 132
    .local v4, "paddingTop":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mContent:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getPaddingLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mContent:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    invoke-virtual {v7, v8, v4, v9, v0}, Landroid/view/View;->layout(IIII)V

    .line 135
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getPaddingRight()I

    move-result v7

    sub-int v5, v6, v7

    .line 138
    .local v5, "rightX":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 139
    .local v3, "moreWidth":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 140
    .local v1, "moreHeight":I
    sub-int v7, v0, v1

    sub-int/2addr v7, v4

    div-int/lit8 v7, v7, 0x2

    add-int v2, v4, v7

    .line 141
    .local v2, "moreTop":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    sub-int v8, v5, v3

    add-int v9, v2, v1

    invoke-virtual {v7, v8, v2, v5, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 143
    .end local v1    # "moreHeight":I
    .end local v2    # "moreTop":I
    .end local v3    # "moreWidth":I
    .end local v5    # "rightX":I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v5, 0x0

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 104
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getPaddingLeft()I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getPaddingRight()I

    move-result v4

    sub-int v1, v3, v4

    .line 106
    .local v1, "titleGroupWidth":I
    const/4 v0, 0x0

    .line 109
    .local v0, "maxContentHeight":I
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 110
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v5}, Landroid/widget/TextView;->measure(II)V

    .line 111
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 112
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v1, v3

    .line 116
    :cond_0
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mContent:Landroid/view/View;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 118
    iget-object v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 120
    iget v3, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMinHeight:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setMeasuredDimension(II)V

    .line 122
    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "titleMain"    # Ljava/lang/String;
    .param p2, "titleSecondary"    # Ljava/lang/String;
    .param p3, "more"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 63
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    :goto_1
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setClickable(Z)V

    .line 83
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
