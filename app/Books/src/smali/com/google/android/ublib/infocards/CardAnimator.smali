.class public Lcom/google/android/ublib/infocards/CardAnimator;
.super Landroid/animation/ValueAnimator;
.source "CardAnimator.java"


# instance fields
.field private fromRight:Z

.field private mAnimationIndex:I

.field private mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

.field private final mIsAppear:Z

.field private mTargetView:Landroid/view/View;


# direct methods
.method private configureTimings()V
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mIsAppear:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-ne v0, v1, :cond_1

    .line 81
    iget v0, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationIndex:I

    mul-int/lit8 v0, v0, 0x64

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ublib/infocards/CardAnimator;->setStartDelay(J)V

    .line 86
    :goto_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_UP:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_DOWN:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-ne v0, v1, :cond_3

    .line 88
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mIsAppear:Z

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x190

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/google/android/ublib/infocards/CardAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 92
    :goto_2
    return-void

    .line 83
    :cond_1
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ublib/infocards/CardAnimator;->setStartDelay(J)V

    goto :goto_0

    .line 88
    :cond_2
    const-wide/16 v0, 0xc8

    goto :goto_1

    .line 90
    :cond_3
    const-wide/16 v0, 0x258

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ublib/infocards/CardAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_2
.end method

.method public static usesAlpha(Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;)Z
    .locals 1
    .param p0, "animationType"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .prologue
    .line 170
    sget-object v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_DOWN:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->SLIDE_UP:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setTarget(Ljava/lang/Object;)V
    .locals 3
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 96
    check-cast p1, Landroid/view/View;

    .end local p1    # "target":Ljava/lang/Object;
    iput-object p1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mTargetView:Landroid/view/View;

    .line 97
    iget-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    if-eqz v1, :cond_5

    .line 98
    iget-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mTargetView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 99
    .local v0, "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iget-boolean v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_0
    iput-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 100
    iget-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-nez v1, :cond_0

    .line 102
    iget-boolean v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_1
    iput-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 104
    :cond_0
    iget v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->animationIndex:I

    rem-int/lit8 v1, v1, 0x2

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->fromRight:Z

    .line 105
    iget v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->animationIndex:I

    iput v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationIndex:I

    .line 109
    .end local v0    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :goto_3
    iget-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    sget-object v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    if-eq v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mTargetView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 112
    :cond_1
    return-void

    .line 99
    .restart local v0    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_2
    iget-object v1, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->disappearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_0

    .line 102
    :cond_3
    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_1

    .line 104
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 107
    .end local v0    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mIsAppear:Z

    if-eqz v1, :cond_6

    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_4
    iput-object v1, p0, Lcom/google/android/ublib/infocards/CardAnimator;->mAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_3

    :cond_6
    sget-object v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_4
.end method

.method public start()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/ublib/infocards/CardAnimator;->configureTimings()V

    .line 75
    invoke-super {p0}, Landroid/animation/ValueAnimator;->start()V

    .line 76
    return-void
.end method
