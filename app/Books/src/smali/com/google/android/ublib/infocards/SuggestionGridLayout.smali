.class public Lcom/google/android/ublib/infocards/SuggestionGridLayout;
.super Landroid/view/ViewGroup;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackViewClickListener;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;,
        Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;
    }
.end annotation


# instance fields
.field private final mAnimatingViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppearTransitionCount:I

.field private mColCount:I

.field private mColWidth:I

.field private mContentWidth:I

.field private mDealingIndex:I

.field private final mDragImageView:Landroid/widget/ImageView;

.field private mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

.field private final mGridItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mHitRect:Landroid/graphics/Rect;

.field private final mHorizontalItemMargin:I

.field private mIsDragging:Z

.field private mItemBottoms:[I

.field private mMaxColumnWidth:I

.field private mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

.field mReinitStackBitmap:Z

.field private final mResources:Landroid/content/res/Resources;

.field private mStackBitmap:Landroid/graphics/Bitmap;

.field private final mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

.field private final mVerticalItemMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    iput-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 65
    iput-boolean v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z

    .line 66
    iput-boolean v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mReinitStackBitmap:Z

    .line 67
    iput-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    .line 71
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    .line 73
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    .line 74
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    .line 79
    iput v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDealingIndex:I

    .line 82
    iput v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAppearTransitionCount:I

    .line 95
    sget-object v5, Lcom/google/android/ublib/R$styleable;->SuggestionGridLayout:[I

    invoke-virtual {p1, p2, v5, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 97
    .local v0, "a":Landroid/content/res/TypedArray;
    const v5, 0x7fffffff

    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mMaxColumnWidth:I

    .line 99
    invoke-virtual {v0, v8, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    .line 100
    iget v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    .line 101
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mVerticalItemMargin:I

    .line 103
    const/4 v5, 0x3

    invoke-virtual {v0, v5, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHorizontalItemMargin:I

    .line 105
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 113
    invoke-virtual {p0, v8}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setClipToPadding(Z)V

    .line 114
    invoke-virtual {p0, v8}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setClipChildren(Z)V

    .line 115
    invoke-virtual {p0, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    .line 118
    .local v1, "density":F
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    .line 120
    .local v4, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v3

    .line 122
    .local v3, "pagingSlop":I
    new-instance v5, Lcom/google/android/ublib/infocards/SwipeHelper;

    new-instance v6, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;

    invoke-direct {v6, p0, v7}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SwipeCallback;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$1;)V

    int-to-float v7, v3

    invoke-direct {v5, v8, v6, v1, v7}, Lcom/google/android/ublib/infocards/SwipeHelper;-><init>(ILcom/google/android/ublib/infocards/SwipeHelper$Callback;FF)V

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mResources:Landroid/content/res/Resources;

    .line 125
    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 127
    .local v2, "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    sget-object v5, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v5, v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    .line 128
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    invoke-super {p0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 130
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 131
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z

    return v0
.end method

.method static synthetic access$1001(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;IZ)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;IZ)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1301(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 36
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$1501(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1601(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1701(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 36
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->fadeIn(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$2001(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragStack:Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setupDragImage(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$801(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/ublib/infocards/SuggestionGridLayout;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    return-object v0
.end method

.method private addNewCardsToDeal(Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;)V
    .locals 5
    .param p1, "i"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    .prologue
    .line 192
    invoke-interface {p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 193
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 196
    .local v1, "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iget-object v3, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;

    invoke-static {v3}, Lcom/google/android/ublib/infocards/CardAnimator;->usesAlpha(Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams$AnimationType;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 197
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 199
    :cond_0
    iget v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDealingIndex:I

    iput v3, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->animationIndex:I

    .line 200
    iget v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDealingIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDealingIndex:I

    goto :goto_0

    .line 202
    .end local v1    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private fadeIn(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 998
    return-void
.end method

.method private getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 615
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 616
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 617
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    .line 618
    .local v2, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 622
    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    :goto_1
    return-object v2

    .line 616
    .restart local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 622
    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setupDragImage(Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;)V
    .locals 7
    .param p1, "item"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .prologue
    .line 740
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 742
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->initStackBitmap()V

    .line 744
    :cond_1
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 745
    .local v0, "c":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 746
    invoke-virtual {p1, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->draw(Landroid/graphics/Canvas;)V

    .line 747
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 748
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    .line 749
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    iget v2, p1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->left:I

    iget v3, p1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->top:I

    iget v4, p1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->left:I

    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->top:I

    iget-object v6, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 755
    return-void
.end method

.method private setupLayoutParams(Landroid/view/View;IZ)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "column"    # I
    .param p3, "spanAllColumns"    # Z

    .prologue
    .line 475
    iget v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    if-lt p2, v2, :cond_0

    .line 476
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Column exceeds column count."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 478
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 479
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_1

    .line 480
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 482
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, v0

    :goto_0
    check-cast v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-object v1, v2

    check-cast v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    .line 483
    .local v1, "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iput p2, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    .line 484
    iput-boolean p3, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    .line 485
    iget-boolean v2, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v2, :cond_2

    iget-boolean v2, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    if-nez v2, :cond_2

    .line 486
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 487
    const-string v2, "SuggestionGridLayout"

    const-string v3, "only spanAllColumns views can have no padding"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 490
    return-object v1

    .line 482
    .end local v1    # "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    goto :goto_0
.end method

.method private toggleStackExpansion(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    .line 277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    if-ne v8, v7, :cond_3

    .line 278
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    add-float/2addr v8, v9

    float-to-int v5, v8

    .line 279
    .local v5, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    add-float/2addr v8, v9

    float-to-int v6, v8

    .line 281
    .local v6, "y":I
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 282
    .local v0, "count":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_3

    .line 284
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 285
    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_1

    .line 282
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 288
    :cond_1
    iget-object v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v8}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 289
    iget-object v8, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 291
    invoke-direct {p0, v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v2

    .line 292
    .local v2, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    if-eqz v2, :cond_0

    .line 296
    instance-of v8, v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    if-eqz v8, :cond_0

    move-object v3, v2

    .line 297
    check-cast v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 298
    .local v3, "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    iget v8, v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mCollapsedCardsTop:I

    if-le v6, v8, :cond_0

    iget v8, v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->mCollapsedCardsBottom:I

    if-ge v6, v8, :cond_0

    .line 299
    invoke-virtual {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isExpanded()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 303
    invoke-virtual {v3, v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->bringToFrontAndCollapse(Landroid/view/View;)V

    .line 314
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v3    # "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .end local v4    # "v":Landroid/view/View;
    .end local v5    # "x":I
    .end local v6    # "y":I
    :goto_1
    return v7

    .line 306
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    .restart local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .restart local v3    # "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .restart local v4    # "v":Landroid/view/View;
    .restart local v5    # "x":I
    .restart local v6    # "y":I
    :cond_2
    invoke-virtual {v3, v7}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    goto :goto_1

    .line 314
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v3    # "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    .end local v4    # "v":Landroid/view/View;
    .end local v5    # "x":I
    .end local v6    # "y":I
    :cond_3
    const/4 v7, 0x0

    goto :goto_1
.end method


# virtual methods
.method public addStackToColumn(Ljava/util/List;ILcom/google/android/ublib/infocards/DismissTrailFactory;Landroid/view/View;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V
    .locals 12
    .param p2, "column"    # I
    .param p3, "dismissTrailFactory"    # Lcom/google/android/ublib/infocards/DismissTrailFactory;
    .param p4, "expandedHeader"    # Landroid/view/View;
    .param p5, "tag"    # Ljava/lang/Object;
    .param p6, "clickListener"    # Landroid/view/View$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;I",
            "Lcom/google/android/ublib/infocards/DismissTrailFactory;",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    if-lt p2, v1, :cond_0

    .line 506
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Column exceeds column count."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 508
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    .line 509
    .local v11, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    .line 510
    .local v10, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v11, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 512
    .end local v10    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v11    # "v":Landroid/view/View;
    :cond_1
    new-instance v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/content/Context;Ljava/util/List;ILcom/google/android/ublib/infocards/DismissTrailFactory;Landroid/view/View;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    .line 514
    .local v0, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 515
    invoke-direct {p0, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;)V

    .line 518
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    .line 519
    .restart local v11    # "v":Landroid/view/View;
    invoke-super {p0, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 521
    .end local v11    # "v":Landroid/view/View;
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mReinitStackBitmap:Z

    .line 522
    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 441
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->addViewToColumn(Landroid/view/View;ILcom/google/android/ublib/infocards/DismissTrailFactory;)V

    .line 442
    return-void
.end method

.method public addViewToColumn(Landroid/view/View;ILcom/google/android/ublib/infocards/DismissTrailFactory;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "column"    # I
    .param p3, "dismissTrailFactory"    # Lcom/google/android/ublib/infocards/DismissTrailFactory;

    .prologue
    .line 451
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;IZ)Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v1

    .line 452
    .local v1, "sglp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    new-instance v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;

    invoke-direct {v0, p0, p1, p3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$SimpleGridItem;-><init>(Lcom/google/android/ublib/infocards/SuggestionGridLayout;Landroid/view/View;Lcom/google/android/ublib/infocards/DismissTrailFactory;)V

    .line 453
    .local v0, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    invoke-direct {p0, v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->addNewCardsToDeal(Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;)V

    .line 455
    invoke-super {p0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 456
    return-void
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 632
    instance-of v0, p1, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "drawingTime"    # J

    .prologue
    .line 1072
    const/4 v1, 0x0

    .line 1074
    .local v1, "restoreCanvas":Z
    invoke-direct {p0, p2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 1077
    .local v0, "gi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    instance-of v4, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mIsDragging:Z

    if-nez v4, :cond_0

    .line 1079
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1080
    const/4 v1, 0x1

    move-object v3, v0

    .line 1082
    check-cast v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    .line 1083
    .local v3, "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    invoke-virtual {v3, p2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->getChildClipRect(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1086
    .end local v3    # "sgi":Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v2

    .line 1089
    .local v2, "ret":Z
    if-eqz v1, :cond_1

    .line 1090
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1092
    :cond_1
    return v2
.end method

.method public forEachCardView(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 732
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Landroid/view/View;>;"
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    .line 733
    .local v2, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 734
    .local v3, "view":Landroid/view/View;
    invoke-interface {p1, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 737
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    .prologue
    .line 627
    new-instance v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 642
    new-instance v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 637
    new-instance v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 7
    .param p1, "childCount"    # I
    .param p2, "index"    # I

    .prologue
    .line 207
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 223
    .end local p2    # "index":I
    :goto_0
    return p2

    .line 210
    .restart local p2    # "index":I
    :cond_0
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 211
    .local v0, "animSize":I
    sub-int v1, p1, v0

    .line 212
    .local v1, "animStartsAt":I
    if-lt p2, v1, :cond_1

    .line 213
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    sub-int v6, p2, v1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result p2

    goto :goto_0

    .line 215
    :cond_1
    move v3, p2

    .line 216
    .local v3, "result":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-gt v2, v3, :cond_3

    .line 217
    invoke-virtual {p0, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 218
    .local v4, "v":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 219
    add-int/lit8 v3, v3, 0x1

    .line 216
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v4    # "v":Landroid/view/View;
    :cond_3
    move p2, v3

    .line 223
    goto :goto_0
.end method

.method public getHeightOfTopStack()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 722
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 725
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    invoke-interface {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getNumberOfStacks()I
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public hasChildAtLocation(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 787
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 788
    .local v0, "count":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 789
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 790
    .local v3, "v":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_1

    .line 788
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 793
    :cond_1
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 794
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 795
    invoke-direct {p0, v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v2

    .line 796
    .local v2, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    if-eqz v2, :cond_0

    .line 797
    const/4 v4, 0x1

    .line 800
    .end local v2    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v3    # "v":Landroid/view/View;
    :goto_1
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method initStackBitmap()V
    .locals 8

    .prologue
    .line 758
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 759
    .local v0, "count":I
    const/4 v5, 0x0

    .line 760
    .local v5, "maxWidth":I
    const/4 v4, 0x0

    .line 761
    .local v4, "maxHeight":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 762
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    .line 765
    .local v3, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    instance-of v7, v3, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    if-eqz v7, :cond_1

    move-object v7, v3

    check-cast v7, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;

    invoke-virtual {v7}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 766
    invoke-interface {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v6

    .line 767
    .local v6, "width":I
    invoke-interface {v3}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v1

    .line 768
    .local v1, "height":I
    if-le v6, v5, :cond_0

    .line 769
    move v5, v6

    .line 771
    :cond_0
    if-le v1, v4, :cond_1

    .line 772
    move v4, v1

    .line 761
    .end local v1    # "height":I
    .end local v6    # "width":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 778
    .end local v3    # "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    :cond_2
    if-lez v5, :cond_4

    if-lez v4, :cond_4

    .line 779
    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-gt v5, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    if-le v4, v7, :cond_4

    .line 781
    :cond_3
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mStackBitmap:Landroid/graphics/Bitmap;

    .line 784
    :cond_4
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 251
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 252
    .local v0, "intercepted":Z
    if-nez v0, :cond_0

    .line 253
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->toggleStackExpansion(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 255
    :cond_0
    if-eqz v0, :cond_1

    .line 256
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 258
    :cond_1
    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 407
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingTop()I

    move-result v10

    invoke-static {v9, v10}, Ljava/util/Arrays;->fill([II)V

    .line 408
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getMeasuredWidth()I

    move-result v9

    iget v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mContentWidth:I

    sub-int/2addr v9, v10

    div-int/lit8 v0, v9, 0x2

    .line 410
    .local v0, "beginLeft":I
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 411
    .local v3, "childCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_3

    .line 412
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    .line 413
    .local v2, "child":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 411
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 416
    :cond_0
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v6

    .line 418
    .local v6, "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    iget-boolean v9, v6, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    if-eqz v9, :cond_1

    const/4 v5, 0x0

    .line 420
    .local v5, "left":I
    :goto_2
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    iget v10, v6, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    aget v8, v9, v10

    .line 421
    .local v8, "top":I
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getMeasuredWidth()I

    move-result v9

    add-int v7, v5, v9

    .line 422
    .local v7, "right":I
    invoke-interface {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v9

    add-int v1, v8, v9

    .line 423
    .local v1, "bottom":I
    invoke-interface {v2, v5, v8, v7, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->gridLayout(IIII)V

    .line 425
    iget-boolean v9, v6, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    if-eqz v9, :cond_2

    .line 426
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    iget v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v10, v1

    invoke-static {v9, v10}, Ljava/util/Arrays;->fill([II)V

    goto :goto_1

    .line 418
    .end local v1    # "bottom":I
    .end local v5    # "left":I
    .end local v7    # "right":I
    .end local v8    # "top":I
    :cond_1
    iget v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColWidth:I

    iget v10, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHorizontalItemMargin:I

    add-int/2addr v9, v10

    iget v10, v6, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    mul-int/2addr v9, v10

    add-int v5, v0, v9

    goto :goto_2

    .line 428
    .restart local v1    # "bottom":I
    .restart local v5    # "left":I
    .restart local v7    # "right":I
    .restart local v8    # "top":I
    :cond_2
    iget-object v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    iget v10, v6, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    iget v11, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mVerticalItemMargin:I

    add-int/2addr v11, v1

    aput v11, v9, v10

    goto :goto_1

    .line 431
    .end local v1    # "bottom":I
    .end local v2    # "child":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v5    # "left":I
    .end local v6    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    .end local v7    # "right":I
    .end local v8    # "top":I
    :cond_3
    iget-boolean v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mReinitStackBitmap:Z

    if-eqz v9, :cond_4

    .line 432
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mReinitStackBitmap:Z

    .line 433
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->initStackBitmap()V

    .line 435
    :cond_4
    const/4 v9, 0x0

    iput v9, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDealingIndex:I

    .line 436
    return-void
.end method

.method protected onMeasure(II)V
    .locals 28
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 319
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v21

    .line 320
    .local v21, "widthMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    .line 321
    .local v11, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v22

    .line 322
    .local v22, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    .line 324
    .local v12, "heightSize":I
    const/16 v20, 0x0

    .line 325
    .local v20, "width":I
    const/4 v7, 0x0

    .line 326
    .local v7, "childWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mHorizontalItemMargin:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    mul-int v15, v23, v24

    .line 327
    .local v15, "margin":I
    sparse-switch v21, :sswitch_data_0

    .line 347
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    move/from16 v23, v0

    mul-int v23, v23, v7

    add-int v23, v23, v15

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mContentWidth:I

    .line 348
    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColWidth:I

    .line 349
    const/high16 v23, 0x40000000    # 2.0f

    move/from16 v0, v23

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    .line 351
    .local v19, "singleColumnWidthSpec":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mContentWidth:I

    move/from16 v23, v0

    const/high16 v24, 0x40000000    # 2.0f

    invoke-static/range {v23 .. v24}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 353
    .local v3, "allColumnsWidthSpec":I
    const/high16 v23, 0x40000000    # 2.0f

    move/from16 v0, v20

    move/from16 v1, v23

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 356
    .local v2, "allColumnsNoPaddingWidthSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static/range {v23 .. v24}, Ljava/util/Arrays;->fill([II)V

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 360
    .local v5, "childCount":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v5, :cond_4

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    .line 362
    .local v4, "child":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    invoke-interface {v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->isGone()Z

    move-result v23

    if-eqz v23, :cond_1

    .line 360
    :cond_0
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 329
    .end local v2    # "allColumnsNoPaddingWidthSpec":I
    .end local v3    # "allColumnsWidthSpec":I
    .end local v4    # "child":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v5    # "childCount":I
    .end local v13    # "i":I
    .end local v19    # "singleColumnWidthSpec":I
    :sswitch_0
    move/from16 v20, v22

    .line 330
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingLeft()I

    move-result v23

    sub-int v23, v20, v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingRight()I

    move-result v24

    sub-int v17, v23, v24

    .line 331
    .local v17, "paddedWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v23, v0

    sub-int v24, v17, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    move/from16 v25, v0

    div-int v24, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 333
    goto/16 :goto_0

    .line 336
    .end local v17    # "paddedWidth":I
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingLeft()I

    move-result v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingRight()I

    move-result v24

    add-int v18, v23, v24

    .line 337
    .local v18, "padding":I
    sub-int v17, v22, v18

    .line 338
    .restart local v17    # "paddedWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mMaxColumnWidth:I

    move/from16 v23, v0

    sub-int v24, v17, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    move/from16 v25, v0

    div-int v24, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 339
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    move/from16 v23, v0

    mul-int v23, v23, v7

    add-int v23, v23, v15

    add-int v20, v23, v18

    .line 340
    goto/16 :goto_0

    .line 343
    .end local v17    # "paddedWidth":I
    .end local v18    # "padding":I
    :sswitch_2
    new-instance v23, Ljava/lang/IllegalArgumentException;

    const-string v24, "Cannot measure SuggestionGridLayout with mode UNSPECIFIED"

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 365
    .restart local v2    # "allColumnsNoPaddingWidthSpec":I
    .restart local v3    # "allColumnsWidthSpec":I
    .restart local v4    # "child":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .restart local v5    # "childCount":I
    .restart local v13    # "i":I
    .restart local v19    # "singleColumnWidthSpec":I
    :cond_1
    invoke-interface {v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getGridLayoutParams()Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;

    move-result-object v14

    .line 369
    .local v14, "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-static/range {v23 .. v24}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 371
    .local v6, "childHeightSpec":I
    iget-boolean v0, v14, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    .line 372
    iget-boolean v0, v14, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->noPadding:Z

    move/from16 v23, v0

    if-eqz v23, :cond_2

    .line 373
    move v8, v2

    .line 380
    .local v8, "childWidthSpec":I
    :goto_3
    invoke-interface {v4, v8, v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->gridMeasure(II)V

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v23, v0

    iget v0, v14, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    move/from16 v24, v0

    aget v25, v23, v24

    invoke-interface {v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getMeasuredHeight()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    add-int v25, v25, v26

    aput v25, v23, v24

    .line 382
    iget-boolean v0, v14, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->spanAllColumns:Z

    move/from16 v23, v0

    if-eqz v23, :cond_0

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v24, v0

    iget v0, v14, Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;->column:I

    move/from16 v25, v0

    aget v24, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/util/Arrays;->fill([II)V

    goto/16 :goto_2

    .line 375
    .end local v8    # "childWidthSpec":I
    :cond_2
    move v8, v3

    .restart local v8    # "childWidthSpec":I
    goto :goto_3

    .line 378
    .end local v8    # "childWidthSpec":I
    :cond_3
    move/from16 v8, v19

    .restart local v8    # "childWidthSpec":I
    goto :goto_3

    .line 387
    .end local v4    # "child":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    .end local v6    # "childHeightSpec":I
    .end local v8    # "childWidthSpec":I
    .end local v14    # "lp":Lcom/google/android/ublib/infocards/SuggestionGridLayout$LayoutParams;
    :cond_4
    move v10, v12

    .line 388
    .local v10, "height":I
    const/high16 v23, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v11, v0, :cond_7

    .line 389
    const/16 v16, 0x0

    .line 390
    .local v16, "maxColHeight":I
    const/4 v13, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mColCount:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v13, v0, :cond_6

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mItemBottoms:[I

    move-object/from16 v23, v0

    aget v23, v23, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mVerticalItemMargin:I

    move/from16 v24, v0

    sub-int v9, v23, v24

    .line 392
    .local v9, "colHeight":I
    move/from16 v0, v16

    if-le v9, v0, :cond_5

    .line 393
    move/from16 v16, v9

    .line 390
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 396
    .end local v9    # "colHeight":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingTop()I

    move-result v23

    add-int v23, v23, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingBottom()I

    move-result v24

    add-int v10, v23, v24

    .line 398
    .end local v16    # "maxColHeight":I
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setMeasuredDimension(II)V

    .line 400
    const-string v23, "SuggestionGridLayout"

    const/16 v24, 0x3

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 401
    const-string v23, "SuggestionGridLayout"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Measured width="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", height="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :cond_8
    return-void

    .line 327
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 266
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeAllViews()V
    .locals 3

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 588
    .local v0, "childCount":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 589
    invoke-virtual {p0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    .line 588
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 591
    :cond_0
    return-void
.end method

.method public removeGridItem(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 594
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    invoke-virtual {v1}, Lcom/google/android/ublib/infocards/SwipeHelper;->cancelOngoingDrag()V

    .line 599
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getGridItemForView(Landroid/view/View;)Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;

    move-result-object v0

    .line 601
    .local v0, "item":Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;
    if-eqz v0, :cond_3

    .line 602
    invoke-interface {v0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->removeView(Landroid/view/View;)V

    .line 603
    invoke-interface {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->getViews()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 604
    invoke-interface {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout$GridItem;->removeExtraViews()V

    .line 605
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mGridItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 607
    :cond_1
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mAnimatingViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 608
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 612
    :cond_2
    :goto_0
    return-void

    .line 609
    :cond_3
    iget-object v1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mDragImageView:Landroid/widget/ImageView;

    if-eq p1, v1, :cond_2

    .line 610
    const-string v1, "SuggestionGridLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeGridItem with non-grid item "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 447
    invoke-virtual {p0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    .line 448
    return-void
.end method

.method public setOnDismissListener(Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    .prologue
    .line 574
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    .line 575
    return-void
.end method

.method public setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/google/android/ublib/view/TranslationHelper;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V

    .line 135
    return-void
.end method
