.class Lcom/google/android/ublib/widget/AbsWarpListView$3;
.super Ljava/lang/Object;
.source "AbsWarpListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/widget/AbsWarpListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/widget/AbsWarpListView;


# direct methods
.method constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 0

    .prologue
    .line 1305
    iput-object p1, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1308
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidatePending:Z
    invoke-static {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2602(Lcom/google/android/ublib/widget/AbsWarpListView;Z)Z

    .line 1309
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2700(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1800(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    add-int v0, v2, v3

    .line 1310
    .local v0, "pos":I
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->isVisiblePosition(I)Z
    invoke-static {v2, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2800(Lcom/google/android/ublib/widget/AbsWarpListView;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1311
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2700(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->nextInvalidateOffset(I)I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2900(I)I

    move-result v3

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I
    invoke-static {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2702(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1312
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I
    invoke-static {v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2700(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mCenterPosition:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$1800(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    add-int v0, v2, v3

    .line 1313
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->isVisiblePosition(I)Z
    invoke-static {v2, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2800(Lcom/google/android/ublib/widget/AbsWarpListView;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1322
    :goto_0
    return-void

    .line 1318
    :cond_0
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v2, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v1

    .line 1319
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 1320
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v3, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    # getter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2700(Lcom/google/android/ublib/widget/AbsWarpListView;)I

    move-result v3

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->nextInvalidateOffset(I)I
    invoke-static {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2900(I)I

    move-result v3

    # setter for: Lcom/google/android/ublib/widget/AbsWarpListView;->mInvalidateOffset:I
    invoke-static {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$2702(Lcom/google/android/ublib/widget/AbsWarpListView;I)I

    .line 1321
    iget-object v2, p0, Lcom/google/android/ublib/widget/AbsWarpListView$3;->this$0:Lcom/google/android/ublib/widget/AbsWarpListView;

    const/16 v3, 0x32

    # invokes: Lcom/google/android/ublib/widget/AbsWarpListView;->scheduleInvalidate(I)V
    invoke-static {v2, v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->access$3000(Lcom/google/android/ublib/widget/AbsWarpListView;I)V

    goto :goto_0
.end method
