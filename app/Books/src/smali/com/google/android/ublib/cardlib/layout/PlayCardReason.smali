.class public Lcom/google/android/ublib/cardlib/layout/PlayCardReason;
.super Landroid/widget/LinearLayout;
.source "PlayCardReason.java"


# instance fields
.field private mReason:Landroid/widget/TextView;

.field private mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/CharSequence;Landroid/net/Uri;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/CharSequence;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "imageProvider"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    if-eqz p2, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setArtURI(Landroid/net/Uri;Lcom/google/android/ublib/cardlib/PlayCardImageProvider;)V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 33
    const v0, 0x7f0e0202

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    .line 34
    const v0, 0x7f0e0201

    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    .line 35
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getHeight()I

    move-result v1

    .line 53
    .local v1, "height":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getWidth()I

    move-result v7

    .line 55
    .local v7, "width":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    .line 56
    .local v5, "reasonHeight":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getMeasuredHeight()I

    move-result v2

    .line 57
    .local v2, "imageHeight":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->getMeasuredWidth()I

    move-result v3

    .line 59
    .local v3, "imageWidth":I
    if-le v2, v5, :cond_0

    .line 62
    sub-int v8, v1, v2

    div-int/lit8 v4, v8, 0x2

    .line 63
    .local v4, "imageY":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    const/4 v9, 0x0

    add-int v10, v4, v2

    invoke-virtual {v8, v9, v4, v3, v10}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->layout(IIII)V

    .line 64
    sub-int v8, v1, v5

    div-int/lit8 v6, v8, 0x2

    .line 65
    .local v6, "reasonY":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v9, v6, v5

    invoke-virtual {v8, v3, v6, v7, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 72
    .end local v4    # "imageY":I
    .end local v6    # "reasonY":I
    :goto_0
    return-void

    .line 68
    :cond_0
    sub-int v8, v1, v5

    div-int/lit8 v0, v8, 0x2

    .line 69
    .local v0, "contentY":I
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReasonImage:Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    const/4 v9, 0x0

    add-int v10, v0, v5

    invoke-virtual {v8, v9, v0, v3, v10}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->layout(IIII)V

    .line 70
    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v9, v0, v5

    invoke-virtual {v8, v3, v0, v7, v9}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method public setReasonMaxLines(I)V
    .locals 1
    .param p1, "reasonMaxLines"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 48
    return-void
.end method
