.class public Lcom/google/android/ublib/infocards/SwipeHelper;
.super Ljava/lang/Object;
.source "SwipeHelper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "FloatMath"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ublib/infocards/SwipeHelper$Callback;
    }
.end annotation


# static fields
.field public static ALPHA_FADE_START:F

.field private static sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private final DEFAULT_ESCAPE_ANIMATION_DURATION:I

.field private final MAX_DISMISS_VELOCITY:I

.field private final MAX_ESCAPE_ANIMATION_DURATION:I

.field private final SWIPE_ESCAPE_VELOCITY:F

.field private final mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrView:Landroid/view/View;

.field private mDensityScale:F

.field private mDragging:Z

.field private mDriftSlop:F

.field private mHasDismissAnimation:Z

.field private mInitialDriftPos:F

.field private mInitialTouchPos:F

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private final mSwipeDirection:I

.field private mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

.field private final mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/ublib/infocards/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    .line 48
    const v0, 0x3e19999a    # 0.15f

    sput v0, Lcom/google/android/ublib/infocards/SwipeHelper;->ALPHA_FADE_START:F

    return-void
.end method

.method public constructor <init>(ILcom/google/android/ublib/infocards/SwipeHelper$Callback;FF)V
    .locals 6
    .param p1, "swipeDirection"    # I
    .param p2, "callback"    # Lcom/google/android/ublib/infocards/SwipeHelper$Callback;
    .param p3, "densityScale"    # F
    .param p4, "pagingTouchSlop"    # F

    .prologue
    .line 85
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/infocards/SwipeHelper;-><init>(ILcom/google/android/ublib/infocards/SwipeHelper$Callback;FFF)V

    .line 86
    return-void
.end method

.method public constructor <init>(ILcom/google/android/ublib/infocards/SwipeHelper$Callback;FFF)V
    .locals 1
    .param p1, "swipeDirection"    # I
    .param p2, "callback"    # Lcom/google/android/ublib/infocards/SwipeHelper$Callback;
    .param p3, "densityScale"    # F
    .param p4, "pagingTouchSlop"    # F
    .param p5, "driftSlop"    # F

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:F

    .line 43
    const/16 v0, 0x4b

    iput v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    .line 44
    const/16 v0, 0x96

    iput v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    .line 45
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mMinAlpha:F

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mHasDismissAnimation:Z

    .line 75
    iput-object p2, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    .line 76
    iput p1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    .line 77
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 78
    iput p3, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDensityScale:F

    .line 79
    iput p4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mPagingTouchSlop:F

    .line 80
    iput p5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDriftSlop:F

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ublib/infocards/SwipeHelper;)Lcom/google/android/ublib/infocards/SwipeHelper$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SwipeHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/ublib/infocards/SwipeHelper;Landroid/view/View;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/ublib/infocards/SwipeHelper;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newPos"    # F

    .prologue
    .line 136
    iget-object v2, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;

    move-result-object v1

    .line 137
    .local v1, "target":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    iget v2, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v2, :cond_0

    const-string v2, "translationX"

    :goto_0
    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput p2, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 139
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    return-object v0

    .line 137
    .end local v0    # "anim":Landroid/animation/ObjectAnimator;
    :cond_0
    const-string v2, "translationY"

    goto :goto_0
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    .line 166
    .local v3, "viewSize":F
    const v4, 0x3f266666    # 0.65f

    mul-float v0, v4, v3

    .line 167
    .local v0, "fadeSize":F
    const/high16 v2, 0x3f800000    # 1.0f

    .line 168
    .local v2, "result":F
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v1

    .line 169
    .local v1, "pos":F
    sget v4, Lcom/google/android/ublib/infocards/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    .line 170
    sget v4, Lcom/google/android/ublib/infocards/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    sub-float v4, v1, v4

    div-float/2addr v4, v0

    sub-float v2, v5, v4

    .line 174
    :cond_0
    :goto_0
    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 175
    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 176
    iget v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    .line 171
    :cond_1
    sget v4, Lcom/google/android/ublib/infocards/SwipeHelper;->ALPHA_FADE_START:F

    sub-float v4, v5, v4

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 172
    sget v4, Lcom/google/android/ublib/infocards/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    add-float/2addr v4, v1

    div-float/2addr v4, v0

    add-float v2, v5, v4

    goto :goto_0
.end method

.method private getDriftPos(Landroid/view/MotionEvent;)F
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1, "vt"    # Landroid/view/VelocityTracker;

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getPos(Landroid/view/MotionEvent;)F
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 157
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    goto :goto_0
.end method

.method private getTranslation(Landroid/view/View;)F
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 126
    iget v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationX(Landroid/view/View;)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v0

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1, "vt"    # Landroid/view/VelocityTracker;

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "translate"    # F

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationX(Landroid/view/View;F)V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationY(Landroid/view/View;F)V

    goto :goto_0
.end method


# virtual methods
.method public cancelOngoingDrag()V
    .locals 2

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/ublib/infocards/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    .line 109
    iget-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    iget-object v1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->onSnapBackCompleted(Landroid/view/View;)V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    .line 112
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    .line 114
    :cond_1
    return-void
.end method

.method public dismissChild(Landroid/view/View;F)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "velocity"    # F

    .prologue
    const/4 v7, 0x0

    .line 231
    iget-boolean v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mHasDismissAnimation:Z

    if-nez v5, :cond_0

    .line 232
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    invoke-interface {v5, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->onChildDismissed(Landroid/view/View;)V

    .line 277
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    invoke-interface {v5, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    .line 238
    .local v1, "canAnimViewBeDismissed":Z
    cmpg-float v5, p2, v7

    if-ltz v5, :cond_2

    cmpl-float v5, p2, v7

    if-nez v5, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v5

    cmpg-float v5, v5, v7

    if-ltz v5, :cond_2

    :cond_1
    cmpl-float v5, p2, v7

    if-nez v5, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v5

    cmpl-float v5, v5, v7

    if-nez v5, :cond_3

    iget v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mSwipeDirection:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 242
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v5

    neg-float v3, v5

    .line 246
    .local v3, "newPos":F
    :goto_1
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v6, p1}, Lcom/google/android/ublib/view/TranslationHelper;->getLeft(Landroid/view/View;)I

    move-result v6

    sub-int v4, v5, v6

    .line 247
    .local v4, "newTranslation":I
    const/16 v2, 0x96

    .line 248
    .local v2, "duration":I
    cmpl-float v5, p2, v7

    if-eqz v5, :cond_4

    .line 249
    int-to-float v5, v4

    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v6

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    div-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 256
    :goto_2
    int-to-float v5, v4

    invoke-direct {p0, p1, v5}, Lcom/google/android/ublib/infocards/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 257
    .local v0, "anim":Landroid/animation/ValueAnimator;
    sget-object v5, Lcom/google/android/ublib/infocards/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 258
    int-to-long v6, v2

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 259
    new-instance v5, Lcom/google/android/ublib/infocards/SwipeHelper$1;

    invoke-direct {v5, p0, p1, v1}, Lcom/google/android/ublib/infocards/SwipeHelper$1;-><init>(Lcom/google/android/ublib/infocards/SwipeHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 268
    new-instance v5, Lcom/google/android/ublib/infocards/SwipeHelper$2;

    invoke-direct {v5, p0, v1, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$2;-><init>(Lcom/google/android/ublib/infocards/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 276
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 244
    .end local v0    # "anim":Landroid/animation/ValueAnimator;
    .end local v2    # "duration":I
    .end local v3    # "newPos":F
    .end local v4    # "newTranslation":I
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    .restart local v3    # "newPos":F
    goto :goto_1

    .line 253
    .restart local v2    # "duration":I
    .restart local v4    # "newTranslation":I
    :cond_4
    const/16 v2, 0x4b

    goto :goto_2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 180
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 182
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 223
    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    return v4

    .line 184
    :pswitch_0
    iput-boolean v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    .line 185
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    .line 186
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->clear()V

    .line 187
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 188
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v4, v5}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCanCurrViewBeDimissed:Z

    .line 189
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 190
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v4

    iput v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mInitialTouchPos:F

    .line 191
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getDriftPos(Landroid/view/MotionEvent;)F

    move-result v4

    iput v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mInitialDriftPos:F

    goto :goto_0

    .line 195
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 196
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 200
    iget-boolean v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    if-nez v4, :cond_1

    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getDriftPos(Landroid/view/MotionEvent;)F

    move-result v2

    .line 202
    .local v2, "driftPos":F
    iget v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mInitialDriftPos:F

    sub-float v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDriftSlop:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 203
    iput-object v6, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    goto :goto_0

    .line 208
    .end local v2    # "driftPos":F
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    .line 209
    .local v3, "pos":F
    iget v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mInitialTouchPos:F

    sub-float v1, v3, v4

    .line 210
    .local v1, "delta":F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mPagingTouchSlop:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 211
    iget-object v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v4, v5}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    .line 212
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    .line 213
    invoke-direct {p0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-direct {p0, v5}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v5

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mInitialTouchPos:F

    goto :goto_0

    .line 219
    .end local v1    # "delta":F
    .end local v3    # "pos":F
    :pswitch_2
    iput-boolean v4, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    .line 220
    iput-object v6, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    goto/16 :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 313
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 314
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 315
    .local v2, "action":I
    packed-switch v2, :pswitch_data_0

    .line 370
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDragging:Z

    return v14

    .line 318
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_0

    .line 319
    invoke-direct/range {p0 .. p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mInitialTouchPos:F

    sub-float v6, v14, v15

    .line 322
    .local v6, "delta":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 323
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/ublib/infocards/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v12

    .line 324
    .local v12, "size":F
    const v14, 0x3e19999a    # 0.15f

    mul-float v9, v14, v12

    .line 325
    .local v9, "maxScrollDistance":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v14, v14, v12

    if-ltz v14, :cond_3

    .line 326
    const/4 v14, 0x0

    cmpl-float v14, v6, v14

    if-lez v14, :cond_2

    move v6, v9

    .line 331
    .end local v9    # "maxScrollDistance":F
    .end local v12    # "size":F
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6}, Lcom/google/android/ublib/infocards/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    .line 332
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCanCurrViewBeDimissed:Z

    if-eqz v14, :cond_0

    .line 333
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/ublib/infocards/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v3

    .line 334
    .local v3, "alpha":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-virtual {v14, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 326
    .end local v3    # "alpha":F
    .restart local v9    # "maxScrollDistance":F
    .restart local v12    # "size":F
    :cond_2
    neg-float v6, v9

    goto :goto_1

    .line 328
    :cond_3
    div-float v14, v6, v12

    float-to-double v14, v14

    const-wide v16, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    double-to-float v14, v14

    mul-float v6, v9, v14

    goto :goto_1

    .line 340
    .end local v6    # "delta":F
    .end local v9    # "maxScrollDistance":F
    .end local v12    # "size":F
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v14, :cond_0

    .line 341
    const/high16 v14, 0x44fa0000    # 2000.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDensityScale:F

    mul-float v10, v14, v15

    .line 342
    .local v10, "maxVelocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v15, 0x3e8

    invoke-virtual {v14, v15, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 343
    const/high16 v14, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mDensityScale:F

    mul-float v8, v14, v15

    .line 344
    .local v8, "escapeVelocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/ublib/infocards/SwipeHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v13

    .line 345
    .local v13, "velocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/ublib/infocards/SwipeHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v11

    .line 348
    .local v11, "perpendicularVelocity":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    float-to-double v14, v14

    const-wide v16, 0x3fe3333333333333L    # 0.6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/ublib/infocards/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v18

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    cmpl-double v14, v14, v16

    if-lez v14, :cond_5

    const/4 v4, 0x1

    .line 350
    .local v4, "childSwipedFarEnough":Z
    :goto_2
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v14, v14, v8

    if-lez v14, :cond_8

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v14

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v15

    cmpl-float v14, v14, v15

    if-lez v14, :cond_8

    const/4 v14, 0x0

    cmpl-float v14, v13, v14

    if-lez v14, :cond_6

    const/4 v14, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/ublib/infocards/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v15

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_7

    const/4 v15, 0x1

    :goto_4
    if-ne v14, v15, :cond_8

    const/4 v5, 0x1

    .line 354
    .local v5, "childSwipedFastEnough":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v14

    if-eqz v14, :cond_9

    if-nez v5, :cond_4

    if-eqz v4, :cond_9

    :cond_4
    const/4 v7, 0x1

    .line 357
    .local v7, "dismissChild":Z
    :goto_6
    if-eqz v7, :cond_b

    .line 359
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v5, :cond_a

    .end local v13    # "velocity":F
    :goto_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lcom/google/android/ublib/infocards/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    goto/16 :goto_0

    .line 348
    .end local v4    # "childSwipedFarEnough":Z
    .end local v5    # "childSwipedFastEnough":Z
    .end local v7    # "dismissChild":Z
    .restart local v13    # "velocity":F
    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    .line 350
    .restart local v4    # "childSwipedFarEnough":Z
    :cond_6
    const/4 v14, 0x0

    goto :goto_3

    :cond_7
    const/4 v15, 0x0

    goto :goto_4

    :cond_8
    const/4 v5, 0x0

    goto :goto_5

    .line 354
    .restart local v5    # "childSwipedFastEnough":Z
    :cond_9
    const/4 v7, 0x0

    goto :goto_6

    .line 359
    .restart local v7    # "dismissChild":Z
    :cond_a
    const/4 v13, 0x0

    goto :goto_7

    .line 362
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v14, v15}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    .line 363
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v13}, Lcom/google/android/ublib/infocards/SwipeHelper;->snapChild(Landroid/view/View;F)V

    goto/16 :goto_0

    .line 315
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setHasDismissAnimation(Z)V
    .locals 0
    .param p1, "hasDismissAnimation"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mHasDismissAnimation:Z

    .line 102
    return-void
.end method

.method public setMinAlpha(F)V
    .locals 0
    .param p1, "minAlpha"    # F

    .prologue
    .line 161
    iput p1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mMinAlpha:F

    .line 162
    return-void
.end method

.method public setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/google/android/ublib/view/TranslationHelper;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 90
    return-void
.end method

.method public snapChild(Landroid/view/View;F)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "velocity"    # F

    .prologue
    .line 280
    iget-object v3, p0, Lcom/google/android/ublib/infocards/SwipeHelper;->mCallback:Lcom/google/android/ublib/infocards/SwipeHelper$Callback;

    invoke-interface {v3, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    .line 281
    .local v1, "canAnimViewBeDismissed":Z
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/google/android/ublib/infocards/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 282
    .local v0, "anim":Landroid/animation/ValueAnimator;
    const/16 v2, 0x96

    .line 283
    .local v2, "duration":I
    const-wide/16 v4, 0x96

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 284
    new-instance v3, Lcom/google/android/ublib/infocards/SwipeHelper$3;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$3;-><init>(Lcom/google/android/ublib/infocards/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 292
    new-instance v3, Lcom/google/android/ublib/infocards/SwipeHelper$4;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/ublib/infocards/SwipeHelper$4;-><init>(Lcom/google/android/ublib/infocards/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 301
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 302
    return-void
.end method
