.class public Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.source "PlayCardViewMedium.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 20
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 21
    .local v1, "availableWidth":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 23
    .local v0, "availableHeight":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->getPaddingTop()I

    move-result v5

    .line 24
    .local v5, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->getPaddingBottom()I

    move-result v2

    .line 25
    .local v2, "paddingBottom":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->getPaddingLeft()I

    move-result v3

    .line 26
    .local v3, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->getPaddingRight()I

    move-result v4

    .line 28
    .local v4, "paddingRight":I
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v12}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 30
    .local v7, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v12, v0, v5

    sub-int/2addr v12, v2

    iget v13, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v12, v13

    iget v13, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v6, v12, v13

    .line 32
    .local v6, "thumbnailHeight":I
    int-to-float v12, v6

    iget v13, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mThumbnailAspectRatio:F

    div-float/2addr v12, v13

    float-to-int v8, v12

    .line 33
    .local v8, "thumbnailWidth":I
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v12}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    iput v8, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 35
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v12, v8, v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->setThumbnailMetrics(II)V

    .line 37
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->measure(II)V

    .line 38
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 41
    .local v10, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v12, v1, v3

    sub-int/2addr v12, v4

    iget-object v13, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v13}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v13

    sub-int/2addr v12, v13

    iget v13, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v12, v13

    iget v13, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v12, v13

    iget v13, v10, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v12, v13

    iget v13, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v11, v12, v13

    .line 47
    .local v11, "widthForTitle":I
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mTitle:Landroid/widget/TextView;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/widget/TextView;->measure(II)V

    .line 48
    iget-object v12, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    .line 50
    .local v9, "titleLongWidth":I
    iget-object v13, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewMedium;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    if-le v9, v11, :cond_0

    const/4 v12, 0x2

    :goto_0
    invoke-virtual {v13, v12}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->setReasonMaxLines(I)V

    .line 52
    invoke-super/range {p0 .. p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onMeasure(II)V

    .line 53
    return-void

    .line 50
    :cond_0
    const/4 v12, 0x3

    goto :goto_0
.end method
