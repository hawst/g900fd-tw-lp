.class public abstract Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;
.super Ljava/lang/Object;
.source "DocumentMenuHandler.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public showPopupMenu(Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    .locals 7
    .param p1, "anchor"    # Landroid/view/View;
    .param p2, "systemUi"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    .line 34
    iget-object v5, p0, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;->mContext:Landroid/content/Context;

    invoke-static {v5, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PopupMenuFactory;->getInstance(Landroid/content/Context;Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    move-result-object v4

    .line 37
    .local v4, "popupMenu":Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 38
    .local v0, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-virtual {p0, v0}, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;->addMenuEntries(Ljava/util/List;)V

    .line 40
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;

    .line 41
    .local v1, "entry":Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
    new-instance v3, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler$1;

    invoke-direct {v3, p0, v1}, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler$1;-><init>(Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;)V

    .line 52
    .local v3, "listener":Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    iget-object v5, v1, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;->menuTitle:Ljava/lang/String;

    iget-boolean v6, v1, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;->isEnabled:Z

    invoke-interface {v4, v5, v6, v3}, Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    goto :goto_0

    .line 55
    .end local v1    # "entry":Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
    .end local v3    # "listener":Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;
    :cond_0
    invoke-interface {v4}, Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;->show()V

    .line 57
    return-object v4
.end method
