.class public Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardMetadata"
.end annotation


# instance fields
.field private final mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

.field private final mHSpan:I

.field private final mLayoutId:I

.field private final mThumbnailAspectRatio:F

.field private final mVSpan:I


# direct methods
.method public constructor <init>(IIIF)V
    .locals 6
    .param p1, "layoutId"    # I
    .param p2, "hSpan"    # I
    .param p3, "vSpan"    # I
    .param p4, "thumbnailAspectRatio"    # F

    .prologue
    .line 47
    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 48
    return-void
.end method

.method public constructor <init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V
    .locals 0
    .param p1, "layoutId"    # I
    .param p2, "hSpan"    # I
    .param p3, "vSpan"    # I
    .param p4, "thumbnailAspectRatio"    # F
    .param p5, "fillStyle"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    .line 40
    iput p2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    .line 41
    iput p3, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    .line 42
    iput p4, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    .line 43
    iput-object p5, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 44
    return-void
.end method


# virtual methods
.method public getFillStyle()Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    return-object v0
.end method

.method public getHSpan()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    return v0
.end method

.method public getThumbnailAspectRatio()F
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    return v0
.end method

.method public getVSpan()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    return v0
.end method
