.class public Lcom/google/android/ublib/cardlib/model/Document;
.super Ljava/lang/Object;
.source "Document.java"


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mIsAvailable:Z

.field private mPrice:Ljava/lang/String;

.field private mReason1:Ljava/lang/String;

.field private mReason2:Ljava/lang/String;

.field private mSubTitle:Ljava/lang/String;

.field private mThumbnailURI:Landroid/net/Uri;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/model/Document;->reset()V

    .line 32
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mPrice:Ljava/lang/String;

    return-object v0
.end method

.method public getReason1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason1:Ljava/lang/String;

    return-object v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mThumbnailURI:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mIsAvailable:Z

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mTitle:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    .line 137
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mThumbnailURI:Landroid/net/Uri;

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mPrice:Ljava/lang/String;

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mDescription:Ljava/lang/String;

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason1:Ljava/lang/String;

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason2:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mThumbnailURI:Landroid/net/Uri;

    .line 36
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mTitle:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mPrice:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mDescription:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason1:Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason2:Ljava/lang/String;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mIsAvailable:Z

    .line 45
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mDescription:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setIsAvailable(Z)V
    .locals 0
    .param p1, "isAvailable"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mIsAvailable:Z

    .line 105
    return-void
.end method

.method public setPrice(Ljava/lang/String;)V
    .locals 0
    .param p1, "price"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mPrice:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setReason1(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason1"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason1:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setSubTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setThumbnailURI(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mThumbnailURI:Landroid/net/Uri;

    .line 77
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mTitle:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Document [mTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSubTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ublib/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mSubTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mThumbnailURI:Landroid/net/Uri;

    invoke-static {p1, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;Landroid/net/Uri;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mPrice:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/model/Document;->mReason2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 128
    return-void
.end method
