.class public abstract Lcom/google/android/ublib/widget/TabFactory;
.super Ljava/lang/Object;
.source "TabFactory.java"


# instance fields
.field private final mTitleResourceId:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "titleResourceId"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/google/android/ublib/widget/TabFactory;->mTitleResourceId:I

    .line 16
    return-void
.end method


# virtual methods
.method public abstract createView(Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public getTitleResourceId()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/ublib/widget/TabFactory;->mTitleResourceId:I

    return v0
.end method

.method public onGainingPrimaryStatus(Landroid/view/View;)V
    .locals 0
    .param p1, "createdView"    # Landroid/view/View;

    .prologue
    .line 26
    return-void
.end method
