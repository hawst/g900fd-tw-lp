.class public Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClusterTileMetadata"
.end annotation


# instance fields
.field private final mCardMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private final mXStart:I

.field private final mYStart:I


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)V
    .locals 0
    .param p1, "cardMetadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p2, "xStart"    # I
    .param p3, "yStart"    # I

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->mCardMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 145
    iput p2, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->mXStart:I

    .line 146
    iput p3, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->mYStart:I

    .line 147
    return-void
.end method


# virtual methods
.method public getCardMetadata()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->mCardMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method public getXStart()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->mXStart:I

    return v0
.end method

.method public getYStart()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$ClusterTileMetadata;->mYStart:I

    return v0
.end method
