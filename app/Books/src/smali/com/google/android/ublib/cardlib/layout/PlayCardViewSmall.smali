.class public Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.source "PlayCardViewSmall.java"


# instance fields
.field private final mExtraVSpace:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    .line 25
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 48
    invoke-super/range {p0 .. p5}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onLayout(ZIIII)V

    .line 51
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v7}, Landroid/widget/RatingBar;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    .line 53
    .local v6, "secondary":Landroid/view/View;
    :goto_0
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReasonSeparator:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v8

    sub-int v5, v7, v8

    .line 54
    .local v5, "gap":I
    if-gtz v5, :cond_2

    .line 115
    :cond_0
    :goto_1
    return-void

    .line 51
    .end local v5    # "gap":I
    .end local v6    # "secondary":Landroid/view/View;
    :cond_1
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    goto :goto_0

    .line 59
    .restart local v5    # "gap":I
    .restart local v6    # "secondary":Landroid/view/View;
    :cond_2
    iget v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    mul-int/lit8 v7, v7, 0x3

    div-int/lit8 v7, v7, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 61
    .local v1, "bumpReason":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-virtual {v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getTop()I

    move-result v9

    sub-int/2addr v9, v1

    iget-object v10, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-virtual {v10}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReason1:Lcom/google/android/ublib/cardlib/layout/PlayCardReason;

    invoke-virtual {v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->getBottom()I

    move-result v11

    sub-int/2addr v11, v1

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardReason;->layout(IIII)V

    .line 65
    sub-int/2addr v5, v1

    .line 66
    if-lez v5, :cond_0

    .line 70
    iget v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    mul-int/lit8 v7, v7, 0x3

    div-int/lit8 v7, v7, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int v2, v7, v1

    .line 72
    .local v2, "bumpSeparator":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReasonSeparator:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReasonSeparator:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReasonSeparator:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v9

    sub-int/2addr v9, v2

    iget-object v10, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReasonSeparator:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mReasonSeparator:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    sub-int/2addr v11, v2

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 76
    sub-int/2addr v5, v2

    .line 77
    if-lez v5, :cond_0

    .line 81
    iget v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    mul-int/lit8 v7, v7, 0x3

    div-int/lit8 v7, v7, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 83
    .local v4, "bumpTitle":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTop()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v10, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getBottom()I

    move-result v11

    add-int/2addr v11, v4

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 89
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-virtual {v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getTop()I

    move-result v9

    add-int/2addr v9, v4

    iget-object v10, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-virtual {v10}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mOverflow:Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;

    invoke-virtual {v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->getBottom()I

    move-result v11

    add-int/2addr v11, v4

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardOverflowView;->layout(IIII)V

    .line 93
    sub-int/2addr v5, v4

    .line 94
    if-lez v5, :cond_0

    .line 98
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v7}, Landroid/widget/RatingBar;->getVisibility()I

    move-result v7

    if-nez v7, :cond_3

    .line 99
    iget v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int v0, v7, v4

    .line 101
    .local v0, "bumpRatingBar":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v8}, Landroid/widget/RatingBar;->getLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v9}, Landroid/widget/RatingBar;->getTop()I

    move-result v9

    add-int/2addr v9, v0

    iget-object v10, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v10}, Landroid/widget/RatingBar;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v11}, Landroid/widget/RatingBar;->getBottom()I

    move-result v11

    add-int/2addr v11, v0

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/RatingBar;->layout(IIII)V

    .line 107
    .end local v0    # "bumpRatingBar":I
    :cond_3
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_0

    .line 108
    iget v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mExtraVSpace:I

    mul-int/lit8 v7, v7, 0x3

    div-int/lit8 v7, v7, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    add-int v3, v7, v4

    .line 110
    .local v3, "bumpSubtitle":I
    iget-object v7, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getLeft()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTop()I

    move-result v9

    add-int/2addr v9, v3

    iget-object v10, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getBottom()I

    move-result v11

    add-int/2addr v11, v3

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 29
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 31
    .local v0, "availableWidth":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->getPaddingLeft()I

    move-result v1

    .line 32
    .local v1, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->getPaddingRight()I

    move-result v2

    .line 34
    .local v2, "paddingRight":I
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 36
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    sub-int v6, v0, v1

    sub-int/2addr v6, v2

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v6, v7

    .line 38
    .local v5, "thumbnailWidth":I
    iget v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mThumbnailAspectRatio:F

    int-to-float v7, v5

    mul-float/2addr v6, v7

    float-to-int v3, v6

    .line 39
    .local v3, "thumbnailHeight":I
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iput v3, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 41
    iget-object v6, p0, Lcom/google/android/ublib/cardlib/layout/PlayCardViewSmall;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v6, v5, v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->setThumbnailMetrics(II)V

    .line 43
    invoke-super {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onMeasure(II)V

    .line 44
    return-void
.end method
