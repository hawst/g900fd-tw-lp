.class Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;
.super Ljava/lang/Object;
.source "HoneycombSystemUi.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/ublib/view/HoneycombSystemUi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerWrapper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/ublib/view/HoneycombSystemUi;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/view/HoneycombSystemUi;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/HoneycombSystemUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/view/HoneycombSystemUi;Lcom/google/android/ublib/view/HoneycombSystemUi$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/view/HoneycombSystemUi;
    .param p2, "x1"    # Lcom/google/android/ublib/view/HoneycombSystemUi$1;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;-><init>(Lcom/google/android/ublib/view/HoneycombSystemUi;)V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/HoneycombSystemUi;

    # getter for: Lcom/google/android/ublib/view/HoneycombSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    invoke-static {v0}, Lcom/google/android/ublib/view/HoneycombSystemUi;->access$100(Lcom/google/android/ublib/view/HoneycombSystemUi;)Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/ublib/view/HoneycombSystemUi$ListenerWrapper;->this$0:Lcom/google/android/ublib/view/HoneycombSystemUi;

    # getter for: Lcom/google/android/ublib/view/HoneycombSystemUi;->mListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;
    invoke-static {v0}, Lcom/google/android/ublib/view/HoneycombSystemUi;->access$100(Lcom/google/android/ublib/view/HoneycombSystemUi;)Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    move-result-object v1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/ublib/view/SystemUi$VisibilityListener;->onSystemUiVisibilityChanged(Z)V

    .line 59
    :cond_0
    return-void

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
