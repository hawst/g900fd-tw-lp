.class Lcom/google/android/libraries/uploader/FutureWithCallback$1;
.super Landroid/os/AsyncTask;
.source "FutureWithCallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/uploader/FutureWithCallback;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/FutureWithCallback;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/uploader/FutureWithCallback;)V
    .locals 0

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback$1;, "Lcom/google/android/libraries/uploader/FutureWithCallback.1;"
    iput-object p1, p0, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->this$0:Lcom/google/android/libraries/uploader/FutureWithCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback$1;, "Lcom/google/android/libraries/uploader/FutureWithCallback.1;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback$1;, "Lcom/google/android/libraries/uploader/FutureWithCallback.1;"
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->this$0:Lcom/google/android/libraries/uploader/FutureWithCallback;

    # invokes: Ljava/util/concurrent/FutureTask;->run()V
    invoke-static {v1}, Lcom/google/android/libraries/uploader/FutureWithCallback;->access$001(Lcom/google/android/libraries/uploader/FutureWithCallback;)V

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->this$0:Lcom/google/android/libraries/uploader/FutureWithCallback;

    invoke-virtual {v1}, Lcom/google/android/libraries/uploader/FutureWithCallback;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 45
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "FutureWithCallback"

    const-string v2, "Interrupted while getting a future."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 43
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v1, "FutureWithCallback"

    const-string v2, "Exception while executing a get on a future."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback$1;, "Lcom/google/android/libraries/uploader/FutureWithCallback.1;"
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback$1;, "Lcom/google/android/libraries/uploader/FutureWithCallback.1;"
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->this$0:Lcom/google/android/libraries/uploader/FutureWithCallback;

    # getter for: Lcom/google/android/libraries/uploader/FutureWithCallback;->callback:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/libraries/uploader/FutureWithCallback;->access$100(Lcom/google/android/libraries/uploader/FutureWithCallback;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FutureWithCallback$1;->this$0:Lcom/google/android/libraries/uploader/FutureWithCallback;

    # getter for: Lcom/google/android/libraries/uploader/FutureWithCallback;->callback:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/libraries/uploader/FutureWithCallback;->access$100(Lcom/google/android/libraries/uploader/FutureWithCallback;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 53
    :cond_0
    return-void
.end method
