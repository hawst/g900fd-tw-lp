.class Lcom/google/android/libraries/uploader/AndroidTransfer$StatusFuturePair;
.super Lcom/google/android/libraries/uploader/FuturePair;
.source "AndroidTransfer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/uploader/AndroidTransfer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StatusFuturePair"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/uploader/FuturePair",
        "<",
        "Lcom/google/uploader/client/Transfer$Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/uploader/AndroidTransfer;Ljava/util/concurrent/RunnableFuture;Ljava/util/concurrent/RunnableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/RunnableFuture",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;",
            "Ljava/util/concurrent/RunnableFuture",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "future1":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    .local p3, "future2":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$StatusFuturePair;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    .line 82
    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/uploader/FuturePair;-><init>(Ljava/util/concurrent/RunnableFuture;Ljava/util/concurrent/RunnableFuture;)V

    .line 83
    return-void
.end method


# virtual methods
.method protected shouldRunSecondFuture(Lcom/google/uploader/client/Transfer$Status;)Z
    .locals 1
    .param p1, "result"    # Lcom/google/uploader/client/Transfer$Status;

    .prologue
    .line 92
    sget-object v0, Lcom/google/uploader/client/Transfer$Status;->ACTIVE:Lcom/google/uploader/client/Transfer$Status;

    if-ne p1, v0, :cond_0

    .line 93
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic shouldRunSecondFuture(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 74
    check-cast p1, Lcom/google/uploader/client/Transfer$Status;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/uploader/AndroidTransfer$StatusFuturePair;->shouldRunSecondFuture(Lcom/google/uploader/client/Transfer$Status;)Z

    move-result v0

    return v0
.end method
