.class public Lcom/google/android/libraries/bind/util/Util;
.super Ljava/lang/Object;
.source "Util.java"


# static fields
.field private static appContext:Landroid/content/Context;

.field private static memoryClass:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, -0x1

    sput v0, Lcom/google/android/libraries/bind/util/Util;->memoryClass:I

    return-void
.end method

.method public static checkPrecondition(Z)V
    .locals 1
    .param p0, "state"    # Z

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 33
    :cond_0
    return-void
.end method

.method public static checkPrecondition(ZLjava/lang/String;)V
    .locals 1
    .param p0, "state"    # Z
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 36
    if-nez p0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    return-void
.end method

.method public static getMemoryClass()I
    .locals 3

    .prologue
    .line 64
    sget v1, Lcom/google/android/libraries/bind/util/Util;->memoryClass:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 65
    sget-object v1, Lcom/google/android/libraries/bind/util/Util;->appContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 67
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    sput v1, Lcom/google/android/libraries/bind/util/Util;->memoryClass:I

    .line 71
    :cond_0
    sget v1, Lcom/google/android/libraries/bind/util/Util;->memoryClass:I

    return v1
.end method

.method public static getResourceName(I)Ljava/lang/String;
    .locals 1
    .param p0, "resId"    # I

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/libraries/bind/util/Util;->appContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 21
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/bind/util/Util;->appContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    .line 22
    :catch_0
    move-exception v0

    .line 26
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs hashCode([Ljava/lang/Object;)I
    .locals 7
    .param p0, "objects"    # [Ljava/lang/Object;

    .prologue
    .line 46
    if-nez p0, :cond_1

    .line 47
    const/4 v3, 0x0

    .line 60
    :cond_0
    return v3

    .line 49
    :cond_1
    const/4 v3, 0x1

    .line 50
    .local v3, "hashCode":I
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 53
    .local v1, "element":Ljava/lang/Object;
    if-nez v1, :cond_2

    .line 54
    const/4 v2, 0x0

    .line 58
    .local v2, "elementHashCode":I
    :goto_1
    mul-int/lit8 v6, v3, 0x1f

    add-int v3, v6, v2

    .line 50
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "elementHashCode":I
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    .restart local v2    # "elementHashCode":I
    goto :goto_1
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/util/Util;->appContext:Landroid/content/Context;

    .line 16
    return-void
.end method

.method public static isLowMemoryDevice()Z
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/libraries/bind/util/Util;->getMemoryClass()I

    move-result v0

    const/16 v1, 0x60

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "a"    # Ljava/lang/Object;
    .param p1, "b"    # Ljava/lang/Object;

    .prologue
    .line 42
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
