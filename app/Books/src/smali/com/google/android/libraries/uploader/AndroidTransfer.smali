.class public Lcom/google/android/libraries/uploader/AndroidTransfer;
.super Ljava/lang/Object;
.source "AndroidTransfer.java"

# interfaces
.implements Lcom/google/uploader/client/Transfer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/uploader/AndroidTransfer$StatusFuturePair;
    }
.end annotation


# instance fields
.field private handle:Lcom/google/uploader/client/ClientProto$TransferHandle;

.field private handler:Lcom/google/android/libraries/uploader/UploadHandler;

.field private protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

.field private responseBody:Ljava/io/InputStream;

.field private responseCode:I

.field private responseHeaders:Lcom/google/api/client/http/HttpHeaders;

.field private settings:Lcom/google/uploader/client/UploadSettings;

.field private uploadFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end field

.field private uploadStream:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Lcom/google/uploader/client/ClientProto$TransferHandle;Lcom/google/android/libraries/uploader/UploadProtocolFactory;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "handle"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p2, "factory"    # Lcom/google/android/libraries/uploader/UploadProtocolFactory;
    .param p3, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 125
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getUploadUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, p3}, Lcom/google/android/libraries/uploader/UploadProtocolFactory;->createUploadProtocol(Ljava/lang/String;Ljava/io/InputStream;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    .line 126
    iput-object p3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadStream:Ljava/io/InputStream;

    .line 127
    return-void
.end method

.method public constructor <init>(Lcom/google/uploader/client/UploadSettings;Lcom/google/android/libraries/uploader/UploadProtocolFactory;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "settings"    # Lcom/google/uploader/client/UploadSettings;
    .param p2, "factory"    # Lcom/google/android/libraries/uploader/UploadProtocolFactory;
    .param p3, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->settings:Lcom/google/uploader/client/UploadSettings;

    .line 110
    invoke-virtual {p1}, Lcom/google/uploader/client/UploadSettings;->getUploadUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, p3}, Lcom/google/android/libraries/uploader/UploadProtocolFactory;->createUploadProtocol(Ljava/lang/String;Ljava/io/InputStream;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    .line 111
    iput-object p3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadStream:Ljava/io/InputStream;

    .line 112
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/AndroidTransfer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadProtocol;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/AndroidTransfer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/uploader/AndroidTransfer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/AndroidTransfer;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->completeUpload()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/libraries/uploader/AndroidTransfer;Lcom/google/uploader/client/Transfer$Status;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/AndroidTransfer;
    .param p1, "x1"    # Lcom/google/uploader/client/Transfer$Status;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->callHandler(Lcom/google/uploader/client/Transfer$Status;)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/libraries/uploader/AndroidTransfer;Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/AndroidTransfer;
    .param p1, "x1"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object p1
.end method

.method private callHandler(Lcom/google/uploader/client/Transfer$Status;)V
    .locals 1
    .param p1, "status"    # Lcom/google/uploader/client/Transfer$Status;

    .prologue
    .line 343
    sget-object v0, Lcom/google/uploader/client/Transfer$Status;->ACTIVE:Lcom/google/uploader/client/Transfer$Status;

    if-ne p1, v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadHandler;->onUploadReady()V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 345
    :cond_1
    sget-object v0, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    if-ne p1, v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadHandler;->onUploadCompleted()V

    goto :goto_0

    .line 347
    :cond_2
    sget-object v0, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    if-ne p1, v0, :cond_3

    .line 348
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadHandler;->onUploadError()V

    goto :goto_0

    .line 349
    :cond_3
    sget-object v0, Lcom/google/uploader/client/Transfer$Status;->CANCELED:Lcom/google/uploader/client/Transfer$Status;

    if-ne p1, v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadHandler;->onUploadCanceled()V

    goto :goto_0
.end method

.method private completeUpload()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadProtocol;->getResponseCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->responseCode:I

    .line 359
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadProtocol;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    .line 360
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadProtocol;->getResponseBody()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->responseBody:Ljava/io/InputStream;

    .line 361
    return-void
.end method


# virtual methods
.method public cancel()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    new-instance v0, Lcom/google/android/libraries/uploader/FutureWithCallback;

    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v1}, Lcom/google/android/libraries/uploader/UploadProtocol;->cancel()Ljava/util/concurrent/Callable;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/uploader/AndroidTransfer$1;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/uploader/AndroidTransfer$1;-><init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/uploader/FutureWithCallback;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/Runnable;)V

    .line 184
    .local v0, "future":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->run()V

    .line 185
    return-object v0
.end method

.method public getResponseBody()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->responseBody:Ljava/io/InputStream;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->responseCode:I

    return v0
.end method

.method public getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method public getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object v0
.end method

.method public pause()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    new-instance v0, Lcom/google/android/libraries/uploader/FutureWithCallback;

    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v1}, Lcom/google/android/libraries/uploader/UploadProtocol;->cancel()Ljava/util/concurrent/Callable;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/uploader/AndroidTransfer$2;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/uploader/AndroidTransfer$2;-><init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/uploader/FutureWithCallback;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/Runnable;)V

    .line 218
    .local v0, "future":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->run()V

    .line 219
    return-object v0
.end method

.method public run()Ljava/util/concurrent/Future;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadFuture:Ljava/util/concurrent/Future;

    if-eqz v3, :cond_1

    .line 230
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v3}, Lcom/google/android/libraries/uploader/UploadProtocol;->getStatus()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v3

    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->PAUSED:Lcom/google/uploader/client/Transfer$Status;

    if-ne v3, v4, :cond_0

    .line 231
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    invoke-interface {v3}, Lcom/google/android/libraries/uploader/UploadProtocol;->resume()Ljava/util/concurrent/Callable;

    .line 233
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadFuture:Ljava/util/concurrent/Future;

    .line 311
    :goto_0
    return-object v3

    .line 237
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    if-eqz v3, :cond_2

    .line 239
    new-instance v0, Lcom/google/android/libraries/uploader/FutureWithCallback;

    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    iget-object v4, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadStream:Ljava/io/InputStream;

    invoke-interface {v3, v4}, Lcom/google/android/libraries/uploader/UploadProtocol;->upload(Ljava/io/InputStream;)Ljava/util/concurrent/Callable;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/uploader/AndroidTransfer$3;

    invoke-direct {v4, p0}, Lcom/google/android/libraries/uploader/AndroidTransfer$3;-><init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    invoke-direct {v0, v3, v4}, Lcom/google/android/libraries/uploader/FutureWithCallback;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/Runnable;)V

    .line 255
    .local v0, "future":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    iput-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadFuture:Ljava/util/concurrent/Future;

    .line 256
    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->run()V

    .line 257
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadFuture:Ljava/util/concurrent/Future;

    goto :goto_0

    .line 261
    .end local v0    # "future":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    :cond_2
    new-instance v1, Lcom/google/android/libraries/uploader/FutureWithCallback;

    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    iget-object v4, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->settings:Lcom/google/uploader/client/UploadSettings;

    invoke-virtual {v4}, Lcom/google/uploader/client/UploadSettings;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->settings:Lcom/google/uploader/client/UploadSettings;

    invoke-virtual {v5}, Lcom/google/uploader/client/UploadSettings;->getMetadata()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/libraries/uploader/UploadProtocol;->start(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/uploader/AndroidTransfer$4;

    invoke-direct {v4, p0}, Lcom/google/android/libraries/uploader/AndroidTransfer$4;-><init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/libraries/uploader/FutureWithCallback;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/Runnable;)V

    .line 287
    .local v1, "startFuture":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    new-instance v2, Lcom/google/android/libraries/uploader/FutureWithCallback;

    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;

    iget-object v4, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadStream:Ljava/io/InputStream;

    invoke-interface {v3, v4}, Lcom/google/android/libraries/uploader/UploadProtocol;->upload(Ljava/io/InputStream;)Ljava/util/concurrent/Callable;

    move-result-object v3

    new-instance v4, Lcom/google/android/libraries/uploader/AndroidTransfer$5;

    invoke-direct {v4, p0}, Lcom/google/android/libraries/uploader/AndroidTransfer$5;-><init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    invoke-direct {v2, v3, v4}, Lcom/google/android/libraries/uploader/FutureWithCallback;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/Runnable;)V

    .line 306
    .local v2, "uploadFuture":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    new-instance v0, Lcom/google/android/libraries/uploader/AndroidTransfer$StatusFuturePair;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/libraries/uploader/AndroidTransfer$StatusFuturePair;-><init>(Lcom/google/android/libraries/uploader/AndroidTransfer;Ljava/util/concurrent/RunnableFuture;Ljava/util/concurrent/RunnableFuture;)V

    .line 309
    .restart local v0    # "future":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<Lcom/google/uploader/client/Transfer$Status;>;"
    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->run()V

    .line 310
    iput-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadFuture:Ljava/util/concurrent/Future;

    .line 311
    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->uploadFuture:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public setHandler(Lcom/google/android/libraries/uploader/UploadHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/google/android/libraries/uploader/UploadHandler;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    .line 136
    return-void
.end method
