.class public Lcom/google/android/libraries/uploader/ResumableProtocol;
.super Ljava/lang/Object;
.source "ResumableProtocol.java"

# interfaces
.implements Lcom/google/android/libraries/uploader/UploadProtocol;


# instance fields
.field private backoffSeconds:J

.field private bytesTransferred:J

.field private estimatedBytesTransferred:J

.field private initialized:Z

.field private isPaused:Z

.field private markPoint:J

.field private metadata:Ljava/lang/String;

.field private final random:Ljava/util/Random;

.field private responseBody:Ljava/io/InputStream;

.field private responseCode:I

.field private responseHeaders:Lcom/google/api/client/http/HttpHeaders;

.field private startHeaders:Lcom/google/api/client/http/HttpHeaders;

.field private status:Lcom/google/uploader/client/Transfer$Status;

.field private uploadStream:Ljava/io/InputStream;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    .line 66
    iput-boolean v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->initialized:Z

    .line 67
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    .line 68
    iput-boolean v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->isPaused:Z

    .line 69
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->random:Ljava/util/Random;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/uploader/ResumableProtocol;)Lcom/google/uploader/client/Transfer$Status;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/ResumableProtocol;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/uploader/client/Transfer$Status;)Lcom/google/uploader/client/Transfer$Status;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/ResumableProtocol;
    .param p1, "x1"    # Lcom/google/uploader/client/Transfer$Status;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/ResumableProtocol;
    .param p1, "x1"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->handleStartResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/ResumableProtocol;
    .param p1, "x1"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->handleUploadResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/libraries/uploader/ResumableProtocol;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/ResumableProtocol;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->isPaused:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/ResumableProtocol;
    .param p1, "x1"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->handleCancelResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    return-object v0
.end method

.method private expBackoff()V
    .locals 8

    .prologue
    .line 394
    iget-boolean v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->isPaused:Z

    if-eqz v1, :cond_0

    .line 396
    monitor-enter p0

    .line 398
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    :goto_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->random:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    .line 408
    .local v2, "rand":D
    :try_start_2
    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    long-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 413
    :goto_1
    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    .line 414
    return-void

    .line 399
    .end local v2    # "rand":D
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v1, "ResumableProtocol"

    const-string v4, "Interrupted while waiting."

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 402
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 409
    .restart local v2    # "rand":D
    :catch_1
    move-exception v0

    .line 410
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    const-string v1, "ResumableProtocol"

    const-string v4, "Interrupted while trying to sleep."

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private handleCancelResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 3
    .param p1, "request"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 472
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    .line 473
    .local v0, "headers":Lcom/google/api/client/http/HttpHeaders;
    const/4 v1, 0x0

    .line 474
    .local v1, "uploadStatus":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "X-Goog-Upload-Status"

    invoke-static {v0, v2}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 477
    const-string v2, "cancelled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 478
    sget-object v2, Lcom/google/uploader/client/Transfer$Status;->CANCELED:Lcom/google/uploader/client/Transfer$Status;

    .line 481
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0
.end method

.method private handleQueryResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 10
    .param p1, "queryRequest"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 500
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    .line 501
    .local v1, "headers":Lcom/google/api/client/http/HttpHeaders;
    const/4 v3, 0x0

    .line 504
    .local v3, "uploadStatus":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v4, "X-Goog-Upload-Status"

    invoke-static {v1, v4}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 506
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->expBackoff()V

    .line 507
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->issueQueryRequest()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v4

    .line 565
    :goto_0
    return-object v4

    .line 513
    :cond_1
    const-string v4, "final"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 514
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->setFinalResponse(Lcom/google/android/libraries/uploader/HttpRequest;)V

    .line 515
    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 519
    :cond_2
    const-string v4, "cancelled"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 520
    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->CANCELED:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 524
    :cond_3
    const-string v4, "active"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseCode()I

    move-result v4

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_7

    .line 526
    const-string v4, "X-Goog-Upload-Size-Received"

    invoke-static {v1, v4}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 528
    .local v2, "uploadSizeReceivedHeader":Ljava/lang/String;
    if-nez v2, :cond_4

    .line 529
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->expBackoff()V

    .line 530
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->issueQueryRequest()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v4

    goto :goto_0

    .line 533
    :cond_4
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    .line 534
    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->estimatedBytesTransferred:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_6

    .line 538
    :try_start_0
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->reset()V

    .line 539
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    iget-wide v8, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->markPoint:J

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/io/InputStream;->skip(J)J

    .line 540
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    const/high16 v5, 0x40000

    invoke-virtual {v4, v5}, Ljava/io/InputStream;->mark(I)V

    .line 541
    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    iput-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->markPoint:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 553
    :cond_5
    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    iput-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->estimatedBytesTransferred:J

    .line 554
    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->ACTIVE:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "ResumableProtocol"

    const-string v5, "Cannot reset file, error out."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 546
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->estimatedBytesTransferred:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 548
    const-string v4, "ResumableProtocol"

    const-string v5, "Server thinks it has more bytes than we sent."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    goto/16 :goto_0

    .line 558
    .end local v2    # "uploadSizeReceivedHeader":Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseCode()I

    move-result v4

    div-int/lit8 v4, v4, 0x64

    const/4 v5, 0x4

    if-ne v4, v5, :cond_8

    .line 559
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->setFinalResponse(Lcom/google/android/libraries/uploader/HttpRequest;)V

    .line 560
    sget-object v4, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    goto/16 :goto_0

    .line 564
    :cond_8
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->expBackoff()V

    .line 565
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->issueQueryRequest()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v4

    goto/16 :goto_0
.end method

.method private handleStartResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 5
    .param p1, "request"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 347
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    .line 348
    .local v0, "headers":Lcom/google/api/client/http/HttpHeaders;
    const/4 v2, 0x0

    .line 349
    .local v2, "uploadStatus":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v3, "X-Goog-Upload-Status"

    invoke-static {v0, v3}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 353
    const-string v3, "final"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 354
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->setFinalResponse(Lcom/google/android/libraries/uploader/HttpRequest;)V

    .line 355
    sget-object v3, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    .line 386
    :goto_0
    return-object v3

    .line 359
    :cond_0
    const-string v3, "active"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseCode()I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 360
    const-string v3, "X-Goog-Upload-URL"

    invoke-static {v0, v3}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    .line 361
    sget-object v3, Lcom/google/uploader/client/Transfer$Status;->ACTIVE:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 365
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseCode()I

    move-result v3

    div-int/lit8 v3, v3, 0x64

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 366
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->setFinalResponse(Lcom/google/android/libraries/uploader/HttpRequest;)V

    .line 367
    sget-object v3, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 373
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseCode()I

    move-result v3

    const/16 v4, 0x194

    if-ne v3, v4, :cond_3

    .line 374
    sget-object v3, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 378
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getError()Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getError()Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    if-eq v3, v4, :cond_4

    .line 379
    sget-object v3, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 383
    :cond_4
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->expBackoff()V

    .line 384
    invoke-virtual {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->createStartRequest()Lcom/google/android/libraries/uploader/HttpRequest;

    move-result-object v1

    .line 385
    .local v1, "startRequest":Lcom/google/android/libraries/uploader/HttpRequest;
    invoke-virtual {v1}, Lcom/google/android/libraries/uploader/HttpRequest;->send()V

    .line 386
    invoke-direct {p0, v1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->handleStartResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v3

    goto :goto_0
.end method

.method private handleUploadResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    .locals 10
    .param p1, "request"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 426
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getError()Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    move-result-object v0

    .line 427
    .local v0, "error":Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
    if-eqz v0, :cond_1

    sget-object v5, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    if-eq v0, v5, :cond_1

    .line 428
    const-string v5, "ResumableProtocol"

    const-string v6, "Upload request had an error."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    sget-object v2, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    .line 461
    :cond_0
    :goto_0
    return-object v2

    .line 433
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    .line 434
    .local v1, "headers":Lcom/google/api/client/http/HttpHeaders;
    const/4 v4, 0x0

    .line 435
    .local v4, "uploadStatus":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string v5, "X-Goog-Upload-Status"

    invoke-static {v1, v5}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v5, "final"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 438
    invoke-direct {p0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->setFinalResponse(Lcom/google/android/libraries/uploader/HttpRequest;)V

    .line 439
    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getBytesTransferred()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    .line 440
    sget-object v2, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0

    .line 444
    :cond_2
    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->estimatedBytesTransferred:J

    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getEstimatedBytesTransferred()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->estimatedBytesTransferred:J

    .line 445
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->issueQueryRequest()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v2

    .line 446
    .local v2, "queryStatus":Lcom/google/uploader/client/Transfer$Status;
    sget-object v5, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    if-eq v2, v5, :cond_0

    sget-object v5, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    if-eq v2, v5, :cond_0

    sget-object v5, Lcom/google/uploader/client/Transfer$Status;->CANCELED:Lcom/google/uploader/client/Transfer$Status;

    if-eq v2, v5, :cond_0

    .line 454
    iget-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->estimatedBytesTransferred:J

    iget-wide v8, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 455
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->expBackoff()V

    .line 459
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->createUploadRequest()Lcom/google/android/libraries/uploader/HttpRequest;

    move-result-object v3

    .line 460
    .local v3, "uploadRequest":Lcom/google/android/libraries/uploader/HttpRequest;
    invoke-virtual {v3}, Lcom/google/android/libraries/uploader/HttpRequest;->send()V

    .line 461
    invoke-direct {p0, v3}, Lcom/google/android/libraries/uploader/ResumableProtocol;->handleUploadResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v2

    goto :goto_0

    .line 457
    .end local v3    # "uploadRequest":Lcom/google/android/libraries/uploader/HttpRequest;
    :cond_3
    const-wide/16 v6, 0x1

    iput-wide v6, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    goto :goto_1
.end method

.method private issueQueryRequest()Lcom/google/uploader/client/Transfer$Status;
    .locals 2

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->createQueryRequest()Lcom/google/android/libraries/uploader/HttpRequest;

    move-result-object v0

    .line 490
    .local v0, "queryRequest":Lcom/google/android/libraries/uploader/HttpRequest;
    invoke-virtual {v0}, Lcom/google/android/libraries/uploader/HttpRequest;->send()V

    .line 491
    invoke-direct {p0, v0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->handleQueryResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v1

    return-object v1
.end method

.method private setFinalResponse(Lcom/google/android/libraries/uploader/HttpRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/libraries/uploader/HttpRequest;

    .prologue
    .line 573
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->responseCode:I

    .line 574
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    .line 575
    invoke-virtual {p1}, Lcom/google/android/libraries/uploader/HttpRequest;->getResponseBody()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->responseBody:Ljava/io/InputStream;

    .line 576
    return-void
.end method


# virtual methods
.method public cancel()Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/uploader/ResumableProtocol$5;-><init>(Lcom/google/android/libraries/uploader/ResumableProtocol;)V

    return-object v0
.end method

.method protected createCancelRequest()Lcom/google/android/libraries/uploader/HttpRequest;
    .locals 5

    .prologue
    .line 326
    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 327
    .local v2, "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    const-string v3, "X-Goog-Upload-Command"

    const-string v4, "cancel"

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 330
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    const-string v4, "PUT"

    invoke-static {v3, v4, v2}, Lcom/google/android/libraries/uploader/HttpRequest;->createConnection(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 332
    .local v0, "connection":Ljava/net/HttpURLConnection;
    new-instance v3, Lcom/google/android/libraries/uploader/HttpRequest;

    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    invoke-direct {v3, v0, v4}, Lcom/google/android/libraries/uploader/HttpRequest;-><init>(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 334
    .end local v0    # "connection":Ljava/net/HttpURLConnection;
    :goto_0
    return-object v3

    .line 333
    :catch_0
    move-exception v1

    .line 334
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected createQueryRequest()Lcom/google/android/libraries/uploader/HttpRequest;
    .locals 5

    .prologue
    .line 288
    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 289
    .local v2, "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    const-string v3, "X-Goog-Upload-Command"

    const-string v4, "query"

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 292
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    const-string v4, "PUT"

    invoke-static {v3, v4, v2}, Lcom/google/android/libraries/uploader/HttpRequest;->createConnection(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 294
    .local v0, "connection":Ljava/net/HttpURLConnection;
    new-instance v3, Lcom/google/android/libraries/uploader/HttpRequest;

    invoke-direct {v3, v0}, Lcom/google/android/libraries/uploader/HttpRequest;-><init>(Ljava/net/HttpURLConnection;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    .end local v0    # "connection":Ljava/net/HttpURLConnection;
    :goto_0
    return-object v3

    .line 295
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected createStartRequest()Lcom/google/android/libraries/uploader/HttpRequest;
    .locals 6

    .prologue
    .line 262
    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 263
    .local v2, "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->startHeaders:Lcom/google/api/client/http/HttpHeaders;

    if-eqz v4, :cond_0

    .line 264
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->startHeaders:Lcom/google/api/client/http/HttpHeaders;

    invoke-virtual {v4}, Lcom/google/api/client/http/HttpHeaders;->clone()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v2

    .line 266
    :cond_0
    const-string v4, "X-Goog-Upload-Protocol"

    const-string v5, "resumable"

    invoke-virtual {v2, v4, v5}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 267
    const-string v4, "X-Goog-Upload-Command"

    const-string v5, "start"

    invoke-virtual {v2, v4, v5}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 268
    const/4 v3, 0x0

    .line 269
    .local v3, "stream":Ljava/io/InputStream;
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->metadata:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 270
    new-instance v3, Ljava/io/ByteArrayInputStream;

    .end local v3    # "stream":Ljava/io/InputStream;
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->metadata:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 274
    .restart local v3    # "stream":Ljava/io/InputStream;
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    const-string v5, "PUT"

    invoke-static {v4, v5, v2}, Lcom/google/android/libraries/uploader/HttpRequest;->createConnection(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 276
    .local v0, "connection":Ljava/net/HttpURLConnection;
    new-instance v4, Lcom/google/android/libraries/uploader/HttpRequest;

    invoke-direct {v4, v0, v3}, Lcom/google/android/libraries/uploader/HttpRequest;-><init>(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    .end local v0    # "connection":Ljava/net/HttpURLConnection;
    :goto_0
    return-object v4

    .line 277
    :catch_0
    move-exception v1

    .line 278
    .local v1, "e":Ljava/io/IOException;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected createUploadRequest()Lcom/google/android/libraries/uploader/HttpRequest;
    .locals 6

    .prologue
    .line 306
    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 307
    .local v2, "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    const-string v3, "X-Goog-Upload-Protocol"

    const-string v4, "resumable"

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 308
    const-string v3, "X-Goog-Upload-Command"

    const-string v4, "upload, finalize"

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 309
    const-string v3, "X-Goog-Upload-Offset"

    iget-wide v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->bytesTransferred:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 312
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    const-string v4, "PUT"

    invoke-static {v3, v4, v2}, Lcom/google/android/libraries/uploader/HttpRequest;->createConnection(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 314
    .local v0, "connection":Ljava/net/HttpURLConnection;
    new-instance v3, Lcom/google/android/libraries/uploader/HttpRequest;

    iget-object v4, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    invoke-direct {v3, v0, v4}, Lcom/google/android/libraries/uploader/HttpRequest;-><init>(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    .end local v0    # "connection":Ljava/net/HttpURLConnection;
    :goto_0
    return-object v3

    .line 315
    :catch_0
    move-exception v1

    .line 316
    .local v1, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getResponseBody()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->responseBody:Ljava/io/InputStream;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->responseCode:I

    return v0
.end method

.method public getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method public getStatus()Lcom/google/uploader/client/Transfer$Status;
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->isPaused:Z

    if-eqz v0, :cond_0

    .line 236
    sget-object v0, Lcom/google/uploader/client/Transfer$Status;->PAUSED:Lcom/google/uploader/client/Transfer$Status;

    .line 238
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0
.end method

.method public getUploadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->url:Ljava/lang/String;

    return-object v0
.end method

.method public resume()Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    new-instance v0, Lcom/google/android/libraries/uploader/ResumableProtocol$4;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/uploader/ResumableProtocol$4;-><init>(Lcom/google/android/libraries/uploader/ResumableProtocol;)V

    return-object v0
.end method

.method public start(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 2
    .param p1, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p2, "metadata"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/http/HttpHeaders;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->initialized:Z

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->initialized:Z

    .line 81
    iput-object p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->startHeaders:Lcom/google/api/client/http/HttpHeaders;

    .line 82
    iput-object p2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->metadata:Ljava/lang/String;

    .line 88
    new-instance v0, Lcom/google/android/libraries/uploader/ResumableProtocol$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/uploader/ResumableProtocol$1;-><init>(Lcom/google/android/libraries/uploader/ResumableProtocol;)V

    :goto_0
    return-object v0

    .line 84
    :cond_0
    const-string v0, "ResumableProtocol"

    const-string v1, "Tried to call start more than once."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public upload(Ljava/io/InputStream;)Ljava/util/concurrent/Callable;
    .locals 5
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x40000

    .line 113
    iget-object v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    if-nez v2, :cond_0

    .line 114
    iput-object p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->uploadStream:Ljava/io/InputStream;

    .line 121
    iget-object v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;

    sget-object v3, Lcom/google/uploader/client/Transfer$Status;->ACTIVE:Lcom/google/uploader/client/Transfer$Status;

    if-eq v2, v3, :cond_1

    .line 122
    const-string v2, "ResumableUpload"

    const-string v3, "Can\'t call upload which has already terminated."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :goto_0
    return-object v1

    .line 116
    :cond_0
    const-string v2, "ResumableUpload"

    const-string v3, "Called upload more than once."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 127
    :cond_1
    const-wide/16 v2, 0x1

    iput-wide v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->backoffSeconds:J

    .line 132
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 140
    :goto_1
    new-instance v1, Lcom/google/android/libraries/uploader/ResumableProtocol$2;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/uploader/ResumableProtocol$2;-><init>(Lcom/google/android/libraries/uploader/ResumableProtocol;)V

    goto :goto_0

    .line 135
    :cond_2
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 136
    .end local p1    # "stream":Ljava/io/InputStream;
    .local v0, "stream":Ljava/io/InputStream;
    invoke-virtual {v0, v4}, Ljava/io/InputStream;->mark(I)V

    .line 137
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol;->markPoint:J

    move-object p1, v0

    .end local v0    # "stream":Ljava/io/InputStream;
    .restart local p1    # "stream":Ljava/io/InputStream;
    goto :goto_1
.end method
