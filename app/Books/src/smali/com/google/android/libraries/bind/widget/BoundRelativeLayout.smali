.class public Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "BoundRelativeLayout.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private final boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundRelativeLayout;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 37
    return-void
.end method
