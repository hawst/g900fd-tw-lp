.class public Lcom/google/android/libraries/bind/data/LayoutResIdFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "LayoutResIdFilter.java"


# instance fields
.field private final originalResId:Ljava/lang/Integer;

.field private final resIdKey:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "originalResId"    # I
    .param p2, "resIdKey"    # I

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;->originalResId:Ljava/lang/Integer;

    .line 20
    iput p2, p0, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;->resIdKey:I

    .line 21
    return-void
.end method


# virtual methods
.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 3
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;->originalResId:Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    iget v2, p0, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;->resIdKey:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    const/4 p1, 0x0

    .line 34
    .end local p1    # "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_0
    return-object p1
.end method
