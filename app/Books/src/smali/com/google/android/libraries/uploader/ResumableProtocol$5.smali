.class Lcom/google/android/libraries/uploader/ResumableProtocol$5;
.super Ljava/lang/Object;
.source "ResumableProtocol.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/uploader/ResumableProtocol;->cancel()Ljava/util/concurrent/Callable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/uploader/client/Transfer$Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/uploader/ResumableProtocol;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/uploader/client/Transfer$Status;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 203
    iget-object v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    invoke-virtual {v1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->createCancelRequest()Lcom/google/android/libraries/uploader/HttpRequest;

    move-result-object v0

    .line 204
    .local v0, "cancelRequest":Lcom/google/android/libraries/uploader/HttpRequest;
    if-nez v0, :cond_0

    .line 206
    const-string v1, "ResumableProtocol"

    const-string v2, "Was not able to create cancel request."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    sget-object v2, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    # setter for: Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;
    invoke-static {v1, v2}, Lcom/google/android/libraries/uploader/ResumableProtocol;->access$002(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/uploader/client/Transfer$Status;)Lcom/google/uploader/client/Transfer$Status;

    .line 212
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    # getter for: Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;
    invoke-static {v1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->access$000(Lcom/google/android/libraries/uploader/ResumableProtocol;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v1

    return-object v1

    .line 209
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/uploader/HttpRequest;->send()V

    .line 210
    iget-object v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    iget-object v2, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    # invokes: Lcom/google/android/libraries/uploader/ResumableProtocol;->handleCancelResponse(Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;
    invoke-static {v2, v0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->access$400(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/android/libraries/uploader/HttpRequest;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v2

    # setter for: Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;
    invoke-static {v1, v2}, Lcom/google/android/libraries/uploader/ResumableProtocol;->access$002(Lcom/google/android/libraries/uploader/ResumableProtocol;Lcom/google/uploader/client/Transfer$Status;)Lcom/google/uploader/client/Transfer$Status;

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol$5;->call()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    return-object v0
.end method
