.class public interface abstract Lcom/google/android/libraries/uploader/UploadProtocol;
.super Ljava/lang/Object;
.source "UploadProtocol.java"


# virtual methods
.method public abstract cancel()Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResponseBody()Ljava/io/InputStream;
.end method

.method public abstract getResponseCode()I
.end method

.method public abstract getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;
.end method

.method public abstract getStatus()Lcom/google/uploader/client/Transfer$Status;
.end method

.method public abstract getUploadUrl()Ljava/lang/String;
.end method

.method public abstract resume()Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract start(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/http/HttpHeaders;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method

.method public abstract upload(Ljava/io/InputStream;)Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/uploader/client/Transfer$Status;",
            ">;"
        }
    .end annotation
.end method
