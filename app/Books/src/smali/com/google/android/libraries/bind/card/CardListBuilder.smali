.class public Lcom/google/android/libraries/bind/card/CardListBuilder;
.super Ljava/lang/Object;
.source "CardListBuilder.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataList$DataListListener;


# static fields
.field public static final DK_GROUP:I

.field public static final DK_GROUP_PROVIDER:I

.field public static final DK_ROW_ID:I

.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;


# instance fields
.field protected final appContext:Landroid/content/Context;

.field private autoGenCounter:I

.field private bottomPaddingEnabled:Z

.field private final cardList:Lcom/google/android/libraries/bind/data/DataList;

.field private cardListRegistered:Z

.field private groups:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/bind/card/CardGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private topPaddingEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListBuilder;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 32
    sget v0, Lcom/google/android/libraries/bind/R$id;->CardListBuilder_group:I

    sput v0, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP:I

    .line 33
    sget v0, Lcom/google/android/libraries/bind/R$id;->CardListBuilder_groupProvider:I

    sput v0, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP_PROVIDER:I

    .line 34
    sget v0, Lcom/google/android/libraries/bind/R$id;->CardListBuilder_rowId:I

    sput v0, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->rows:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    .line 40
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListBuilder$1;

    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/bind/card/CardListBuilder$1;-><init>(Lcom/google/android/libraries/bind/card/CardListBuilder;I)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    .line 69
    iput-boolean v2, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->topPaddingEnabled:Z

    .line 70
    iput-boolean v2, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->bottomPaddingEnabled:Z

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->appContext:Landroid/content/Context;

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/data/DataList;->addListener(Lcom/google/android/libraries/bind/data/DataList$DataListListener;)V

    .line 75
    return-void
.end method

.method private convertBuilderRows()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    const/4 v14, 0x0

    .line 364
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 365
    .local v11, "updatedGroups":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/libraries/bind/card/CardGroup;>;"
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->rows:Ljava/util/List;

    .line 366
    .local v0, "builderRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v3, 0x0

    .line 368
    .local v3, "firstException":Lcom/google/android/libraries/bind/data/DataException;
    :goto_0
    const/4 v4, 0x0

    .line 369
    .local v4, "foundGroup":Z
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v9, "outputList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 371
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v12, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP_PROVIDER:I

    invoke-virtual {v1, v12}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/bind/util/Provider;

    .line 372
    .local v7, "groupProvider":Lcom/google/android/libraries/bind/util/Provider;, "Lcom/google/android/libraries/bind/util/Provider<Lcom/google/android/libraries/bind/card/CardGroup;>;"
    if-nez v7, :cond_3

    sget v12, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP:I

    invoke-virtual {v1, v12}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/libraries/bind/card/CardGroup;

    move-object v5, v12

    .line 374
    .local v5, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    :goto_2
    if-eqz v5, :cond_5

    invoke-interface {v11, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 376
    invoke-interface {v11, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 377
    iget-object v12, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    invoke-interface {v12, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 379
    iget-boolean v12, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardListRegistered:Z

    if-eqz v12, :cond_0

    .line 381
    invoke-virtual {v5, p0}, Lcom/google/android/libraries/bind/card/CardGroup;->register(Lcom/google/android/libraries/bind/card/CardListBuilder;)V

    .line 383
    :cond_0
    sget v12, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {v1, v12}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v6

    .line 384
    .local v6, "groupId":Ljava/lang/String;
    if-eqz v6, :cond_4

    move v12, v13

    :goto_3
    invoke-static {v12}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 385
    invoke-virtual {v5, v6}, Lcom/google/android/libraries/bind/card/CardGroup;->setGroupId(Ljava/lang/String;)V

    .line 388
    .end local v6    # "groupId":Ljava/lang/String;
    :cond_1
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/libraries/bind/card/CardGroup;->getRows()Ljava/util/List;

    move-result-object v10

    .line 389
    .local v10, "rows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/google/android/libraries/bind/data/DataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    .end local v10    # "rows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_2
    :goto_4
    const/4 v4, 0x1

    goto :goto_1

    .line 372
    .end local v5    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    :cond_3
    invoke-interface {v7}, Lcom/google/android/libraries/bind/util/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/libraries/bind/card/CardGroup;

    move-object v5, v12

    goto :goto_2

    .restart local v5    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    .restart local v6    # "groupId":Ljava/lang/String;
    :cond_4
    move v12, v14

    .line 384
    goto :goto_3

    .line 390
    .end local v6    # "groupId":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 391
    .local v2, "e":Lcom/google/android/libraries/bind/data/DataException;
    if-nez v3, :cond_2

    .line 392
    move-object v3, v2

    goto :goto_4

    .line 397
    .end local v2    # "e":Lcom/google/android/libraries/bind/data/DataException;
    :cond_5
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 400
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v5    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    .end local v7    # "groupProvider":Lcom/google/android/libraries/bind/util/Provider;, "Lcom/google/android/libraries/bind/util/Provider<Lcom/google/android/libraries/bind/card/CardGroup;>;"
    :cond_6
    if-eqz v4, :cond_7

    .line 401
    move-object v0, v9

    goto :goto_0

    .line 404
    :cond_7
    iget-object v12, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/bind/card/CardGroup;

    .line 405
    .restart local v5    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    iget-boolean v12, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardListRegistered:Z

    if-eqz v12, :cond_8

    .line 406
    invoke-virtual {v5}, Lcom/google/android/libraries/bind/card/CardGroup;->unregister()V

    .line 408
    :cond_8
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Lcom/google/android/libraries/bind/card/CardGroup;->setGroupId(Ljava/lang/String;)V

    goto :goto_5

    .line 411
    .end local v5    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    :cond_9
    iput-object v11, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    .line 413
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_b

    .line 414
    iget-boolean v12, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->topPaddingEnabled:Z

    if-eqz v12, :cond_a

    .line 415
    const-string v12, "topPadding"

    invoke-virtual {p0, v12, v13}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makePadding(Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v12

    invoke-interface {v9, v14, v12}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 417
    :cond_a
    iget-boolean v12, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->bottomPaddingEnabled:Z

    if-eqz v12, :cond_b

    .line 418
    const-string v12, "bottomPadding"

    invoke-virtual {p0, v12, v14}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makePadding(Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    :cond_b
    if-eqz v3, :cond_c

    .line 422
    throw v3

    .line 424
    :cond_c
    return-object v9
.end method

.method private registerGroups()V
    .locals 3

    .prologue
    .line 457
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/CardGroup;

    .line 458
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/card/CardGroup;->register(Lcom/google/android/libraries/bind/card/CardListBuilder;)V

    goto :goto_0

    .line 460
    .end local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    :cond_0
    return-void
.end method

.method private setRowId(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "optRowId"    # Ljava/lang/String;

    .prologue
    .line 430
    move-object v0, p2

    .line 431
    .local v0, "rowId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 432
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "auto_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->autoGenCounter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 433
    iget v1, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->autoGenCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->autoGenCounter:I

    .line 435
    :cond_0
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 436
    return-object p1
.end method

.method private unregisterGroups()V
    .locals 3

    .prologue
    .line 463
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/CardGroup;

    .line 464
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardGroup;->unregister()V

    goto :goto_0

    .line 466
    .end local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    :cond_0
    return-void
.end method


# virtual methods
.method public append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 1
    .param p1, "row"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->append(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v0

    return-object v0
.end method

.method public append(Ljava/lang/String;Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 2
    .param p1, "optRowId"    # Ljava/lang/String;
    .param p2, "row"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 120
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->rows:Ljava/util/List;

    invoke-direct {p0, p2, p1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->setRowId(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    return-object p0
.end method

.method public cardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 327
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 328
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method public finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->refresh()V

    .line 92
    return-object p0
.end method

.method public haveAllGroupsRefreshedOnce()Z
    .locals 4

    .prologue
    .line 336
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->groups:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/CardGroup;

    .line 337
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 338
    .local v1, "groupList":Lcom/google/android/libraries/bind/data/DataList;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v3

    if-nez v3, :cond_0

    .line 339
    const/4 v3, 0x0

    .line 342
    .end local v0    # "group":Lcom/google/android/libraries/bind/card/CardGroup;
    .end local v1    # "groupList":Lcom/google/android/libraries/bind/data/DataList;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;
    .locals 2
    .param p1, "group"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 301
    .local v0, "groupData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 303
    return-object v0
.end method

.method protected makePadding(Ljava/lang/String;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "top"    # Z

    .prologue
    .line 317
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makePaddingRow(Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 318
    .local v0, "paddingRow":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->setRowId(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)Lcom/google/android/libraries/bind/data/Data;

    .line 319
    return-object v0
.end method

.method protected makePaddingRow(Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "top"    # Z

    .prologue
    .line 313
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardListPadding;->make()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public onDataListRegisteredForInvalidation()V
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardListRegistered:Z

    .line 471
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->registerGroups()V

    .line 472
    return-void
.end method

.method public onDataListUnregisteredForInvalidation()V
    .locals 1

    .prologue
    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardListRegistered:Z

    .line 477
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->unregisterGroups()V

    .line 478
    return-void
.end method

.method refresh()V
    .locals 5

    .prologue
    .line 346
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 351
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->convertBuilderRows()Ljava/util/List;

    move-result-object v0

    .line 356
    .local v0, "convertedRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->haveAllGroupsRefreshedOnce()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    sget-object v4, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 361
    .end local v0    # "convertedRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :goto_1
    return-void

    .line 356
    .restart local v0    # "convertedRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_0
    new-instance v2, Lcom/google/android/libraries/bind/data/Snapshot;

    sget v4, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-direct {v2, v4, v0}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V
    :try_end_0
    .catch Lcom/google/android/libraries/bind/data/DataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 358
    .end local v0    # "convertedRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :catch_0
    move-exception v1

    .line 359
    .local v1, "e":Lcom/google/android/libraries/bind/data/DataException;
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    new-instance v3, Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILcom/google/android/libraries/bind/data/DataException;)V

    new-instance v4, Lcom/google/android/libraries/bind/data/DataChange;

    invoke-direct {v4, v1}, Lcom/google/android/libraries/bind/data/DataChange;-><init>(Lcom/google/android/libraries/bind/data/DataException;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    goto :goto_1
.end method

.method public setBottomPaddingEnabled(Z)Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 0
    .param p1, "bottomPaddingEnabled"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->bottomPaddingEnabled:Z

    .line 108
    return-object p0
.end method

.method public setTopPaddingEnabled(Z)Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 0
    .param p1, "topPaddingEnabled"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/card/CardListBuilder;->topPaddingEnabled:Z

    .line 100
    return-object p0
.end method
