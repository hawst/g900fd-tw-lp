.class Lcom/google/android/libraries/uploader/AndroidTransfer$1;
.super Ljava/lang/Object;
.source "AndroidTransfer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/uploader/AndroidTransfer;->cancel()Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$1;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$1;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;
    invoke-static {v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$000(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$1;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;
    invoke-static {v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$100(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadProtocol;->getStatus()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->CANCELED:Lcom/google/uploader/client/Transfer$Status;

    if-ne v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$1;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;
    invoke-static {v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$000(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/uploader/UploadHandler;->onUploadCanceled()V

    .line 181
    :cond_0
    return-void
.end method
