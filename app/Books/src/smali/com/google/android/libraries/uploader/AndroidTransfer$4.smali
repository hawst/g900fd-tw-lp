.class Lcom/google/android/libraries/uploader/AndroidTransfer$4;
.super Ljava/lang/Object;
.source "AndroidTransfer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/uploader/AndroidTransfer;->run()Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 266
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;
    invoke-static {v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$100(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/libraries/uploader/UploadProtocol;->getStatus()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    .line 267
    .local v0, "state":Lcom/google/uploader/client/Transfer$Status;
    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->ACTIVE:Lcom/google/uploader/client/Transfer$Status;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->PAUSED:Lcom/google/uploader/client/Transfer$Status;

    if-ne v0, v1, :cond_1

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->newBuilder()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;
    invoke-static {v3}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$100(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/libraries/uploader/UploadProtocol;->getUploadUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->setUploadUrl(Ljava/lang/String;)Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->build()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v2

    # setter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->handle:Lcom/google/uploader/client/ClientProto$TransferHandle;
    invoke-static {v1, v2}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$402(Lcom/google/android/libraries/uploader/AndroidTransfer;Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 276
    :cond_1
    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    if-ne v0, v1, :cond_3

    .line 277
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # invokes: Lcom/google/android/libraries/uploader/AndroidTransfer;->completeUpload()V
    invoke-static {v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$200(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    .line 280
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;
    invoke-static {v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$000(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadHandler;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 281
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$4;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # invokes: Lcom/google/android/libraries/uploader/AndroidTransfer;->callHandler(Lcom/google/uploader/client/Transfer$Status;)V
    invoke-static {v1, v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$300(Lcom/google/android/libraries/uploader/AndroidTransfer;Lcom/google/uploader/client/Transfer$Status;)V

    .line 283
    :cond_4
    return-void
.end method
