.class public Lcom/google/android/libraries/uploader/AndroidClient;
.super Ljava/lang/Object;
.source "AndroidClient.java"

# interfaces
.implements Lcom/google/uploader/client/Client;


# instance fields
.field private handler:Lcom/google/android/libraries/uploader/UploadHandler;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/uploader/UploadHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/google/android/libraries/uploader/UploadHandler;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidClient;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    .line 24
    return-void
.end method


# virtual methods
.method public initTransfer(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/io/InputStream;)Lcom/google/uploader/client/Transfer;
    .locals 2
    .param p1, "handle"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p2, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/libraries/uploader/AndroidTransfer;

    new-instance v1, Lcom/google/android/libraries/uploader/UploadProtocolFactory;

    invoke-direct {v1}, Lcom/google/android/libraries/uploader/UploadProtocolFactory;-><init>()V

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/libraries/uploader/AndroidTransfer;-><init>(Lcom/google/uploader/client/ClientProto$TransferHandle;Lcom/google/android/libraries/uploader/UploadProtocolFactory;Ljava/io/InputStream;)V

    .line 48
    .local v0, "transfer":Lcom/google/android/libraries/uploader/AndroidTransfer;
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidClient;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->setHandler(Lcom/google/android/libraries/uploader/UploadHandler;)V

    .line 49
    return-object v0
.end method

.method public initTransfer(Lcom/google/uploader/client/UploadSettings;Ljava/io/InputStream;)Lcom/google/uploader/client/Transfer;
    .locals 2
    .param p1, "settings"    # Lcom/google/uploader/client/UploadSettings;
    .param p2, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/libraries/uploader/AndroidTransfer;

    new-instance v1, Lcom/google/android/libraries/uploader/UploadProtocolFactory;

    invoke-direct {v1}, Lcom/google/android/libraries/uploader/UploadProtocolFactory;-><init>()V

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/libraries/uploader/AndroidTransfer;-><init>(Lcom/google/uploader/client/UploadSettings;Lcom/google/android/libraries/uploader/UploadProtocolFactory;Ljava/io/InputStream;)V

    .line 35
    .local v0, "transfer":Lcom/google/android/libraries/uploader/AndroidTransfer;
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidClient;->handler:Lcom/google/android/libraries/uploader/UploadHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->setHandler(Lcom/google/android/libraries/uploader/UploadHandler;)V

    .line 36
    return-object v0
.end method
