.class Lcom/google/android/libraries/happiness/HatsSurveyManager$4;
.super Ljava/lang/Object;
.source "HatsSurveyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/happiness/HatsSurveyManager;->getSurveyDialog()Landroid/support/v4/app/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/happiness/HatsSurveyManager;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/happiness/HatsSurveyManager;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$4;->this$0:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$4;->this$0:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    iget-object v0, v0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    invoke-virtual {v0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->isComplete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$4;->this$0:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->declineSurvey()V

    .line 285
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$4;->this$0:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    iget-object v0, v0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    invoke-virtual {v0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->onSurveyCanceled()V

    .line 287
    :cond_0
    return-void
.end method
