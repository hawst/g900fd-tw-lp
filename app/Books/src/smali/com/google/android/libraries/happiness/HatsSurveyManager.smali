.class public Lcom/google/android/libraries/happiness/HatsSurveyManager;
.super Ljava/lang/Object;
.source "HatsSurveyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mClient:Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

.field private final mHandler:Landroid/os/Handler;

.field protected mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

.field protected final mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/happiness/HatsSurveyClient;Lcom/google/android/libraries/happiness/HatsSurveyParams;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "client"    # Lcom/google/android/libraries/happiness/HatsSurveyClient;
    .param p3, "params"    # Lcom/google/android/libraries/happiness/HatsSurveyParams;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iput-object p1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mContext:Landroid/content/Context;

    .line 157
    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    .line 158
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mHandler:Landroid/os/Handler;

    .line 159
    iput-object p3, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    .line 160
    new-instance v0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyClient;Landroid/os/Handler;Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    .line 162
    invoke-direct {p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->configureWebView()V

    .line 163
    invoke-direct {p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->configureCookieManager()V

    .line 164
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private configureCookieManager()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 168
    iget-object v12, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v13, "survey_url"

    invoke-virtual {v12, v13}, Lcom/google/android/libraries/happiness/HatsSurveyParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 169
    .local v8, "rawUrl":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    :try_start_0
    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 174
    .local v10, "url":Ljava/net/URL;
    invoke-virtual {v10}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 175
    .local v5, "host":Ljava/lang/String;
    const-string v12, "google."

    invoke-virtual {v5, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 176
    .local v7, "offset":I
    if-gez v7, :cond_2

    const/4 v7, 0x0

    .line 177
    :cond_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 184
    .local v6, "hostPrefix":Ljava/lang/String;
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v12, "EEE, dd-MMM-yyyy HH:mm:ss zzz"

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v9, v12, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 185
    .local v9, "sdf":Ljava/text/SimpleDateFormat;
    const-string v12, "GMT"

    invoke-static {v12}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 186
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 187
    .local v4, "expiresCal":Ljava/util/Calendar;
    invoke-virtual {v4, v14, v14}, Ljava/util/Calendar;->add(II)V

    .line 188
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 191
    .local v3, "expires":Ljava/lang/String;
    iget-object v12, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v13, "zwieback_cookie"

    invoke-virtual {v12, v13}, Lcom/google/android/libraries/happiness/HatsSurveyParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 192
    .local v11, "zwieback":Ljava/lang/String;
    if-eqz v11, :cond_0

    .line 195
    const-string v12, "NID="

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 197
    :goto_1
    iget-object v12, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v12}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 198
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 199
    .local v0, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v0, v14}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    .line 200
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "; expires="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "; path=/; domain="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "; HttpOnly"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "cookieValue":Ljava/lang/String;
    sget-object v12, Lcom/google/android/libraries/happiness/HatsSurveyManager;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "setting cookie on host="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", value="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {v0, v6, v1}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v12

    invoke-virtual {v12}, Landroid/webkit/CookieSyncManager;->sync()V

    goto/16 :goto_0

    .line 178
    .end local v0    # "cookieManager":Landroid/webkit/CookieManager;
    .end local v1    # "cookieValue":Ljava/lang/String;
    .end local v3    # "expires":Ljava/lang/String;
    .end local v4    # "expiresCal":Ljava/util/Calendar;
    .end local v5    # "host":Ljava/lang/String;
    .end local v6    # "hostPrefix":Ljava/lang/String;
    .end local v7    # "offset":I
    .end local v9    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v10    # "url":Ljava/net/URL;
    .end local v11    # "zwieback":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 179
    .local v2, "e":Ljava/net/MalformedURLException;
    new-instance v12, Ljava/lang/IllegalArgumentException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Value for HatsSurveyParams.SURVEY_URL_KEY is an invalid URL: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 195
    .end local v2    # "e":Ljava/net/MalformedURLException;
    .restart local v3    # "expires":Ljava/lang/String;
    .restart local v4    # "expiresCal":Ljava/util/Calendar;
    .restart local v5    # "host":Ljava/lang/String;
    .restart local v6    # "hostPrefix":Ljava/lang/String;
    .restart local v7    # "offset":I
    .restart local v9    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v10    # "url":Ljava/net/URL;
    .restart local v11    # "zwieback":Ljava/lang/String;
    :cond_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "NID="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1
.end method

.method private configureWebView()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 209
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 210
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 213
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 214
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 215
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v3, "user_agent"

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/happiness/HatsSurveyParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "userAgent":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 226
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    const-string v4, "_402m_native"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/google/android/libraries/happiness/HatsSurveyManager$1;

    invoke-direct {v3, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$1;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 237
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/google/android/libraries/happiness/HatsSurveyManager$2;

    invoke-direct {v3, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$2;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 245
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/google/android/libraries/happiness/HatsSurveyManager$3;

    invoke-direct {v3, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$3;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 269
    return-void
.end method

.method private wrapNativeCallbackJS(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "callback"    # Ljava/lang/String;
    .param p2, "paramNames"    # [Ljava/lang/String;

    .prologue
    .line 311
    if-nez p2, :cond_0

    const-string v0, ""

    .line 312
    .local v0, "params":Ljava/lang/String;
    :goto_0
    const-string v1, "_402m[\'%s\'] = function(%s) { _402m_native.%s(%s); };\n"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 311
    .end local v0    # "params":Ljava/lang/String;
    :cond_0
    const-string v1, ", "

    invoke-static {v1}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/common/base/Joiner;->join([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private wrappedResponseCallback()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    const-string v0, "_402m[\'onSurveyResponse\'] = function(response) {var surveyId = _402.params.svyid;_402m_native.onSurveyResponse(response, surveyId);};\n"

    .line 324
    .local v0, "js":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public declineSurvey()V
    .locals 2

    .prologue
    .line 365
    sget-object v0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->TAG:Ljava/lang/String;

    const-string v1, "declineSurvey() called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    const-string v1, "javascript:try { _402.close(true) } catch(e) {}"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 367
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    invoke-virtual {v0}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->dismiss()V

    .line 302
    :cond_0
    return-void
.end method

.method public getSurveyClientForTesting()Lcom/google/android/libraries/happiness/HatsSurveyClient;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    return-object v0
.end method

.method public getSurveyDialog()Landroid/support/v4/app/DialogFragment;
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    if-nez v0, :cond_0

    .line 279
    new-instance v0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    invoke-direct {v0}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    .line 280
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyManager$4;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$4;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->setOnCancelAction(Ljava/lang/Runnable;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->setWebView(Landroid/webkit/WebView;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    const/4 v1, 0x2

    const v2, 0x1030006

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->setStyle(II)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mDialog:Lcom/google/android/libraries/happiness/HatsSurveyDialog;

    return-object v0
.end method

.method public requestSurvey()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 330
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->onResume()V

    .line 331
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    const-string v3, "about:blank"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 333
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v3, "site_id"

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/happiness/HatsSurveyParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 338
    .local v1, "siteId":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<!doctype><html><head><meta name=\"viewport\" content=\"initial-scale=1.0,user-scalable=no\"><script>_402m = {};"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onWindowError"

    invoke-direct {p0, v3, v7}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->wrapNativeCallbackJS(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "window.onerror=function(){_402m.onWindowError();};"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onSurveyReady"

    invoke-direct {p0, v3, v7}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->wrapNativeCallbackJS(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onSurveyComplete"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "justAnswered"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "unused"

    aput-object v6, v4, v5

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->wrapNativeCallbackJS(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onSurveyCanceled"

    invoke-direct {p0, v3, v7}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->wrapNativeCallbackJS(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->wrappedResponseCallback()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v4, "_402m"

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/happiness/HatsSurveyParams;->toJS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</script>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->surveyScriptTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</head><body></body></html>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 359
    .local v0, "js":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mWebView:Landroid/webkit/WebView;

    const-string v3, "text/html"

    invoke-virtual {v2, v0, v3, v7}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return-void
.end method

.method protected surveyScriptTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "siteId"    # Ljava/lang/String;

    .prologue
    .line 306
    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager;->mParams:Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v2, "survey_url"

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/happiness/HatsSurveyParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "surveyUrl":Ljava/lang/String;
    const-string v1, "<script src=\"%s?site=%s&force_http=1\"></script>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
