.class public Lcom/google/android/libraries/happiness/HatsSurveyDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "HatsSurveyDialog.java"


# instance fields
.field private mOnCancelAction:Ljava/lang/Runnable;

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private bindWebview()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 69
    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebView:Landroid/webkit/WebView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebViewContainer:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 70
    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 71
    .local v0, "container":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 72
    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebViewContainer:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebView:Landroid/webkit/WebView;

    const/4 v3, 0x0

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 78
    .end local v0    # "container":Landroid/view/ViewGroup;
    :cond_1
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "unused"    # Landroid/content/DialogInterface;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mOnCancelAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mOnCancelAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 54
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 31
    iget-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebView:Landroid/webkit/WebView;

    if-nez v1, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 35
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 37
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->setCancelable(Z)V

    .line 43
    const v1, 0x7f0400c7

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 44
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e01dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebViewContainer:Landroid/view/ViewGroup;

    .line 45
    invoke-direct {p0}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->bindWebview()V

    .line 46
    return-object v0
.end method

.method public setOnCancelAction(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mOnCancelAction:Ljava/lang/Runnable;

    .line 58
    return-void
.end method

.method public setWebView(Landroid/webkit/WebView;)V
    .locals 0
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->mWebView:Landroid/webkit/WebView;

    .line 62
    invoke-direct {p0}, Lcom/google/android/libraries/happiness/HatsSurveyDialog;->bindWebview()V

    .line 63
    return-void
.end method
