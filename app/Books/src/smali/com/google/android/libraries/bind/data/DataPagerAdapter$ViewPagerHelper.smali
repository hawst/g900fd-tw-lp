.class public Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;
.super Ljava/lang/Object;
.source "DataPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/DataPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ViewPagerHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/data/DataPagerAdapter;

.field private view:Landroid/view/View;

.field viewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataPagerAdapter;Lcom/google/android/libraries/bind/data/ViewProvider;)V
    .locals 0
    .param p2, "viewProvider"    # Lcom/google/android/libraries/bind/data/ViewProvider;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->this$0:Lcom/google/android/libraries/bind/data/DataPagerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->viewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    .line 317
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 331
    return-void
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v2, -0x1

    .line 320
    if-lez p2, :cond_0

    .line 321
    const/4 p0, 0x0

    .line 326
    .end local p0    # "this":Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;
    :goto_0
    return-object p0

    .line 323
    .restart local p0    # "this":Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->viewProvider:Lcom/google/android/libraries/bind/data/ViewProvider;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->this$0:Lcom/google/android/libraries/bind/data/DataPagerAdapter;

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-interface {v0, p1, v1}, Lcom/google/android/libraries/bind/data/ViewProvider;->getView(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;

    .line 324
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
