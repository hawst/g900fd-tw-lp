.class Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "CardGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/card/CardGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditFilter"
.end annotation


# instance fields
.field private currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

.field private final tempOperations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/libraries/bind/card/CardGroup;


# direct methods
.method private applyOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;Ljava/util/List;Z)V
    .locals 7
    .param p1, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .param p3, "committed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 726
    .local p2, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    if-eqz p1, :cond_0

    .line 727
    iget v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    packed-switch v6, :pswitch_data_0

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 730
    :pswitch_0
    if-nez p3, :cond_0

    .line 731
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-direct {p0, p2, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->findId(Ljava/util/List;Ljava/lang/Object;)I

    move-result v5

    .line 732
    .local v5, "replacePos":I
    if-ltz v5, :cond_0

    .line 733
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 734
    .local v2, "originalCard":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v2}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->makePlaceHolder(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 735
    .local v3, "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {p2, v5, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 741
    .end local v2    # "originalCard":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    .end local v5    # "replacePos":I
    :pswitch_1
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-direct {p0, p2, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->findId(Ljava/util/List;Ljava/lang/Object;)I

    move-result v4

    .line 742
    .local v4, "removePos":I
    if-ltz v4, :cond_0

    .line 743
    invoke-interface {p2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 748
    .end local v4    # "removePos":I
    :pswitch_2
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-direct {p0, p2, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->findId(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 749
    .local v0, "editRow":I
    if-ltz v0, :cond_0

    .line 750
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 751
    .local v1, "newPosition":I
    invoke-interface {p2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 752
    .restart local v2    # "originalCard":Lcom/google/android/libraries/bind/data/Data;
    if-eqz p3, :cond_1

    .line 753
    invoke-interface {p2, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 755
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->makePlaceHolder(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 756
    .restart local v3    # "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {p2, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 727
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private findId(Ljava/util/List;Ljava/lang/Object;)I
    .locals 4
    .param p2, "id"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    .line 773
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    if-eqz p2, :cond_1

    .line 774
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 775
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v3}, Lcom/google/android/libraries/bind/card/CardGroup;->access$500(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 776
    .local v0, "currentId":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 781
    .end local v0    # "currentId":Ljava/lang/Object;
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 774
    .restart local v0    # "currentId":Ljava/lang/Object;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 781
    .end local v0    # "currentId":Ljava/lang/Object;
    .end local v1    # "i":I
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private makePlaceHolder(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "originalCard"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 764
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 765
    .local v0, "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v1

    .line 766
    .local v1, "primaryKey":I
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 767
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    iget v2, v2, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->placeHolderCardLayoutResId:I
    invoke-static {v3}, Lcom/google/android/libraries/bind/card/CardGroup;->access$400(Lcom/google/android/libraries/bind/card/CardGroup;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 768
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    iget v2, v2, Lcom/google/android/libraries/bind/card/CardGroup;->equalityFieldsKey:I

    sget-object v3, Lcom/google/android/libraries/bind/experimental/card/CardEditPlaceHolder;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 769
    return-object v0
.end method


# virtual methods
.method public clearCommittedOperations()V
    .locals 5

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardGroup;->access$200()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "Clearing %d temporary operations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 699
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 701
    :cond_0
    return-void
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 9
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 705
    const/4 v0, 0x0

    .line 706
    .local v0, "editOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    monitor-enter p0

    .line 707
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    if-nez v3, :cond_1

    const/4 v0, 0x0

    .line 708
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 711
    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardGroup;->access$200()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v3

    const-string v4, "Playing back %d temporary operations"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 713
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 714
    .local v2, "operation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    invoke-direct {p0, v2, p1, v8}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->applyOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;Ljava/util/List;Z)V

    goto :goto_1

    .line 707
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "operation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    :cond_1
    :try_start_1
    new-instance v3, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    invoke-direct {v3, v4}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;-><init>(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    move-object v0, v3

    goto :goto_0

    .line 708
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 717
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/bind/card/CardGroup;->allowEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 718
    invoke-direct {p0, v0, p1, v7}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->applyOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;Ljava/util/List;Z)V

    .line 721
    :cond_3
    return-object p1
.end method
