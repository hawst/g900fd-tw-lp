.class public Lcom/google/android/libraries/uploader/HttpRequest;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
    }
.end annotation


# instance fields
.field protected connection:Ljava/net/HttpURLConnection;

.field protected error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

.field private estimatedBytesTransferred:J

.field private final input:Ljava/io/InputStream;

.field private responseBody:Ljava/io/InputStream;

.field private responseCode:I

.field private responseHeaders:Lcom/google/api/client/http/HttpHeaders;


# direct methods
.method public constructor <init>(Ljava/net/HttpURLConnection;)V
    .locals 1
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/uploader/HttpRequest;-><init>(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/net/HttpURLConnection;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .param p2, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const-string v0, "connection"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    .line 91
    iput-object p2, p0, Lcom/google/android/libraries/uploader/HttpRequest;->input:Ljava/io/InputStream;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseCode:I

    .line 94
    return-void
.end method

.method public static createConnection(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;)Ljava/net/HttpURLConnection;
    .locals 6
    .param p0, "urlString"    # Ljava/lang/String;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 107
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 108
    .local v3, "url":Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 109
    .local v0, "conn":Ljava/net/HttpURLConnection;
    const/16 v4, 0x2710

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 110
    const/16 v4, 0x3a98

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 111
    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 113
    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 114
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 115
    invoke-virtual {p2}, Lcom/google/api/client/http/HttpHeaders;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 116
    .local v1, "headerName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 117
    invoke-static {p2, v1}, Lcom/google/uploader/util/HttpUtils;->getFirstHeaderStringValue(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    .end local v1    # "headerName":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private handleResponse()V
    .locals 7

    .prologue
    .line 212
    :try_start_0
    iget-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    iput v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseCode:I

    .line 213
    iget-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v1

    .line 214
    .local v1, "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v1, :cond_1

    .line 215
    new-instance v4, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v4}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    iput-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    .line 216
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 217
    .local v3, "key":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 218
    iget-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 224
    .end local v1    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->MALFORMED_RESPONSE:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 227
    const-string v4, "Uploader.HttpRequest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error parsing response: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 223
    .restart local v1    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseBody:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public getBytesTransferred()J
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 268
    const-wide/16 v0, 0x0

    .line 272
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->estimatedBytesTransferred:J

    goto :goto_0
.end method

.method public getError()Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    return-object v0
.end method

.method public getEstimatedBytesTransferred()J
    .locals 2

    .prologue
    .line 280
    iget-wide v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->estimatedBytesTransferred:J

    return-wide v0
.end method

.method public getResponseBody()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseBody:Ljava/io/InputStream;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseCode:I

    return v0
.end method

.method public getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/libraries/uploader/HttpRequest;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method public send()V
    .locals 3

    .prologue
    .line 130
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 143
    iget-object v1, p0, Lcom/google/android/libraries/uploader/HttpRequest;->input:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/libraries/uploader/HttpRequest;->writeBytesToOutputStream()Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/io/FileNotFoundException;
    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->UNRESOLVED_HOST:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v1, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 134
    const-string v1, "Uploader.HttpRequest"

    const-string v2, "Could not resolve host"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 136
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 137
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v1, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 138
    const-string v1, "Uploader.HttpRequest"

    const-string v2, "Could not connect."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 149
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/uploader/HttpRequest;->handleResponse()V

    goto :goto_0
.end method

.method protected setConnection(Ljava/net/HttpURLConnection;)V
    .locals 0
    .param p1, "connection"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    .line 204
    return-void
.end method

.method protected writeBytesToOutputStream()Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 157
    const/4 v4, 0x0

    .line 159
    .local v4, "outStream":Ljava/io/OutputStream;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 167
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->input:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v6

    if-lez v6, :cond_2

    .line 168
    const/high16 v6, 0x40000

    iget-object v7, p0, Lcom/google/android/libraries/uploader/HttpRequest;->input:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->available()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 169
    .local v2, "bytesToRead":I
    const/4 v1, 0x0

    .line 170
    .local v1, "bytesRead":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 171
    sub-int v6, v2, v1

    new-array v0, v6, [B

    .line 172
    .local v0, "buffer":[B
    iget-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->input:Ljava/io/InputStream;

    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 173
    const/4 v6, -0x1

    if-ne v1, v6, :cond_1

    .line 174
    const-string v6, "Uploader.HttpRequest"

    const-string v7, "Bytes read does not match available."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    sget-object v6, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->OUTPUT_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 194
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    .end local v2    # "bytesToRead":I
    :goto_1
    return v5

    .line 160
    :catch_0
    move-exception v3

    .line 161
    .local v3, "e":Ljava/io/IOException;
    const-string v6, "Uploader.HttpRequest"

    const-string v7, "Could not get output stream."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    sget-object v6, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->OUTPUT_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    goto :goto_1

    .line 180
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v2    # "bytesToRead":I
    :cond_1
    :try_start_2
    iget-wide v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->estimatedBytesTransferred:J

    int-to-long v8, v1

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->estimatedBytesTransferred:J

    .line 181
    invoke-virtual {v4, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 185
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    .end local v2    # "bytesToRead":I
    :catch_1
    move-exception v3

    .line 186
    .local v3, "e":Ljava/net/SocketException;
    const-string v6, "Uploader.HttpRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SocketException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    sget-object v6, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    goto :goto_1

    .line 184
    .end local v3    # "e":Ljava/net/SocketException;
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 194
    const/4 v5, 0x1

    goto :goto_1

    .line 189
    :catch_2
    move-exception v3

    .line 190
    .local v3, "e":Ljava/io/IOException;
    sget-object v6, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->OUTPUT_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    iput-object v6, p0, Lcom/google/android/libraries/uploader/HttpRequest;->error:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 191
    const-string v6, "Uploader.HttpRequest"

    const-string v7, "Could not write bytes."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
