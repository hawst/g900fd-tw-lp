.class Lcom/google/android/libraries/uploader/AndroidTransfer$5;
.super Ljava/lang/Object;
.source "AndroidTransfer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/uploader/AndroidTransfer;->run()Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/uploader/AndroidTransfer;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$5;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 292
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$5;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;
    invoke-static {v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$100(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/libraries/uploader/UploadProtocol;->getStatus()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    .line 294
    .local v0, "state":Lcom/google/uploader/client/Transfer$Status;
    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->COMPLETED:Lcom/google/uploader/client/Transfer$Status;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/uploader/client/Transfer$Status;->ERROR:Lcom/google/uploader/client/Transfer$Status;

    if-ne v0, v1, :cond_1

    .line 295
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$5;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # invokes: Lcom/google/android/libraries/uploader/AndroidTransfer;->completeUpload()V
    invoke-static {v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$200(Lcom/google/android/libraries/uploader/AndroidTransfer;)V

    .line 298
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$5;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->handler:Lcom/google/android/libraries/uploader/UploadHandler;
    invoke-static {v1}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$000(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadHandler;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 299
    iget-object v1, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$5;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    iget-object v2, p0, Lcom/google/android/libraries/uploader/AndroidTransfer$5;->this$0:Lcom/google/android/libraries/uploader/AndroidTransfer;

    # getter for: Lcom/google/android/libraries/uploader/AndroidTransfer;->protocol:Lcom/google/android/libraries/uploader/UploadProtocol;
    invoke-static {v2}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$100(Lcom/google/android/libraries/uploader/AndroidTransfer;)Lcom/google/android/libraries/uploader/UploadProtocol;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/libraries/uploader/UploadProtocol;->getStatus()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v2

    # invokes: Lcom/google/android/libraries/uploader/AndroidTransfer;->callHandler(Lcom/google/uploader/client/Transfer$Status;)V
    invoke-static {v1, v2}, Lcom/google/android/libraries/uploader/AndroidTransfer;->access$300(Lcom/google/android/libraries/uploader/AndroidTransfer;Lcom/google/uploader/client/Transfer$Status;)V

    .line 301
    :cond_2
    return-void
.end method
