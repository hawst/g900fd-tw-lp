.class public Lcom/google/android/libraries/bind/card/CardListView;
.super Landroid/widget/ListView;
.source "CardListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/card/CardListView$SavedState;,
        Lcom/google/android/libraries/bind/card/CardListView$CaptureData;
    }
.end annotation


# static fields
.field protected static final ENABLE_ANIMATION:Z

.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final a11yTempCount:[I

.field private static final alphaInterpolator:Landroid/view/animation/Interpolator;

.field private static listScrapBitmap:Landroid/graphics/Bitmap;

.field private static final translationInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private animateChanges:Z

.field private blendAnimationOnNextInvalidation:Z

.field private final captures:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/bind/card/CardListView$CaptureData;",
            ">;"
        }
    .end annotation
.end field

.field private invisibleHeight:I

.field private legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private final multicastOnScrollListener:Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

.field private final postUpdateObserver:Landroid/database/DataSetObserver;

.field private final preUpdateObserver:Landroid/database/DataSetObserver;

.field private stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

.field private final tempRect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/google/android/libraries/bind/card/CardListView;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/bind/card/CardListView;->ENABLE_ANIMATION:Z

    .line 60
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->translationInterpolator:Landroid/view/animation/Interpolator;

    .line 62
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->alphaInterpolator:Landroid/view/animation/Interpolator;

    .line 412
    const/4 v0, 0x3

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/card/CardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/card/CardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    new-instance v0, Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->multicastOnScrollListener:Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->animateChanges:Z

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->captures:Ljava/util/Map;

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->tempRect:Landroid/graphics/RectF;

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->invisibleHeight:I

    .line 86
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->setSelector(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->multicastOnScrollListener:Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 88
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardListView$1;-><init>(Lcom/google/android/libraries/bind/card/CardListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->postUpdateObserver:Landroid/database/DataSetObserver;

    .line 95
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardListView$2;-><init>(Lcom/google/android/libraries/bind/card/CardListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->preUpdateObserver:Landroid/database/DataSetObserver;

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/card/CardListView;)Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardListView;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    return-object v0
.end method

.method private capture(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->blendAnimationOnNextInvalidation:Z

    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getScrapBitmap()Landroid/graphics/Bitmap;

    .line 268
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/bind/card/CardListView;->traverse(Landroid/view/View;Z)V

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->blendAnimationOnNextInvalidation:Z

    .line 271
    return-void
.end method

.method private static capturePosition(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "ancestor"    # Landroid/view/View;
    .param p2, "result"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x0

    .line 274
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 275
    invoke-static {p2, p0, p1}, Lcom/google/android/libraries/bind/card/CardListView;->transformRelativeToParent(Landroid/graphics/RectF;Landroid/view/View;Landroid/view/View;)V

    .line 276
    return-object p2
.end method

.method private captureState()Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 496
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 497
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_0

    .line 499
    invoke-direct {p0, v1}, Lcom/google/android/libraries/bind/card/CardListView;->captureState(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    .line 507
    :goto_0
    return-object v0

    .line 501
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 504
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->captureState(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    goto :goto_0

    .line 507
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->captureState(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    goto :goto_0
.end method

.method private captureState(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    .locals 9
    .param p1, "preserveRowAtYPosition"    # I

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 514
    .local v0, "adapter":Landroid/widget/ListAdapter;
    instance-of v5, v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    if-eqz v5, :cond_0

    move-object v1, v0

    .line 515
    check-cast v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 516
    .local v1, "bindingDataAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {p0, v5, p1}, Lcom/google/android/libraries/bind/card/CardListView;->pointToPosition(II)I

    move-result v3

    .line 517
    .local v3, "rowIndex":I
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 518
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getRowFirstCardId(I)Ljava/lang/Object;

    move-result-object v2

    .line 519
    .local v2, "cardIdObject":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 520
    sget-object v5, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v6, "Saving state - cardId: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 521
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v5

    sub-int v5, v3, v5

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 522
    .local v4, "view":Landroid/view/View;
    new-instance v5, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v6

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-direct {v5, v6, v2, v7}, Lcom/google/android/libraries/bind/card/CardListView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/Object;I)V

    .line 526
    .end local v1    # "bindingDataAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    .end local v2    # "cardIdObject":Ljava/lang/Object;
    .end local v3    # "rowIndex":I
    .end local v4    # "view":Landroid/view/View;
    :goto_0
    return-object v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static clearScrapBitmap()V
    .locals 1

    .prologue
    .line 240
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 241
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 242
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    .line 244
    :cond_0
    return-void
.end method

.method private computeAccessibilityFirstAndLast()Z
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 414
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 415
    .local v0, "adapter":Landroid/widget/ListAdapter;
    instance-of v7, v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    if-eqz v7, :cond_3

    move-object v1, v0

    .line 416
    check-cast v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 417
    .local v1, "bindingAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    const/4 v2, 0x0

    .line 418
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 419
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getA11yRowCount(I)I

    move-result v4

    .line 420
    .local v4, "rowCount":I
    sget-object v7, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v8, "position %d, count: %d"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-virtual {v7, v8, v9}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 421
    add-int/2addr v2, v4

    .line 418
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 423
    .end local v4    # "rowCount":I
    :cond_0
    sget-object v7, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    aput v2, v7, v6

    .line 425
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v3

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getLastVisiblePosition()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 426
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getA11yRowCount(I)I

    move-result v4

    .line 427
    .restart local v4    # "rowCount":I
    sget-object v7, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v8, "position %d, count: %d"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-virtual {v7, v8, v9}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 428
    add-int/2addr v2, v4

    .line 425
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 430
    .end local v4    # "rowCount":I
    :cond_1
    sget-object v7, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    aput v2, v7, v5

    .line 432
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getLastVisiblePosition()I

    move-result v3

    :goto_2
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getCount()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 433
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getA11yRowCount(I)I

    move-result v4

    .line 434
    .restart local v4    # "rowCount":I
    sget-object v7, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v8, "position %d, count: %d"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-virtual {v7, v8, v9}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 435
    add-int/2addr v2, v4

    .line 432
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 437
    .end local v4    # "rowCount":I
    :cond_2
    sget-object v6, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    aput v2, v6, v11

    .line 440
    .end local v1    # "bindingAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    .end local v2    # "count":I
    .end local v3    # "i":I
    :goto_3
    return v5

    :cond_3
    move v5, v6

    goto :goto_3
.end method

.method private restoreSavedState(Lcom/google/android/libraries/bind/card/CardListView$SavedState;)V
    .locals 9
    .param p1, "savedState"    # Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .prologue
    const/4 v7, 0x0

    .line 531
    iget-object v2, p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->cardId:Ljava/lang/Object;

    .line 532
    .local v2, "cardId":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 533
    .local v0, "adapter":Landroid/widget/ListAdapter;
    instance-of v4, v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    if-eqz v4, :cond_0

    move-object v1, v0

    .line 534
    check-cast v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 535
    .local v1, "bindingDataAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->hasRefreshedOnce()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 536
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->findRowWithCardId(Ljava/lang/Object;)I

    move-result v3

    .line 537
    .local v3, "position":I
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 538
    iget v4, p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->offsetFromTop:I

    invoke-virtual {p0, v3, v4}, Lcom/google/android/libraries/bind/card/CardListView;->setSelectionFromTop(II)V

    .line 539
    sget-object v4, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "Restoring for cardId %s to position %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    const/4 v7, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 546
    .end local v1    # "bindingDataAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    .end local v3    # "position":I
    :cond_0
    :goto_0
    return-void

    .line 542
    .restart local v1    # "bindingDataAdapter":Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardListView;->stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 543
    sget-object v4, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "Stashing restore state"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static supportScrapBitmap()Z
    .locals 1

    .prologue
    .line 221
    invoke-static {}, Lcom/google/android/libraries/bind/util/Util;->isLowMemoryDevice()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static transformRelativeToParent(Landroid/graphics/RectF;Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p0, "rect"    # Landroid/graphics/RectF;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "ancestorView"    # Landroid/view/View;

    .prologue
    .line 280
    if-ne p1, p2, :cond_0

    .line 285
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 284
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {p0, v0, p2}, Lcom/google/android/libraries/bind/card/CardListView;->transformRelativeToParent(Landroid/graphics/RectF;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->multicastOnScrollListener:Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;->add(Landroid/widget/AbsListView$OnScrollListener;)Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    .line 197
    return-void
.end method

.method protected animateIfNeeded()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 400
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->animateChanges:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->captures:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 401
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "animateTransition"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 402
    sget-boolean v0, Lcom/google/android/libraries/bind/card/CardListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 403
    invoke-virtual {p0, p0, v3}, Lcom/google/android/libraries/bind/card/CardListView;->traverse(Landroid/view/View;Z)V

    .line 404
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->captures:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 407
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily(J)V

    .line 410
    :cond_0
    return-void
.end method

.method captureCardPositions()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 247
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "captureCardPositions"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    sget-boolean v0, Lcom/google/android/libraries/bind/card/CardListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->captures:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    invoke-static {p0}, Lcom/google/android/libraries/bind/widget/WidgetUtil;->isVisibleOnScreen(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "Skipping capture since we\'re offscreen"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 256
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "Skipping capture since we\'re offscreen"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    :cond_2
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "capturing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    invoke-direct {p0, p0}, Lcom/google/android/libraries/bind/card/CardListView;->capture(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected captureView(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;J)V
    .locals 6
    .param p1, "viewId"    # Ljava/lang/Object;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "optBlendMode"    # Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;
    .param p4, "animationDuration"    # J

    .prologue
    const/4 v2, 0x0

    .line 376
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;-><init>(Lcom/google/android/libraries/bind/card/CardListView$1;)V

    .line 377
    .local v0, "captureData":Lcom/google/android/libraries/bind/card/CardListView$CaptureData;
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-static {p2, p0, v3}, Lcom/google/android/libraries/bind/card/CardListView;->capturePosition(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v3

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-static {v3, v4}, Lcom/google/android/libraries/bind/util/RectUtil;->round(Landroid/graphics/RectF;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->position:Landroid/graphics/Rect;

    .line 378
    iput-wide p4, v0, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->animationDuration:J

    .line 379
    instance-of v3, p2, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v3, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getScrapBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 381
    check-cast p2, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    .end local p2    # "view":Landroid/view/View;
    sget-object v3, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->position:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget-object v5, v0, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->position:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    invoke-interface {p2, v3, v4, v5}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->captureToBitmap(Landroid/graphics/Bitmap;FF)Z

    move-result v1

    .line 383
    .local v1, "captured":Z
    if-eqz v1, :cond_1

    .end local p3    # "optBlendMode":Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;
    :goto_0
    iput-object p3, v0, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .line 385
    .end local v1    # "captured":Z
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->captures:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    return-void

    .restart local v1    # "captured":Z
    .restart local p3    # "optBlendMode":Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;
    :cond_1
    move-object p3, v2

    .line 383
    goto :goto_0
.end method

.method public clearOnScrollListeners()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->multicastOnScrollListener:Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;->clear()Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    .line 205
    return-void
.end method

.method protected disableClipChildren(Landroid/view/ViewParent;)V
    .locals 3
    .param p1, "viewParent"    # Landroid/view/ViewParent;

    .prologue
    .line 389
    instance-of v2, p1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 390
    check-cast v1, Landroid/view/ViewGroup;

    .line 391
    .local v1, "viewGroup":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 392
    invoke-interface {p1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 393
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    if-eq p1, p0, :cond_0

    .line 394
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->disableClipChildren(Landroid/view/ViewParent;)V

    .line 397
    .end local v0    # "parent":Landroid/view/ViewParent;
    .end local v1    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->animateIfNeeded()V

    .line 217
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 218
    return-void
.end method

.method protected getScrapBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 225
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardListView;->supportScrapBitmap()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 227
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 229
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardListView;->clearScrapBitmap()V

    .line 232
    :cond_1
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 233
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    .line 236
    :cond_2
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    .line 210
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->clearOnScrollListeners()V

    .line 212
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 447
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 448
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->computeAccessibilityFirstAndLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 450
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 451
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 453
    :cond_0
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->a11yTempCount:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 455
    :cond_1
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 465
    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "onRestoreInstanceState"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 466
    instance-of v1, p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 467
    check-cast v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 468
    .local v0, "savedState":Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 469
    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->restoreSavedState(Lcom/google/android/libraries/bind/card/CardListView$SavedState;)V

    .line 473
    .end local v0    # "savedState":Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    :goto_0
    return-void

    .line 471
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->captureState()Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    .line 460
    .local v0, "savedState":Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .end local v0    # "savedState":Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    :cond_0
    return-object v0
.end method

.method protected prepareInvalidationAnimation()V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->captureCardPositions()V

    .line 148
    return-void
.end method

.method public removeOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->multicastOnScrollListener:Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;->remove(Landroid/widget/AbsListView$OnScrollListener;)Lcom/google/android/libraries/bind/widget/MulticastOnScrollListener;

    .line 201
    return-void
.end method

.method restoreStashedStateIfNeeded()V
    .locals 4

    .prologue
    .line 550
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    if-eqz v1, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 552
    .local v0, "savedState":Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 553
    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "Trying to restore stashed state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 554
    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->restoreSavedState(Lcom/google/android/libraries/bind/card/CardListView$SavedState;)V

    .line 556
    .end local v0    # "savedState":Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 47
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 157
    .local v1, "oldAdapter":Landroid/widget/ListAdapter;
    if-eqz v1, :cond_0

    .line 158
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->postUpdateObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 159
    iget-boolean v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->animateChanges:Z

    if-eqz v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->preUpdateObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 163
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 164
    if-eqz p1, :cond_1

    move-object v0, p1

    .line 165
    check-cast v0, Lcom/google/android/libraries/bind/data/DataAdapter;

    .line 168
    .local v0, "dataAdapter":Lcom/google/android/libraries/bind/data/DataAdapter;
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->postUpdateObserver:Landroid/database/DataSetObserver;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/DataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;I)V

    .line 170
    iget-boolean v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->animateChanges:Z

    if-eqz v2, :cond_1

    .line 173
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardListView;->preUpdateObserver:Landroid/database/DataSetObserver;

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/DataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;I)V

    .line 177
    .end local v0    # "dataAdapter":Lcom/google/android/libraries/bind/data/DataAdapter;
    :cond_1
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 183
    if-nez p1, :cond_1

    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->removeOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 190
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardListView;->legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->legacyOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->addOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0

    .line 189
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public stashSavedState()V
    .locals 1

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->captureState()Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->stashedSavedState:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 481
    return-void
.end method

.method protected traverse(Landroid/view/View;Z)V
    .locals 21
    .param p1, "view"    # Landroid/view/View;
    .param p2, "capture"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 289
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/libraries/bind/data/DataView;

    if-eqz v2, :cond_7

    move-object/from16 v15, p1

    .line 290
    check-cast v15, Lcom/google/android/libraries/bind/data/DataView;

    .line 291
    .local v15, "dataView":Lcom/google/android/libraries/bind/data/DataView;
    invoke-interface {v15}, Lcom/google/android/libraries/bind/data/DataView;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v14

    .line 292
    .local v14, "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    if-eqz v14, :cond_0

    invoke-virtual {v14}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 293
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    .line 294
    .local v3, "id":Ljava/lang/Object;
    if-eqz p2, :cond_2

    .line 296
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/bind/card/CardListView;->blendAnimationOnNextInvalidation:Z

    if-eqz v2, :cond_1

    sget-object v5, Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;->FADE_SOURCE_ONLY:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    :goto_0
    const-wide/16 v6, 0xfa

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/libraries/bind/card/CardListView;->captureView(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;J)V

    .line 300
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 302
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 303
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 304
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 305
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    .line 363
    .end local v3    # "id":Ljava/lang/Object;
    .end local v14    # "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    .end local v15    # "dataView":Lcom/google/android/libraries/bind/data/DataView;
    :cond_0
    :goto_1
    return-void

    .line 296
    .restart local v3    # "id":Ljava/lang/Object;
    .restart local v14    # "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    .restart local v15    # "dataView":Lcom/google/android/libraries/bind/data/DataView;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 309
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/bind/card/CardListView;->captures:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;

    .line 310
    .local v12, "captureData":Lcom/google/android/libraries/bind/card/CardListView$CaptureData;
    if-eqz v12, :cond_6

    .line 311
    iget-object v0, v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->position:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    .line 314
    .local v17, "originalPosition":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/bind/card/CardListView;->tempRect:Landroid/graphics/RectF;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/bind/card/CardListView;->capturePosition(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 315
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/bind/card/CardListView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    sub-float v18, v2, v4

    .line 316
    .local v18, "tx":F
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/bind/card/CardListView;->tempRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    sub-float v19, v2, v4

    .line 317
    .local v19, "ty":F
    iget-object v2, v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v2, :cond_5

    const/4 v11, 0x1

    .line 318
    .local v11, "blendBitmap":Z
    :goto_2
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v4, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_3

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v4, 0x40a00000    # 5.0f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_3

    if-eqz v11, :cond_0

    .line 321
    :cond_3
    if-eqz v11, :cond_4

    move-object/from16 v5, p1

    .line 322
    check-cast v5, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    sget-object v6, Lcom/google/android/libraries/bind/card/CardListView;->listScrapBitmap:Landroid/graphics/Bitmap;

    iget-object v7, v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->position:Landroid/graphics/Rect;

    iget-wide v8, v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->animationDuration:J

    iget-object v10, v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    invoke-interface/range {v5 .. v10}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V

    .line 324
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 325
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 327
    :cond_4
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 328
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 329
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    .line 330
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-wide v4, v12, Lcom/google/android/libraries/bind/card/CardListView$CaptureData;->animationDuration:J

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v4, Lcom/google/android/libraries/bind/card/CardListView;->translationInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 338
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/card/CardListView;->disableClipChildren(Landroid/view/ViewParent;)V

    goto/16 :goto_1

    .line 317
    .end local v11    # "blendBitmap":Z
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 342
    .end local v17    # "originalPosition":Landroid/graphics/Rect;
    .end local v18    # "tx":F
    .end local v19    # "ty":F
    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 343
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v4, Lcom/google/android/libraries/bind/card/CardListView;->alphaInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 350
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v2, v4, :cond_0

    .line 351
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_1

    .line 356
    .end local v3    # "id":Ljava/lang/Object;
    .end local v12    # "captureData":Lcom/google/android/libraries/bind/card/CardListView$CaptureData;
    .end local v14    # "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    .end local v15    # "dataView":Lcom/google/android/libraries/bind/data/DataView;
    :cond_7
    move-object/from16 v0, p1

    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object/from16 v20, p1

    .line 357
    check-cast v20, Landroid/view/ViewGroup;

    .line 358
    .local v20, "viewGroup":Landroid/view/ViewGroup;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_3
    invoke-virtual/range {v20 .. v20}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_0

    .line 359
    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 360
    .local v13, "childView":Landroid/view/View;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v13, v1}, Lcom/google/android/libraries/bind/card/CardListView;->traverse(Landroid/view/View;Z)V

    .line 358
    add-int/lit8 v16, v16, 0x1

    goto :goto_3
.end method
