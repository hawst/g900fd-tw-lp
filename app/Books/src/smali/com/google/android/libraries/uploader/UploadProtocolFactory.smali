.class public Lcom/google/android/libraries/uploader/UploadProtocolFactory;
.super Ljava/lang/Object;
.source "UploadProtocolFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createUploadProtocol(Ljava/lang/String;Ljava/io/InputStream;)Lcom/google/android/libraries/uploader/UploadProtocol;
    .locals 1
    .param p1, "uploadUrl"    # Ljava/lang/String;
    .param p2, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/libraries/uploader/ResumableProtocol;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/uploader/ResumableProtocol;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
