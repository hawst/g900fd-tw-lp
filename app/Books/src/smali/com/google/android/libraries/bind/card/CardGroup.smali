.class public abstract Lcom/google/android/libraries/bind/card/CardGroup;
.super Ljava/lang/Object;
.source "CardGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;,
        Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;,
        Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;


# instance fields
.field protected final a11yCardCountKey:I

.field private cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

.field private editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

.field private editList:Lcom/google/android/libraries/bind/data/DataList;

.field private emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

.field protected final equalityFieldsKey:I

.field private errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

.field private final extraRowMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;>;"
        }
    .end annotation
.end field

.field private final filteredListIndexMap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final footers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private groupId:Ljava/lang/String;

.field private final groupList:Lcom/google/android/libraries/bind/data/DataList;

.field private final groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private final headerIndexMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private hideOnError:Z

.field private final listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

.field private placeHolderCardLayoutResId:I

.field private final realCardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private registered:Z

.field private rowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field protected final viewGeneratorKey:I

.field protected final viewResourceIdKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 95
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/card/CardGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 6
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 157
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_GENERATOR:I

    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_A11Y_COUNT:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/bind/card/CardGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;IIII)V

    .line 159
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;IIII)V
    .locals 3
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "viewResourceIdKey"    # I
    .param p3, "viewGeneratorKey"    # I
    .param p4, "equalityFieldsKey"    # I
    .param p5, "a11yCardCountKey"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->footers:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headerIndexMap:Ljava/util/Map;

    .line 115
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    .line 121
    sget v0, Lcom/google/android/libraries/bind/experimental/card/CardEditPlaceHolder;->LAYOUT:I

    iput v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->placeHolderCardLayoutResId:I

    .line 125
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardGroup$2;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 132
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$3;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardGroup$3;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 146
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 147
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    .line 148
    iput p2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    .line 149
    iput p3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewGeneratorKey:I

    .line 150
    iput p4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->equalityFieldsKey:I

    .line 151
    iput p5, p0, Lcom/google/android/libraries/bind/card/CardGroup;->a11yCardCountKey:I

    .line 152
    new-array v0, v1, [I

    aput p2, v0, v2

    new-instance v1, Lcom/google/android/libraries/bind/data/ImmediateReadOnlyFilter;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/ImmediateReadOnlyFilter;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    .line 154
    return-void

    :cond_0
    move v0, v2

    .line 146
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/bind/card/CardGroup;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->placeHolderCardLayoutResId:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method private addExtraRows()V
    .locals 5

    .prologue
    .line 504
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 505
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 507
    .local v1, "rowIndex":I
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v1, :cond_0

    .line 508
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "extra "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V

    .line 504
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 511
    .end local v1    # "rowIndex":I
    :cond_1
    return-void
.end method

.method private addRowAtPosition(ILcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "rowToAdd"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "rowId"    # Ljava/lang/String;

    .prologue
    .line 526
    invoke-virtual {p2}, Lcom/google/android/libraries/bind/data/Data;->copy()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 527
    .local v0, "rowCopy":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {v0, v1, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 528
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 529
    return-void
.end method

.method private addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V
    .locals 5
    .param p1, "position"    # I
    .param p3, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 514
    .local p2, "rowsToAdd":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le p1, v2, :cond_1

    .line 523
    :cond_0
    return-void

    .line 518
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 519
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/Data;->copy()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 520
    .local v1, "rowCopy":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->rowPrefix()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 521
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v2, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 518
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private getEmptyRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/DataProvider;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method private getErrorRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/DataProvider;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method private makeRowList()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 460
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v7

    .line 461
    .local v7, "list":Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v1

    .line 462
    .local v1, "externalGroupRows":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;>;"
    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v10

    .line 463
    .local v10, "rowCount":I
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 464
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 465
    const/4 v6, -0x1

    .line 470
    .local v6, "insertAfterCard":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v10, :cond_2

    .line 471
    invoke-virtual {v7, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    .line 472
    .local v9, "row":Lcom/google/android/libraries/bind/data/Data;
    sget v11, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP:I

    invoke-virtual {v9, v11}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v11

    if-nez v11, :cond_0

    sget v11, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP_PROVIDER:I

    invoke-virtual {v9, v11}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 475
    :cond_0
    new-instance v11, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;

    invoke-direct {v11, v6, v9}, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;-><init>(ILcom/google/android/libraries/bind/data/Data;)V

    invoke-virtual {v1, v11}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    .line 470
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 478
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 479
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 480
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 483
    .end local v9    # "row":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v12

    invoke-virtual {p0, v11, v12}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRows(Ljava/util/List;I)Ljava/util/List;

    move-result-object v4

    .line 484
    .local v4, "groupRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 485
    .local v8, "outputRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v0, 0x0

    .line 486
    .local v0, "currentCard":I
    const/4 v5, 0x0

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    if-ge v5, v11, :cond_5

    .line 487
    :goto_3
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 488
    invoke-virtual {v1, v13}, Lcom/google/android/libraries/bind/collections/RingBuffer;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;

    .line 489
    .local v2, "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    iget v11, v2, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;->insertAfterCard:I

    if-lt v11, v0, :cond_4

    .line 495
    .end local v2    # "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    :cond_3
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;

    .line 496
    .local v3, "groupRow":Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
    iget-object v11, v3, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->row:Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    invoke-virtual {v3}, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->getCardCount()I

    move-result v11

    add-int/2addr v0, v11

    .line 486
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 492
    .end local v3    # "groupRow":Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
    .restart local v2    # "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    :cond_4
    iget-object v11, v2, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;->row:Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 493
    invoke-virtual {v1, v13}, Lcom/google/android/libraries/bind/collections/RingBuffer;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 499
    .end local v2    # "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    :cond_5
    return-object v8
.end method


# virtual methods
.method public addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 1
    .param p1, "header"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    return-object p0
.end method

.method public allowEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)Z
    .locals 6
    .param p1, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    const/4 v3, 0x0

    .line 346
    if-nez p1, :cond_1

    .line 366
    :cond_0
    :goto_0
    return v3

    .line 349
    :cond_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v5, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 350
    .local v1, "rowPos":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 353
    iget v4, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    packed-switch v4, :pswitch_data_0

    .line 366
    const/4 v3, 0x1

    goto :goto_0

    .line 355
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 357
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_REMOVABLE:I

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    goto :goto_0

    .line 360
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v5, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 361
    .local v2, "targetData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v2, :cond_0

    .line 364
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_EDITABLE:I

    invoke-virtual {v2, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    goto :goto_0

    .line 353
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getCardId(I)Ljava/lang/Object;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 572
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 573
    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v3}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 574
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 575
    .local v1, "mappedPosition":I
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 576
    .local v0, "list":Lcom/google/android/libraries/bind/data/DataList;
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .end local v0    # "list":Lcom/google/android/libraries/bind/data/DataList;
    .end local v1    # "mappedPosition":I
    :cond_0
    move v2, v4

    .line 572
    goto :goto_0

    :cond_1
    move v3, v4

    .line 573
    goto :goto_1
.end method

.method protected getMappedPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method getRows()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 408
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 409
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    if-nez v3, :cond_5

    .line 410
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v3

    if-eqz v3, :cond_1

    move v2, v4

    .line 411
    .local v2, "refreshError":Z
    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 412
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->getErrorRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 414
    .local v1, "errorRow":Lcom/google/android/libraries/bind/data/Data;
    if-nez v1, :cond_2

    iget-boolean v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->hideOnError:Z

    if-nez v3, :cond_2

    .line 417
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v3

    throw v3

    .end local v1    # "errorRow":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "refreshError":Z
    :cond_0
    move v3, v5

    .line 408
    goto :goto_0

    :cond_1
    move v2, v5

    .line 410
    goto :goto_1

    .line 421
    .restart local v1    # "errorRow":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "refreshError":Z
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    .line 422
    if-eqz v1, :cond_3

    .line 423
    const-string v3, "errorRow"

    invoke-direct {p0, v5, v1, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowAtPosition(ILcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    .line 431
    .end local v1    # "errorRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 433
    if-nez v2, :cond_4

    .line 434
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->addExtraRows()V

    .line 437
    :cond_4
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    const-string v4, "header"

    invoke-direct {p0, v5, v3, v4}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V

    .line 439
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->footers:Ljava/util/List;

    const-string v5, "footer"

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V

    .line 447
    .end local v2    # "refreshError":Z
    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    return-object v3

    .line 429
    .restart local v2    # "refreshError":Z
    :cond_6
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRowList()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    goto :goto_2

    .line 441
    :cond_7
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->getEmptyRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 442
    .local v0, "emptyRow":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 443
    const-string v3, "emptyRow"

    invoke-direct {p0, v5, v0, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowAtPosition(ILcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    goto :goto_3
.end method

.method groupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 375
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    return-object v0

    .line 374
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidateRows()V
    .locals 1

    .prologue
    .line 536
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 537
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    .line 538
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->refresh()V

    .line 541
    :cond_0
    return-void
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected list()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    goto :goto_0
.end method

.method protected makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 1
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "position"    # I
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 584
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/bind/card/CardGroup;->makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;
    .locals 10
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "position"    # I
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;
    .param p4, "viewResIdOverride"    # Ljava/lang/Integer;

    .prologue
    .line 590
    iget-object v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge p2, v8, :cond_1

    const/4 v8, 0x1

    :goto_0
    invoke-static {v8}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 591
    if-ltz p2, :cond_2

    const/4 v8, 0x1

    :goto_1
    invoke-static {v8}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 592
    iget-object v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v8, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 594
    .local v5, "mappedPosition":I
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 595
    .local v2, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez p4, :cond_3

    iget v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    invoke-virtual {v2, v8}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    :goto_2
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 597
    .local v7, "viewResId":I
    iget v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->equalityFieldsKey:I

    invoke-virtual {v2, v8}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    .line 598
    .local v3, "equalityFields":[I
    if-nez p4, :cond_4

    new-instance v4, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;

    iget v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    invoke-direct {v4, v7, v8}, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;-><init>(II)V

    .line 601
    .local v4, "filter":Lcom/google/android/libraries/bind/data/Filter;
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9, v4, v3}, Lcom/google/android/libraries/bind/data/DataList;->filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v1

    .line 603
    .local v1, "cardDataRow":Lcom/google/android/libraries/bind/data/FilteredDataRow;
    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8, p3}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    .local v0, "bindingViewGroup":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    move-object v6, v0

    .line 605
    check-cast v6, Landroid/view/View;

    .line 606
    .local v6, "view":Landroid/view/View;
    invoke-interface {v0, v1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 607
    invoke-interface {v0, p0, v5}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V

    .line 608
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->isEditable()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 609
    sget-object v8, Lcom/google/android/libraries/bind/card/CardGroup;->editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v6, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 611
    :cond_0
    return-object v6

    .line 590
    .end local v0    # "bindingViewGroup":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .end local v1    # "cardDataRow":Lcom/google/android/libraries/bind/data/FilteredDataRow;
    .end local v2    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "equalityFields":[I
    .end local v4    # "filter":Lcom/google/android/libraries/bind/data/Filter;
    .end local v5    # "mappedPosition":I
    .end local v6    # "view":Landroid/view/View;
    .end local v7    # "viewResId":I
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 591
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .restart local v2    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v5    # "mappedPosition":I
    :cond_3
    move-object v8, p4

    .line 595
    goto :goto_2

    .line 598
    .restart local v3    # "equalityFields":[I
    .restart local v7    # "viewResId":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_3
.end method

.method protected makeRowData(ILcom/google/android/libraries/bind/card/ViewGenerator;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "row"    # I
    .param p2, "viewGenerator"    # Lcom/google/android/libraries/bind/card/ViewGenerator;

    .prologue
    .line 551
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 552
    .local v0, "rowData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRowId(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 553
    iget v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewGeneratorKey:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 554
    iget v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->a11yCardCountKey:I

    new-instance v2, Lcom/google/android/libraries/bind/card/CardGroup$4;

    invoke-direct {v2, p0, p2}, Lcom/google/android/libraries/bind/card/CardGroup$4;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;Lcom/google/android/libraries/bind/card/ViewGenerator;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 559
    return-object v0
.end method

.method protected makeRowId(I)Ljava/lang/String;
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->rowPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract makeRows(Ljava/util/List;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;",
            ">;"
        }
    .end annotation
.end method

.method register(Lcom/google/android/libraries/bind/card/CardListBuilder;)V
    .locals 3
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 383
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 384
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 385
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 386
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 387
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 388
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    .line 389
    return-void

    :cond_1
    move v0, v2

    .line 383
    goto :goto_0
.end method

.method protected rowPrefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->groupId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method setGroupId(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupId"    # Ljava/lang/String;

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    .line 371
    return-void
.end method

.method public startEditing(Landroid/view/View;I)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 617
    sget-object v4, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "startEditing position: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 620
    .local v2, "parent":Landroid/view/ViewParent;
    const/4 v1, 0x0

    .line 621
    .local v1, "editableListView":Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;
    :goto_0
    if-eqz v2, :cond_0

    .line 622
    instance-of v4, v2, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;

    if-eqz v4, :cond_2

    move-object v1, v2

    .line 623
    check-cast v1, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;

    .line 628
    :cond_0
    if-nez v1, :cond_3

    .line 636
    :cond_1
    :goto_1
    return v3

    .line 626
    :cond_2
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_0

    .line 632
    :cond_3
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, p2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 633
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_EDITABLE:I

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 636
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, p2}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, p1, p0, p2, v3}, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->startEditing(Landroid/view/View;Lcom/google/android/libraries/bind/card/CardGroup;ILjava/lang/Object;)Z

    move-result v3

    goto :goto_1
.end method

.method unregister()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 392
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 393
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 395
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 396
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 398
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 399
    return-void

    :cond_0
    move v0, v1

    .line 393
    goto :goto_0
.end method
