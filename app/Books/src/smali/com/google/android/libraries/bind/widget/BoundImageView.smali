.class public Lcom/google/android/libraries/bind/widget/BoundImageView;
.super Landroid/widget/ImageView;
.source "BoundImageView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private bindDrawableKey:Ljava/lang/Integer;

.field private bindImageUriKey:Ljava/lang/Integer;

.field private final boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

.field private currentDrawableRef:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/widget/BoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v1, Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-direct {v1, p1, p2, p0}, Lcom/google/android/libraries/bind/data/BoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    .line 38
    sget-object v1, Lcom/google/android/libraries/bind/R$styleable;->BoundImageView:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 40
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundImageView_bindImageUri:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->bindImageUriKey:Ljava/lang/Integer;

    .line 41
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundImageView_bindDrawable:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->bindDrawableKey:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v3, 0x0

    .line 48
    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-virtual {v4, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 49
    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->bindImageUriKey:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 50
    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->bindImageUriKey:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 51
    .local v2, "imageUri":Ljava/lang/Object;
    instance-of v4, v2, Landroid/net/Uri;

    if-eqz v4, :cond_2

    .line 52
    check-cast v2, Landroid/net/Uri;

    .end local v2    # "imageUri":Ljava/lang/Object;
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageURI(Landroid/net/Uri;)V

    .line 58
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->bindDrawableKey:Ljava/lang/Integer;

    if-eqz v4, :cond_1

    .line 59
    if-nez p1, :cond_3

    move-object v1, v3

    .line 60
    .local v1, "drawableRef":Ljava/lang/Integer;
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->currentDrawableRef:Ljava/lang/Integer;

    invoke-static {v4, v1}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 61
    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->currentDrawableRef:Ljava/lang/Integer;

    .line 62
    if-nez v1, :cond_4

    move-object v0, v3

    .line 64
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "drawableRef":Ljava/lang/Integer;
    :cond_1
    return-void

    .line 54
    .restart local v2    # "imageUri":Ljava/lang/Object;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0

    .line 59
    .end local v2    # "imageUri":Ljava/lang/Object;
    :cond_3
    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->bindDrawableKey:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    .line 62
    .restart local v1    # "drawableRef":Ljava/lang/Integer;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2
.end method
