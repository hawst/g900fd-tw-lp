.class public interface abstract Lcom/google/android/libraries/bind/data/BindingViewGroup;
.super Ljava/lang/Object;
.source "BindingViewGroup.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;
    }
.end annotation


# virtual methods
.method public abstract blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V
.end method

.method public abstract captureToBitmap(Landroid/graphics/Bitmap;FF)Z
.end method

.method public abstract isOwnedByParent()Z
.end method

.method public abstract prepareForRecycling()V
.end method

.method public abstract setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V
.end method

.method public abstract setMeasuredDimensionProxy(II)V
.end method

.method public abstract setOwnedByParent(Z)V
.end method

.method public abstract startEditingIfPossible()Z
.end method

.method public abstract superDrawProxy(Landroid/graphics/Canvas;)V
.end method

.method public abstract updateBoundDataProxy(Lcom/google/android/libraries/bind/data/Data;)V
.end method
