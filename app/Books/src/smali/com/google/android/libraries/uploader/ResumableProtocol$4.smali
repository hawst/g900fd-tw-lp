.class Lcom/google/android/libraries/uploader/ResumableProtocol$4;
.super Ljava/lang/Object;
.source "ResumableProtocol.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/uploader/ResumableProtocol;->resume()Ljava/util/concurrent/Callable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/uploader/client/Transfer$Status;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/uploader/ResumableProtocol;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$4;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/uploader/client/Transfer$Status;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$4;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/libraries/uploader/ResumableProtocol;->isPaused:Z
    invoke-static {v0, v1}, Lcom/google/android/libraries/uploader/ResumableProtocol;->access$302(Lcom/google/android/libraries/uploader/ResumableProtocol;Z)Z

    .line 185
    iget-object v1, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$4;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    monitor-enter v1

    .line 186
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$4;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 187
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/uploader/ResumableProtocol$4;->this$0:Lcom/google/android/libraries/uploader/ResumableProtocol;

    # getter for: Lcom/google/android/libraries/uploader/ResumableProtocol;->status:Lcom/google/uploader/client/Transfer$Status;
    invoke-static {v0}, Lcom/google/android/libraries/uploader/ResumableProtocol;->access$000(Lcom/google/android/libraries/uploader/ResumableProtocol;)Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    return-object v0

    .line 187
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/libraries/uploader/ResumableProtocol$4;->call()Lcom/google/uploader/client/Transfer$Status;

    move-result-object v0

    return-object v0
.end method
