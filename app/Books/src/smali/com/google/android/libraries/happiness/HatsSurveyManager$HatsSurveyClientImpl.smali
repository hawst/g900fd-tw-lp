.class public Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;
.super Ljava/lang/Object;
.source "HatsSurveyManager.java"

# interfaces
.implements Lcom/google/android/libraries/happiness/HatsSurveyClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/happiness/HatsSurveyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "HatsSurveyClientImpl"
.end annotation


# instance fields
.field private isComplete:Z

.field private final mClient:Lcom/google/android/libraries/happiness/HatsSurveyClient;

.field private final mHandler:Landroid/os/Handler;

.field private final mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/happiness/HatsSurveyClient;Landroid/os/Handler;Landroid/webkit/WebView;)V
    .locals 1
    .param p1, "client"    # Lcom/google/android/libraries/happiness/HatsSurveyClient;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->isComplete:Z

    .line 75
    iput-object p1, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyClient;

    .line 76
    iput-object p2, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mHandler:Landroid/os/Handler;

    .line 77
    iput-object p3, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mWebView:Landroid/webkit/WebView;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;)Lcom/google/android/libraries/happiness/HatsSurveyClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mClient:Lcom/google/android/libraries/happiness/HatsSurveyClient;

    return-object v0
.end method


# virtual methods
.method public isComplete()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->isComplete:Z

    return v0
.end method

.method public onSurveyCanceled()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$5;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$5;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 148
    return-void
.end method

.method public onSurveyComplete(ZZ)V
    .locals 2
    .param p1, "justAnswered"    # Z
    .param p2, "unused"    # Z
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->isComplete:Z

    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$3;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 118
    return-void
.end method

.method public onSurveyReady()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->isComplete:Z

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$2;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 106
    return-void
.end method

.method public onSurveyResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "surveyId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 128
    const-string v0, "t=a"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$4;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onWindowError()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->isComplete:Z

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$1;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl$1;-><init>(Lcom/google/android/libraries/happiness/HatsSurveyManager$HatsSurveyClientImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 94
    return-void
.end method
