.class public Lcom/google/android/libraries/uploader/FutureWithCallback;
.super Ljava/util/concurrent/FutureTask;
.source "FutureWithCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private callback:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "callback"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback;, "Lcom/google/android/libraries/uploader/FutureWithCallback<TV;>;"
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TV;>;"
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 28
    iput-object p2, p0, Lcom/google/android/libraries/uploader/FutureWithCallback;->callback:Ljava/lang/Runnable;

    .line 29
    return-void
.end method

.method static synthetic access$001(Lcom/google/android/libraries/uploader/FutureWithCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/FutureWithCallback;

    .prologue
    .line 14
    invoke-super {p0}, Ljava/util/concurrent/FutureTask;->run()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/uploader/FutureWithCallback;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/uploader/FutureWithCallback;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FutureWithCallback;->callback:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/libraries/uploader/FutureWithCallback;, "Lcom/google/android/libraries/uploader/FutureWithCallback<TV;>;"
    new-instance v0, Lcom/google/android/libraries/uploader/FutureWithCallback$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/uploader/FutureWithCallback$1;-><init>(Lcom/google/android/libraries/uploader/FutureWithCallback;)V

    .line 55
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 56
    return-void
.end method
