.class public final enum Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
.super Ljava/lang/Enum;
.source "HttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/uploader/HttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HttpError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/libraries/uploader/HttpRequest$HttpError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

.field public static final enum CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

.field public static final enum GENERIC_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

.field public static final enum MALFORMED_RESPONSE:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

.field public static final enum OUTPUT_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

.field public static final enum UNRESOLVED_HOST:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    const-string v1, "OUTPUT_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->OUTPUT_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 67
    new-instance v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    const-string v1, "CONNECTION_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 68
    new-instance v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    const-string v1, "MALFORMED_RESPONSE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->MALFORMED_RESPONSE:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 69
    new-instance v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    const-string v1, "UNRESOLVED_HOST"

    invoke-direct {v0, v1, v5}, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->UNRESOLVED_HOST:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 70
    new-instance v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    const-string v1, "GENERIC_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->GENERIC_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->OUTPUT_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->CONNECTION_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->MALFORMED_RESPONSE:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->UNRESOLVED_HOST:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->GENERIC_ERROR:Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->$VALUES:[Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/uploader/HttpRequest$HttpError;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->$VALUES:[Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    invoke-virtual {v0}, [Lcom/google/android/libraries/uploader/HttpRequest$HttpError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/uploader/HttpRequest$HttpError;

    return-object v0
.end method
