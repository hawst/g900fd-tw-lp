.class public abstract Lcom/google/android/libraries/uploader/FuturePair;
.super Ljava/lang/Object;
.source "FuturePair.java"

# interfaces
.implements Ljava/util/concurrent/RunnableFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/RunnableFuture",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private future1:Ljava/util/concurrent/RunnableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/RunnableFuture",
            "<TV;>;"
        }
    .end annotation
.end field

.field private future2:Ljava/util/concurrent/RunnableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/RunnableFuture",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/RunnableFuture;Ljava/util/concurrent/RunnableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/RunnableFuture",
            "<TV;>;",
            "Ljava/util/concurrent/RunnableFuture",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    .local p1, "future1":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<TV;>;"
    .local p2, "future2":Ljava/util/concurrent/RunnableFuture;, "Ljava/util/concurrent/RunnableFuture<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    .line 26
    iput-object p2, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    .line 27
    return-void
.end method


# virtual methods
.method public cancel(Z)Z
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/RunnableFuture;->cancel(Z)Z

    move-result v0

    .line 39
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/RunnableFuture;->cancel(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->get()Ljava/lang/Object;

    move-result-object v0

    .line 48
    .local v0, "result":Ljava/lang/Object;, "TV;"
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/uploader/FuturePair;->shouldRunSecondFuture(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->get()Ljava/lang/Object;

    move-result-object v0

    .line 52
    .end local v0    # "result":Ljava/lang/Object;, "TV;"
    :cond_0
    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1, p1, p2, p3}, Ljava/util/concurrent/RunnableFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    .line 63
    .local v0, "result":Ljava/lang/Object;, "TV;"
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/uploader/FuturePair;->shouldRunSecondFuture(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1, p1, p2, p3}, Ljava/util/concurrent/RunnableFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    .line 67
    .end local v0    # "result":Ljava/lang/Object;, "TV;"
    :cond_0
    return-object v0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 75
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->isCancelled()Z

    move-result v0

    .line 79
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v0}, Ljava/util/concurrent/RunnableFuture;->isCancelled()Z

    move-result v0

    goto :goto_0
.end method

.method public isDone()Z
    .locals 4

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->isDone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/uploader/FuturePair;->shouldRunSecondFuture(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->isDone()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 99
    :goto_0
    return v1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "FuturePair"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Interrupted while running: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->isDone()Z

    move-result v1

    goto :goto_0

    .line 94
    :catch_1
    move-exception v0

    .line 95
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v1, "FuturePair"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not execute: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public run()V
    .locals 4

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/libraries/uploader/FuturePair;, "Lcom/google/android/libraries/uploader/FuturePair<TV;>;"
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->run()V

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future1:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/uploader/FuturePair;->shouldRunSecondFuture(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/google/android/libraries/uploader/FuturePair;->future2:Ljava/util/concurrent/RunnableFuture;

    invoke-interface {v1}, Ljava/util/concurrent/RunnableFuture;->run()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "FuturePair"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Interrupted while running: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 115
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v1, "FuturePair"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not execute: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract shouldRunSecondFuture(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation
.end method
