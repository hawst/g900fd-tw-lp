.class public Lcom/google/android/libraries/bind/data/BindAdapter;
.super Ljava/lang/Object;
.source "BindAdapter.java"


# static fields
.field public static final DK_VIEW_A11Y_COUNT:I

.field public static final DK_VIEW_EQUALITY_FIELDS:I

.field public static final DK_VIEW_GENERATOR:I

.field public static final DK_VIEW_RES_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget v0, Lcom/google/android/libraries/bind/R$id;->BindAdapter_viewResId:I

    sput v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 19
    sget v0, Lcom/google/android/libraries/bind/R$id;->BindAdapter_viewGenerator:I

    sput v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_GENERATOR:I

    .line 24
    sget v0, Lcom/google/android/libraries/bind/R$id;->BindAdapter_viewEqualityFields:I

    sput v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    .line 29
    sget v0, Lcom/google/android/libraries/bind/R$id;->BindAdapter_viewA11yCardCount:I

    sput v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_A11Y_COUNT:I

    return-void
.end method
