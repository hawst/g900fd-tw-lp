.class public final Lcom/google/android/finsky/protos/DocumentV2$Annotations;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Annotations"
.end annotation


# instance fields
.field public attributionHtml:Ljava/lang/String;

.field public badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

.field public hasAttributionHtml:Z

.field public hasOfferNote:Z

.field public hasPrivacyPolicyUrl:Z

.field public link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

.field public offerNote:Ljava/lang/String;

.field public optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

.field public overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

.field public plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

.field public privacyPolicyUrl:Ljava/lang/String;

.field public promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

.field public purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

.field public sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

.field public template:Lcom/google/android/finsky/protos/DocumentV2$Template;

.field public voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

.field public warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 796
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 797
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->clear()Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 798
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 801
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 802
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 803
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 804
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 805
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 806
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 807
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 808
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 809
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 810
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 811
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 812
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 813
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 814
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 815
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 816
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 817
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 818
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 819
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 820
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    .line 821
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 822
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 823
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    .line 824
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 825
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 826
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 827
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 828
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    .line 829
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 830
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 831
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    .line 832
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    .line 833
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    .line 834
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    .line 835
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->cachedSize:I

    .line 836
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 980
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 981
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_0

    .line 982
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 985
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1

    .line 986
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 989
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v3, :cond_2

    .line 990
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 993
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 994
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 995
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v0, v3, v1

    .line 996
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v0, :cond_3

    .line 997
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 994
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1002
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .end local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_5

    .line 1003
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1006
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_6

    .line 1007
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1010
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v3, :cond_7

    .line 1011
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1014
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 1015
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 1016
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v3, v1

    .line 1017
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_8

    .line 1018
    const/16 v3, 0x8

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1015
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1023
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 1024
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 1025
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v3, v1

    .line 1026
    .restart local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_a

    .line 1027
    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1024
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1032
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_b
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_c

    .line 1033
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1036
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_d

    .line 1037
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1040
    :cond_d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_e

    .line 1041
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1044
    :cond_e
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v3, v3

    if-lez v3, :cond_10

    .line 1045
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v3, v3

    if-ge v1, v3, :cond_10

    .line 1046
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    aget-object v0, v3, v1

    .line 1047
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v0, :cond_f

    .line 1048
    const/16 v3, 0xd

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1045
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1053
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 1054
    :cond_11
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1057
    :cond_12
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_14

    .line 1058
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_14

    .line 1059
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 1060
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 1061
    const/16 v3, 0x10

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1058
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1066
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-eqz v3, :cond_15

    .line 1067
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1070
    :cond_15
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 1071
    :cond_16
    const/16 v3, 0x12

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1074
    :cond_17
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v3, :cond_18

    .line 1075
    const/16 v3, 0x13

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1078
    :cond_18
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v3, :cond_19

    .line 1079
    const/16 v3, 0x14

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1082
    :cond_19
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v3, :cond_1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v3, v3

    if-lez v3, :cond_1b

    .line 1083
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v3, v3

    if-ge v1, v3, :cond_1b

    .line 1084
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    aget-object v0, v3, v1

    .line 1085
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v0, :cond_1a

    .line 1086
    const/16 v3, 0x15

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1083
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1091
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v1    # "i":I
    :cond_1b
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1c

    .line 1092
    const/16 v3, 0x16

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1095
    :cond_1c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1d

    .line 1096
    const/16 v3, 0x17

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1099
    :cond_1d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1e

    .line 1100
    const/16 v3, 0x18

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1103
    :cond_1e
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-eqz v3, :cond_20

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v3, v3

    if-lez v3, :cond_20

    .line 1104
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v3, v3

    if-ge v1, v3, :cond_20

    .line 1105
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    aget-object v0, v3, v1

    .line 1106
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v0, :cond_1f

    .line 1107
    const/16 v3, 0x19

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1104
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1112
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v1    # "i":I
    :cond_20
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_21

    .line 1113
    const/16 v3, 0x1a

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1116
    :cond_21
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    if-nez v3, :cond_22

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    .line 1117
    :cond_22
    const/16 v3, 0x1b

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1120
    :cond_23
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-eqz v3, :cond_24

    .line 1121
    const/16 v3, 0x1c

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1124
    :cond_24
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_25

    .line 1125
    const/16 v3, 0x1d

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1128
    :cond_25
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    if-eqz v3, :cond_27

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    array-length v3, v3

    if-lez v3, :cond_27

    .line 1129
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_27

    .line 1130
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    aget-object v0, v3, v1

    .line 1131
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    if-eqz v0, :cond_26

    .line 1132
    const/16 v3, 0x1e

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1129
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1137
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    .end local v1    # "i":I
    :cond_27
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_28

    .line 1138
    const/16 v3, 0x1f

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1141
    :cond_28
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_29

    .line 1142
    const/16 v3, 0x20

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1145
    :cond_29
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1153
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1154
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1158
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1159
    :sswitch_0
    return-object p0

    .line 1164
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_1

    .line 1165
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1167
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1171
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_2

    .line 1172
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1174
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1178
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-nez v5, :cond_3

    .line 1179
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 1181
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1185
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1187
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v5, :cond_5

    move v1, v4

    .line 1188
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1190
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v1, :cond_4

    .line 1191
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1193
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1194
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    aput-object v5, v2, v1

    .line 1195
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1196
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1193
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1187
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v1, v5

    goto :goto_1

    .line 1199
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    aput-object v5, v2, v1

    .line 1200
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1201
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    goto :goto_0

    .line 1205
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_7

    .line 1206
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1208
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1212
    :sswitch_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_8

    .line 1213
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1215
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1219
    :sswitch_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-nez v5, :cond_9

    .line 1220
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Template;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 1222
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1226
    :sswitch_8
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1228
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_b

    move v1, v4

    .line 1229
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1231
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v1, :cond_a

    .line 1232
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1234
    :cond_a
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 1235
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1236
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1237
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1234
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1228
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v1, v5

    goto :goto_3

    .line 1240
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_c
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1241
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1242
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    goto/16 :goto_0

    .line 1246
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :sswitch_9
    const/16 v5, 0x4a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1248
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_e

    move v1, v4

    .line 1249
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1251
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v1, :cond_d

    .line 1252
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1254
    :cond_d
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 1255
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1256
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1257
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1254
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1248
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v1, v5

    goto :goto_5

    .line 1260
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_f
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1261
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1262
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    goto/16 :goto_0

    .line 1266
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :sswitch_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_10

    .line 1267
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1269
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1273
    :sswitch_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_11

    .line 1274
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1276
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1280
    :sswitch_c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_12

    .line 1281
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1283
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1287
    :sswitch_d
    const/16 v5, 0x6a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1289
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-nez v5, :cond_14

    move v1, v4

    .line 1290
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 1292
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v1, :cond_13

    .line 1293
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1295
    :cond_13
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_15

    .line 1296
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;-><init>()V

    aput-object v5, v2, v1

    .line 1297
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1298
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1295
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1289
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :cond_14
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v1, v5

    goto :goto_7

    .line 1301
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :cond_15
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;-><init>()V

    aput-object v5, v2, v1

    .line 1302
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1303
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    goto/16 :goto_0

    .line 1307
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 1308
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    goto/16 :goto_0

    .line 1312
    :sswitch_f
    const/16 v5, 0x82

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1314
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_17

    move v1, v4

    .line 1315
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1317
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_16

    .line 1318
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1320
    :cond_16
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_18

    .line 1321
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1322
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1323
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1320
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1314
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_9

    .line 1326
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_18
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1327
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1328
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 1332
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-nez v5, :cond_19

    .line 1333
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 1335
    :cond_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1339
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 1340
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    goto/16 :goto_0

    .line 1344
    :sswitch_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-nez v5, :cond_1a

    .line 1345
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 1347
    :cond_1a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1351
    :sswitch_13
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v5, :cond_1b

    .line 1352
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1354
    :cond_1b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1358
    :sswitch_14
    const/16 v5, 0xaa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1360
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-nez v5, :cond_1d

    move v1, v4

    .line 1361
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    .line 1363
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v1, :cond_1c

    .line 1364
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1366
    :cond_1c
    :goto_c
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1e

    .line 1367
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;-><init>()V

    aput-object v5, v2, v1

    .line 1368
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1369
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1366
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 1360
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :cond_1d
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v1, v5

    goto :goto_b

    .line 1372
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :cond_1e
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;-><init>()V

    aput-object v5, v2, v1

    .line 1373
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1374
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    goto/16 :goto_0

    .line 1378
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :sswitch_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_1f

    .line 1379
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1381
    :cond_1f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1385
    :sswitch_16
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_20

    .line 1386
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1388
    :cond_20
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1392
    :sswitch_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_21

    .line 1393
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1395
    :cond_21
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1399
    :sswitch_18
    const/16 v5, 0xca

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1401
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-nez v5, :cond_23

    move v1, v4

    .line 1402
    .restart local v1    # "i":I
    :goto_d
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 1404
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v1, :cond_22

    .line 1405
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1407
    :cond_22
    :goto_e
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_24

    .line 1408
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;-><init>()V

    aput-object v5, v2, v1

    .line 1409
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1410
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1407
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 1401
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_23
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v1, v5

    goto :goto_d

    .line 1413
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_24
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;-><init>()V

    aput-object v5, v2, v1

    .line 1414
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1415
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    goto/16 :goto_0

    .line 1419
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :sswitch_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_25

    .line 1420
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1422
    :cond_25
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1426
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    .line 1427
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    goto/16 :goto_0

    .line 1431
    :sswitch_1b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-nez v5, :cond_26

    .line 1432
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    .line 1434
    :cond_26
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1438
    :sswitch_1c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_27

    .line 1439
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1441
    :cond_27
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1445
    :sswitch_1d
    const/16 v5, 0xf2

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1447
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    if-nez v5, :cond_29

    move v1, v4

    .line 1448
    .restart local v1    # "i":I
    :goto_f
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    .line 1450
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    if-eqz v1, :cond_28

    .line 1451
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1453
    :cond_28
    :goto_10
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_2a

    .line 1454
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;-><init>()V

    aput-object v5, v2, v1

    .line 1455
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1456
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1453
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 1447
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    :cond_29
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    array-length v1, v5

    goto :goto_f

    .line 1459
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    :cond_2a
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;-><init>()V

    aput-object v5, v2, v1

    .line 1460
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1461
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    goto/16 :goto_0

    .line 1465
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    :sswitch_1e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_2b

    .line 1466
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1468
    :cond_2b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1472
    :sswitch_1f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_2c

    .line 1473
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1475
    :cond_2c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1154
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xc2 -> :sswitch_17
        0xca -> :sswitch_18
        0xd2 -> :sswitch_19
        0xda -> :sswitch_1a
        0xe2 -> :sswitch_1b
        0xea -> :sswitch_1c
        0xf2 -> :sswitch_1d
        0xfa -> :sswitch_1e
        0x102 -> :sswitch_1f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 683
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 842
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_0

    .line 843
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 845
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1

    .line 846
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 848
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v2, :cond_2

    .line 849
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 851
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 852
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 853
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v0, v2, v1

    .line 854
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v0, :cond_3

    .line 855
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 852
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 859
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_5

    .line 860
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 862
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_6

    .line 863
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 865
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v2, :cond_7

    .line 866
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 868
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 869
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 870
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v2, v1

    .line 871
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_8

    .line 872
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 869
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 876
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 877
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 878
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v2, v1

    .line 879
    .restart local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_a

    .line 880
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 877
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 884
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_c

    .line 885
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 887
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_d

    .line 888
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 890
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_e

    .line 891
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 893
    :cond_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 894
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 895
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    aget-object v0, v2, v1

    .line 896
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v0, :cond_f

    .line 897
    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 894
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 901
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 902
    :cond_11
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 904
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_14

    .line 905
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_14

    .line 906
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 907
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 908
    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 905
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 912
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-eqz v2, :cond_15

    .line 913
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 915
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 916
    :cond_16
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 918
    :cond_17
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v2, :cond_18

    .line 919
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 921
    :cond_18
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v2, :cond_19

    .line 922
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 924
    :cond_19
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 925
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 926
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    aget-object v0, v2, v1

    .line 927
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v0, :cond_1a

    .line 928
    const/16 v2, 0x15

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 925
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 932
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v1    # "i":I
    :cond_1b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1c

    .line 933
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 935
    :cond_1c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1d

    .line 936
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 938
    :cond_1d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1e

    .line 939
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 941
    :cond_1e
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v2, v2

    if-lez v2, :cond_20

    .line 942
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v2, v2

    if-ge v1, v2, :cond_20

    .line 943
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    aget-object v0, v2, v1

    .line 944
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v0, :cond_1f

    .line 945
    const/16 v2, 0x19

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 942
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 949
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v1    # "i":I
    :cond_20
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_21

    .line 950
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 952
    :cond_21
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasAttributionHtml:Z

    if-nez v2, :cond_22

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 953
    :cond_22
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 955
    :cond_23
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-eqz v2, :cond_24

    .line 956
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 958
    :cond_24
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_25

    .line 959
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 961
    :cond_25
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    array-length v2, v2

    if-lez v2, :cond_27

    .line 962
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_27

    .line 963
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    aget-object v0, v2, v1

    .line 964
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    if-eqz v0, :cond_26

    .line 965
    const/16 v2, 0x1e

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 962
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 969
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    .end local v1    # "i":I
    :cond_27
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_28

    .line 970
    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForController:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 972
    :cond_28
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_29

    .line 973
    const/16 v2, 0x20

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionFeaturedApps:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 975
    :cond_29
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 976
    return-void
.end method
