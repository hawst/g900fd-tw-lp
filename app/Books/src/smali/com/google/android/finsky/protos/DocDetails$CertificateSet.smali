.class public final Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CertificateSet"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;


# instance fields
.field public certificateHash:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1558
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1559
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->clear()Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    .line 1560
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    .locals 2

    .prologue
    .line 1544
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    if-nez v0, :cond_1

    .line 1545
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1547
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    if-nez v0, :cond_0

    .line 1548
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    .line 1550
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1552
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    return-object v0

    .line 1550
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    .locals 1

    .prologue
    .line 1563
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    .line 1564
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->cachedSize:I

    .line 1565
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1584
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1585
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 1586
    const/4 v0, 0x0

    .line 1587
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1588
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 1589
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1590
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1591
    add-int/lit8 v0, v0, 0x1

    .line 1592
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1588
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1596
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 1597
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1599
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1608
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1612
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1613
    :sswitch_0
    return-object p0

    .line 1618
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1620
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 1621
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1622
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1623
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1625
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1626
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1627
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1625
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1620
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 1630
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1631
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    goto :goto_0

    .line 1608
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1538
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1571
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1572
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 1573
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1574
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1575
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1572
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1579
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1580
    return-void
.end method
