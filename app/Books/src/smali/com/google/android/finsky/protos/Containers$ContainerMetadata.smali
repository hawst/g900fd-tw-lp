.class public final Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Containers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Containers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContainerMetadata"
.end annotation


# instance fields
.field public analyticsCookie:Ljava/lang/String;

.field public browseUrl:Ljava/lang/String;

.field public containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

.field public estimatedResults:J

.field public hasAnalyticsCookie:Z

.field public hasBrowseUrl:Z

.field public hasEstimatedResults:Z

.field public hasNextPageUrl:Z

.field public hasOrdered:Z

.field public hasRelevance:Z

.field public nextPageUrl:Ljava/lang/String;

.field public ordered:Z

.field public relevance:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->clear()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    .line 54
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    .line 58
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl:Z

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    .line 60
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl:Z

    .line 61
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance:D

    .line 62
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance:Z

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    .line 64
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults:Z

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie:Ljava/lang/String;

    .line 66
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie:Z

    .line 67
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    .line 68
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered:Z

    .line 69
    invoke-static {}, Lcom/google/android/finsky/protos/Containers$ContainerView;->emptyArray()[Lcom/google/android/finsky/protos/Containers$ContainerView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->cachedSize:I

    .line 71
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 110
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 111
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 114
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 115
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 118
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance:Z

    if-nez v3, :cond_4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    .line 120
    :cond_4
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance:D

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    .line 123
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults:Z

    if-nez v3, :cond_6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    .line 124
    :cond_6
    const/4 v3, 0x4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 127
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 128
    :cond_8
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 131
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered:Z

    if-nez v3, :cond_a

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    if-eqz v3, :cond_b

    .line 132
    :cond_a
    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 135
    :cond_b
    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    array-length v3, v3

    if-lez v3, :cond_d

    .line 136
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    array-length v3, v3

    if-ge v1, v3, :cond_d

    .line 137
    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    aget-object v0, v3, v1

    .line 138
    .local v0, "element":Lcom/google/android/finsky/protos/Containers$ContainerView;
    if-eqz v0, :cond_c

    .line 139
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 136
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    .end local v0    # "element":Lcom/google/android/finsky/protos/Containers$ContainerView;
    .end local v1    # "i":I
    :cond_d
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 152
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 153
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 157
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 158
    :sswitch_0
    return-object p0

    .line 163
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    .line 164
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl:Z

    goto :goto_0

    .line 168
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    .line 169
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl:Z

    goto :goto_0

    .line 173
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance:D

    .line 174
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance:Z

    goto :goto_0

    .line 178
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    .line 179
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults:Z

    goto :goto_0

    .line 183
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie:Ljava/lang/String;

    .line 184
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie:Z

    goto :goto_0

    .line 188
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    .line 189
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered:Z

    goto :goto_0

    .line 193
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 195
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-nez v5, :cond_2

    move v1, v4

    .line 196
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Containers$ContainerView;

    .line 198
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    if-eqz v1, :cond_1

    .line 199
    iget-object v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 202
    new-instance v5, Lcom/google/android/finsky/protos/Containers$ContainerView;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Containers$ContainerView;-><init>()V

    aput-object v5, v2, v1

    .line 203
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 204
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 195
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    array-length v1, v5

    goto :goto_1

    .line 207
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Containers$ContainerView;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Containers$ContainerView;-><init>()V

    aput-object v5, v2, v1

    .line 208
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 209
    iput-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    goto/16 :goto_0

    .line 153
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 80
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 81
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 83
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 85
    :cond_4
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance:D

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 87
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults:Z

    if-nez v2, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 88
    :cond_6
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 90
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 91
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 93
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered:Z

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    if-eqz v2, :cond_b

    .line 94
    :cond_a
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 96
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 97
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 98
    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    aget-object v0, v2, v1

    .line 99
    .local v0, "element":Lcom/google/android/finsky/protos/Containers$ContainerView;
    if-eqz v0, :cond_c

    .line 100
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 97
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v0    # "element":Lcom/google/android/finsky/protos/Containers$ContainerView;
    .end local v1    # "i":I
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 105
    return-void
.end method
