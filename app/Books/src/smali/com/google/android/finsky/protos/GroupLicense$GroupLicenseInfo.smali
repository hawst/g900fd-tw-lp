.class public final Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "GroupLicense.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/GroupLicense;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupLicenseInfo"
.end annotation


# instance fields
.field public gaiaGroupId:J

.field public groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

.field public hasGaiaGroupId:Z

.field public hasLicensedOfferType:Z

.field public licensedOfferType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->clear()Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    .line 38
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType:I

    .line 42
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType:Z

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId:J

    .line 44
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId:Z

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->cachedSize:I

    .line 47
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 67
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 68
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType:Z

    if-eqz v1, :cond_1

    .line 69
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 73
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    if-eqz v1, :cond_4

    .line 77
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 89
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 93
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    :sswitch_0
    return-object p0

    .line 99
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 100
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 113
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType:I

    .line 114
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType:Z

    goto :goto_0

    .line 120
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId:J

    .line 121
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId:Z

    goto :goto_0

    .line 125
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    if-nez v2, :cond_1

    .line 126
    new-instance v2, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    .line 128
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 53
    iget v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType:Z

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 56
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 57
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 59
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    if-eqz v0, :cond_4

    .line 60
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 62
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 63
    return-void
.end method
