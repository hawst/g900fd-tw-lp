.class public final Lcom/google/android/finsky/protos/Common$SubscriptionTerms;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubscriptionTerms"
.end annotation


# instance fields
.field public formattedPriceWithRecurrencePeriod:Ljava/lang/String;

.field public hasFormattedPriceWithRecurrencePeriod:Z

.field public recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

.field public replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

.field public seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

.field public trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2424
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2425
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->clear()Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 2426
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$SubscriptionTerms;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2429
    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2430
    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2431
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 2432
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    .line 2433
    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    .line 2434
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Docid;->emptyArray()[Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    .line 2435
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->cachedSize:I

    .line 2436
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2467
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2468
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v3, :cond_0

    .line 2469
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2472
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v3, :cond_1

    .line 2473
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2476
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2477
    :cond_2
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2480
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    if-eqz v3, :cond_4

    .line 2481
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2484
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 2485
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 2486
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    aget-object v0, v3, v1

    .line 2487
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v0, :cond_5

    .line 2488
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2485
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2493
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v1    # "i":I
    :cond_6
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SubscriptionTerms;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2501
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2502
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2506
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2507
    :sswitch_0
    return-object p0

    .line 2512
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v5, :cond_1

    .line 2513
    new-instance v5, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2515
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2519
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v5, :cond_2

    .line 2520
    new-instance v5, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2522
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2526
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 2527
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    goto :goto_0

    .line 2531
    :sswitch_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    if-nez v5, :cond_3

    .line 2532
    new-instance v5, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    .line 2534
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2538
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2540
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v5, :cond_5

    move v1, v4

    .line 2541
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Docid;

    .line 2543
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v1, :cond_4

    .line 2544
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2546
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 2547
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    aput-object v5, v2, v1

    .line 2548
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2549
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2546
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2540
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Docid;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v1, v5

    goto :goto_1

    .line 2552
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Docid;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    aput-object v5, v2, v1

    .line 2553
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2554
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    goto/16 :goto_0

    .line 2502
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2391
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2442
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v2, :cond_0

    .line 2443
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2445
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v2, :cond_1

    .line 2446
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2448
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2449
    :cond_2
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2451
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    if-eqz v2, :cond_4

    .line 2452
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2454
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2455
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 2456
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    aget-object v0, v2, v1

    .line 2457
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v0, :cond_5

    .line 2458
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2455
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2462
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v1    # "i":I
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2463
    return-void
.end method
