.class public final Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionBanner"
.end annotation


# instance fields
.field public action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

.field public primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3759
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3760
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->clear()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3761
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .locals 1

    .prologue
    .line 3764
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 3765
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3766
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3767
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->cachedSize:I

    .line 3768
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 3798
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3799
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 3800
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 3801
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    aget-object v0, v3, v1

    .line 3802
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v0, :cond_0

    .line 3803
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3800
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3808
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_2

    .line 3809
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3812
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 3813
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 3814
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 3815
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_3

    .line 3816
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3813
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3821
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_4
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3829
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3830
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3834
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3835
    :sswitch_0
    return-object p0

    .line 3840
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3842
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-nez v5, :cond_2

    move v1, v4

    .line 3843
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 3845
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v1, :cond_1

    .line 3846
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3848
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3849
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;-><init>()V

    aput-object v5, v2, v1

    .line 3850
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3851
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3848
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3842
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v1, v5

    goto :goto_1

    .line 3854
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;-><init>()V

    aput-object v5, v2, v1

    .line 3855
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3856
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    goto :goto_0

    .line 3860
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_4

    .line 3861
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3863
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3867
    :sswitch_3
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3869
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_6

    move v1, v4

    .line 3870
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3872
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_5

    .line 3873
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3875
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 3876
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 3877
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3878
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3875
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3869
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 3881
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_7
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 3882
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3883
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 3830
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3733
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3774
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 3775
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 3776
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    aget-object v0, v2, v1

    .line 3777
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v0, :cond_0

    .line 3778
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3775
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3782
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_2

    .line 3783
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3785
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 3786
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 3787
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 3788
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_3

    .line 3789
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3786
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3793
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3794
    return-void
.end method
