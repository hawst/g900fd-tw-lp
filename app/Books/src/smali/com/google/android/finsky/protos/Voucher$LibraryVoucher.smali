.class public final Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Voucher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Voucher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryVoucher"
.end annotation


# instance fields
.field public voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->clear()Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    .line 30
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->cachedSize:I

    .line 35
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 50
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    if-eqz v1, :cond_0

    .line 51
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 63
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 67
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    :sswitch_0
    return-object p0

    .line 73
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    if-nez v1, :cond_1

    .line 74
    new-instance v1, Lcom/google/android/finsky/protos/Common$VoucherId;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$VoucherId;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    .line 76
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 63
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Voucher$LibraryVoucher;->voucherId:Lcom/google/android/finsky/protos/Common$VoucherId;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 44
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 45
    return-void
.end method
