.class public final Lcom/google/android/finsky/protos/Common$RentalTerms;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RentalTerms"
.end annotation


# instance fields
.field public activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

.field public dEPRECATEDActivatePeriodSeconds:I

.field public dEPRECATEDGrantPeriodSeconds:I

.field public grantEndTimeSeconds:J

.field public grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

.field public hasDEPRECATEDActivatePeriodSeconds:Z

.field public hasDEPRECATEDGrantPeriodSeconds:Z

.field public hasGrantEndTimeSeconds:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1930
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1931
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$RentalTerms;->clear()Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 1932
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$RentalTerms;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1935
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 1936
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 1937
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    .line 1938
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    .line 1939
    iput v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    .line 1940
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    .line 1941
    iput v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    .line 1942
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    .line 1943
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->cachedSize:I

    .line 1944
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1970
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1971
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    if-eqz v1, :cond_1

    .line 1972
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1975
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    if-eqz v1, :cond_3

    .line 1976
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1979
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v1, :cond_4

    .line 1980
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1983
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v1, :cond_5

    .line 1984
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1987
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 1988
    :cond_6
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1991
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$RentalTerms;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1999
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2000
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2004
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2005
    :sswitch_0
    return-object p0

    .line 2010
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    .line 2011
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    goto :goto_0

    .line 2015
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    .line 2016
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    goto :goto_0

    .line 2020
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v1, :cond_1

    .line 2021
    new-instance v1, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2023
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2027
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v1, :cond_2

    .line 2028
    new-instance v1, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2030
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2034
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    .line 2035
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    goto :goto_0

    .line 2000
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1895
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$RentalTerms;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$RentalTerms;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1950
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    if-eqz v0, :cond_1

    .line 1951
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1953
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    if-eqz v0, :cond_3

    .line 1954
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1956
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v0, :cond_4

    .line 1957
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1959
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v0, :cond_5

    .line 1960
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1962
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    .line 1963
    :cond_6
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1965
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1966
    return-void
.end method
