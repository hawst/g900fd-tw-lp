.class public final Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LicensedDocumentInfo"
.end annotation


# instance fields
.field public assignedByGaiaId:J

.field public assignmentId:Ljava/lang/String;

.field public gaiaGroupId:[J

.field public groupLicenseCheckoutOrderId:Ljava/lang/String;

.field public groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

.field public hasAssignedByGaiaId:Z

.field public hasAssignmentId:Z

.field public hasGroupLicenseCheckoutOrderId:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3724
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3725
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->clear()Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    .line 3726
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3729
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    .line 3730
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseCheckoutOrderId:Ljava/lang/String;

    .line 3731
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasGroupLicenseCheckoutOrderId:Z

    .line 3732
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    .line 3733
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignedByGaiaId:J

    .line 3734
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignedByGaiaId:Z

    .line 3735
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignmentId:Ljava/lang/String;

    .line 3736
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignmentId:Z

    .line 3737
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->cachedSize:I

    .line 3738
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 3766
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v1

    .line 3767
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v2, v2

    if-lez v2, :cond_0

    .line 3768
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v2, v2

    mul-int/lit8 v0, v2, 0x8

    .line 3769
    .local v0, "dataSize":I
    add-int/2addr v1, v0

    .line 3770
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 3772
    .end local v0    # "dataSize":I
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasGroupLicenseCheckoutOrderId:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseCheckoutOrderId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3773
    :cond_1
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseCheckoutOrderId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3776
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    if-eqz v2, :cond_3

    .line 3777
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3780
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignedByGaiaId:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignedByGaiaId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 3781
    :cond_4
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignedByGaiaId:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3784
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignmentId:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignmentId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 3785
    :cond_6
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignmentId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3788
    :cond_7
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;
    .locals 11
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 3796
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v5

    .line 3797
    .local v5, "tag":I
    sparse-switch v5, :sswitch_data_0

    .line 3801
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v7

    if-nez v7, :cond_0

    .line 3802
    :sswitch_0
    return-object p0

    .line 3807
    :sswitch_1
    const/16 v7, 0x9

    invoke-static {p1, v7}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3809
    .local v0, "arrayLength":I
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    if-nez v7, :cond_2

    move v1, v6

    .line 3810
    .local v1, "i":I
    :goto_1
    add-int v7, v1, v0

    new-array v4, v7, [J

    .line 3811
    .local v4, "newArray":[J
    if-eqz v1, :cond_1

    .line 3812
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    invoke-static {v7, v6, v4, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3814
    :cond_1
    :goto_2
    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    if-ge v1, v7, :cond_3

    .line 3815
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 3816
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3814
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3809
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_2
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v1, v7

    goto :goto_1

    .line 3819
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 3820
    iput-object v4, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    goto :goto_0

    .line 3824
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 3825
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 3826
    .local v3, "limit":I
    div-int/lit8 v0, v2, 0x8

    .line 3827
    .restart local v0    # "arrayLength":I
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    if-nez v7, :cond_5

    move v1, v6

    .line 3828
    .restart local v1    # "i":I
    :goto_3
    add-int v7, v1, v0

    new-array v4, v7, [J

    .line 3829
    .restart local v4    # "newArray":[J
    if-eqz v1, :cond_4

    .line 3830
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    invoke-static {v7, v6, v4, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3832
    :cond_4
    :goto_4
    array-length v7, v4

    if-ge v1, v7, :cond_6

    .line 3833
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 3832
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3827
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_5
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v1, v7

    goto :goto_3

    .line 3835
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_6
    iput-object v4, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    .line 3836
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto :goto_0

    .line 3840
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[J
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseCheckoutOrderId:Ljava/lang/String;

    .line 3841
    iput-boolean v10, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasGroupLicenseCheckoutOrderId:Z

    goto :goto_0

    .line 3845
    :sswitch_4
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    if-nez v7, :cond_7

    .line 3846
    new-instance v7, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-direct {v7}, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;-><init>()V

    iput-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    .line 3848
    :cond_7
    iget-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-virtual {p1, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3852
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignedByGaiaId:J

    .line 3853
    iput-boolean v10, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignedByGaiaId:Z

    goto/16 :goto_0

    .line 3857
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignmentId:Ljava/lang/String;

    .line 3858
    iput-boolean v10, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignmentId:Z

    goto/16 :goto_0

    .line 3797
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x21 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3689
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3744
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v1, v1

    if-lez v1, :cond_0

    .line 3745
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 3746
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->gaiaGroupId:[J

    aget-wide v2, v2, v0

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 3745
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3749
    .end local v0    # "i":I
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasGroupLicenseCheckoutOrderId:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseCheckoutOrderId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3750
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseCheckoutOrderId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3752
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    if-eqz v1, :cond_3

    .line 3753
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->groupLicenseKey:Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3755
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignedByGaiaId:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignedByGaiaId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 3756
    :cond_4
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignedByGaiaId:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 3758
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->hasAssignmentId:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignmentId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3759
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;->assignmentId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3761
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3762
    return-void
.end method
