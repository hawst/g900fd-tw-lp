.class public final Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PlayResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayResponseWrapper"
.end annotation


# instance fields
.field public commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

.field public payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

.field public preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

.field public serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->clear()Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;

    .line 39
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    iput-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    .line 43
    iput-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 44
    invoke-static {}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->emptyArray()[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 45
    iput-object v1, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->cachedSize:I

    .line 47
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 75
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 76
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    if-eqz v3, :cond_0

    .line 77
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 80
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-eqz v3, :cond_1

    .line 81
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 84
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 85
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 86
    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    aget-object v0, v3, v1

    .line 87
    .local v0, "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    if-eqz v0, :cond_2

    .line 88
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 85
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    .end local v0    # "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-eqz v3, :cond_4

    .line 94
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 97
    :cond_4
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 105
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 106
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 110
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 111
    :sswitch_0
    return-object p0

    .line 116
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    if-nez v5, :cond_1

    .line 117
    new-instance v5, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    .line 119
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 123
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-nez v5, :cond_2

    .line 124
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 126
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 130
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 132
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-nez v5, :cond_4

    move v1, v4

    .line 133
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 135
    .local v2, "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    if-eqz v1, :cond_3

    .line 136
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 139
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;-><init>()V

    aput-object v5, v2, v1

    .line 140
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 141
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 132
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v1, v5

    goto :goto_1

    .line 144
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :cond_5
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;-><init>()V

    aput-object v5, v2, v1

    .line 145
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 146
    iput-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    goto :goto_0

    .line 150
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :sswitch_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-nez v5, :cond_6

    .line 151
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    .line 153
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 106
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    if-eqz v2, :cond_0

    .line 54
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->payload:Lcom/google/android/finsky/protos/PlayResponse$PlayPayload;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 56
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-eqz v2, :cond_1

    .line 57
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 59
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 60
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 61
    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    aget-object v0, v2, v1

    .line 62
    .local v0, "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    if-eqz v0, :cond_2

    .line 63
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 60
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-eqz v2, :cond_4

    .line 68
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/PlayResponse$PlayResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 70
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 71
    return-void
.end method
