.class public final Lcom/google/android/finsky/protos/Common$Image;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/Common$Image$Citation;,
        Lcom/google/android/finsky/protos/Common$Image$Dimension;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Common$Image;


# instance fields
.field public altTextLocalized:Ljava/lang/String;

.field public attribution:Lcom/google/android/finsky/protos/Common$Attribution;

.field public autogen:Z

.field public citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

.field public dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

.field public durationSeconds:I

.field public fillColorRgb:Ljava/lang/String;

.field public hasAltTextLocalized:Z

.field public hasAutogen:Z

.field public hasDurationSeconds:Z

.field public hasFillColorRgb:Z

.field public hasImageType:Z

.field public hasImageUrl:Z

.field public hasPositionInSequence:Z

.field public hasSecureUrl:Z

.field public hasSupportsFifeUrlOptions:Z

.field public imageType:I

.field public imageUrl:Ljava/lang/String;

.field public positionInSequence:I

.field public secureUrl:Ljava/lang/String;

.field public supportsFifeUrlOptions:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1255
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1256
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Image;->clear()Lcom/google/android/finsky/protos/Common$Image;

    .line 1257
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Common$Image;
    .locals 2

    .prologue
    .line 1199
    sget-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v0, :cond_1

    .line 1200
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1202
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v0, :cond_0

    .line 1203
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Image;

    sput-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    .line 1205
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1207
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    return-object v0

    .line 1205
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Image;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1260
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 1261
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    .line 1262
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    .line 1263
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    .line 1264
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 1265
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 1266
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    .line 1267
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    .line 1268
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    .line 1269
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    .line 1270
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    .line 1271
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    .line 1272
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    .line 1273
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    .line 1274
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    .line 1275
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    .line 1276
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    .line 1277
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    .line 1278
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    .line 1279
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    .line 1280
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    .line 1281
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image;->cachedSize:I

    .line 1282
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1329
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1330
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    if-eqz v1, :cond_1

    .line 1331
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1334
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-eqz v1, :cond_2

    .line 1335
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1338
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1339
    :cond_3
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1342
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1343
    :cond_5
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1346
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1347
    :cond_7
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1350
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    if-nez v1, :cond_9

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    if-eqz v1, :cond_a

    .line 1351
    :cond_9
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1354
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v1, :cond_c

    .line 1355
    :cond_b
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1358
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    if-eqz v1, :cond_d

    .line 1359
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1362
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    if-nez v1, :cond_e

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    if-eqz v1, :cond_f

    .line 1363
    :cond_e
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1366
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1367
    :cond_10
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1370
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    if-nez v1, :cond_12

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    if-eqz v1, :cond_13

    .line 1371
    :cond_12
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1374
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    if-eqz v1, :cond_14

    .line 1375
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1378
    :cond_14
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1386
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1387
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1391
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1392
    :sswitch_0
    return-object p0

    .line 1397
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1398
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1412
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 1413
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    goto :goto_0

    .line 1419
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-nez v2, :cond_1

    .line 1420
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image$Dimension;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image$Dimension;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 1422
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    const/4 v3, 0x2

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    goto :goto_0

    .line 1426
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 1427
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    goto :goto_0

    .line 1431
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    .line 1432
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    goto :goto_0

    .line 1436
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    .line 1437
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    goto :goto_0

    .line 1441
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    .line 1442
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    goto :goto_0

    .line 1446
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    .line 1447
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    goto :goto_0

    .line 1451
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    if-nez v2, :cond_2

    .line 1452
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image$Citation;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image$Citation;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    .line 1454
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    const/16 v3, 0xa

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    goto :goto_0

    .line 1458
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    .line 1459
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    goto :goto_0

    .line 1463
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    .line 1464
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    goto :goto_0

    .line 1468
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    .line 1469
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    goto/16 :goto_0

    .line 1473
    :sswitch_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    if-nez v2, :cond_3

    .line 1474
    new-instance v2, Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Attribution;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    .line 1476
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1387
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x13 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x53 -> :sswitch_8
        0x70 -> :sswitch_9
        0x7a -> :sswitch_a
        0x80 -> :sswitch_b
        0x8a -> :sswitch_c
    .end sparse-switch

    .line 1398
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 939
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Image;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1288
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    if-eqz v0, :cond_1

    .line 1289
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1291
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-eqz v0, :cond_2

    .line 1292
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1294
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1295
    :cond_3
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1297
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1298
    :cond_5
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1300
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1301
    :cond_7
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1303
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    if-nez v0, :cond_9

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    if-eqz v0, :cond_a

    .line 1304
    :cond_9
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1306
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v0, :cond_c

    .line 1307
    :cond_b
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1309
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    if-eqz v0, :cond_d

    .line 1310
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1312
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    if-eqz v0, :cond_f

    .line 1313
    :cond_e
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1315
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1316
    :cond_10
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1318
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    if-eqz v0, :cond_13

    .line 1319
    :cond_12
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1321
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    if-eqz v0, :cond_14

    .line 1322
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1324
    :cond_14
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1325
    return-void
.end method
