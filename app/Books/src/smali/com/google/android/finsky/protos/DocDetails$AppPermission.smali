.class public final Lcom/google/android/finsky/protos/DocDetails$AppPermission;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppPermission"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;


# instance fields
.field public hasKey:Z

.field public hasPermissionRequired:Z

.field public key:Ljava/lang/String;

.field public permissionRequired:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1675
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1676
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->clear()Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    .line 1677
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .locals 2

    .prologue
    .line 1656
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-nez v0, :cond_1

    .line 1657
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1659
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-nez v0, :cond_0

    .line 1660
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    .line 1662
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1664
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    return-object v0

    .line 1662
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1680
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    .line 1681
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    .line 1682
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    .line 1683
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    .line 1684
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->cachedSize:I

    .line 1685
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1702
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1703
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1704
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1707
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    if-eq v1, v3, :cond_3

    .line 1708
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1711
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1719
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1720
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1724
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1725
    :sswitch_0
    return-object p0

    .line 1730
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    .line 1731
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    goto :goto_0

    .line 1735
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    .line 1736
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    goto :goto_0

    .line 1720
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1650
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1691
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1692
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1694
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    if-eq v0, v2, :cond_3

    .line 1695
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1697
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1698
    return-void
.end method
