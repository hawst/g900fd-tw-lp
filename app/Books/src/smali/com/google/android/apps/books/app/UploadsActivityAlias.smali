.class public Lcom/google/android/apps/books/app/UploadsActivityAlias;
.super Lcom/google/android/ublib/actionbar/UBLibActivity;
.source "UploadsActivityAlias.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;
    }
.end annotation


# instance fields
.field private final mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;-><init>(Lcom/google/android/apps/books/app/UploadsActivityAlias;Lcom/google/android/apps/books/app/UploadsActivityAlias$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias;->mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/UploadsActivityAlias;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/UploadsActivityAlias;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->performChecksAndStartUpload(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/UploadsActivityAlias;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/UploadsActivityAlias;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->startUploadAndFinishActivity(Landroid/net/Uri;)V

    return-void
.end method

.method private getAccount()Landroid/accounts/Account;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 67
    const/4 v1, 0x1

    .line 68
    .local v1, "defaultToAny":Z
    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/books/util/AccountUtils;->findIntentAccount(Landroid/content/Context;Landroid/accounts/Account;Z)Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;

    move-result-object v0

    .line 70
    .local v0, "accountResult":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    iget-object v3, v0, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    :cond_0
    const-string v3, "UploadsActivityAlias"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 72
    const-string v3, "UploadsActivityAlias"

    const-string v4, "No account in UploadsActivityAlias"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_1
    :goto_0
    return-object v2

    .line 78
    :cond_2
    iget-boolean v2, v0, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    if-nez v2, :cond_3

    .line 79
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iget-object v3, v0, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 82
    :cond_3
    iget-object v2, v0, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    goto :goto_0
.end method

.method private getFileName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/FileUtils;->getFileName(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 200
    .end local v0    # "result":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "result":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01da

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getUploadsController()Lcom/google/android/apps/books/data/UploadsController;
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 87
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 88
    invoke-static {p0, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataController;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v1

    .line 91
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private performChecksAndStartUpload(Landroid/net/Uri;)V
    .locals 13
    .param p1, "uriToUpload"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x0

    const v10, 0x104000a

    .line 140
    move-object v1, p0

    .line 141
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 143
    .local v7, "res":Landroid/content/res/Resources;
    invoke-static {v1}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 145
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0f01e8

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v10, v11}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 150
    .local v0, "alert":Landroid/app/AlertDialog;
    iget-object v8, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias;->mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 151
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 193
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getMaxUploadSizeMB(Landroid/content/Context;)I

    move-result v3

    .line 157
    .local v3, "maxSizeMB":I
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/google/android/apps/books/util/FileUtils;->getFileSize(Landroid/net/Uri;Landroid/content/ContentResolver;)J

    move-result-wide v4

    .line 158
    .local v4, "filesize":J
    const/high16 v8, 0x100000

    mul-int/2addr v8, v3

    int-to-long v8, v8

    cmp-long v8, v4, v8

    if-lez v8, :cond_2

    .line 159
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0f01e5

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x104000a

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 164
    .restart local v0    # "alert":Landroid/app/AlertDialog;
    iget-object v8, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias;->mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 165
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 168
    .end local v0    # "alert":Landroid/app/AlertDialog;
    .end local v4    # "filesize":J
    :catch_0
    move-exception v2

    .line 169
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "UploadsActivityAlias"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 170
    const-string v8, "UploadsActivityAlias"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IO error when opening file "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->finish()V

    goto :goto_0

    .line 176
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v4    # "filesize":J
    :cond_2
    invoke-static {v1}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 177
    new-instance v6, Lcom/google/android/apps/books/app/UploadsActivityAlias$2;

    invoke-direct {v6, p0, p1}, Lcom/google/android/apps/books/app/UploadsActivityAlias$2;-><init>(Lcom/google/android/apps/books/app/UploadsActivityAlias;Landroid/net/Uri;)V

    .line 183
    .local v6, "onOkListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0f01e9

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v10, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const/high16 v9, 0x1040000

    iget-object v10, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias;->mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 187
    .restart local v0    # "alert":Landroid/app/AlertDialog;
    iget-object v8, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias;->mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 188
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 191
    .end local v0    # "alert":Landroid/app/AlertDialog;
    .end local v6    # "onOkListener":Landroid/content/DialogInterface$OnClickListener;
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->startUploadAndFinishActivity(Landroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method private startUploadAndFinishActivity(Landroid/net/Uri;)V
    .locals 10
    .param p1, "uriToUpload"    # Landroid/net/Uri;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 207
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 211
    .local v5, "resolver":Landroid/content/ContentResolver;
    :try_start_0
    const-string v7, "r"

    invoke-virtual {v5, p1, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 212
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v6

    .line 213
    .local v6, "uploader":Lcom/google/android/apps/books/data/UploadsController;
    if-eqz v6, :cond_1

    .line 214
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getFileName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v2, v7}, Lcom/google/android/apps/books/data/UploadsController;->addUpload(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/lang/String;

    .line 218
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/google/android/apps/books/app/HomeFragment;->setShowUploads(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V

    .line 225
    :goto_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 226
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "android.intent.action.MAIN"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const v3, 0x14004000

    .line 231
    .local v3, "flags":I
    const v7, 0x14004000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 232
    const-class v7, Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v4, v0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 234
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    .end local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v3    # "flags":I
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v6    # "uploader":Lcom/google/android/apps/books/data/UploadsController;
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->finish()V

    .line 243
    return-void

    .line 220
    .restart local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local v6    # "uploader":Lcom/google/android/apps/books/data/UploadsController;
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getFileName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/apps/books/app/HomeFragment;->setShowUploads(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 236
    .end local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local v6    # "uploader":Lcom/google/android/apps/books/data/UploadsController;
    :catch_0
    move-exception v1

    .line 237
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v7, "UploadsActivityAlias"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 238
    const-string v7, "UploadsActivityAlias"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startUpload "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onCreate(Landroid/os/Bundle;)V

    .line 98
    sget-object v7, Lcom/google/android/apps/books/util/ConfigValue;->UPLOAD_URL:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v7, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 99
    const-string v7, "This test configuration doesn\'t support uploads"

    const/4 v8, 0x1

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->finish()V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    move-object v1, p0

    .line 106
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 107
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 108
    .local v5, "uploadsIntent":Landroid/content/Intent;
    invoke-static {v5, v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->isUploadIntent(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 109
    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 111
    .local v6, "uriToUpload":Landroid/net/Uri;
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 112
    .local v3, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->hasSeenCloudLoadingIntro()Z

    move-result v7

    if-nez v7, :cond_2

    .line 114
    new-instance v2, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;

    invoke-direct {v2, p0, v3, v6}, Lcom/google/android/apps/books/app/UploadsActivityAlias$1;-><init>(Lcom/google/android/apps/books/app/UploadsActivityAlias;Lcom/google/android/apps/books/preference/LocalPreferences;Landroid/net/Uri;)V

    .line 122
    .local v2, "okListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f0f01e6

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0f01e7

    invoke-virtual {v7, v8, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 126
    .local v0, "alert":Landroid/app/AlertDialog;
    iget-object v7, p0, Lcom/google/android/apps/books/app/UploadsActivityAlias;->mCloser:Lcom/google/android/apps/books/app/UploadsActivityAlias$Closer;

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 127
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 129
    .end local v0    # "alert":Landroid/app/AlertDialog;
    .end local v2    # "okListener":Landroid/content/DialogInterface$OnClickListener;
    :cond_2
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->performChecksAndStartUpload(Landroid/net/Uri;)V

    goto :goto_0

    .line 131
    .end local v3    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    .end local v6    # "uriToUpload":Landroid/net/Uri;
    :cond_3
    const-string v7, "UploadsActivityAlias"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 132
    const-string v7, "UploadsActivityAlias"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCreate unrecognized intent. Type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5, v1}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Data: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/UploadsActivityAlias;->finish()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 247
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onDestroy()V

    .line 248
    return-void
.end method
