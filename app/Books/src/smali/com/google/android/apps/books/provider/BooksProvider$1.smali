.class Lcom/google/android/apps/books/provider/BooksProvider$1;
.super Ljava/lang/Object;
.source "BooksProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/provider/BooksProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/provider/BooksProvider;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/apps/books/provider/BooksProvider$1;->this$0:Lcom/google/android/apps/books/provider/BooksProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 145
    const-string v0, "BooksProvider"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "BooksProvider"

    const-string v1, "mNotifyWatchedTableRunnable running"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider$1;->this$0:Lcom/google/android/apps/books/provider/BooksProvider;

    # getter for: Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksProvider;->access$000(Lcom/google/android/apps/books/provider/BooksProvider;)Lcom/google/android/apps/books/provider/database/BooksDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/provider/IcingGlobalSearchHelper;->TABLE_STORAGE_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->notifyWatchedTableChanged(Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)Z

    .line 149
    return-void
.end method
