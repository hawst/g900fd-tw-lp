.class public Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
.super Ljava/lang/Object;
.source "JsonAnnotation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VersionRanges"
.end annotation


# instance fields
.field public contentVersion:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentVersion"
    .end annotation
.end field

.field public gbTextRange:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gbTextRange"
    .end annotation
.end field

.field public imageCfiRange:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "imageCfiRange"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
