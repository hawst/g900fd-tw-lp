.class Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;
.super Ljava/lang/Object;
.source "SentenceSearchTextProcessor.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SentenceBeforeAndAfter"
.end annotation


# static fields
.field private static EMPTY:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;


# instance fields
.field private mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

.field private mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 182
    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;-><init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V

    sput-object v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->EMPTY:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    sget-object v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->EMPTY:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    iput-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;-><init>()V

    return-void
.end method


# virtual methods
.method public needsMoreSentencesToFinishCurrentMatch()Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSentence(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;Ljava/util/Collection;Z)V
    .locals 6
    .param p1, "sentence"    # Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    .param p3, "endOfDocument"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p2, "queue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    if-eqz v1, :cond_3

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->isComplete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    iput-object p1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .line 196
    if-eqz p3, :cond_0

    .line 197
    new-array v1, v5, [Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    iget-object v2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->buildSearchResult([Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;)Lcom/google/android/apps/books/model/SearchResult;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->isComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    move-object v0, p1

    .line 204
    .local v0, "sentenceAfter":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    iget-object v2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    aput-object v2, v1, v4

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->withNoResults()Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->buildSearchResult([Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;)Lcom/google/android/apps/books/model/SearchResult;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 206
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->hasMatch()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->withNoResults()Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .line 209
    iput-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    goto :goto_0

    .line 212
    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .line 213
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    goto :goto_0

    .line 216
    .end local v0    # "sentenceAfter":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->hasMatch()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 217
    iput-object p1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mMatchingSentence:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    goto :goto_0

    .line 219
    :cond_4
    iput-object p1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;->mPotentialSentenceBefore:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    goto :goto_0
.end method
