.class public Lcom/google/android/apps/books/widget/PagesView2D;
.super Landroid/widget/FrameLayout;
.source "PagesView2D.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PagesView2D$1;,
        Lcom/google/android/apps/books/widget/PagesView2D$LRPair;,
        Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    }
.end annotation


# instance fields
.field private final mBitmapConfig:Landroid/graphics/Bitmap$Config;

.field private final mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

.field private mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

.field private mDestroyed:Z

.field private mDisplayFitWidth:Z

.field private mDisplayTwoPages:Z

.field private volatile mExecutingInitialLoadTransition:Z

.field private final mGetPageFittedSizeResult:Landroid/graphics/Point;

.field private final mLetterboxMasks:[Landroid/view/View;

.field private final mLoadedPageViews:[Lcom/google/android/apps/books/widget/SinglePageView;

.field private mLoadingPageVisible:Z

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mPageBitmapsAndBookmarks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;",
            ">;"
        }
    .end annotation
.end field

.field private mPageWidth:I

.field private final mProgressViews:[Landroid/widget/ProgressBar;

.field private mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

.field private final mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

.field private final mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

.field private mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x6

    const/4 v6, 0x0

    .line 161
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 101
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDestroyed:Z

    .line 103
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mExecutingInitialLoadTransition:Z

    .line 106
    invoke-static {v7}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    .line 112
    new-array v3, v7, [Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mProgressViews:[Landroid/widget/ProgressBar;

    .line 117
    new-array v3, v7, [Lcom/google/android/apps/books/widget/SinglePageView;

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLoadedPageViews:[Lcom/google/android/apps/books/widget/SinglePageView;

    .line 124
    new-array v3, v9, [Landroid/view/View;

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    .line 132
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDisplayTwoPages:Z

    .line 133
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDisplayFitWidth:Z

    .line 141
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    new-instance v4, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;-><init>(Lcom/google/android/apps/books/widget/PagesView2D$1;)V

    aput-object v4, v3, v6

    new-instance v4, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;-><init>(Lcom/google/android/apps/books/widget/PagesView2D$1;)V

    aput-object v4, v3, v8

    new-instance v4, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;-><init>(Lcom/google/android/apps/books/widget/PagesView2D$1;)V

    aput-object v4, v3, v9

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    .line 147
    new-instance v3, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    invoke-direct {v3, v5}, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;-><init>(Lcom/google/android/apps/books/widget/PagesView2D$1;)V

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    .line 359
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mGetPageFittedSizeResult:Landroid/graphics/Point;

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->from(Landroid/content/res/Resources;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .line 164
    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/widget/PagesView2D;->setWillNotDraw(Z)V

    .line 166
    invoke-virtual {p0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->setFocusable(Z)V

    .line 167
    invoke-virtual {p0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->setFocusableInTouchMode(Z)V

    .line 169
    const/4 v0, 0x0

    .local v0, "ii":I
    :goto_0
    if-ge v0, v7, :cond_0

    .line 170
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mProgressViews:[Landroid/widget/ProgressBar;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesView2D;->makeProgressView(Landroid/content/Context;)Landroid/widget/ProgressBar;

    move-result-object v4

    aput-object v4, v3, v0

    .line 171
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLoadedPageViews:[Lcom/google/android/apps/books/widget/SinglePageView;

    new-instance v4, Lcom/google/android/apps/books/widget/SinglePageView;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-direct {v4, p1, v5}, Lcom/google/android/apps/books/widget/SinglePageView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    aput-object v4, v3, v0

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_0
    const/4 v1, 0x0

    .local v1, "jj":I
    :goto_1
    if-ge v1, v9, :cond_1

    .line 175
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 176
    .local v2, "mask":Landroid/view/View;
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 177
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    aput-object v2, v3, v1

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 180
    .end local v2    # "mask":Landroid/view/View;
    :cond_1
    const/16 v3, -0x100

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesView2D;->setBackgroundColor(I)V

    .line 181
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->chooseBitmapConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mBitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 183
    invoke-virtual {p0, v6, v6}, Lcom/google/android/apps/books/widget/PagesView2D;->setDisplayTwoPages(ZZ)V

    .line 185
    new-instance v3, Lcom/google/android/apps/books/util/ProductionLogger;

    invoke-direct {v3, p1}, Lcom/google/android/apps/books/util/ProductionLogger;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 186
    return-void
.end method

.method private chooseBitmapConfig()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method private computeLetterbox()V
    .locals 8

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->displayTwoPages()Z

    move-result v5

    .line 366
    .local v5, "twoPages":Z
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageWidth()I

    move-result v1

    .line 370
    .local v1, "onePageWidth":I
    const/4 v3, 0x0

    .local v3, "screenIndex":I
    :goto_0
    const/4 v6, 0x2

    if-gt v3, v6, :cond_2

    .line 371
    if-eqz v5, :cond_0

    add-int/lit8 v6, v3, -0x1

    mul-int/lit8 v2, v6, 0x2

    .line 372
    .local v2, "relativeOffset":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    aget-object v0, v6, v3

    .line 373
    .local v0, "currLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    if-eqz v5, :cond_1

    .line 374
    add-int/lit8 v6, v2, -0x1

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    sub-int v6, v1, v6

    iput v6, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    .line 375
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    sub-int v6, v1, v6

    iput v6, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    .line 370
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 371
    .end local v0    # "currLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    .end local v2    # "relativeOffset":I
    :cond_0
    add-int/lit8 v2, v3, -0x1

    goto :goto_1

    .line 377
    .restart local v0    # "currLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    .restart local v2    # "relativeOffset":I
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v6

    iget v4, v6, Landroid/graphics/Point;->x:I

    .line 378
    .local v4, "thisPageWidth":I
    sub-int v6, v1, v4

    div-int/lit8 v6, v6, 0x2

    iput v6, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    .line 379
    sub-int v6, v1, v4

    iget v7, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    sub-int/2addr v6, v7

    iput v6, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    goto :goto_2

    .line 382
    .end local v0    # "currLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    .end local v2    # "relativeOffset":I
    .end local v4    # "thisPageWidth":I
    :cond_2
    return-void
.end method

.method private displayTwoPages()Z
    .locals 1

    .prologue
    .line 314
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDisplayTwoPages:Z

    return v0
.end method

.method private getBaseLeftForRelativeOffset(I)I
    .locals 2
    .param p1, "relativeOffset"    # I

    .prologue
    .line 336
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->displayTwoPages()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    add-int v0, p1, v1

    .line 337
    .local v0, "numberPagesToSkip":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageWidth()I

    move-result v1

    mul-int/2addr v1, v0

    return v1

    .line 336
    .end local v0    # "numberPagesToSkip":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getChildrenCountForSwipe()I
    .locals 2

    .prologue
    .line 617
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    array-length v1, v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private getNearbyRelativeOffsets()[I
    .locals 1

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->displayTwoPages()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/PagesViewUtils;->getRelativePageOffsets(Z)[I

    move-result-object v0

    return-object v0
.end method

.method private getPageFittedSize(I)Landroid/graphics/Point;
    .locals 6
    .param p1, "nearbyRelativeOffset"    # I

    .prologue
    .line 346
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    if-nez v3, :cond_0

    const/4 v2, 0x0

    .line 349
    .local v2, "offsetFromBase":I
    :goto_0
    add-int v0, p1, v2

    .line 350
    .local v0, "absoluteOffset":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;

    .line 351
    .local v1, "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    if-eqz v1, :cond_1

    .line 352
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mGetPageFittedSizeResult:Landroid/graphics/Point;

    iget-object v4, v1, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->fitSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, v1, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->fitSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 356
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mGetPageFittedSizeResult:Landroid/graphics/Point;

    return-object v3

    .line 346
    .end local v0    # "absoluteOffset":I
    .end local v1    # "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    .end local v2    # "offsetFromBase":I
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v3}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v2

    goto :goto_0

    .line 354
    .restart local v0    # "absoluteOffset":I
    .restart local v1    # "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    .restart local v2    # "offsetFromBase":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mGetPageFittedSizeResult:Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    goto :goto_1
.end method

.method private getPageView(I)Lcom/google/android/apps/books/widget/SinglePageView;
    .locals 7
    .param p1, "offsetFromBasePosition"    # I

    .prologue
    const/4 v5, 0x0

    .line 745
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v2

    .line 751
    .local v2, "childViewPageOffsets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .line 752
    .local v4, "viewIndex":I
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 753
    .local v1, "childViewPageOffset":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v6, p1, :cond_1

    .line 754
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 755
    .local v0, "child":Landroid/view/View;
    instance-of v6, v0, Lcom/google/android/apps/books/widget/SinglePageView;

    if-eqz v6, :cond_0

    .line 756
    check-cast v0, Lcom/google/android/apps/books/widget/SinglePageView;

    .line 763
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childViewPageOffset":Ljava/lang/Integer;
    :goto_1
    return-object v0

    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "childViewPageOffset":Ljava/lang/Integer;
    :cond_0
    move-object v0, v5

    .line 758
    goto :goto_1

    .line 760
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 761
    goto :goto_0

    .end local v1    # "childViewPageOffset":Ljava/lang/Integer;
    :cond_2
    move-object v0, v5

    .line 763
    goto :goto_1
.end method

.method private getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .locals 1
    .param p1, "offsetFromMainPage"    # I

    .prologue
    .line 580
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDisplayTwoPages:Z

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/PagesViewUtils;->getSideOfSpine(ZI)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v0

    return-object v0
.end method

.method private layoutProgressView(Landroid/view/View;II)V
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "pageLeft"    # I
    .param p3, "pageWidth"    # I

    .prologue
    .line 484
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 485
    .local v8, "progressWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 486
    .local v7, "progressHeight":I
    sub-int v0, p3, v8

    div-int/lit8 v6, v0, 0x2

    .line 487
    .local v6, "leftPadding":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getHeight()I

    move-result v0

    sub-int/2addr v0, v7

    div-int/lit8 v9, v0, 0x2

    .line 488
    .local v9, "topPadding":I
    add-int v2, p2, v6

    .line 489
    .local v2, "left":I
    add-int v4, v2, v8

    .line 490
    .local v4, "right":I
    move v3, v9

    .line 491
    .local v3, "top":I
    add-int v5, v3, v7

    .line 492
    .local v5, "bottom":I
    const-string v0, "PagesView2D"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    const-string v0, "PagesView2D"

    const-string v1, "Position progress view at %d/%d/%d/%d"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    .line 497
    return-void
.end method

.method private makeProgressView(Landroid/content/Context;)Landroid/widget/ProgressBar;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 209
    .local v0, "result":Landroid/widget/ProgressBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 210
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 211
    return-object v0
.end method

.method private makeProgressViewLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 504
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method private setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "offset"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p4, "page"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p5, "specialPageDisplaySize"    # Landroid/graphics/Point;

    .prologue
    .line 238
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDestroyed:Z

    if-eqz v1, :cond_0

    .line 252
    :goto_0
    return-void

    .line 243
    :cond_0
    if-eqz p5, :cond_1

    .line 244
    move-object v0, p5

    .line 250
    .local v0, "drawSize":Landroid/graphics/Point;
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;

    invoke-direct {v3, p2, v0, p3, p4}, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Point;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->updateViews()V

    goto :goto_0

    .line 246
    .end local v0    # "drawSize":Landroid/graphics/Point;
    :cond_1
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 247
    .restart local v0    # "drawSize":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    goto :goto_1
.end method

.method private setSwipeDistance(F)V
    .locals 4
    .param p1, "distance"    # F

    .prologue
    .line 621
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildrenCountForSwipe()I

    move-result v0

    .line 622
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "ii":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 623
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationX(Landroid/view/View;F)V

    .line 622
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 625
    :cond_0
    return-void
.end method

.method private updateViews()V
    .locals 25

    .prologue
    .line 515
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->removeAllViews()V

    .line 517
    const/4 v7, 0x0

    .line 518
    .local v7, "currentProgressViewIndex":I
    const/4 v5, 0x0

    .line 519
    .local v5, "currentImageViewIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    move-object/from16 v22, v0

    if-nez v22, :cond_1

    const/4 v14, 0x0

    .line 522
    .local v14, "offsetFromBase":I
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getNearbyRelativeOffsets()[I

    move-result-object v4

    .local v4, "arr$":[I
    array-length v11, v4

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move v6, v5

    .end local v5    # "currentImageViewIndex":I
    .local v6, "currentImageViewIndex":I
    move v8, v7

    .end local v7    # "currentProgressViewIndex":I
    .local v8, "currentProgressViewIndex":I
    :goto_1
    if-ge v10, v11, :cond_7

    aget v18, v4, v10

    .line 523
    .local v18, "relativeOffset":I
    add-int v3, v18, v14

    .line 524
    .local v3, "absoluteOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    move-object/from16 v22, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;

    .line 527
    .local v9, "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    if-eqz v9, :cond_4

    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    if-eqz v22, :cond_4

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLoadedPageViews:[Lcom/google/android/apps/books/widget/SinglePageView;

    move-object/from16 v22, v0

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "currentImageViewIndex":I
    .restart local v5    # "currentImageViewIndex":I
    aget-object v15, v22, v6

    .line 531
    .local v15, "pageView":Lcom/google/android/apps/books/widget/SinglePageView;
    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->pageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    move-object/from16 v22, v0

    sget-object v23, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_2

    .line 532
    sget-object v19, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    .line 538
    .local v19, "scaleType":Landroid/widget/ImageView$ScaleType;
    :goto_2
    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Lcom/google/android/apps/books/widget/SinglePageView;->setPageBitmap(Landroid/graphics/Bitmap;Landroid/widget/ImageView$ScaleType;)V

    .line 540
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/PagesView2D;->getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v20

    .line 541
    .local v20, "sideOfSpine":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/google/android/apps/books/widget/SinglePageView;->setSideOfSpine(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 543
    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    .line 544
    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Lcom/google/android/apps/books/widget/SinglePageView;->attachBookmarkAnimator(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 549
    :goto_3
    move-object/from16 v21, v15

    .line 550
    .local v21, "view":Landroid/view/View;
    new-instance v16, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->fitSize:Landroid/graphics/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v22, v0

    iget-object v0, v9, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;->fitSize:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 552
    .local v16, "params":Landroid/widget/FrameLayout$LayoutParams;
    const-string v22, "PagesView2D"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 553
    const-string v22, "PagesView2D"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Found bitmap for relative page "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v7, v8

    .line 566
    .end local v8    # "currentProgressViewIndex":I
    .end local v15    # "pageView":Lcom/google/android/apps/books/widget/SinglePageView;
    .end local v19    # "scaleType":Landroid/widget/ImageView$ScaleType;
    .end local v20    # "sideOfSpine":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .restart local v7    # "currentProgressViewIndex":I
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 522
    add-int/lit8 v10, v10, 0x1

    move v6, v5

    .end local v5    # "currentImageViewIndex":I
    .restart local v6    # "currentImageViewIndex":I
    move v8, v7

    .end local v7    # "currentProgressViewIndex":I
    .restart local v8    # "currentProgressViewIndex":I
    goto/16 :goto_1

    .line 519
    .end local v3    # "absoluteOffset":I
    .end local v4    # "arr$":[I
    .end local v6    # "currentImageViewIndex":I
    .end local v8    # "currentProgressViewIndex":I
    .end local v9    # "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v14    # "offsetFromBase":I
    .end local v16    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v18    # "relativeOffset":I
    .end local v21    # "view":Landroid/view/View;
    .restart local v5    # "currentImageViewIndex":I
    .restart local v7    # "currentProgressViewIndex":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v14

    goto/16 :goto_0

    .line 535
    .end local v7    # "currentProgressViewIndex":I
    .restart local v3    # "absoluteOffset":I
    .restart local v4    # "arr$":[I
    .restart local v8    # "currentProgressViewIndex":I
    .restart local v9    # "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    .restart local v10    # "i$":I
    .restart local v11    # "len$":I
    .restart local v14    # "offsetFromBase":I
    .restart local v15    # "pageView":Lcom/google/android/apps/books/widget/SinglePageView;
    .restart local v18    # "relativeOffset":I
    :cond_2
    sget-object v19, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    .restart local v19    # "scaleType":Landroid/widget/ImageView$ScaleType;
    goto/16 :goto_2

    .line 546
    .restart local v20    # "sideOfSpine":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    :cond_3
    invoke-virtual {v15}, Lcom/google/android/apps/books/widget/SinglePageView;->hideAllBookmarks()V

    goto :goto_3

    .line 556
    .end local v5    # "currentImageViewIndex":I
    .end local v15    # "pageView":Lcom/google/android/apps/books/widget/SinglePageView;
    .end local v19    # "scaleType":Landroid/widget/ImageView$ScaleType;
    .end local v20    # "sideOfSpine":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .restart local v6    # "currentImageViewIndex":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mProgressViews:[Landroid/widget/ProgressBar;

    move-object/from16 v22, v0

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "currentProgressViewIndex":I
    .restart local v7    # "currentProgressViewIndex":I
    aget-object v17, v22, v8

    .line 557
    .local v17, "progressBar":Landroid/widget/ProgressBar;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->isExecutingInitialLoadTransition()Z

    move-result v22

    if-eqz v22, :cond_6

    const/16 v22, 0x8

    :goto_5
    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 559
    move-object/from16 v21, v17

    .line 561
    .restart local v21    # "view":Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->makeProgressViewLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v16

    .line 562
    .restart local v16    # "params":Landroid/widget/FrameLayout$LayoutParams;
    const-string v22, "PagesView2D"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 563
    const-string v22, "PagesView2D"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Showing progress indicator for relative page "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move v5, v6

    .end local v6    # "currentImageViewIndex":I
    .restart local v5    # "currentImageViewIndex":I
    goto :goto_4

    .line 557
    .end local v5    # "currentImageViewIndex":I
    .end local v16    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v21    # "view":Landroid/view/View;
    .restart local v6    # "currentImageViewIndex":I
    :cond_6
    const/16 v22, 0x0

    goto :goto_5

    .line 570
    .end local v3    # "absoluteOffset":I
    .end local v7    # "currentProgressViewIndex":I
    .end local v9    # "data":Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;
    .end local v17    # "progressBar":Landroid/widget/ProgressBar;
    .end local v18    # "relativeOffset":I
    .restart local v8    # "currentProgressViewIndex":I
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getWidth()I

    move-result v22

    mul-int/lit8 v13, v22, 0x2

    .line 571
    .local v13, "maskWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getHeight()I

    move-result v22

    mul-int/lit8 v12, v22, 0x2

    .line 572
    .local v12, "maskHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aget-object v22, v22, v23

    new-instance v23, Landroid/widget/FrameLayout$LayoutParams;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    aget-object v22, v22, v23

    new-instance v23, Landroid/widget/FrameLayout$LayoutParams;

    move-object/from16 v0, v23

    invoke-direct {v0, v13, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 576
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->requestLayout()V

    .line 577
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    if-nez v0, :cond_1

    .line 843
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 855
    :cond_0
    :goto_0
    return-void

    .line 847
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/ViewScale;->applyTo(Landroid/graphics/Canvas;)V

    .line 849
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 850
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 851
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ViewScale;->isIdentity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    goto :goto_0
.end method

.method public dxToFraction(FLcom/google/android/apps/books/util/ScreenDirection;)F
    .locals 7
    .param p1, "dx"    # F
    .param p2, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 585
    sget-object v4, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p2, v4, :cond_0

    const/4 v1, 0x1

    .line 586
    .local v1, "goingRight":Z
    :goto_0
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    iget v3, v4, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    .line 593
    .local v3, "swipeWidth":I
    :goto_1
    invoke-static {p2}, Lcom/google/android/apps/books/util/ScreenDirection;->oppositeOf(Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v2

    .line 594
    .local v2, "swipeDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v2, p1}, Lcom/google/android/apps/books/util/ScreenDirection;->dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;F)F

    move-result v0

    .line 595
    .local v0, "dxInSwipeDirection":F
    int-to-float v4, v3

    div-float v4, v0, v4

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v4

    return v4

    .line 585
    .end local v0    # "dxInSwipeDirection":F
    .end local v1    # "goingRight":Z
    .end local v2    # "swipeDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    .end local v3    # "swipeWidth":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 586
    .restart local v1    # "goingRight":Z
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    iget v3, v4, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    goto :goto_1
.end method

.method public finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V
    .locals 20
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "flinging"    # Z
    .param p3, "velocity"    # F
    .param p4, "isCompletion"    # Z

    .prologue
    .line 657
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildrenCountForSwipe()I

    move-result v10

    .line 658
    .local v10, "childCount":I
    const-wide/16 v12, 0x0

    .line 659
    .local v12, "flingDuration":J
    const/4 v14, 0x0

    .local v14, "ii":I
    :goto_0
    if-ge v14, v10, :cond_3

    .line 660
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 662
    .local v2, "child":Landroid/view/View;
    if-eqz p4, :cond_1

    .line 663
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    move-object/from16 v0, p1

    if-ne v0, v3, :cond_0

    .line 664
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    iget v3, v3, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    neg-int v3, v3

    int-to-float v5, v3

    .line 675
    .local v5, "newTranslation":F
    :goto_1
    if-nez v14, :cond_2

    const/4 v6, 0x1

    .line 677
    .local v6, "addListener":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    const-string v8, "PagesView2D"

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/books/util/ViewUtils;->finish2DPageTurn(Landroid/view/View;ZFFZLcom/google/android/apps/books/widget/PagesView$Callbacks;Ljava/lang/String;)V

    .line 659
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 669
    .end local v5    # "newTranslation":F
    .end local v6    # "addListener":Z
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    iget v3, v3, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    int-to-float v5, v3

    .restart local v5    # "newTranslation":F
    goto :goto_1

    .line 672
    .end local v5    # "newTranslation":F
    :cond_1
    const/4 v5, 0x0

    .restart local v5    # "newTranslation":F
    goto :goto_1

    .line 675
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 682
    .end local v2    # "child":Landroid/view/View;
    .end local v5    # "newTranslation":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v4, 0x1

    aget-object v16, v3, v4

    .line 684
    .local v16, "mainLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    if-eqz p4, :cond_7

    .line 685
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    move-object/from16 v0, p1

    if-ne v0, v3, :cond_5

    const/4 v11, 0x1

    .line 686
    .local v11, "goingRight":Z
    :goto_3
    if-eqz v11, :cond_6

    const/16 v17, 0x2

    .line 687
    .local v17, "targetIndex":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    aget-object v18, v3, v17

    .line 692
    .end local v11    # "goingRight":Z
    .end local v17    # "targetIndex":I
    .local v18, "targetScreenLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    :goto_5
    const/4 v15, 0x0

    .local v15, "jj":I
    :goto_6
    const/4 v3, 0x2

    if-ge v15, v3, :cond_9

    .line 693
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    aget-object v2, v3, v15

    .line 694
    .restart local v2    # "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v3, v2}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;

    move-result-object v19

    .line 695
    .local v19, "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    if-nez v15, :cond_8

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    sub-int/2addr v3, v4

    int-to-float v5, v3

    .line 698
    .restart local v5    # "newTranslation":F
    :goto_7
    const-string v3, "translationX"

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v7, 0x0

    aput v5, v4, v7

    move-object/from16 v0, v19

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 701
    .local v9, "animator":Landroid/animation/Animator;
    if-eqz p2, :cond_4

    .line 702
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v9, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 704
    invoke-virtual {v9, v12, v13}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 707
    :cond_4
    invoke-virtual {v9}, Landroid/animation/Animator;->start()V

    .line 692
    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    .line 685
    .end local v2    # "child":Landroid/view/View;
    .end local v5    # "newTranslation":F
    .end local v9    # "animator":Landroid/animation/Animator;
    .end local v15    # "jj":I
    .end local v18    # "targetScreenLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    .end local v19    # "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    :cond_5
    const/4 v11, 0x0

    goto :goto_3

    .line 686
    .restart local v11    # "goingRight":Z
    :cond_6
    const/16 v17, 0x0

    goto :goto_4

    .line 689
    .end local v11    # "goingRight":Z
    :cond_7
    move-object/from16 v18, v16

    .restart local v18    # "targetScreenLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    goto :goto_5

    .line 695
    .restart local v2    # "child":Landroid/view/View;
    .restart local v15    # "jj":I
    .restart local v19    # "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    :cond_8
    move-object/from16 v0, v16

    iget v3, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    move-object/from16 v0, v18

    iget v4, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    sub-int/2addr v3, v4

    int-to-float v5, v3

    goto :goto_7

    .line 710
    .end local v2    # "child":Landroid/view/View;
    .end local v19    # "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    :cond_9
    return-void
.end method

.method public getBitmapConfig()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mBitmapConfig:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public getOnePageHeight()I
    .locals 1

    .prologue
    .line 781
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getHeight()I

    move-result v0

    return v0
.end method

.method public getOnePageWidth()I
    .locals 2

    .prologue
    .line 773
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageWidth:I

    if-nez v0, :cond_0

    .line 774
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->displayTwoPages()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/PagesViewUtils;->getPageWidth(IZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageWidth:I

    .line 776
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageWidth:I

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 732
    return-object p0
.end method

.method public inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
    .locals 9
    .param p1, "event"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    .line 878
    iget v3, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->offset:I

    .line 879
    .local v3, "offsetFromBasePosition":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v8}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v8

    sub-int v4, v3, v8

    .line 881
    .local v4, "offsetFromMainPage":I
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v6

    .line 882
    .local v6, "pageSize":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageWidth()I

    move-result v1

    .line 883
    .local v1, "availablePageWidth":I
    iget v8, v6, Landroid/graphics/Point;->x:I

    sub-int v2, v1, v8

    .line 884
    .local v2, "horizontalLetterbox":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->displayTwoPages()Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    div-int v5, v2, v8

    .line 885
    .local v5, "outerHorizontalLetterbox":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getOnePageHeight()I

    move-result v0

    .line 886
    .local v0, "availablePageHeight":I
    iget v8, v6, Landroid/graphics/Point;->y:I

    sub-int v8, v0, v8

    div-int/lit8 v7, v8, 0x2

    .line 887
    .local v7, "verticalLetterbox":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v8, p1, v1, v5, v7}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;III)Z

    move-result v8

    return v8

    .line 884
    .end local v0    # "availablePageHeight":I
    .end local v5    # "outerHorizontalLetterbox":I
    .end local v7    # "verticalLetterbox":I
    :cond_0
    const/4 v8, 0x2

    goto :goto_0
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 1
    .param p1, "location"    # [I
    .param p2, "dirty"    # Landroid/graphics/Rect;

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/util/ViewScale;->applyToDirtyRect([ILandroid/graphics/Rect;)V

    .line 864
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public isCached(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isExecutingInitialLoadTransition()Z
    .locals 1

    .prologue
    .line 868
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mExecutingInitialLoadTransition:Z

    return v0
.end method

.method public onBasePositionChanged()V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesView2D;->setSwipeDistance(F)V

    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->refreshPages()V

    .line 258
    return-void
.end method

.method public onBookmarkChanged(IZ)V
    .locals 1
    .param p1, "targetOffset"    # I
    .param p2, "isGainingBookmark"    # Z

    .prologue
    .line 737
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageView(I)Lcom/google/android/apps/books/widget/SinglePageView;

    move-result-object v0

    .line 738
    .local v0, "child":Lcom/google/android/apps/books/widget/SinglePageView;
    if-eqz v0, :cond_0

    .line 739
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SinglePageView;->onBookmarksChanged()V

    .line 741
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 285
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDestroyed:Z

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 287
    const/4 v0, 0x0

    .local v0, "ii":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLoadedPageViews:[Lcom/google/android/apps/books/widget/SinglePageView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/SinglePageView;->releaseBitmap()V

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    :cond_0
    return-void
.end method

.method public onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZI)V
    .locals 2
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "completed"    # Z
    .param p3, "offset"    # I

    .prologue
    .line 717
    if-eqz p2, :cond_0

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->flipPage(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 720
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesView2D;->setSwipeDistance(F)V

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->loadPages([I)V

    .line 722
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->updateViews()V

    .line 723
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 26
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 387
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->displayTwoPages()Z

    move-result v25

    .line 388
    .local v25, "twoPages":Z
    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/books/util/PagesViewUtils;->getRelativePageOffsets(Z)[I

    move-result-object v21

    .line 389
    .local v21, "nearbyRelativeOffsets":[I
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v24, v0

    .line 391
    .local v24, "relevantPagesCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildCount()I

    move-result v2

    move/from16 v0, v24

    if-ge v2, v0, :cond_2

    .line 394
    const-string v2, "PagesView2D"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 395
    const-string v2, "PagesView2D"

    const-string v8, "Not as many views as pages"

    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 478
    :cond_1
    :goto_0
    return-void

    .line 401
    :cond_2
    const/4 v15, 0x0

    .line 403
    .local v15, "loadingPageVisible":Z
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getWidth()I

    move-result v20

    .line 404
    .local v20, "myWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getHeight()I

    move-result v13

    .line 407
    .local v13, "myHeight":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesView2D;->computeLetterbox()V

    .line 415
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->getBaseLeftForRelativeOffset(I)I

    move-result v19

    .line 416
    .local v19, "minLeft":I
    if-nez v25, :cond_3

    .line 418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v8, 0x1

    aget-object v2, v2, v8

    iget v2, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    add-int v19, v19, v2

    .line 420
    :cond_3
    move/from16 v18, v19

    .line 421
    .local v18, "maxRight":I
    const/4 v14, 0x0

    .local v14, "ii":I
    :goto_1
    move/from16 v0, v24

    if-ge v14, v0, :cond_7

    .line 422
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/books/widget/PagesView2D;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 426
    .local v3, "child":Landroid/view/View;
    aget v23, v21, v14

    .line 427
    .local v23, "relativeOffset":I
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v22

    .line 429
    .local v22, "pageSize":Landroid/graphics/Point;
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->y:I

    sub-int v2, v13, v2

    div-int/lit8 v5, v2, 0x2

    .line 430
    .local v5, "pageTop":I
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->y:I

    add-int v7, v5, v2

    .line 433
    .local v7, "pageBottom":I
    if-ltz v23, :cond_4

    .line 434
    move/from16 v4, v18

    .line 435
    .local v4, "pageLeft":I
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    add-int v6, v4, v2

    .line 436
    .local v6, "pageRight":I
    move/from16 v18, v6

    .line 443
    :goto_2
    instance-of v2, v3, Landroid/widget/ProgressBar;

    if-eqz v2, :cond_6

    .line 444
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/apps/books/widget/PagesView2D;->layoutProgressView(Landroid/view/View;II)V

    .line 445
    move/from16 v0, v20

    if-ge v4, v0, :cond_5

    move/from16 v0, p2

    if-le v6, v0, :cond_5

    const/4 v2, 0x1

    :goto_3
    or-int/2addr v15, v2

    .line 421
    :goto_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 438
    .end local v4    # "pageLeft":I
    .end local v6    # "pageRight":I
    :cond_4
    move/from16 v6, v19

    .line 439
    .restart local v6    # "pageRight":I
    move-object/from16 v0, v22

    iget v2, v0, Landroid/graphics/Point;->x:I

    sub-int v4, v6, v2

    .line 440
    .restart local v4    # "pageLeft":I
    move/from16 v19, v4

    goto :goto_2

    .line 445
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 447
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    goto :goto_4

    .line 451
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "pageLeft":I
    .end local v5    # "pageTop":I
    .end local v6    # "pageRight":I
    .end local v7    # "pageBottom":I
    .end local v22    # "pageSize":Landroid/graphics/Point;
    .end local v23    # "relativeOffset":I
    :cond_7
    mul-int/lit8 v17, v20, 0x2

    .line 452
    .local v17, "maskWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v8, 0x1

    aget-object v16, v2, v8

    .line 453
    .local v16, "mainLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    const/4 v9, 0x0

    aget-object v9, v2, v9

    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    sub-int v10, v2, v17

    const/4 v11, 0x0

    move-object/from16 v0, v16

    iget v12, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    invoke-virtual/range {v8 .. v13}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    .line 455
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    const/4 v9, 0x1

    aget-object v9, v2, v9

    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    sub-int v10, v20, v2

    const/4 v11, 0x0

    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    sub-int v2, v20, v2

    add-int v12, v2, v17

    invoke-virtual/range {v8 .. v13}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    .line 458
    if-eqz v25, :cond_8

    .line 462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    add-int/2addr v8, v9

    iput v8, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    .line 463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    const/4 v9, -0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    add-int/2addr v8, v9

    iput v8, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    .line 472
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLoadingPageVisible:Z

    if-eq v15, v2, :cond_1

    .line 473
    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mLoadingPageVisible:Z

    .line 474
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    if-eqz v2, :cond_1

    .line 475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v2, v15}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->onLoadingPageVisibilityChanged(Z)V

    goto/16 :goto_0

    .line 468
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    add-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    iput v8, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    .line 469
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    const/4 v9, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/books/widget/PagesView2D;->getPageFittedSize(I)Landroid/graphics/Point;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Point;->x:I

    add-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    iput v8, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    goto :goto_5
.end method

.method public onPageOffsetChanged(I)V
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 267
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 270
    .local v0, "entries":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;>;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v3}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v2

    .line 272
    .local v2, "visiblePages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 273
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 274
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 275
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 278
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;>;"
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method public onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 0
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 713
    return-void
.end method

.method public refreshPages()V
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->loadPages([I)V

    .line 310
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->updateViews()V

    .line 311
    return-void
.end method

.method public resetZoom()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 609
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1, v1}, Lcom/google/android/apps/books/widget/PagesView2D;->setZoom(FFF)V

    .line 610
    return-void
.end method

.method public setCallbacks(Lcom/google/android/apps/books/widget/PagesView$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    .line 196
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->updateViews()V

    .line 197
    return-void
.end method

.method public setDisplayTwoPages(ZZ)V
    .locals 1
    .param p1, "displayTwoPages"    # Z
    .param p2, "displayFitWidth"    # Z

    .prologue
    .line 325
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDisplayTwoPages:Z

    .line 326
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mDisplayFitWidth:Z

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->getWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/PagesViewUtils;->getPageWidth(IZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageWidth:I

    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->updateViews()V

    .line 329
    return-void
.end method

.method public setExecutingInitialLoadTransition(Z)V
    .locals 0
    .param p1, "executing"    # Z

    .prologue
    .line 873
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mExecutingInitialLoadTransition:Z

    .line 874
    return-void
.end method

.method public setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;FI)V
    .locals 11
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "fraction"    # F
    .param p3, "offsetFromBasePosition"    # I

    .prologue
    .line 630
    sget-object v8, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v8, :cond_0

    const/4 v0, 0x1

    .line 631
    .local v0, "goingRight":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    iget v5, v8, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    .line 636
    .local v5, "swipeWidth":I
    :goto_1
    invoke-static {p1}, Lcom/google/android/apps/books/util/ScreenDirection;->oppositeOf(Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v4

    .line 637
    .local v4, "swipeDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    int-to-float v8, v5

    mul-float/2addr v8, p2

    invoke-static {v4, v8}, Lcom/google/android/apps/books/util/ScreenDirection;->dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;F)F

    move-result v8

    invoke-direct {p0, v8}, Lcom/google/android/apps/books/widget/PagesView2D;->setSwipeDistance(F)V

    .line 639
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    const/4 v9, 0x1

    aget-object v2, v8, v9

    .line 640
    .local v2, "mainLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    if-eqz v0, :cond_2

    const/4 v6, 0x2

    .line 641
    .local v6, "targetIndex":I
    :goto_2
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScreensLetterbox:[Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    aget-object v7, v8, v6

    .line 644
    .local v7, "targetScreenLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    const/4 v8, 0x0

    iget v9, v7, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    iget v10, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-static {v8, v9, p2}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v1

    .line 645
    .local v1, "lLB":F
    const/4 v8, 0x0

    iget v9, v7, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    iget v10, v2, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->right:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-static {v8, v9, p2}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v3

    .line 647
    .local v3, "rLB":F
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-virtual {v8, v9, v1}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationX(Landroid/view/View;F)V

    .line 648
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    neg-float v10, v3

    invoke-virtual {v8, v9, v10}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationX(Landroid/view/View;F)V

    .line 649
    return-void

    .line 630
    .end local v0    # "goingRight":Z
    .end local v1    # "lLB":F
    .end local v2    # "mainLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    .end local v3    # "rLB":F
    .end local v4    # "swipeDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    .end local v5    # "swipeWidth":I
    .end local v6    # "targetIndex":I
    .end local v7    # "targetScreenLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 631
    .restart local v0    # "goingRight":Z
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mSwipeLength:Lcom/google/android/apps/books/widget/PagesView2D$LRPair;

    iget v5, v8, Lcom/google/android/apps/books/widget/PagesView2D$LRPair;->left:I

    goto :goto_1

    .line 640
    .restart local v2    # "mainLB":Lcom/google/android/apps/books/widget/PagesView2D$LRPair;
    .restart local v4    # "swipeDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    .restart local v5    # "swipeWidth":I
    :cond_2
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public setPageBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PagesView2D;->setBackgroundColor(I)V

    .line 202
    const/4 v0, 0x0

    .local v0, "jj":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mLetterboxMasks:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_0
    return-void
.end method

.method public setPageLoading(ILandroid/graphics/Point;)V
    .locals 4
    .param p1, "offset"    # I
    .param p2, "displaySize"    # Landroid/graphics/Point;

    .prologue
    const/4 v3, 0x0

    .line 809
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mPageBitmapsAndBookmarks:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;

    invoke-direct {v2, v3, p2, v3, v3}, Lcom/google/android/apps/books/widget/PagesView2D$PageDrawingData;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Point;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 810
    return-void
.end method

.method public setPageRendering(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V
    .locals 7
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p4, "transitionStartTime"    # J

    .prologue
    const/4 v4, 0x0

    .line 217
    const-string v0, "painter can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v6

    .line 222
    .local v6, "relevantPages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 226
    :cond_0
    const-string v0, "PagesView2D"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    const-string v0, "PagesView2D"

    const-string v1, "PV2D creating bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_1
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/render/PagePainter;->getSharedBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .local v2, "ourBitmap":Landroid/graphics/Bitmap;
    move-object v0, p0

    move v1, p1

    move-object v3, p3

    move-object v5, v4

    .line 232
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesView2D;->setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method public setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;J)V
    .locals 7
    .param p1, "offset"    # I
    .param p2, "page"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "displaySize"    # Landroid/graphics/Point;
    .param p5, "transitionStartTime"    # J

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v6

    .line 795
    .local v6, "visiblePages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 802
    :cond_1
    if-eqz p3, :cond_0

    .line 803
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesView2D;->setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method public setPagesLoading()V
    .locals 0

    .prologue
    .line 819
    return-void
.end method

.method public setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V
    .locals 0
    .param p1, "translationHelper"    # Lcom/google/android/ublib/view/TranslationHelper;

    .prologue
    .line 826
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 827
    return-void
.end method

.method public setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V
    .locals 0
    .param p1, "direction"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    .line 787
    return-void
.end method

.method public setZoom(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    if-nez v0, :cond_0

    .line 601
    new-instance v0, Lcom/google/android/apps/books/util/ViewScale;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/ViewScale;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView2D;->mScaleInfo:Lcom/google/android/apps/books/util/ViewScale;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/util/ViewScale;->set(FFF)V

    .line 604
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView2D;->invalidate()V

    .line 605
    return-void
.end method

.method public supportsPageTurnMode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pageTurnMode"    # Ljava/lang/String;

    .prologue
    .line 768
    const-string v0, "turn2d"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
