.class public Lcom/google/android/apps/books/annotations/JsonAnnotations;
.super Ljava/lang/Object;
.source "JsonAnnotations.java"


# direct methods
.method public static asJsonAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    .locals 5
    .param p0, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p1, "a"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "palette"    # Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    .prologue
    .line 194
    new-instance v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;-><init>()V

    .line 196
    .local v0, "aa":Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getTextualContext()Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v1

    .line 198
    .local v1, "context":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->includePageIds(Lcom/google/android/apps/books/annotations/Annotation;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getPageId(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->pageIds:Ljava/util/List;

    .line 203
    :cond_0
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->beforeSelectedText:Ljava/lang/String;

    .line 204
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->selectedText:Ljava/lang/String;

    .line 205
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->afterSelectedText:Ljava/lang/String;

    .line 207
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->positionRangeForJson(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->createJsonClientVersionRanges(Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/render/PageStructureLocationRange;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->clientVersionRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    .line 209
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerId:Ljava/lang/String;

    .line 210
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getAnnotationType()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->annotationType:Ljava/lang/String;

    .line 211
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->volumeId:Ljava/lang/String;

    .line 212
    const-string v2, "background-color"

    invoke-static {p1, p2}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getColorRRGGBB(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->serializeJsonWithField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->highlightedStyle:Ljava/lang/String;

    .line 214
    const-string v2, "note"

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->serializeJsonWithField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->data:Ljava/lang/String;

    .line 215
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getDataId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->dataId:Ljava/lang/String;

    .line 217
    return-object v0
.end method

.method private static colorWithoutAlpha(I)I
    .locals 1
    .param p0, "androidColor"    # I

    .prologue
    .line 44
    const/high16 v0, -0x1000000

    sub-int v0, p0, v0

    return v0
.end method

.method public static contextFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .locals 4
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->beforeSelectedText:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->selectedText:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->afterSelectedText:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createAnnotationFromJsonAnnotation(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 10
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 56
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->positionRangeFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v3

    .line 57
    .local v3, "textRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->imageRangeFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v7

    .line 58
    .local v7, "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->contextFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v4

    .line 59
    .local v4, "context":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getDataId(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "dataId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->type:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getColor(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)I

    move-result v5

    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getMarginNotes(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getTimestamp(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)J

    move-result-wide v8

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/annotations/Annotation;->withFreshId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method private static createJsonCfiRange(Lcom/google/android/apps/books/render/PageStructureLocationRange;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;
    .locals 2
    .param p0, "range"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .prologue
    .line 287
    if-nez p0, :cond_0

    .line 288
    const/4 v0, 0x0

    .line 294
    :goto_0
    return-object v0

    .line 291
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;-><init>()V

    .line 292
    .local v0, "cfiRange":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->toCfiString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;->startPosition:Ljava/lang/String;

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->toCfiString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;->endPosition:Ljava/lang/String;

    goto :goto_0
.end method

.method private static createJsonClientVersionRanges(Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/render/PageStructureLocationRange;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    .locals 2
    .param p0, "positionRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p1, "pageStructureLocationRange"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .param p2, "contentVersion"    # Ljava/lang/String;

    .prologue
    .line 265
    new-instance v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;-><init>()V

    .line 266
    .local v0, "ranges":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    iput-object p2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;->contentVersion:Ljava/lang/String;

    .line 267
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->createJsonTextRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;->gbTextRange:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;

    .line 268
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->createJsonCfiRange(Lcom/google/android/apps/books/render/PageStructureLocationRange;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;->imageCfiRange:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;

    .line 269
    return-object v0
.end method

.method private static createJsonTextRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;
    .locals 2
    .param p0, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;

    .prologue
    .line 273
    if-nez p0, :cond_0

    .line 274
    const/4 v0, 0x0

    .line 283
    :goto_0
    return-object v0

    .line 276
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;-><init>()V

    .line 278
    .local v0, "gbTextRange":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->startPosition:Ljava/lang/String;

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->startOffset:Ljava/lang/String;

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->endPosition:Ljava/lang/String;

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/TextLocation;

    iget v1, v1, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->endOffset:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getBestVersionRange(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    .locals 7
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 170
    const/4 v5, 0x3

    new-array v4, v5, [Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->contentRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->clientVersionRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->currentVersionRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    aput-object v6, v4, v5

    .line 174
    .local v4, "ranges":[Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    move-object v0, v4

    .local v0, "arr$":[Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 175
    .local v3, "range":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    if-eqz v3, :cond_0

    .line 179
    .end local v3    # "range":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    :goto_1
    return-object v3

    .line 174
    .restart local v3    # "range":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .end local v3    # "range":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getColor(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)I
    .locals 7
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    const/4 v3, 0x0

    .line 104
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->highlightedStyle:Ljava/lang/String;

    const-string v5, "background-color"

    invoke-static {v4, v5}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getFieldFromSerializedJsonObject(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "colorString":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v3

    .line 111
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x7

    if-le v4, v5, :cond_2

    .line 112
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x6

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "rrggbb":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 115
    .end local v2    # "rrggbb":Ljava/lang/String;
    :cond_2
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 116
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "JsonAnnotations"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 118
    const-string v4, "JsonAnnotations"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t parse color: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getColorRRGGBB(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)Ljava/lang/String;
    .locals 5
    .param p0, "a"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p1, "palette"    # Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v0

    .line 226
    .local v0, "color":I
    if-nez v0, :cond_0

    .line 227
    const-string v1, "#000000"

    .line 229
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "#%06X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;->getClosestMatch(I)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->colorWithoutAlpha(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getDataId(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Ljava/lang/String;
    .locals 5
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    const/4 v3, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->dataLink:Ljava/lang/String;

    .line 154
    .local v0, "dataLink":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 155
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 156
    .local v1, "dataUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 157
    .local v2, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 166
    .end local v1    # "dataUri":Landroid/net/Uri;
    .end local v2    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v3

    .line 160
    .restart local v1    # "dataUri":Landroid/net/Uri;
    .restart local v2    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_0

    .line 163
    .end local v1    # "dataUri":Landroid/net/Uri;
    .end local v2    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->dataId:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 164
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->dataId:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getFieldFromSerializedJsonObject(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "json"    # Ljava/lang/String;
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 130
    invoke-static {p0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v2, v3

    .line 147
    :cond_0
    :goto_0
    return-object v2

    .line 134
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 135
    .local v1, "jo":Lorg/json/JSONObject;
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object v2, v3

    .line 136
    goto :goto_0

    .line 138
    :cond_2
    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "value":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    move-object v2, v3

    .line 140
    goto :goto_0

    .line 143
    .end local v1    # "jo":Lorg/json/JSONObject;
    .end local v2    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "JsonAnnotations"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 145
    const-string v4, "JsonAnnotations"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t find field "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in JSON: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v2, v3

    .line 147
    goto :goto_0
.end method

.method private static getMarginNotes(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Ljava/lang/String;
    .locals 3
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 125
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->data:Ljava/lang/String;

    const-string v2, "note"

    invoke-static {v1, v2}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getFieldFromSerializedJsonObject(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "field":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v0, ""

    .end local v0    # "field":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private static getPageId(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/String;
    .locals 1
    .param p0, "a"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTimestamp(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)J
    .locals 4
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 326
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 327
    .local v0, "time":Landroid/text/format/Time;
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->updated:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 328
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    return-wide v2
.end method

.method public static imageRangeFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .locals 6
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getBestVersionRange(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    move-result-object v3

    iget-object v1, v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;->imageCfiRange:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;

    .line 88
    .local v1, "imageCfiRange":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;
    if-nez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object v2

    .line 92
    :cond_1
    :try_start_0
    new-instance v3, Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-object v4, v1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;->startPosition:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->parseCfi(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;->endPosition:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->parseCfi(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/books/render/PageStructureLocationRange;-><init>(Lcom/google/android/apps/books/annotations/PageStructureLocation;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;
    const-string v3, "JsonAnnotations"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    const-string v3, "JsonAnnotations"

    const-string v4, "exception parsing image range"

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static includePageIds(Lcom/google/android/apps/books/annotations/Annotation;)Z
    .locals 1
    .param p0, "a"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getTextualContext()Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parseOffset(Ljava/lang/String;)I
    .locals 1
    .param p0, "offset"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-static {p0}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private static positionRangeForJson(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 1
    .param p0, "a"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v0

    return-object v0
.end method

.method public static positionRangeFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 10
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    const/4 v9, 0x0

    .line 66
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getBestVersionRange(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;

    move-result-object v8

    iget-object v3, v8, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;->gbTextRange:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;

    .line 67
    .local v3, "gbTextRange":Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->pageIds:Ljava/util/List;

    .line 68
    .local v4, "pageIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v3, :cond_3

    .line 69
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 70
    :cond_0
    const-string v8, "JsonAnnotations"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 71
    const-string v8, "JsonAnnotations"

    const-string v9, "No text range and no pageIds in JSON annotation"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    const/4 v8, 0x0

    .line 83
    :goto_0
    return-object v8

    .line 75
    :cond_2
    new-instance v5, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v5, v8, v9}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    .line 76
    .local v5, "start":Lcom/google/android/apps/books/annotations/TextLocation;
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v0, v8, v9}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    .line 77
    .local v0, "end":Lcom/google/android/apps/books/annotations/TextLocation;
    new-instance v8, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v8, v5, v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)V

    goto :goto_0

    .line 79
    .end local v0    # "end":Lcom/google/android/apps/books/annotations/TextLocation;
    .end local v5    # "start":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_3
    iget-object v7, v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->startPosition:Ljava/lang/String;

    .line 80
    .local v7, "startPos":Ljava/lang/String;
    iget-object v8, v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->startOffset:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->parseOffset(Ljava/lang/String;)I

    move-result v6

    .line 81
    .local v6, "startOffset":I
    iget-object v2, v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->endPosition:Ljava/lang/String;

    .line 82
    .local v2, "endPos":Ljava/lang/String;
    iget-object v8, v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;->endOffset:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->parseOffset(Ljava/lang/String;)I

    move-result v1

    .line 83
    .local v1, "endOffset":I
    new-instance v8, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v8, v7, v6, v2, v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private static serializeJsonWithField(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 239
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 240
    .local v1, "object":Lorg/json/JSONObject;
    invoke-virtual {v1, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 241
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 244
    .end local v1    # "object":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "JsonAnnotations"

    const-string v3, "Can\'t figure out how to throw an exception on such a simple operation"

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v2, 0x0

    goto :goto_0
.end method
