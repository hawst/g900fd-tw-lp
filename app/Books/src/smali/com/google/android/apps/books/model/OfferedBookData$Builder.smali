.class public Lcom/google/android/apps/books/model/OfferedBookData$Builder;
.super Ljava/lang/Object;
.source "OfferedBookData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/OfferedBookData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAuthor:Ljava/lang/String;

.field private mCanonicalVolumeLink:Ljava/lang/String;

.field private mCoverUri:Landroid/net/Uri;

.field private mDescription:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mAuthor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCoverUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCanonicalVolumeLink:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/OfferedBookData;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/apps/books/model/OfferedBookData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/model/OfferedBookData;-><init>(Lcom/google/android/apps/books/model/OfferedBookData$Builder;Lcom/google/android/apps/books/model/OfferedBookData$1;)V

    return-object v0
.end method

.method public setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mAuthor:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public setCanonicalVolumeLink(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 0
    .param p1, "canonicalVolumeLink"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCanonicalVolumeLink:Ljava/lang/String;

    .line 92
    return-object p0
.end method

.method public setCoverUri(Landroid/net/Uri;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 0
    .param p1, "coverUri"    # Landroid/net/Uri;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCoverUri:Landroid/net/Uri;

    .line 88
    return-object p0
.end method

.method public setCoverUri(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 1
    .param p1, "coverUri"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCoverUri:Landroid/net/Uri;

    .line 84
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mDescription:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mTitle:Ljava/lang/String;

    .line 68
    return-object p0
.end method

.method public setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mVolumeId:Ljava/lang/String;

    .line 80
    return-object p0
.end method
