.class public Lcom/google/android/apps/books/util/ByteArrayUtils;
.super Ljava/lang/Object;
.source "ByteArrayUtils.java"


# direct methods
.method public static bufferIndexOf([BI[B)I
    .locals 4
    .param p0, "input"    # [B
    .param p1, "inputOffset"    # I
    .param p2, "search"    # [B

    .prologue
    .line 97
    array-length v2, p2

    if-nez v2, :cond_0

    .line 106
    .end local p1    # "inputOffset":I
    :goto_0
    return p1

    .line 98
    .restart local p1    # "inputOffset":I
    :cond_0
    move v0, p1

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 99
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    array-length v2, p2

    if-ge v1, v2, :cond_2

    .line 100
    add-int v2, v0, v1

    aget-byte v2, p0, v2

    aget-byte v3, p2, v1

    if-eq v2, v3, :cond_1

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move p1, v0

    .line 104
    goto :goto_0

    .line 106
    .end local v1    # "j":I
    :cond_3
    const/4 p1, -0x1

    goto :goto_0
.end method

.method public static buffersEqual([B[B)Z
    .locals 4
    .param p0, "a"    # [B
    .param p1, "b"    # [B

    .prologue
    const/4 v1, 0x0

    .line 192
    array-length v2, p0

    array-length v3, p1

    if-eq v2, v3, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v1

    .line 193
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 194
    aget-byte v2, p0, v0

    aget-byte v3, p1, v0

    if-ne v2, v3, :cond_0

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 198
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static concatBuffers(Ljava/util/Collection;)[B
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<[B>;)[B"
        }
    .end annotation

    .prologue
    .line 172
    .local p0, "buffers":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    const/4 v2, 0x0

    .line 173
    .local v2, "length":I
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 174
    .local v0, "buffer":[B
    array-length v5, v0

    add-int/2addr v2, v5

    .line 175
    goto :goto_0

    .line 177
    .end local v0    # "buffer":[B
    :cond_0
    new-array v3, v2, [B

    .line 179
    .local v3, "output":[B
    const/4 v4, 0x0

    .line 180
    .local v4, "position":I
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 181
    .restart local v0    # "buffer":[B
    const/4 v5, 0x0

    array-length v6, v0

    invoke-static {v0, v5, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    array-length v5, v0

    add-int/2addr v4, v5

    .line 183
    goto :goto_1

    .line 185
    .end local v0    # "buffer":[B
    :cond_1
    return-object v3
.end method

.method public static varargs concatBuffers([[B)[B
    .locals 9
    .param p0, "buffers"    # [[B

    .prologue
    .line 152
    const/4 v4, 0x0

    .line 153
    .local v4, "length":I
    move-object v0, p0

    .local v0, "arr$":[[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 154
    .local v1, "buffer":[B
    array-length v7, v1

    add-int/2addr v4, v7

    .line 153
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 157
    .end local v1    # "buffer":[B
    :cond_0
    new-array v5, v4, [B

    .line 159
    .local v5, "output":[B
    const/4 v6, 0x0

    .line 160
    .local v6, "position":I
    move-object v0, p0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 161
    .restart local v1    # "buffer":[B
    const/4 v7, 0x0

    array-length v8, v1

    invoke-static {v1, v7, v5, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    array-length v7, v1

    add-int/2addr v6, v7

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 165
    .end local v1    # "buffer":[B
    :cond_1
    return-object v5
.end method

.method public static readIntLittle([B)I
    .locals 2
    .param p0, "buff"    # [B

    .prologue
    .line 61
    const/4 v0, 0x3

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const/4 v1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    const/4 v1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    const/4 v1, 0x0

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static readIntString([B)I
    .locals 1
    .param p0, "buff"    # [B

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static readLongString([B)J
    .locals 2
    .param p0, "buff"    # [B

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static readString([B)Ljava/lang/String;
    .locals 3
    .param p0, "buff"    # [B

    .prologue
    .line 67
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v1, p0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-object v1

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "ByteArrayUtils"

    const-string v2, "Falling back to default string encoding"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public static readString([BI)Ljava/lang/String;
    .locals 5
    .param p0, "buff"    # [B
    .param p1, "length"    # I

    .prologue
    const/4 v4, 0x0

    .line 76
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "UTF-8"

    invoke-direct {v1, p0, v2, p1, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-object v1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "ByteArrayUtils"

    const-string v2, "Falling back to default string encoding"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0, v4, p1}, Ljava/lang/String;-><init>([BII)V

    goto :goto_0
.end method

.method public static splitBufferUsing([B[B)Ljava/util/ArrayList;
    .locals 4
    .param p0, "input"    # [B
    .param p1, "separator"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B[B)",
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    const/4 v2, 0x0

    .line 117
    .local v2, "start":I
    invoke-static {p0, v2, p1}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v1

    .line 118
    .local v1, "offset":I
    :goto_0
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 119
    sub-int v3, v1, v2

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    array-length v3, p1

    add-int v2, v1, v3

    .line 121
    invoke-static {p0, v2, p1}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v1

    goto :goto_0

    .line 123
    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    .line 124
    invoke-static {p0, v2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_1
    :goto_1
    return-object v0

    .line 125
    :cond_2
    array-length v3, p0

    if-ne v2, v3, :cond_1

    .line 126
    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static subBuffer([BI)[B
    .locals 2
    .param p0, "a"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 135
    array-length v1, p0

    sub-int v0, v1, p1

    .line 136
    .local v0, "length":I
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v1

    return-object v1
.end method

.method public static subBuffer([BII)[B
    .locals 2
    .param p0, "a"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 143
    new-array v0, p2, [B

    .line 144
    .local v0, "c":[B
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 145
    return-object v0
.end method

.method public static writeIntLittle(I)[B
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 37
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 38
    .local v0, "buff":[B
    const/4 v1, 0x3

    shr-int/lit8 v2, p0, 0x18

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 39
    const/4 v1, 0x2

    shr-int/lit8 v2, p0, 0x10

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 40
    const/4 v1, 0x1

    shr-int/lit8 v2, p0, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 41
    const/4 v1, 0x0

    int-to-byte v2, p0

    aput-byte v2, v0, v1

    .line 42
    return-object v0
.end method

.method public static writeIntString(I)[B
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 18
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static writeLongString(J)[B
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 22
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static writeString(Ljava/lang/String;)[B
    .locals 3
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 85
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 88
    :goto_0
    return-object v1

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "ByteArrayUtils"

    const-string v2, "Falling back to default string encoding"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    goto :goto_0
.end method
