.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onLoadedRangeData(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$handlesPointsJson:Ljava/lang/String;

.field final synthetic val$requestId:I

.field final synthetic val$textContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

.field final synthetic val$textPointsJson:Ljava/lang/String;

.field final synthetic val$textRange:Lcom/google/android/apps/books/annotations/TextLocationRange;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$requestId:I

    iput-object p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$textPointsJson:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$handlesPointsJson:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$textRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iput-object p6, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$textContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$requestId:I

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$textPointsJson:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$handlesPointsJson:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$textRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v6, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;->val$textContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/render/ReaderListener;->onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V

    .line 300
    return-void
.end method
