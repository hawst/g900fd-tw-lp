.class public Lcom/google/android/apps/books/util/ViewScale;
.super Ljava/lang/Object;
.source "ViewScale.java"


# instance fields
.field private mIsIdentity:Z

.field private mScale:F

.field private mTranslateX:F

.field private mTranslateY:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    .line 12
    iput v1, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateX:F

    .line 13
    iput v1, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateY:F

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/ViewScale;->mIsIdentity:Z

    return-void
.end method


# virtual methods
.method public applyTo(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ViewScale;->isIdentity()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    iget v0, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    iget v1, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    iget v2, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateX:F

    iget v3, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateY:F

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 32
    :cond_0
    return-void
.end method

.method public applyToDirtyRect([ILandroid/graphics/Rect;)V
    .locals 7
    .param p1, "location"    # [I
    .param p2, "dirty"    # Landroid/graphics/Rect;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ViewScale;->isIdentity()Z

    move-result v2

    if-nez v2, :cond_0

    .line 41
    aget v0, p1, v5

    .line 42
    .local v0, "childLeft":I
    aget v1, p1, v6

    .line 45
    .local v1, "childTop":I
    iget v2, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateX:F

    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateX:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, p1, v5

    .line 46
    iget v2, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateY:F

    int-to-float v3, v1

    iget v4, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateY:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, p1, v6

    .line 50
    iget v2, p2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p2, Landroid/graphics/Rect;->left:I

    .line 51
    iget v2, p2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p2, Landroid/graphics/Rect;->right:I

    .line 52
    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p2, Landroid/graphics/Rect;->top:I

    .line 53
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p2, Landroid/graphics/Rect;->bottom:I

    .line 55
    .end local v0    # "childLeft":I
    .end local v1    # "childTop":I
    :cond_0
    return-void
.end method

.method public isIdentity()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/ViewScale;->mIsIdentity:Z

    return v0
.end method

.method public set(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 17
    const v0, 0x3c23d70a    # 0.01f

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 18
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/ViewScale;->mIsIdentity:Z

    .line 26
    :goto_0
    return-void

    .line 21
    :cond_0
    iput p1, p0, Lcom/google/android/apps/books/util/ViewScale;->mScale:F

    .line 22
    iput p2, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateX:F

    .line 23
    iput p3, p0, Lcom/google/android/apps/books/util/ViewScale;->mTranslateY:F

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/ViewScale;->mIsIdentity:Z

    goto :goto_0
.end method
