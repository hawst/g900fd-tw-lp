.class public Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;
.super Ljava/lang/Object;
.source "LibraryFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LibraryFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LibraryFilterSource"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mListener:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;

.field private final mPossibleFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/LibraryFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mPossibleFilters:Ljava/util/List;

    .line 113
    iput-object p1, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mContext:Landroid/content/Context;

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->initializeToEmptyLibrary()V

    .line 115
    return-void
.end method

.method private initializeToEmptyLibrary()V
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->onCardDataLoaded(Ljava/util/List;)V

    .line 123
    return-void
.end method


# virtual methods
.method public getPossibleFilters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/LibraryFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mPossibleFilters:Ljava/util/List;

    return-object v0
.end method

.method public onCardDataLoaded(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "volumesData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_3

    .line 131
    iget-object v4, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mPossibleFilters:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 134
    invoke-static {}, Lcom/google/android/apps/books/app/LibraryFilter;->values()[Lcom/google/android/apps/books/app/LibraryFilter;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/app/LibraryFilter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 135
    .local v1, "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    iget-object v4, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/app/LibraryFilter;->shouldShowWhenEmpty(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    # invokes: Lcom/google/android/apps/books/app/LibraryFilter;->isNonEmpty(Ljava/util/List;)Z
    invoke-static {v1, p1}, Lcom/google/android/apps/books/app/LibraryFilter;->access$100(Lcom/google/android/apps/books/app/LibraryFilter;Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 136
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mPossibleFilters:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 141
    .end local v1    # "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mListener:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;

    if-eqz v4, :cond_3

    .line 142
    iget-object v4, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mListener:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;->onUpdateFilters()V

    .line 145
    .end local v0    # "arr$":[Lcom/google/android/apps/books/app/LibraryFilter;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_3
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->mListener:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;

    .line 119
    return-void
.end method
