.class Lcom/google/android/apps/books/app/HomeFragment$23;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 1888
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismissedRecommendations(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x1

    .line 1903
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v0, v0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->onDismissedRecommendations(Ljava/util/Set;)V

    .line 1904
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1900(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Eventual;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mLoadedDismissedRecs:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3200(Lcom/google/android/apps/books/app/HomeFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1905
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mLoadedDismissedRecs:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3202(Lcom/google/android/apps/books/app/HomeFragment;Z)Z

    .line 1907
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1900(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Eventual;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->maybeSetupRecommendationFooter(Ljava/util/List;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$2200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/List;)V

    .line 1911
    :goto_0
    return-void

    .line 1909
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mLoadedDismissedRecs:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3202(Lcom/google/android/apps/books/app/HomeFragment;Z)Z

    goto :goto_0
.end method

.method public onLocalVolumeData(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1891
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v0, v0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->onLocalVolumeData(Ljava/util/Map;)V

    .line 1892
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->sendLocalVolumeDataToViews(Ljava/util/Map;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3000(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/Map;)V

    .line 1893
    return-void
.end method

.method public onNewUpload(Lcom/google/android/apps/books/upload/Upload;)V
    .locals 2
    .param p1, "upload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    .line 1926
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v1, p1}, Lcom/google/android/apps/books/upload/Upload;-><init>(Lcom/google/android/apps/books/upload/Upload;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->put(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1927
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->setUploadsDataInCardsData()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3400(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1928
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3500(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1930
    :cond_0
    return-void
.end method

.method public onUploadDeleted(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1952
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->setUploadsDataInCardsData()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3400(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1954
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3500(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1955
    return-void
.end method

.method public onUploadStatusUpdate(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 1934
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/Upload;

    .line 1935
    .local v0, "upload":Lcom/google/android/apps/books/upload/Upload;
    if-eqz v0, :cond_0

    .line 1936
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/upload/Upload;->setUploadStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 1938
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->setUploadsDataInCardsData()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3400(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1939
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3500(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1940
    return-void
.end method

.method public onUploadTooLarge()V
    .locals 7

    .prologue
    .line 1959
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getMaxUploadSizeMB(Landroid/content/Context;)I

    move-result v0

    .line 1960
    .local v0, "maxSizeMB":I
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1961
    .local v1, "res":Landroid/content/res/Resources;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f01e5

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1964
    return-void
.end method

.method public onUploadVolumeIdUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uploadId"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1944
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/Upload;

    .line 1945
    .local v0, "upload":Lcom/google/android/apps/books/upload/Upload;
    if-eqz v0, :cond_0

    .line 1946
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/upload/Upload;->setVolumeId(Ljava/lang/String;)V

    .line 1948
    :cond_0
    return-void
.end method

.method public onUploads(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
    .locals 1
    .param p1, "uploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 1915
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/upload/Upload$Uploads;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1922
    :goto_0
    return-void

    .line 1918
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload$Uploads;->clear()V

    .line 1919
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->copyAll(Lcom/google/android/apps/books/upload/Upload$Uploads;)V

    .line 1920
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->setUploadsDataInCardsData()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3400(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 1921
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$3500(Lcom/google/android/apps/books/app/HomeFragment;)V

    goto :goto_0
.end method

.method public onVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1897
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v0, v0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->onVolumeDownloadProgress(Ljava/util/Map;)V

    .line 1898
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$23;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->sendDownloadFractionsToViews(Ljava/util/Map;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3100(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/Map;)V

    .line 1899
    return-void
.end method
