.class public abstract Lcom/google/android/apps/books/app/BooksApplication;
.super Landroid/app/Application;
.source "BooksApplication.java"

# interfaces
.implements Lcom/google/android/apps/books/common/BooksContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;
    }
.end annotation


# static fields
.field private static sEnableHeapDump:Z

.field private static sHasSetDefaultUncaughtExceptionHandler:Z

.field private static sHprofDumped:Z

.field private static sMainThreadId:I

.field private static sRandom:Ljava/util/Random;


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

.field private final mAnnotationControllers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final mAnnotationServers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/annotations/AnnotationServer;",
            ">;"
        }
    .end annotation
.end field

.field private mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClientImpl;

.field private final mBackgroundDataControllers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ">;"
        }
    .end annotation
.end field

.field private mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

.field private mClientConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

.field private mConfig:Lcom/google/android/apps/books/util/Config;

.field private mCookieStore:Lorg/apache/http/client/CookieStore;

.field private mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

.field final mDataExecutor:Ljava/util/concurrent/Executor;

.field private final mDataStores:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/model/BooksDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeveloperKeySupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEventualIsSchoolOwned:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

.field private final mForegroundDataControllers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/data/ForegroundBooksDataController;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private mImageManager:Lcom/google/android/apps/books/app/BooksImageManager;

.field private final mKeyFactories:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/util/SessionKeyFactory;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

.field private mLogger:Lcom/google/android/apps/books/util/Logger;

.field final mNetworkExecutor:Ljava/util/concurrent/Executor;

.field private mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

.field private mReadNowCoverStore:Lcom/google/android/apps/books/model/RemoteFileCache;

.field private mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

.field private final mServers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/net/BooksServer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncControllers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/sync/SyncController;",
            ">;"
        }
    .end annotation
.end field

.field private mTransitionBitmap:Landroid/graphics/Bitmap;

.field private mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

.field private mUtilityHandler:Landroid/os/Handler;

.field private mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

.field private final mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 202
    sput-boolean v0, Lcom/google/android/apps/books/app/BooksApplication;->sEnableHeapDump:Z

    .line 204
    sput v0, Lcom/google/android/apps/books/app/BooksApplication;->sMainThreadId:I

    .line 205
    sput-boolean v0, Lcom/google/android/apps/books/app/BooksApplication;->sHasSetDefaultUncaughtExceptionHandler:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 142
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 188
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mKeyFactories:Ljava/util/Map;

    .line 192
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationServers:Ljava/util/Map;

    .line 195
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationControllers:Ljava/util/Map;

    .line 209
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mForegroundDataControllers:Ljava/util/Map;

    .line 212
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mBackgroundDataControllers:Ljava/util/Map;

    .line 214
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mServers:Ljava/util/Map;

    .line 216
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataStores:Ljava/util/Map;

    .line 220
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mSyncControllers:Ljava/util/Map;

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mReadNowCoverStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    .line 453
    new-instance v0, Lcom/google/android/apps/books/app/BooksApplication$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/BooksApplication$2;-><init>(Lcom/google/android/apps/books/app/BooksApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDeveloperKeySupplier:Lcom/google/common/base/Supplier;

    .line 589
    new-instance v0, Lcom/google/android/apps/books/app/BooksApplication$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/BooksApplication$3;-><init>(Lcom/google/android/apps/books/app/BooksApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    .line 710
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataExecutor:Ljava/util/concurrent/Executor;

    .line 718
    const-string v0, "BackgroundData"

    const/4 v1, 0x6

    const/4 v2, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/ExecutorUtils;->createThreadPoolExecutor(Ljava/lang/String;IILjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mNetworkExecutor:Ljava/util/concurrent/Executor;

    .line 836
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 142
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksApplication;->sEnableHeapDump:Z

    return v0
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 142
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->report(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/BooksApplication;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksApplication;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mEventualIsSchoolOwned:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method private buildDeviceInfo()Lcom/google/android/apps/books/app/DeviceInfo;
    .locals 3

    .prologue
    .line 965
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 966
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    invoke-direct {v1}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;-><init>()V

    const-string v2, "android_id"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setAndroidId(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setSerialNumber(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setModel(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setManufacturer(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setDeviceName(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setProductName(Ljava/lang/String;)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_OFFERS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->setUseFakeOffersData(Z)Lcom/google/android/apps/books/app/DeviceInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/DeviceInfo$Builder;->build()Lcom/google/android/apps/books/app/DeviceInfo;

    move-result-object v1

    return-object v1
.end method

.method public static buildExternalHomeIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 666
    const-string v0, "https://play.google.com/books#ReadNow"

    .line 667
    .local v0, "homeUri":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "https://play.google.com/books#ReadNow"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 668
    .local v1, "result":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 669
    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 670
    const-string v2, "books:internalIntent"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 671
    const v2, 0x1000c000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 673
    return-object v1
.end method

.method public static buildExternalOffersIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 680
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 681
    .local v0, "result":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 682
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/books/app/OffersActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 683
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 684
    const-string v1, "fromWidget"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 685
    return-object v0
.end method

.method public static buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 652
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/app/BooksApplication;->buildInternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 655
    .local v0, "result":Landroid/content/Intent;
    const v1, 0x1000c000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 658
    return-object v0
.end method

.method public static buildInternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 638
    const-string v2, "https://play.google.com/books/reader"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 640
    .local v1, "uri":Landroid/net/Uri$Builder;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 641
    .local v0, "result":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 642
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 643
    const-string v2, "books:internalIntent"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 644
    return-object v0
.end method

.method private declared-synchronized getAccountManager()Lcom/google/android/apps/books/net/BooksAccountManager;
    .locals 2

    .prologue
    .line 863
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

    if-nez v0, :cond_0

    .line 864
    new-instance v0, Lcom/google/android/apps/books/net/BooksAccountManagerImpl;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/net/BooksAccountManagerImpl;-><init>(Landroid/accounts/AccountManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

    .line 866
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 863
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static getApiaryClient(Landroid/content/Context;)Lcom/google/android/apps/books/api/ApiaryClient;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 405
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getApiaryClient()Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v0

    return-object v0
.end method

.method public static getBackgroundDataController(Landroid/content/Context;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 768
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    return-object v0
.end method

.method public static getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 797
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    return-object v0
.end method

.method public static getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 482
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    return-object v0
.end method

.method public static getChangeBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 514
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster()Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v0

    return-object v0
.end method

.method public static getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 447
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    return-object v0
.end method

.method public static getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 706
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceInfo()Lcom/google/android/apps/books/app/DeviceInfo;
    .locals 1

    .prologue
    .line 993
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksApplication;->buildDeviceInfo()Lcom/google/android/apps/books/app/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getFileStorageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/FileStorageManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 436
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getFileStorageManager()Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v0

    return-object v0
.end method

.method public static getForegroundDataController(Landroid/content/Context;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 761
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v0

    return-object v0
.end method

.method public static getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 753
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 7

    .prologue
    .line 325
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    :goto_0
    monitor-exit p0

    return-object v3

    .line 327
    :cond_0
    :try_start_1
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 329
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    const/16 v3, 0x4e20

    invoke-static {v1, v3}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 330
    const/16 v3, 0x4e20

    invoke-static {v1, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 331
    const/16 v3, 0x2000

    invoke-static {v1, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 333
    const/4 v3, 0x5

    invoke-static {v1, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 335
    invoke-static {p0}, Lcom/google/android/apps/books/net/HttpUtils;->buildUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "agent":Ljava/lang/String;
    invoke-static {v1, v0}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 338
    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 339
    .local v2, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v5

    const/16 v6, 0x50

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 341
    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v5

    const/16 v6, 0x1bb

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 344
    new-instance v3, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v3, v1, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mClientConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

    .line 349
    new-instance v3, Lcom/google/android/apps/books/app/BooksApplication$1;

    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mClientConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

    invoke-direct {v3, p0, v4, v1}, Lcom/google/android/apps/books/app/BooksApplication$1;-><init>(Lcom/google/android/apps/books/app/BooksApplication;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 358
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v4, Lcom/google/android/apps/books/net/CurlInterceptor;

    invoke-direct {v4}, Lcom/google/android/apps/books/net/CurlInterceptor;-><init>()V

    invoke-virtual {v3, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 359
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v4, Lcom/google/android/apps/books/net/GoogleRedirectHandler;

    invoke-direct {v4}, Lcom/google/android/apps/books/net/GoogleRedirectHandler;-><init>()V

    invoke-virtual {v3, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 361
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 325
    .end local v0    # "agent":Ljava/lang/String;
    .end local v1    # "params":Lorg/apache/http/params/HttpParams;
    .end local v2    # "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public static getLogger(Landroid/content/Context;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 626
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getLogger()Lcom/google/android/apps/books/util/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static getMainThreadId()I
    .locals 1

    .prologue
    .line 475
    sget v0, Lcom/google/android/apps/books/app/BooksApplication;->sMainThreadId:I

    return v0
.end method

.method private getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    if-nez v0, :cond_0

    .line 1020
    new-instance v0, Lcom/google/android/play/utils/PlayCommonNetworkStack;

    new-instance v1, Lcom/google/android/apps/books/app/BooksApplication$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/BooksApplication$6;-><init>(Lcom/google/android/apps/books/app/BooksApplication;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/utils/PlayCommonNetworkStack;-><init>(Landroid/content/Context;Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    .line 1031
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mPlayCommonNetworkStack:Lcom/google/android/play/utils/PlayCommonNetworkStack;

    return-object v0
.end method

.method public static getPlayCommonNetworkStack(Landroid/content/Context;)Lcom/google/android/play/utils/PlayCommonNetworkStack;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1015
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getRandom()Ljava/util/Random;
    .locals 2

    .prologue
    .line 889
    const-class v1, Lcom/google/android/apps/books/app/BooksApplication;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksApplication;->sRandom:Ljava/util/Random;

    if-nez v0, :cond_0

    .line 891
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/BooksApplication;->sRandom:Ljava/util/Random;

    .line 893
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksApplication;->sRandom:Ljava/util/Random;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 889
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getResponseGetter(Landroid/content/Context;)Lcom/google/android/apps/books/net/ResponseGetter;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 390
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v0

    return-object v0
.end method

.method public static getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 821
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v0

    return-object v0
.end method

.method public static getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 870
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v0

    return-object v0
.end method

.method public static getVolumeUsageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/VolumeUsageManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 586
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksApplication;

    iget-object v0, v0, Lcom/google/android/apps/books/app/BooksApplication;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    return-object v0
.end method

.method public static isAccessibilityEnabled(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 489
    if-eqz p0, :cond_0

    .line 490
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->isAccessibilityEnabled_()Z

    move-result v0

    .line 492
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isScreenReaderActive(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 499
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->isScreenReaderActive_()Z

    move-result v0

    return v0
.end method

.method private static declared-synchronized report(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 566
    const-class v2, Lcom/google/android/apps/books/app/BooksApplication;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/google/android/apps/books/app/BooksApplication;->sHprofDumped:Z

    if-nez v1, :cond_0

    .line 567
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/books/app/BooksApplication;->sHprofDumped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EXTERNAL_STORAGE"

    invoke-static {v3}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/books.hprof"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 572
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 573
    const-string v1, "BooksApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Out of memory.\nPlease do the following to copy the heap dump to your computer:\n\n  adb pull "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "And attach the file to your bug report."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580
    .end local v0    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 566
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 577
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static reportAndRethrow(Landroid/content/Context;Ljava/lang/OutOfMemoryError;II)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "e"    # Ljava/lang/OutOfMemoryError;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 551
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksApplication;->sEnableHeapDump:Z

    if-eqz v0, :cond_0

    .line 552
    const-string v0, "BooksApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Out of memory allocating a ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") sized texture."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->report(Landroid/content/Context;)V

    .line 555
    :cond_0
    throw p1
.end method

.method public static restart(Landroid/app/Activity;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 829
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 831
    .local v0, "restart":Landroid/content/Intent;
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 832
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 833
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 834
    return-void
.end method

.method private setupBind()V
    .locals 3

    .prologue
    .line 1035
    invoke-static {p0}, Lcom/google/android/libraries/bind/util/Util;->init(Landroid/content/Context;)V

    .line 1038
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f040072

    aput v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->init([I)V

    .line 1041
    return-void
.end method

.method public static withIsSchoolOwned(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 939
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->withIsSchoolOwned(Lcom/google/android/ublib/utils/Consumer;)V

    .line 940
    return-void
.end method


# virtual methods
.method protected applyPrngFixes()V
    .locals 0

    .prologue
    .line 883
    invoke-static {}, Lcom/google/android/ublib/util/PRNGFixes;->apply()V

    .line 884
    return-void
.end method

.method public declared-synchronized clearImageCache()V
    .locals 1

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mImageManager:Lcom/google/android/apps/books/app/BooksImageManager;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mImageManager:Lcom/google/android/apps/books/app/BooksImageManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksImageManager;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    :cond_0
    monitor-exit p0

    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public consumeTransitionBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTransitionBitmap:Landroid/graphics/Bitmap;

    .line 923
    .local v0, "bitmapToReturn":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTransitionBitmap:Landroid/graphics/Bitmap;

    .line 924
    return-object v0
.end method

.method public declared-synchronized getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 536
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationControllers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationControllers:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    new-instance v2, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;-><init>(Landroid/content/Context;Landroid/accounts/Account;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationServer(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationServer;

    move-result-object v3

    invoke-direct {v1, p0, p1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;Lcom/google/android/apps/books/annotations/AnnotationServer;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationControllers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 536
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAnnotationServer(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationServer;
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 525
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationServers:Ljava/util/Map;

    monitor-enter v1

    .line 526
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationServers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationServers:Ljava/util/Map;

    new-instance v2, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getApiaryClient()Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v4

    invoke-direct {v2, p1, v3, v4}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/api/ApiaryClient;Lcom/google/android/apps/books/util/Config;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAnnotationServers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationServer;

    monitor-exit v1

    return-object v0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized getApiaryClient()Lcom/google/android/apps/books/api/ApiaryClient;
    .locals 6

    .prologue
    .line 395
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClientImpl;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClientImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    :goto_0
    monitor-exit p0

    return-object v2

    .line 396
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;

    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;-><init>(Landroid/net/ConnectivityManager;)V

    .line 398
    .local v0, "connectivityManager":Lcom/google/android/apps/books/net/BooksConnectivityManager;
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getAccountManager()Lcom/google/android/apps/books/net/BooksAccountManager;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/books/net/HttpHelper;-><init>(Lcom/google/android/apps/books/net/BooksConnectivityManager;Lcom/google/android/apps/books/net/BooksAccountManager;)V

    .line 399
    .local v1, "httpHelper":Lcom/google/android/apps/books/net/HttpHelper;
    new-instance v2, Lcom/google/android/apps/books/api/ApiaryClientImpl;

    invoke-static {p0}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getDeveloperKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/Config;->getSourceParam()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/apps/books/net/HttpUtils;->buildUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClientImpl;-><init>(Lcom/google/android/apps/books/net/HttpHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClientImpl;

    .line 401
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClientImpl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395
    .end local v0    # "connectivityManager":Lcom/google/android/apps/books/net/BooksConnectivityManager;
    .end local v1    # "httpHelper":Lcom/google/android/apps/books/net/HttpHelper;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 19
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 776
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/BooksApplication;->mBackgroundDataControllers:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/apps/books/data/BooksDataController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 777
    .local v18, "cached":Lcom/google/android/apps/books/data/BooksDataController;
    if-eqz v18, :cond_0

    .line 789
    .end local v18    # "cached":Lcom/google/android/apps/books/data/BooksDataController;
    :goto_0
    monitor-exit p0

    return-object v18

    .line 781
    .restart local v18    # "cached":Lcom/google/android/apps/books/data/BooksDataController;
    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/BooksApplication;->mDataExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/BooksApplication;->mNetworkExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x3

    const/4 v5, 0x6

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v6

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster()Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v8

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v9

    new-instance v11, Lcom/google/android/apps/books/data/SessionKeySubcontroller$ProductionKeyValidator;

    invoke-direct {v11}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$ProductionKeyValidator;-><init>()V

    new-instance v12, Lcom/google/android/apps/books/data/ProductionEncryptionScheme;

    invoke-direct {v12}, Lcom/google/android/apps/books/data/ProductionEncryptionScheme;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->fromContext(Landroid/content/Context;)Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/BooksApplication;->getLogger()Lcom/google/android/apps/books/util/Logger;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/BooksApplication;->getDeviceInfo()Lcom/google/android/apps/books/app/DeviceInfo;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/BooksApplication;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/BooksApplication;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    move-object/from16 v17, v0

    move-object/from16 v10, p1

    invoke-direct/range {v1 .. v17}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;IILcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;Lcom/google/android/apps/books/net/BooksServer;Landroid/accounts/Account;Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/app/DeviceInfo;Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/DataControllerStore;)V

    .line 788
    .local v1, "newInstance":Lcom/google/android/apps/books/data/BooksDataController;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/BooksApplication;->mBackgroundDataControllers:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v18, v1

    .line 789
    goto :goto_0

    .line 776
    .end local v1    # "newInstance":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v18    # "cached":Lcom/google/android/apps/books/data/BooksDataController;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getChangeBroadcaster()Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    .locals 1

    .prologue
    .line 518
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    if-nez v0, :cond_0

    .line 519
    new-instance v0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 518
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConfig()Lcom/google/android/apps/books/util/Config;
    .locals 1

    .prologue
    .line 440
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mConfig:Lcom/google/android/apps/books/util/Config;

    if-nez v0, :cond_0

    .line 441
    invoke-static {p0}, Lcom/google/android/apps/books/util/Config;->buildFrom(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mConfig:Lcom/google/android/apps/books/util/Config;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCookieStore()Lorg/apache/http/client/CookieStore;
    .locals 4

    .prologue
    .line 372
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mCookieStore:Lorg/apache/http/client/CookieStore;

    if-nez v0, :cond_0

    .line 373
    new-instance v0, Lcom/google/android/apps/books/net/BooksCookieStore;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "CookieStore"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/net/BooksCookieStore;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mCookieStore:Lorg/apache/http/client/CookieStore;

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mCookieStore:Lorg/apache/http/client/CookieStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 372
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 14
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 692
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataStores:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/model/BooksDataStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 693
    .local v13, "cached":Lcom/google/android/apps/books/model/BooksDataStore;
    if-eqz v13, :cond_0

    .line 702
    .end local v13    # "cached":Lcom/google/android/apps/books/model/BooksDataStore;
    :goto_0
    monitor-exit p0

    return-object v13

    .line 697
    .restart local v13    # "cached":Lcom/google/android/apps/books/model/BooksDataStore;
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster()Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getVolumeUsageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/VolumeUsageManager;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getFileStorageManager()Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v6

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/books/model/Clock;->SYSTEM_CLOCK:Lcom/google/android/apps/books/model/Clock;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getLogger()Lcom/google/android/apps/books/util/Logger;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/BooksApplication;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    move-object v2, p1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;-><init>(Landroid/content/ContentResolver;Landroid/accounts/Account;Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;Lcom/google/android/apps/books/common/VolumeUsageManager;Lcom/google/android/apps/books/sync/SyncAccountsState;Lcom/google/android/apps/books/common/FileStorageManager;Lcom/google/android/apps/books/sync/SyncController;Lcom/google/android/apps/books/model/Clock;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/provider/VolumeContentStore;Landroid/content/res/AssetManager;Lcom/google/android/apps/books/model/DataControllerStore;)V

    .line 701
    .local v0, "result":Lcom/google/android/apps/books/model/BooksDataStore;
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataStores:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v13, v0

    .line 702
    goto :goto_0

    .line 692
    .end local v0    # "result":Lcom/google/android/apps/books/model/BooksDataStore;
    .end local v13    # "cached":Lcom/google/android/apps/books/model/BooksDataStore;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getFileStorageManager()Lcom/google/android/apps/books/common/FileStorageManager;
    .locals 1

    .prologue
    .line 432
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getForegroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    .locals 13
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 725
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mForegroundDataControllers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 726
    .local v9, "cached":Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    if-eqz v9, :cond_0

    .line 745
    .end local v9    # "cached":Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    :goto_0
    monitor-exit p0

    return-object v9

    .line 732
    .restart local v9    # "cached":Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getFilesDir()Ljava/io/File;

    move-result-object v10

    .line 733
    .local v10, "filesDir":Ljava/io/File;
    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10, v1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    .line 734
    .local v8, "accountDir":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    const-string v1, "uploads.proto"

    invoke-direct {v11, v8, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 735
    .local v11, "uploadsListDataFile":Ljava/io/File;
    new-instance v12, Ljava/io/File;

    const-string v1, "temp_upload_files"

    invoke-direct {v12, v8, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 736
    .local v12, "uploadsTempFilesDirectory":Ljava/io/File;
    new-instance v3, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;

    invoke-direct {v3, v11, v12, p0}, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;-><init>(Ljava/io/File;Ljava/io/File;Landroid/content/Context;)V

    .line 740
    .local v3, "uploadsStore":Lcom/google/android/apps/books/upload/UploadDataStorage;
    new-instance v0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataExecutor:Ljava/util/concurrent/Executor;

    sget-object v4, Lcom/google/android/apps/books/model/Clock;->MONOTONIC_CLOCK:Lcom/google/android/apps/books/model/Clock;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v5

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/books/upload/UploadManagerImpl;

    invoke-direct {v7, p0, p1}, Lcom/google/android/apps/books/upload/UploadManagerImpl;-><init>(Landroid/content/Context;Landroid/accounts/Account;)V

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/upload/UploadDataStorage;Lcom/google/android/apps/books/model/Clock;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/upload/UploadManager;)V

    .line 744
    .local v0, "newInstance":Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mForegroundDataControllers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v9, v0

    .line 745
    goto :goto_0

    .line 725
    .end local v0    # "newInstance":Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    .end local v3    # "uploadsStore":Lcom/google/android/apps/books/upload/UploadDataStorage;
    .end local v8    # "accountDir":Ljava/io/File;
    .end local v9    # "cached":Lcom/google/android/apps/books/data/ForegroundBooksDataController;
    .end local v10    # "filesDir":Ljava/io/File;
    .end local v11    # "uploadsListDataFile":Ljava/io/File;
    .end local v12    # "uploadsTempFilesDirectory":Ljava/io/File;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getImageManager()Lcom/google/android/apps/books/common/ImageManager;
    .locals 3

    .prologue
    .line 425
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mImageManager:Lcom/google/android/apps/books/app/BooksImageManager;

    if-nez v0, :cond_0

    .line 426
    new-instance v0, Lcom/google/android/apps/books/app/BooksImageManager;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/app/BooksImageManager;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/apps/books/net/ResponseGetter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mImageManager:Lcom/google/android/apps/books/app/BooksImageManager;

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mImageManager:Lcom/google/android/apps/books/app/BooksImageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 425
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLogger()Lcom/google/android/apps/books/util/Logger;
    .locals 1

    .prologue
    .line 619
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mLogger:Lcom/google/android/apps/books/util/Logger;

    if-nez v0, :cond_0

    .line 620
    new-instance v0, Lcom/google/android/apps/books/util/ProductionLogger;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/ProductionLogger;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mLogger:Lcom/google/android/apps/books/util/Logger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 619
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getReadNowCoverStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/RemoteFileCache;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 897
    const/high16 v0, 0x100000

    .line 899
    .local v0, "CACHE_MAX_FILESIZE_BYTES":I
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mReadNowCoverStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    if-nez v2, :cond_0

    .line 901
    :try_start_0
    new-instance v2, Lcom/google/android/apps/books/model/RemoteFileCache;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getRecommendationsCacheDir(Z)Ljava/io/File;

    move-result-object v3

    const-string v4, "read_now_rec_covers_cache"

    const/high16 v5, 0x100000

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/model/RemoteFileCache;-><init>(Ljava/io/File;Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mReadNowCoverStore:Lcom/google/android/apps/books/model/RemoteFileCache;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 911
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mReadNowCoverStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    return-object v2

    .line 905
    :catch_0
    move-exception v1

    .line 906
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "BooksApp"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 907
    const-string v2, "BooksApp"

    const-string v3, "Could not create ReadNowCoverStore"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public declared-synchronized getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;
    .locals 5

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    :goto_0
    monitor-exit p0

    return-object v2

    .line 381
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v1

    .line 382
    .local v1, "httpClient":Lorg/apache/http/client/HttpClient;
    new-instance v0, Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;

    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;-><init>(Landroid/net/ConnectivityManager;)V

    .line 384
    .local v0, "connectivityManager":Lcom/google/android/apps/books/net/BooksConnectivityManager;
    new-instance v2, Lcom/google/android/apps/books/net/BooksResponseGetter;

    new-instance v3, Lcom/google/android/apps/books/net/HttpHelper;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getAccountManager()Lcom/google/android/apps/books/net/BooksAccountManager;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/books/net/HttpHelper;-><init>(Lcom/google/android/apps/books/net/BooksConnectivityManager;Lcom/google/android/apps/books/net/BooksAccountManager;)V

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, Lcom/google/android/apps/books/net/BooksResponseGetter;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/books/net/HttpHelper;Lcom/google/android/apps/books/util/Config;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    .line 386
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 380
    .end local v0    # "connectivityManager":Lcom/google/android/apps/books/net/BooksConnectivityManager;
    .end local v1    # "httpClient":Lorg/apache/http/client/HttpClient;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getServer(Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 804
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mServers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/net/BooksServer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 805
    .local v8, "cached":Lcom/google/android/apps/books/net/BooksServer;
    if-eqz v8, :cond_0

    .line 814
    .end local v8    # "cached":Lcom/google/android/apps/books/net/BooksServer;
    :goto_0
    monitor-exit p0

    return-object v8

    .line 808
    .restart local v8    # "cached":Lcom/google/android/apps/books/net/BooksServer;
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/books/net/NetworkBooksServer;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getApiaryClient()Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getLogger()Lcom/google/android/apps/books/util/Logger;

    move-result-object v5

    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->getRandom()Ljava/util/Random;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->fromContext(Landroid/content/Context;)Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    move-result-object v7

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/net/NetworkBooksServer;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/api/ApiaryClient;Lcom/google/android/apps/books/net/ResponseGetter;Lcom/google/android/apps/books/util/Logger;Ljava/util/Random;Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)V

    .line 810
    .local v0, "server":Lcom/google/android/apps/books/net/BooksServer;
    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_PROXY_SERVER:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 811
    invoke-static {v0, p0}, Lcom/google/android/apps/books/app/NastyTestingProxyServer;->wrap(Lcom/google/android/apps/books/net/BooksServer;Landroid/content/Context;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v0

    .line 813
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mServers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v8, v0

    .line 814
    goto :goto_0

    .line 804
    .end local v0    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v8    # "cached":Lcom/google/android/apps/books/net/BooksServer;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getSyncController(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 874
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mSyncControllers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/sync/SyncController;

    .line 875
    .local v0, "result":Lcom/google/android/apps/books/sync/SyncController;
    if-nez v0, :cond_0

    .line 876
    new-instance v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;

    .end local v0    # "result":Lcom/google/android/apps/books/sync/SyncController;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/books/sync/SyncControllerImpl;-><init>(Landroid/accounts/Account;Landroid/content/ContentResolver;Ljava/util/concurrent/Executor;)V

    .line 877
    .restart local v0    # "result":Lcom/google/android/apps/books/sync/SyncController;
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mSyncControllers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 879
    :cond_0
    monitor-exit p0

    return-object v0

    .line 874
    .end local v0    # "result":Lcom/google/android/apps/books/sync/SyncController;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public abstract getSyncUi()Lcom/google/android/apps/books/service/SyncService$SyncUi;
.end method

.method public getTranslationServerController()Lcom/google/android/apps/books/api/TranslationServerController;
    .locals 4

    .prologue
    .line 461
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

    if-nez v3, :cond_0

    .line 462
    new-instance v1, Lcom/google/android/apps/books/api/ApiaryTranslationServer;

    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDeveloperKeySupplier:Lcom/google/common/base/Supplier;

    invoke-direct {v1, v3}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;-><init>(Lcom/google/common/base/Supplier;)V

    .line 463
    .local v1, "server":Lcom/google/android/apps/books/api/TranslationServer;
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v2

    .line 464
    .local v2, "uiThreadExecutor":Ljava/util/concurrent/Executor;
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 465
    .local v0, "backgroundExecutor":Ljava/util/concurrent/Executor;
    new-instance v3, Lcom/google/android/apps/books/api/TranslationServerController;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/apps/books/api/TranslationServerController;-><init>(Lcom/google/android/apps/books/api/TranslationServer;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

    .line 468
    .end local v0    # "backgroundExecutor":Ljava/util/concurrent/Executor;
    .end local v1    # "server":Lcom/google/android/apps/books/api/TranslationServer;
    .end local v2    # "uiThreadExecutor":Ljava/util/concurrent/Executor;
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

    return-object v3
.end method

.method public getUtilityHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mUtilityHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public hasTransitionBitmap()Z
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTransitionBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initializeAnalytics()V
    .locals 0

    .prologue
    .line 299
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->initialize(Landroid/content/Context;)V

    .line 300
    return-void
.end method

.method protected isAccessibilityEnabled_()Z
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected isScreenReaderActive_()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 507
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v1

    .line 509
    .local v1, "screenReaders":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 510
    .local v0, "hasScreenReader":Z
    :goto_0
    return v0

    .line 509
    .end local v0    # "hasScreenReader":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 242
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 244
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksApplication;->setupBind()V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->applyPrngFixes()V

    .line 251
    const-string v4, "books:show_testing_ui"

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Lcom/google/android/apps/books/util/GservicesHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lcom/google/android/apps/books/app/BooksApplication;->sEnableHeapDump:Z

    .line 254
    sget-boolean v4, Lcom/google/android/apps/books/app/BooksApplication;->sEnableHeapDump:Z

    if-eqz v4, :cond_0

    sget-boolean v4, Lcom/google/android/apps/books/app/BooksApplication;->sHasSetDefaultUncaughtExceptionHandler:Z

    if-nez v4, :cond_0

    .line 255
    const/4 v4, 0x1

    sput-boolean v4, Lcom/google/android/apps/books/app/BooksApplication;->sHasSetDefaultUncaughtExceptionHandler:Z

    .line 256
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    .line 257
    .local v2, "oldHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    new-instance v4, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;

    invoke-direct {v4, p0, v2}, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;-><init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v4}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 261
    .end local v2    # "oldHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->initializeAnalytics()V

    .line 263
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v4

    sput v4, Lcom/google/android/apps/books/app/BooksApplication;->sMainThreadId:I

    .line 264
    new-instance v4, Lcom/google/android/apps/books/common/FileStorageManagerImpl;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/common/FileStorageManagerImpl;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    .line 266
    new-instance v1, Landroid/os/HandlerThread;

    const-string v4, "Books-utility"

    invoke-direct {v1, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 267
    .local v1, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 268
    new-instance v4, Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mUtilityHandler:Landroid/os/Handler;

    .line 274
    move-object v0, p0

    .line 275
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/books/service/SetSyncableService;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 276
    .local v3, "service":Landroid/content/Intent;
    invoke-virtual {v0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 279
    const-string v4, "BooksApp"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mConfig:Lcom/google/android/apps/books/util/Config;

    if-eqz v4, :cond_1

    .line 280
    const-string v4, "BooksApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Created application version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/BooksApplication;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v6}, Lcom/google/android/apps/books/util/Config;->getVersionString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/provider/VolumeContentStore;->sourceUsingProvider(Landroid/content/ContentResolver;)Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createFromContext(Landroid/content/Context;Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;)Lcom/google/android/apps/books/provider/VolumeContentStore;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    .line 287
    new-instance v4, Lcom/google/android/apps/books/model/DataControllerStoreImpl;

    iget-object v5, p0, Lcom/google/android/apps/books/app/BooksApplication;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-direct {v4, v5, p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;-><init>(Lcom/google/android/apps/books/provider/VolumeContentStore;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    .line 288
    new-instance v4, Lcom/google/android/apps/books/data/LocalDictionarySubController;

    new-instance v5, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/dictionary/DictionaryNotificationListenerImpl;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/google/android/apps/books/app/BooksApplication;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    iget-object v7, p0, Lcom/google/android/apps/books/app/BooksApplication;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/books/data/LocalDictionarySubController;-><init>(Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/provider/VolumeContentStore;)V

    iput-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    .line 292
    const-string v4, "accessibility"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/google/android/apps/books/app/BooksApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 295
    invoke-static {p0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->initialize(Landroid/content/Context;)V

    .line 296
    return-void
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mClientConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication;->mClientConnectionManager:Lorg/apache/http/conn/ClientConnectionManager;

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 310
    :cond_0
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 311
    return-void
.end method

.method public setTransitionBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 932
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mTransitionBitmap:Landroid/graphics/Bitmap;

    .line 933
    return-void
.end method

.method public withIsSchoolOwned(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 946
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mEventualIsSchoolOwned:Lcom/google/android/apps/books/util/Eventual;

    if-nez v1, :cond_0

    .line 947
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mEventualIsSchoolOwned:Lcom/google/android/apps/books/util/Eventual;

    .line 948
    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->FAKE_SLOW_IS_SCHOOL_OWNED:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getRawBoolean(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    .line 949
    .local v0, "override":Ljava/lang/Boolean;
    if-eqz v0, :cond_1

    .line 950
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/apps/books/app/BooksApplication$4;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/app/BooksApplication$4;-><init>(Lcom/google/android/apps/books/app/BooksApplication;Ljava/lang/Boolean;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 961
    .end local v0    # "override":Ljava/lang/Boolean;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mEventualIsSchoolOwned:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 962
    return-void

    .line 957
    .restart local v0    # "override":Ljava/lang/Boolean;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication;->mEventualIsSchoolOwned:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/Eventual;->loadingConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/books/app/RecommendationsUtil;->loadIsSchoolOwned(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method
