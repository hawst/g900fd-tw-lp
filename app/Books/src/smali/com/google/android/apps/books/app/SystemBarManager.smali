.class public Lcom/google/android/apps/books/app/SystemBarManager;
.super Ljava/lang/Object;
.source "SystemBarManager.java"


# instance fields
.field private mActionBarHeight:I

.field private final mBarAwareViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mSystemWindowInsets:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    .line 43
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mBarAwareViews:Ljava/util/Set;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mActionBarHeight:I

    return-void
.end method

.method private avoidSystemBars(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 109
    if-eqz p1, :cond_0

    .line 110
    const v2, 0x7f0e003c

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 112
    .local v1, "usePadding":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SystemBarManager;->getActionBarBottomWhenVisible()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 124
    .end local v1    # "usePadding":Ljava/lang/Boolean;
    :cond_0
    :goto_0
    return-void

    .line 117
    .restart local v1    # "usePadding":Ljava/lang/Boolean;
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 119
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SystemBarManager;->getActionBarBottomWhenVisible()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 121
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private updateViews()V
    .locals 3

    .prologue
    .line 103
    iget-object v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mBarAwareViews:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 104
    .local v1, "view":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/SystemBarManager;->avoidSystemBars(Landroid/view/View;)V

    goto :goto_0

    .line 106
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method public getActionBarBottomWhenVisible()I
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mActionBarHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mActionBarHeight:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method

.method public getActionBarCurrentBottom(Landroid/support/v7/app/ActionBarActivity;)I
    .locals 3
    .param p1, "activity"    # Landroid/support/v7/app/ActionBarActivity;

    .prologue
    const/4 v1, 0x0

    .line 70
    if-nez p1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v1

    .line 74
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 75
    .local v0, "bar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SystemBarManager;->getActionBarBottomWhenVisible()I

    move-result v1

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mActionBarHeight:I

    .line 62
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 63
    iget-object v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mBarAwareViews:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 64
    return-void

    .line 52
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 53
    .local v0, "theme":Landroid/content/res/Resources$Theme;
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 54
    .local v1, "typedValue":Landroid/util/TypedValue;
    const v2, 0x7f010049

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    iget v2, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mActionBarHeight:I

    goto :goto_0

    .line 59
    :cond_1
    iput v4, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mActionBarHeight:I

    goto :goto_0
.end method

.method public onSystemWindowInsetsChanged(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 96
    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mSystemWindowInsets:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SystemBarManager;->updateViews()V

    .line 100
    :cond_0
    return-void
.end method

.method public registerSystemBarAvoidingView(Landroid/view/View;Z)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "usePadding"    # Z

    .prologue
    .line 144
    const v0, 0x7f0e003c

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SystemBarManager;->avoidSystemBars(Landroid/view/View;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/app/SystemBarManager;->mBarAwareViews:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 147
    return-void
.end method
