.class public Lcom/google/android/apps/books/view/pages/PageTurnJava;
.super Ljava/lang/Object;
.source "PageTurnJava.java"

# interfaces
.implements Lcom/google/android/apps/books/view/pages/PageTurnCallback;
.implements Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;


# static fields
.field private static final LOGD:Z

.field private static final LOGW:Z


# instance fields
.field private mAnchorIndex:Ljava/lang/Integer;

.field private final mContext:Landroid/content/Context;

.field private final mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

.field private final mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

.field private final mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

.field private final mView:Lcom/google/android/apps/books/widget/PagesView3D;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-string v0, "PageTurnJava"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->LOGD:Z

    .line 40
    const-string v0, "PageTurnJava"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->LOGW:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/widget/PagesView3D;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/google/android/apps/books/widget/PagesView3D;
    .param p3, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mAnchorIndex:Ljava/lang/Integer;

    .line 59
    sget-boolean v0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->LOGD:Z

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "PageTurnJava"

    const-string v1, "creating PageTurnDelegate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    .line 65
    const/4 v0, -0x3

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setSurfacePixelFormat(I)V

    .line 67
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    invoke-direct {v0}, Lcom/google/android/apps/books/view/pages/PageTurnSetting;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    .line 68
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;-><init>(Landroid/content/Context;Lcom/google/android/opengl/common/GLSurfaceView;Lcom/google/android/apps/books/view/pages/PageTurnSetting;Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->setRenderer(Lcom/google/android/opengl/common/GLSurfaceView$Renderer;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setCallback(Lcom/google/android/apps/books/view/pages/PageTurnCallback;)V

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->init()V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnScene;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/view/pages/PageTurnJava;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnSetting;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/view/pages/PageTurnJava;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/view/pages/PageTurnJava;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/view/pages/PageTurnJava;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->normalizePageNumber(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/view/pages/PageTurnJava;ILjava/lang/String;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/view/pages/PageTurnJava;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->maybeLogSpecialPageTypes(ILjava/lang/String;ZZZ)V

    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setRenderMode(I)V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->resetZoom()V

    .line 94
    return-void
.end method

.method static makeBitmapForGL(Landroid/content/Context;Landroid/graphics/Bitmap;ILcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;)Landroid/graphics/Bitmap;
    .locals 25
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "maxTextureSize"    # I
    .param p3, "borders"    # Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;

    .prologue
    .line 239
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 240
    .local v16, "srcWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    .line 241
    .local v15, "srcHeight":I
    move-object/from16 v0, p3

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->left:I

    move/from16 v19, v0

    move-object/from16 v0, p3

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->right:I

    move/from16 v20, v0

    add-int v4, v19, v20

    .line 242
    .local v4, "bordersWidth":I
    move-object/from16 v0, p3

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->top:I

    move/from16 v19, v0

    move-object/from16 v0, p3

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->bottom:I

    move/from16 v20, v0

    add-int v3, v19, v20

    .line 243
    .local v3, "bordersHeight":I
    add-int v18, v16, v4

    .line 244
    .local v18, "wantedWidth":I
    add-int v17, v15, v3

    .line 246
    .local v17, "wantedHeight":I
    move/from16 v0, v18

    move/from16 v1, p2

    if-gt v0, v1, :cond_0

    move/from16 v0, v17

    move/from16 v1, p2

    if-gt v0, v1, :cond_0

    .line 293
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p1

    .line 250
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    const/4 v9, 0x0

    .line 251
    .local v9, "dstWidth":I
    const/4 v8, 0x0

    .line 259
    .local v8, "dstHeight":I
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v19

    sub-int v11, p2, v19

    .line 262
    .local v11, "maxBitmapSide":I
    int-to-float v0, v11

    move/from16 v19, v0

    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v14, v19, v20

    .line 263
    .local v14, "scale":F
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v14

    move/from16 v0, v19

    float-to-int v9, v0

    .line 264
    int-to-float v0, v15

    move/from16 v19, v0

    mul-float v19, v19, v14

    move/from16 v0, v19

    float-to-int v8, v0

    .line 266
    const-string v19, "PageTurnJava"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 267
    const-string v19, "PageTurnJava"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "bitmap is too large. Max size: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " width: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " height: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " wantedWidth: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " wantedHeight: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " dstWidth: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " dstHeight: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " scale: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_1
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getMemoryClass()I

    move-result v12

    .line 274
    .local v12, "memoryClass":I
    const/16 v19, 0x40

    move/from16 v0, v19

    if-lt v12, v0, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    .line 278
    .local v6, "config":Landroid/graphics/Bitmap$Config;
    :goto_1
    :try_start_0
    const-string v19, "PageTurnJava#makeBitmapForGL"

    move-object/from16 v0, v19

    invoke-static {v0, v9, v8, v6}, Lcom/google/android/apps/books/util/BitmapUtils;->createBitmapInReader(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 281
    .local v7, "copy":Landroid/graphics/Bitmap;
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 282
    .local v13, "paint":Landroid/graphics/Paint;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 283
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 284
    .local v5, "canvas":Landroid/graphics/Canvas;
    const/16 v19, 0x0

    new-instance v20, Landroid/graphics/RectF;

    const/16 v21, 0x0

    const/16 v22, 0x0

    int-to-float v0, v9

    move/from16 v23, v0

    int-to-float v0, v8

    move/from16 v24, v0

    invoke-direct/range {v20 .. v24}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v5, v0, v1, v2, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 p1, v7

    .line 286
    goto/16 :goto_0

    .line 274
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "config":Landroid/graphics/Bitmap$Config;
    .end local v7    # "copy":Landroid/graphics/Bitmap;
    .end local v13    # "paint":Landroid/graphics/Paint;
    :cond_2
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_1

    .line 291
    .restart local v6    # "config":Landroid/graphics/Bitmap$Config;
    :catch_0
    move-exception v10

    .line 292
    .local v10, "e":Ljava/lang/OutOfMemoryError;
    move-object/from16 v0, p0

    invoke-static {v0, v10, v9, v8}, Lcom/google/android/apps/books/app/BooksApplication;->reportAndRethrow(Landroid/content/Context;Ljava/lang/OutOfMemoryError;II)V

    .line 293
    const/16 p1, 0x0

    goto/16 :goto_0
.end method

.method private maybeLogSpecialPageTypes(ILjava/lang/String;ZZZ)V
    .locals 3
    .param p1, "slot"    # I
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "valid"    # Z
    .param p4, "recycle"    # Z
    .param p5, "hasBookMark"    # Z

    .prologue
    .line 183
    const-string v0, "PageTurnJava"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "PageTurnJava"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPage(slot="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", valid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", recyc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bkmk="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_0
    return-void
.end method

.method private normalizePageNumber(I)I
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 472
    :goto_0
    if-gez p1, :cond_0

    .line 473
    add-int/lit8 p1, p1, 0x6

    goto :goto_0

    .line 475
    :cond_0
    rem-int/lit8 v0, p1, 0x6

    return v0
.end method

.method private runOnGlThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/PagesView3D;->queueEvent(Ljava/lang/Runnable;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->requestRender()V

    .line 152
    return-void
.end method

.method private setSurfacePixelFormat(I)V
    .locals 2
    .param p1, "surfacePixelFormat"    # I

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setEGLContextClientVersion(I)V

    .line 484
    if-nez p1, :cond_0

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setEGLConfigChooser(Z)V

    .line 490
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    new-instance v1, Lcom/google/android/opengl/common/PixelFormatConfigChooser;

    invoke-direct {v1, p1}, Lcom/google/android/opengl/common/PixelFormatConfigChooser;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setEGLConfigChooser(Lcom/google/android/opengl/common/GLSurfaceView$EGLConfigChooser;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_0
.end method


# virtual methods
.method public finishOpening(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 1
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 394
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava$8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava$8;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;Lcom/google/android/apps/books/util/ScreenDirection;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V

    .line 401
    return-void
.end method

.method public getCoverView()Landroid/view/View;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getCoverView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

    goto :goto_0
.end method

.method public initializationComplete()V
    .locals 0

    .prologue
    .line 497
    return-void
.end method

.method public onLoadingPageVisibilityChanged(Z)V
    .locals 1
    .param p1, "loadingPageVisible"    # Z

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/PagesView3D;->onLoadingPageVisibilityChanged(Z)V

    .line 546
    return-void
.end method

.method public onPagesInvalidated()V
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->onPagesInvalidated()V

    .line 541
    return-void
.end method

.method public onTurnAnimationFinished()V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mView:Lcom/google/android/apps/books/widget/PagesView3D;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->onTurnAnimationFinished()V

    .line 502
    return-void
.end method

.method public resetZoom()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 380
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1, v1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setZoom(FFF)V

    .line 381
    return-void
.end method

.method public setAnchorIndex(I)V
    .locals 1
    .param p1, "anchorIndex"    # I

    .prologue
    .line 177
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mAnchorIndex:Ljava/lang/Integer;

    .line 178
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .param p1, "backgroundColor"    # I

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->getCoverView()Landroid/view/View;

    move-result-object v0

    .line 464
    .local v0, "coverView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 466
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 468
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setBackgroundColor(I)V

    .line 469
    return-void
.end method

.method public setCoverView(Landroid/view/View;)V
    .locals 1
    .param p1, "coverView"    # Landroid/view/View;

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setCoverView(Landroid/view/View;)V

    .line 525
    return-void
.end method

.method public setExecutingInitialLoadTransition(Z)V
    .locals 1
    .param p1, "executing"    # Z

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setExecutingInitialLoadTransition(Z)V

    .line 550
    return-void
.end method

.method public setLoadingBitmap(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 119
    if-nez p1, :cond_0

    .line 146
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v1, v2, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    .line 124
    .local v1, "previous":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_1
    new-instance v2, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/apps/books/app/BooksApplication;->reportAndRethrow(Landroid/content/Context;Ljava/lang/OutOfMemoryError;II)V

    goto :goto_1
.end method

.method public setMeasurements(Lcom/google/android/apps/books/view/pages/PageMeasurements;Z)V
    .locals 1
    .param p1, "measurements"    # Lcom/google/android/apps/books/view/pages/PageMeasurements;
    .param p2, "displayTwoPages"    # Z

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setMeasurements(Lcom/google/android/apps/books/view/pages/PageMeasurements;Z)V

    .line 80
    return-void
.end method

.method public declared-synchronized setPageLoading(ILandroid/graphics/Point;)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 219
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->normalizePageNumber(I)I

    move-result v1

    .line 222
    .local v1, "slot":I
    const-string v2, "loading "

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->maybeLogSpecialPageTypes(ILjava/lang/String;ZZZ)V

    .line 223
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava$5;

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnJava$5;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;ILandroid/graphics/Point;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    monitor-exit p0

    return-void

    .line 219
    .end local v1    # "slot":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V
    .locals 12
    .param p1, "index"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "pageRenderDetails"    # Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    .prologue
    .line 308
    monitor-enter p0

    :try_start_0
    const-string v0, "missing bitmap"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMaxTextureSize()I

    move-result v9

    .line 311
    .local v9, "maxTextureSize":I
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;

    move-result-object v10

    .line 312
    .local v10, "measurements":Lcom/google/android/apps/books/view/pages/PageMeasurements;
    if-eqz v10, :cond_0

    const/16 v0, 0x100

    if-ge v9, v0, :cond_2

    .line 324
    :cond_0
    const-string v0, "PageTurnJava"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 325
    const-string v0, "PageTurnJava"

    const-string v2, "Skipping early setPageToBitmap."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 331
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mAnchorIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-lt p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mAnchorIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    if-le p1, v0, :cond_4

    .line 338
    :cond_3
    const-string v0, "PageTurnJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPageToBitmap() delivered texture outside of neighborhood, index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mAnchorIndex: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mAnchorIndex:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 308
    .end local v9    # "maxTextureSize":I
    .end local v10    # "measurements":Lcom/google/android/apps/books/view/pages/PageMeasurements;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 344
    .restart local v9    # "maxTextureSize":I
    .restart local v10    # "measurements":Lcom/google/android/apps/books/view/pages/PageMeasurements;
    :cond_4
    :try_start_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->normalizePageNumber(I)I

    move-result v1

    .line 346
    .local v1, "slot":I
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v3}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->displayTwoPages()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v4, v4, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    aget-object v4, v4, v1

    iget-boolean v4, v4, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    invoke-virtual {v10, v0, v2, v3, v4}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->calculateNeededBorders(IIZZ)Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;

    move-result-object v8

    .line 351
    .local v8, "borders":Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mContext:Landroid/content/Context;

    invoke-static {v0, p2, v9, v8}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->makeBitmapForGL(Landroid/content/Context;Landroid/graphics/Bitmap;ILcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 354
    .local v11, "smallEnoughBitmap":Landroid/graphics/Bitmap;
    const-string v2, "bitmap "

    const/4 v3, 0x1

    const/4 v4, 0x1

    iget-boolean v5, p3, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->hasBookmark:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->maybeLogSpecialPageTypes(ILjava/lang/String;ZZZ)V

    .line 356
    new-instance v2, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;

    move-object v3, p0

    move v4, v1

    move-object v5, v11

    move-object v6, p3

    move-object v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/view/pages/PageTurnJava$6;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized setPagesEmpty()V
    .locals 1

    .prologue
    .line 193
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/view/pages/PageTurnJava$3;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    monitor-exit p0

    return-void

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "fraction"    # F
    .param p3, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p4, "movingToNewPosition"    # Z

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setAnchorIndex(I)V

    .line 158
    const-string v0, "PageTurnJava"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "PageTurnJava"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setTurnState: fraction="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->normalizePageNumber(I)I

    move-result v2

    .line 168
    .local v2, "slot":I
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;

    move-object v1, p0

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;IFLcom/google/android/apps/books/util/ScreenDirection;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    monitor-exit p0

    return-void

    .line 156
    .end local v2    # "slot":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V
    .locals 1
    .param p1, "direction"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V

    .line 104
    :cond_0
    return-void
.end method

.method public setZoom(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 384
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava$7;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/books/view/pages/PageTurnJava$7;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;FFF)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V

    .line 391
    return-void
.end method

.method public startBookmarkAnimate(IZ)V
    .locals 1
    .param p1, "targetSlot"    # I
    .param p2, "isGainingBookmark"    # Z

    .prologue
    .line 528
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava$10;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnJava$10;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;IZ)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->runOnGlThread(Ljava/lang/Runnable;)V

    .line 535
    return-void
.end method
