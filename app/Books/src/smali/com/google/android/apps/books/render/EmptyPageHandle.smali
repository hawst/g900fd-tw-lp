.class public Lcom/google/android/apps/books/render/EmptyPageHandle;
.super Ljava/lang/Object;
.source "EmptyPageHandle.java"

# interfaces
.implements Lcom/google/android/apps/books/render/FixedLayoutPageHandle;
.implements Lcom/google/android/apps/books/render/FixedPaginationPageHandle;
.implements Lcom/google/android/apps/books/render/TextPageHandle;


# static fields
.field public static final INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/books/render/EmptyPageHandle;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/EmptyPageHandle;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFirstBookPage()Lcom/google/android/apps/books/model/Page;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFirstBookPageIndex()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, -0x1

    return v0
.end method

.method public getFirstChapter()Lcom/google/android/apps/books/model/Chapter;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFirstChapterIndex()I
    .locals 1

    .prologue
    .line 21
    const/4 v0, -0x1

    return v0
.end method

.method public getGridRowIndex()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, -0x1

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, -0x1

    return v0
.end method

.method public getIndices()Lcom/google/android/apps/books/render/PageIndices;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, -0x1

    return v0
.end method
