.class Lcom/google/android/apps/books/app/OffersFragment$1;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$1;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 5
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 107
    const/4 v0, 0x1

    .line 108
    .local v0, "refetchLibrary":Z
    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment$1;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # getter for: Lcom/google/android/apps/books/app/OffersFragment;->mOffersProxy:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v3}, Lcom/google/android/apps/books/app/OffersFragment;->access$000(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v1, v2, v3, v4}, Lcom/google/android/apps/books/data/BooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 109
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 102
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment$1;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
