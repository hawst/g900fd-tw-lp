.class public final enum Lcom/google/android/apps/books/app/MoveType;
.super Ljava/lang/Enum;
.source "MoveType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/MoveType$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/MoveType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/MoveType;

.field public static final enum ACCEPT_NEW_POSITION_FROM_SERVER:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum CHOSE_HIGHLIGHT:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum CHOSE_PAGE_IN_SKIM:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum CHOSE_TOC_ANNOTATION:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum CHOSE_TOC_CHAPTER:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum END_OF_SCRUB:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum FALLBACK_TO_DEFAULT_POSITION:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum FOLLOW_LINK:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum NEXT_PAGE:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum PREV_PAGE:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum RESUME_POSITION:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum RETURN_TO_START_OF_SKIM:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum SCRUB_CONTINUES:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum TAP_QUICK_BOOKMARK:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum UNDO_JUMP:Lcom/google/android/apps/books/app/MoveType;

.field public static final enum UNSPECIFIED:Lcom/google/android/apps/books/app/MoveType;


# instance fields
.field private final mIsJump:Z

.field private final mServerAction:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "NEXT_PAGE"

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->NEXT_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->NEXT_PAGE:Lcom/google/android/apps/books/app/MoveType;

    .line 7
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "PREV_PAGE"

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->PREV_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->PREV_PAGE:Lcom/google/android/apps/books/app/MoveType;

    .line 9
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "SCRUB_CONTINUES"

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->SCRUB_CONTINUES:Lcom/google/android/apps/books/app/MoveType;

    .line 10
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "END_OF_SCRUB"

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->END_OF_SCRUB:Lcom/google/android/apps/books/app/MoveType;

    .line 11
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "RETURN_TO_START_OF_SKIM"

    sget-object v2, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->RETURN_TO_START_OF_SKIM:Lcom/google/android/apps/books/app/MoveType;

    .line 12
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "TAP_QUICK_BOOKMARK"

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->TAP_QUICK_BOOKMARK:Lcom/google/android/apps/books/app/MoveType;

    .line 14
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "CHOSE_TOC_ANNOTATION"

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->CHOSE_BOOKMARK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_ANNOTATION:Lcom/google/android/apps/books/app/MoveType;

    .line 15
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "CHOSE_TOC_CHAPTER"

    const/4 v2, 0x7

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->CHOSE_BOOKMARK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_CHAPTER:Lcom/google/android/apps/books/app/MoveType;

    .line 17
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "FOLLOW_LINK"

    const/16 v2, 0x8

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->FOLLOW_LINK:Lcom/google/android/apps/books/app/MoveType;

    .line 18
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "UNDO_JUMP"

    const/16 v2, 0x9

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->UNDO_JUMP:Lcom/google/android/apps/books/app/MoveType;

    .line 21
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "CHOSE_HIGHLIGHT"

    const/16 v2, 0xa

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->CHOSE_BOOKMARK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->CHOSE_HIGHLIGHT:Lcom/google/android/apps/books/app/MoveType;

    .line 22
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "CHOSE_PAGE_IN_SKIM"

    const/16 v2, 0xb

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->CHOSE_PAGE_IN_SKIM:Lcom/google/android/apps/books/app/MoveType;

    .line 24
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "MOVE_TO_SEARCH_RESULT"

    const/16 v2, 0xc

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SEARCH_WITHIN_BOOK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

    .line 26
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "RESUME_POSITION"

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->RESUME_POSITION:Lcom/google/android/apps/books/app/MoveType;

    .line 27
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "FALLBACK_TO_DEFAULT_POSITION"

    const/16 v2, 0xe

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->FALLBACK_TO_DEFAULT_POSITION:Lcom/google/android/apps/books/app/MoveType;

    .line 28
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "ACCEPT_NEW_POSITION_FROM_SERVER"

    const/16 v2, 0xf

    sget-object v3, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SCROLL_TO_PAGE:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->ACCEPT_NEW_POSITION_FROM_SERVER:Lcom/google/android/apps/books/app/MoveType;

    .line 34
    new-instance v0, Lcom/google/android/apps/books/app/MoveType;

    const-string v1, "UNSPECIFIED"

    const/16 v2, 0x10

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/MoveType;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->UNSPECIFIED:Lcom/google/android/apps/books/app/MoveType;

    .line 5
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/google/android/apps/books/app/MoveType;

    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->NEXT_PAGE:Lcom/google/android/apps/books/app/MoveType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->PREV_PAGE:Lcom/google/android/apps/books/app/MoveType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->SCRUB_CONTINUES:Lcom/google/android/apps/books/app/MoveType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->END_OF_SCRUB:Lcom/google/android/apps/books/app/MoveType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->RETURN_TO_START_OF_SKIM:Lcom/google/android/apps/books/app/MoveType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->TAP_QUICK_BOOKMARK:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_ANNOTATION:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_CHAPTER:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->FOLLOW_LINK:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->UNDO_JUMP:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->CHOSE_HIGHLIGHT:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->CHOSE_PAGE_IN_SKIM:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->RESUME_POSITION:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->FALLBACK_TO_DEFAULT_POSITION:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->ACCEPT_NEW_POSITION_FROM_SERVER:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->UNSPECIFIED:Lcom/google/android/apps/books/app/MoveType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/MoveType;->$VALUES:[Lcom/google/android/apps/books/app/MoveType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
    .locals 0
    .param p3, "serverAction"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .param p4, "isJump"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/books/app/MoveType;->mServerAction:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    .line 46
    iput-boolean p4, p0, Lcom/google/android/apps/books/app/MoveType;->mIsJump:Z

    .line 47
    return-void
.end method

.method public static nonNull(Lcom/google/android/apps/books/app/MoveType;)Lcom/google/android/apps/books/app/MoveType;
    .locals 0
    .param p0, "type"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 50
    if-eqz p0, :cond_0

    .end local p0    # "type":Lcom/google/android/apps/books/app/MoveType;
    :goto_0
    return-object p0

    .restart local p0    # "type":Lcom/google/android/apps/books/app/MoveType;
    :cond_0
    sget-object p0, Lcom/google/android/apps/books/app/MoveType;->UNSPECIFIED:Lcom/google/android/apps/books/app/MoveType;

    goto :goto_0
.end method

.method private shouldAffectState()Z
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/apps/books/app/MoveType$1;->$SwitchMap$com$google$android$apps$books$app$MoveType:[I

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/MoveType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 113
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 111
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/MoveType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/google/android/apps/books/app/MoveType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/MoveType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/MoveType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/google/android/apps/books/app/MoveType;->$VALUES:[Lcom/google/android/apps/books/app/MoveType;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/MoveType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/MoveType;

    return-object v0
.end method


# virtual methods
.method public getServerAction()Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/app/MoveType;->mServerAction:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    return-object v0
.end method

.method public isJump()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/MoveType;->mIsJump:Z

    return v0
.end method

.method public isPageTurn()Z
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/apps/books/app/MoveType;->NEXT_PAGE:Lcom/google/android/apps/books/app/MoveType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/app/MoveType;->PREV_PAGE:Lcom/google/android/apps/books/app/MoveType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrubberMove()Z
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/apps/books/app/MoveType;->SCRUB_CONTINUES:Lcom/google/android/apps/books/app/MoveType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/app/MoveType;->END_OF_SCRUB:Lcom/google/android/apps/books/app/MoveType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldClearJumpBit()Z
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/MoveType;->isJump()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/MoveType;->shouldAffectState()Z

    move-result v0

    goto :goto_0
.end method

.method shouldClearUndoJumpBit()Z
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/books/app/MoveType;->UNDO_JUMP:Lcom/google/android/apps/books/app/MoveType;

    if-ne p0, v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/MoveType;->shouldAffectState()Z

    move-result v0

    goto :goto_0
.end method

.method shouldZoomToMinWhenMoving()Z
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/MoveType;->getServerAction()Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->SEARCH_WITHIN_BOOK:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
