.class Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;
.super Landroid/os/AsyncTask;
.source "OfflineDictionaryDownloadCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LookupTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Landroid/view/View;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 63
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    # invokes: Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->maybeGetOfflineCard()Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->access$000(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Landroid/view/View;

    move-result-object v0

    .line 68
    .local v0, "offlineCard":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 69
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    .line 71
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 58
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/view/View;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    aput-object v1, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 77
    .local v0, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 81
    .end local v0    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 58
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;->onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
