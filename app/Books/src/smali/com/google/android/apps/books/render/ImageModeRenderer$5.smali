.class Lcom/google/android/apps/books/render/ImageModeRenderer$5;
.super Ljava/lang/Object;
.source "ImageModeRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;)V
    .locals 0

    .prologue
    .line 1164
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$5;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V
    .locals 1
    .param p1, "selection"    # Lcom/google/android/apps/books/render/PageStructureSelection;

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$5;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$1800(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/PageStructureSelection;)V

    .line 1169
    return-void
.end method

.method public onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$5;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V

    .line 1174
    return-void
.end method
