.class Lcom/google/android/apps/books/geo/LayerAnnotationLoader$3;
.super Lcom/google/android/apps/books/annotations/StubAnnotationListener;
.source "LayerAnnotationLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/geo/LayerAnnotationLoader;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/geo/AnnotationSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$3;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/StubAnnotationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public volumeAnnotationsLoaded(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "segment"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p3, "annotations":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$3;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    # getter for: Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mLayerId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->access$000(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$3;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    invoke-static {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->replaceValueIfSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->onLoadedAnnotationsForSegmentIndex(ILcom/google/android/apps/books/util/ExceptionOr;)V
    invoke-static {v0, p2, v1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->access$100(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;ILcom/google/android/apps/books/util/ExceptionOr;)V

    .line 92
    :cond_0
    return-void
.end method
