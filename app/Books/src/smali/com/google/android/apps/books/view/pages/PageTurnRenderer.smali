.class Lcom/google/android/apps/books/view/pages/PageTurnRenderer;
.super Ljava/lang/Object;
.source "PageTurnRenderer.java"

# interfaces
.implements Lcom/google/android/opengl/common/GLSurfaceView$Renderer;
.implements Lcom/google/android/opengl/common/IFpsRenderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;
    }
.end annotation


# instance fields
.field private final m2DZoomMatrix:[F

.field private mAspect:F

.field private final mCallbacks:Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;

.field public final mContext:Landroid/content/Context;

.field private mCoverView:Landroid/view/View;

.field public mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field private final mCurvedOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field private final mCurvedTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field private final mDisplayFps:Z

.field private final mFinalPageMatrix:[F

.field private final mFlatOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field private final mFlatOneTextureProgramColorBg:Lcom/google/android/opengl/carousel/GLProgram;

.field private final mFlatTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field private mFovy:F

.field private final mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

.field private mFrameCount:I

.field private mHeight:I

.field private mMaxTextureSize:I

.field private mMeasurements:Lcom/google/android/apps/books/view/pages/PageMeasurements;

.field private final mPageStretchMatrix:[F

.field final mProjMatrix:[F

.field public final mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

.field private final mTempMatrix:[F

.field private final mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

.field private mTwoPageLandscape:Z

.field final mVMatrix:[F

.field final mVPMatrix:[F

.field private mVerboseMode:Z

.field public final mView:Lcom/google/android/opengl/common/GLSurfaceView;

.field private mWidth:I

.field private mZoomCenterX:F

.field private mZoomCenterY:F

.field private mZoomScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/opengl/common/GLSurfaceView;Lcom/google/android/apps/books/view/pages/PageTurnSetting;Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/google/android/opengl/common/GLSurfaceView;
    .param p3, "setting"    # Lcom/google/android/apps/books/view/pages/PageTurnSetting;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;
    .param p5, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-boolean v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mDisplayFps:Z

    .line 55
    new-instance v0, Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {v0}, Lcom/google/android/opengl/carousel/GLProgram;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatOneTextureProgramColorBg:Lcom/google/android/opengl/carousel/GLProgram;

    .line 56
    new-instance v0, Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {v0}, Lcom/google/android/opengl/carousel/GLProgram;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 57
    new-instance v0, Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {v0}, Lcom/google/android/opengl/carousel/GLProgram;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 58
    new-instance v0, Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {v0}, Lcom/google/android/opengl/carousel/GLProgram;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurvedOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 59
    new-instance v0, Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {v0}, Lcom/google/android/opengl/carousel/GLProgram;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurvedTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 62
    new-instance v0, Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {v0}, Lcom/google/android/opengl/carousel/GLProgram;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 73
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTempMatrix:[F

    .line 75
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mProjMatrix:[F

    .line 77
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVMatrix:[F

    .line 79
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVPMatrix:[F

    .line 81
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    .line 83
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    .line 85
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFinalPageMatrix:[F

    .line 90
    iput v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mWidth:I

    .line 91
    iput v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mHeight:I

    .line 92
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFovy:F

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mAspect:F

    .line 109
    const/16 v0, 0x40

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mMaxTextureSize:I

    .line 150
    iput v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFrameCount:I

    .line 119
    new-instance v0, Lcom/google/android/opengl/common/FpsDisplay;

    invoke-direct {v0, p0}, Lcom/google/android/opengl/common/FpsDisplay;-><init>(Lcom/google/android/opengl/common/IFpsRenderer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    .line 120
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mContext:Landroid/content/Context;

    .line 121
    iput-object p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mView:Lcom/google/android/opengl/common/GLSurfaceView;

    .line 123
    iput-object p4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCallbacks:Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;

    .line 125
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-direct {v0, p0, p3, p5}, Lcom/google/android/apps/books/view/pages/PageTurnScene;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnRenderer;Lcom/google/android/apps/books/view/pages/PageTurnSetting;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/view/pages/PageTurnRenderer;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCoverView:Landroid/view/View;

    return-object v0
.end method

.method private varargs createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V
    .locals 5
    .param p1, "program"    # Lcom/google/android/opengl/carousel/GLProgram;
    .param p2, "vertexShaderResource"    # I
    .param p3, "fragmentShaderResource"    # I
    .param p4, "textureNames"    # [Ljava/lang/String;

    .prologue
    .line 314
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mContext:Landroid/content/Context;

    invoke-static {v3, p2}, Lcom/google/android/opengl/carousel/GLProgram;->getShaderFromRes(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "vertexSource":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mContext:Landroid/content/Context;

    invoke-static {v3, p3}, Lcom/google/android/opengl/carousel/GLProgram;->getShaderFromRes(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "fragmentSource":Ljava/lang/String;
    invoke-virtual {p1, v2, v0}, Lcom/google/android/opengl/carousel/GLProgram;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    array-length v3, p4

    if-lez v3, :cond_0

    .line 319
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 320
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p4

    if-ge v1, v3, :cond_0

    .line 321
    aget-object v3, p4, v1

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getUniformLocation(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 322
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load uniform location: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 320
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 325
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method private enableVertexAttribArrays()V
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 487
    const/4 v0, 0x2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 488
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 489
    const/4 v0, 0x3

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 490
    return-void
.end method

.method private getUniformLocation(Ljava/lang/String;)I
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 493
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 494
    .local v0, "location":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 495
    const-string v1, "PageTurnRenderer"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496
    const-string v1, "PageTurnRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uniform location not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in program: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_0
    return v0
.end method

.method private hideCoverView()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCoverView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCoverView:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/books/view/pages/PageTurnRenderer$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer$1;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnRenderer;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private resetFrameCount()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFrameCount:I

    .line 194
    return-void
.end method

.method private setGlParameter()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 474
    const/16 v0, 0xcf5

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    .line 475
    const/16 v0, 0xbd0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 476
    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 477
    invoke-static {v2, v2, v2, v2}, Landroid/opengl/GLES20;->glColorMask(ZZZZ)V

    .line 479
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 480
    const/16 v0, 0x405

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 482
    const-string v0, "setGlParameter"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 483
    return-void
.end method

.method private setUpTexture(ILcom/google/android/apps/books/view/pages/TexturePlacement;Ljava/lang/String;)V
    .locals 4
    .param p1, "textureSlot"    # I
    .param p2, "texture"    # Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .param p3, "uniformMatrixName"    # Ljava/lang/String;

    .prologue
    .line 516
    invoke-static {p1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 517
    const/16 v1, 0xde1

    invoke-virtual {p2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;->getId()I

    move-result v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 519
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    .line 520
    .local v0, "matrixLoc":I
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;->matrixFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZLjava/nio/FloatBuffer;)V

    .line 521
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUpTexture: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;->getDebugName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 522
    return-void
.end method

.method private updateFinalPageMatrix()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTempMatrix:[F

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVPMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFinalPageMatrix:[F

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTempMatrix:[F

    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 418
    return-void
.end method

.method private updateProjection()V
    .locals 12

    .prologue
    .line 330
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setPageStretchMatrix([F)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->displayTwoPages()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mAspect:F

    .line 334
    const/high16 v0, 0x40000000    # 2.0f

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float v11, v0, v1

    .line 335
    .local v11, "fovy":F
    float-to-double v0, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFovy:F

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mProjMatrix:[F

    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFovy:F

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mAspect:F

    const v3, 0x3dcccccd    # 0.1f

    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/opengl/carousel/GL2Helper;->loadPerspectiveMatrix([FFFFF)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVMatrix:[F

    const/4 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v3, 0x0

    const/high16 v4, -0x40000000    # -2.0f

    const/high16 v5, -0x40800000    # -1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v10, 0x0

    invoke-static/range {v0 .. v10}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 345
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVPMatrix:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mProjMatrix:[F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVMatrix:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 346
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->updateFinalPageMatrix()V

    .line 347
    return-void

    .line 339
    .end local v11    # "fovy":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mAspect:F

    .line 340
    const/high16 v0, 0x40000000    # 2.0f

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float v11, v0, v1

    .line 341
    .restart local v11    # "fovy":F
    float-to-double v0, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFovy:F

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mProjMatrix:[F

    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFovy:F

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mAspect:F

    const v3, 0x3dcccccd    # 0.1f

    const/high16 v4, 0x41200000    # 10.0f

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/opengl/carousel/GL2Helper;->loadPerspectiveMatrix([FFFFF)V

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVMatrix:[F

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, -0x40000000    # -2.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v10, 0x0

    invoke-static/range {v0 .. v10}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    goto :goto_0
.end method

.method private updateZoomMatrix()V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 387
    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    aget v0, v4, v7

    .line 388
    .local v0, "stretchScaleX":F
    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    const/4 v5, 0x5

    aget v1, v4, v5

    .line 390
    .local v1, "stretchScaleY":F
    iget v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomCenterX:F

    div-float v2, v4, v0

    .line 391
    .local v2, "x":F
    iget v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomCenterY:F

    div-float v3, v4, v1

    .line 394
    .local v3, "y":F
    iget-boolean v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTwoPageLandscape:Z

    if-eqz v4, :cond_0

    .line 398
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v2

    sub-float v2, v4, v9

    .line 401
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    invoke-static {v4, v7}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 403
    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    invoke-static {v4, v7, v2, v3, v8}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 404
    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomScale:F

    iget v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomScale:F

    invoke-static {v4, v7, v5, v6, v9}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 405
    iget-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->m2DZoomMatrix:[F

    neg-float v5, v2

    neg-float v6, v3

    invoke-static {v4, v7, v5, v6, v8}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 407
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->updateFinalPageMatrix()V

    .line 408
    return-void
.end method

.method private useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V
    .locals 1
    .param p1, "program"    # Lcom/google/android/opengl/carousel/GLProgram;

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    if-eq v0, p1, :cond_0

    .line 431
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v0, v0, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 433
    const-string v0, "glUseProgram"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 435
    :cond_0
    return-void
.end method


# virtual methods
.method getAttribLocation(Ljava/lang/String;)I
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 504
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v1, v1, Lcom/google/android/opengl/carousel/GLProgram;->mProgram:I

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    .line 505
    .local v0, "location":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 506
    const-string v1, "PageTurnRenderer"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    const-string v1, "PageTurnRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attrib location not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in program: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_0
    return v0
.end method

.method getCoverView()Landroid/view/View;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCoverView:Landroid/view/View;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mHeight:I

    return v0
.end method

.method public getMaxTextureSize()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mMaxTextureSize:I

    return v0
.end method

.method public getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mMeasurements:Lcom/google/android/apps/books/view/pages/PageMeasurements;

    return-object v0
.end method

.method public getPageSize(Landroid/graphics/Point;)V
    .locals 1
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTwoPageLandscape:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mWidth:I

    div-int/lit8 v0, v0, 0x2

    :goto_0
    iput v0, p1, Landroid/graphics/Point;->x:I

    .line 142
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mHeight:I

    iput v0, p1, Landroid/graphics/Point;->y:I

    .line 143
    return-void

    .line 141
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mWidth:I

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mWidth:I

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 166
    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFrameCount:I

    .line 167
    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFrameCount:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 169
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->hideCoverView()V

    .line 172
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVerboseMode:Z

    if-eqz v1, :cond_1

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v1}, Lcom/google/android/opengl/common/FpsDisplay;->preDraw()V

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->onDrawFrame()Z

    move-result v0

    .line 178
    .local v0, "stillAnimating":Z
    const-string v1, "gl error after mScene.onDrawFrame()"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 179
    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVerboseMode:Z

    if-eqz v1, :cond_2

    .line 180
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v1}, Lcom/google/android/opengl/common/FpsDisplay;->postDraw()V

    .line 183
    :cond_2
    if-eqz v0, :cond_3

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->requestRender()V

    .line 186
    :cond_3
    return-void
.end method

.method public onLoadingPageVisibilityChanged(Z)V
    .locals 1
    .param p1, "loadingPageVisible"    # Z

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCallbacks:Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;->onLoadingPageVisibilityChanged(Z)V

    .line 581
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v1, 0x0

    .line 208
    const-string v0, "gl detect error"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 209
    invoke-static {v1, v1, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 210
    iput v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFrameCount:I

    .line 211
    iput p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mWidth:I

    .line 212
    iput p3, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mHeight:I

    .line 214
    const-string v0, "PageTurnRenderer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "PageTurnRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged, width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_0
    const-string v0, "gl detect error"

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->updateProjection()V

    .line 221
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVerboseMode:Z

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/opengl/common/FpsDisplay;->updateDimension(II)V

    .line 225
    :cond_1
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 10
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/4 v9, 0x0

    const v7, 0x7f080006

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 264
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setGlParameter()V

    .line 266
    iput-object v9, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 268
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatOneTextureProgramColorBg:Lcom/google/android/opengl/carousel/GLProgram;

    const v4, 0x7f080004

    new-array v5, v2, [Ljava/lang/String;

    const-string v6, "uTexture1"

    aput-object v6, v5, v3

    invoke-direct {p0, v1, v7, v4, v5}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V

    .line 271
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const v4, 0x7f080005

    new-array v5, v2, [Ljava/lang/String;

    const-string v6, "uTexture1"

    aput-object v6, v5, v3

    invoke-direct {p0, v1, v7, v4, v5}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const v4, 0x7f080008

    const v5, 0x7f080007

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "uTexture1"

    aput-object v7, v6, v3

    const-string v7, "uTexture2"

    aput-object v7, v6, v2

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurvedOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const v4, 0x7f080001

    const/high16 v5, 0x7f080000

    new-array v6, v2, [Ljava/lang/String;

    const-string v7, "uTexture1"

    aput-object v7, v6, v3

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V

    .line 277
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurvedTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const v4, 0x7f080003

    const v5, 0x7f080002

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "uTexture1"

    aput-object v7, v6, v3

    const-string v7, "uTexture2"

    aput-object v7, v6, v2

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    const v4, 0x7f08000a

    const v5, 0x7f080009

    new-array v6, v2, [Ljava/lang/String;

    const-string v7, "uTexture"

    aput-object v7, v6, v3

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->createProgram(Lcom/google/android/opengl/carousel/GLProgram;II[Ljava/lang/String;)V

    .line 282
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 283
    const-string v1, "uOpacity"

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1, v4}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUniformFloat(Ljava/lang/String;F)V

    .line 285
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->enableVertexAttribArrays()V

    .line 287
    const-string v1, "PageTurnRenderer"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "eng"

    sget-object v4, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "userdebug"

    sget-object v4, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVerboseMode:Z

    .line 290
    new-array v0, v2, [I

    .line 291
    .local v0, "maxTextureSizeV":[I
    const/16 v1, 0xd33

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 292
    const/16 v1, 0x40

    aget v2, v0, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mMaxTextureSize:I

    .line 294
    const-string v1, "PageTurnRenderer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    const-string v1, "PageTurnRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSurfaceCreated max texture size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mMaxTextureSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_1
    iput-object v9, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->initialize()V

    .line 303
    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mVerboseMode:Z

    if-eqz v1, :cond_2

    .line 304
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFpsDisplay:Lcom/google/android/opengl/common/FpsDisplay;

    invoke-virtual {v1}, Lcom/google/android/opengl/common/FpsDisplay;->onSurfaceCreated()V

    .line 306
    :cond_2
    const-string v1, "end of onSurfaceCreated"

    invoke-static {v1}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->requestRender()V

    .line 309
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCallbacks:Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;

    invoke-interface {v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer$Callbacks;->onPagesInvalidated()V

    .line 310
    return-void

    .end local v0    # "maxTextureSizeV":[I
    :cond_3
    move v1, v3

    .line 287
    goto :goto_0
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mView:Lcom/google/android/opengl/common/GLSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/opengl/common/GLSurfaceView;->requestRender()V

    .line 201
    return-void
.end method

.method public setBackFacing(Z)V
    .locals 2
    .param p1, "backFacing"    # Z

    .prologue
    .line 444
    const-string v1, "UNI_backFacing"

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    .line 445
    .local v0, "loc":I
    if-eqz p1, :cond_0

    .line 446
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 450
    :goto_0
    return-void

    .line 448
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    goto :goto_0
.end method

.method setCoverView(Landroid/view/View;)V
    .locals 0
    .param p1, "coverView"    # Landroid/view/View;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCoverView:Landroid/view/View;

    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->resetFrameCount()V

    .line 257
    return-void
.end method

.method public setMVPUniform()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v0, v0, Lcom/google/android/opengl/carousel/GLProgram;->muMVPMatHandle:I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFinalPageMatrix:[F

    invoke-static {v0, v1, v3, v2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 471
    return-void
.end method

.method public setMVPUniform([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurrentProgram:Lcom/google/android/opengl/carousel/GLProgram;

    iget v0, v0, Lcom/google/android/opengl/carousel/GLProgram;->muMVPMatHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 464
    return-void
.end method

.method public setMeasurements(Lcom/google/android/apps/books/view/pages/PageMeasurements;Z)V
    .locals 1
    .param p1, "measurements"    # Lcom/google/android/apps/books/view/pages/PageMeasurements;
    .param p2, "displayTwoPages"    # Z

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setDisplayTwoPages(Z)V

    .line 439
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mMeasurements:Lcom/google/android/apps/books/view/pages/PageMeasurements;

    .line 440
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->updateProjection()V

    .line 441
    return-void
.end method

.method public setNormalPointer(Ljava/nio/FloatBuffer;)V
    .locals 6
    .param p1, "normals"    # Ljava/nio/FloatBuffer;

    .prologue
    const/4 v0, 0x3

    const/4 v3, 0x0

    .line 453
    const/16 v2, 0x1406

    move v1, v0

    move v4, v3

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 454
    return-void
.end method

.method public setPageStretchMatrix([F)V
    .locals 3
    .param p1, "stretchMatrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 355
    if-nez p1, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 361
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->updateZoomMatrix()V

    .line 362
    return-void

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mPageStretchMatrix:[F

    array-length v1, v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method setUniform3f(Ljava/lang/String;FFF)V
    .locals 2
    .param p1, "uniformName"    # Ljava/lang/String;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    .line 530
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0, p2, p3, p4}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setUniform3f: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 532
    return-void
.end method

.method setUniformFloat(Ljava/lang/String;F)V
    .locals 2
    .param p1, "uniformName"    # Ljava/lang/String;
    .param p2, "value"    # F

    .prologue
    .line 525
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setUniformFloat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 527
    return-void
.end method

.method setUpOneTexture(Lcom/google/android/apps/books/view/pages/TexturePlacement;FZ[F)V
    .locals 4
    .param p1, "texture1"    # Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .param p2, "opacity"    # F
    .param p3, "curved"    # Z
    .param p4, "clearColor"    # [F

    .prologue
    .line 544
    if-eqz p3, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurvedOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 553
    :goto_0
    const v0, 0x84c0

    const-string v1, "uTransform1"

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpTexture(ILcom/google/android/apps/books/view/pages/TexturePlacement;Ljava/lang/String;)V

    .line 554
    const-string v0, "uOpacity"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUniformFloat(Ljava/lang/String;F)V

    .line 555
    return-void

    .line 546
    :cond_0
    if-nez p4, :cond_1

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatOneTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    goto :goto_0

    .line 549
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatOneTextureProgramColorBg:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 550
    const-string v0, "uColorBg"

    const/4 v1, 0x0

    aget v1, p4, v1

    const/4 v2, 0x1

    aget v2, p4, v2

    const/4 v3, 0x2

    aget v3, p4, v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUniform3f(Ljava/lang/String;FFF)V

    goto :goto_0
.end method

.method setUpTwoTextures(Lcom/google/android/apps/books/view/pages/TexturePlacement;Lcom/google/android/apps/books/view/pages/TexturePlacement;FZF[F)V
    .locals 4
    .param p1, "texture1"    # Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .param p2, "texture2"    # Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .param p3, "fadeToTexture2"    # F
    .param p4, "curved"    # Z
    .param p5, "opacity"    # F
    .param p6, "clearColor"    # [F

    .prologue
    .line 559
    if-eqz p4, :cond_0

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mCurvedTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 567
    :goto_0
    const v0, 0x84c0

    const-string v1, "uTransform1"

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpTexture(ILcom/google/android/apps/books/view/pages/TexturePlacement;Ljava/lang/String;)V

    .line 568
    const v0, 0x84c1

    const-string v1, "uTransform2"

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpTexture(ILcom/google/android/apps/books/view/pages/TexturePlacement;Ljava/lang/String;)V

    .line 569
    const-string v0, "uFadeToTexture2"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUniformFloat(Ljava/lang/String;F)V

    .line 570
    return-void

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mFlatTwoTextureProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 563
    const-string v0, "uColorBg"

    const/4 v1, 0x0

    aget v1, p6, v1

    const/4 v2, 0x1

    aget v2, p6, v2

    const/4 v3, 0x2

    aget v3, p6, v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUniform3f(Ljava/lang/String;FFF)V

    .line 564
    const-string v0, "uOpacity"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUniformFloat(Ljava/lang/String;F)V

    goto :goto_0
.end method

.method public setZoom(FFFZ)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "twoPageLandscape"    # Z

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 370
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getWidth()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    div-float v0, p2, v0

    sub-float/2addr v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomCenterX:F

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getHeight()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    div-float v0, p3, v0

    sub-float/2addr v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomCenterY:F

    .line 373
    const v0, 0x3c23d70a    # 0.01f

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mZoomScale:F

    .line 374
    iput-boolean p4, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTwoPageLandscape:Z

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->updateZoomMatrix()V

    .line 377
    return-void
.end method

.method public useTextureProgram()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mTexProgram:Lcom/google/android/opengl/carousel/GLProgram;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->useProgram(Lcom/google/android/opengl/carousel/GLProgram;)V

    .line 427
    return-void
.end method
