.class Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "SessionKeySubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;->upgradeKeyOnNetworkThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

.field final synthetic val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iput-object p3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 8
    .param p1, "callbacks"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 178
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    .line 179
    .local v0, "badKey":Lcom/google/android/apps/books/model/SessionKey;
    const/4 v6, 0x1

    new-array v6, v6, [Lcom/google/android/apps/books/model/SessionKey;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 180
    .local v2, "keysToUpgrade":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v6

    invoke-interface {v6, v2}, Lcom/google/android/apps/books/net/BooksServer;->getNewSessionKey(Ljava/util/List;)Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;

    move-result-object v3

    .line 182
    .local v3, "response":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    iget-object v6, v3, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;->oldKeyToNewKey:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/SessionKey;

    .line 183
    .local v4, "upgradedKey":Lcom/google/android/apps/books/model/SessionKey;
    if-eqz v4, :cond_0

    .line 184
    iget-object v6, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v5

    .line 186
    .local v5, "upgradedLocalKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v6, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->handleUpgradedKeyOnControlThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v6, v5, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$500(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    .line 196
    .end local v0    # "badKey":Lcom/google/android/apps/books/model/SessionKey;
    .end local v2    # "keysToUpgrade":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    .end local v3    # "response":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .end local v4    # "upgradedKey":Lcom/google/android/apps/books/model/SessionKey;
    .end local v5    # "upgradedLocalKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :goto_0
    return-void

    .line 188
    .restart local v0    # "badKey":Lcom/google/android/apps/books/model/SessionKey;
    .restart local v2    # "keysToUpgrade":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    .restart local v3    # "response":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .restart local v4    # "upgradedKey":Lcom/google/android/apps/books/model/SessionKey;
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v6, "Server refused to upgrade a key"

    invoke-direct {v1, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 189
    .local v1, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v7, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v7

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v6, v7, v1, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$400(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 191
    .end local v0    # "badKey":Lcom/google/android/apps/books/model/SessionKey;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "keysToUpgrade":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SessionKey;>;"
    .end local v3    # "response":Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .end local v4    # "upgradedKey":Lcom/google/android/apps/books/model/SessionKey;
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v7, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v7

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v6, v7, v1, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$400(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    goto :goto_0

    .line 193
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 194
    .local v1, "e":Ljava/security/GeneralSecurityException;
    iget-object v6, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v7, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;->val$savedKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v7

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v6, v7, v1, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$400(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    goto :goto_0
.end method
