.class Lcom/google/android/apps/books/app/HomeFragment$8;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->maybeStartOffersLoad(Landroid/app/Activity;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$localLibraryAvailable:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Z)V
    .locals 0

    .prologue
    .line 598
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$8;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-boolean p2, p0, Lcom/google/android/apps/books/app/HomeFragment$8;->val$localLibraryAvailable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 4
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 601
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment$8;->val$localLibraryAvailable:Z

    .line 604
    .local v0, "localOnly":Z
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$8;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mOffersProxy:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController;->JUST_FETCH:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/google/android/apps/books/data/BooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 605
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 598
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$8;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
