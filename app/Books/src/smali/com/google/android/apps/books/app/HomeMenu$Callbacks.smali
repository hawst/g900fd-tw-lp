.class public interface abstract Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
.super Ljava/lang/Object;
.source "HomeMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract getAccount()Landroid/accounts/Account;
.end method

.method public abstract getActivity()Landroid/app/Activity;
.end method

.method public abstract getVolumes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onSearchSuggestionSelected(Ljava/lang/String;)V
.end method

.method public abstract startRefresh()V
.end method

.method public abstract startSearch(Ljava/lang/String;)V
.end method
