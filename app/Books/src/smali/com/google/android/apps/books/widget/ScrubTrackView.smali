.class public Lcom/google/android/apps/books/widget/ScrubTrackView;
.super Landroid/view/View;
.source "ScrubTrackView.java"


# static fields
.field private static final ENABLED_NIGHT_STATE:[I

.field private static final ENABLED_STATE:[I

.field private static final NIGHT_STATE:[I


# instance fields
.field private mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

.field private mPosition:I

.field private mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-array v0, v3, [I

    const v1, 0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/books/widget/ScrubTrackView;->ENABLED_STATE:[I

    .line 56
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/widget/ScrubTrackView;->ENABLED_NIGHT_STATE:[I

    .line 59
    new-array v0, v3, [I

    const v1, 0x10100a6

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/books/widget/ScrubTrackView;->NIGHT_STATE:[I

    return-void

    .line 56
    nop

    :array_0
    .array-data 4
        0x101009e
        0x10100a6
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubTrackView;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubTrackView;->init(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubTrackView;->init(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 45
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    invoke-virtual {p1, v1, v2, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 46
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    .line 47
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 63
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 65
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-nez v11, :cond_0

    .line 95
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getHeight()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v9, v11, 0x2

    .line 70
    .local v9, "top":I
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    add-int v3, v9, v11

    .line 72
    .local v3, "bottom":I
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v11}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->getAvailableContentFraction()F

    move-result v0

    .line 73
    .local v0, "availableFraction":F
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getWidth()I

    move-result v10

    .line 74
    .local v10, "width":I
    int-to-float v11, v10

    mul-float/2addr v11, v0

    float-to-int v2, v11

    .line 75
    .local v2, "availableRegionWidth":I
    sub-int v5, v10, v2

    .line 77
    .local v5, "forbiddenRegionWidth":I
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v11}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->isRightToLeft()Z

    move-result v8

    .line 78
    .local v8, "rtl":Z
    if-eqz v8, :cond_1

    sub-int v1, v10, v2

    .line 79
    .local v1, "availableRegionLeft":I
    :goto_1
    if-eqz v8, :cond_2

    .line 82
    .local v4, "forbiddenRegionLeft":I
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getDrawableState()[I

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/apps/books/widget/StateUtil;->hasStateLast([I)Z

    move-result v6

    .line 83
    .local v6, "isNightMode":Z
    if-eqz v6, :cond_3

    sget-object v7, Lcom/google/android/apps/books/widget/ScrubTrackView;->ENABLED_NIGHT_STATE:[I

    .line 84
    .local v7, "newState":[I
    :goto_3
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11, v7}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 85
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    add-int v12, v1, v2

    invoke-virtual {v11, v1, v9, v12, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 87
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 90
    if-eqz v6, :cond_4

    sget-object v7, Lcom/google/android/apps/books/widget/ScrubTrackView;->NIGHT_STATE:[I

    .line 91
    :goto_4
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11, v7}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 92
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    add-int v12, v4, v5

    invoke-virtual {v11, v4, v9, v12, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 94
    iget-object v11, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mGroupWellDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .end local v1    # "availableRegionLeft":I
    .end local v4    # "forbiddenRegionLeft":I
    .end local v6    # "isNightMode":Z
    .end local v7    # "newState":[I
    :cond_1
    move v1, v4

    .line 78
    goto :goto_1

    .restart local v1    # "availableRegionLeft":I
    :cond_2
    move v4, v2

    .line 79
    goto :goto_2

    .line 83
    .restart local v4    # "forbiddenRegionLeft":I
    .restart local v6    # "isNightMode":Z
    :cond_3
    sget-object v7, Lcom/google/android/apps/books/widget/ScrubTrackView;->ENABLED_STATE:[I

    goto :goto_3

    .line 90
    .restart local v7    # "newState":[I
    :cond_4
    sget-object v7, Landroid/view/View;->EMPTY_STATE_SET:[I

    goto :goto_4
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    iget v1, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mPosition:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->getScrubberDescription(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const-string v0, "ScrubTrackView"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "ScrubTrackView"

    const-string v1, "onInitializeAccessibilityEvent called before mScrubberMetadata set"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMetadata(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "metadata":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;, "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata<*>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubTrackView;->invalidate()V

    .line 53
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/google/android/apps/books/widget/ScrubTrackView;->mPosition:I

    .line 99
    return-void
.end method
