.class public Lcom/google/android/apps/books/data/PageContentSubcontroller;
.super Ljava/lang/Object;
.source "PageContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    }
.end annotation


# static fields
.field protected static final UNSAVEABLE:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;


# instance fields
.field private final mCcBoxConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentIdToResult:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;",
            ">;"
        }
    .end annotation
.end field

.field private final mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

.field private final mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/PageContentSubcontroller$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->UNSAVEABLE:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/data/EncryptionScheme;)V
    .locals 1
    .param p1, "encryptionScheme"    # Lcom/google/android/apps/books/data/EncryptionScheme;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 51
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mCcBoxConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 53
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 68
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    .line 71
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p6, "x6"    # Lcom/google/android/apps/books/model/LocalSessionKey;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/model/CcBox;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/CcBox;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->serverCcBoxSupplier(Lcom/google/android/apps/books/model/CcBox;)Lcom/google/common/base/Supplier;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "x4"    # Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .param p5, "x5"    # Z

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p4, "x4"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p5, "x5"    # Ljava/lang/Runnable;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .param p4, "x4"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->localCcBoxSupplier(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/common/base/Supplier;

    move-result-object v0

    return-object v0
.end method

.method private getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 10
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p5, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p6, "saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p7, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p8, "shouldSave"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/Runnable;",
            "Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 238
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;

    iget-object v3, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p7

    move-object v4, p2

    move-object v5, p4

    move-object v6, p3

    move/from16 v7, p8

    move-object/from16 v8, p6

    move-object v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/model/Page;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 282
    return-void
.end method

.method private getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 196
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/PageContentSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 208
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V

    .line 209
    return-void
.end method

.method private getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/Page;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .param p4, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 322
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;

    invoke-direct {v0, p0, p2, p1, p4}, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;)V

    invoke-interface {p1, p3, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 337
    return-void
.end method

.method private getSpecificKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p6, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$3;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/PageContentSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 184
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    invoke-interface {p1, p6, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 185
    return-void
.end method

.method private handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p4, "e"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p5, "continuation"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 296
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$7;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/PageContentSubcontroller$7;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 307
    return-void
.end method

.method private localCcBoxSupplier(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)V

    return-object v0
.end method

.method private onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 10
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Page;",
            "Ljava/lang/Runnable;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p6, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-interface {v0, v1, p3}, Lcom/google/android/apps/books/model/BooksDataStore;->getPageImageFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 227
    .local v6, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p6

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 229
    .end local v6    # "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v9

    .line 224
    .local v9, "e":Ljava/io/IOException;
    invoke-direct {p0, p2, v9}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mCcBoxConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 417
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 402
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$12;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/PageContentSubcontroller$12;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 408
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p4, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p5, "ccBoxConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/CcBox;>;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mCcBoxConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p5, :cond_1

    .line 388
    :cond_0
    iget-object v2, p3, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->ccBox:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/CcBox;

    .line 389
    .local v0, "ccBox":Lcom/google/android/apps/books/model/CcBox;
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mCcBoxConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 390
    invoke-static {p5, v0}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverSuccess(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Object;)V

    .line 393
    .end local v0    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p4, :cond_3

    .line 394
    :cond_2
    iget-object v2, p3, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    iget-object v4, p3, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/data/DataControllerUtils;->buildContentStreamSource(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/data/InputStreamSource;

    move-result-object v1

    .line 396
    .local v1, "contentStreamSource":Lcom/google/android/apps/books/data/InputStreamSource;
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v2, p2, v1, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 398
    .end local v1    # "contentStreamSource":Lcom/google/android/apps/books/data/InputStreamSource;
    :cond_3
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .param p4, "save"    # Z

    .prologue
    const/4 v4, 0x0

    .line 356
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 358
    if-eqz p4, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;

    invoke-direct {v0, p0, p3, p2}, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 382
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, p2, v1, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p4, "result"    # Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .param p5, "shouldSave"    # Z

    .prologue
    .line 312
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$8;

    invoke-direct {v0, p0, p2, p4, p5}, Lcom/google/android/apps/books/data/PageContentSubcontroller$8;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 318
    return-void
.end method

.method private serverCcBoxSupplier(Lcom/google/android/apps/books/model/CcBox;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p1, "ccBox"    # Lcom/google/android/apps/books/model/CcBox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/CcBox;",
            ")",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    new-instance v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$6;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/PageContentSubcontroller$6;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/model/CcBox;)V

    return-object v0
.end method


# virtual methods
.method public getPageContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 19
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p7, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    .local p4, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p5, "ccBoxConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/CcBox;>;>;"
    .local p6, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    if-nez p4, :cond_0

    if-nez p5, :cond_0

    if-eqz p6, :cond_2

    :cond_0
    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkArgument(Z)V

    .line 81
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v3

    if-nez v3, :cond_3

    .line 82
    new-instance v16, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;

    const-string v3, "Page is forbidden"

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;-><init>(Ljava/lang/String;)V

    .line 83
    .local v16, "e":Ljava/lang/Exception;
    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 84
    move-object/from16 v0, p5

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 85
    move-object/from16 v0, p6

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 160
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-void

    .line 78
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 89
    :cond_3
    new-instance v5, Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v3}, Lcom/google/android/apps/books/sync/VolumeContentId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .local v5, "contentId":Lcom/google/android/apps/books/sync/VolumeContentId;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;

    .line 93
    .local v6, "pendingSaveResult":Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    if-eqz v6, :cond_4

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 94
    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 95
    const/16 p4, 0x0

    .line 96
    const/16 p5, 0x0

    .line 97
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p6

    invoke-virtual {v3, v5, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    goto :goto_1

    .line 100
    :cond_4
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getLocalPageState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;

    move-result-object v18

    .line 103
    .local v18, "state":Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->imageIsLocal()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 104
    if-eqz p5, :cond_5

    .line 105
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getCcBox(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/CcBox;

    move-result-object v15

    .line 106
    .local v15, "ccBox":Lcom/google/android/apps/books/model/CcBox;
    if-eqz v15, :cond_6

    .line 107
    invoke-static {v15}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 108
    const/16 p5, 0x0

    .line 115
    .end local v15    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    :cond_5
    :goto_2
    if-nez p4, :cond_7

    if-nez p5, :cond_7

    .line 120
    if-eqz p6, :cond_1

    .line 121
    sget-object v3, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_1

    .line 110
    .restart local v15    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    :cond_6
    const-string v3, "PCSC"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 111
    const-string v3, "PCSC"

    const-string v4, "Database says page is local but can\'t find CC box"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 127
    .end local v15    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    :cond_7
    const/16 v17, 0x0

    .line 128
    .local v17, "hadExistingConsumers":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mImageConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p4

    invoke-virtual {v3, v5, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    :goto_3
    or-int v17, v17, v3

    .line 129
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mCcBoxConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p5

    invoke-virtual {v3, v5, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v3

    if-nez v3, :cond_9

    const/4 v3, 0x1

    :goto_4
    or-int v17, v17, v3

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p6

    invoke-virtual {v3, v5, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v3

    if-nez v3, :cond_a

    const/4 v3, 0x1

    :goto_5
    or-int v17, v17, v3

    .line 131
    if-nez v17, :cond_1

    .line 135
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->imageIsLocal()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 138
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v5, v3, v2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/Page;)V

    goto/16 :goto_1

    .line 128
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    .line 129
    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    .line 130
    :cond_a
    const/4 v3, 0x0

    goto :goto_5

    .line 142
    :cond_b
    new-instance v7, Lcom/google/android/apps/books/data/PageContentSubcontroller$2;

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v5

    move-object/from16 v11, p3

    move-object/from16 v12, p7

    invoke-direct/range {v7 .. v12}, Lcom/google/android/apps/books/data/PageContentSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 150
    .local v7, "expiredKeyContinuation":Ljava/lang/Runnable;
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v14

    .line 153
    .local v14, "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    if-eqz v14, :cond_c

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v5

    move-object/from16 v11, p3

    move-object v12, v7

    move-object/from16 v13, p7

    .line 154
    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->getSpecificKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/SessionKeyId;)V

    goto/16 :goto_1

    :cond_c
    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v5

    move-object/from16 v11, p3

    move-object v12, v7

    move-object/from16 v13, p7

    .line 159
    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto/16 :goto_1
.end method
