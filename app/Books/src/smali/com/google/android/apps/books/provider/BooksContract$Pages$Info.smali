.class public Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
.super Ljava/lang/Object;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract$Pages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# instance fields
.field public final accountName:Ljava/lang/String;

.field public final pageId:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "acct"    # Ljava/lang/String;
    .param p2, "vol"    # Ljava/lang/String;
    .param p3, "page"    # Ljava/lang/String;

    .prologue
    .line 2863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2864
    iput-object p1, p0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->accountName:Ljava/lang/String;

    .line 2865
    iput-object p2, p0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->volumeId:Ljava/lang/String;

    .line 2866
    iput-object p3, p0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->pageId:Ljava/lang/String;

    .line 2867
    return-void
.end method
