.class public interface abstract Lcom/google/android/apps/books/model/BooksDataListener;
.super Ljava/lang/Object;
.source "BooksDataListener.java"


# virtual methods
.method public abstract onDismissedRecommendations(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onLocalVolumeData(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onMyEbooksVolumes(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onNewResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onNewSegmentResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onNewUpload(Lcom/google/android/apps/books/upload/Upload;)V
.end method

.method public abstract onOffersData(Ljava/util/List;ZJ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;ZJ)V"
        }
    .end annotation
.end method

.method public abstract onRecentSearchesChanged(Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onRequestedDictionaryLanguages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onUploadDeleted(Ljava/lang/String;)V
.end method

.method public abstract onUploadProgressUpdate(Ljava/lang/String;I)V
.end method

.method public abstract onUploadStatusUpdate(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V
.end method

.method public abstract onUploadTooLarge()V
.end method

.method public abstract onUploadVolumeIdUpdate(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onUploads(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
.end method

.method public abstract onVolumeData(Lcom/google/android/apps/books/model/VolumeData;)V
.end method

.method public abstract onVolumeDownloadProgress(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onVolumeManifest(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ")V"
        }
    .end annotation
.end method
