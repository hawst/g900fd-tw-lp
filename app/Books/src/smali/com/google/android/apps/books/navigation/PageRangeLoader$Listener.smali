.class public interface abstract Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;
.super Ljava/lang/Object;
.source "PageRangeLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/PageRangeLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract getPageSizes(Lcom/google/android/apps/books/render/PageHandle;Landroid/graphics/Point;Landroid/graphics/Point;)V
.end method

.method public abstract isDestroyed()Z
.end method

.method public abstract spreadRangeChanged()V
.end method
