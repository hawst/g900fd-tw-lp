.class Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$CubicDecelerateInterpolator;
.super Ljava/lang/Object;
.source "ShrinkToIconAnimationFactory.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CubicDecelerateInterpolator"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$1;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$CubicDecelerateInterpolator;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3
    .param p1, "input"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 25
    sub-float v0, v2, p1

    .line 26
    .local v0, "inv":F
    mul-float v1, v0, v0

    mul-float/2addr v1, v0

    sub-float v1, v2, v1

    return v1
.end method
