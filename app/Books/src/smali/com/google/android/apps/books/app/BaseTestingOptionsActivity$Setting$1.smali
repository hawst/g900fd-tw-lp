.class Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;
.super Ljava/lang/Object;
.source "BaseTestingOptionsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->createValidView(Landroid/app/Activity;Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

.field final synthetic val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;->this$0:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    iput-object p2, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;->val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "av":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;->val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;->this$0:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    # getter for: Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    invoke-static {v1}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->access$000(Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;)[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    move-result-object v1

    aget-object v1, v1, p3

    iget-object v1, v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;->values:[Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;->onOptionValuesChanged([Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
