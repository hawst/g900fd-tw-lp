.class public Lcom/google/android/apps/books/net/BooksCookieStore;
.super Lorg/apache/http/impl/client/BasicCookieStore;
.source "BooksCookieStore.java"


# instance fields
.field private final mSaveFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0
    .param p1, "saveFile"    # Ljava/io/File;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/http/impl/client/BasicCookieStore;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->load()V

    .line 46
    return-void
.end method

.method private createCookie(Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;)Lorg/apache/http/impl/cookie/BasicClientCookie;
    .locals 6
    .param p1, "protoCookie"    # Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;

    .prologue
    .line 250
    new-instance v0, Lorg/apache/http/impl/cookie/BasicClientCookie;

    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/impl/cookie/BasicClientCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    .local v0, "cookie":Lorg/apache/http/impl/cookie/BasicClientCookie;
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->hasComment()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 253
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getComment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setComment(Ljava/lang/String;)V

    .line 256
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->hasCommentUrl()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 263
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->hasExpiryDate()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 264
    new-instance v1, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getExpiryDate()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 265
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v0, v1}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setExpiryDate(Ljava/util/Date;)V

    .line 268
    .end local v1    # "date":Ljava/util/Date;
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->hasDomain()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 269
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getDomain()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setDomain(Ljava/lang/String;)V

    .line 272
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getPortsCount()I

    move-result v3

    .line 273
    .local v3, "numPorts":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 277
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getIsSecure()Z

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setSecure(Z)V

    .line 278
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->getVersion()I

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setVersion(I)V

    .line 280
    return-object v0
.end method

.method private createProtoCookie(Lorg/apache/http/cookie/Cookie;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;
    .locals 10
    .param p1, "cookie"    # Lorg/apache/http/cookie/Cookie;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-static {}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;->newBuilder()Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    move-result-object v1

    .line 193
    .local v1, "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v7

    .line 194
    .local v7, "val":Ljava/lang/String;
    if-eqz v7, :cond_6

    .line 195
    invoke-virtual {v1, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setName(Ljava/lang/String;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 200
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 201
    if-eqz v7, :cond_0

    .line 202
    invoke-virtual {v1, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setValue(Ljava/lang/String;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 205
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getComment()Ljava/lang/String;

    move-result-object v7

    .line 206
    if-eqz v7, :cond_1

    .line 207
    invoke-virtual {v1, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setComment(Ljava/lang/String;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 210
    :cond_1
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getCommentURL()Ljava/lang/String;

    move-result-object v7

    .line 211
    if-eqz v7, :cond_2

    .line 212
    invoke-virtual {v1, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setCommentUrl(Ljava/lang/String;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 215
    :cond_2
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v2

    .line 216
    .local v2, "date":Ljava/util/Date;
    if-eqz v2, :cond_3

    .line 217
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setExpiryDate(J)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 220
    :cond_3
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v7

    .line 221
    if-eqz v7, :cond_4

    .line 222
    invoke-virtual {v1, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setDomain(Ljava/lang/String;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 225
    :cond_4
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 226
    if-eqz v7, :cond_5

    .line 227
    invoke-virtual {v1, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setPath(Ljava/lang/String;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 230
    :cond_5
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getPorts()[I

    move-result-object v6

    .line 231
    .local v6, "ports":[I
    if-eqz v6, :cond_7

    .line 232
    move-object v0, v6

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_7

    aget v5, v0, v3

    .line 233
    .local v5, "port":I
    aget v8, v6, v5

    invoke-virtual {v1, v8}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->addPorts(I)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 232
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 197
    .end local v0    # "arr$":[I
    .end local v2    # "date":Ljava/util/Date;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "port":I
    .end local v6    # "ports":[I
    :cond_6
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cookie name is null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 237
    .restart local v2    # "date":Ljava/util/Date;
    .restart local v6    # "ports":[I
    :cond_7
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->isSecure()Z

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setIsSecure(Z)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 238
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getVersion()I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;->setVersion(I)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    .line 240
    return-object v1
.end method

.method private saveIfChanged(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p1, "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .local p2, "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-interface {p1, p2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->save()V

    .line 287
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized addCookie(Lorg/apache/http/cookie/Cookie;)V
    .locals 3
    .param p1, "cookie"    # Lorg/apache/http/cookie/Cookie;

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 62
    .local v1, "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/BasicCookieStore;->addCookie(Lorg/apache/http/cookie/Cookie;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v0

    .line 64
    .local v0, "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/books/net/BooksCookieStore;->saveIfChanged(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    monitor-exit p0

    return-void

    .line 61
    .end local v0    # "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .end local v1    # "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized addCookies([Lorg/apache/http/cookie/Cookie;)V
    .locals 7
    .param p1, "cookies"    # [Lorg/apache/http/cookie/Cookie;

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 82
    .local v2, "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    if-eqz p1, :cond_0

    .line 83
    move-object v1, p1

    .local v1, "arr$":[Lorg/apache/http/cookie/Cookie;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v3, v1, v4

    .line 84
    .local v3, "cookie":Lorg/apache/http/cookie/Cookie;
    invoke-super {p0, v3}, Lorg/apache/http/impl/client/BasicCookieStore;->addCookie(Lorg/apache/http/cookie/Cookie;)V

    .line 83
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "arr$":[Lorg/apache/http/cookie/Cookie;
    .end local v3    # "cookie":Lorg/apache/http/cookie/Cookie;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v0

    .line 88
    .local v0, "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/books/net/BooksCookieStore;->saveIfChanged(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    .line 81
    .end local v0    # "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .end local v2    # "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized clear()V
    .locals 3

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 116
    .local v1, "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-super {p0}, Lorg/apache/http/impl/client/BasicCookieStore;->clear()V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v0

    .line 118
    .local v0, "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/books/net/BooksCookieStore;->saveIfChanged(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 115
    .end local v0    # "after":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .end local v1    # "before":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized clearExpired(Ljava/util/Date;)Z
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/BasicCookieStore;->clearExpired(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->save()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    const/4 v0, 0x1

    .line 107
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized load()V
    .locals 11

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;->newBuilder()Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 160
    .local v0, "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    :try_start_1
    new-instance v7, Ljava/io/FileInputStream;

    iget-object v8, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    invoke-direct {v7, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 161
    .local v7, "stream":Ljava/io/FileInputStream;
    invoke-virtual {v0, v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    .line 162
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    .end local v7    # "stream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;->build()Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;

    move-result-object v6

    .line 174
    .local v6, "store":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;
    invoke-virtual {v6}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;->getCookiesCount()I

    move-result v5

    .line 175
    .local v5, "numCookies":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_1

    .line 176
    invoke-virtual {v6, v3}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;->getCookies(I)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/google/android/apps/books/net/BooksCookieStore;->createCookie(Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie;)Lorg/apache/http/impl/cookie/BasicClientCookie;

    move-result-object v1

    .line 177
    .local v1, "cookie":Lorg/apache/http/cookie/Cookie;
    invoke-super {p0, v1}, Lorg/apache/http/impl/client/BasicCookieStore;->addCookie(Lorg/apache/http/cookie/Cookie;)V

    .line 175
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 163
    .end local v1    # "cookie":Lorg/apache/http/cookie/Cookie;
    .end local v3    # "i":I
    .end local v5    # "numCookies":I
    .end local v6    # "store":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;
    :catch_0
    move-exception v2

    .line 164
    .local v2, "fileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v8, "BooksCookieStore"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 165
    const-string v8, "BooksCookieStore"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cookie store file not found: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 158
    .end local v0    # "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    .end local v2    # "fileNotFoundException":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 167
    .restart local v0    # "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    :catch_1
    move-exception v4

    .line 168
    .local v4, "ioException":Ljava/io/IOException;
    :try_start_3
    const-string v8, "BooksCookieStore"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 169
    const-string v8, "BooksCookieStore"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cookie store IO exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 179
    .end local v4    # "ioException":Ljava/io/IOException;
    .restart local v3    # "i":I
    .restart local v5    # "numCookies":I
    .restart local v6    # "store":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected declared-synchronized save()V
    .locals 12

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/BooksCookieStore;->getCookies()Ljava/util/List;

    move-result-object v2

    .line 126
    .local v2, "cookies":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-static {}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;->newBuilder()Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;

    move-result-object v0

    .line 127
    .local v0, "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/cookie/Cookie;

    .line 128
    .local v1, "cookie":Lorg/apache/http/cookie/Cookie;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/net/BooksCookieStore;->createProtoCookie(Lorg/apache/http/cookie/Cookie;)Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;

    move-result-object v6

    .line 129
    .local v6, "protoCookie":Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;
    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;->addCookies(Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;)Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 125
    .end local v0    # "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    .end local v1    # "cookie":Lorg/apache/http/cookie/Cookie;
    .end local v2    # "cookies":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "protoCookie":Lcom/google/android/apps/books/net/proto/BooksCookies$Cookie$Builder;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 131
    .restart local v0    # "builder":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;
    .restart local v2    # "cookies":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore$Builder;->build()Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 134
    .local v7, "store":Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;
    :try_start_2
    new-instance v8, Ljava/io/FileOutputStream;

    iget-object v9, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 135
    .local v8, "stream":Ljava/io/FileOutputStream;
    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;->writeTo(Ljava/io/OutputStream;)V

    .line 136
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V

    .line 137
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 139
    const-string v9, "BooksCookieStore"

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 140
    const-string v9, "BooksCookieStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cookie store file written: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const-string v9, "BooksCookieStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "File had "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/google/android/apps/books/net/proto/BooksCookies$CookieStore;->getCookiesCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " cookies"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 152
    .end local v8    # "stream":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 143
    :catch_0
    move-exception v3

    .line 144
    .local v3, "fileNotFoundException":Ljava/io/FileNotFoundException;
    :try_start_3
    const-string v9, "BooksCookieStore"

    const/4 v10, 0x6

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 145
    const-string v9, "BooksCookieStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cookie store file not found: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 147
    .end local v3    # "fileNotFoundException":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v5

    .line 148
    .local v5, "ioException":Ljava/io/IOException;
    const-string v9, "BooksCookieStore"

    const/4 v10, 0x6

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 149
    const-string v9, "BooksCookieStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cookie store IO exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/books/net/BooksCookieStore;->mSaveFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
