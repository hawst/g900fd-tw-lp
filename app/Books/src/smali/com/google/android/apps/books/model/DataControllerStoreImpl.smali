.class public Lcom/google/android/apps/books/model/DataControllerStoreImpl;
.super Ljava/lang/Object;
.source "DataControllerStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/DataControllerStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;
    }
.end annotation


# static fields
.field private static final DICTIONARY_METADATA_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# instance fields
.field private final mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private final mLocalPreferences:Lcom/google/android/apps/books/preference/LocalPreferences;

.field private final mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "language_code"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "version"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "encrypted_dict_key"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "url"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "size"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "session_key_version"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "account_name"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->DICTIONARY_METADATA_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/provider/VolumeContentStore;Landroid/content/Context;)V
    .locals 1
    .param p1, "volumeContentStore"    # Lcom/google/android/apps/books/provider/VolumeContentStore;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/model/DataControllerStoreImpl$MetadataDatabaseHelper;-><init>(Lcom/google/android/apps/books/model/DataControllerStoreImpl;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 85
    iput-object p1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    .line 86
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mLocalPreferences:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/DataControllerStoreImpl;)Lcom/google/android/apps/books/provider/VolumeContentStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/DataControllerStoreImpl;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    return-object v0
.end method

.method private getAllMetadata()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 188
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v11

    .line 189
    .local v11, "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    sget-object v0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->DICTIONARY_METADATA_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v12, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "dictionary_metadata"

    const-string v7, "language_code ASC, version DESC "

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v10

    .line 194
    .local v10, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :goto_0
    :try_start_0
    invoke-virtual {v10}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "language_code"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "languageCode":Ljava/lang/String;
    const-string v0, "version"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 197
    .local v2, "version":J
    const-string v0, "encrypted_dict_key"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBlob(Ljava/lang/String;)[B

    move-result-object v4

    .line 199
    .local v4, "encryptedDictionaryKey":[B
    const-string v0, "size"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 200
    .local v6, "size":J
    const-string v0, "url"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 201
    .local v5, "url":Ljava/lang/String;
    const-string v0, "session_key_version"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 203
    .local v8, "sessionKeyVersion":Ljava/lang/String;
    const-string v0, "account_name"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 204
    .local v9, "accountName":Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;-><init>(Ljava/lang/String;J[BLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 208
    .end local v1    # "languageCode":Ljava/lang/String;
    .end local v2    # "version":J
    .end local v4    # "encryptedDictionaryKey":[B
    .end local v5    # "url":Ljava/lang/String;
    .end local v6    # "size":J
    .end local v8    # "sessionKeyVersion":Ljava/lang/String;
    .end local v9    # "accountName":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-virtual {v10}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v0

    :cond_0
    invoke-virtual {v10}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 210
    return-object v11
.end method

.method private getRequestedLanguages()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v2, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "select language_code from dictionary_preference where download_requested=1 order by language_code"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 157
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 159
    .local v1, "requestedLanguages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 165
    return-object v1
.end method

.method private removeDataAssociatedWithUserSessionKey()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->getLocalDictionaryMetadataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 334
    .local v1, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    goto :goto_0

    .line 336
    .end local v1    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_0
    return-void
.end method


# virtual methods
.method public clearLastDictionaryMetadataSyncTime()V
    .locals 4

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mLocalPreferences:Lcom/google/android/apps/books/preference/LocalPreferences;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->setLastDictionaryMetadataSync(J)V

    .line 328
    return-void
.end method

.method public deleteDictionaryMetadataForAccount(Landroid/accounts/Account;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "dictionary_metadata"

    const-string v2, "account_name=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 323
    return-void
.end method

.method public getDownloadedDictionaryFile(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Ljava/io/File;
    .locals 3
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 311
    iget-object v1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionaryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v0

    .line 313
    .local v0, "result":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 316
    .end local v0    # "result":Ljava/io/File;
    :goto_0
    return-object v0

    .restart local v0    # "result":Ljava/io/File;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocalDictionaryMetadataList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->getAllMetadata()Ljava/util/List;

    move-result-object v2

    .line 92
    .local v2, "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 111
    .end local v2    # "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :goto_0
    return-object v2

    .line 97
    .restart local v2    # "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 98
    .local v4, "returnList":Ljava/util/List;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 99
    .local v0, "added":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 100
    .local v3, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionaryName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 103
    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    goto :goto_1

    .end local v3    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_2
    move-object v2, v4

    .line 111
    goto :goto_0
.end method

.method public getRequestedDictionaryMetadataList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->getServerDictionaryMetadataList()Ljava/util/List;

    move-result-object v4

    .line 138
    .local v4, "serverDictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 148
    .end local v4    # "serverDictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :goto_0
    return-object v4

    .line 141
    .restart local v4    # "serverDictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 142
    .local v3, "returnList":Ljava/util/List;
    invoke-direct {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->getRequestedLanguages()Ljava/util/Set;

    move-result-object v2

    .line 143
    .local v2, "requestedLanguages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 144
    .local v1, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 145
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v1    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_2
    move-object v4, v3

    .line 148
    goto :goto_0
.end method

.method public getServerDictionaryMetadataList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->getAllMetadata()Ljava/util/List;

    move-result-object v2

    .line 117
    .local v2, "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 132
    .end local v2    # "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :goto_0
    return-object v2

    .line 121
    .restart local v2    # "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 122
    .local v4, "returnList":Ljava/util/List;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 123
    .local v0, "added":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 124
    .local v3, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 125
    invoke-virtual {v3}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    goto :goto_1

    .end local v3    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_2
    move-object v2, v4

    .line 132
    goto :goto_0
.end method

.method public getUserSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_session_key"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 270
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    const-string v0, "DataCtlDB"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "DataCtlDB"

    const-string v1, ">1 session key rows for user "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    invoke-static {v8}, Lcom/google/android/apps/books/model/UserSessionKeyId;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/LocalSessionKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 277
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 279
    :goto_0
    return-object v2

    .line 277
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 5
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    const/4 v4, 0x0

    .line 246
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 247
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "language_code"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v1, "version"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getVersion()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 249
    const-string v1, "encrypted_dict_key"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getEncryptedDictionaryKey()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 251
    const-string v1, "url"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDownloadUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v1, "size"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionarySizeInBytes()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 253
    const-string v1, "session_key_version"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getSessionKeyVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v1, "account_name"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "dictionary_metadata"

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v4, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 258
    new-instance v0, Landroid/content/ContentValues;

    .end local v0    # "values":Landroid/content/ContentValues;
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 259
    .restart local v0    # "values":Landroid/content/ContentValues;
    const-string v1, "language_code"

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v1, "download_requested"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 261
    iget-object v1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "dictionary_preference"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v4, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 263
    return-void
.end method

.method public removeDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 8
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "dictionary_metadata"

    const-string v2, "language_code=? AND version=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getVersion()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 228
    return-void
.end method

.method public removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 3
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 215
    iget-object v1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionaryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v0

    .line 217
    .local v0, "dictionary":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    invoke-static {v0}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    .line 220
    :cond_0
    return-void
.end method

.method public removeUserSessionKey()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_session_key"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 305
    invoke-direct {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->removeDataAssociatedWithUserSessionKey()V

    .line 306
    return-void
.end method

.method public revokeDictionaryLanguage(Ljava/lang/String;)V
    .locals 8
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->getLocalDictionaryMetadataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 233
    .local v1, "m":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 238
    .end local v1    # "m":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "dictionary_metadata"

    const-string v4, "language_code=?"

    new-array v5, v7, [Ljava/lang/String;

    aput-object p1, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 240
    iget-object v2, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "dictionary_preference"

    const-string v4, "language_code=?"

    new-array v5, v7, [Ljava/lang/String;

    aput-object p1, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 242
    return-void
.end method

.method public saveUserSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 4
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 285
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "DataCtlDB"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    const-string v1, "DataCtlDB"

    const-string v2, "UserSessionKeyId inserting new row"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_0
    invoke-static {p1, v0}, Lcom/google/android/apps/books/model/UserSessionKeyId;->setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V

    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->removeUserSessionKey()V

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "user_session_key"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 291
    invoke-static {}, Lcom/google/android/apps/books/model/UserSessionKeyId;->get()Lcom/google/android/apps/books/model/UserSessionKeyId;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v1

    return-object v1
.end method

.method public setRequestedDictionaryLanguages(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 170
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 171
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "download_requested"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 172
    iget-object v3, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "dictionary_preference"

    invoke-virtual {v3, v4, v2, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 174
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 175
    .local v1, "language":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    .end local v2    # "values":Landroid/content/ContentValues;
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 176
    .restart local v2    # "values":Landroid/content/ContentValues;
    const-string v3, "language_code"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v3, "download_requested"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 178
    iget-object v3, p0, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->mDatabaseHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "dictionary_preference"

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v6, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_0

    .line 182
    .end local v1    # "language":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateUserSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/UserSessionKeyId;>;"
    const-string v0, "DataCtlDB"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const-string v0, "DataCtlDB"

    const-string v1, "Updating user session key"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/DataControllerStoreImpl;->saveUserSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 300
    return-void
.end method
