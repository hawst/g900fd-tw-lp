.class public Lcom/google/android/apps/books/annotations/UnoptimizedAnnotationListener;
.super Lcom/google/android/apps/books/annotations/StubAnnotationListener;
.source "UnoptimizedAnnotationListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/StubAnnotationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public annotationAdded(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p3, "newFullSet":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-static {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/annotations/UnoptimizedAnnotationListener;->annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V

    .line 17
    return-void
.end method
