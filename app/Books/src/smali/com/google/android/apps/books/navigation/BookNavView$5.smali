.class Lcom/google/android/apps/books/navigation/BookNavView$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "BookNavView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/BookNavView;->snapback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$5;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x0

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$5;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1100(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookScrollController;

    move-result-object v0

    if-nez v0, :cond_0

    .line 453
    :goto_0
    return-void

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$5;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1600(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookGridView;->clear()V

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$5;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setStasis(Z)V

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$5;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1100(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookScrollController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->setPaused(Z)V

    goto :goto_0
.end method
