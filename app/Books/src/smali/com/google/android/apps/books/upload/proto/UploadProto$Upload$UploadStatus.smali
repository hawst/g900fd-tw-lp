.class public final enum Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
.super Ljava/lang/Enum;
.source "UploadProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UploadStatus"
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum INITIALIZED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SCOTTY_ACTIVE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SCOTTY_COMPLETED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SCOTTY_PAUSED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SERVER_COMPLETE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SERVER_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field public static final enum SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 30
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->INITIALIZED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 31
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SCOTTY_PAUSED"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_PAUSED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 32
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SCOTTY_ACTIVE"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ACTIVE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 33
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SCOTTY_COMPLETED"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_COMPLETED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 34
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SCOTTY_ERROR"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 35
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SCOTTY_CANCELED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 36
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SERVER_PROCESSING"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 37
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SERVER_COMPLETE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_COMPLETE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 38
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    const-string v1, "SERVER_ERROR"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 28
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->INITIALIZED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_PAUSED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ACTIVE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_COMPLETED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_COMPLETE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->$VALUES:[Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 64
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "index"    # I
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->index:I

    .line 75
    iput p4, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->value:I

    .line 76
    return-void
.end method

.method public static valueOf(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 45
    packed-switch p0, :pswitch_data_0

    .line 55
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 46
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->INITIALIZED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 47
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_PAUSED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 48
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ACTIVE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 49
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_COMPLETED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 50
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 51
    :pswitch_5
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 52
    :pswitch_6
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 53
    :pswitch_7
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_COMPLETE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 54
    :pswitch_8
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->$VALUES:[Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->value:I

    return v0
.end method
