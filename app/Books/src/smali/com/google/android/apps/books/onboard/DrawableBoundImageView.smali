.class public Lcom/google/android/apps/books/onboard/DrawableBoundImageView;
.super Landroid/widget/ImageView;
.source "DrawableBoundImageView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private mBindDrawableKey:Ljava/lang/Integer;

.field private final mBoundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

.field private mCurrentDrawable:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v1, Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-direct {v1, p1, p2, p0}, Lcom/google/android/libraries/bind/data/BoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mBoundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    .line 38
    sget-object v1, Lcom/google/android/libraries/bind/R$styleable;->BoundImageView:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 41
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mBindDrawableKey:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mBoundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mBindDrawableKey:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 51
    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    check-cast v1, Landroid/graphics/Bitmap;

    move-object v0, v1

    check-cast v0, Landroid/graphics/Bitmap;

    .line 52
    .local v0, "drawable":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mCurrentDrawable:Landroid/graphics/Bitmap;

    invoke-static {v1, v0}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    iput-object v0, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mCurrentDrawable:Landroid/graphics/Bitmap;

    .line 54
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 57
    .end local v0    # "drawable":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/DrawableBoundImageView;->mBindDrawableKey:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method
