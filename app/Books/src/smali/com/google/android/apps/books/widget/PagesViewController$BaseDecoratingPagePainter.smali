.class Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;
.super Lcom/google/android/apps/books/widget/DecoratingPagePainter;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BaseDecoratingPagePainter"
.end annotation


# instance fields
.field private final mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field private final mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/Logger;)V
    .locals 1
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p5, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 2971
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    .line 2972
    const/4 v0, 0x1

    invoke-direct {p0, p4, v0, p5}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;-><init>(Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/util/Logger;)V

    .line 2973
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 2974
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 2975
    return-void
.end method


# virtual methods
.method protected decorate(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2979
    const-string v0, "PagesViewHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2980
    const-string v0, "PagesViewHelper"

    const-string v1, "Painting base decorations"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2982
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3600(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 2983
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempSize:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3600(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->decoratePage(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)V
    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3700(Lcom/google/android/apps/books/widget/PagesViewController;Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)V

    .line 2984
    return-void
.end method
