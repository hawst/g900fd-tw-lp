.class Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->setupButtons(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

.field final synthetic val$mainLayout:Landroid/view/View;

.field final synthetic val$offerResultConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$spinner:Landroid/view/View;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;Landroid/view/View;Landroid/view/View;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$mainLayout:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$spinner:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$volumeId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$offerResultConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/OffersFragment;->isDeviceConnectedElseToast(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$mainLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$spinner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 466
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACCEPTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->this$0:Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;->access$1100(Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog$2;->val$offerResultConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v2}, Lcom/google/android/apps/books/util/UiThreadConsumer;->proxyTo(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/apps/books/util/UiThreadConsumer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 470
    :cond_0
    return-void
.end method
