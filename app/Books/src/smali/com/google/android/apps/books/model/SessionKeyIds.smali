.class public Lcom/google/android/apps/books/model/SessionKeyIds;
.super Ljava/lang/Object;
.source "SessionKeyIds.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/SessionKeyIds$1;
    }
.end annotation


# direct methods
.method public static addToContentValues(Lcom/google/android/apps/books/model/SessionKeyId;Landroid/content/ContentValues;)V
    .locals 2
    .param p0, "identifier"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const-string v0, "session_key_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 27
    const-string v0, "storage_format"

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->UNENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    invoke-virtual {v1}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->getDatabaseValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-interface {p0, p1}, Lcom/google/android/apps/books/model/SessionKeyId;->addToContentValues(Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method public static keyIdFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;Ljava/lang/Long;)Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 4
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "storageFormat"    # Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    .param p2, "sessionKeyId"    # Ljava/lang/Long;

    .prologue
    const/4 v0, 0x0

    .line 39
    sget-object v1, Lcom/google/android/apps/books/model/SessionKeyIds$1;->$SwitchMap$com$google$android$apps$books$provider$BooksContract$StorageFormat:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 54
    :cond_0
    :goto_0
    return-object v0

    .line 41
    :pswitch_0
    if-eqz p2, :cond_1

    .line 42
    new-instance v0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p0}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;-><init>(JLjava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_1
    const-string v1, "SessionKeyIds"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    const-string v1, "SessionKeyIds"

    const-string v2, "Missing session key ID"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50
    :pswitch_1
    invoke-static {}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->get()Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v0

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
