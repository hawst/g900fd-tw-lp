.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNoteAtCharOffset(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

.field final synthetic val$makeEditable:Z

.field final synthetic val$res:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/content/res/Resources;Z)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iput-object p2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->val$res:Landroid/content/res/Resources;

    iput-boolean p3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->val$makeEditable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 510
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 527
    :goto_0
    return-void

    .line 515
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 516
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->val$res:Landroid/content/res/Resources;

    const v3, 0x7f090125

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 518
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 521
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$900(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3$1;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;)V

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
