.class public Lcom/google/android/apps/books/model/VolumeSessionKeyId;
.super Ljava/lang/Object;
.source "VolumeSessionKeyId.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SessionKeyId;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/VolumeSessionKeyId$Query;
    }
.end annotation


# instance fields
.field private final mRowId:J

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1
    .param p1, "sessionKeyId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mRowId:J

    .line 25
    iput-object p3, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mVolumeId:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static keyFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/SessionKey;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/apps/books/model/SessionKey;

    const/4 v1, 0x4

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/SessionKey;-><init>(ILjava/lang/String;[B)V

    return-object v0
.end method

.method public static setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V
    .locals 2
    .param p0, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 91
    const-string v0, "session_key_version"

    iget-object v1, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v0, "root_key_version"

    iget v1, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    const-string v0, "session_key_blob"

    iget-object v1, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 94
    return-void
.end method


# virtual methods
.method public addToContentValues(Landroid/content/ContentValues;)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 52
    const-string v0, "storage_format"

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->VOLUME_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    invoke-virtual {v1}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->getDatabaseValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 54
    const-string v0, "session_key_id"

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->getRowId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 55
    return-void
.end method

.method public deleteKeyAndEncryptedContent(Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 0
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 59
    invoke-interface {p1, p0}, Lcom/google/android/apps/books/model/BooksDataStore;->removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/VolumeSessionKeyId;)V

    .line 60
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 98
    instance-of v2, p1, Lcom/google/android/apps/books/model/VolumeSessionKeyId;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 99
    check-cast v0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;

    .line 100
    .local v0, "o":Lcom/google/android/apps/books/model/VolumeSessionKeyId;
    iget-wide v2, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mRowId:J

    iget-wide v4, v0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mRowId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mVolumeId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mVolumeId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 102
    .end local v0    # "o":Lcom/google/android/apps/books/model/VolumeSessionKeyId;
    :cond_0
    return v1
.end method

.method public getRowId()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mRowId:J

    return-wide v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 107
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mRowId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->mVolumeId:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public load(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/SessionKey;
    .locals 2
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 30
    invoke-interface {p1, p0}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolumeSessionKey(Lcom/google/android/apps/books/model/VolumeSessionKeyId;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    .line 31
    .local v0, "localKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/VolumeSessionKeyId;>;"
    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v1

    .line 34
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public update(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p2, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 47
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->updateVolumeSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 48
    return-void
.end method
