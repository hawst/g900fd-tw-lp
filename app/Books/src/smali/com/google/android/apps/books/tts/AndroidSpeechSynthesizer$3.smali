.class Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "AndroidSpeechSynthesizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->attachUtteranceListenerIcsMR1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 1
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    # invokes: Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->onUtteranceCompleted(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->access$300(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 3
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 295
    const-string v0, "AndroidTTS"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "AndroidTTS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TextToSpeech error for phrase "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_0
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 281
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3$1;-><init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$3;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 291
    return-void
.end method
