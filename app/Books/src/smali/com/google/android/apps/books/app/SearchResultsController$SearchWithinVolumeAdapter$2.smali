.class Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;
.super Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
.source "SearchResultsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->createFoundItem(Lcom/google/android/apps/books/model/SearchResult;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

.field final synthetic val$searchResult:Lcom/google/android/apps/books/model/SearchResult;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;IILcom/google/android/apps/books/model/SearchResult;)V
    .locals 1
    .param p2, "x0"    # I
    .param p3, "x1"    # I

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;->this$0:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    iput-object p4, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;->val$searchResult:Lcom/google/android/apps/books/model/SearchResult;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;-><init>(IILcom/google/android/apps/books/app/SearchResultsController$1;)V

    return-void
.end method


# virtual methods
.method public getSearchResultMatchStart()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;->val$searchResult:Lcom/google/android/apps/books/model/SearchResult;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/SearchResult;->getAnchorLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    return-object v0
.end method

.method protected populateView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;->this$0:Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;->val$searchResult:Lcom/google/android/apps/books/model/SearchResult;

    # invokes: Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->populateFoundView(Lcom/google/android/apps/books/model/SearchResult;Landroid/view/View;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->access$500(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;Lcom/google/android/apps/books/model/SearchResult;Landroid/view/View;)V

    .line 170
    return-void
.end method
