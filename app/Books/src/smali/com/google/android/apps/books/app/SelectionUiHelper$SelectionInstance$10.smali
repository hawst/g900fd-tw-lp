.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;
.super Lcom/google/android/ublib/utils/StubViewAnimationListener;
.source "SelectionUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->animateNoteIn(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

.field final synthetic val$makeEditable:Z

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 699
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iput-boolean p2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;->val$makeEditable:Z

    iput-object p3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;->val$view:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/ublib/utils/StubViewAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$900(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10$1;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 721
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 702
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;->val$makeEditable:Z

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->activateNoteEditor()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1100(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    .line 705
    :cond_0
    return-void
.end method
