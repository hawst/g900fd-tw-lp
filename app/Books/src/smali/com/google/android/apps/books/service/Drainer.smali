.class public Lcom/google/android/apps/books/service/Drainer;
.super Ljava/lang/Object;
.source "Drainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;
    }
.end annotation


# instance fields
.field private final mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/service/Drainer;->mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/service/Drainer;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/Drainer;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/books/service/Drainer;->mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method public abortAndDrain()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/ExecutionException;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v3, p0, Lcom/google/android/apps/books/service/Drainer;->mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 80
    .local v3, "futures":Ljava/util/concurrent/ConcurrentLinkedQueue;, "Ljava/util/concurrent/ConcurrentLinkedQueue<Ljava/util/concurrent/Future<*>;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 81
    .local v1, "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .local v2, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    :goto_0
    if-eqz v2, :cond_1

    .line 82
    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 84
    :try_start_0
    invoke-interface {v2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 81
    :cond_0
    :goto_1
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    check-cast v2, Ljava/util/concurrent/Future;

    .restart local v2    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v4, "Drainer"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    const-string v4, "Drainer"

    const-string v5, "Unexpected interuption"

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 89
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 90
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 94
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_1
    return-object v1
.end method

.method public drain()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/ExecutionException;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v3, p0, Lcom/google/android/apps/books/service/Drainer;->mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 63
    .local v3, "futures":Ljava/util/concurrent/ConcurrentLinkedQueue;, "Ljava/util/concurrent/ConcurrentLinkedQueue<Ljava/util/concurrent/Future<*>;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 64
    .local v1, "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .local v2, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    :goto_0
    if-eqz v2, :cond_0

    .line 66
    :try_start_0
    invoke-interface {v2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_1
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    check-cast v2, Ljava/util/concurrent/Future;

    .restart local v2    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 71
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :cond_0
    return-object v1
.end method

.method public managed(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;
    .locals 1
    .param p1, "delegate"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/apps/books/service/Drainer$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/service/Drainer$1;-><init>(Lcom/google/android/apps/books/service/Drainer;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method
