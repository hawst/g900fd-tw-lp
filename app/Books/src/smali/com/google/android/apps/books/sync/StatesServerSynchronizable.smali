.class public Lcom/google/android/apps/books/sync/StatesServerSynchronizable;
.super Lcom/google/android/apps/books/sync/BaseStatesSynchronizable;
.source "StatesServerSynchronizable.java"


# instance fields
.field private final mLocalSynchronizable:Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 2
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 45
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/books/sync/BaseStatesSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 46
    new-instance v0, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->mLocalSynchronizable:Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;

    .line 47
    return-void
.end method


# virtual methods
.method public extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 11
    .param p1, "oldValues"    # Landroid/content/ContentValues;
    .param p2, "newValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 67
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->checkValues(Landroid/content/ContentValues;)V

    .line 68
    const-string v4, "last_action"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "newAction":Ljava/lang/String;
    if-nez v0, :cond_2

    move v4, v5

    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Action not allowed in server-side update: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 72
    invoke-static {p1, p2}, Lcom/google/android/apps/books/sync/SyncUtil;->extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v3

    .line 74
    .local v3, "result":Landroid/content/ContentValues;
    if-eqz v3, :cond_0

    const-string v4, "position"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 75
    const-string v4, "last_access"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 76
    .local v2, "oldTime":Ljava/lang/Long;
    const-string v4, "last_access"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 77
    .local v1, "newTime":Ljava/lang/Long;
    const-string v4, "oldValues missing %s: %s"

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "last_access"

    aput-object v8, v7, v6

    aput-object p1, v7, v5

    invoke-static {v2, v4, v7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    const-string v4, "newValues missing %s: %s"

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, "last_access"

    aput-object v8, v7, v6

    aput-object p2, v7, v5

    invoke-static {v1, v4, v7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 84
    const-string v4, "position"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 85
    const-string v4, "last_access"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 86
    const-string v4, "StatesServerSync"

    invoke-static {v4, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    const-string v4, "StatesServerSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "extractUpdates dropping stale position, result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    .end local v1    # "newTime":Ljava/lang/Long;
    .end local v2    # "oldTime":Ljava/lang/Long;
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "StatesServerSync"

    invoke-static {v4, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 93
    const-string v4, "StatesServerSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updates: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_1
    return-object v3

    .end local v3    # "result":Landroid/content/ContentValues;
    :cond_2
    move v4, v6

    .line 69
    goto/16 :goto_0
.end method

.method public queryAllForUpsync()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 102
    const-string v6, "last_action IS NOT NULL"

    .line 103
    .local v6, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->getResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->getDirUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "last_action IS NOT NULL"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 5
    .param p1, "oldValues"    # Landroid/content/ContentValues;
    .param p2, "updateValues"    # Landroid/content/ContentValues;

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->getItemUri(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 52
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, "last_access"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "oldTime":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    .line 54
    .local v1, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->getResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "last_access=? AND last_action IS NULL"

    invoke-virtual {v3, v2, p2, v4, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method public updateAfterUpsync(Landroid/content/ContentValues;)I
    .locals 2
    .param p1, "oldValues"    # Landroid/content/ContentValues;

    .prologue
    .line 111
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 112
    .local v0, "updateValues":Landroid/content/ContentValues;
    const-string v1, "last_action"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->mLocalSynchronizable:Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;->update(Landroid/content/ContentValues;Landroid/content/ContentValues;)I

    move-result v1

    return v1
.end method
