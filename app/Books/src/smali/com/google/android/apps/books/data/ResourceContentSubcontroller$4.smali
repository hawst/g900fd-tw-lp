.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "ResourceContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$haveContentConsumers:Z

.field final synthetic val$resource:Lcom/google/android/apps/books/model/Resource;

.field final synthetic val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p5, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    iput-object p6, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-boolean p7, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$haveContentConsumers:Z

    iput-object p8, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    iput-object p9, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method

.method private expectResourceCompressed(Lcom/google/android/apps/books/model/Resource;)Z
    .locals 2
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;

    .prologue
    .line 373
    const-string v0, "smil"

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private maybeDecrypt(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/net/EncryptedContentResponse;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 5
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "content"    # Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-interface {p2}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->shouldDecryptEncryptedServerResponse(Lcom/google/android/apps/books/model/Resource;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$800(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/EncryptionScheme;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getContent()Ljava/io/InputStream;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/data/EncryptionScheme;->decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;

    move-result-object v0

    .line 364
    .local v0, "decrypted":Ljava/io/InputStream;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->expectResourceCompressed(Lcom/google/android/apps/books/model/Resource;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 367
    .local v1, "maybeInflated":Ljava/io/InputStream;
    :goto_0
    new-instance p2, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;

    .end local p2    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    const/4 v2, 0x0

    invoke-direct {p2, v1, v2}, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 369
    .end local v0    # "decrypted":Ljava/io/InputStream;
    .end local v1    # "maybeInflated":Ljava/io/InputStream;
    :cond_0
    return-object p2

    .restart local v0    # "decrypted":Ljava/io/InputStream;
    .restart local p2    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    :cond_1
    move-object v1, v0

    .line 364
    goto :goto_0
.end method

.method private maybeRequery(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/net/EncryptedContentResponse;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 6
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p3, "content"    # Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    const-string v3, "audio"

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 336
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-interface {p3}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getContent()Ljava/io/InputStream;

    move-result-object v4

    sget-object v5, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-static {v3}, Lcom/google/common/io/CharStreams;->toString(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v1

    .line 338
    .local v1, "contentString":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/util/MediaUrls;

    invoke-direct {v2}, Lcom/google/android/apps/books/util/MediaUrls;-><init>()V

    .line 339
    .local v2, "mediaUrls":Lcom/google/android/apps/books/util/MediaUrls;
    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/util/MediaUrls;->loadMediaServerResponse(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 340
    const-string v3, "RCSC"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 341
    const-string v3, "RCSC"

    const-string v4, "failed to load media URLs"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    .end local v1    # "contentString":Ljava/lang/String;
    .end local v2    # "mediaUrls":Lcom/google/android/apps/books/util/MediaUrls;
    .end local p3    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    :cond_0
    :goto_0
    return-object p3

    .line 345
    .restart local v1    # "contentString":Ljava/lang/String;
    .restart local v2    # "mediaUrls":Lcom/google/android/apps/books/util/MediaUrls;
    .restart local p3    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/MediaUrls;->getBestMediaUrl(I)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "bestAudioUrl":Ljava/lang/String;
    const-string v3, "RCSC"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 347
    const-string v3, "RCSC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fetching audio resource content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_2
    new-instance p3, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;

    .end local p3    # "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    invoke-interface {p2}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/net/BooksServer;->getAudioResourceContent(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {p3, v3}, Lcom/google/android/apps/books/net/EncryptedContentResponseImpl;-><init>(Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method private maybeTransformResource([BLcom/google/android/apps/books/model/Resource;Ljava/util/List;)[B
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lcom/google/android/apps/books/model/Resource;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 383
    .local p3, "subResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    const-string v2, "text/css"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    .line 386
    .local v0, "original":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/util/BooksTextUtils;->transformCssForWebView(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 388
    .local v1, "transformForWebView":Ljava/lang/String;
    invoke-direct {p0, v1, p3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->replaceResourceUrls(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    .line 391
    .end local v0    # "original":Ljava/lang/String;
    .end local v1    # "transformForWebView":Ljava/lang/String;
    .end local p1    # "bytes":[B
    :cond_0
    return-object p1
.end method

.method private replaceResourceUrls(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .param p1, "original"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 395
    .local p2, "subResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    move-object v3, p1

    .line 396
    .local v3, "result":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 397
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Resource;

    .line 398
    .local v1, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Resource;->getUrl()Ljava/lang/String;

    move-result-object v4

    .line 399
    .local v4, "url":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcePolicy:Lcom/google/android/apps/books/data/ResourcePolicy;
    invoke-static {v5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$900(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ResourcePolicy;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mAccount:Landroid/accounts/Account;
    invoke-static {v6}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1000(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Landroid/accounts/Account;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v7, v7, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v6, v7, v8}, Lcom/google/android/apps/books/data/ResourcePolicy;->getResourceContentUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 402
    .local v2, "resourceContentUri":Ljava/lang/String;
    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 403
    goto :goto_0

    .line 405
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "resource":Lcom/google/android/apps/books/model/Resource;
    .end local v2    # "resourceContentUri":Ljava/lang/String;
    .end local v4    # "url":Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method private serverResourceRetriever(Ljava/util/List;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)",
            "Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;"
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    new-instance v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4$1;-><init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;Ljava/util/List;)V

    return-object v0
.end method

.method private shouldDecryptEncryptedServerResponse(Lcom/google/android/apps/books/model/Resource;)Z
    .locals 2
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mResourcePolicy:Lcom/google/android/apps/books/data/ResourcePolicy;
    invoke-static {v0}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$900(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ResourcePolicy;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/ResourcePolicy;->alwaysStoreResourcesDecrypted()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "smil"

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldSlurpIntoMemory(Lcom/google/android/apps/books/model/Resource;Z)Z
    .locals 2
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "haveContentConsumers"    # Z

    .prologue
    .line 409
    const-string v0, "text/css"

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->LARGE_RESOURCE_TYPES:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1100()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 17
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 282
    const/4 v12, 0x0

    .line 283
    .local v12, "response":Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    const/4 v11, 0x0

    .line 285
    .local v11, "content":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v15

    .line 286
    .local v15, "server":Lcom/google/android/apps/books/net/BooksServer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, v2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Resource;->getUrl()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-interface {v15, v2, v3, v4, v5}, Lcom/google/android/apps/books/net/BooksServer;->getResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;

    move-result-object v12

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->getContent()Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->maybeDecrypt(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/net/EncryptedContentResponse;)Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->maybeRequery(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/net/EncryptedContentResponse;)Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v11

    .line 290
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v16

    .line 292
    .local v16, "sessionKeyForStorage":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$haveContentConsumers:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->shouldSlurpIntoMemory(Lcom/google/android/apps/books/model/Resource;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 293
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getContent()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v10

    .line 294
    .local v10, "bytes":[B
    new-instance v9, Lcom/google/android/apps/books/data/SmallBlobFromServer;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->getResources()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2, v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->maybeTransformResource([BLcom/google/android/apps/books/model/Resource;Ljava/util/List;)[B

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    if-nez v16, :cond_2

    const/4 v2, 0x0

    :goto_0
    invoke-direct {v9, v3, v4, v2}, Lcom/google/android/apps/books/data/SmallBlobFromServer;-><init>([BLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/model/SessionKeyId;)V

    .line 301
    .end local v10    # "bytes":[B
    .local v9, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    :goto_1
    new-instance v13, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->getResources()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->serverResourceRetriever(Ljava/util/List;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-direct {v13, v9, v0, v2, v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;)V

    .line 303
    .local v13, "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;)V
    invoke-static {v2, v0, v3, v4, v13}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$500(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    if-eqz v12, :cond_0

    .line 318
    :try_start_1
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_0
    if-eqz v11, :cond_1

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 329
    .end local v9    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v13    # "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .end local v15    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v16    # "sessionKeyForStorage":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :cond_1
    :goto_2
    return-void

    .line 294
    .restart local v10    # "bytes":[B
    .restart local v15    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .restart local v16    # "sessionKeyForStorage":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :cond_2
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    goto :goto_0

    .line 298
    .end local v10    # "bytes":[B
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    invoke-interface {v2, v11}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v14

    .line 299
    .local v14, "saveFinalizer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v9, Lcom/google/android/apps/books/data/LargeBlobFromServer;

    invoke-direct {v9, v14}, Lcom/google/android/apps/books/data/LargeBlobFromServer;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V
    :try_end_2
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v9    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    goto :goto_1

    .line 323
    .end local v14    # "saveFinalizer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .restart local v13    # "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    :catch_0
    move-exception v7

    .line 324
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "RCSC"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    const-string v2, "RCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 304
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v13    # "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    .end local v15    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v16    # "sessionKeyForStorage":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :catch_1
    move-exception v7

    .line 305
    .local v7, "e":Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$resource:Lcom/google/android/apps/books/model/Resource;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    move-object/from16 v3, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$600(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 317
    if-eqz v12, :cond_4

    .line 318
    :try_start_4
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_4
    if-eqz v11, :cond_1

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 323
    :catch_2
    move-exception v7

    .line 324
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "RCSC"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    const-string v2, "RCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 307
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 308
    .restart local v7    # "e":Ljava/io/IOException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v2, v0, v3, v7}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 317
    if-eqz v12, :cond_5

    .line 318
    :try_start_6
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_5
    if-eqz v11, :cond_1

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_2

    .line 323
    :catch_4
    move-exception v7

    .line 324
    const-string v2, "RCSC"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    const-string v2, "RCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 309
    .end local v7    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v7

    .line 310
    .local v7, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v2, v0, v3, v7}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 317
    if-eqz v12, :cond_6

    .line 318
    :try_start_8
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_6
    if-eqz v11, :cond_1

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_2

    .line 323
    :catch_6
    move-exception v7

    .line 324
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "RCSC"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    const-string v2, "RCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 311
    .end local v7    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 312
    .local v7, "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v2, v0, v3, v7}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 317
    if-eqz v12, :cond_7

    .line 318
    :try_start_a
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_7
    if-eqz v11, :cond_1

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    goto/16 :goto_2

    .line 323
    :catch_8
    move-exception v7

    .line 324
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "RCSC"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    const-string v2, "RCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 313
    .end local v7    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v7

    .line 314
    .local v7, "e":Ljava/security/GeneralSecurityException;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v2, v0, v3, v7}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 317
    if-eqz v12, :cond_8

    .line 318
    :try_start_c
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_8
    if-eqz v11, :cond_1

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    goto/16 :goto_2

    .line 323
    :catch_a
    move-exception v7

    .line 324
    .local v7, "e":Ljava/io/IOException;
    const-string v2, "RCSC"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    const-string v2, "RCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing response object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 316
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 317
    if-eqz v12, :cond_9

    .line 318
    :try_start_d
    invoke-interface {v12}, Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;->close()V

    .line 320
    :cond_9
    if-eqz v11, :cond_a

    .line 321
    invoke-interface {v11}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    .line 327
    :cond_a
    :goto_3
    throw v2

    .line 323
    :catch_b
    move-exception v7

    .line 324
    .restart local v7    # "e":Ljava/io/IOException;
    const-string v3, "RCSC"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 325
    const-string v3, "RCSC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing response object: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
