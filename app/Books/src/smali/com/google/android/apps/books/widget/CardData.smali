.class public Lcom/google/android/apps/books/widget/CardData;
.super Ljava/lang/Object;
.source "CardData.java"


# instance fields
.field private final mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

.field private final mUpload:Lcom/google/android/apps/books/upload/Upload;

.field private final mVolumeData:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;)V
    .locals 1
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "localVolumeData"    # Lcom/google/android/apps/books/model/LocalVolumeData;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    .line 20
    iput-object p2, p0, Lcom/google/android/apps/books/widget/CardData;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/upload/Upload;)V
    .locals 1
    .param p1, "upload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    .line 26
    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    .line 28
    return-void
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastInteraction()J
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/CardData;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/model/VolumeDataUtils;->getLastInteraction(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;)J

    move-result-wide v0

    .line 72
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getStartedTimestamp()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUploadData()Lcom/google/android/apps/books/upload/Upload;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    return-object v0
.end method

.method public getVolumeData()Lcom/google/android/apps/books/model/VolumeData;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    return-object v0
.end method

.method public hasUploadData()Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVolumeData()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardData;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
