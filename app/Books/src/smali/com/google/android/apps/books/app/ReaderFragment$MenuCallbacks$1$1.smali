.class Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;->take(Lcom/google/android/apps/books/app/BookmarkController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/widget/DevicePageRendering;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;

.field final synthetic val$bc:Lcom/google/android/apps/books/app/BookmarkController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;Lcom/google/android/apps/books/app/BookmarkController;)V
    .locals 0

    .prologue
    .line 4158
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;->val$bc:Lcom/google/android/apps/books/app/BookmarkController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 4158
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;->take(Ljava/util/List;)V

    return-void
.end method

.method public take(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4161
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1$1;->val$bc:Lcom/google/android/apps/books/app/BookmarkController;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/app/BookmarkController;->toggleBookmarks(Ljava/util/List;Z)V

    .line 4162
    return-void
.end method
