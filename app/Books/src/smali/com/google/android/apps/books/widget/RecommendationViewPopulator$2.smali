.class Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;
.super Ljava/lang/Object;
.source "RecommendationViewPopulator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->fillView(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;ZLcom/google/android/apps/books/app/BooksFragmentCallbacks;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/RecommendationViewPopulator;

.field final synthetic val$bookDoc:Lcom/google/android/apps/books/playcards/BookDocument;

.field final synthetic val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/RecommendationViewPopulator;Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->this$0:Lcom/google/android/apps/books/widget/RecommendationViewPopulator;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$bookDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    iput-object p3, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$bookDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v1}, Lcom/google/android/apps/books/playcards/BookDocument;->getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v0

    .line 71
    .local v0, "saleability":Lcom/google/android/apps/books/playcards/BookDocument$Saleability;
    sget-object v1, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->FREE:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/books/util/RentalUtils;->canBePurchasedOrRented(Lcom/google/android/apps/books/playcards/BookDocument$Saleability;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$bookDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_RECOMMENDED_SAMPLE:Lcom/google/android/apps/books/app/BookOpeningFlags;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    .line 79
    :cond_1
    :goto_0
    return-void

    .line 75
    :cond_2
    sget-object v1, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->FOR_PREORDER:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    if-ne v0, v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$bookDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;->val$bookDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v3}, Lcom/google/android/apps/books/playcards/BookDocument;->getCanonicalVolumeLink()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->canStartAboutVolume(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
