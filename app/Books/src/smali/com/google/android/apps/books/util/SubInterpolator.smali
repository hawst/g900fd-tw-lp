.class public Lcom/google/android/apps/books/util/SubInterpolator;
.super Ljava/lang/Object;
.source "SubInterpolator.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private final mInitialProgress:F

.field private final mInterpolator:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(Landroid/animation/TimeInterpolator;F)V
    .locals 0
    .param p1, "interpolator"    # Landroid/animation/TimeInterpolator;
    .param p2, "initialProgress"    # F

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/util/SubInterpolator;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 17
    iput p2, p0, Lcom/google/android/apps/books/util/SubInterpolator;->mInitialProgress:F

    .line 18
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4
    .param p1, "input"    # F

    .prologue
    .line 22
    iget v1, p0, Lcom/google/android/apps/books/util/SubInterpolator;->mInitialProgress:F

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lcom/google/android/apps/books/util/SubInterpolator;->mInitialProgress:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float v0, v1, v2

    .line 23
    .local v0, "projectedInput":F
    iget-object v1, p0, Lcom/google/android/apps/books/util/SubInterpolator;->mInterpolator:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    return v1
.end method
