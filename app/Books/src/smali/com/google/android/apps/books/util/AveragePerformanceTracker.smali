.class public Lcom/google/android/apps/books/util/AveragePerformanceTracker;
.super Ljava/lang/Object;
.source "AveragePerformanceTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;
    }
.end annotation


# instance fields
.field private final mMeanMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker;->mMeanMap:Ljava/util/Map;

    .line 18
    return-void
.end method

.method private extractCategoryFromMsg(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "category":Ljava/lang/String;
    const-string v2, " "

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "splitMessage":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 69
    const/4 v2, 0x0

    aget-object v0, v1, v2

    .line 71
    :cond_0
    return-object v0
.end method


# virtual methods
.method public buildAverageMessage(Ljava/lang/String;J)Ljava/lang/String;
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "elapsed"    # J

    .prologue
    .line 46
    const-string v0, ""

    .line 47
    .local v0, "averageMsg":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/AveragePerformanceTracker;->extractCategoryFromMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "category":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 49
    iget-object v3, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker;->mMeanMap:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;

    .line 50
    .local v2, "mean":Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;
    if-nez v2, :cond_0

    .line 51
    new-instance v2, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;

    .end local v2    # "mean":Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;
    invoke-direct {v2, p0}, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;-><init>(Lcom/google/android/apps/books/util/AveragePerformanceTracker;)V

    .line 52
    .restart local v2    # "mean":Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;
    iget-object v3, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker;->mMeanMap:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    :cond_0
    invoke-virtual {v2, p2, p3}, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->addValue(J)V

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Average: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->getMean()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 57
    .end local v2    # "mean":Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;
    :cond_1
    return-object v0
.end method
