.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->submit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

.field final synthetic val$numSegments:I

.field final synthetic val$thisSegmentIndexInPassage:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;II)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    iput p2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->val$thisSegmentIndexInPassage:I

    iput p3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->val$numSegments:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "content":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mBackgroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$300(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1$1;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 133
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
