.class Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->handleResultUi(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;

.field final synthetic val$grantedLicense:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;Z)V
    .locals 0

    .prologue
    .line 2193
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;

    iput-boolean p2, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;->val$grantedLicense:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 2
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->mVolumeId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->access$3700(Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;->val$grantedLicense:Z

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->setHasOfflineLicense(Ljava/lang/String;Z)V

    .line 2197
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2193
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
