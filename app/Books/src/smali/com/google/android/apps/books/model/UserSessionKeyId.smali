.class public Lcom/google/android/apps/books/model/UserSessionKeyId;
.super Ljava/lang/Object;
.source "UserSessionKeyId.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SessionKeyId;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final INSTANCE:Lcom/google/android/apps/books/model/UserSessionKeyId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/apps/books/model/UserSessionKeyId;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/UserSessionKeyId;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/model/UserSessionKeyId;->INSTANCE:Lcom/google/android/apps/books/model/UserSessionKeyId;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {p0}, Lcom/google/android/apps/books/model/UserSessionKeyId;->keyFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    .line 48
    .local v0, "key":Lcom/google/android/apps/books/model/SessionKey;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/UserSessionKeyId;->INSTANCE:Lcom/google/android/apps/books/model/UserSessionKeyId;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v1

    goto :goto_0
.end method

.method public static get()Lcom/google/android/apps/books/model/UserSessionKeyId;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/apps/books/model/UserSessionKeyId;->INSTANCE:Lcom/google/android/apps/books/model/UserSessionKeyId;

    return-object v0
.end method

.method public static keyFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/SessionKey;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 52
    const-string v1, "session_key_version"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "sessionKeyVersion":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 55
    const/4 v1, 0x0

    .line 57
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/SessionKey;

    const-string v2, "root_key_version"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const-string v3, "session_key_blob"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/books/model/SessionKey;-><init>(ILjava/lang/String;[B)V

    goto :goto_0
.end method

.method public static setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V
    .locals 2
    .param p0, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 65
    const-string v0, "session_key_version"

    iget-object v1, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "root_key_version"

    iget v1, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 67
    const-string v0, "session_key_blob"

    iget-object v1, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 68
    return-void
.end method


# virtual methods
.method public addToContentValues(Landroid/content/ContentValues;)V
    .locals 0
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 39
    return-void
.end method

.method public deleteKeyAndEncryptedContent(Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 0
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 43
    invoke-interface {p1}, Lcom/google/android/apps/books/model/BooksDataStore;->removeUserSessionKey()V

    .line 44
    return-void
.end method

.method public load(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/SessionKey;
    .locals 2
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 27
    invoke-interface {p1}, Lcom/google/android/apps/books/model/BooksDataStore;->getUserSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    .line 28
    .local v0, "localKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/UserSessionKeyId;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public update(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p2, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->updateUserSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 34
    return-void
.end method
