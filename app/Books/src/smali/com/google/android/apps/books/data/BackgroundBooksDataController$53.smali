.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->syncRequestedDictionaries(Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1449
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;->val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/LocalDictionarySubController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getRequestedDictionaryMetadataList()Ljava/util/List;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getLanguageCodes(Ljava/util/List;)Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$5300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->syncLocalDictionaries(Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 1454
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;->val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1455
    return-void
.end method
