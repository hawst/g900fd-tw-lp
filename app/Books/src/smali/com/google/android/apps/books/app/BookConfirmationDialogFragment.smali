.class public Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "BookConfirmationDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 32
    return-void
.end method

.method private getBodyText()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 148
    new-instance v2, Lcom/google/android/apps/books/util/StyledStringBuilder;

    invoke-direct {v2}, Lcom/google/android/apps/books/util/StyledStringBuilder;-><init>()V

    .line 150
    .local v2, "builder":Lcom/google/android/apps/books/util/StyledStringBuilder;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 151
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/apps/books/util/VolumeArguments;->getTitle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    .line 152
    .local v3, "title":Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/books/util/VolumeArguments;->getAuthor(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "author":Ljava/lang/CharSequence;
    if-eqz v3, :cond_1

    .line 154
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/util/StyledStringBuilder;->addStyledText(Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 155
    if-eqz v1, :cond_0

    .line 156
    const-string v4, "\n"

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/util/StyledStringBuilder;->addText(Ljava/lang/CharSequence;)V

    .line 157
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2, v1, v4}, Lcom/google/android/apps/books/util/StyledStringBuilder;->addStyledText(Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 159
    :cond_0
    const-string v4, "\n\n"

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/util/StyledStringBuilder;->addText(Ljava/lang/CharSequence;)V

    .line 162
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/util/StyledStringBuilder;->addText(Ljava/lang/CharSequence;)V

    .line 164
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/StyledStringBuilder;->build()Landroid/text/SpannableString;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method protected getBody()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogBody"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getCancelLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogCancelLabel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getCheckboxLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogCheckboxLabel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getIconResource()I
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogIcon"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected getOkLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogOkLabel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelClicked()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 99
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v11

    const v12, 0x7f040059

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 101
    .local v10, "view":Landroid/view/View;
    const v11, 0x7f0e0131

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 102
    .local v9, "tv":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getBodyText()Ljava/lang/CharSequence;

    move-result-object v11

    sget-object v12, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v9, v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 104
    const v11, 0x7f0e0088

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 105
    .local v4, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getCheckboxLabel()Ljava/lang/String;

    move-result-object v5

    .line 107
    .local v5, "checkboxLabel":Ljava/lang/CharSequence;
    if-eqz v5, :cond_2

    .line 108
    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 109
    const/4 v6, 0x0

    .line 113
    .local v6, "checkboxVisibility":I
    :goto_0
    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 115
    new-instance v8, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$1;

    invoke-direct {v8, p0, v4}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$1;-><init>(Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;Landroid/widget/CheckBox;)V

    .line 122
    .local v8, "okListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v11, Landroid/app/AlertDialog$Builder;

    invoke-direct {v11, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getOkLabel()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 125
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getCancelLabel()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, "cancelLabel":Ljava/lang/CharSequence;
    if-eqz v2, :cond_0

    .line 127
    new-instance v3, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$2;-><init>(Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;)V

    .line 135
    .local v3, "cancelListener":Landroid/content/DialogInterface$OnClickListener;
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 138
    .end local v3    # "cancelListener":Landroid/content/DialogInterface$OnClickListener;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->getIconResource()I

    move-result v7

    .line 139
    .local v7, "iconResource":I
    if-eqz v7, :cond_1

    .line 140
    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 143
    :cond_1
    invoke-virtual {v1, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 144
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    return-object v11

    .line 111
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "cancelLabel":Ljava/lang/CharSequence;
    .end local v6    # "checkboxVisibility":I
    .end local v7    # "iconResource":I
    .end local v8    # "okListener":Landroid/content/DialogInterface$OnClickListener;
    :cond_2
    const/16 v6, 0x8

    .restart local v6    # "checkboxVisibility":I
    goto :goto_0
.end method

.method protected onOkClicked(Z)V
    .locals 0
    .param p1, "checkboxChecked"    # Z

    .prologue
    .line 170
    return-void
.end method
