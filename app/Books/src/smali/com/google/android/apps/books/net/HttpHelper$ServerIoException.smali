.class public final Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;
.super Lcom/google/android/ublib/utils/WrappedIoException;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServerIoException"
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final mRetryMillis:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 277
    invoke-direct {p0, p1}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;->mRetryMillis:Ljava/lang/Long;

    .line 279
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;->mRetryMillis:Ljava/lang/Long;

    .line 284
    return-void
.end method
