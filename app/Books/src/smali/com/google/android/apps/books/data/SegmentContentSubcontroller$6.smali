.class Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;
.super Ljava/lang/Object;
.source "SegmentContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v4

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$100(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 336
    :goto_0
    return-void

    .line 330
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/data/LocalBlob;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-interface {v2}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, v3, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v4, v4, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/data/LocalBlob;-><init>(Lcom/google/android/apps/books/data/InputStreamSource;)V

    .line 333
    .local v0, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    new-instance v1, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/List;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$1;)V

    .line 335
    .local v1, "result":Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    const/4 v5, 0x0

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V
    invoke-static {v2, v3, v4, v1, v5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$900(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 323
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
