.class public Lcom/google/android/apps/books/navigation/SnapshottingPage;
.super Ljava/lang/Object;
.source "SnapshottingPage.java"

# interfaces
.implements Lcom/google/android/apps/books/navigation/PageViewContent;
.implements Lcom/google/android/apps/books/util/Destroyable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/navigation/SnapshottingPage$1;,
        Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    }
.end annotation


# static fields
.field private static final LIVE_CONFIG:Landroid/graphics/Bitmap$Config;

.field private static final SNAPSHOT_CONFIG:Landroid/graphics/Bitmap$Config;

.field private static final sPaint:Landroid/graphics/Paint;

.field private static final sTmpPoint:Landroid/graphics/Point;

.field private static final sTmpRect:Landroid/graphics/Rect;


# instance fields
.field private final mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;"
        }
    .end annotation
.end field

.field private mBookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

.field private mDestroyed:Z

.field private mDevicePageRendering:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field private mDirectBitmap:Landroid/graphics/Bitmap;

.field private mDirectDraw:Z

.field private final mFullHeight:I

.field private final mFullWidth:I

.field private mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

.field private mListenerRef:Ljava/lang/ref/Reference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/Reference",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedsPainter:Z

.field private final mPageHandle:Lcom/google/android/apps/books/render/PageHandle;

.field private final mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

.field private final mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field private mPainter:Lcom/google/android/apps/books/render/PagePainter;

.field private final mPainterSize:Landroid/graphics/Point;

.field private mRenderTime:J

.field private mSharedPainter:Z

.field private mSnapshotTime:J

.field private final mThumbnailHeight:I

.field private final mThumbnailWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->SNAPSHOT_CONFIG:Landroid/graphics/Bitmap$Config;

    .line 54
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->LIVE_CONFIG:Landroid/graphics/Bitmap$Config;

    .line 78
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sPaint:Landroid/graphics/Paint;

    .line 79
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpRect:Landroid/graphics/Rect;

    .line 85
    sget-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sPaint:Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/render/PageHandle;IIIILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 2
    .param p2, "pageHandle"    # Lcom/google/android/apps/books/render/PageHandle;
    .param p3, "fullWidth"    # I
    .param p4, "fullHeight"    # I
    .param p5, "thumbnailWidth"    # I
    .param p6, "thumbnailHeight"    # I
    .param p7, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;",
            "Lcom/google/android/apps/books/render/PageHandle;",
            "IIII",
            "Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "bitmapCache":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<Lcom/google/android/apps/books/navigation/NavPageKey;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mNeedsPainter:Z

    .line 76
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainterSize:Landroid/graphics/Point;

    .line 82
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    .line 122
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 123
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    .line 124
    new-instance v0, Lcom/google/android/apps/books/navigation/NavPageKey;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/books/navigation/NavPageKey;-><init>(Lcom/google/android/apps/books/render/PageHandle;Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    .line 126
    iput p5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailWidth:I

    .line 127
    iput p6, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailHeight:I

    .line 129
    iput p3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullWidth:I

    .line 130
    iput p4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullHeight:I

    .line 132
    iput-object p7, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 133
    return-void
.end method

.method private attachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    .locals 2
    .param p1, "listener"    # Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    .prologue
    .line 345
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq p1, v1, :cond_0

    .line 346
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    .line 349
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->getCookie()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    .line 351
    .local v0, "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    if-nez v0, :cond_1

    .line 352
    new-instance v0, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    .end local v0    # "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingPage$1;)V

    .line 353
    .restart local v0    # "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    invoke-interface {p1, v0}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->setCookie(Ljava/lang/Object;)V

    .line 356
    :cond_1
    iput-object p0, v0, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    .line 358
    return-object v0
.end method

.method private drawAlignedBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "dstRect"    # Landroid/graphics/Rect;

    .prologue
    .line 396
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpRect:Landroid/graphics/Rect;

    invoke-direct {p0, p3, v0, v1, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getPageRect(Landroid/graphics/Rect;IILandroid/graphics/Rect;)V

    .line 397
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 398
    return-void
.end method

.method private drawToBitmap(Landroid/graphics/Bitmap$Config;II)Landroid/graphics/Bitmap;
    .locals 15
    .param p1, "config"    # Landroid/graphics/Bitmap$Config;
    .param p2, "boxWidth"    # I
    .param p3, "boxHeight"    # I

    .prologue
    .line 284
    const-string v7, "SnapshottingPage"

    const/4 v12, 0x3

    invoke-static {v7, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    .line 286
    .local v4, "d":Z
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    sget-object v12, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    invoke-interface {v7, v12}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 287
    sget-object v7, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v6, v7, Landroid/graphics/Point;->x:I

    .line 288
    .local v6, "pageWidth":I
    sget-object v7, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v5, v7, Landroid/graphics/Point;->y:I

    .line 290
    .local v5, "pageHeight":I
    if-eqz v6, :cond_0

    if-nez v5, :cond_2

    .line 291
    :cond_0
    const/4 v2, 0x0

    .line 312
    :cond_1
    :goto_0
    return-object v2

    .line 294
    :cond_2
    sget-object v7, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v7, v0, v1}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 296
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v7}, Lcom/google/android/apps/books/util/BitmapCache;->getReusePool()Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v7

    sget-object v12, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v12, v12, Landroid/graphics/Point;->x:I

    sget-object v13, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v13, v13, Landroid/graphics/Point;->y:I

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v7, v12, v13, v0, v14}, Lcom/google/android/apps/books/util/BitmapReusePool;->obtain(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 299
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 301
    .local v8, "startMillis":J
    :goto_1
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 302
    .local v3, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    int-to-float v12, v6

    div-float/2addr v7, v12

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v12, v12

    int-to-float v13, v5

    div-float/2addr v12, v13

    invoke-virtual {v3, v7, v12}, Landroid/graphics/Canvas;->scale(FF)V

    .line 304
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v7, v3}, Lcom/google/android/apps/books/render/PagePainter;->draw(Landroid/graphics/Canvas;)V

    .line 306
    if-eqz v4, :cond_1

    .line 307
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long v10, v12, v8

    .line 308
    .local v10, "time":J
    const-string v7, "SnapshottingPage"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Page "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " rendered at "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap$Config;->name()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "ms"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v7, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 299
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v8    # "startMillis":J
    .end local v10    # "time":J
    :cond_3
    const-wide/16 v8, 0x0

    goto :goto_1
.end method

.method private getListener()Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    return-object v0
.end method

.method private getPageRect(Landroid/graphics/Rect;IILandroid/graphics/Rect;)V
    .locals 8
    .param p1, "frameRect"    # Landroid/graphics/Rect;
    .param p2, "pageWidth"    # I
    .param p3, "pageHeight"    # I
    .param p4, "outPageRect"    # Landroid/graphics/Rect;

    .prologue
    .line 363
    sget-object v4, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    invoke-virtual {v4, p2, p3}, Landroid/graphics/Point;->set(II)V

    .line 364
    sget-object v4, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 366
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    sub-int v0, v4, v5

    .line 367
    .local v0, "extraX":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    div-int/lit8 v3, v4, 0x2

    .line 368
    .local v3, "verticalMargin":I
    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getLeftLetterboxFraction()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v1, v4

    .line 369
    .local v1, "leftMargin":I
    sub-int v2, v0, v1

    .line 371
    .local v2, "rightMargin":I
    iget v4, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v1

    iget v5, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v3

    iget v6, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v2

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v3

    invoke-virtual {p4, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 373
    return-void
.end method

.method private hasScaledThumbnail()Z
    .locals 2

    .prologue
    .line 217
    iget v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullWidth:I

    iget v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailWidth:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullHeight:I

    iget v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailHeight:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private invalidateListener()V
    .locals 3

    .prologue
    .line 156
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    .line 157
    .local v0, "listener":Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 164
    .local v1, "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    if-ne v2, p0, :cond_0

    .line 165
    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->pageUpdated()V

    .line 167
    :cond_0
    return-void

    .line 157
    .end local v1    # "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->getCookie()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    move-object v1, v2

    goto :goto_0
.end method

.method private putBitmap(Lcom/google/android/apps/books/navigation/NavPageKey;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;Z)V
    .locals 8
    .param p1, "id"    # Lcom/google/android/apps/books/navigation/NavPageKey;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "boxWidth"    # I
    .param p4, "boxHeight"    # I
    .param p5, "config"    # Landroid/graphics/Bitmap$Config;
    .param p6, "needCopy"    # Z

    .prologue
    const/4 v5, 0x0

    .line 228
    sget-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 229
    sget-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    invoke-static {v0, p3, p4}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 231
    sget-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v3, v0, Landroid/graphics/Point;->x:I

    .line 232
    .local v3, "width":I
    sget-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v4, v0, Landroid/graphics/Point;->y:I

    .line 234
    .local v4, "height":I
    if-eqz p6, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/BitmapCache;->getReusePool()Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v4, p5, v1}, Lcom/google/android/apps/books/util/BitmapReusePool;->obtain(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 237
    .local v6, "copy":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 238
    .local v7, "scaledCanvas":Landroid/graphics/Canvas;
    sget-object v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 239
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 240
    move-object p2, v6

    .line 243
    .end local v6    # "copy":Landroid/graphics/Bitmap;
    .end local v7    # "scaledCanvas":Landroid/graphics/Canvas;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    move-object v1, p1

    move-object v2, p2

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/util/BitmapCache;->putBitmap(Ljava/lang/Object;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)V

    .line 244
    return-void
.end method

.method private saveSnapshot(Landroid/graphics/Bitmap;Z)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "needCopy"    # Z

    .prologue
    .line 252
    if-nez p1, :cond_0

    .line 269
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    if-eqz v0, :cond_1

    .line 262
    const/4 p2, 0x1

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    iget v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullWidth:I

    iget v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullHeight:I

    sget-object v5, Lcom/google/android/apps/books/navigation/SnapshottingPage;->SNAPSHOT_CONFIG:Landroid/graphics/Bitmap$Config;

    move-object v0, p0

    move-object v2, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->putBitmap(Lcom/google/android/apps/books/navigation/NavPageKey;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;Z)V

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    iget v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailWidth:I

    iget v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailHeight:I

    sget-object v5, Lcom/google/android/apps/books/navigation/SnapshottingPage;->SNAPSHOT_CONFIG:Landroid/graphics/Bitmap$Config;

    move-object v0, p0

    move-object v2, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->putBitmap(Lcom/google/android/apps/books/navigation/NavPageKey;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;Z)V

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mSnapshotTime:J

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDestroyed:Z

    if-eqz v0, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDestroyed:Z

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectDraw:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mSharedPainter:Z

    if-nez v0, :cond_2

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PagePainter;->recycle()V

    .line 502
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/util/BitmapCache;->removeBitmap(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public detachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq p1, v1, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mListenerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->clear()V

    .line 148
    invoke-interface {p1}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->getCookie()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    .line 150
    .local v0, "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    if-ne v1, p0, :cond_0

    .line 151
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    goto :goto_0
.end method

.method public draw(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 7
    .param p1, "listener"    # Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "dstRect"    # Landroid/graphics/Rect;

    .prologue
    .line 402
    invoke-virtual {p3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDestroyed:Z

    if-eqz v4, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->attachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    move-result-object v3

    .line 412
    .local v3, "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    iget-object v4, v3, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 413
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v4}, Lcom/google/android/apps/books/util/BitmapCache;->getReusePool()Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v4

    iget-object v5, v3, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/util/BitmapReusePool;->release(Landroid/graphics/Bitmap;)V

    .line 414
    const/4 v4, 0x0

    iput-object v4, v3, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->bitmap:Landroid/graphics/Bitmap;

    .line 417
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    .line 419
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, p2, v4, p3}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->drawAlignedBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    goto :goto_0

    .line 423
    :cond_3
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v1

    .line 427
    .local v1, "isBusy":Z
    if-nez v1, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v4, :cond_4

    .line 429
    sget-object v4, Lcom/google/android/apps/books/navigation/SnapshottingPage;->LIVE_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->drawToBitmap(Landroid/graphics/Bitmap$Config;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 430
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x1

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->saveSnapshot(Landroid/graphics/Bitmap;Z)V

    .line 437
    :goto_1
    if-eqz v0, :cond_0

    .line 438
    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->drawAlignedBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 439
    iput-object v0, v3, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->bitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 433
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    if-nez v1, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    if-nez v4, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    .line 434
    .local v2, "key":Lcom/google/android/apps/books/navigation/NavPageKey;
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v4, v2}, Lcom/google/android/apps/books/util/BitmapCache;->getBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 433
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "key":Lcom/google/android/apps/books/navigation/NavPageKey;
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    goto :goto_2
.end method

.method public getBookmark()Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    return-object v0
.end method

.method public getPageHandle()Lcom/google/android/apps/books/render/PageHandle;
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    return-object v0
.end method

.method public getPagePainter()Lcom/google/android/apps/books/render/PagePainter;
    .locals 3

    .prologue
    .line 519
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    if-eqz v1, :cond_0

    .line 520
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/util/BitmapCache;->getBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 521
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 522
    new-instance v1, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;-><init>(Landroid/graphics/Bitmap;)V

    .line 532
    :goto_0
    return-object v1

    .line 526
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/util/BitmapCache;->getBitmap(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 527
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 528
    new-instance v1, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 531
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mSharedPainter:Z

    .line 532
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    goto :goto_0
.end method

.method public getPagePositionOnScreen()Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    return-object v0
.end method

.method public getPageRect(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 5
    .param p1, "frameRect"    # Landroid/graphics/Rect;
    .param p2, "outPageRect"    # Landroid/graphics/Rect;

    .prologue
    .line 377
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainterSize:Landroid/graphics/Point;

    iget v1, v2, Landroid/graphics/Point;->x:I

    .local v1, "pageWidth":I
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainterSize:Landroid/graphics/Point;

    iget v0, v2, Landroid/graphics/Point;->y:I

    .line 378
    .local v0, "pageHeight":I
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    sget-object v4, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/util/BitmapCache;->getBitmapSize(Ljava/lang/Object;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 380
    sget-object v2, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v1, v2, Landroid/graphics/Point;->x:I

    .line 381
    sget-object v2, Lcom/google/android/apps/books/navigation/SnapshottingPage;->sTmpPoint:Landroid/graphics/Point;

    iget v0, v2, Landroid/graphics/Point;->y:I

    .line 383
    :cond_1
    if-eqz v1, :cond_2

    if-nez v0, :cond_3

    .line 384
    :cond_2
    invoke-virtual {p2, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 385
    const/4 v2, 0x0

    .line 388
    :goto_0
    return v2

    .line 387
    :cond_3
    invoke-direct {p0, p1, v1, v0, p2}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getPageRect(Landroid/graphics/Rect;IILandroid/graphics/Rect;)V

    .line 388
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getPageRendering()Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDevicePageRendering:Lcom/google/android/apps/books/widget/DevicePageRendering;

    return-object v0
.end method

.method public getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 1
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullWidth:I

    iput v0, p1, Landroid/graphics/Point;->x:I

    .line 98
    iget v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullHeight:I

    iput v0, p1, Landroid/graphics/Point;->y:I

    .line 99
    return-object p1
.end method

.method public getSnapshotWaitMillis()J
    .locals 8

    .prologue
    .line 279
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x3e8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mRenderTime:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDestroyed:Z

    return v0
.end method

.method public isInUse()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 471
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getListener()Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    move-result-object v0

    .line 473
    .local v0, "listener":Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    if-nez v0, :cond_1

    .line 483
    :cond_0
    :goto_0
    return v2

    .line 477
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->getCookie()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    .line 479
    .local v1, "state":Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;
    if-eqz v1, :cond_0

    .line 483
    iget-object v3, v1, Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;->page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    if-ne v3, p0, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isReadyToDraw(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Z
    .locals 5
    .param p1, "listener"    # Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 451
    iget-boolean v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDestroyed:Z

    if-eqz v3, :cond_1

    .line 462
    :cond_0
    :goto_0
    return v1

    .line 455
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->attachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    .line 457
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    move v1, v2

    .line 458
    goto :goto_0

    .line 461
    :cond_2
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v0

    .line 462
    .local v0, "isBusy":Z
    if-nez v0, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    if-nez v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/BitmapCache;->containsBitmap(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public needsPainter()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mNeedsPainter:Z

    return v0
.end method

.method public needsSnapshot()Z
    .locals 4

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDestroyed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mSnapshotTime:J

    iget-wide v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mRenderTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBookmarksChanged()V
    .locals 1

    .prologue
    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getListener()Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    move-result-object v0

    .line 329
    .local v0, "listener":Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    if-eqz v0, :cond_0

    .line 330
    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent$Listener;->onBookmarksChanged()V

    .line 332
    :cond_0
    return-void
.end method

.method public onDecorationsChanged()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    if-nez v0, :cond_0

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mNeedsPainter:Z

    .line 214
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->attachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Lcom/google/android/apps/books/navigation/SnapshottingPage$ListenerState;

    .line 138
    return-void
.end method

.method public setPainter(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 8
    .param p1, "devicePageRendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "directDraw"    # Z
    .param p4, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 176
    iput-boolean p3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectDraw:Z

    .line 177
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDevicePageRendering:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 178
    iput-object p4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mBookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 179
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    .line 180
    iput-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainterSize:Landroid/graphics/Point;

    invoke-interface {v1, v5}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 183
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mRenderTime:J

    .line 184
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mSnapshotTime:J

    .line 185
    if-eqz p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mNeedsPainter:Z

    .line 187
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectDraw:Z

    if-eqz v1, :cond_2

    invoke-interface {p2, v3}, Lcom/google/android/apps/books/render/PagePainter;->getSharedBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectBitmap:Landroid/graphics/Bitmap;

    .line 189
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mDirectDraw:Z

    if-nez v1, :cond_4

    if-eqz p2, :cond_4

    invoke-interface {p2}, Lcom/google/android/apps/books/render/PagePainter;->isMutableBitmap()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 190
    invoke-interface {p2, v3}, Lcom/google/android/apps/books/render/PagePainter;->getSharedBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 191
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->hasScaledThumbnail()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    new-instance v1, Lcom/google/android/apps/books/navigation/NavPageKey;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    invoke-direct {v1, v5, v3}, Lcom/google/android/apps/books/navigation/NavPageKey;-><init>(Lcom/google/android/apps/books/render/PageHandle;Z)V

    iput-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    .line 196
    :goto_2
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->saveSnapshot(Landroid/graphics/Bitmap;Z)V

    .line 197
    iput-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    .line 203
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->invalidateListener()V

    .line 204
    return-void

    :cond_1
    move v1, v2

    .line 185
    goto :goto_0

    :cond_2
    move-object v1, v4

    .line 187
    goto :goto_1

    .line 194
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    iput-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    goto :goto_2

    .line 199
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    .line 200
    iput-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    goto :goto_3
.end method

.method public snapshot()V
    .locals 4

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v1, :cond_0

    .line 318
    sget-object v3, Lcom/google/android/apps/books/navigation/SnapshottingPage;->SNAPSHOT_CONFIG:Landroid/graphics/Bitmap$Config;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullWidth:I

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mHiresKey:Lcom/google/android/apps/books/navigation/NavPageKey;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mFullHeight:I

    :goto_1
    invoke-direct {p0, v3, v1, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->drawToBitmap(Landroid/graphics/Bitmap$Config;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 321
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->saveSnapshot(Landroid/graphics/Bitmap;Z)V

    .line 322
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->invalidateListener()V

    .line 324
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 318
    :cond_1
    iget v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailWidth:I

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPage;->mThumbnailHeight:I

    goto :goto_1
.end method
