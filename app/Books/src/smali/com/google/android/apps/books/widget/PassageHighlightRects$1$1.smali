.class Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;
.super Ljava/lang/Object;
.source "PassageHighlightRects.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PassageHighlightRects$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Lcom/google/android/apps/books/render/LabeledRect;",
        ">;"
    }
.end annotation


# instance fields
.field private mRectIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/android/apps/books/widget/PassageHighlightRects$1;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PassageHighlightRects$1;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->this$1:Lcom/google/android/apps/books/widget/PassageHighlightRects$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/render/LabeledRect;)Z
    .locals 3
    .param p1, "item"    # Lcom/google/android/apps/books/render/LabeledRect;

    .prologue
    .line 100
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->mRectIterator:Ljava/util/Iterator;

    if-nez v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->reset()V

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->mRectIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->mRectIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/LabeledRect;

    .line 105
    .local v0, "next":Lcom/google/android/apps/books/render/LabeledRect;
    iget-object v1, v0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    iget-object v2, v0, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/books/render/LabeledRect;->set(Landroid/graphics/Rect;Ljava/lang/String;)Lcom/google/android/apps/books/render/LabeledRect;

    .line 106
    const/4 v1, 0x1

    .line 108
    .end local v0    # "next":Lcom/google/android/apps/books/render/LabeledRect;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 90
    check-cast p1, Lcom/google/android/apps/books/render/LabeledRect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->next(Lcom/google/android/apps/books/render/LabeledRect;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->this$1:Lcom/google/android/apps/books/widget/PassageHighlightRects$1;

    # invokes: Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->currentMap()Lcom/google/common/collect/SetMultimap;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->access$100(Lcom/google/android/apps/books/widget/PassageHighlightRects$1;)Lcom/google/common/collect/SetMultimap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->this$1:Lcom/google/android/apps/books/widget/PassageHighlightRects$1;

    # getter for: Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mCurrentColor:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->access$000(Lcom/google/android/apps/books/widget/PassageHighlightRects$1;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/common/collect/SetMultimap;->get(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;->mRectIterator:Ljava/util/Iterator;

    .line 96
    return-void
.end method
