.class Lcom/google/android/apps/books/app/ReaderMenuImpl$5;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;->createSearchMenuExpandListener()Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 429
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCollapsingSearchView:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$702(Lcom/google/android/apps/books/app/ReaderMenuImpl;Z)Z

    .line 430
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$200(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;->onSearchClosed()Z

    move-result v0

    .line 431
    .local v0, "result":Z
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # setter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCollapsingSearchView:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$702(Lcom/google/android/apps/books/app/ReaderMenuImpl;Z)Z

    .line 432
    if-eqz v0, :cond_0

    .line 433
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->setSearchMode(Z)V

    .line 435
    :cond_0
    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->setSearchMode(Z)V

    .line 422
    return v1
.end method
