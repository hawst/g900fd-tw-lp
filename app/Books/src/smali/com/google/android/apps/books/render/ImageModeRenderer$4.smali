.class Lcom/google/android/apps/books/render/ImageModeRenderer$4;
.super Lcom/google/android/apps/books/widget/DecoratingPagePainter;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;->overlayDebugStructurePainter(Lcom/google/android/apps/books/render/PagePainter;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/android/apps/books/render/PagePainter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

.field final synthetic val$page:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/util/Logger;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "x1"    # Z
    .param p4, "x2"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 941
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iput-object p5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->val$page:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;-><init>(Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/util/Logger;)V

    return-void
.end method

.method private getHighlightPaint(Ljava/lang/String;I)Landroid/graphics/Paint;
    .locals 6
    .param p1, "theme"    # Ljava/lang/String;
    .param p2, "color"    # I

    .prologue
    const/4 v3, 0x0

    .line 976
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$1700(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/HighlightPaintBox;

    move-result-object v0

    const/4 v5, 0x1

    move-object v1, p1

    move v2, p2

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/render/HighlightPaintBox;->getHighlightPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method private paintBox(Landroid/graphics/Canvas;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Landroid/graphics/Paint;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "box"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 981
    invoke-virtual {p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v7

    .line 982
    .local v7, "left":I
    invoke-virtual {p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getY()I

    move-result v9

    .line 983
    .local v9, "top":I
    invoke-virtual {p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v0

    add-int v8, v7, v0

    .line 984
    .local v8, "right":I
    invoke-virtual {p2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v0

    add-int v6, v9, v0

    .line 985
    .local v6, "bottom":I
    int-to-float v1, v7

    int-to-float v2, v9

    int-to-float v3, v8

    int-to-float v4, v6

    move-object v0, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 986
    return-void
.end method

.method private paintDebugWordBoxes(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    .line 948
    iget-object v11, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->getTheme()Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$1600(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/lang/String;

    move-result-object v8

    .line 949
    .local v8, "theme":Ljava/lang/String;
    iget-object v11, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->val$page:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v11}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v7

    .line 950
    .local v7, "structure":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    const v11, -0xff0001

    invoke-direct {p0, v8, v11}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->getHighlightPaint(Ljava/lang/String;I)Landroid/graphics/Paint;

    move-result-object v3

    .line 951
    .local v3, "bodyBlockPaint":Landroid/graphics/Paint;
    const v11, -0xff01

    invoke-direct {p0, v8, v11}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->getHighlightPaint(Ljava/lang/String;I)Landroid/graphics/Paint;

    move-result-object v4

    .line 952
    .local v4, "graphicBlockPaint":Landroid/graphics/Paint;
    const v11, -0xff0100

    invoke-direct {p0, v8, v11}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->getHighlightPaint(Ljava/lang/String;I)Landroid/graphics/Paint;

    move-result-object v10

    .line 953
    .local v10, "wordPaint":Landroid/graphics/Paint;
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->save(I)I

    .line 955
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v11, v13

    iget-object v12, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->val$page:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v12}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getW()I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v12, v13

    iget-object v13, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->val$page:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v13}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getH()I

    move-result v13

    int-to-float v13, v13

    div-float/2addr v12, v13

    invoke-virtual {p1, v11, v12}, Landroid/graphics/Canvas;->scale(FF)V

    .line 957
    const/4 v1, 0x0

    .local v1, "blockI":I
    :goto_0
    invoke-virtual {v7}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockCount()I

    move-result v11

    if-ge v1, v11, :cond_3

    .line 958
    invoke-virtual {v7, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlock(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v0

    .line 959
    .local v0, "block":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getAppearance()Ljava/lang/String;

    move-result-object v11

    const-string v12, "body"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    move-object v2, v3

    .line 962
    .local v2, "blockPaint":Landroid/graphics/Paint;
    :goto_1
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v11

    invoke-direct {p0, p1, v11, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->paintBox(Landroid/graphics/Canvas;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Landroid/graphics/Paint;)V

    .line 963
    const/4 v6, 0x0

    .local v6, "paraI":I
    :goto_2
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraphCount()I

    move-result v11

    if-ge v6, v11, :cond_2

    .line 964
    invoke-virtual {v0, v6}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraph(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v5

    .line 965
    .local v5, "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    const/4 v9, 0x0

    .local v9, "wordI":I
    :goto_3
    invoke-virtual {v5}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxCount()I

    move-result v11

    if-ge v9, v11, :cond_1

    .line 966
    invoke-virtual {v5, v9}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordbox(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v11

    invoke-direct {p0, p1, v11, v10}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->paintBox(Landroid/graphics/Canvas;Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;Landroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 965
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .end local v2    # "blockPaint":Landroid/graphics/Paint;
    .end local v5    # "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .end local v6    # "paraI":I
    .end local v9    # "wordI":I
    :cond_0
    move-object v2, v4

    .line 959
    goto :goto_1

    .line 963
    .restart local v2    # "blockPaint":Landroid/graphics/Paint;
    .restart local v5    # "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .restart local v6    # "paraI":I
    .restart local v9    # "wordI":I
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 957
    .end local v5    # "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .end local v9    # "wordI":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 971
    .end local v0    # "block":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    .end local v2    # "blockPaint":Landroid/graphics/Paint;
    .end local v6    # "paraI":I
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 973
    return-void

    .line 971
    .end local v1    # "blockI":I
    :catchall_0
    move-exception v11

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    throw v11
.end method


# virtual methods
.method public decorate(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 944
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;->paintDebugWordBoxes(Landroid/graphics/Canvas;)V

    .line 945
    return-void
.end method

.method public getOwnedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 995
    const/4 v0, 0x0

    return-object v0
.end method
