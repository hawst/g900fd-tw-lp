.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->onSuccessfulParse()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/widget/TtsLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/widget/TtsLoadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequestId:I
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->access$1600(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->access$1700(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->getPassage()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mFoundElements:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->access$1500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/TtsLoadListener;->onPassageMoListReady(IILjava/util/Map;)V

    .line 320
    :cond_0
    return-void
.end method
