.class public Lcom/google/android/apps/books/annotations/AnnotationData$Key;
.super Ljava/lang/Object;
.source "AnnotationData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Key"
.end annotation


# instance fields
.field public final id:Ljava/lang/String;

.field private final mEntityLocale:Ljava/lang/String;

.field private final mUserLocale:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "userLocale"    # Ljava/lang/String;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "userLocale"    # Ljava/lang/String;
    .param p3, "entityLocale"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mUserLocale:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mEntityLocale:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 67
    instance-of v2, p1, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 68
    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 69
    .local v0, "otherKey":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mUserLocale:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mUserLocale:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mEntityLocale:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mEntityLocale:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 73
    .end local v0    # "otherKey":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :cond_0
    return v1
.end method

.method public getEntityLocaleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mEntityLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getUserLocaleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mUserLocale:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mUserLocale:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mEntityLocale:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "userLocale"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mUserLocale:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "entityLocale"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->mEntityLocale:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
