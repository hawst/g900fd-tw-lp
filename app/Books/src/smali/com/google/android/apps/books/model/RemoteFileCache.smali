.class public Lcom/google/android/apps/books/model/RemoteFileCache;
.super Ljava/lang/Object;
.source "RemoteFileCache.java"


# instance fields
.field private mBlobStore:Lcom/google/android/apps/books/annotations/DiskBlobStore;

.field private mCacheParentDir:Ljava/io/File;

.field private final mMaxSize:I

.field private final mUniqueName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;I)V
    .locals 0
    .param p1, "cacheParentDir"    # Ljava/io/File;
    .param p2, "uniqueName"    # Ljava/lang/String;
    .param p3, "maxSize"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mCacheParentDir:Ljava/io/File;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mUniqueName:Ljava/lang/String;

    .line 52
    iput p3, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mMaxSize:I

    .line 53
    return-void
.end method

.method private getBlobsStore()Lcom/google/android/apps/books/annotations/DiskBlobStore;
    .locals 5

    .prologue
    .line 60
    iget-object v2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mBlobStore:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    if-nez v2, :cond_0

    .line 61
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mCacheParentDir:Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mUniqueName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 62
    .local v1, "parentDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v2, "blobs_dir"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 63
    .local v0, "blobsCacheDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 64
    new-instance v2, Lcom/google/android/apps/books/annotations/DiskBlobStore;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/model/RemoteFileCache;->getIndex(Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mMaxSize:I

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/apps/books/annotations/DiskBlobStore;-><init>(Ljava/io/File;Ljava/io/File;I)V

    iput-object v2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mBlobStore:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    .line 65
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mCacheParentDir:Ljava/io/File;

    .line 67
    .end local v0    # "blobsCacheDir":Ljava/io/File;
    .end local v1    # "parentDir":Ljava/io/File;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mBlobStore:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    return-object v2
.end method

.method private getIndex(Ljava/io/File;)Ljava/io/File;
    .locals 2
    .param p1, "parent"    # Ljava/io/File;

    .prologue
    .line 56
    new-instance v0, Ljava/io/File;

    const-string v1, "index"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getFile(Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/model/RemoteFileCache;->getBlobsStore()Lcom/google/android/apps/books/annotations/DiskBlobStore;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/BlobKeys;->forUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->get(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setFile(Landroid/net/Uri;[B)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "byteArray"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    monitor-enter p0

    const/4 v1, 0x0

    .line 76
    .local v1, "os":Ljava/io/OutputStream;
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/model/RemoteFileCache;->getBlobsStore()Lcom/google/android/apps/books/annotations/DiskBlobStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 78
    .local v0, "blobsStore":Lcom/google/android/apps/books/annotations/DiskBlobStore;
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/annotations/BlobKeys;->forUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->set(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    .line 79
    invoke-virtual {v1, p2}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 83
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->save()V

    .line 85
    const-string v2, "RemoteFileCache"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    const-string v2, "RemoteFileCache"

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/RemoteFileCache;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 88
    :cond_1
    monitor-exit p0

    return-void

    .line 81
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_2
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 75
    .end local v0    # "blobsStore":Lcom/google/android/apps/books/annotations/DiskBlobStore;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "uniqueName"

    iget-object v2, p0, Lcom/google/android/apps/books/model/RemoteFileCache;->mUniqueName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
