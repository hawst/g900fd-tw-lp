.class public Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
.super Ljava/lang/Object;
.source "SegmentPositionResolver.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SegmentPositionResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Segment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;",
        ">;"
    }
.end annotation


# instance fields
.field public center:I

.field public width:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "center"    # I
    .param p2, "width"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    .line 22
    iput p2, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->width:I

    .line 23
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;)I
    .locals 2
    .param p1, "segment"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    iget v1, p1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->compareTo(Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;)I

    move-result v0

    return v0
.end method

.method public constrainTo(II)V
    .locals 1
    .param p1, "leftCenter"    # I
    .param p2, "rightCenter"    # I

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    if-ge v0, p1, :cond_0

    .line 32
    iput p1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    .line 34
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    if-le v0, p2, :cond_1

    .line 35
    iput p2, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    .line 37
    :cond_1
    return-void
.end method

.method public getLeft()I
    .locals 2

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeftHalfWidth()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getLeftHalfWidth()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->width:I

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getRight()I
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->width:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getRightHalfWidth()I
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->width:I

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeftHalfWidth()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public setLeft(I)V
    .locals 1
    .param p1, "left"    # I

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeftHalfWidth()I

    move-result v0

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    .line 49
    return-void
.end method
