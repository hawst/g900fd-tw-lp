.class public interface abstract Lcom/google/android/apps/books/app/SelectionState;
.super Ljava/lang/Object;
.source "SelectionState.java"


# static fields
.field public static final NONE:Lcom/google/android/apps/books/app/SelectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/books/app/SelectionState$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/SelectionState$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/SelectionState;->NONE:Lcom/google/android/apps/books/app/SelectionState;

    return-void
.end method


# virtual methods
.method public abstract createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;
.end method

.method public abstract createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
.end method

.method public abstract getContainingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation
.end method

.method public abstract getNormalizedSelectedText()Ljava/lang/String;
.end method

.method public abstract getOverlappingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation
.end method

.method public abstract getPassageIndex()I
.end method

.method public abstract getSelectedText()Ljava/lang/String;
.end method

.method public abstract loadMatchingAnnotation(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/geo/LayerAnnotationLoader;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract moveSelectionHandle(IZIIIZ)I
.end method
