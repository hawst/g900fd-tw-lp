.class Lcom/google/android/apps/books/navigation/BookNavView$13;
.super Ljava/lang/Object;
.source "BookNavView.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/BookNavView;->setupPageEditor()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0

    .prologue
    .line 789
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$13;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "action"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 792
    const/4 v3, 0x2

    if-eq p2, v3, :cond_0

    move v3, v4

    .line 811
    :goto_0
    return v3

    .line 796
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 797
    .local v1, "pageTitle":Ljava/lang/CharSequence;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 800
    .local v0, "page":Lcom/google/android/apps/books/model/Page;
    :goto_1
    if-eqz v0, :cond_2

    .line 801
    new-instance v2, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 804
    .local v2, "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$13;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->hidePageNumberEditor()V
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$3000(Lcom/google/android/apps/books/navigation/BookNavView;)V

    .line 805
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$13;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/books/navigation/BookNavView;->scrollToLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    .line 806
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$13;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mOnlyFallbackHandle:Z
    invoke-static {v3, v5}, Lcom/google/android/apps/books/navigation/BookNavView;->access$202(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z

    .end local v2    # "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_2
    move v3, v5

    .line 811
    goto :goto_0

    .line 797
    .end local v0    # "page":Lcom/google/android/apps/books/model/Page;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$13;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$3200(Lcom/google/android/apps/books/navigation/BookNavView;)Ljava/util/TreeMap;

    move-result-object v3

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Page;

    move-object v0, v3

    goto :goto_1

    .line 808
    .restart local v0    # "page":Lcom/google/android/apps/books/model/Page;
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$13;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->setPageNumberValid(Z)V
    invoke-static {v3, v4}, Lcom/google/android/apps/books/navigation/BookNavView;->access$3300(Lcom/google/android/apps/books/navigation/BookNavView;Z)V

    goto :goto_2
.end method
