.class public Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;
.super Ljava/lang/Object;
.source "BaseTestingOptionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Resources"
.end annotation


# instance fields
.field final contentViewLayoutId:I

.field final freetextInputViewId:I

.field final freetextSubmitButtonViewId:I

.field final freetextViewLayoutId:I

.field final optionViewLabelViewId:I

.field final optionViewLayoutId:I

.field final optionViewSpinnerViewId:I

.field final optionsContainerViewId:I


# direct methods
.method public constructor <init>(IIIIIIII)V
    .locals 0
    .param p1, "contentViewLayoutId"    # I
    .param p2, "optionsContainerViewId"    # I
    .param p3, "optionViewLayoutId"    # I
    .param p4, "optionViewLabelViewId"    # I
    .param p5, "optionViewSpinnerViewId"    # I
    .param p6, "freetextViewLayoutId"    # I
    .param p7, "freetextInputViewId"    # I
    .param p8, "freetextSubmitButtonViewId"    # I

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput p1, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->contentViewLayoutId:I

    .line 272
    iput p2, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionsContainerViewId:I

    .line 273
    iput p3, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewLayoutId:I

    .line 274
    iput p4, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewLabelViewId:I

    .line 275
    iput p5, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewSpinnerViewId:I

    .line 276
    iput p6, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->freetextViewLayoutId:I

    .line 277
    iput p7, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->freetextInputViewId:I

    .line 278
    iput p8, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->freetextSubmitButtonViewId:I

    .line 279
    return-void
.end method
