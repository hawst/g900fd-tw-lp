.class public Lcom/google/android/apps/books/util/PurchaseHelper;
.super Ljava/lang/Object;
.source "PurchaseHelper.java"


# instance fields
.field private final mPurchaseBundle:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "purchaseBundle"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/books/util/PurchaseHelper;->mPurchaseBundle:Landroid/os/Bundle;

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/PurchaseHelper;->mPurchaseBundle:Landroid/os/Bundle;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/books/util/PurchaseHelper;->mPurchaseBundle:Landroid/os/Bundle;

    const-string v1, "volumeId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/util/PurchaseHelper;->mPurchaseBundle:Landroid/os/Bundle;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public static buildReadIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/apps/books/util/PurchaseHelper;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultIntent"    # Landroid/content/Intent;
    .param p2, "pendingPurchase"    # Lcom/google/android/apps/books/util/PurchaseHelper;

    .prologue
    .line 104
    const/4 v1, 0x0

    .line 105
    .local v1, "volumeId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 107
    .local v0, "accountName":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 109
    const-string v2, "backend_docid"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string v2, "authAccount"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_0
    if-nez v1, :cond_1

    if-eqz p2, :cond_1

    .line 115
    invoke-direct {p2}, Lcom/google/android/apps/books/util/PurchaseHelper;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    .line 116
    invoke-direct {p2}, Lcom/google/android/apps/books/util/PurchaseHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_1
    if-eqz v1, :cond_2

    if-nez v0, :cond_3

    .line 121
    :cond_2
    const/4 v2, 0x0

    .line 124
    :goto_0
    return-object v2

    :cond_3
    invoke-static {p0, v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method public static createInstance(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/apps/books/util/PurchaseHelper;
    .locals 2
    .param p0, "instanceState"    # Landroid/os/Bundle;
    .param p1, "stateKey"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 77
    .local v0, "purchaseBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/books/util/PurchaseHelper;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/util/PurchaseHelper;-><init>(Landroid/os/Bundle;)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static directPurchaseIntentV1(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "installedFinskyPackage"    # Ljava/lang/String;
    .param p3, "accountName"    # Ljava/lang/String;

    .prologue
    .line 167
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "use_direct_purchase"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 171
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 175
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-object v0
.end method

.method public static directPurchaseIntentV2(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)Landroid/content/Intent;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "directPurchaseVolumeId"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 187
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 188
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    .line 190
    .local v4, "phoneskyIsRentalCapable":Z
    :try_start_0
    const-string v8, "com.android.vending"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 192
    .local v5, "phoneskyPackageInfo":Landroid/content/pm/PackageInfo;
    iget v8, v5, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const v9, 0x4c83670

    if-lt v8, v9, :cond_4

    move v4, v6

    .line 197
    .end local v5    # "phoneskyPackageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    const-string v8, "PurchaseHelper"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 198
    const-string v8, "PurchaseHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "directPIV2(\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'): store can rent="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.android.vending.billing.PURCHASE"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v9, "com.android.vending"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "authAccount"

    invoke-virtual {v8, v9, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "backend"

    invoke-virtual {v8, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "document_type"

    const/4 v10, 0x5

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "backend_docid"

    invoke-virtual {v8, v9, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "full_docid"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "book-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 214
    .local v1, "intent":Landroid/content/Intent;
    if-eqz p3, :cond_6

    iget-boolean v8, p3, Lcom/google/android/apps/books/app/PurchaseInfo;->hasMultipleOffers:Z

    if-nez v8, :cond_5

    move v0, v6

    .line 216
    .local v0, "hasSingleOffer":Z
    :goto_1
    if-eqz v4, :cond_1

    if-eqz v0, :cond_2

    .line 217
    :cond_1
    if-eqz p3, :cond_7

    iget v2, p3, Lcom/google/android/apps/books/app/PurchaseInfo;->offerType:I

    .line 219
    .local v2, "offerType":I
    :goto_2
    const-string v6, "offer_type"

    invoke-virtual {v1, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 221
    .end local v2    # "offerType":I
    :cond_2
    const/high16 v6, 0x10000

    invoke-virtual {v3, v1, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    if-nez v6, :cond_3

    .line 222
    const/4 v1, 0x0

    .line 224
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    return-object v1

    .end local v0    # "hasSingleOffer":Z
    .restart local v5    # "phoneskyPackageInfo":Landroid/content/pm/PackageInfo;
    :cond_4
    move v4, v7

    .line 192
    goto/16 :goto_0

    .end local v5    # "phoneskyPackageInfo":Landroid/content/pm/PackageInfo;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_5
    move v0, v7

    .line 214
    goto :goto_1

    :cond_6
    move v0, v7

    goto :goto_1

    .restart local v0    # "hasSingleOffer":Z
    :cond_7
    move v2, v6

    .line 217
    goto :goto_2

    .line 194
    .end local v0    # "hasSingleOffer":Z
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v8

    goto/16 :goto_0
.end method

.method public static fallbackPurchaseIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    const/high16 v4, 0x10000

    .line 133
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "authAccount"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 138
    .local v1, "purchaseIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_2

    .line 140
    const-string v2, "PurchaseHelper"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    const-string v2, "PurchaseHelper"

    const-string v3, "synthesizing fallback purchase intent"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://play.google.com/store/books/details?id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 144
    .local v0, "fallbackUri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "authAccount"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 147
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_2

    .line 149
    const-string v2, "PurchaseHelper"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 150
    const-string v2, "PurchaseHelper"

    const-string v3, "synthesized fallback purchase intent failed resolution"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "authAccount"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 157
    .end local v0    # "fallbackUri":Landroid/net/Uri;
    :cond_2
    return-object v1
.end method

.method private getAccountName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/util/PurchaseHelper;->mPurchaseBundle:Landroid/os/Bundle;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVolumeId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/util/PurchaseHelper;->mPurchaseBundle:Landroid/os/Bundle;

    const-string v1, "volumeId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    const-string v0, "volId=%s, acct=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/books/util/PurchaseHelper;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/books/util/PurchaseHelper;->getAccountName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
