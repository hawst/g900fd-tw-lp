.class public abstract Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;
.super Ljava/lang/Object;
.source "BaseBooksActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "FragmentCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 230
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private canShowView(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "webUrl"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 265
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v3}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "installedFinskyApp":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 267
    if-eqz p1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 267
    goto :goto_0

    .line 269
    :cond_2
    if-nez p2, :cond_0

    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public addFragment(Landroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 249
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 252
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 255
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v2, 0x0

    invoke-static {p1, v2, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->addFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/support/v4/app/FragmentTransaction;)V

    .line 257
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3
    .param p2, "args"    # Landroid/os/Bundle;
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 434
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 435
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 436
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    const/4 v2, 0x0

    invoke-static {p1, p3, p2, v1, v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 437
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 438
    return-void
.end method

.method public addVolumeToMyEBooks(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "prompt"    # Z

    .prologue
    .line 388
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    if-eqz p3, :cond_0

    .line 391
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/app/AddVolumeTask;->addVolume(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public authenticationFinished(Landroid/content/Intent;Ljava/lang/Exception;I)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "requestId"    # I

    .prologue
    .line 369
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$200(Lcom/google/android/apps/books/app/BaseBooksActivity;)I

    move-result v0

    if-ne p3, v0, :cond_0

    .line 370
    if-eqz p1, :cond_1

    .line 371
    invoke-static {p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->markExternalIntent(Landroid/content/Intent;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startActivity(Landroid/content/Intent;)V

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const v2, 0x7f0f00d9

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public canStartAboutVolume(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "canonicalUrl"    # Ljava/lang/String;

    .prologue
    .line 275
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->canShowView(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getActionBar()Landroid/support/v7/app/ActionBar;
    .locals 1

    .prologue
    .line 382
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    return-object v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 236
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const/4 v0, 0x1

    return v0
.end method

.method public onExternalStorageException()V
    .locals 3

    .prologue
    .line 427
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getFileStorageManager()Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v0

    .line 429
    .local v0, "fsm":Lcom/google/android/apps/books/common/FileStorageManager;
    new-instance v1, Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;-><init>(Lcom/google/android/apps/books/common/FileStorageManager;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 430
    return-void
.end method

.method public openAuthenticatedUrl(Ljava/lang/String;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 356
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    :goto_0
    return-void

    .line 358
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 359
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 360
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # operator++ for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I
    invoke-static {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$208(Lcom/google/android/apps/books/app/BaseBooksActivity;)I

    .line 361
    const-class v2, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$300(Lcom/google/android/apps/books/app/BaseBooksActivity;)Landroid/accounts/Account;

    move-result-object v4

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mBrowserAuthenticationRequestId:I
    invoke-static {v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$200(Lcom/google/android/apps/books/app/BaseBooksActivity;)I

    move-result v5

    invoke-static {v4, p1, v5}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->create(Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v1, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 364
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public setActionBarDisplayOptions(II)V
    .locals 1
    .param p1, "options"    # I
    .param p2, "mask"    # I

    .prologue
    .line 240
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 241
    return-void
.end method

.method public startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "canonicalUrl"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 280
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v2

    .line 281
    .local v2, "installedFinskyApp":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-static {v0, p1, p3}, Lcom/google/android/apps/books/util/OceanUris;->getAboutTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 283
    .local v1, "aboutUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v4, p1

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$100(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 288
    .end local v1    # "aboutUri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 285
    :cond_0
    invoke-static {p2, p3}, Lcom/google/android/apps/books/util/OceanUris;->appendCampaignId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->openAuthenticatedUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "buyUrl"    # Ljava/lang/String;
    .param p3, "useDirectPurchaseApi"    # Z
    .param p4, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 293
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const-string v0, "BooksActivity"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    const-string v0, "BooksActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startBuyVolume volumeId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", buyUrl="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", useDirect="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v2

    .line 298
    .local v2, "installedFinskyApp":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 299
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 300
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    move v3, p3

    move-object v4, p1

    move-object v5, p4

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$100(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 305
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 303
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->openAuthenticatedUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startForcedSync()V
    .locals 1

    .prologue
    .line 442
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->startManualSync()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$400(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    .line 443
    return-void
.end method

.method public startSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const/4 v4, 0x0

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris;->buildSearchIntent(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 332
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v2

    .line 333
    .local v2, "installedFinskyApp":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v3, 0x0

    move-object v5, v4

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$100(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 338
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->openAuthenticatedUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startShare(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/net/Uri;)V
    .locals 10
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "author"    # Ljava/lang/CharSequence;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 412
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 413
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 414
    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const v4, 0x7f0f010d

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v8

    aput-object p2, v5, v9

    const/4 v6, 0x2

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    const-string v2, "android.intent.extra.SUBJECT"

    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const v4, 0x7f0f010c

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    invoke-static {v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->markExternalIntent(Landroid/content/Intent;)V

    .line 419
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const v3, 0x7f0f0107

    new-array v4, v9, [Ljava/lang/Object;

    aput-object p1, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "dialogTitle":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/BaseBooksActivity;->startActivity(Landroid/content/Intent;)V

    .line 422
    return-void
.end method

.method public startShop(Ljava/lang/String;)V
    .locals 6
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const/4 v4, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/OceanUris;->getShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 321
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v2

    .line 322
    .local v2, "installedFinskyApp":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v3, 0x0

    move-object v5, v4

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$100(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 327
    :goto_0
    return-void

    .line 325
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->openAuthenticatedUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startViewCollection(Ljava/lang/String;)V
    .locals 6
    .param p1, "collectionUri"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.FragmentCallbacks;"
    const/4 v4, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mConfig:Lcom/google/android/apps/books/util/Config;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v2

    .line 310
    .local v2, "installedFinskyApp":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 311
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 312
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v3, 0x0

    move-object v5, v4

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->startFinskyIntent(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$100(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 316
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 314
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->openAuthenticatedUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
