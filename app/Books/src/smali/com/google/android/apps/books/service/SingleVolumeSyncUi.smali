.class public Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
.super Ljava/lang/Object;
.source "SingleVolumeSyncUi.java"


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mNumVolumesToPin:I

.field private final mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;

.field private final mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

.field private final mVolumeIndex:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/service/SyncService$SyncUi;IILandroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0
    .param p1, "syncUi"    # Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .param p2, "volumeIndex"    # I
    .param p3, "numVolumesToPin"    # I
    .param p4, "account"    # Landroid/accounts/Account;
    .param p5, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;

    .line 21
    iput p2, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mVolumeIndex:I

    .line 22
    iput p3, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mNumVolumesToPin:I

    .line 23
    iput-object p4, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mAccount:Landroid/accounts/Account;

    .line 24
    iput-object p5, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    .line 25
    return-void
.end method


# virtual methods
.method public finishedVolumeDownload(Z)V
    .locals 4
    .param p1, "fullyDownloaded"    # Z

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;

    iget-object v1, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->finishedVolumeDownload(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 36
    return-void
.end method

.method public startingVolumeDownload()V
    .locals 5

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mNumVolumesToPin:I

    if-lez v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;

    iget-object v1, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    iget v2, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mVolumeIndex:I

    iget v3, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mNumVolumesToPin:I

    iget-object v4, p0, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->mAccount:Landroid/accounts/Account;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->startingVolumeDownload(Lcom/google/android/apps/books/model/VolumeData;IILandroid/accounts/Account;)V

    .line 31
    :cond_0
    return-void
.end method
