.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;
.super Landroid/os/AsyncTask;
.source "TextureEndOfBookView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "profileImage"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 363
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    iget-object v1, v1, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 365
    .local v0, "frameWidth":F
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v1, v0}, Lcom/google/android/apps/books/util/ImageUtils;->frameBitmapInCircle(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 360
    check-cast p1, [Landroid/graphics/Bitmap;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->doInBackground([Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "croppedImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    iget-object v0, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->val$readingActivity:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    iget-object v0, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # setter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCroppedProfileImage:Landroid/graphics/Bitmap;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$702(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    iget-object v0, v0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    iget-object v1, v1, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCroppedProfileImage:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$700(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->this$1:Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;

    iget-object v2, v2, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8;->val$authorImageView:Landroid/widget/ImageView;

    # invokes: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setAuthorImage(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$800(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 360
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView$8$1;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
