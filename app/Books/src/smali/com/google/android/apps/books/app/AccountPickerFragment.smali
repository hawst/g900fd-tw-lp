.class public Lcom/google/android/apps/books/app/AccountPickerFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "AccountPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;,
        Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;,
        Lcom/google/android/apps/books/app/AccountPickerFragment$Arguments;
    }
.end annotation


# instance fields
.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mAdapter:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;

.field private mCurrentAccount:Landroid/accounts/Account;

.field private mCurrentAccountPosition:I

.field private mListView:Landroid/widget/ListView;

.field private mProgressView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 231
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/AccountPickerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->refreshAccounts()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/AccountPickerFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AccountPickerFragment;->setState(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/app/AccountPickerFragment;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AccountPickerFragment;->handleError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/AccountPickerFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mProgressView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/AccountPickerFragment;)Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mAdapter:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/app/AccountPickerFragment;Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;)Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mAdapter:Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsAdapter;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/AccountPickerFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/AccountPickerFragment;Landroid/accounts/Account;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AccountPickerFragment;->onAccountClick(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/app/AccountPickerFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccountPosition:I

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/AccountPickerFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$800([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p0, "x0"    # [Ljava/lang/Object;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/AccountPickerFragment;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/AccountPickerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AccountPickerFragment;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->selectCurrentAccount()V

    return-void
.end method

.method private handleError(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 136
    const-string v0, "AccountPickerFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "AccountPickerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception raised while retrieving accounts: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->setAccountInScene(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->dismiss()V

    .line 142
    :cond_1
    return-void
.end method

.method private static indexOf([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .param p0, "array"    # [Ljava/lang/Object;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 253
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 254
    aget-object v1, p0, v0

    invoke-static {v1, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 253
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 258
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private onAccountClick(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccount:Landroid/accounts/Account;

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 187
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->dismiss()V

    .line 191
    :cond_0
    return-void
.end method

.method private refreshAccounts()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 127
    new-array v1, v5, [Ljava/lang/String;

    .line 128
    .local v1, "features":[Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/books/app/AccountPickerFragment$AccountsCallback;-><init>(Lcom/google/android/apps/books/app/AccountPickerFragment;Lcom/google/android/apps/books/app/AccountPickerFragment$1;)V

    .line 129
    .local v0, "callback":Landroid/accounts/AccountManagerCallback;, "Landroid/accounts/AccountManagerCallback<[Landroid/accounts/Account;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v3, "com.google"

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 132
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/app/AccountPickerFragment;->setState(I)V

    .line 133
    return-void
.end method

.method private selectCurrentAccount()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccountPosition:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccountPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 196
    return-void
.end method

.method private setAccountInScene(Landroid/accounts/Account;)Z
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 166
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 167
    check-cast v0, Lcom/google/android/apps/books/app/AccountPicker$HostActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-interface {v0}, Lcom/google/android/apps/books/app/AccountPicker$HostActivity;->getAccountPickerCallbacks()Lcom/google/android/apps/books/app/AccountPicker$Callbacks;

    move-result-object v1

    .line 169
    .local v1, "callbacks":Lcom/google/android/apps/books/app/AccountPicker$Callbacks;
    if-eqz v1, :cond_0

    .line 170
    invoke-interface {v1, p1}, Lcom/google/android/apps/books/app/AccountPicker$Callbacks;->finishedPickingAccount(Landroid/accounts/Account;)Z

    move-result v2

    .line 173
    .end local v1    # "callbacks":Lcom/google/android/apps/books/app/AccountPicker$Callbacks;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setState(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 146
    packed-switch p1, :pswitch_data_0

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 156
    const v0, 0x7f0f00be

    .line 158
    .local v0, "titleId":I
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->setTitle(I)V

    .line 159
    return-void

    .line 148
    .end local v0    # "titleId":I
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 150
    const v0, 0x7f0f00bf

    .line 151
    .restart local v0    # "titleId":I
    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private setTitle(I)V
    .locals 2
    .param p1, "titleId"    # I

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 178
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 181
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 110
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 111
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/apps/books/app/AccountPickerFragment$Arguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccount:Landroid/accounts/Account;

    .line 113
    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mAccountManager:Landroid/accounts/AccountManager;

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->refreshAccounts()V

    .line 115
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    const v2, 0x7f040050

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 81
    .local v1, "result":Landroid/view/ViewGroup;
    const v2, 0x7f0e0101

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mProgressView:Landroid/view/View;

    .line 82
    const v2, 0x7f0e0102

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mListView:Landroid/widget/ListView;

    .line 84
    const v2, 0x7f0e0103

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 85
    .local v0, "addAccountButton":Landroid/widget/Button;
    new-instance v2, Lcom/google/android/apps/books/app/AccountPickerFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/AccountPickerFragment$1;-><init>(Lcom/google/android/apps/books/app/AccountPickerFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    return-object v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 119
    const-string v0, "AccountPickerFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "AccountPickerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDismiss() with current account = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/app/AccountPickerFragment;->mCurrentAccount:Landroid/accounts/Account;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/AccountPickerFragment;->setAccountInScene(Landroid/accounts/Account;)Z

    .line 124
    return-void
.end method
