.class Lcom/google/android/apps/books/model/FootnoteExtractor$Element;
.super Ljava/lang/Object;
.source "FootnoteExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/FootnoteExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Element"
.end annotation


# instance fields
.field mContainsText:Z

.field final mIsTypeDisplayNoneDiv:Z

.field final mOpenTag:Ljava/lang/String;

.field final mQName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/model/FootnoteExtractor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/FootnoteExtractor;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p2, "qName"    # Ljava/lang/String;
    .param p3, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v1, 0x0

    .line 247
    iput-object p1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->this$0:Lcom/google/android/apps/books/model/FootnoteExtractor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mContainsText:Z

    .line 248
    iput-object p2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mQName:Ljava/lang/String;

    .line 255
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->createOpenTag(Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mOpenTag:Ljava/lang/String;

    .line 257
    const-string v2, "style"

    invoke-interface {p3, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "style":Ljava/lang/String;
    const-string v2, "div"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const-string v2, "display:none;"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mIsTypeDisplayNoneDiv:Z

    .line 260
    return-void
.end method

.method private createOpenTag(Lorg/xml/sax/Attributes;)Ljava/lang/String;
    .locals 6
    .param p1, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 263
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    .local v2, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v4, "<"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mQName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 266
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "name":Ljava/lang/String;
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v3

    .line 269
    .local v3, "value":Ljava/lang/String;
    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object v4, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mQName:Ljava/lang/String;

    const-string v5, "a"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "href"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "#GBS."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 271
    iget-object v4, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->this$0:Lcom/google/android/apps/books/model/FootnoteExtractor;

    # getter for: Lcom/google/android/apps/books/model/FootnoteExtractor;->mUrlScheme:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/model/FootnoteExtractor;->access$000(Lcom/google/android/apps/books/model/FootnoteExtractor;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    .end local v1    # "name":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    const-string v4, ">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public getCloseTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mQName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOpenTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/apps/books/model/FootnoteExtractor$Element;->mOpenTag:Ljava/lang/String;

    return-object v0
.end method
