.class public Lcom/google/android/apps/books/widget/RecommendationMenuEntry;
.super Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
.source "RecommendationMenuEntry.java"


# instance fields
.field private final mBuyCampaignId:Ljava/lang/String;

.field private final mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

.field private final mContext:Landroid/content/Context;

.field private final mDoc:Lcom/google/android/apps/books/playcards/BookDocument;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "buyCampaignId"    # Ljava/lang/String;
    .param p3, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;

    .prologue
    .line 23
    const/4 v0, -0x1

    invoke-static {p3, p1}, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->getMenuText(Lcom/google/android/apps/books/playcards/BookDocument;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;Z)V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mContext:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mBuyCampaignId:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    .line 27
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksActivity;->getFragmentCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    .line 28
    return-void
.end method

.method private static getMenuText(Lcom/google/android/apps/books/playcards/BookDocument;Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/books/playcards/BookDocument;->getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v0

    .line 32
    .local v0, "saleability":Lcom/google/android/apps/books/playcards/BookDocument$Saleability;
    invoke-static {v0}, Lcom/google/android/apps/books/util/RentalUtils;->canBePurchasedOrRented(Lcom/google/android/apps/books/playcards/BookDocument$Saleability;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/books/playcards/BookDocument;->getPurchaseInfo()Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/RentalUtils;->getSaleOrRentalText(Lcom/google/android/apps/books/app/PurchaseInfo;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 35
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public onActionSelected()V
    .locals 7

    .prologue
    .line 40
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v4}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    .line 41
    .local v3, "volumeId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mBuyCampaignId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/StoreLink;->forBook(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/app/StoreLink;

    move-result-object v1

    .line 42
    .local v1, "storeLink":Lcom/google/android/apps/books/app/StoreLink;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/app/StoreLink;->getUri(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "uri":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v4}, Lcom/google/android/apps/books/playcards/BookDocument;->getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v0

    .line 44
    .local v0, "saleability":Lcom/google/android/apps/books/playcards/BookDocument$Saleability;
    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/books/util/RentalUtils;->canBePurchasedOrRented(Lcom/google/android/apps/books/playcards/BookDocument$Saleability;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 45
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v6}, Lcom/google/android/apps/books/playcards/BookDocument;->getPurchaseInfo()Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v6

    invoke-interface {v4, v3, v2, v5, v6}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;->mDoc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {v5}, Lcom/google/android/apps/books/playcards/BookDocument;->getCanonicalVolumeLink()Ljava/lang/String;

    move-result-object v5

    const-string v6, "books_inapp_eob_about"

    invoke-interface {v4, v3, v5, v6}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
