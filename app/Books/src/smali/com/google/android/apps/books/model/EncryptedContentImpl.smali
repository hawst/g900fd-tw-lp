.class public Lcom/google/android/apps/books/model/EncryptedContentImpl;
.super Ljava/lang/Object;
.source "EncryptedContentImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/EncryptedContent;


# instance fields
.field private final mContent:Ljava/io/InputStream;

.field private final mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/io/InputStream;)V
    .locals 0
    .param p1, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .param p2, "content"    # Ljava/io/InputStream;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/apps/books/model/EncryptedContentImpl;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    .line 15
    iput-object p2, p0, Lcom/google/android/apps/books/model/EncryptedContentImpl;->mContent:Ljava/io/InputStream;

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "content"    # Ljava/io/InputStream;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/model/EncryptedContentImpl;-><init>(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/io/InputStream;)V

    .line 20
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/model/EncryptedContentImpl;->mContent:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 35
    return-void
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/model/EncryptedContentImpl;->mContent:Ljava/io/InputStream;

    return-object v0
.end method

.method public getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/model/EncryptedContentImpl;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    return-object v0
.end method
