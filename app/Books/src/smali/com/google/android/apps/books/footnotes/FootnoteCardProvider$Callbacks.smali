.class public interface abstract Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;
.super Ljava/lang/Object;
.source "FootnoteCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract disallowVerticalCardSwipes()V
.end method

.method public abstract getCardsState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
.end method

.method public abstract showInBook(Lcom/google/android/apps/books/common/Position;)V
.end method
