.class abstract Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;
.super Lcom/google/android/apps/books/render/BasePageHandle;
.source "WebViewRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/render/TextPageHandle;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BaseFlowingPageHandle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRenderer;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 0
    .param p2, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    .line 442
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/render/BasePageHandle;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 443
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$500(Lcom/google/android/apps/books/render/WebViewRenderer;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public getIndices()Lcom/google/android/apps/books/render/PageIndices;
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$500(Lcom/google/android/apps/books/render/WebViewRenderer;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method
