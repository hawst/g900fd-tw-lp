.class Lcom/google/android/apps/books/provider/VolumeContentProvidelet;
.super Ljava/lang/Object;
.source "VolumeContentProvidelet.java"


# instance fields
.field private final mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;Landroid/content/Context;)V
    .locals 0
    .param p1, "contentStore"    # Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method private getFile(ILandroid/net/Uri;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 6
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x1

    .line 55
    sparse-switch p1, :sswitch_data_0

    .line 85
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2

    .line 57
    :sswitch_0
    invoke-static {p2}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getInfo(Landroid/net/Uri;)Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;

    move-result-object v0

    .line 58
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    iget-object v3, v0, Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;->accountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;->volumeId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;->getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v2

    .line 82
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;
    :goto_0
    return-object v2

    .line 61
    :sswitch_1
    invoke-static {p2}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getInfo(Landroid/net/Uri;)Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;

    move-result-object v0

    .line 62
    .restart local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    iget-object v3, v0, Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;->accountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;->volumeId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;->getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v2

    goto :goto_0

    .line 65
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Volumes$Info;
    :sswitch_2
    invoke-static {p2, v2}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;

    move-result-object v0

    .line 66
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    iget-object v3, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->accountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->volumeId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->sectionId:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;->getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v2

    goto :goto_0

    .line 70
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
    :sswitch_3
    invoke-static {p2, v2}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;

    move-result-object v0

    .line 71
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    iget-object v3, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->accountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->volumeId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->pageId:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;->getPageContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v2

    goto :goto_0

    .line 75
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    :sswitch_4
    invoke-static {p2, v2}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;

    move-result-object v0

    .line 76
    .restart local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    iget-object v3, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->accountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->volumeId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->pageId:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;->getPageStructureContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v2

    goto :goto_0

    .line 81
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    :sswitch_5
    invoke-static {p2}, Lcom/google/android/apps/books/provider/BooksContract$SharedResources;->getSharedResourceId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "resId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContentStore:Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;

    invoke-interface {v2, v1}, Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;->getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v2

    goto :goto_0

    .line 55
    :sswitch_data_0
    .sparse-switch
        0x96 -> :sswitch_0
        0x97 -> :sswitch_1
        0xca -> :sswitch_2
        0x192 -> :sswitch_3
        0x193 -> :sswitch_4
        0x1a5 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public openFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 9
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 111
    const/16 v5, 0x12e

    if-ne p1, v5, :cond_0

    .line 112
    const/4 v5, 0x1

    invoke-static {p2, v5}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;

    move-result-object v3

    .line 113
    .local v3, "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    iget-object v5, p0, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/accounts/Account;

    iget-object v7, v3, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->accountName:Ljava/lang/String;

    const-string v8, "com.google"

    invoke-direct {v6, v7, v8}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    .line 117
    .local v0, "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    :try_start_0
    iget-object v5, v3, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->volumeId:Ljava/lang/String;

    iget-object v6, v3, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->resId:Ljava/lang/String;

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceParcelFileDescriptor(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 126
    .end local v0    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v3    # "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    :goto_0
    return-object v5

    .line 119
    .restart local v0    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .restart local v3    # "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    .line 121
    .local v2, "fnf":Ljava/io/FileNotFoundException;
    invoke-virtual {v2, v1}, Ljava/io/FileNotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 122
    throw v2

    .line 126
    .end local v0    # "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "fnf":Ljava/io/FileNotFoundException;
    .end local v3    # "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->getFile(ILandroid/net/Uri;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;

    move-result-object v5

    invoke-static {p3, p2}, Lcom/google/android/apps/books/util/ProviderUtils;->providerModeToPfdMode(Ljava/lang/String;Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;->openParcelFileDescriptor(I)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    goto :goto_0

    .line 128
    :catch_1
    move-exception v1

    .line 129
    .local v1, "e":Ljava/io/FileNotFoundException;
    new-instance v4, Ljava/io/FileNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find file for URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    .line 131
    .local v4, "newException":Ljava/io/FileNotFoundException;
    invoke-virtual {v4, v1}, Ljava/io/FileNotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 132
    throw v4
.end method
