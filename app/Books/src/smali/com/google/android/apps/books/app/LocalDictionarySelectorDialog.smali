.class public Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "LocalDictionarySelectorDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;
    }
.end annotation


# instance fields
.field mOriginalRequestedLanguages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->processResults(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;)V

    return-void
.end method

.method private createCheckableItems(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "serverMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p2, "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 88
    .local v0, "checkableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 89
    .local v2, "serverMeta":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    new-instance v3, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;

    invoke-virtual {v2}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;-><init>(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Z)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    .end local v2    # "serverMeta":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 93
    return-object v0
.end method

.method public static newInstance(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "serverMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p1, "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;

    invoke-direct {v1}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;-><init>()V

    .line 37
    .local v1, "frag":Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "server_metadata_list"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 40
    const-string v2, "requested_languages"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 41
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->setArguments(Landroid/os/Bundle;)V

    .line 42
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->setRetainInstance(Z)V

    .line 43
    return-object v1
.end method

.method private processResults(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;)V
    .locals 8
    .param p1, "dictionaryItemsAdapter"    # Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;

    .prologue
    const/4 v7, 0x1

    .line 100
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 101
    .local v4, "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 102
    .local v2, "numNewLanguages":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 103
    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;

    .line 104
    .local v1, "item":Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->getValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 105
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->getMetadata()Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, "requestedLanguage":Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v5, p0, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->mOriginalRequestedLanguages:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 108
    add-int/lit8 v2, v2, 0x1

    .line 102
    .end local v3    # "requestedLanguage":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    .end local v1    # "item":Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v5

    invoke-interface {v5, v4}, Lcom/google/android/apps/books/data/ForegroundBooksDataController;->setRequestedDictionaryLanguages(Ljava/util/List;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 117
    :cond_2
    if-lt v2, v7, :cond_3

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    if-ne v2, v7, :cond_4

    const v5, 0x7f0f01c3

    :goto_1
    const/4 v7, 0x0

    invoke-static {v6, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 127
    :cond_3
    :goto_2
    return-void

    .line 118
    :cond_4
    const v5, 0x7f0f01c4

    goto :goto_1

    .line 124
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/data/DataControllerUtils;->DUMMY_EXCEPTION_OR_NOTHING_CONSUMER:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/data/BooksDataController;->syncRequestedDictionaries(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_2
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    new-instance v3, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 51
    .local v3, "lv":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v6, "requested_languages"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 53
    .local v5, "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v6, p0, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->mOriginalRequestedLanguages:Ljava/util/Set;

    .line 54
    new-instance v2, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x7f040027

    const-string v8, "server_metadata_list"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-direct {p0, v8, v5}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->createCheckableItems(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v2, p0, v6, v7, v8}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;-><init>(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;Landroid/content/Context;ILjava/util/List;)V

    .line 59
    .local v2, "dictionaryItemsAdapter":Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;
    invoke-virtual {v3, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    new-instance v6, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$1;

    invoke-direct {v6, p0, v2}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$1;-><init>(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;)V

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 68
    new-instance v4, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$2;

    invoke-direct {v4, p0, v2}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$2;-><init>(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;)V

    .line 75
    .local v4, "okListener":Landroid/content/DialogInterface$OnClickListener;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x104000a

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "buttonTitle":Ljava/lang/String;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0f0158

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6
.end method
