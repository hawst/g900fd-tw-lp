.class public Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;
.super Ljava/lang/Object;
.source "DictionaryUpdateLists.java"


# instance fields
.field private final mNewestVersionLocal:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mNewestVersionServerOnly:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mOlderVersionLocal:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "newestVersionServerOnly":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p2, "newestVersionLocal":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p3, "olderVersionLocal":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;->mNewestVersionServerOnly:Ljava/util/List;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;->mNewestVersionLocal:Ljava/util/List;

    .line 26
    iput-object p3, p0, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;->mOlderVersionLocal:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method public getNewestVersionServerOnly()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;->mNewestVersionServerOnly:Ljava/util/List;

    return-object v0
.end method
