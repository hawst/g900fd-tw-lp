.class Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;
.super Ljava/lang/Object;
.source "PageTurnController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EndTurnDirectional"
.end annotation


# instance fields
.field private final mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field private final mFlinging:Z

.field private final mSequenceNumber:I

.field private final mVelocity:F

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PageTurnController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;ZFI)V
    .locals 0
    .param p2, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p3, "flinging"    # Z
    .param p4, "velocity"    # F
    .param p5, "startSequenceNumber"    # I

    .prologue
    .line 553
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 555
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mFlinging:Z

    .line 556
    iput p4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mVelocity:F

    .line 557
    iput p5, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mSequenceNumber:I

    .line 558
    return-void
.end method


# virtual methods
.method public canBeCanceled()Z
    .locals 2

    .prologue
    .line 598
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mSequenceNumber:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$200(Lcom/google/android/apps/books/widget/PageTurnController;)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canRun()Z
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$200(Lcom/google/android/apps/books/widget/PageTurnController;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mSequenceNumber:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDirection()Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    return-object v0
.end method

.method public run()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 568
    const-string v3, "PageTurnController"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 569
    const-string v3, "PageTurnController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ending directional turn in direction "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with sequence number "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mSequenceNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mSequenceNumber:I

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I
    invoke-static {v3, v4}, Lcom/google/android/apps/books/widget/PageTurnController;->access$702(Lcom/google/android/apps/books/widget/PageTurnController;I)I

    .line 574
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 575
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    if-eq v3, v4, :cond_1

    move v0, v1

    .line 576
    .local v0, "changingDirection":Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v3, v4}, Lcom/google/android/apps/books/widget/PageTurnController;->access$102(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    .line 577
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z
    invoke-static {v3, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$802(Lcom/google/android/apps/books/widget/PageTurnController;Z)Z

    .line 578
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PageTurnController;->access$300(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mFlinging:Z

    iget v6, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;->mVelocity:F

    if-nez v0, :cond_2

    :goto_1
    invoke-interface {v3, v4, v5, v6, v1}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V

    .line 588
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .end local v0    # "changingDirection":Z
    :goto_2
    return-object v1

    :cond_1
    move v0, v2

    .line 575
    goto :goto_0

    .restart local v0    # "changingDirection":Z
    :cond_2
    move v1, v2

    .line 578
    goto :goto_1

    .line 582
    .end local v0    # "changingDirection":Z
    :cond_3
    const-string v1, "PageTurnController"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 583
    const-string v1, "PageTurnController"

    const-string v2, "EndTurnInertial.run() early exit: Not turning"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_2
.end method
