.class public Lcom/google/android/apps/books/app/ChaptersAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "ChaptersAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;,
        Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

.field private final mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

.field private final mCurrentChapterIndex:I

.field private final mCurrentPosition:Lcom/google/android/apps/books/common/Position;

.field private final mGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mView:Landroid/widget/ExpandableListView;

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;ILcom/google/android/apps/books/common/Position;Landroid/widget/ExpandableListView;Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;)V
    .locals 7
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .param p3, "currentChapterIndex"    # I
    .param p4, "currentPosition"    # Lcom/google/android/apps/books/common/Position;
    .param p5, "view"    # Landroid/widget/ExpandableListView;
    .param p6, "callbacks"    # Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    .prologue
    const/4 v6, -0x1

    .line 133
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 129
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mGroups:Ljava/util/List;

    .line 134
    iput-object p1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 135
    iput-object p2, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 136
    iput p3, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentChapterIndex:I

    .line 137
    iput-object p4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentPosition:Lcom/google/android/apps/books/common/Position;

    .line 138
    iput-object p5, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    .line 139
    iput-object p6, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    .line 141
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v3

    .line 142
    .local v3, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 143
    .local v1, "chapterCount":I
    const/4 v4, -0x1

    .line 144
    .local v4, "groupStartChapterIndex":I
    const/4 v2, 0x0

    .local v2, "chapterIndex":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 145
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    .line 146
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getDepth()I

    move-result v5

    if-nez v5, :cond_1

    .line 147
    if-eq v4, v6, :cond_0

    .line 149
    add-int/lit8 v5, v2, -0x1

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/books/app/ChaptersAdapter;->addGroups(II)V

    .line 151
    :cond_0
    move v4, v2

    .line 144
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    :cond_2
    if-eq v4, v6, :cond_3

    .line 157
    add-int/lit8 v5, v1, -0x1

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/books/app/ChaptersAdapter;->addGroups(II)V

    .line 160
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    new-instance v6, Lcom/google/android/apps/books/app/ChaptersAdapter$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/app/ChaptersAdapter$1;-><init>(Lcom/google/android/apps/books/app/ChaptersAdapter;)V

    invoke-virtual {v5, v6}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 168
    iget-object v5, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    new-instance v6, Lcom/google/android/apps/books/app/ChaptersAdapter$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/app/ChaptersAdapter$2;-><init>(Lcom/google/android/apps/books/app/ChaptersAdapter;)V

    invoke-virtual {v5, v6}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 176
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ChaptersAdapter;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ChaptersAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;

    .prologue
    .line 23
    iget v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentChapterIndex:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/common/Position;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentPosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/ChaptersAdapter;I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ChaptersAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    move-result-object v0

    return-object v0
.end method

.method private addGroups(II)V
    .locals 7
    .param p1, "firstChapterIndex"    # I
    .param p2, "lastChapterIndex"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 187
    new-instance v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;-><init>(Lcom/google/android/apps/books/app/ChaptersAdapter;)V

    .line 190
    .local v0, "chapterGroup":Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    iget v6, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentChapterIndex:I

    if-gt p1, v6, :cond_3

    iget v6, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentChapterIndex:I

    if-lt p2, v6, :cond_3

    .line 194
    if-le p2, p1, :cond_1

    move v3, v4

    .line 195
    .local v3, "groupHasChapterChildren":Z
    :goto_0
    if-eqz v3, :cond_2

    .line 197
    iput-boolean v5, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->isCurrentPage:Z

    .line 198
    iput-boolean v4, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->containsCurrentPage:Z

    .line 199
    const/4 v1, 0x1

    .line 200
    .local v1, "currentPageAddition":I
    const/4 v2, 0x0

    .line 214
    .end local v3    # "groupHasChapterChildren":Z
    .local v2, "currentPageGroup":Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    :goto_1
    iput p1, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    .line 215
    sub-int v4, p2, p1

    add-int/2addr v4, v1

    iput v4, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->childCount:I

    .line 216
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mGroups:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    if-eqz v2, :cond_0

    .line 218
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mGroups:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_0
    return-void

    .end local v1    # "currentPageAddition":I
    .end local v2    # "currentPageGroup":Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    :cond_1
    move v3, v5

    .line 194
    goto :goto_0

    .line 203
    .restart local v3    # "groupHasChapterChildren":Z
    :cond_2
    iput-boolean v5, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->isCurrentPage:Z

    .line 204
    iput-boolean v5, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->containsCurrentPage:Z

    .line 205
    const/4 v1, 0x0

    .line 206
    .restart local v1    # "currentPageAddition":I
    new-instance v2, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;-><init>(Lcom/google/android/apps/books/app/ChaptersAdapter;)V

    .line 207
    .restart local v2    # "currentPageGroup":Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    iput-boolean v4, v2, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->isCurrentPage:Z

    goto :goto_1

    .line 210
    .end local v1    # "currentPageAddition":I
    .end local v2    # "currentPageGroup":Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    .end local v3    # "groupHasChapterChildren":Z
    :cond_3
    iput-boolean v5, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->containsCurrentPage:Z

    iput-boolean v5, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->isCurrentPage:Z

    .line 211
    const/4 v1, 0x0

    .line 212
    .restart local v1    # "currentPageAddition":I
    const/4 v2, 0x0

    .restart local v2    # "currentPageGroup":Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    goto :goto_1
.end method

.method private getChapterView(I)Lcom/google/android/apps/books/view/TopLevelChapterView;
    .locals 5
    .param p1, "groupPosition"    # I

    .prologue
    .line 299
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    invoke-static {p1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v0

    .line 300
    .local v0, "packed":J
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    invoke-virtual {v4, v0, v1}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v3

    .line 301
    .local v3, "viewPosition":I
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    invoke-virtual {v4}, Landroid/widget/ExpandableListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int v2, v3, v4

    .line 302
    .local v2, "viewIndex":I
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;

    invoke-virtual {v4, v2}, Landroid/widget/ExpandableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/view/TopLevelChapterView;

    return-object v4
.end method

.method private getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
    .locals 1
    .param p1, "groupIndex"    # I

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mGroups:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    return-object v0
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "groupIndex"    # I
    .param p2, "childIndex"    # I

    .prologue
    .line 243
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupIndex"    # I
    .param p2, "childIndex"    # I

    .prologue
    .line 253
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "groupIndex"    # I
    .param p2, "childIndex"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 274
    const v1, 0x7f0400c5

    invoke-static {p5, p4, v1}, Lcom/google/android/apps/books/util/ViewUtils;->maybeInflateAdapterItemView(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/view/LocationItemView;

    .line 277
    .local v0, "view":Lcom/google/android/apps/books/view/LocationItemView;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->bindChildView(ILcom/google/android/apps/books/view/LocationItemView;)V

    .line 278
    return-object v0
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupIndex"    # I

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->childCount:I

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "groupIndex"    # I

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter;->mGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupIndex"    # I

    .prologue
    .line 248
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "groupIndex"    # I
    .param p2, "isLastGroup"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 264
    const v1, 0x7f0400d3

    invoke-static {p4, p3, v1}, Lcom/google/android/apps/books/util/ViewUtils;->maybeInflateAdapterItemView(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/view/TopLevelChapterView;

    .line 267
    .local v0, "view":Lcom/google/android/apps/books/view/TopLevelChapterView;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getGroupInfo(I)Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->bindGroupView(ILcom/google/android/apps/books/view/TopLevelChapterView;)V

    .line 268
    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 283
    const/4 v0, 0x1

    return v0
.end method

.method public onGroupCollapsed(I)V
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 288
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupCollapsed(I)V

    .line 289
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getChapterView(I)Lcom/google/android/apps/books/view/TopLevelChapterView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/view/TopLevelChapterView;->setExpanded(Z)V

    .line 290
    return-void
.end method

.method public onGroupExpanded(I)V
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupExpanded(I)V

    .line 295
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->getChapterView(I)Lcom/google/android/apps/books/view/TopLevelChapterView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/view/TopLevelChapterView;->setExpanded(Z)V

    .line 296
    return-void
.end method
