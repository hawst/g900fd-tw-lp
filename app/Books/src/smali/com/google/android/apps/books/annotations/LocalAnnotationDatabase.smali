.class public interface abstract Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
.super Ljava/lang/Object;
.source "LocalAnnotationDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;,
        Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;
    }
.end annotation


# static fields
.field public static final UNKNOWN_SEGMENT_INDEX:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->UNKNOWN_SEGMENT_INDEX:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public abstract addUserAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;)V
.end method

.method public abstract deleteAll()V
.end method

.method public abstract editAnnotationFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
.end method

.method public abstract expungeServerId(Ljava/lang/String;)V
.end method

.method public abstract getAllServerIds(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLatestServerTimestamp(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)J
.end method

.method public abstract getLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;
.end method

.method public abstract getLocalIdForServerId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getSegmentVolumeAnnotations(Lcom/google/android/apps/books/annotations/Layer;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Layer;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract load(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract markEditedOnClient(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)V
.end method

.method public abstract markForDeletion(Ljava/lang/String;)V
.end method

.method public abstract removeAnnotationsForLayer(Lcom/google/android/apps/books/annotations/Layer;)V
.end method

.method public abstract removeLocalAnnotation(Ljava/lang/String;)V
.end method

.method public abstract removeVolumeAnnotationsForVolume(Ljava/lang/String;)V
.end method

.method public abstract setLastUsedTimestamp(Ljava/lang/String;J)V
.end method

.method public abstract setSegmentVolumeAnnotations(Lcom/google/android/apps/books/annotations/Layer;ILjava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Layer;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/ServerAnnotation;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract unsyncedAdditions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract unsyncedDeletedServerIds()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract unsyncedEdits()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateLayer(Lcom/google/android/apps/books/annotations/Layer;)V
.end method

.method public abstract updateServerReceipt(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V
.end method
