.class Lcom/google/android/apps/books/api/TranslationServerController$1$1;
.super Ljava/lang/Object;
.source "TranslationServerController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/api/TranslationServerController$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/api/TranslationServerController$1;

.field final synthetic val$results:Lcom/google/common/collect/BiMap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/api/TranslationServerController$1;Lcom/google/common/collect/BiMap;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->this$1:Lcom/google/android/apps/books/api/TranslationServerController$1;

    iput-object p2, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->val$results:Lcom/google/common/collect/BiMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->this$1:Lcom/google/android/apps/books/api/TranslationServerController$1;

    iget-object v0, v0, Lcom/google/android/apps/books/api/TranslationServerController$1;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    iget-object v1, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->val$results:Lcom/google/common/collect/BiMap;

    # invokes: Lcom/google/android/apps/books/api/TranslationServerController;->setCachedLanguages(Lcom/google/common/collect/BiMap;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/api/TranslationServerController;->access$100(Lcom/google/android/apps/books/api/TranslationServerController;Lcom/google/common/collect/BiMap;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->this$1:Lcom/google/android/apps/books/api/TranslationServerController$1;

    iget-object v0, v0, Lcom/google/android/apps/books/api/TranslationServerController$1;->val$callback:Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->this$1:Lcom/google/android/apps/books/api/TranslationServerController$1;

    iget-object v0, v0, Lcom/google/android/apps/books/api/TranslationServerController$1;->val$callback:Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;

    iget-object v1, p0, Lcom/google/android/apps/books/api/TranslationServerController$1$1;->val$results:Lcom/google/common/collect/BiMap;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;->onCompleted(Lcom/google/common/collect/BiMap;)V

    .line 78
    :cond_0
    return-void
.end method
