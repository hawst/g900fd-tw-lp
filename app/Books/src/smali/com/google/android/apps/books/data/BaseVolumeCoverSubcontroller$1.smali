.class Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BaseVolumeCoverSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getImageOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

.field final synthetic val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field final synthetic val$volume:Lcom/google/android/apps/books/model/VolumeData;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    iput-object p6, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 96
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "coverUri":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mCoverHeight:I
    invoke-static {v5}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->access$000(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;)I

    move-result v5

    invoke-interface {v4, v2, v5}, Lcom/google/android/apps/books/net/BooksServer;->getCoverImage(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v1

    .line 99
    .local v1, "content":Ljava/io/InputStream;
    iget-object v4, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    # invokes: Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->saveTempBlob(Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/io/InputStream;)Lcom/google/android/apps/books/data/DataControllerBlob;
    invoke-static {v4, v5, v1}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->access$100(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/io/InputStream;)Lcom/google/android/apps/books/data/DataControllerBlob;

    move-result-object v0

    .line 100
    .local v0, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    iget-object v4, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    # invokes: Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->handleServerResultOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;)V
    invoke-static {v4, p1, v5, v0}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->access$200(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v0    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v1    # "content":Ljava/io/InputStream;
    .end local v2    # "coverUri":Ljava/lang/String;
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v3

    .line 102
    .local v3, "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$1;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;Ljava/lang/Exception;)V
    invoke-static {v4, p1, v5, v3}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->access$300(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
