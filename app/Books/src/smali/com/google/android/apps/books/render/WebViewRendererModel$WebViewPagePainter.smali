.class Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;
.super Ljava/lang/Object;
.source "WebViewRendererModel.java"

# interfaces
.implements Lcom/google/android/apps/books/render/PagePainter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRendererModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WebViewPagePainter"
.end annotation


# instance fields
.field private final mActiveSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

.field private final mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field private mRecycled:Z

.field private final mViewLayerType:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;Lcom/google/android/apps/books/widget/DevicePageRendering;ILjava/util/Set;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "viewLayerType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "I",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1083
    .local p4, "activeSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    .line 1085
    iput-object p2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 1086
    iput p3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mViewLayerType:I

    .line 1087
    iput-object p4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mActiveSet:Ljava/util/Set;

    .line 1088
    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;

    .prologue
    .line 1075
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mRecycled:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;)Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    return-object v0
.end method


# virtual methods
.method public canStillDraw()Z
    .locals 1

    .prologue
    .line 1140
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mRecycled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->canDrawPages()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1108
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mRecycled:Z

    if-eqz v0, :cond_0

    .line 1109
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Painter is recycled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1111
    :cond_0
    const-string v0, "WebViewRendererModel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1112
    const-string v0, "WebViewRendererModel"

    const-string v1, "Drawing from WebView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V

    .line 1115
    return-void
.end method

.method public getOwnedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 1097
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSharedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 1092
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->getPageSize(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 1103
    return-object p1
.end method

.method public getViewLayerType()I
    .locals 1

    .prologue
    .line 1135
    iget v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mViewLayerType:I

    return v0
.end method

.method public isMutableBitmap()Z
    .locals 1

    .prologue
    .line 1130
    const/4 v0, 0x0

    return v0
.end method

.method public isUpdatable()Z
    .locals 1

    .prologue
    .line 1125
    const/4 v0, 0x1

    return v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 1119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mRecycled:Z

    .line 1120
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mActiveSet:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1121
    return-void
.end method
