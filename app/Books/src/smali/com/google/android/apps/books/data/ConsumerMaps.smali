.class public Lcom/google/android/apps/books/data/ConsumerMaps;
.super Ljava/lang/Object;
.source "ConsumerMaps.java"


# direct methods
.method public static addConsumers(Ljava/lang/Object;Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Key:",
            "Ljava/lang/Object;",
            "Value:",
            "Ljava/lang/Object;",
            ">(TKey;",
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<TKey;TValue;>;",
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<TKey;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TValue;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "key":Ljava/lang/Object;, "TKey;"
    .local p1, "consumers":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TKey;TValue;>;"
    .local p2, "saveConsumers":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TKey;Lcom/google/android/apps/books/util/Nothing;>;"
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TValue;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    move v2, v4

    :goto_0
    invoke-static {v2}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkArgument(Z)V

    .line 23
    invoke-virtual {p1, p0, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v0

    .line 24
    .local v0, "wereNoConsumers":Z
    invoke-virtual {p2, p0, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v1

    .line 26
    .local v1, "wereNoSaveConsumers":Z
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    :goto_1
    return v4

    .end local v0    # "wereNoConsumers":Z
    .end local v1    # "wereNoSaveConsumers":Z
    :cond_1
    move v2, v3

    .line 22
    goto :goto_0

    .restart local v0    # "wereNoConsumers":Z
    .restart local v1    # "wereNoSaveConsumers":Z
    :cond_2
    move v4, v3

    .line 26
    goto :goto_1
.end method
