.class public abstract Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;
.super Lcom/google/android/play/onboard/OnboardSimpleQuizPage;
.source "BaseBooksQuizPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;
    }
.end annotation


# static fields
.field private static final COLOR_MUTED_RES_IDS:[I

.field private static final COLOR_RES_IDS:[I


# instance fields
.field private NEARLY_RANDOM_COLOR_MAPPING:[I

.field protected mController:Lcom/google/android/apps/books/app/OnboardingController;

.field protected mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

.field protected mQuizItemDataListFiltered:Lcom/google/android/libraries/bind/data/DataList;

.field private mRandomOffsetIntoColorMapping:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 91
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->COLOR_RES_IDS:[I

    .line 98
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->COLOR_MUTED_RES_IDS:[I

    return-void

    .line 91
    :array_0
    .array-data 4
        0x7f0b001a
        0x7f0b0015
        0x7f0b001b
        0x7f0b001c
    .end array-data

    .line 98
    :array_1
    .array-data 4
        0x7f0b0006
        0x7f0b0009
        0x7f0b0007
        0x7f0b0008
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    const/16 v0, 0x73

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->NEARLY_RANDOM_COLOR_MAPPING:[I

    .line 89
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getNewRandomOffsetIntoColorMap()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mRandomOffsetIntoColorMapping:I

    .line 115
    return-void

    .line 80
    nop

    :array_0
    .array-data 4
        0x3
        0x1
        0x3
        0x2
        0x1
        0x0
        0x3
        0x0
        0x2
        0x1
        0x3
        0x2
        0x1
        0x0
        0x2
        0x1
        0x2
        0x3
        0x1
        0x0
        0x1
        0x3
        0x2
        0x0
        0x1
        0x2
        0x3
        0x1
        0x0
        0x3
        0x0
        0x2
        0x3
        0x1
        0x0
        0x3
        0x2
        0x3
        0x0
        0x1
        0x2
        0x2
        0x1
        0x2
        0x3
        0x2
        0x3
        0x0
        0x3
        0x1
        0x2
        0x0
        0x1
        0x2
        0x1
        0x0
        0x1
        0x3
        0x0
        0x1
        0x3
        0x1
        0x3
        0x2
        0x3
        0x2
        0x3
        0x2
        0x1
        0x3
        0x0
        0x2
        0x3
        0x1
        0x2
        0x1
        0x3
        0x2
        0x0
        0x2
        0x0
        0x3
        0x1
        0x2
        0x0
        0x2
        0x3
        0x0
        0x1
        0x3
        0x1
        0x3
        0x2
        0x3
        0x0
        0x1
        0x2
        0x3
        0x3
        0x0
        0x3
        0x0
        0x2
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x2
        0x0
        0x2
        0x0
        0x1
        0x3
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static addQuizPagesIfAbsent(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 6
    .param p0, "pageList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p1, "controller"    # Lcom/google/android/apps/books/app/OnboardingController;
    .param p2, "booksDataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 152
    const-string v4, "quizGenre"

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    .line 155
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 156
    .local v0, "genrePage":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    const-string v5, "quizGenre"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 157
    sget v4, Lcom/google/android/play/onboard/OnboardPagerAdapter;->DK_PAGE_GENERATOR:I

    new-instance v5, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$1;

    invoke-direct {v5, p1}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$1;-><init>(Lcom/google/android/apps/books/app/OnboardingController;)V

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 165
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v3, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v3}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 168
    .local v3, "samplePage":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    const-string v5, "quizSample"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 169
    sget v4, Lcom/google/android/play/onboard/OnboardPagerAdapter;->DK_PAGE_GENERATOR:I

    new-instance v5, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$2;

    invoke-direct {v5, p1, p2}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$2;-><init>(Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/apps/books/data/BooksDataController;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 178
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    new-instance v2, Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v4

    iget v4, v4, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    invoke-direct {v2, v4, v1}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V

    .line 181
    .local v2, "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/libraries/bind/data/DataChange;->get(ZZ)Lcom/google/android/libraries/bind/data/DataChange;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 183
    .end local v0    # "genrePage":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v2    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    .end local v3    # "samplePage":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-void
.end method

.method protected static getItemContentDescription(Ljava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "itemTitle"    # Ljava/lang/String;
    .param p1, "selected"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    if-eqz p1, :cond_0

    const v0, 0x7f0f0208

    .line 144
    .local v0, "contentDescriptionResId":I
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 141
    .end local v0    # "contentDescriptionResId":I
    :cond_0
    const v0, 0x7f0f0209

    goto :goto_0
.end method

.method private getNewRandomOffsetIntoColorMap()I
    .locals 4

    .prologue
    .line 247
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->NEARLY_RANDOM_COLOR_MAPPING:[I

    array-length v2, v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method protected colorMutedResIdFromIndex(I)I
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 196
    iget v1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mRandomOffsetIntoColorMapping:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->NEARLY_RANDOM_COLOR_MAPPING:[I

    array-length v2, v2

    rem-int v0, v1, v2

    .line 198
    .local v0, "randomIndex":I
    sget-object v1, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->COLOR_MUTED_RES_IDS:[I

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->NEARLY_RANDOM_COLOR_MAPPING:[I

    aget v2, v2, v0

    aget v1, v1, v2

    return v1
.end method

.method protected colorResIdFromIndex(I)I
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 190
    iget v1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mRandomOffsetIntoColorMapping:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->NEARLY_RANDOM_COLOR_MAPPING:[I

    array-length v2, v2

    rem-int v0, v1, v2

    .line 192
    .local v0, "randomIndex":I
    sget-object v1, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->COLOR_RES_IDS:[I

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->NEARLY_RANDOM_COLOR_MAPPING:[I

    aget v2, v2, v0

    aget v1, v1, v2

    return v1
.end method

.method protected columnsPerRow(I)I
    .locals 3
    .param p1, "columnMinWidthResId"    # I

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getColumnsPerRow(Landroid/content/res/Resources;Landroid/view/WindowManager;I)I

    move-result v0

    return v0
.end method

.method protected getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    check-cast v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    return-object v0
.end method

.method public getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getPageCount()I

    move-result v0

    return v0
.end method

.method protected getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 4

    .prologue
    const v3, 0x7f0e002f

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {v0, v3}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    sget-object v1, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->EQUALITY_FIELDS:[I

    new-instance v2, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$BooksReadWriteFilter;-><init>(Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;)V

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/libraries/bind/data/DataList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mQuizItemDataListFiltered:Lcom/google/android/libraries/bind/data/DataList;

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mQuizItemDataListFiltered:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method protected handleNoConnection()V
    .locals 3

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f01e8

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 223
    return-void
.end method

.method public restoreOnboardState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 242
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->restoreOnboardState(Landroid/os/Bundle;)V

    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->getNewRandomOffsetIntoColorMap()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mRandomOffsetIntoColorMapping:I

    .line 244
    return-void
.end method

.method protected serverOrderSortAndUpdateSnapshot(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "snapshotList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    new-instance v0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage$3;-><init>(Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    new-instance v1, Lcom/google/android/libraries/bind/data/Snapshot;

    const v2, 0x7f0e002f

    invoke-direct {v1, v2, p1}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V

    sget-object v2, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 215
    return-void
.end method

.method public setController(Lcom/google/android/apps/books/app/OnboardingController;)V
    .locals 0
    .param p1, "controller"    # Lcom/google/android/apps/books/app/OnboardingController;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->mController:Lcom/google/android/apps/books/app/OnboardingController;

    .line 187
    return-void
.end method
