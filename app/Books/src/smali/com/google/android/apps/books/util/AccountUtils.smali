.class public Lcom/google/android/apps/books/util/AccountUtils;
.super Ljava/lang/Object;
.source "AccountUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    }
.end annotation


# direct methods
.method private static bestSystemAccount(Landroid/content/Context;Z)Landroid/accounts/Account;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "defaultToAny"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 120
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    const-string v8, "com.google"

    invoke-virtual {v7, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 122
    .local v3, "googleAccounts":[Landroid/accounts/Account;
    array-length v7, v3

    if-ne v7, v10, :cond_1

    .line 124
    aget-object v1, v3, v9

    .line 146
    :cond_0
    :goto_0
    return-object v1

    .line 125
    :cond_1
    array-length v7, v3

    if-le v7, v10, :cond_5

    .line 127
    const/4 v1, 0x0

    .line 128
    .local v1, "bestAccount":Landroid/accounts/Account;
    move-object v0, v3

    .local v0, "arr$":[Landroid/accounts/Account;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v2, v0, v4

    .line 130
    .local v2, "candidate":Landroid/accounts/Account;
    invoke-static {p0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/sync/SyncController;->getSyncAutomatically()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 131
    if-nez v1, :cond_3

    .line 132
    move-object v1, v2

    .line 128
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 133
    :cond_3
    if-nez p1, :cond_2

    move-object v1, v6

    .line 136
    goto :goto_0

    .line 140
    .end local v2    # "candidate":Landroid/accounts/Account;
    :cond_4
    if-nez v1, :cond_0

    .line 142
    if-eqz p1, :cond_5

    .line 143
    aget-object v1, v3, v9

    goto :goto_0

    .end local v0    # "arr$":[Landroid/accounts/Account;
    .end local v1    # "bestAccount":Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_5
    move-object v1, v6

    .line 146
    goto :goto_0
.end method

.method public static findAccount([Landroid/accounts/Account;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 7
    .param p0, "accounts"    # [Landroid/accounts/Account;
    .param p1, "target"    # Landroid/accounts/Account;

    .prologue
    const/4 v4, 0x0

    .line 42
    if-nez p1, :cond_1

    move-object v0, v4

    .line 50
    :cond_0
    :goto_0
    return-object v0

    .line 43
    :cond_1
    move-object v1, p0

    .local v1, "arr$":[Landroid/accounts/Account;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v0, v1, v2

    .line 46
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v6, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 43
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "account":Landroid/accounts/Account;
    :cond_3
    move-object v0, v4

    .line 50
    goto :goto_0
.end method

.method public static findIntentAccount(Landroid/content/Context;Landroid/accounts/Account;Z)Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intentAccount"    # Landroid/accounts/Account;
    .param p2, "defaultToAny"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 72
    new-instance v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;

    invoke-direct {v4}, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;-><init>()V

    .line 74
    .local v4, "result":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 75
    .local v2, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    .line 77
    .local v3, "prefsAccount":Landroid/accounts/Account;
    if-eqz p1, :cond_4

    .line 80
    invoke-virtual {p1, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 81
    .local v1, "matchesPrefAccount":Z
    iput-object p1, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    .line 82
    if-nez v1, :cond_3

    :goto_0
    iput-boolean v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->needsAuthorization:Z

    .line 83
    iput-boolean v1, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    .line 95
    .end local v1    # "matchesPrefAccount":Z
    :goto_1
    iget-object v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    if-eqz v5, :cond_2

    .line 97
    invoke-static {p0}, Lcom/google/android/apps/books/util/AccountUtils;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 98
    .local v0, "accounts":[Landroid/accounts/Account;
    iget-object v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    invoke-static {v0, v5}, Lcom/google/android/apps/books/util/AccountUtils;->findAccount([Landroid/accounts/Account;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    .line 99
    iget-object v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    if-nez v5, :cond_2

    if-eqz p2, :cond_2

    .line 102
    const-string v5, "AccountUtils"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 103
    const-string v5, "AccountUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Account not found, from prefs: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    iget-boolean v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    if-eqz v5, :cond_1

    .line 106
    invoke-virtual {v2, v8}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 109
    :cond_1
    invoke-static {p0, v8, p2}, Lcom/google/android/apps/books/util/AccountUtils;->findIntentAccount(Landroid/content/Context;Landroid/accounts/Account;Z)Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;

    move-result-object v4

    .line 113
    .end local v0    # "accounts":[Landroid/accounts/Account;
    .end local v4    # "result":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    :cond_2
    return-object v4

    .restart local v1    # "matchesPrefAccount":Z
    .restart local v4    # "result":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    :cond_3
    move v5, v6

    .line 82
    goto :goto_0

    .line 84
    .end local v1    # "matchesPrefAccount":Z
    :cond_4
    if-eqz v3, :cond_5

    .line 85
    iput-object v3, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    .line 87
    iput-boolean v6, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->needsAuthorization:Z

    .line 88
    iput-boolean v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    goto :goto_1

    .line 90
    :cond_5
    invoke-static {p0, p2}, Lcom/google/android/apps/books/util/AccountUtils;->bestSystemAccount(Landroid/content/Context;Z)Landroid/accounts/Account;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    .line 91
    iput-boolean v5, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->needsAuthorization:Z

    .line 92
    iput-boolean v6, v4, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    goto :goto_1
.end method

.method public static getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 33
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    return-object v1
.end method

.method public static showAddAccount(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Ljava/lang/CharSequence;)V
    .locals 8
    .param p0, "activity"    # Landroid/app/Activity;
    .param p2, "introMessage"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "callback":Landroid/accounts/AccountManagerCallback;, "Landroid/accounts/AccountManagerCallback<Landroid/os/Bundle;>;"
    const/4 v3, 0x0

    .line 162
    invoke-static {p2}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 164
    .local v4, "options":Landroid/os/Bundle;
    const-string v0, "introMessage"

    invoke-virtual {v4, v0, p2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 169
    :goto_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    const-string v2, "print"

    move-object v5, p0

    move-object v6, p1

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 177
    return-void

    .line 166
    .end local v4    # "options":Landroid/os/Bundle;
    :cond_0
    const/4 v4, 0x0

    .restart local v4    # "options":Landroid/os/Bundle;
    goto :goto_0
.end method
