.class Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;
.super Landroid/view/View;
.source "SnapshottingPageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/SnapshottingPageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageViewContentView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .line 338
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 339
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Landroid/content/Context;Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;

    .prologue
    .line 335
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Landroid/content/Context;)V

    return-void
.end method

.method private getMeasure(II)I
    .locals 2
    .param p1, "contentSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 342
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 344
    .local v0, "measureSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    move v0, p1

    .line 350
    .end local v0    # "measureSize":I
    :goto_0
    :sswitch_0
    return v0

    .line 348
    .restart local v0    # "measureSize":I
    :sswitch_1
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 344
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private isReadyToDraw()Z
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1200(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/navigation/PageViewContent;->isReadyToDraw(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x3

    .line 367
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getBounds(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 369
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->isReadyToDraw()Z

    move-result v1

    if-nez v1, :cond_1

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLoadingPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 371
    const-string v1, "NavPageView"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    const-string v1, "NavPageView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " drawing loading color"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 377
    :cond_1
    const-string v1, "NavPageView"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 378
    const-string v1, "NavPageView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " drawing content"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLetterboxPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1400(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1200(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    move-result-object v2

    invoke-interface {v1, v2, p1, v0}, Lcom/google/android/apps/books/navigation/PageViewContent;->draw(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 382
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mDrewCurrentPage:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1502(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Z)Z

    .line 383
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;->onDrewPage()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 356
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # invokes: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getContentWidth()I
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1000(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)I

    move-result v2

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->getMeasure(II)I

    move-result v1

    .line 357
    .local v1, "measuredWidth":I
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # invokes: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getContentHeight()I
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$1100(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)I

    move-result v2

    invoke-direct {p0, v2, p2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->getMeasure(II)I

    move-result v0

    .line 358
    .local v0, "measuredHeight":I
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->setMeasuredDimension(II)V

    .line 359
    return-void
.end method
