.class public final enum Lcom/google/android/apps/books/app/ReaderMenu$Item;
.super Ljava/lang/Enum;
.source "ReaderMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/ReaderMenu$Item;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum ABOUT:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum DISPLAY_OPTIONS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum HELP:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum HOME:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum PLAY_MO:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum READING_MODE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum READ_ALOUD:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum SEARCH:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum SETTINGS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum SHARE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

.field public static final enum TABLE_OF_CONTENTS:Lcom/google/android/apps/books/app/ReaderMenu$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->HOME:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 32
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 33
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "TABLE_OF_CONTENTS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->TABLE_OF_CONTENTS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 34
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "DISPLAY_OPTIONS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->DISPLAY_OPTIONS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 35
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "READING_MODE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->READING_MODE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 36
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "ABOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->ABOUT:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 37
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "SHARE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SHARE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 38
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "BOOKMARK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 39
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "PLAY_MO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->PLAY_MO:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 40
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "READ_ALOUD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->READ_ALOUD:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 41
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "SETTINGS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SETTINGS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 42
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const-string v1, "HELP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->HELP:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 30
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/apps/books/app/ReaderMenu$Item;

    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->HOME:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->TABLE_OF_CONTENTS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->DISPLAY_OPTIONS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->READING_MODE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->ABOUT:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SHARE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->PLAY_MO:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->READ_ALOUD:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SETTINGS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->HELP:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->$VALUES:[Lcom/google/android/apps/books/app/ReaderMenu$Item;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/ReaderMenu$Item;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/ReaderMenu$Item;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;->$VALUES:[Lcom/google/android/apps/books/app/ReaderMenu$Item;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/ReaderMenu$Item;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/ReaderMenu$Item;

    return-object v0
.end method
