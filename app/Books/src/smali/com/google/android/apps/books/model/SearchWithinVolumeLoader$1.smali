.class Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;
.super Landroid/os/AsyncTask;
.source "SearchWithinVolumeLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->loadTask(Lcom/google/android/apps/books/model/SearchResultListener;)Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;

.field final synthetic val$listener:Lcom/google/android/apps/books/model/SearchResultListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->this$0:Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;

    iput-object p2, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->val$listener:Lcom/google/android/apps/books/model/SearchResultListener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 41
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->this$0:Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;

    iget-object v1, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->val$listener:Lcom/google/android/apps/books/model/SearchResultListener;

    # invokes: Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->loadInBackground(Lcom/google/android/apps/books/model/SearchResultListener;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->access$000(Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;Lcom/google/android/apps/books/model/SearchResultListener;)V

    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 41
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;->val$listener:Lcom/google/android/apps/books/model/SearchResultListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/SearchResultListener;->onBookDone()V

    .line 51
    return-void
.end method
