.class public Lcom/google/android/apps/books/provider/VolumeContentStore;
.super Ljava/lang/Object;
.source "VolumeContentStore.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;
.implements Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;,
        Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;
    }
.end annotation


# instance fields
.field private final mBaseDirectorySource:Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

.field private final mCallbacks:Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;)V
    .locals 0
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;
    .param p3, "baseDirectorySource"    # Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mResolver:Landroid/content/ContentResolver;

    .line 94
    iput-object p2, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mCallbacks:Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;

    .line 95
    iput-object p3, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mBaseDirectorySource:Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/provider/VolumeContentStore;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/VolumeContentStore;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createAccountTempFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/provider/VolumeContentStore;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/VolumeContentStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createSharedTempFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/provider/VolumeContentStore;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/VolumeContentStore;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createTempDictionaryFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 2
    .param p1, "file"    # Ljava/io/File;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    new-instance v1, Lcom/google/android/apps/books/provider/VolumeContentStore$3;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore$3;-><init>(Lcom/google/android/apps/books/provider/VolumeContentStore;Ljava/lang/String;)V

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;-><init>(Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;)V

    return-object v0
.end method

.method private clearPages(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 212
    invoke-static {p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 213
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 214
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "content_status"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215
    iget-object v3, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "content_status=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 220
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    move-result v3

    return v3
.end method

.method private clearResources(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 241
    invoke-static {p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 242
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 243
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "content_status"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 244
    iget-object v3, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "content_status=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 248
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildResContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 249
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    move-result v3

    return v3
.end method

.method private clearSections(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 198
    invoke-static {p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 199
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 200
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "content_status"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 201
    iget-object v3, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "content_status=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSectionContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 206
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    move-result v3

    return v3
.end method

.method private clearStructure(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 226
    invoke-static {p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 227
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 228
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "structure_status"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    iget-object v3, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "structure_status=?"

    new-array v5, v6, [Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageStructureDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 235
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    move-result v3

    return v3
.end method

.method public static contextCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/books/provider/VolumeContentStore$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/provider/VolumeContentStore$1;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private createAccountTempFile(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getTempDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createTempContentFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static createFromContext(Landroid/content/Context;Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;)Lcom/google/android/apps/books/provider/VolumeContentStore;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseDirectorySource"    # Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->contextCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;-><init>(Landroid/content/ContentResolver;Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;)V

    return-object v0
.end method

.method private createSharedTempFile()Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getSharedTempDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createTempContentFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private createTempContentFile(Ljava/io/File;)Ljava/io/File;
    .locals 3
    .param p1, "tempDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    const-string v0, "content"

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/books/util/FileUtils;->ensureDir(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private createTempDictionaryFile(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1, "dictionaryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {p1, v0, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private dictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 3
    .param p1, "dictionaryName"    # Ljava/lang/String;

    .prologue
    .line 174
    new-instance v0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/books/provider/VolumeContentStore$5;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore$5;-><init>(Lcom/google/android/apps/books/provider/VolumeContentStore;Ljava/lang/String;)V

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;-><init>(Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;)V

    return-object v0
.end method

.method private declared-synchronized getBaseDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mBaseDirectorySource:Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

    invoke-interface {v0}, Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;->getBaseDirectory()Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private sharedFile(Ljava/io/File;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 165
    new-instance v0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    new-instance v1, Lcom/google/android/apps/books/provider/VolumeContentStore$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/provider/VolumeContentStore$4;-><init>(Lcom/google/android/apps/books/provider/VolumeContentStore;)V

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;-><init>(Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;)V

    return-object v0
.end method

.method public static sourceUsingProvider(Landroid/content/ContentResolver;)Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;
    .locals 1
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/apps/books/provider/VolumeContentStore$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/provider/VolumeContentStore$2;-><init>(Landroid/content/ContentResolver;)V

    return-object v0
.end method

.method private usingExternalStorage()Z
    .locals 3

    .prologue
    .line 282
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mCallbacks:Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;->getInternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 283
    .local v1, "internalStorageDir":Ljava/io/File;
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    .line 284
    .local v0, "currentStorageDir":Ljava/io/File;
    invoke-static {v1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearAllContentFiles(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->clearSections(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 185
    .local v0, "result":Z
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->clearPages(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    and-int/2addr v0, v1

    .line 186
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->clearResources(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    and-int/2addr v0, v1

    .line 187
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->clearStructure(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    and-int/2addr v0, v1

    .line 192
    return v0
.end method

.method public decorateFileNotFoundException(Ljava/io/IOException;Ljava/io/File;)Ljava/io/FileNotFoundException;
    .locals 4
    .param p1, "cause"    # Ljava/io/IOException;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 259
    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error finding "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 263
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->usingExternalStorage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 264
    iget-object v2, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mCallbacks:Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;->externalStorageIsAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 265
    new-instance v1, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;-><init>(Ljava/lang/String;)V

    .line 269
    .local v1, "toThrow":Ljava/io/FileNotFoundException;
    :goto_0
    invoke-virtual {v1, p1}, Ljava/io/FileNotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 274
    :goto_1
    return-object v1

    .line 267
    .end local v1    # "toThrow":Ljava/io/FileNotFoundException;
    :cond_1
    new-instance v1, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;-><init>(Ljava/lang/String;)V

    .restart local v1    # "toThrow":Ljava/io/FileNotFoundException;
    goto :goto_0

    .line 271
    .end local v1    # "toThrow":Ljava/io/FileNotFoundException;
    :cond_2
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    .line 272
    .restart local v1    # "toThrow":Ljava/io/FileNotFoundException;
    invoke-virtual {v1, p1}, Ljava/io/FileNotFoundException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    goto :goto_1
.end method

.method public getDictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "dictionaryName"    # Ljava/lang/String;

    .prologue
    .line 336
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->dictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getDictionaryStorageDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 343
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/books/provider/VolumeContentStore;->mBaseDirectorySource:Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;

    invoke-interface {v1}, Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;->getBaseDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "dictionaries"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 345
    .local v0, "result":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 346
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 348
    :cond_0
    return-object v0
.end method

.method public getPageContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageContentFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPageContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getPageContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getPageStructureContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageStructureFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPageStructureContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getPageStructureContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getResourceContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildResContentFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSectionContentFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSharedResContentFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->sharedFile(Ljava/io/File;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getSharedResourceMD5File(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 1
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSharedResMD5File(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->sharedFile(Ljava/io/File;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getSharedTempDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 329
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSharedResContentDir(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    const-string v2, "temp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTempDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 322
    new-instance v0, Ljava/io/File;

    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "temp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 129
    .local v0, "volumeDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "cover.png"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 136
    .local v0, "volumeDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "cover_thumbnail.png"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeSearchesFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getBaseDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 142
    .local v0, "volumeDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "searches.json"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->accountFile(Ljava/io/File;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    return-object v1
.end method
