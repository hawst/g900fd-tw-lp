.class public Lcom/google/android/apps/books/model/VolumeDataUtils;
.super Ljava/lang/Object;
.source "VolumeDataUtils.java"


# direct methods
.method public static equals(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 4
    .param p0, "v1"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p1, "v2"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 110
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPageCount()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getPageCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLocalCoverUri()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getLocalCoverUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPublisher()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getPublisher()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDate()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalStart()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getRentalStart()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getLastInteraction(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;)J
    .locals 4
    .param p0, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p1, "localData"    # Lcom/google/android/apps/books/model/LocalVolumeData;

    .prologue
    .line 40
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hashCode(Lcom/google/android/apps/books/model/VolumeData;)I
    .locals 4
    .param p0, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 138
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPageCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLocalCoverUri()Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPublisher()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDate()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDescription()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalStart()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static isOnlineOnly(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 1
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 105
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isReadNowVolume(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;J)Z
    .locals 7
    .param p0, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p1, "localData"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p2, "downloadProgress"    # Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .param p3, "lastSyncTime"    # J

    .prologue
    .line 36
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/books/model/VolumeDataUtils;->isVolumeOfNotableInterest(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;JZ)Z

    move-result v0

    return v0
.end method

.method public static isVolumeOfNotableInterest(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;JZ)Z
    .locals 9
    .param p0, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p1, "localData"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p2, "downloadProgress"    # Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .param p3, "lastSyncTime"    # J
    .param p5, "includeNewUnreadUploads"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    if-nez p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v2

    .line 54
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/books/util/RentalUtils;->isExpiredNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 57
    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-eq v4, v5, :cond_0

    .line 60
    invoke-virtual {p2}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->isPartiallyOrFullyDownloaded()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 61
    goto :goto_0

    .line 63
    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    .line 64
    goto :goto_0

    .line 66
    :cond_3
    if-nez p5, :cond_4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 78
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x240c8400

    sub-long v0, v4, v6

    .line 84
    .local v0, "cutoffTime":J
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/VolumeDataUtils;->getLastInteraction(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;)J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-lez v4, :cond_5

    move v2, v3

    .line 85
    goto :goto_0

    .line 98
    :cond_5
    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v4

    cmp-long v4, v4, p3

    if-lez v4, :cond_0

    move v2, v3

    .line 99
    goto :goto_0
.end method

.method public static stringToTtsPermission(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    .locals 4
    .param p0, "ttsPermission"    # Ljava/lang/String;

    .prologue
    .line 25
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 30
    :goto_0
    return-object v1

    .line 26
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VolumeDataUtils"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    const-string v1, "VolumeDataUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad TTS permission value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->NOT_ALLOWED:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    goto :goto_0
.end method

.method public static toString(Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;
    .locals 4
    .param p0, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 150
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "volumeId"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "title"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "author"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "isLimitedPreview"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "isUploaded"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "pageCount"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPageCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "viewability"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "serverCoverUri"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "localCoverUri"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLocalCoverUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "canonicalUrl"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "buyUrl"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "readingPosition"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastAccess"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "etag"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "etag"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "publisher"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPublisher()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "date"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "description"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "language"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "isPublicDomain"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "textToSpeechPermission"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "rentalState"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "rentalStart"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalStart()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "rentalExpiration"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "explicitOfflineLicense"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "maxOfflineDevices"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
