.class public Lcom/google/android/apps/books/app/ReaderMenuImpl;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReaderMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;,
        Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;,
        Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;
    }
.end annotation


# static fields
.field private static final mItemResourceIds:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Lcom/google/android/apps/books/app/ReaderMenu$Item;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActionBar:Landroid/support/v7/app/ActionBar;

.field private final mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

.field private mCollapsingSearchView:Z

.field private final mContext:Landroid/content/Context;

.field private final mMenu:Landroid/view/Menu;

.field private final mNavigator:Lcom/google/android/apps/books/util/Navigator;

.field private final mOnSearchClickListener:Landroid/view/View$OnClickListener;

.field private final mOnSearchCloseListener:Landroid/support/v7/widget/SearchView$OnCloseListener;

.field private final mOnSearchFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private final mOnSearchQueryTextListener:Landroid/support/v7/widget/SearchView$OnQueryTextListener;

.field private mRecentSearches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

.field private mRecentSearchesListView:Landroid/widget/ListView;

.field private mRecentSearchesPopup:Landroid/widget/PopupWindow;

.field private final mSearchView:Landroid/support/v7/widget/SearchView;

.field private final mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

.field private final mTocCallbacks:Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256
    invoke-static {}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->buildItemResourceIdMap()Lcom/google/common/collect/BiMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mItemResourceIds:Lcom/google/common/collect/BiMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/apps/books/util/Navigator;Landroid/support/v7/app/ActionBar;Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/TableOfContents;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "menu"    # Landroid/view/Menu;
    .param p3, "inflater"    # Landroid/view/MenuInflater;
    .param p4, "navigator"    # Lcom/google/android/apps/books/util/Navigator;
    .param p5, "actionBar"    # Landroid/support/v7/app/ActionBar;
    .param p6, "callbacks"    # Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
    .param p7, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p8, "tableOfContentsActionItem"    # Lcom/google/android/apps/books/widget/TableOfContents;

    .prologue
    const/4 v4, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v2, Lcom/google/android/apps/books/app/ReaderMenuImpl$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$2;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTocCallbacks:Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;

    .line 353
    new-instance v2, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Lcom/google/android/apps/books/app/ReaderMenuImpl$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchCloseListener:Landroid/support/v7/widget/SearchView$OnCloseListener;

    .line 367
    new-instance v2, Lcom/google/android/apps/books/app/ReaderMenuImpl$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$3;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 383
    new-instance v2, Lcom/google/android/apps/books/app/ReaderMenuImpl$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$4;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchClickListener:Landroid/view/View$OnClickListener;

    .line 89
    const-string v2, "Null context"

    invoke-static {p1, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string v2, "Null menu"

    invoke-static {p2, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    const-string v2, "Null inflater"

    invoke-static {p3, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    const-string v2, "Null navigator"

    invoke-static {p4, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    const-string v2, "Null action bar helper"

    invoke-static {p5, v2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mContext:Landroid/content/Context;

    .line 96
    const v2, 0x7f120001

    invoke-virtual {p3, v2, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 97
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    .line 98
    iput-object p4, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mNavigator:Lcom/google/android/apps/books/util/Navigator;

    .line 99
    iput-object p5, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mActionBar:Landroid/support/v7/app/ActionBar;

    .line 100
    iput-object p6, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    .line 101
    iput-object p8, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mNavigator:Lcom/google/android/apps/books/util/Navigator;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/TableOfContents;->setNavigator(Lcom/google/android/apps/books/util/Navigator;)V

    .line 105
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v2, p7}, Lcom/google/android/apps/books/widget/TableOfContents;->updateSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 107
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 108
    .local v1, "searchViewInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0400bd

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/SearchView;

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    const v3, 0x7f0e020f

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 115
    .local v0, "searchItem":Landroid/view/MenuItem;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-static {v0, v2}, Landroid/support/v4/view/MenuItemCompat;->setActionView(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 116
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/SearchView;->setOnSearchClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchCloseListener:Landroid/support/v7/widget/SearchView$OnCloseListener;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/SearchView;->setOnCloseListener(Landroid/support/v7/widget/SearchView$OnCloseListener;)V

    .line 118
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 119
    new-instance v2, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Lcom/google/android/apps/books/app/ReaderMenuImpl$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchQueryTextListener:Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    .line 120
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mOnSearchQueryTextListener:Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0111

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    new-instance v3, Lcom/google/android/apps/books/app/ReaderMenuImpl$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$1;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/SearchView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 133
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->createSearchMenuExpandListener()Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    const v3, 0x7f0901aa

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->configureSearchViewLayoutParams(Landroid/support/v7/widget/SearchView;I)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->updateIconVisibilities()V

    .line 137
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->dismissRecentSearchesPopup()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearches:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/ReaderMenuImpl;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->showFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/app/ReaderMenuImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCollapsingSearchView:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->shouldShowRecentSearchesPopup()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/support/v7/widget/SearchView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method private static buildItemResourceIdMap()Lcom/google/common/collect/BiMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Lcom/google/android/apps/books/app/ReaderMenu$Item;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    invoke-static {}, Lcom/google/common/collect/HashBiMap;->create()Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    .line 260
    .local v0, "result":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Lcom/google/android/apps/books/app/ReaderMenu$Item;Ljava/lang/Integer;>;"
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->HOME:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x102002c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e020f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->TABLE_OF_CONTENTS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0213

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->DISPLAY_OPTIONS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0215

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->READING_MODE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0216

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->ABOUT:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0217

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SHARE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0218

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0219

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->PLAY_MO:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e0214

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->READ_ALOUD:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e021a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SETTINGS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e021c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->HELP:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const v2, 0x7f0e021d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    return-object v0
.end method

.method private collapseSearchView()V
    .locals 2

    .prologue
    .line 346
    const v1, 0x7f0e020f

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->findMenuItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 347
    .local v0, "searchItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 349
    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    .line 351
    :cond_0
    return-void
.end method

.method private configureSearchViewLayoutParams(Landroid/support/v7/widget/SearchView;I)V
    .locals 2
    .param p1, "searchView"    # Landroid/support/v7/widget/SearchView;
    .param p2, "searchWidth"    # I

    .prologue
    .line 140
    new-instance v0, Landroid/support/v7/app/ActionBar$LayoutParams;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/support/v7/app/ActionBar$LayoutParams;-><init>(I)V

    .line 141
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1}, Landroid/support/v7/widget/SearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 142
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/SearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    return-void
.end method

.method private createSearchMenuExpandListener()Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;
    .locals 1

    .prologue
    .line 418
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$5;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    return-object v0
.end method

.method private dismissRecentSearchesPopup()V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 415
    :cond_0
    return-void
.end method

.method private static doOnPreDraw(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 610
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenuImpl$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl$7;-><init>(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 619
    .local v0, "listener":Landroid/view/ViewTreeObserver$OnPreDrawListener;
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 620
    return-void
.end method

.method private findMenuItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method private getFilteredRecentSearches(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p2, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 500
    .local p1, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 501
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 510
    :cond_0
    return-object v0

    .line 503
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v0, "filteredRecentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 505
    .local v2, "lowercasePrefix":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 506
    .local v3, "next":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 507
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private isScreenBigEnoughForExtraButtons(Landroid/content/res/Configuration;I)Z
    .locals 2
    .param p1, "configuration"    # Landroid/content/res/Configuration;
    .param p2, "numButtons"    # I

    .prologue
    .line 221
    iget v0, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    .line 222
    .local v0, "width":I
    add-int/lit8 v1, p2, -0x1

    mul-int/lit8 v1, v1, 0x30

    add-int/lit16 v1, v1, 0x226

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private populateFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)Z
    .locals 4
    .param p2, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 450
    .local p1, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->getFilteredRecentSearches(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 452
    .local v0, "filteredRecentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "ReaderMenu"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453
    const-string v1, "ReaderMenu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Recent searches: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " prefix: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Filtered recent searches:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 457
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->populateRecentSearchesPopup(Ljava/util/List;)V

    .line 458
    const/4 v1, 0x1

    .line 460
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private populateRecentSearchesPopup(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    .local p1, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 466
    const-string v0, "ReaderMenu"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    const-string v0, "ReaderMenu"

    const-string v1, "Should not call populateRecentSearchesPopup with empty list!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    if-nez v0, :cond_2

    .line 475
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 481
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesListView:Landroid/widget/ListView;

    if-nez v0, :cond_3

    .line 482
    new-instance v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesListView:Landroid/widget/ListView;

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 487
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    if-nez v0, :cond_4

    .line 488
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 491
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->clear()V

    .line 492
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->addAll(Ljava/util/Collection;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesAdapter:Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$RecentSearchesAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private setItemLabel(II)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "titleId"    # I

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->findMenuItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 340
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 341
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 343
    :cond_0
    return-void
.end method

.method private setItemVisible(IZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 332
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->findMenuItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 333
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 334
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 336
    :cond_0
    return-void
.end method

.method private shouldShowRecentSearchesPopup()Z
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .param p2, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 536
    .local p1, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->populateFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    .line 539
    .local v0, "hasFilteredResults":Z
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    new-instance v2, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl$6;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Z)V

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->doOnPreDraw(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 553
    :cond_0
    return-void
.end method


# virtual methods
.method public clearSearchViewFocus()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 302
    :cond_0
    return-void
.end method

.method public exitSearch()V
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCollapsingSearchView:Z

    if-nez v0, :cond_0

    .line 307
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->collapseSearchView()V

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->clearSearchViewFocus()V

    .line 310
    return-void
.end method

.method public getSearchQuery()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 171
    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mItemResourceIds:Lcom/google/common/collect/BiMap;

    invoke-interface {v1}, Lcom/google/common/collect/BiMap;->inverse()Lcom/google/common/collect/BiMap;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .line 173
    .local v0, "item":Lcom/google/android/apps/books/app/ReaderMenu$Item;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;->onItemSelected(Lcom/google/android/apps/books/app/ReaderMenu$Item;)V

    .line 174
    return-void
.end method

.method public onRecentSearchesChanged(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearches:Ljava/util/List;

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearches:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->showFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method public onSearchRequested()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0e020f

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->expandActionView(Landroid/view/MenuItem;)Z

    .line 290
    return-void
.end method

.method public setBookmarkIsAdd(Z)V
    .locals 2
    .param p1, "add"    # Z

    .prologue
    .line 282
    if-eqz p1, :cond_0

    const v0, 0x7f0f0143

    .line 284
    .local v0, "titleId":I
    :goto_0
    const v1, 0x7f0e0219

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->setItemLabel(II)V

    .line 285
    return-void

    .line 282
    .end local v0    # "titleId":I
    :cond_0
    const v0, 0x7f0f0144

    goto :goto_0
.end method

.method public setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V
    .locals 1
    .param p1, "item"    # Lcom/google/android/apps/books/app/ReaderMenu$Item;
    .param p2, "visible"    # Z

    .prologue
    .line 277
    sget-object v0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mItemResourceIds:Lcom/google/common/collect/BiMap;

    invoke-interface {v0, p1}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->setItemVisible(IZ)V

    .line 278
    return-void
.end method

.method public setMoItemState(Landroid/content/Context;ZZZ)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "visible"    # Z
    .param p3, "active"    # Z
    .param p4, "canResume"    # Z

    .prologue
    .line 185
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    const v6, 0x7f0e0214

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 186
    .local v3, "moItem":Landroid/view/MenuItem;
    if-eqz p2, :cond_0

    .line 188
    if-eqz p3, :cond_1

    const v2, 0x7f0f007d

    .line 190
    .local v2, "labelId":I
    :goto_0
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 192
    if-eqz p3, :cond_3

    const v1, 0x7f010169

    .line 193
    .local v1, "iconId":I
    :goto_1
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 194
    .local v4, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v1, v4, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 195
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v6, v4, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 197
    .local v0, "actionIcon":Landroid/graphics/drawable/Drawable;
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 201
    .end local v0    # "actionIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "iconId":I
    .end local v2    # "labelId":I
    .end local v4    # "outValue":Landroid/util/TypedValue;
    :cond_0
    invoke-interface {v3, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 202
    return-void

    .line 188
    :cond_1
    if-eqz p4, :cond_2

    const v2, 0x7f0f007e

    goto :goto_0

    :cond_2
    const v2, 0x7f0f007c

    goto :goto_0

    .line 192
    .restart local v2    # "labelId":I
    :cond_3
    const v1, 0x7f010168

    goto :goto_1
.end method

.method public setPosition(Lcom/google/android/apps/books/common/Position;)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/TableOfContents;->setPosition(Lcom/google/android/apps/books/common/Position;)V

    .line 295
    return-void
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/TableOfContents;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 254
    return-void
.end method

.method public setReadingModeItemState(ZLcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 3
    .param p1, "visible"    # Z
    .param p2, "otherMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    const v2, 0x7f0e0216

    .line 245
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->setItemVisible(IZ)V

    .line 246
    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne p2, v1, :cond_0

    const v0, 0x7f0f0068

    .line 248
    .local v0, "titleId":I
    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->setItemLabel(II)V

    .line 249
    return-void

    .line 246
    .end local v0    # "titleId":I
    :cond_0
    const v0, 0x7f0f0069

    goto :goto_0
.end method

.method public setSearchMode(Z)V
    .locals 3
    .param p1, "searchMode"    # Z

    .prologue
    .line 178
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0e0212

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 180
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSearchQuery(Ljava/lang/CharSequence;Z)V
    .locals 1
    .param p1, "query"    # Ljava/lang/CharSequence;
    .param p2, "submit"    # Z

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 320
    return-void
.end method

.method public setTtsItemState(ZZZLandroid/app/Activity;)V
    .locals 4
    .param p1, "visible"    # Z
    .param p2, "active"    # Z
    .param p3, "available"    # Z
    .param p4, "activity"    # Landroid/app/Activity;

    .prologue
    .line 228
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    const v3, 0x7f0e021a

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 229
    .local v1, "ttsItem":Landroid/view/MenuItem;
    if-eqz p1, :cond_0

    .line 231
    if-nez p3, :cond_1

    .line 232
    const v0, 0x7f0f006d

    .line 233
    .local v0, "menuTextId":I
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 238
    :goto_0
    invoke-virtual {p4, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 240
    .end local v0    # "menuTextId":I
    :cond_0
    invoke-interface {v1, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 241
    return-void

    .line 235
    :cond_1
    if-eqz p2, :cond_2

    const v0, 0x7f0f006e

    .line 236
    .restart local v0    # "menuTextId":I
    :goto_1
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 235
    .end local v0    # "menuTextId":I
    :cond_2
    const v0, 0x7f0f006c

    goto :goto_1
.end method

.method public setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "annotationSet"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTocCallbacks:Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/books/widget/TableOfContents;->setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;)V

    .line 167
    return-void
.end method

.method public showRecentSearchesPopup(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 516
    .local p1, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearches:Ljava/util/List;

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearches:Ljava/util/List;

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->showFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)V

    .line 518
    return-void
.end method

.method public tearDown()V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TableOfContents;->dismiss()V

    .line 325
    return-void
.end method

.method public updateIconVisibilities()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 206
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mMenu:Landroid/view/Menu;

    const v5, 0x7f0e0214

    invoke-interface {v4, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 208
    .local v1, "moItem":Landroid/view/MenuItem;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 211
    .local v0, "configuration":Landroid/content/res/Configuration;
    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->isScreenBigEnoughForExtraButtons(Landroid/content/res/Configuration;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 214
    .local v2, "showMoIcon":Z
    :goto_0
    if-eqz v2, :cond_0

    const/4 v3, 0x2

    :cond_0
    invoke-static {v1, v3}, Landroid/support/v4/view/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    .line 216
    return-void

    .end local v2    # "showMoIcon":Z
    :cond_1
    move v2, v3

    .line 211
    goto :goto_0
.end method

.method public updateSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/TableOfContents;->updateSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 442
    return-void
.end method
