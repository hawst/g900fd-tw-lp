.class public Lcom/google/android/apps/books/navigation/PageListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PageListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;
.implements Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
.implements Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;


# instance fields
.field private final mAllowPageInteraction:Z

.field private final mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

.field private mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

.field private mHideCurrentSpread:Z

.field private mHighlightCurrentSpread:Z

.field private mIsDestroyed:Z

.field private final mLetterboxColor:I

.field private mListView:Lcom/google/android/ublib/widget/AbsWarpListView;

.field private final mLoadingColor:I

.field private final mMaxPageWidth:I

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private final mOwner:Ljava/lang/ref/Reference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/Reference",
            "<",
            "Lcom/google/android/apps/books/util/Destroyable;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

.field private final mRightToLeft:Z

.field private final mRowHeight:I

.field private mSelectedPosition:I

.field private final mSelectedSpreadPadding:Landroid/graphics/Rect;

.field private final mSelectedSpreadShadowId:I

.field private final mShowPageNumbers:Z

.field private final mSideClicksScroll:Z

.field private final mSpreadPadding:Landroid/graphics/Rect;

.field private final mSpreadScale:F

.field private final mSpreadShadowId:I

.field private mTempSize:Landroid/graphics/Point;

.field private final mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/util/Destroyable;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/navigation/PageRangeLoader;Lcom/google/android/ublib/widget/AbsWarpListView;IIZZZLandroid/graphics/Point;IIZ)V
    .locals 10
    .param p1, "owner"    # Lcom/google/android/apps/books/util/Destroyable;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p3, "pageLoader"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p4, "listView"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p5, "thumbnailFraction"    # I
    .param p6, "maxWidth"    # I
    .param p7, "showPageNumbers"    # Z
    .param p8, "rightToLeft"    # Z
    .param p9, "allowPageInteraction"    # Z
    .param p10, "fullScreenSpreadSize"    # Landroid/graphics/Point;
    .param p11, "spreadShadowId"    # I
    .param p12, "selectedSpreadShadowId"    # I
    .param p13, "sideClicksScroll"    # Z

    .prologue
    .line 92
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 53
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadPadding:Landroid/graphics/Rect;

    .line 54
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadPadding:Landroid/graphics/Rect;

    .line 64
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHighlightCurrentSpread:Z

    .line 66
    new-instance v7, Lcom/google/android/apps/books/render/SpreadItems;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    .line 68
    const v7, 0x7fffffff

    iput v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedPosition:I

    .line 77
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSize:Landroid/graphics/Point;

    .line 253
    new-instance v7, Lcom/google/android/apps/books/navigation/PageListAdapter$2;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/navigation/PageListAdapter$2;-><init>(Lcom/google/android/apps/books/navigation/PageListAdapter;)V

    iput-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 93
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 94
    iput-object p3, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .line 95
    new-instance v7, Ljava/lang/ref/WeakReference;

    invoke-direct {v7, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mOwner:Ljava/lang/ref/Reference;

    .line 96
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mShowPageNumbers:Z

    .line 97
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mRightToLeft:Z

    .line 98
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mAllowPageInteraction:Z

    .line 99
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSideClicksScroll:Z

    .line 101
    invoke-virtual {p4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 103
    .local v2, "res":Landroid/content/res/Resources;
    invoke-interface {p2}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->getTheme()Ljava/lang/String;

    move-result-object v6

    .line 105
    .local v6, "theme":Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mLoadingColor:I

    .line 106
    invoke-static {v6}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedLetterboxColorId(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mLetterboxColor:I

    .line 108
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadShadowId:I

    .line 109
    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadShadowId:I

    .line 111
    move/from16 v0, p11

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 112
    .local v4, "spreadBackground":Landroid/graphics/drawable/Drawable;
    move/from16 v0, p12

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 114
    .local v3, "selectedSpreadBackground":Landroid/graphics/drawable/Drawable;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadPadding:Landroid/graphics/Rect;

    invoke-virtual {v4, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 115
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadPadding:Landroid/graphics/Rect;

    invoke-virtual {v3, v7}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 117
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadPadding:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadPadding:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadPadding:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 120
    .local v1, "paddingHeight":I
    if-eqz p7, :cond_0

    .line 121
    invoke-virtual {p4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageNumberHeight(Landroid/content/Context;)I

    move-result v7

    add-int/2addr v1, v7

    .line 128
    :cond_0
    invoke-virtual {p4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v1

    div-int/2addr v7, p5

    mul-int/2addr v7, p5

    iput v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mRowHeight:I

    .line 131
    invoke-virtual {p3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v7

    if-eqz v7, :cond_1

    div-int/lit8 v7, p6, 0x2

    :goto_0
    iput v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mMaxPageWidth:I

    .line 132
    iput-object p4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 134
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSize:Landroid/graphics/Point;

    move-object/from16 v0, p10

    iget v8, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p10

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Point;->set(II)V

    .line 135
    new-instance v5, Lcom/google/android/apps/books/util/Holder;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/google/android/apps/books/util/Holder;-><init>(Ljava/lang/Object;)V

    .line 136
    .local v5, "spreadScale":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Float;>;"
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSize:Landroid/graphics/Point;

    iget v8, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mRowHeight:I

    move/from16 v0, p6

    invoke-static {v7, v0, v8, v5}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;IILcom/google/android/apps/books/util/Holder;)V

    .line 137
    invoke-virtual {v5}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadScale:F

    .line 139
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    new-instance v8, Lcom/google/android/apps/books/navigation/PageListAdapter$1;

    invoke-direct {v8, p0, p5}, Lcom/google/android/apps/books/navigation/PageListAdapter$1;-><init>(Lcom/google/android/apps/books/navigation/PageListAdapter;I)V

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->setListener(Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;)V

    .line 161
    invoke-virtual {p4}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162
    return-void

    .end local v5    # "spreadScale":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Float;>;"
    :cond_1
    move/from16 v7, p6

    .line 131
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/PageListAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageListAdapter;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mMaxPageWidth:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/navigation/PageListAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageListAdapter;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mRowHeight:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/navigation/PageListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageListAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/navigation/PageListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageListAdapter;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSideClicksScroll:Z

    return v0
.end method

.method private populateView(ILcom/google/android/apps/books/navigation/SnapshottingSpreadView;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "view"    # Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .prologue
    const/4 v3, 0x0

    .line 231
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mAllowPageInteraction:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    :goto_0
    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->prepare(Lcom/google/android/apps/books/widget/OnPageTouchListener;)V

    .line 232
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setAlpha(F)V

    .line 234
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHighlightCurrentSpread:Z

    if-eqz v2, :cond_2

    if-nez p1, :cond_2

    const/4 v0, 0x1

    .line 236
    .local v0, "isSelected":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 237
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .line 238
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHideCurrentSpread:Z

    if-eqz v2, :cond_0

    .line 239
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setAlpha(F)V

    .line 247
    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPages(ILcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 249
    if-eqz v0, :cond_4

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadShadowId:I

    .line 250
    .local v1, "shadowId":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    iget-boolean v3, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mShowPageNumbers:Z

    iget-boolean v4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mRightToLeft:Z

    invoke-virtual {p2, v2, v1, v3, v4}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setPages(Lcom/google/android/apps/books/render/SpreadItems;IZZ)V

    .line 251
    return-void

    .end local v0    # "isSelected":Z
    .end local v1    # "shadowId":I
    :cond_1
    move-object v2, v3

    .line 231
    goto :goto_0

    .line 234
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 242
    .restart local v0    # "isSelected":Z
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    if-ne v2, p2, :cond_0

    .line 243
    iput-object v3, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    goto :goto_2

    .line 249
    :cond_4
    iget v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadShadowId:I

    goto :goto_3
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mIsDestroyed:Z

    if-nez v0, :cond_0

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mIsDestroyed:Z

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->destroy()V

    .line 307
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastDisplaySpreadOffset()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getCurrentSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 186
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNegativeCount()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstDisplaySpreadOffset()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    return-object v0
.end method

.method public getSelectedPosition()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedPosition:I

    return v0
.end method

.method public getSelectedView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 2

    .prologue
    .line 355
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedPosition:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 356
    const/4 v0, 0x0

    .line 358
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedPosition:I

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 278
    const-class v1, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    const v2, 0x7f0400c2

    invoke-static {v1, p2, p3, v2}, Lcom/google/android/apps/books/util/ViewUtils;->getChildView(Ljava/lang/Class;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .line 282
    .local v0, "view":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    if-eq v0, p2, :cond_0

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->getBookmarkController()Lcom/google/android/apps/books/app/BookmarkController;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadScale:F

    iget v3, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mLoadingColor:I

    iget v4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mLetterboxColor:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->init(Lcom/google/android/apps/books/app/BookmarkController;FII)V

    .line 287
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->populateView(ILcom/google/android/apps/books/navigation/SnapshottingSpreadView;)V

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    return-object v0
.end method

.method public getViewSize(I)I
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 217
    iget-boolean v4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHighlightCurrentSpread:Z

    if-eqz v4, :cond_0

    if-nez p1, :cond_0

    const/4 v1, 0x1

    .line 218
    .local v1, "isSelected":Z
    :goto_0
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedSpreadPadding:Landroid/graphics/Rect;

    .line 220
    .local v2, "padding":Landroid/graphics/Rect;
    :goto_1
    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    add-int v3, v4, v5

    .line 222
    .local v3, "width":I
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4, p1, v5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPages(ILcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 224
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/navigation/PageViewContent;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mTempSize:Landroid/graphics/Point;

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/navigation/PageViewContent;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    add-int/2addr v3, v4

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 217
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "padding":Landroid/graphics/Rect;
    .end local v3    # "width":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 218
    .restart local v1    # "isSelected":Z
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSpreadPadding:Landroid/graphics/Rect;

    goto :goto_1

    .line 227
    .restart local v0    # "i":I
    .restart local v2    # "padding":Landroid/graphics/Rect;
    .restart local v3    # "width":I
    :cond_2
    return v3
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public isDestroyed()Z
    .locals 2

    .prologue
    .line 310
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mOwner:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/Destroyable;

    .line 311
    .local v0, "owner":Lcom/google/android/apps/books/util/Destroyable;
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mIsDestroyed:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/books/util/Destroyable;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isViewPinned(Landroid/view/View;I)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public onScroll(Lcom/google/android/ublib/widget/AbsWarpListView;III)V
    .locals 4
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 324
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getCenterPosition()I

    move-result v0

    .line 325
    .local v0, "center":I
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFirstVisiblePosition()I

    move-result v1

    .local v1, "pos":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLastVisiblePosition()I

    move-result v3

    if-gt v1, v3, :cond_0

    .line 327
    invoke-virtual {p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .line 329
    .local v2, "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    sub-int v3, v1, v0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setDistanceFromCenter(I)V

    .line 326
    invoke-virtual {p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getNextVisiblePosition(I)I

    move-result v1

    goto :goto_0

    .line 331
    .end local v2    # "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 2
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 316
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mListView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFinalCenterPosition()I

    move-result v0

    .line 318
    .local v0, "centerPosition":I
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->setCenterSpreadOffset(I)V

    .line 319
    return-void
.end method

.method public preload(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 299
    return-void
.end method

.method public setHideCurrentSpread(Z)V
    .locals 2
    .param p1, "toHide"    # Z

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHideCurrentSpread:Z

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    if-eqz v0, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCurrentSpread:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHideCurrentSpread:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setAlpha(F)V

    .line 213
    :cond_0
    return-void

    .line 211
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public setHighlightCurrentSpread(Z)V
    .locals 0
    .param p1, "toHighlight"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mHighlightCurrentSpread:Z

    .line 192
    return-void
.end method

.method public setViewVisible(Landroid/view/View;IZ)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "isVisible"    # Z

    .prologue
    .line 335
    if-eqz p3, :cond_0

    move-object v0, p1

    .line 336
    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .line 337
    .local v0, "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->hasDestroyedPage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->populateView(ILcom/google/android/apps/books/navigation/SnapshottingSpreadView;)V

    .line 341
    .end local v0    # "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    :cond_0
    invoke-virtual {p1, p3}, Landroid/view/View;->setFocusable(Z)V

    .line 342
    return-void
.end method

.method public showSpreadInFullView(I)V
    .locals 4
    .param p1, "spreadPosition"    # I

    .prologue
    .line 165
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 168
    .local v0, "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iput p1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mSelectedPosition:I

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->CHOSE_PAGE_IN_SKIM:Lcom/google/android/apps/books/app/MoveType;

    const/4 v3, 0x1

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->showSpreadInFullView(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Z)V

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageListAdapter;->mPageLoader:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->exportPages(I)V

    .line 172
    return-void
.end method

.method public transformView(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 352
    return-void
.end method
