.class Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;
.super Ljava/lang/Object;
.source "WhitespaceCompressingTextBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SlidingWindow"
.end annotation


# instance fields
.field private final mStringBuilder:Ljava/lang/StringBuilder;

.field final synthetic this$0:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;)V
    .locals 1

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->this$0:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mStringBuilder:Ljava/lang/StringBuilder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$1;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;-><init>(Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;)V

    return-void
.end method

.method private mapOffsetToBufferIndex(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->this$0:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    # getter for: Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mHeadOffset:I
    invoke-static {v0}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->access$000(Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;)I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method


# virtual methods
.method public append(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    return-void
.end method

.method public charAt(I)C
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mapOffsetToBufferIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    return v0
.end method

.method public delete(I)I
    .locals 3
    .param p1, "end"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mapOffsetToBufferIndex(I)I

    move-result v0

    .line 71
    .local v0, "amountToDelete":I
    iget-object v1, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 72
    return v0
.end method

.method public substring(I)Ljava/lang/String;
    .locals 2
    .param p1, "start"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->mapOffsetToBufferIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
