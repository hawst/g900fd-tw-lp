.class Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageTurnCallbacks"
.end annotation


# instance fields
.field private mTurningFromEob:Z

.field private mTurningToEob:Z

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 3618
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 3618
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    return-void
.end method

.method private isTurningFromEobPage(Lcom/google/android/apps/books/util/ScreenDirection;)Z
    .locals 4
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3763
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->showingEndOfBookPage()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3764
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v2, v3, :cond_2

    .line 3765
    sget-object v2, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v2, :cond_1

    .line 3769
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 3765
    goto :goto_0

    .line 3767
    :cond_2
    sget-object v2, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 3769
    goto :goto_0
.end method

.method private isTurningToEobPage(Lcom/google/android/apps/books/util/ScreenDirection;)Z
    .locals 8
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 3727
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v6

    if-nez v6, :cond_0

    .line 3755
    :goto_0
    return v5

    .line 3740
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v6, v7, :cond_3

    .line 3741
    sget-object v6, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v6, :cond_2

    move v2, v4

    .line 3742
    .local v2, "turningTowardEob":Z
    :goto_1
    const/4 v1, 0x1

    .line 3743
    .local v1, "onePageOffset":I
    const/4 v3, 0x2

    .line 3750
    .local v3, "twoPageOffset":I
    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v1

    if-eq v6, v7, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5600(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v3

    if-ne v6, v7, :cond_5

    :cond_1
    move v0, v4

    .line 3755
    .local v0, "onLastPage":Z
    :goto_3
    if-eqz v0, :cond_6

    if-eqz v2, :cond_6

    :goto_4
    move v5, v4

    goto :goto_0

    .end local v0    # "onLastPage":Z
    .end local v1    # "onePageOffset":I
    .end local v2    # "turningTowardEob":Z
    .end local v3    # "twoPageOffset":I
    :cond_2
    move v2, v5

    .line 3741
    goto :goto_1

    .line 3745
    :cond_3
    sget-object v6, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v6, :cond_4

    move v2, v4

    .line 3746
    .restart local v2    # "turningTowardEob":Z
    :goto_5
    const/4 v1, -0x1

    .line 3747
    .restart local v1    # "onePageOffset":I
    const/4 v3, 0x0

    .restart local v3    # "twoPageOffset":I
    goto :goto_2

    .end local v1    # "onePageOffset":I
    .end local v2    # "turningTowardEob":Z
    .end local v3    # "twoPageOffset":I
    :cond_4
    move v2, v5

    .line 3745
    goto :goto_5

    .restart local v1    # "onePageOffset":I
    .restart local v2    # "turningTowardEob":Z
    .restart local v3    # "twoPageOffset":I
    :cond_5
    move v0, v5

    .line 3750
    goto :goto_3

    .restart local v0    # "onLastPage":Z
    :cond_6
    move v4, v5

    .line 3755
    goto :goto_4
.end method

.method private okToTurnPageBackward()Z
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 3641
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/common/Position;

    move-result-object v7

    if-nez v7, :cond_1

    move v5, v6

    .line 3678
    :cond_0
    :goto_0
    return v5

    .line 3645
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->showingEndOfBookPage()Z

    move-result v7

    if-nez v7, :cond_0

    .line 3649
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5600(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v0

    .line 3650
    .local v0, "displayTwoPages":Z
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5700(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v7

    if-nez v7, :cond_3

    .line 3651
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/WritingDirection;->dotProduct(Lcom/google/android/apps/books/util/WritingDirection;I)I

    move-result v2

    .line 3658
    .local v2, "offsetFromBaseWritingDir":I
    if-eqz v0, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/books/util/WritingDirection;->LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v6, v7, :cond_2

    .line 3661
    .local v4, "prevPageDistanceFromBasePosition":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/common/Position;

    move-result-object v6

    sub-int v7, v2, v4

    invoke-static {v6, v7}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/render/Renderer;->pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z

    move-result v5

    goto :goto_0

    .end local v4    # "prevPageDistanceFromBasePosition":I
    :cond_2
    move v4, v5

    .line 3658
    goto :goto_1

    .line 3665
    .end local v2    # "offsetFromBaseWritingDir":I
    :cond_3
    if-eqz v0, :cond_5

    move v3, v4

    .line 3666
    .local v3, "pageTurnDistance":I
    :goto_2
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5700(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 3667
    .local v1, "firstBadSlot":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v7, v8, :cond_6

    .line 3673
    if-eqz v0, :cond_4

    .line 3674
    add-int/lit8 v1, v1, 0x1

    .line 3676
    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v7

    add-int/2addr v7, v3

    if-lt v7, v1, :cond_0

    move v5, v6

    goto :goto_0

    .end local v1    # "firstBadSlot":I
    .end local v3    # "pageTurnDistance":I
    :cond_5
    move v3, v5

    .line 3665
    goto :goto_2

    .line 3678
    .restart local v1    # "firstBadSlot":I
    .restart local v3    # "pageTurnDistance":I
    :cond_6
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v7

    sub-int/2addr v7, v3

    if-gt v7, v1, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method private okToTurnPageForward()Z
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3687
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/common/Position;

    move-result-object v8

    if-nez v8, :cond_1

    .line 3720
    :cond_0
    :goto_0
    return v7

    .line 3691
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->showingEndOfBookPage()Z

    move-result v8

    if-nez v8, :cond_0

    .line 3695
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5600(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v0

    .line 3696
    .local v0, "displayTwoPages":Z
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v8

    if-nez v8, :cond_3

    .line 3697
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v8

    invoke-static {v7, v8}, Lcom/google/android/apps/books/util/WritingDirection;->dotProduct(Lcom/google/android/apps/books/util/WritingDirection;I)I

    move-result v3

    .line 3703
    .local v3, "offsetFromBaseWritingDir":I
    if-eqz v0, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v7, v8, :cond_2

    .line 3706
    .local v2, "nextPageDistanceFromBasePosition":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/common/Position;

    move-result-object v7

    add-int v8, v3, v2

    invoke-static {v7, v8}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/render/Renderer;->pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z

    move-result v7

    goto :goto_0

    .end local v2    # "nextPageDistanceFromBasePosition":I
    :cond_2
    move v2, v6

    .line 3703
    goto :goto_1

    .line 3710
    .end local v3    # "offsetFromBaseWritingDir":I
    :cond_3
    if-eqz v0, :cond_4

    move v4, v2

    .line 3714
    .local v4, "pageTurnDistance":I
    :goto_2
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->endOfBookPageSequenceLength()I

    move-result v5

    .line 3715
    .local v5, "pagesToShowAfterVolumeEnd":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v8, v9, :cond_6

    .line 3716
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sub-int v1, v8, v5

    .line 3717
    .local v1, "firstBadSlot":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v8

    sub-int/2addr v8, v4

    if-le v8, v1, :cond_5

    :goto_3
    move v7, v6

    goto :goto_0

    .end local v1    # "firstBadSlot":I
    .end local v4    # "pageTurnDistance":I
    .end local v5    # "pagesToShowAfterVolumeEnd":I
    :cond_4
    move v4, v6

    .line 3710
    goto :goto_2

    .restart local v1    # "firstBadSlot":I
    .restart local v4    # "pageTurnDistance":I
    .restart local v5    # "pagesToShowAfterVolumeEnd":I
    :cond_5
    move v6, v7

    .line 3717
    goto :goto_3

    .line 3719
    .end local v1    # "firstBadSlot":I
    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int v1, v8, v5

    .line 3720
    .restart local v1    # "firstBadSlot":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v8

    add-int/2addr v8, v4

    if-ge v8, v1, :cond_7

    :goto_4
    move v7, v6

    goto/16 :goto_0

    :cond_7
    move v6, v7

    goto :goto_4
.end method


# virtual methods
.method public canTurn(Lcom/google/android/apps/books/util/ScreenDirection;)Z
    .locals 2
    .param p1, "screenDirection"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 3625
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/ReadingDirection;->fromScreenDirection(Lcom/google/android/apps/books/util/ScreenDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ReadingDirection;

    move-result-object v0

    .line 3628
    .local v0, "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    sget-object v1, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    if-ne v0, v1, :cond_0

    .line 3630
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->okToTurnPageForward()Z

    move-result v1

    .line 3632
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->okToTurnPageBackward()Z

    move-result v1

    goto :goto_0
.end method

.method public directionTowardPosition(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 7
    .param p1, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 3907
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v5

    invoke-interface {v5, p1}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v3

    .line 3909
    .local v3, "newSpi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-nez v3, :cond_1

    .line 3910
    const-string v5, "PagesViewHelper"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3911
    const-string v5, "PagesViewHelper"

    const-string v6, "directionTowardPosition can\'t get spread for new position"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3913
    :cond_0
    const/4 v5, 0x0

    .line 3929
    :goto_0
    return-object v5

    .line 3916
    :cond_1
    iget-object v2, v3, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 3917
    .local v2, "newSi":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 3918
    .local v0, "currentSi":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v5

    invoke-interface {v5, v0, v2}, Lcom/google/android/apps/books/render/Renderer;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I

    move-result v1

    .line 3920
    .local v1, "distance":I
    const v5, 0x7fffffff

    if-ne v1, v5, :cond_2

    .line 3921
    const/4 v4, 0x0

    .line 3929
    .local v4, "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/ScreenDirection;->fromReadingDirection(Lcom/google/android/apps/books/util/ReadingDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v5

    goto :goto_0

    .line 3922
    .end local v4    # "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    :cond_2
    if-gez v1, :cond_3

    .line 3923
    sget-object v4, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    .restart local v4    # "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    goto :goto_1

    .line 3924
    .end local v4    # "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    :cond_3
    if-lez v1, :cond_4

    .line 3925
    sget-object v4, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    .restart local v4    # "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    goto :goto_1

    .line 3927
    .end local v4    # "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    :cond_4
    const/4 v4, 0x0

    .restart local v4    # "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    goto :goto_1
.end method

.method public finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V
    .locals 18
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "flinging"    # Z
    .param p3, "velocity"    # F
    .param p4, "isCompletion"    # Z

    .prologue
    .line 3846
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    if-eqz v5, :cond_8

    .line 3847
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/BookView;->getView()Landroid/view/View;

    move-result-object v4

    .line 3848
    .local v4, "bookView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getEndOfBookView()Landroid/view/View;

    move-result-object v16

    .line 3850
    .local v16, "eobView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-eqz v5, :cond_1

    if-nez p4, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    if-eqz v5, :cond_3

    if-nez p4, :cond_3

    :cond_2
    const/4 v15, 0x1

    .line 3852
    .local v15, "endOnEobPage":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v5, v6, :cond_4

    const/16 v17, 0x1

    .line 3857
    .local v17, "rtl":Z
    :goto_1
    if-eqz v17, :cond_5

    if-eqz v15, :cond_5

    .line 3858
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v7, v5

    .line 3859
    .local v7, "bookViewTranslation":F
    const/4 v11, 0x0

    .line 3871
    .local v11, "eobViewTranslation":F
    :goto_2
    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6300(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    move-result-object v9

    const-string v10, "PagesViewHelper"

    move/from16 v5, p2

    move/from16 v6, p3

    invoke-static/range {v4 .. v10}, Lcom/google/android/apps/books/util/ViewUtils;->finish2DPageTurn(Landroid/view/View;ZFFZLcom/google/android/apps/books/widget/PagesView$Callbacks;Ljava/lang/String;)V

    .line 3873
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6300(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    move-result-object v13

    const-string v14, "PagesViewHelper"

    move-object/from16 v8, v16

    move/from16 v9, p2

    move/from16 v10, p3

    invoke-static/range {v8 .. v14}, Lcom/google/android/apps/books/util/ViewUtils;->finish2DPageTurn(Landroid/view/View;ZFFZLcom/google/android/apps/books/widget/PagesView$Callbacks;Ljava/lang/String;)V

    .line 3879
    .end local v4    # "bookView":Landroid/view/View;
    .end local v7    # "bookViewTranslation":F
    .end local v11    # "eobViewTranslation":F
    .end local v15    # "endOnEobPage":Z
    .end local v16    # "eobView":Landroid/view/View;
    .end local v17    # "rtl":Z
    :goto_3
    return-void

    .line 3850
    .restart local v4    # "bookView":Landroid/view/View;
    .restart local v16    # "eobView":Landroid/view/View;
    :cond_3
    const/4 v15, 0x0

    goto :goto_0

    .line 3852
    .restart local v15    # "endOnEobPage":Z
    :cond_4
    const/16 v17, 0x0

    goto :goto_1

    .line 3860
    .restart local v17    # "rtl":Z
    :cond_5
    if-eqz v17, :cond_6

    .line 3861
    const/4 v7, 0x0

    .line 3862
    .restart local v7    # "bookViewTranslation":F
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v11, v5

    .restart local v11    # "eobViewTranslation":F
    goto :goto_2

    .line 3863
    .end local v7    # "bookViewTranslation":F
    .end local v11    # "eobViewTranslation":F
    :cond_6
    if-eqz v15, :cond_7

    .line 3864
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v7, v5

    .line 3865
    .restart local v7    # "bookViewTranslation":F
    const/4 v11, 0x0

    .restart local v11    # "eobViewTranslation":F
    goto :goto_2

    .line 3867
    .end local v7    # "bookViewTranslation":F
    .end local v11    # "eobViewTranslation":F
    :cond_7
    const/4 v7, 0x0

    .line 3868
    .restart local v7    # "bookViewTranslation":F
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v11, v5

    .restart local v11    # "eobViewTranslation":F
    goto :goto_2

    .line 3877
    .end local v4    # "bookView":Landroid/view/View;
    .end local v7    # "bookViewTranslation":F
    .end local v11    # "eobViewTranslation":F
    .end local v15    # "endOnEobPage":Z
    .end local v16    # "eobView":Landroid/view/View;
    .end local v17    # "rtl":Z
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-interface {v5, v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/PagesView;->finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V

    goto :goto_3
.end method

.method public onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 3
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "completed"    # Z

    .prologue
    const/4 v2, 0x0

    .line 3883
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSwipeDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6002(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    .line 3885
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    if-eqz v0, :cond_3

    .line 3886
    :cond_0
    if-eqz p2, :cond_1

    .line 3887
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    # setter for: Lcom/google/android/apps/books/widget/PagesViewController;->mShowingEndOfBookPage:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6402(Lcom/google/android/apps/books/widget/PagesViewController;Z)Z

    .line 3888
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateEobStatus()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6500(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 3890
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    .line 3891
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    .line 3897
    :goto_0
    if-eqz p2, :cond_2

    .line 3898
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onCompletedPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 3900
    :cond_2
    return-void

    .line 3893
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v1

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/books/widget/PagesView;->onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZI)V

    .line 3894
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->showBookView()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6600(Lcom/google/android/apps/books/widget/PagesViewController;)V

    goto :goto_0
.end method

.method public onIsTurningChanged(Z)V
    .locals 2
    .param p1, "isTurning"    # Z

    .prologue
    .line 3934
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mLastTurnFromUser:Z
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6700(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onIsTurningChanged(ZZ)V

    .line 3935
    return-void
.end method

.method public onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 4
    .param p1, "screenDirection"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    const/4 v3, 0x3

    .line 3777
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->isTurningToEobPage(Lcom/google/android/apps/books/util/ScreenDirection;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    .line 3778
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->isTurningFromEobPage(Lcom/google/android/apps/books/util/ScreenDirection;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    .line 3780
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    if-nez v1, :cond_1

    .line 3781
    const-string v1, "PagesViewHelper"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3782
    const-string v1, "PagesViewHelper"

    const-string v2, "Hiding BookView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3784
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/BookView;->setVisibility(I)V

    .line 3787
    :cond_1
    const-string v1, "PagesViewHelper"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3788
    const-string v1, "PagesViewHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startTurn anchor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dir "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3792
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # setter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSwipeDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6002(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    .line 3793
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 3794
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onStartedPageTurn()V

    .line 3798
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/ReadingDirection;->fromScreenDirection(Lcom/google/android/apps/books/util/ScreenDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ReadingDirection;

    move-result-object v0

    .line 3800
    .local v0, "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_4

    sget-object v1, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    if-ne v0, v1, :cond_6

    :cond_4
    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->NEXT_PAGE:Lcom/google/android/apps/books/app/MoveType;

    :goto_0
    # setter for: Lcom/google/android/apps/books/widget/PagesViewController;->mLastAction:Lcom/google/android/apps/books/app/MoveType;
    invoke-static {v2, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6102(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/app/MoveType;)Lcom/google/android/apps/books/app/MoveType;

    .line 3802
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSaveLastMove:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$6202(Lcom/google/android/apps/books/widget/PagesViewController;Z)Z

    .line 3804
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    if-nez v1, :cond_5

    .line 3805
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/widget/PagesView;->onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 3807
    :cond_5
    return-void

    .line 3800
    :cond_6
    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->PREV_PAGE:Lcom/google/android/apps/books/app/MoveType;

    goto :goto_0
.end method

.method public setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;F)V
    .locals 10
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "fraction"    # F

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 3811
    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningFromEob:Z

    if-eqz v7, :cond_6

    .line 3812
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/BookView;->getView()Landroid/view/View;

    move-result-object v0

    .line 3813
    .local v0, "bookView":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getEndOfBookView()Landroid/view/View;

    move-result-object v3

    .line 3815
    .local v3, "eobView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v1, v7

    .line 3816
    .local v1, "bookViewWidth":F
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v4, v7

    .line 3817
    .local v4, "eobViewWidth":F
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v7, v8, :cond_2

    const/4 v6, 0x1

    .line 3822
    .local v6, "rtl":Z
    :goto_0
    if-eqz v6, :cond_3

    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-eqz v7, :cond_3

    .line 3823
    mul-float v2, v1, p2

    .line 3824
    .local v2, "bookViewX":F
    sub-float v7, p2, v9

    mul-float v5, v4, v7

    .line 3835
    .local v5, "eobViewX":F
    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 3836
    invoke-virtual {v3, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 3841
    .end local v0    # "bookView":Landroid/view/View;
    .end local v1    # "bookViewWidth":F
    .end local v2    # "bookViewX":F
    .end local v3    # "eobView":Landroid/view/View;
    .end local v4    # "eobViewWidth":F
    .end local v5    # "eobViewX":F
    .end local v6    # "rtl":Z
    :cond_1
    :goto_2
    return-void

    .line 3817
    .restart local v0    # "bookView":Landroid/view/View;
    .restart local v1    # "bookViewWidth":F
    .restart local v3    # "eobView":Landroid/view/View;
    .restart local v4    # "eobViewWidth":F
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 3825
    .restart local v6    # "rtl":Z
    :cond_3
    if-eqz v6, :cond_4

    .line 3826
    sub-float v7, v9, p2

    mul-float v2, v1, v7

    .line 3827
    .restart local v2    # "bookViewX":F
    neg-float v7, p2

    mul-float v5, v4, v7

    .restart local v5    # "eobViewX":F
    goto :goto_1

    .line 3828
    .end local v2    # "bookViewX":F
    .end local v5    # "eobViewX":F
    :cond_4
    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->mTurningToEob:Z

    if-eqz v7, :cond_5

    .line 3829
    neg-float v7, p2

    mul-float v2, v1, v7

    .line 3830
    .restart local v2    # "bookViewX":F
    sub-float v7, v9, p2

    mul-float v5, v4, v7

    .restart local v5    # "eobViewX":F
    goto :goto_1

    .line 3832
    .end local v2    # "bookViewX":F
    .end local v5    # "eobViewX":F
    :cond_5
    sub-float v7, p2, v9

    mul-float v2, v1, v7

    .line 3833
    .restart local v2    # "bookViewX":F
    mul-float v5, v4, p2

    .restart local v5    # "eobViewX":F
    goto :goto_1

    .line 3838
    .end local v0    # "bookView":Landroid/view/View;
    .end local v1    # "bookViewWidth":F
    .end local v2    # "bookViewX":F
    .end local v3    # "eobView":Landroid/view/View;
    .end local v4    # "eobViewWidth":F
    .end local v5    # "eobViewX":F
    .end local v6    # "rtl":Z
    :cond_6
    if-eqz p1, :cond_1

    .line 3839
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I
    invoke-static {v8}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v8

    invoke-interface {v7, p1, p2, v8}, Lcom/google/android/apps/books/widget/PagesView;->setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;FI)V

    goto :goto_2
.end method
