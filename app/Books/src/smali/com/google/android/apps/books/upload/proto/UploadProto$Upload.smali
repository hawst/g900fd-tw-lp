.class public final Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "UploadProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/proto/UploadProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Upload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;,
        Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;


# instance fields
.field private fileSize_:I

.field private hasFileSize:Z

.field private hasId:Z

.field private hasOrigFileName:Z

.field private hasStatus:Z

.field private hasTransferHandle:Z

.field private hasUploadPercentage:Z

.field private hasVolumeId:Z

.field private id_:Ljava/lang/String;

.field private memoizedSerializedSize:I

.field private origFileName_:Ljava/lang/String;

.field private status_:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field private transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;

.field private uploadPercentage_:I

.field private volumeId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 584
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 585
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto;->internalForceInit()V

    .line 586
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    invoke-direct {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->initFields()V

    .line 587
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->id_:Ljava/lang/String;

    .line 91
    iput v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->uploadPercentage_:I

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->origFileName_:Ljava/lang/String;

    .line 119
    iput v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->fileSize_:I

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->volumeId_:Ljava/lang/String;

    .line 164
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/upload/proto/UploadProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->id_:Ljava/lang/String;

    .line 91
    iput v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->uploadPercentage_:I

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->origFileName_:Ljava/lang/String;

    .line 119
    iput v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->fileSize_:I

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->volumeId_:Ljava/lang/String;

    .line 164
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasOrigFileName:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->origFileName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasFileSize:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # I

    .prologue
    .line 11
    iput p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->fileSize_:I

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasVolumeId:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->volumeId_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->id_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasUploadPercentage:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # I

    .prologue
    .line 11
    iput p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->uploadPercentage_:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasStatus:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->status_:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->INITIALIZED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->status_:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 132
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getDefaultInstance()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 133
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    .locals 1

    .prologue
    .line 269
    # invokes: Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->create()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->access$100()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getFileSize()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->fileSize_:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->origFileName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->memoizedSerializedSize:I

    .line 167
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 199
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 169
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 174
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasUploadPercentage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 175
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getUploadPercentage()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 178
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 179
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 182
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 183
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    .line 186
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasOrigFileName()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 187
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 190
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasFileSize()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 191
    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getFileSize()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 194
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasVolumeId()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 195
    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 198
    :cond_7
    iput v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->memoizedSerializedSize:I

    move v1, v0

    .line 199
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->status_:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    return-object v0
.end method

.method public getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->transferHandle_:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object v0
.end method

.method public getUploadPercentage()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->uploadPercentage_:I

    return v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->volumeId_:Ljava/lang/String;

    return-object v0
.end method

.method public hasFileSize()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasFileSize:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasId:Z

    return v0
.end method

.method public hasOrigFileName()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasOrigFileName:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasStatus:Z

    return v0
.end method

.method public hasTransferHandle()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle:Z

    return v0
.end method

.method public hasUploadPercentage()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasUploadPercentage:Z

    return v0
.end method

.method public hasVolumeId()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasVolumeId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getSerializedSize()I

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 144
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasUploadPercentage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getUploadPercentage()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 147
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 150
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasTransferHandle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 153
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasOrigFileName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 154
    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 156
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasFileSize()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 157
    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getFileSize()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 159
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->hasVolumeId()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 160
    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 162
    :cond_6
    return-void
.end method
