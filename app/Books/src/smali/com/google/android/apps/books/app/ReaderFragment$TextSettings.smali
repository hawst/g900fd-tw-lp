.class Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;
.super Ljava/lang/Object;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextSettings"
.end annotation


# instance fields
.field lineHeight:F

.field textZoom:F

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;FF)V
    .locals 0
    .param p2, "textZoom"    # F
    .param p3, "lineHeight"    # F

    .prologue
    .line 6214
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6215
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->setTextZoom(F)V

    .line 6216
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->setLineHeight(F)V

    .line 6217
    return-void
.end method


# virtual methods
.method setLineHeight(F)V
    .locals 0
    .param p1, "lineHeight"    # F

    .prologue
    .line 6224
    iput p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->lineHeight:F

    .line 6225
    return-void
.end method

.method setTextZoom(F)V
    .locals 0
    .param p1, "textZoom"    # F

    .prologue
    .line 6220
    iput p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->textZoom:F

    .line 6221
    return-void
.end method
