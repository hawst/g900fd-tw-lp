.class Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;
.super Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;
.source "Logging.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/Logging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExpensivePerformanceTracker"
.end annotation


# instance fields
.field private mCheckpointAllocCount:I

.field private mCheckpointAllocSize:I

.field private mCheckpointTime:J

.field private final mDescription:Ljava/lang/String;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mStartAllocCount:I

.field private final mStartAllocSize:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)V
    .locals 3
    .param p1, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/google/android/apps/books/util/Logging$CompletionCallback;

    .prologue
    .line 91
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;-><init>(Lcom/google/android/apps/books/util/Logging$CompletionCallback;)V

    .line 92
    iput-object p1, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 93
    iput-object p2, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mDescription:Ljava/lang/String;

    .line 95
    const-class v1, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;

    monitor-enter v1

    .line 96
    :try_start_0
    # ++operator for: Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I
    invoke-static {}, Lcom/google/android/apps/books/util/Logging;->access$004()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 101
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    .line 103
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->getStartTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointTime:J

    .line 106
    invoke-static {}, Landroid/os/Debug;->getThreadAllocCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mStartAllocCount:I

    iput v0, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointAllocCount:I

    .line 107
    invoke-static {}, Landroid/os/Debug;->getThreadAllocSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mStartAllocSize:I

    iput v0, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointAllocSize:I

    .line 108
    return-void

    .line 103
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;Lcom/google/android/apps/books/util/Logging$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/util/Logger;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/apps/books/util/Logging$CompletionCallback;
    .param p4, "x3"    # Lcom/google/android/apps/books/util/Logging$1;

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;-><init>(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)V

    return-void
.end method


# virtual methods
.method public checkpoint(Ljava/lang/String;)V
    .locals 15
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 113
    .local v4, "now":J
    invoke-static {}, Landroid/os/Debug;->getThreadAllocCount()I

    move-result v0

    .line 114
    .local v0, "allocCount":I
    invoke-static {}, Landroid/os/Debug;->getThreadAllocSize()I

    move-result v2

    .line 116
    .local v2, "allocSize":I
    iget-wide v10, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointTime:J

    sub-long v8, v4, v10

    .line 117
    .local v8, "timeDelta":J
    iget v10, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointAllocCount:I

    sub-int v1, v0, v10

    .line 118
    .local v1, "allocDelta":I
    iget v10, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointAllocSize:I

    sub-int v6, v2, v10

    .line 119
    .local v6, "sizeDelta":I
    const-string v7, "%s %s:\n  Allocations: %d\n  Heap size delta: %d\n  Time: %dms"

    .line 122
    .local v7, "template":Ljava/lang/String;
    sget-object v10, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v11, "%s %s:\n  Allocations: %d\n  Heap size delta: %d\n  Time: %dms"

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mDescription:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object p1, v12, v13

    const/4 v13, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 124
    .local v3, "message":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v11, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v10, v11, v3}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 126
    iput-wide v4, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointTime:J

    .line 127
    iput v0, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointAllocCount:I

    .line 128
    iput v2, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mCheckpointAllocSize:I

    .line 129
    return-void
.end method

.method public done()J
    .locals 11

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;->done()J

    move-result-wide v2

    .line 135
    .local v2, "elapsedTime":J
    invoke-static {}, Landroid/os/Debug;->getThreadAllocCount()I

    move-result v6

    iget v7, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mStartAllocCount:I

    sub-int v0, v6, v7

    .line 136
    .local v0, "allocDelta":I
    invoke-static {}, Landroid/os/Debug;->getThreadAllocSize()I

    move-result v6

    iget v7, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mStartAllocSize:I

    sub-int v4, v6, v7

    .line 137
    .local v4, "sizeDelta":I
    const-string v5, "Completed %s:\n  Allocations: %d\n  Heap size delta: %d\n  Time: %dms"

    .line 140
    .local v5, "template":Ljava/lang/String;
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v7, "Completed %s:\n  Allocations: %d\n  Heap size delta: %d\n  Time: %dms"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mDescription:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "message":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v7, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v6, v7, v1}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 144
    const-class v7, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;

    monitor-enter v7

    .line 145
    :try_start_0
    # --operator for: Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I
    invoke-static {}, Lcom/google/android/apps/books/util/Logging;->access$006()I

    move-result v6

    if-nez v6, :cond_0

    .line 146
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    .line 148
    :cond_0
    monitor-exit v7

    .line 149
    return-wide v2

    .line 148
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method
