.class public Lcom/google/android/apps/books/model/StubBooksDataListener;
.super Ljava/lang/Object;
.source "StubBooksDataListener.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismissedRecommendations(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "dismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    return-void
.end method

.method public onLocalVolumeData(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    return-void
.end method

.method public onMyEbooksVolumes(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "myEbooksVolumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    return-void
.end method

.method public onNewResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    return-void
.end method

.method public onNewSegmentResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    return-void
.end method

.method public onNewUpload(Lcom/google/android/apps/books/upload/Upload;)V
    .locals 0
    .param p1, "upload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    .line 18
    return-void
.end method

.method public onOffersData(Ljava/util/List;ZJ)V
    .locals 0
    .param p2, "fromServer"    # Z
    .param p3, "lastUpdateTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;ZJ)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    return-void
.end method

.method public onRecentSearchesChanged(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p2, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public onRequestedDictionaryLanguages(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public onUploadDeleted(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 27
    return-void
.end method

.method public onUploadProgressUpdate(Ljava/lang/String;I)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "uploadPercentage"    # I

    .prologue
    .line 21
    return-void
.end method

.method public onUploadStatusUpdate(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 24
    return-void
.end method

.method public onUploadTooLarge()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public onUploadVolumeIdUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "uploadId"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 54
    return-void
.end method

.method public onUploads(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
    .locals 0
    .param p1, "uploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 30
    return-void
.end method

.method public onVolumeData(Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 50
    return-void
.end method

.method public onVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    return-void
.end method

.method public onVolumeManifest(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "manifest"    # Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    return-void
.end method
