.class Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;
.super Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;
.source "NativeHeapBitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/NativeHeapBitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingBitmap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/NativeHeapBitmapCache",
        "<TT;>.BitmapSource;"
    }
.end annotation


# instance fields
.field final mBitmap:Landroid/graphics/Bitmap;

.field final mBitmapState:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)V
    .locals 2
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "cacheWidth"    # I
    .param p4, "cacheHeight"    # I
    .param p5, "cacheConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 222
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    iput-object p1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    .line 223
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;-><init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;IILandroid/graphics/Bitmap$Config;)V

    .line 218
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmapState:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 224
    iput-object p2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 225
    return-void
.end method


# virtual methods
.method getBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 234
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmapState:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 237
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmapState:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method getScaledBitmap()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    const/4 v7, 0x0

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->needsResize()Z

    move-result v2

    if-nez v2, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 256
    :goto_0
    return-object v0

    .line 251
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;
    invoke-static {v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$300(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mWidth:I

    iget v4, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mHeight:I

    iget-object v5, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mConfig:Landroid/graphics/Bitmap$Config;

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/books/util/BitmapReusePool;->obtain(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 252
    .local v0, "scaledBitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 253
    .local v1, "scaledCanvas":Landroid/graphics/Canvas;
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    new-instance v4, Landroid/graphics/Rect;

    iget v5, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mWidth:I

    iget v6, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mHeight:I

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v5, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$400(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 255
    iget-object v2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->releaseBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method needsResize()Z
    .locals 2

    .prologue
    .line 228
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    iget v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mWidth:I

    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mHeight:I

    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mConfig:Landroid/graphics/Bitmap$Config;

    iget-object v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap$Config;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method releaseBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 241
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.PendingBitmap;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmap:Landroid/graphics/Bitmap;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->mBitmapState:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$PendingBitmap;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    # getter for: Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->mReusePool:Lcom/google/android/apps/books/util/BitmapReusePool;
    invoke-static {v0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;->access$300(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;)Lcom/google/android/apps/books/util/BitmapReusePool;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/BitmapReusePool;->release(Landroid/graphics/Bitmap;)V

    .line 244
    :cond_1
    return-void
.end method
