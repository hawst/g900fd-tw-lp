.class Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;
.super Lcom/google/android/apps/books/app/AbstractSetting;
.source "BaseTestingOptionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Setting"
.end annotation


# instance fields
.field private final mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V
    .locals 2
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "key"    # Lcom/google/android/apps/books/util/ConfigValue;
    .param p3, "choices"    # [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .prologue
    .line 189
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/books/util/ConfigValue;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;[Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    .line 190
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;[Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V
    .locals 1
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "keys"    # [Lcom/google/android/apps/books/util/ConfigValue;
    .param p3, "choices"    # [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/AbstractSetting;-><init>(Ljava/lang/String;[Lcom/google/android/apps/books/util/ConfigValue;)V

    .line 176
    invoke-static {p3, p2}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->prependDefault([Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;[Lcom/google/android/apps/books/util/ConfigValue;)[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .line 177
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;)[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    return-object v0
.end method

.method private createAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008

    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->getChoiceLabels()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 241
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 242
    return-object v0
.end method

.method private getChoiceLabels()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 194
    .local v3, "labels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .local v0, "arr$":[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v0, v2

    .line 195
    .local v1, "choice":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    iget-object v5, v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;->label:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 197
    .end local v1    # "choice":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    :cond_0
    return-object v3
.end method

.method private static prependDefault([Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;[Lcom/google/android/apps/books/util/ConfigValue;)[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .locals 5
    .param p0, "choices"    # [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .param p1, "keys"    # [Lcom/google/android/apps/books/util/ConfigValue;

    .prologue
    const/4 v4, 0x0

    .line 180
    array-length v2, p0

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .line 182
    .local v1, "newChoices":[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    array-length v2, p1

    new-array v0, v2, [Ljava/lang/String;

    .line 183
    .local v0, "nNulls":[Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    const-string v3, "Default"

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v2, v1, v4

    .line 184
    const/4 v2, 0x1

    array-length v3, p0

    invoke-static {p0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 185
    return-object v1
.end method


# virtual methods
.method protected createValidView(Landroid/app/Activity;Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)Landroid/view/View;
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "res"    # Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;
    .param p3, "listener"    # Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;

    .prologue
    .line 215
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    iget v4, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewLayoutId:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 217
    .local v2, "view":Landroid/view/View;
    iget v3, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewLabelViewId:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 218
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget v3, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewSpinnerViewId:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 221
    .local v0, "spinner":Landroid/widget/Spinner;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->createAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 222
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->findCurrentPosition(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 223
    new-instance v3, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;

    invoke-direct {v3, p0, p3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting$1;-><init>(Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 234
    return-object v2
.end method

.method public findCurrentPosition(Landroid/content/Context;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 202
    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->mChoices:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    aget-object v1, v3, v0

    .line 203
    .local v1, "settingChoice":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    iget-object v2, v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;->values:[Ljava/lang/String;

    .line 204
    .local v2, "values":[Ljava/lang/String;
    invoke-virtual {p0, v2, p1}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;->areCurrentValues([Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    .end local v0    # "i":I
    .end local v1    # "settingChoice":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .end local v2    # "values":[Ljava/lang/String;
    :goto_1
    return v0

    .line 201
    .restart local v0    # "i":I
    .restart local v1    # "settingChoice":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .restart local v2    # "values":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    .end local v1    # "settingChoice":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .end local v2    # "values":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
