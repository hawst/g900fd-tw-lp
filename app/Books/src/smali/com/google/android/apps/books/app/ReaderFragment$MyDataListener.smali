.class Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDataListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 8390
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 8390
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public onRecentSearchesChanged(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 8403
    .local p2, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8404
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/app/ReaderMenu;->onRecentSearchesChanged(Ljava/util/List;)V

    .line 8406
    :cond_0
    return-void
.end method

.method public onVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 8394
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 8395
    .local v0, "ourVolumeProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    if-eqz v0, :cond_0

    .line 8396
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17202(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeDownloadProgress;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 8397
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->resetUsableContentFormats()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8399
    :cond_0
    return-void
.end method
