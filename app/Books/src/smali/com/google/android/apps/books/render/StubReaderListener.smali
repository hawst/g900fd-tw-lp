.class public Lcom/google/android/apps/books/render/StubReaderListener;
.super Ljava/lang/Object;
.source "StubReaderListener.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ReaderListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 86
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 90
    return-void
.end method

.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 111
    return-void
.end method

.method public onChapterReady(ILcom/google/android/apps/books/util/JsPerformanceMetrics;)V
    .locals 0
    .param p1, "chapterIndex"    # I
    .param p2, "performanceMetrics"    # Lcom/google/android/apps/books/util/JsPerformanceMetrics;

    .prologue
    .line 40
    return-void
.end method

.method public onChapterUnloaded(I)V
    .locals 0
    .param p1, "chapterIndex"    # I

    .prologue
    .line 44
    return-void
.end method

.method public onDocumentChanged()V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 82
    return-void
.end method

.method public onInvalidPosition(II)V
    .locals 0
    .param p1, "margin"    # I
    .param p2, "requestId"    # I

    .prologue
    .line 63
    return-void
.end method

.method public onJsApiReady()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "textPointsJson"    # Ljava/lang/String;
    .param p4, "handlesPointsJson"    # Ljava/lang/String;
    .param p5, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 78
    return-void
.end method

.method public onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V
    .locals 0
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    return-void
.end method

.method public onMissingPosition(I)V
    .locals 0
    .param p1, "requestId"    # I

    .prologue
    .line 67
    return-void
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I

    .prologue
    .line 99
    return-void
.end method

.method public onPageData(IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "readingPosition"    # Ljava/lang/String;
    .param p6, "bounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p7, "firstPositionOnNextPageInPassage"    # Lcom/google/android/apps/books/common/Position;
    .param p8, "pageText"    # Ljava/lang/String;
    .param p9, "debugText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p4, "allPositionsOnThisPage":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    .local p5, "touchableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    return-void
.end method

.method public onPageLoaded(III)V
    .locals 0
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "requestId"    # I

    .prologue
    .line 52
    return-void
.end method

.method public onPageRangeReady(II)V
    .locals 0
    .param p1, "passageIndex"    # I
    .param p2, "lastReadyPageIndex"    # I

    .prologue
    .line 48
    return-void
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    return-void
.end method

.method public onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 0
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 72
    return-void
.end method

.method public onPassagesPurged(ILjava/util/Collection;)V
    .locals 0
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p2, "passages":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    return-void
.end method

.method public onPreferencesApplied()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public onReaderInitialized()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public onReaderReady()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
