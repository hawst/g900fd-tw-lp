.class Lcom/google/android/apps/books/widget/StaggeredCoverView$2;
.super Ljava/lang/Object;
.source "StaggeredCoverView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView;->maybeRunAnimation(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V
    .locals 0

    .prologue
    .line 726
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$2;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 730
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 734
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$2;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    # invokes: Lcom/google/android/apps/books/widget/StaggeredCoverView;->recordLocations()V
    invoke-static {v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->access$200(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V

    .line 735
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$2;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 736
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$2;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 737
    .local v0, "child":Landroid/view/View;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 735
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 739
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 743
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 747
    return-void
.end method
