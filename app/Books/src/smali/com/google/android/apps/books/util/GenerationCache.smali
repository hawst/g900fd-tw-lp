.class public Lcom/google/android/apps/books/util/GenerationCache;
.super Ljava/lang/Object;
.source "GenerationCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mCache:Ljava/util/HashMap;

.field private final mMaxCapacity:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxCapacity"    # I

    .prologue
    .line 44
    .local p0, "this":Lcom/google/android/apps/books/util/GenerationCache;, "Lcom/google/android/apps/books/util/GenerationCache<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lcom/google/android/apps/books/util/GenerationCache;->mMaxCapacity:I

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/GenerationCache;->mCache:Ljava/util/HashMap;

    .line 47
    return-void
.end method
