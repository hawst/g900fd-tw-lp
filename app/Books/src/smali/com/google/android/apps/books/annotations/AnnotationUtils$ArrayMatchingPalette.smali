.class Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;
.super Ljava/lang/Object;
.source "AnnotationUtils.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArrayMatchingPalette"
.end annotation


# instance fields
.field private mColors:[I


# direct methods
.method public constructor <init>([I)V
    .locals 0
    .param p1, "colors"    # [I

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;->mColors:[I

    .line 147
    return-void
.end method


# virtual methods
.method public getClosestMatch(I)I
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;->mColors:[I

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationUtils;->matchColor(I)I
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->access$000(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method
