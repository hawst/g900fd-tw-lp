.class public interface abstract Lcom/google/android/apps/books/render/ReaderDataModel;
.super Ljava/lang/Object;
.source "ReaderDataModel.java"


# virtual methods
.method public abstract getPassageIndex(Ljava/lang/String;)Ljava/lang/Integer;
.end method

.method public abstract isLoaded(Lcom/google/android/apps/books/render/PageIndices;)Z
.end method

.method public abstract isPassageReady(I)Z
.end method

.method public abstract normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;
.end method
