.class Lcom/google/android/apps/books/navigation/BookNavView$9;
.super Ljava/lang/Object;
.source "BookNavView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/BookNavView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0

    .prologue
    .line 701
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$9;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$9;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 708
    :goto_0
    return-void

    .line 707
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$9;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$500(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->showTableOfContents(I)V

    goto :goto_0
.end method
