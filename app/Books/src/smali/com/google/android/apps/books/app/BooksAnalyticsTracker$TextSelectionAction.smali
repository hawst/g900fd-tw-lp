.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TextSelectionAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

.field public static final enum SELECTION_COPY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

.field public static final enum SELECTION_SEARCH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

.field public static final enum SELECTION_SHARE_QUOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

.field public static final enum SELECTION_TRANSLATE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 461
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    const-string v1, "SELECTION_SEARCH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_SEARCH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    .line 465
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    const-string v1, "SELECTION_COPY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_COPY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    .line 469
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    const-string v1, "SELECTION_SHARE_QUOTE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_SHARE_QUOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    .line 473
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    const-string v1, "SELECTION_TRANSLATE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_TRANSLATE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    .line 457
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_SEARCH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_COPY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_SHARE_QUOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_TRANSLATE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 457
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;
    .locals 1

    .prologue
    .line 457
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    return-object v0
.end method
