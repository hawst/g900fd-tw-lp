.class public Lcom/google/android/apps/books/widget/DevicePageRendering;
.super Ljava/lang/Object;
.source "DevicePageRendering.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/DevicePageRendering$1;,
        Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;,
        Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    }
.end annotation


# instance fields
.field public final debugText:Ljava/lang/String;

.field public final indices:Lcom/google/android/apps/books/render/PageIndices;

.field private final mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

.field private final mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

.field private final mOffsetFromPosition:I

.field private final mPageBounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

.field private mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

.field private final mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

.field private mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

.field private final mPrimaryPosition:Lcom/google/android/apps/books/common/Position;

.field private final mViewablePositions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end field

.field public final pageText:Ljava/lang/String;

.field public final touchableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/PageIndices;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;ILcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V
    .locals 2
    .param p1, "indices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p3, "readingPosition"    # Ljava/lang/String;
    .param p4, "firstPositionOnNextPage"    # Lcom/google/android/apps/books/common/Position;
    .param p6, "pageBounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p7, "pageText"    # Ljava/lang/String;
    .param p8, "debugText"    # Ljava/lang/String;
    .param p9, "fallbackPosition"    # Lcom/google/android/apps/books/common/Position;
    .param p10, "offsetFromPosition"    # I
    .param p11, "pageStructure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/PageIndices;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/common/Position;",
            "I",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ")V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "viewablePositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    .local p5, "touchables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    .line 98
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/Position;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    .line 103
    if-eqz p3, :cond_1

    .line 104
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-direct {v0, p3}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPrimaryPosition:Lcom/google/android/apps/books/common/Position;

    .line 111
    :goto_1
    iput p10, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mOffsetFromPosition:I

    .line 113
    iput-object p4, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    .line 114
    iput-object p6, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageBounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    .line 115
    iput-object p5, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    .line 116
    iput-object p7, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->pageText:Ljava/lang/String;

    .line 117
    iput-object p8, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->debugText:Ljava/lang/String;

    .line 118
    iput-object p11, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .line 119
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    if-eqz v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPrimaryPosition:Lcom/google/android/apps/books/common/Position;

    goto :goto_1

    .line 109
    :cond_2
    iput-object p9, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPrimaryPosition:Lcom/google/android/apps/books/common/Position;

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/PageIndices;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;ILcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/android/apps/books/widget/DevicePageRendering$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p2, "x1"    # Ljava/util/List;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/google/android/apps/books/common/Position;
    .param p5, "x4"    # Ljava/util/List;
    .param p6, "x5"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p7, "x6"    # Ljava/lang/String;
    .param p8, "x7"    # Ljava/lang/String;
    .param p9, "x8"    # Lcom/google/android/apps/books/common/Position;
    .param p10, "x9"    # I
    .param p11, "x10"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p12, "x11"    # Lcom/google/android/apps/books/widget/DevicePageRendering$1;

    .prologue
    .line 35
    invoke-direct/range {p0 .. p11}, Lcom/google/android/apps/books/widget/DevicePageRendering;-><init>(Lcom/google/android/apps/books/render/PageIndices;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;ILcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V

    return-void
.end method

.method public static getViewableBookmarks(Ljava/util/List;Ljava/util/SortedMap;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    .local p1, "allBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 46
    .local v0, "bookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 47
    .local v2, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/SortedMap;)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 49
    .end local v2    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_0
    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getFirstViewablePosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public getLastViewablePosition()Lcom/google/android/apps/books/common/Position;
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 200
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/Position;

    goto :goto_0
.end method

.method public getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageBounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    return-object v0
.end method

.method public getPageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPrimaryPosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/render/PageIdentifier;-><init>(Lcom/google/android/apps/books/render/PositionPageIdentifier;Lcom/google/android/apps/books/render/PageIndices;I)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    return-object v0
.end method

.method public getPageIndex()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    iget v0, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    return v0
.end method

.method public getPageStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    return-object v0
.end method

.method public getPageStructureDimensions()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    if-nez v0, :cond_0

    .line 305
    const/4 v0, 0x0

    .line 307
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getW()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    invoke-virtual {v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getH()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    iget v0, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    return v0
.end method

.method public getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;
    .locals 3

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPrimaryPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mOffsetFromPosition:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/PositionPageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    return-object v0
.end method

.method public getPrimaryPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mPrimaryPosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public getViewableBookmarks(Ljava/util/SortedMap;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "bookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-nez p1, :cond_0

    .line 161
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 179
    :goto_0
    return-object v1

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    if-nez v1, :cond_1

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    if-nez v1, :cond_2

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    invoke-interface {p1, v1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    goto :goto_0

    .line 170
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    invoke-interface {p1, v1, v2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "DevicePageRendering"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 175
    const-string v1, "DevicePageRendering"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subMap(mFirstViewablePosition, mFirstPositionOnNextPage) threw IllegalArgumentException with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstViewablePosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public hasTouchables()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTouchablesOfType(I)Z
    .locals 3
    .param p1, "typeMask"    # I

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->hasTouchables()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/TouchableItem;

    .line 222
    .local v1, "item":Lcom/google/android/apps/books/render/TouchableItem;
    iget v2, v1, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    and-int/2addr v2, p1

    if-eqz v2, :cond_0

    .line 223
    const/4 v2, 0x1

    .line 228
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public suggestedLocationForBookmark()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPrimaryPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 148
    .local v0, "position":Lcom/google/android/apps/books/common/Position;
    if-nez v0, :cond_0

    .line 149
    const/4 v1, 0x0

    .line 151
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/books/annotations/TextLocation;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 233
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPrimaryPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "indices"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "offset"

    iget v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mOffsetFromPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "positions"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mViewablePositions:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "firstPositionOnNextPage"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->mFirstPositionOnNextPage:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "debug text"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering;->debugText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
