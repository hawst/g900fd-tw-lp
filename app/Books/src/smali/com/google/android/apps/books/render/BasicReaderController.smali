.class public Lcom/google/android/apps/books/render/BasicReaderController;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ReaderController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/BasicReaderController$1;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;,
        Lcom/google/android/apps/books/render/BasicReaderController$MyListener;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;,
        Lcom/google/android/apps/books/render/BasicReaderController$Request;,
        Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;,
        Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;,
        Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;,
        Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;,
        Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;
    }
.end annotation


# instance fields
.field private mConfigData:Lcom/google/android/apps/books/util/JsConfiguration;

.field private final mInboundListener:Lcom/google/android/apps/books/render/ReaderListener;

.field private mIssuedCurrentSettings:Z

.field private final mLoadPositionFailureHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mOnLoadedRangeDataBulkHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnLoadedRangeDataHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnPageLoadedHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnReadableItemsReadyHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;

.field private mOverlayActiveClass:Ljava/lang/String;

.field private final mReader:Lcom/google/android/apps/books/render/TextReader;

.field private mReaderAlive:Z

.field private final mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;

.field private mReaderInitialized:Z

.field private mReaderIsBusy:Z

.field private mReaderIsReady:Z

.field private mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

.field private mRequestId:I

.field private final mRequests:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/google/android/apps/books/render/BasicReaderController$Request;",
            ">;"
        }
    .end annotation
.end field

.field private mSettingsApplied:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/TextReader;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/util/Logger;)V
    .locals 3
    .param p1, "reader"    # Lcom/google/android/apps/books/render/TextReader;
    .param p2, "listener"    # Lcom/google/android/apps/books/render/ReaderListener;
    .param p3, "model"    # Lcom/google/android/apps/books/render/ReaderDataModel;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    const/4 v2, 0x3

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/render/BasicReaderController$MyListener;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;Lcom/google/android/apps/books/render/BasicReaderController$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mInboundListener:Lcom/google/android/apps/books/render/ReaderListener;

    .line 118
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mLoadPositionFailureHandlers:Ljava/util/Map;

    .line 129
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;

    .line 142
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnReadableItemsReadyHandlers:Ljava/util/Map;

    .line 150
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataHandlers:Ljava/util/Map;

    .line 157
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataBulkHandlers:Ljava/util/Map;

    .line 497
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    .line 512
    const-string v0, "JsApi"

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/TextReader;

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;

    .line 514
    const-string v0, "RENDERERCALLS"

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/ReaderListener;

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;

    .line 516
    iput-object p3, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;

    .line 517
    iput-object p4, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 518
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnReadableItemsReadyHandlers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/render/BasicReaderController;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/BasicReaderController;->logEvent(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderAlive:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/render/BasicReaderController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->clearBusy()V

    return-void
.end method

.method static synthetic access$1402(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderInitialized:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/render/BasicReaderController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mIssuedCurrentSettings:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/render/BasicReaderController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataHandlers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataBulkHandlers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/render/BasicReaderController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/PriorityQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mLoadPositionFailureHandlers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;

    return-object v0
.end method

.method private advance()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 753
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderAlive:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z

    if-nez v0, :cond_4

    .line 754
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderInitialized:Z

    if-nez v0, :cond_0

    .line 755
    iput-boolean v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z

    .line 756
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mConfigData:Lcom/google/android/apps/books/util/JsConfiguration;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/TextReader;->initializeJavascript(Lcom/google/android/apps/books/util/JsConfiguration;)V

    goto :goto_0

    .line 762
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z

    if-nez v0, :cond_2

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    if-eqz v0, :cond_3

    .line 765
    const-string v0, "ReaderController"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 766
    const-string v0, "ReaderController"

    const-string v1, "Issuing apply settings request"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/TextReader;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 770
    iput-boolean v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mIssuedCurrentSettings:Z

    goto :goto_0

    .line 774
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOverlayActiveClass:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOverlayActiveClass:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/TextReader;->setOverlayActiveClass(Ljava/lang/String;)V

    .line 776
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOverlayActiveClass:Ljava/lang/String;

    goto :goto_0

    .line 780
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsReady:Z

    if-eqz v0, :cond_4

    .line 781
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/BasicReaderController$Request;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->run()V

    goto :goto_0

    .line 789
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z

    if-nez v0, :cond_5

    const-string v0, "ReaderController"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 790
    const-string v0, "ReaderController"

    const-string v1, "Reverting to idle state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    :cond_5
    return-void
.end method

.method private allocateRequestId()I
    .locals 2

    .prologue
    .line 795
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequestId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequestId:I

    return v0
.end method

.method private clearBusy()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 104
    return-void
.end method

.method private forceApplySettings()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 547
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderInitialized:Z

    .line 548
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mSettingsApplied:Z

    .line 549
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 550
    return-void
.end method

.method private logEvent(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v1, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v1, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 802
    :cond_0
    return-void
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)I
    .locals 7
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 698
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v5

    .line 700
    .local v5, "result":I
    iget-object v6, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IILjava/lang/String;I)V

    invoke-virtual {v6, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 701
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 703
    return v5
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 536
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 537
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mIssuedCurrentSettings:Z

    .line 538
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->forceApplySettings()V

    .line 540
    :cond_0
    return-void
.end method

.method public clearPendingTasks()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    .line 645
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 646
    return-void
.end method

.method public getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mInboundListener:Lcom/google/android/apps/books/render/ReaderListener;

    return-object v0
.end method

.method public initializeJavascript(Lcom/google/android/apps/books/util/JsConfiguration;)V
    .locals 1
    .param p1, "configData"    # Lcom/google/android/apps/books/util/JsConfiguration;

    .prologue
    const/4 v0, 0x0

    .line 527
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderInitialized:Z

    .line 528
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsReady:Z

    .line 529
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mConfigData:Lcom/google/android/apps/books/util/JsConfiguration;

    .line 530
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 531
    return-void
.end method

.method public loadNearbyText(IIFFI)V
    .locals 6
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "radius"    # I

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/TextReader;->loadNearbyText(IIFFI)V

    .line 744
    return-void
.end method

.method public loadNotNormalizedRangeData(IIII)I
    .locals 10
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v6

    .line 689
    .local v6, "result":I
    iget-object v9, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;

    const/4 v8, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v7, v6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/render/BasicReaderController$NotNormalizedRangeDataRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIIIIIZ)V

    invoke-virtual {v9, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 691
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 693
    return v6
.end method

.method public loadPage(IILjava/lang/Integer;)I
    .locals 6
    .param p1, "passage"    # I
    .param p2, "page"    # I
    .param p3, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v2

    .line 581
    .local v2, "requestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;

    move-object v1, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIILjava/lang/Integer;)V

    .line 582
    .local v0, "request":Lcom/google/android/apps/books/render/BasicReaderController$LoadPageRequest;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 584
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 585
    return v2
.end method

.method public loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)I
    .locals 7
    .param p1, "passage"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offset"    # Ljava/lang/Integer;
    .param p4, "fallbackPassageIndex"    # Ljava/lang/Integer;

    .prologue
    .line 616
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v2

    .line 618
    .local v2, "requestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 620
    .local v0, "request":Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 622
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 623
    return v2
.end method

.method public loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIII)I
    .locals 13
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "needSelectionData"    # Z
    .param p4, "handleIndex"    # I
    .param p5, "deltaX"    # I
    .param p6, "deltaY"    # I
    .param p7, "prevRequest"    # I

    .prologue
    .line 664
    const/4 v2, -0x1

    move/from16 v0, p7

    if-eq v0, v2, :cond_1

    .line 665
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 666
    .local v12, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/render/BasicReaderController$Request;>;"
    :cond_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 667
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/BasicReaderController$Request;

    .line 668
    .local v1, "request":Lcom/google/android/apps/books/render/BasicReaderController$Request;
    invoke-virtual {v1}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getOriginalOrder()I

    move-result v2

    move/from16 v0, p7

    if-ne v0, v2, :cond_0

    .line 669
    invoke-interface {v12}, Ljava/util/Iterator;->remove()V

    .line 675
    .end local v1    # "request":Lcom/google/android/apps/books/render/BasicReaderController$Request;
    .end local v12    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/render/BasicReaderController$Request;>;"
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v9

    .line 676
    .local v9, "result":I
    new-instance v1, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;

    const/4 v11, 0x0

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move v10, v9

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIIZ)V

    .line 678
    .local v1, "request":Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 679
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 681
    return v9
.end method

.method public loadRangeDataBulk(ILjava/util/Map;)I
    .locals 7
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 709
    .local p2, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v4

    .line 711
    .local v4, "result":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/util/Map;IIZ)V

    .line 713
    .local v0, "loadRangeDataBulkRequest":Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 714
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 716
    return v4
.end method

.method public loadSpreadPage(ILjava/lang/String;II)I
    .locals 7
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "spreadOffset"    # I
    .param p4, "spreadPageIndex"    # I

    .prologue
    .line 591
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v6

    .line 593
    .local v6, "requestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/lang/String;III)V

    .line 595
    .local v0, "request":Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 597
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 598
    return v6
.end method

.method public loadSpreadPageFromEob(II)I
    .locals 3
    .param p1, "spreadOffset"    # I
    .param p2, "spreadPageIndex"    # I

    .prologue
    .line 603
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v1

    .line 605
    .local v1, "requestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;III)V

    .line 607
    .local v0, "request":Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 609
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 610
    return v1
.end method

.method public maybePurgePassages(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 554
    .local p1, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v1

    .line 556
    .local v1, "requestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/util/Collection;)V

    .line 558
    .local v0, "request":Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 560
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 561
    return-void
.end method

.method public purgeRequests(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 565
    .local p1, "requestIdsToPurge":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 566
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/render/BasicReaderController$Request;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 567
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/BasicReaderController$Request;

    .line 568
    .local v1, "queuedReq":Lcom/google/android/apps/books/render/BasicReaderController$Request;
    invoke-virtual {v1}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getOriginalOrder()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 569
    const-string v2, "ReaderController"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 570
    const-string v2, "ReaderController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Purging request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getOriginalOrder()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 575
    .end local v1    # "queuedReq":Lcom/google/android/apps/books/render/BasicReaderController$Request;
    :cond_2
    return-void
.end method

.method public requestPreloadPassage(I)V
    .locals 3
    .param p1, "passageIndex"    # I

    .prologue
    .line 628
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v1

    .line 630
    .local v1, "requestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;II)V

    .line 631
    .local v0, "request":Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 633
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 634
    return-void
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 3
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 650
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I

    move-result v1

    .line 652
    .local v1, "result":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;II)V

    .line 654
    .local v0, "r":Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 655
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 657
    return v1
.end method

.method public setOverlayActiveClass(Ljava/lang/String;)V
    .locals 0
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 638
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mOverlayActiveClass:Ljava/lang/String;

    .line 639
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 640
    return-void
.end method

.method public transitionToReaderReady()V
    .locals 1

    .prologue
    .line 729
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsReady:Z

    .line 730
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController;->advance()V

    .line 731
    return-void
.end method
