.class public Lcom/google/android/apps/books/util/IntArrayParser;
.super Ljava/lang/Object;
.source "IntArrayParser.java"


# instance fields
.field private mCurIndex:I

.field private mString:Ljava/lang/String;

.field private mStringLength:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasMore()Z
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    iget v1, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mStringLength:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mString:Ljava/lang/String;

    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mStringLength:I

    .line 22
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    .line 23
    return-void
.end method

.method public nextInt()I
    .locals 6

    .prologue
    .line 29
    const/4 v2, 0x0

    .line 30
    .local v2, "result":I
    iget-object v3, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mString:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 32
    .local v0, "nextChar":C
    const/16 v3, 0x2d

    if-ne v0, v3, :cond_0

    .line 33
    iget-object v3, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mString:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 34
    const/4 v1, 0x0

    .line 38
    .local v1, "positive":Z
    :goto_0
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 39
    mul-int/lit8 v3, v2, 0xa

    const/16 v4, 0xa

    invoke-static {v0, v4}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    add-int v2, v3, v4

    .line 40
    iget-object v3, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mString:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/books/util/IntArrayParser;->mCurIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 36
    .end local v1    # "positive":Z
    :cond_0
    const/4 v1, 0x1

    .restart local v1    # "positive":Z
    goto :goto_0

    .line 42
    :cond_1
    if-eqz v1, :cond_2

    .end local v2    # "result":I
    :goto_1
    return v2

    .restart local v2    # "result":I
    :cond_2
    neg-int v2, v2

    goto :goto_1
.end method
