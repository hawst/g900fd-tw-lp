.class public abstract enum Lcom/google/android/apps/books/app/LibraryComparator;
.super Ljava/lang/Enum;
.source "LibraryComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/LibraryComparator;",
        ">;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/widget/CardData;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/LibraryComparator;

.field public static final enum BY_AUTHOR:Lcom/google/android/apps/books/app/LibraryComparator;

.field public static final enum BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

.field public static final enum BY_TITLE:Lcom/google/android/apps/books/app/LibraryComparator;


# instance fields
.field private final mAnalyticsAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field private final mLabelResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/books/app/LibraryComparator$1;

    const-string v1, "BY_RECENCY"

    const v2, 0x7f0f01ab

    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_RECENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/books/app/LibraryComparator$1;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

    .line 32
    new-instance v0, Lcom/google/android/apps/books/app/LibraryComparator$2;

    const-string v1, "BY_TITLE"

    const v2, 0x7f0f01ac

    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_TITLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/books/app/LibraryComparator$2;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->BY_TITLE:Lcom/google/android/apps/books/app/LibraryComparator;

    .line 40
    new-instance v0, Lcom/google/android/apps/books/app/LibraryComparator$3;

    const-string v1, "BY_AUTHOR"

    const v2, 0x7f0f01ad

    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_AUTHOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/books/app/LibraryComparator$3;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->BY_AUTHOR:Lcom/google/android/apps/books/app/LibraryComparator;

    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/app/LibraryComparator;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryComparator;->BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/LibraryComparator;->BY_TITLE:Lcom/google/android/apps/books/app/LibraryComparator;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/LibraryComparator;->BY_AUTHOR:Lcom/google/android/apps/books/app/LibraryComparator;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->$VALUES:[Lcom/google/android/apps/books/app/LibraryComparator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V
    .locals 0
    .param p3, "labelResourceId"    # I
    .param p4, "analyticsAction"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 96
    iput p3, p0, Lcom/google/android/apps/books/app/LibraryComparator;->mLabelResourceId:I

    .line 97
    iput-object p4, p0, Lcom/google/android/apps/books/app/LibraryComparator;->mAnalyticsAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 98
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;Lcom/google/android/apps/books/app/LibraryComparator$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;
    .param p5, "x4"    # Lcom/google/android/apps/books/app/LibraryComparator$1;

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/app/LibraryComparator;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/CardData;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 12
    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/LibraryComparator;->compareByTitle(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/CardData;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 12
    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/LibraryComparator;->compareByAuthor(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I

    move-result v0

    return v0
.end method

.method private static compareByAuthor(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    .locals 2
    .param p0, "lhs"    # Lcom/google/android/apps/books/widget/CardData;
    .param p1, "rhs"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardData;->getAuthor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static compareByTitle(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    .locals 2
    .param p0, "lhs"    # Lcom/google/android/apps/books/widget/CardData;
    .param p1, "rhs"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 4
    .param p0, "comparatorString"    # Ljava/lang/String;

    .prologue
    .line 75
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/books/app/LibraryComparator;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/LibraryComparator;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 80
    :goto_0
    return-object v1

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "LibraryComparator"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    const-string v1, "LibraryComparator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t find LibraryComparator for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/app/LibraryComparator;->getDefault()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v1

    goto :goto_0
.end method

.method public static getDefault()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/google/android/apps/books/app/LibraryComparator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/LibraryComparator;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->$VALUES:[Lcom/google/android/apps/books/app/LibraryComparator;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/LibraryComparator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/LibraryComparator;

    return-object v0
.end method


# virtual methods
.method public getAnalyticsAction()Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/books/app/LibraryComparator;->mAnalyticsAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    return-object v0
.end method

.method public getLabelResourceId()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/apps/books/app/LibraryComparator;->mLabelResourceId:I

    return v0
.end method
