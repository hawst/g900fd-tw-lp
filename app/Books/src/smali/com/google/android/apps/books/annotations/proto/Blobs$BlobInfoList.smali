.class public final Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Blobs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/proto/Blobs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BlobInfoList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;


# instance fields
.field private info_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 596
    new-instance v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    .line 597
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs;->internalForceInit()V

    .line 598
    sget-object v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->initFields()V

    .line 599
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 324
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;

    .line 348
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->memoizedSerializedSize:I

    .line 309
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->initFields()V

    .line 310
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/annotations/proto/Blobs$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$1;

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 324
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;

    .line 348
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->memoizedSerializedSize:I

    .line 311
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .locals 1

    .prologue
    .line 315
    sget-object v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 335
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 1

    .prologue
    .line 429
    # invokes: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->access$800()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->newBuilder()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    # invokes: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->buildParsed()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->access$700(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 350
    iget v2, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->memoizedSerializedSize:I

    .line 351
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 359
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 353
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->getInfoList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 355
    .local v0, "element":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 357
    goto :goto_1

    .line 358
    .end local v0    # "element":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    :cond_1
    iput v2, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->memoizedSerializedSize:I

    move v3, v2

    .line 359
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->getSerializedSize()I

    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->getInfoList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 344
    .local v0, "element":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 346
    .end local v0    # "element":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    :cond_0
    return-void
.end method
