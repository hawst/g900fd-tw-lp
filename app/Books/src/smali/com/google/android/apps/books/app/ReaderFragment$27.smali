.class Lcom/google/android/apps/books/app/ReaderFragment$27;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->updateBookmarkMenuItemTextForPages(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/BookmarkController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$pages:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 5259
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->val$pages:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/BookmarkController;)V
    .locals 5
    .param p1, "bc"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5262
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 5263
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 5264
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$27;->val$pages:Ljava/util/List;

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/app/BookmarkController;->pagesContainBookmark(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/ReaderMenu;->setBookmarkIsAdd(Z)V

    .line 5271
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 5263
    goto :goto_0

    :cond_2
    move v1, v2

    .line 5264
    goto :goto_1

    .line 5267
    :cond_3
    const-string v0, "ReaderFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5268
    const-string v0, "ReaderFragment"

    const-string v1, "Options menu is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 5259
    check-cast p1, Lcom/google/android/apps/books/app/BookmarkController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$27;->take(Lcom/google/android/apps/books/app/BookmarkController;)V

    return-void
.end method
