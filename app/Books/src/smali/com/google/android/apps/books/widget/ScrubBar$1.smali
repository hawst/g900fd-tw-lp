.class Lcom/google/android/apps/books/widget/ScrubBar$1;
.super Landroid/os/Handler;
.source "ScrubBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ScrubBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ScrubBar;)V
    .locals 0

    .prologue
    .line 593
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar$1;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 596
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 599
    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$1;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$300(Lcom/google/android/apps/books/widget/ScrubBar;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 604
    const-string v0, "ScrubBar"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 605
    const-string v0, "ScrubBar"

    const-string v1, "done waiting..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$1;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$402(Lcom/google/android/apps/books/widget/ScrubBar;Z)Z

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$1;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->access$500(Lcom/google/android/apps/books/widget/ScrubBar;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$1;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$1;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$500(Lcom/google/android/apps/books/widget/ScrubBar;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # invokes: Lcom/google/android/apps/books/widget/ScrubBar;->setPositionAndPause(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->access$600(Lcom/google/android/apps/books/widget/ScrubBar;I)V

    goto :goto_0
.end method
