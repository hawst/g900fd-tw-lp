.class Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;
.super Ljava/lang/Object;
.source "InfoCardsHelper.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/InfoCardsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProviderCallbackResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;",
        ">;"
    }
.end annotation


# instance fields
.field final providerOrder:I

.field final result:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field final sequenceNumber:I

.field final synthetic this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/InfoCardsHelper;IILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p2, "sequenceNumber"    # I
    .param p3, "providerOrder"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 301
    .local p4, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    iput p2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->sequenceNumber:I

    .line 303
    iput p3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->providerOrder:I

    .line 304
    iput-object p4, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    .line 305
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;)I
    .locals 2
    .param p1, "c"    # Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;

    .prologue
    .line 309
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->providerOrder:I

    iget v1, p1, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->providerOrder:I

    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 295
    check-cast p1, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->compareTo(Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProviderCallbackResult{sequenceNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->sequenceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->providerOrder:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
