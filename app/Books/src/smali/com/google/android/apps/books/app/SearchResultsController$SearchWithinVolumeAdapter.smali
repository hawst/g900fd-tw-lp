.class Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;
.super Lcom/google/android/apps/books/widget/CustomViewArrayAdapter;
.source "SearchResultsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SearchResultsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchWithinVolumeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/CustomViewArrayAdapter",
        "<",
        "Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;",
        ">;"
    }
.end annotation


# instance fields
.field private mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private mNumMatches:I

.field private mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/CustomViewArrayAdapter;-><init>(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->onLoadStart()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;Ljava/lang/String;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->populateChapterView(Ljava/lang/String;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;Lcom/google/android/apps/books/model/SearchResult;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/SearchResult;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->populateFoundView(Lcom/google/android/apps/books/model/SearchResult;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getSearchResultCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->onLoadComplete()V

    return-void
.end method

.method private createChapterHeading(Ljava/lang/String;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    .locals 3
    .param p1, "chapterHeading"    # Ljava/lang/String;

    .prologue
    .line 136
    new-instance v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$1;

    const/4 v1, 0x3

    const v2, 0x7f04005c

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$1;-><init>(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;IILjava/lang/String;)V

    return-object v0
.end method

.method private createFoundItem(Lcom/google/android/apps/books/model/SearchResult;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    .locals 3
    .param p1, "searchResult"    # Lcom/google/android/apps/books/model/SearchResult;

    .prologue
    .line 164
    # getter for: Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->typeToLayoutMap:Ljava/util/Map;
    invoke-static {}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->access$400()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getItemType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 166
    .local v0, "layoutId":I
    new-instance v1, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getItemType()I

    move-result v2

    invoke-direct {v1, p0, v2, v0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter$2;-><init>(Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;IILcom/google/android/apps/books/model/SearchResult;)V

    return-object v1
.end method

.method private getPageTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 202
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 204
    :goto_0
    return-object v1

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, ""

    goto :goto_0
.end method

.method private getSearchResultCount()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mNumMatches:I

    return v0
.end method

.method private onLoadComplete()V
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 210
    # getter for: Lcom/google/android/apps/books/app/SearchResultsController;->NONE_FOUND_ROW:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    invoke-static {}, Lcom/google/android/apps/books/app/SearchResultsController;->access$600()Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->add(Ljava/lang/Object;)V

    .line 212
    :cond_0
    return-void
.end method

.method private onLoadStart()V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->clear()V

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mNumMatches:I

    .line 129
    return-void
.end method

.method private populateChapterView(Ljava/lang/String;Landroid/view/View;)V
    .locals 2
    .param p1, "chapterHeading"    # Ljava/lang/String;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 153
    const v1, 0x1020014

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 154
    .local v0, "text1":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    return-void
.end method

.method private populateFoundView(Lcom/google/android/apps/books/model/SearchResult;Landroid/view/View;)V
    .locals 9
    .param p1, "searchResult"    # Lcom/google/android/apps/books/model/SearchResult;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 180
    iget-object v6, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-eqz v6, :cond_0

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 181
    iget-object v6, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v6, :cond_1

    :goto_1
    invoke-static {v7}, Lcom/google/android/common/base/Preconditions;->checkState(Z)V

    .line 183
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getAnchorLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v6

    iget-object v3, v6, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    .line 184
    .local v3, "position":Lcom/google/android/apps/books/common/Position;
    invoke-virtual {v3}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v2

    .line 187
    .local v2, "pageId":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v7, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v7

    invoke-interface {v6, v2, v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->isPageEnabled(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 191
    .local v1, "isPageEnabled":Z
    :goto_2
    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 193
    const v6, 0x1020014

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 194
    .local v4, "text1":Landroid/widget/TextView;
    const v6, 0x1020015

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 196
    .local v5, "text2":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getSnippet()Lcom/google/android/apps/books/model/Snippet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/Snippet;->getStyledText()Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getPageTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    return-void

    .end local v1    # "isPageEnabled":Z
    .end local v2    # "pageId":Ljava/lang/String;
    .end local v3    # "position":Lcom/google/android/apps/books/common/Position;
    .end local v4    # "text1":Landroid/widget/TextView;
    .end local v5    # "text2":Landroid/widget/TextView;
    :cond_0
    move v6, v8

    .line 180
    goto :goto_0

    :cond_1
    move v7, v8

    .line 181
    goto :goto_1

    .line 188
    .restart local v2    # "pageId":Ljava/lang/String;
    .restart local v3    # "position":Lcom/google/android/apps/books/common/Position;
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const/4 v1, 0x0

    .restart local v1    # "isPageEnabled":Z
    goto :goto_2
.end method


# virtual methods
.method protected found(Lcom/google/android/apps/books/model/SearchResult;)V
    .locals 2
    .param p1, "searchResult"    # Lcom/google/android/apps/books/model/SearchResult;

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mNumMatches:I

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getMatchRanges()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mNumMatches:I

    .line 159
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->createFoundItem(Lcom/google/android/apps/books/model/SearchResult;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->insert(Ljava/lang/Object;I)V

    .line 160
    return-void
.end method

.method protected getItemView(Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0
    .param p1, "item"    # Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 119
    if-nez p2, :cond_0

    .line 120
    # invokes: Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->createNewView(Landroid/view/ViewGroup;)Landroid/view/View;
    invoke-static {p1, p3}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->access$200(Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 122
    :cond_0
    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->populateView(Landroid/view/View;)V

    .line 123
    return-object p2
.end method

.method protected bridge synthetic getItemView(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    check-cast p1, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getItemView(Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    # invokes: Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->getItemViewType()I
    invoke-static {v0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->access$100(Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;)I

    move-result v0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 114
    const/16 v0, 0x8

    return v0
.end method

.method protected setChapterHeading(Ljava/lang/String;)V
    .locals 2
    .param p1, "chapterHeading"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->createChapterHeading(Ljava/lang/String;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->getCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->insert(Ljava/lang/Object;I)V

    .line 133
    return-void
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 105
    return-void
.end method

.method public setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultsController$SearchWithinVolumeAdapter;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 101
    return-void
.end method
