.class public Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;
.super Ljava/lang/Object;
.source "HiddenTextViewsAccessiblePages.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;


# static fields
.field private static final ONE_UP_ACCESSIBLE_PAGE_IDS:[I

.field private static final TWO_UP_ACCESSIBLE_PAGE_IDS:[I


# instance fields
.field private final mFocusParker:Landroid/view/View;

.field private final mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->TWO_UP_ACCESSIBLE_PAGE_IDS:[I

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0e01ad

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->ONE_UP_ACCESSIBLE_PAGE_IDS:[I

    return-void

    .line 32
    :array_0
    .array-data 4
        0x7f0e01af
        0x7f0e01ad
    .end array-data
.end method

.method public constructor <init>(ILcom/google/android/apps/books/widget/PagesView;)V
    .locals 13
    .param p1, "pagesCount"    # I
    .param p2, "pagesView"    # Lcom/google/android/apps/books/widget/PagesView;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-array v12, p1, [Lcom/google/android/apps/books/widget/AccessiblePage;

    iput-object v12, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    .line 48
    invoke-interface {p2}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    .line 50
    .local v9, "parent":Landroid/view/ViewGroup;
    const/4 v12, 0x1

    if-ne p1, v12, :cond_0

    const v5, 0x7f0400b2

    .line 53
    .local v5, "layoutId":I
    :goto_0
    invoke-interface {p2}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 54
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const/4 v12, 0x0

    invoke-virtual {v4, v5, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "container":Landroid/view/View;
    const/4 v12, 0x0

    invoke-virtual {v9, v1, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 57
    const/4 v12, 0x1

    if-ne p1, v12, :cond_1

    sget-object v2, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->ONE_UP_ACCESSIBLE_PAGE_IDS:[I

    .line 60
    .local v2, "hiddenPagesIds":[I
    :goto_1
    const/4 v10, 0x0

    .line 61
    .local v10, "viewIndex":I
    move-object v0, v2

    .local v0, "arr$":[I
    array-length v6, v0

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v11, v10

    .end local v10    # "viewIndex":I
    .local v11, "viewIndex":I
    :goto_2
    if-ge v3, v6, :cond_2

    aget v8, v0, v3

    .line 62
    .local v8, "pageId":I
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/widget/AccessiblePage;

    .line 63
    .local v7, "page":Lcom/google/android/apps/books/widget/AccessiblePage;
    iget-object v12, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "viewIndex":I
    .restart local v10    # "viewIndex":I
    aput-object v7, v12, v11

    .line 61
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "viewIndex":I
    .restart local v11    # "viewIndex":I
    goto :goto_2

    .line 50
    .end local v0    # "arr$":[I
    .end local v1    # "container":Landroid/view/View;
    .end local v2    # "hiddenPagesIds":[I
    .end local v3    # "i$":I
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "layoutId":I
    .end local v6    # "len$":I
    .end local v7    # "page":Lcom/google/android/apps/books/widget/AccessiblePage;
    .end local v8    # "pageId":I
    .end local v11    # "viewIndex":I
    :cond_0
    const v5, 0x7f0400b3

    goto :goto_0

    .line 57
    .restart local v1    # "container":Landroid/view/View;
    .restart local v4    # "inflater":Landroid/view/LayoutInflater;
    .restart local v5    # "layoutId":I
    :cond_1
    sget-object v2, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->TWO_UP_ACCESSIBLE_PAGE_IDS:[I

    goto :goto_1

    .line 66
    .restart local v0    # "arr$":[I
    .restart local v2    # "hiddenPagesIds":[I
    .restart local v3    # "i$":I
    .restart local v6    # "len$":I
    .restart local v11    # "viewIndex":I
    :cond_2
    const v12, 0x7f0e01ae

    invoke-virtual {v1, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mFocusParker:Landroid/view/View;

    .line 67
    return-void
.end method


# virtual methods
.method public setImportantForAccessibility(Z)V
    .locals 5
    .param p1, "isImportant"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    .local v0, "arr$":[Lcom/google/android/apps/books/widget/AccessiblePage;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 100
    .local v3, "page":Lcom/google/android/apps/books/widget/AccessiblePage;
    invoke-static {v3, p1}, Lcom/google/android/ublib/utils/AccessibilityUtils;->setImportantForAccessibility(Landroid/view/View;Z)V

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v3    # "page":Lcom/google/android/apps/books/widget/AccessiblePage;
    :cond_0
    if-eqz p1, :cond_1

    .line 109
    iget-object v4, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mFocusParker:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 111
    :cond_1
    return-void
.end method

.method public setPageRendering(ILcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 1
    .param p1, "pageIndex"    # I
    .param p2, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/widget/AccessiblePage;->setPageRendering(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 95
    return-void
.end method

.method public setSelectionChangedListener(Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;)V
    .locals 9
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    .prologue
    .line 72
    iget-object v8, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    array-length v4, v8

    .line 73
    .local v4, "pagesCount":I
    const/4 v6, 0x0

    .line 74
    .local v6, "viewIndex":I
    iget-object v0, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    .local v0, "arr$":[Lcom/google/android/apps/books/widget/AccessiblePage;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    move v7, v6

    .end local v6    # "viewIndex":I
    .local v7, "viewIndex":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 76
    .local v3, "page":Lcom/google/android/apps/books/widget/AccessiblePage;
    const/4 v8, 0x1

    if-ne v4, v8, :cond_0

    .line 77
    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 82
    .local v5, "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_1
    invoke-virtual {v3, p1, v5}, Lcom/google/android/apps/books/widget/AccessiblePage;->setup(Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 83
    iget-object v8, p0, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;->mHiddenTextViewPages:[Lcom/google/android/apps/books/widget/AccessiblePage;

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "viewIndex":I
    .restart local v6    # "viewIndex":I
    aput-object v3, v8, v7

    .line 74
    add-int/lit8 v1, v1, 0x1

    move v7, v6

    .end local v6    # "viewIndex":I
    .restart local v7    # "viewIndex":I
    goto :goto_0

    .line 79
    .end local v5    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_0
    if-nez v7, :cond_1

    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .restart local v5    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_2
    goto :goto_1

    .end local v5    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_1
    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_2

    .line 85
    .end local v3    # "page":Lcom/google/android/apps/books/widget/AccessiblePage;
    :cond_2
    return-void
.end method

.method public wantsPageRenderings()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method
