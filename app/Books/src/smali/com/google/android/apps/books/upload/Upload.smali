.class public Lcom/google/android/apps/books/upload/Upload;
.super Ljava/lang/Object;
.source "Upload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/upload/Upload$Uploads;
    }
.end annotation


# instance fields
.field private mFileSize:I

.field private final mId:Ljava/lang/String;

.field private final mOrigFileName:Ljava/lang/String;

.field private mStartedTimestamp:J

.field private mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

.field private mUploadPercentage:I

.field private mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

.field private mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/upload/Upload;)V
    .locals 1
    .param p1, "otherUpload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mOrigFileName:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getUploadPercentage()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    .line 69
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 70
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/uploader/client/ClientProto$TransferHandle;->toBuilder()Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/uploader/client/ClientProto$TransferHandle$Builder;->build()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 73
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getFileSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/upload/Upload;->mFileSize:I

    .line 74
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)V
    .locals 2
    .param p1, "uploadProto"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mOrigFileName:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getUploadPercentage()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    .line 54
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 55
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getDefaultInstance()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getFileSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/upload/Upload;->mFileSize:I

    .line 59
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 60
    return-void

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "origFileName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/books/upload/Upload;->mOrigFileName:Ljava/lang/String;

    .line 40
    iput v1, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    .line 41
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->INITIALIZED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 42
    iput v1, p0, Lcom/google/android/apps/books/upload/Upload;->mFileSize:I

    .line 43
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/upload/Upload;->mStartedTimestamp:J

    .line 45
    return-void
.end method

.method private compareTransferHandlers(Lcom/google/uploader/client/ClientProto$TransferHandle;Lcom/google/uploader/client/ClientProto$TransferHandle;)Z
    .locals 4
    .param p1, "t1"    # Lcom/google/uploader/client/ClientProto$TransferHandle;
    .param p2, "t2"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 135
    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    .line 136
    :cond_0
    if-ne p1, p2, :cond_2

    .line 138
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 136
    goto :goto_0

    .line 138
    :cond_3
    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getMetadata()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getMetadata()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getScottyUploadId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getScottyUploadId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getUploadUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getUploadUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 120
    if-nez p1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 123
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/books/upload/Upload;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 127
    check-cast v0, Lcom/google/android/apps/books/upload/Upload;

    .line 128
    .local v0, "otherUpload":Lcom/google/android/apps/books/upload/Upload;
    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getUploadPercentage()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/upload/Upload;->compareTransferHandlers(Lcom/google/uploader/client/ClientProto$TransferHandle;Lcom/google/uploader/client/ClientProto$TransferHandle;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getFileSize()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/android/apps/books/upload/Upload;->mFileSize:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mOrigFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getStartedTimestamp()J
    .locals 2

    .prologue
    .line 203
    iget-wide v0, p0, Lcom/google/android/apps/books/upload/Upload;->mStartedTimestamp:J

    return-wide v0
.end method

.method public getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    return-object v0
.end method

.method public getUploadPercentage()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    return v0
.end method

.method public getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    return-object v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public setFileSize(I)V
    .locals 0
    .param p1, "fileSize"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/google/android/apps/books/upload/Upload;->mFileSize:I

    .line 178
    return-void
.end method

.method public setTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)V
    .locals 0
    .param p1, "transferHandle"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    .line 103
    return-void
.end method

.method public setUploadPercentage(I)V
    .locals 0
    .param p1, "uploadPercentage"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    .line 87
    return-void
.end method

.method public setUploadStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V
    .locals 0
    .param p1, "uploadStatus"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .line 95
    return-void
.end method

.method public setVolumeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 194
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "upload ID"

    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "filename"

    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mOrigFileName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "status"

    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "volumeId"

    iget-object v2, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUploadProto()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .locals 2

    .prologue
    .line 106
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->newBuilder()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/Upload;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/Upload;->mOrigFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setOrigFileName(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadPercentage:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setUploadPercentage(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/Upload;->mUploadStatus:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/upload/Upload;->mTransferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/upload/Upload;->mFileSize:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setFileSize(I)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/Upload;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->build()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/uploader/client/ClientProto$TransferHandle;->getDefaultInstance()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v0

    goto :goto_0
.end method
