.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"

# interfaces
.implements Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->getSearchTextProcessor()Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleEndDocument(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "resultQueue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    return-void
.end method

.method public handleNewReadingPosition(Ljava/lang/String;)V
    .locals 2
    .param p1, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mLabels:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$1200(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mOffsets:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$1300(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mTextContent:Ljava/lang/StringBuilder;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$800(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    return-void
.end method

.method public handleNewText(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p2, "resultQueue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mTextContent:Ljava/lang/StringBuilder;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$800(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    return-void
.end method
