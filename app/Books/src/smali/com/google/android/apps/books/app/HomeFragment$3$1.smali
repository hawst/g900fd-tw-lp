.class Lcom/google/android/apps/books/app/HomeFragment$3$1;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment$3;->run()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/HomeFragment$3;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment$3;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$3$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 4
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 503
    const-string v0, "HomeFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    const-string v0, "HomeFragment"

    const-string v1, "Periodic getMyEbooks!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$3$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$3;

    iget-object v1, v1, Lcom/google/android/apps/books/app/HomeFragment$3;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$1500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/google/android/apps/books/data/BooksDataController;->getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 510
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 500
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$3$1;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
