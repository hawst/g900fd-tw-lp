.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getSessionKeyToUploadCollectionChanges(JLcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$collectionId:J

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-wide p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->val$collectionId:J

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 1015
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1016
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-wide v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->val$collectionId:J

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->uploadCollectionChanges(JLcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1020
    :goto_0
    return-void

    .line 1018
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1012
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
