.class public interface abstract Lcom/google/android/apps/books/model/BooksDataStore;
.super Ljava/lang/Object;
.source "BooksDataStore.java"

# interfaces
.implements Lcom/google/android/apps/books/model/DataControllerStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;,
        Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;,
        Lcom/google/android/apps/books/model/BooksDataStore$Committer;,
        Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;,
        Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;,
        Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;,
        Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;,
        Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;,
        Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
    }
.end annotation


# virtual methods
.method public abstract addToCollection(JLjava/lang/String;)V
.end method

.method public abstract cleanTempDirectory()V
.end method

.method public abstract deleteAllContent()V
.end method

.method public abstract deleteContent(Ljava/lang/String;)V
.end method

.method public abstract ensureMyEbooksCollection()V
.end method

.method public abstract getAccountSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAccountSessionKey(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCachedOffers()Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCachedRecommendations()Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCcBox(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/CcBox;
.end method

.method public abstract getCollectionChanges(J)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContentSize(Ljava/lang/String;)J
.end method

.method public abstract getDismissedOffers()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDismissedRecommendations()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDownloadProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;
.end method

.method public abstract getFileStorageDirectory()Ljava/io/File;
.end method

.method public abstract getLocalPageState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
.end method

.method public abstract getLocalResourceState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLocalSegmentState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getMyEbooksVolumeIdsWithMaybeVersions(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getMyEbooksVolumes(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getMyEbooksVolumesRange(ILjava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getMyEbooksVolumesRange(ILjava/util/List;Ljava/util/Map;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getOwnedVolumeIds()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;
.end method

.method public abstract getPageImageFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract getPageStructureFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getRecentVolumeSearches(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getRecommendationsCacheDir(Z)Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getResource(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
.end method

.method public abstract getResourceContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract getResourceResources(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract getSharedResourceMD5File(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract getStaleVolumeIdsToDelete(J)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVolume(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVolumeCoverFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract getVolumeIdsForLicenseRenewal()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVolumeManifest(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/model/VolumeManifest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVolumeNetworkResources(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVolumeSessionKey(Lcom/google/android/apps/books/model/VolumeSessionKeyId;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeSessionKeyId;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/VolumeSessionKeyId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVolumeThumbnailFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end method

.method public abstract isVolumeCoverLocal(Ljava/lang/String;)Z
.end method

.method public abstract openFallbackCoverInputStream()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract purgeDeletedCollectionVolumes(JLjava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract removeFromCollection(JLjava/lang/String;)V
.end method

.method public abstract removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/AccountSessionKeyId;)V
.end method

.method public abstract removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/VolumeSessionKeyId;)V
.end method

.method public abstract saveAccountSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCachedOffers(Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setCachedRecommendations(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/CcBox;)V
.end method

.method public abstract setDismissedOffers(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setDismissedRecommendations(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setFitWidth(Ljava/lang/String;Z)V
.end method

.method public abstract setForceDownload(Ljava/lang/String;Z)V
.end method

.method public abstract setHasOfflineLicense(Ljava/lang/String;Z)V
.end method

.method public abstract setLastVolumeSearch(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V
.end method

.method public abstract setLineHeight(Ljava/lang/String;F)V
.end method

.method public abstract setMyEbooksVolumes(Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)Z
.end method

.method public abstract setPinned(Ljava/lang/String;Z)V
.end method

.method public abstract setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V
.end method

.method public abstract setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
.end method

.method public abstract setResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setResources(Ljava/lang/String;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setSegmentResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setTextZoom(Ljava/lang/String;F)V
.end method

.method public abstract setUserSelectedMode(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
.end method

.method public abstract setVolume(Lcom/google/android/apps/books/model/VolumeData;)V
.end method

.method public abstract setVolumeManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract updateAccountSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateLastLocalAccess(Ljava/lang/String;JZ)V
.end method

.method public abstract updateVolumeSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/VolumeSessionKeyId;",
            ">;)V"
        }
    .end annotation
.end method
