.class public Lcom/google/android/apps/books/app/TextModeSelectionState;
.super Ljava/lang/Object;
.source "TextModeSelectionState.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SelectionState;


# instance fields
.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final mPassageIndex:I

.field private final mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private final mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

.field private final mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;


# direct methods
.method public constructor <init>(ILcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;Lcom/google/android/apps/books/render/Renderer;Ljava/util/Comparator;)V
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .param p4, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            "Lcom/google/android/apps/books/annotations/AnnotationTextualContext;",
            "Lcom/google/android/apps/books/render/Renderer;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p5, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/annotations/TextLocation;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mPassageIndex:I

    .line 31
    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocationRange;

    iput-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 32
    invoke-static {p3}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iput-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .line 33
    invoke-static {p4}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/Renderer;

    iput-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 34
    iput-object p5, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mComparator:Ljava/util/Comparator;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/TextModeSelectionState;)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TextModeSelectionState;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    return-object v0
.end method


# virtual methods
.method public createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 69
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    const-string v1, "copy"

    iget-object v3, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v4, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object v6, v2

    move-object v7, v2

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/annotations/Annotation;->withFreshId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 10
    .param p1, "androidColor"    # I
    .param p2, "marginNoteText"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 62
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    const-string v1, "notes"

    iget-object v3, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v4, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move v5, p1

    move-object v6, p2

    move-object v7, v2

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/annotations/Annotation;->withFreshId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;ILjava/lang/String;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public getContainingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .param p1, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget v1, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mPassageIndex:I

    invoke-interface {p1, v1, p2}, Lcom/google/android/apps/books/widget/AnnotationIndex;->getAnnotationsForPassage(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 86
    .local v0, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mComparator:Ljava/util/Comparator;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/annotations/TextLocationRange;->firstContainingAnnotation(Ljava/util/Collection;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    return-object v1
.end method

.method public getNormalizedSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->getNormalizedSelectedText()Ljava/lang/String;

    move-result-object v0

    .line 56
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getOverlappingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .param p1, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget v1, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mPassageIndex:I

    invoke-interface {p1, v1, p2}, Lcom/google/android/apps/books/widget/AnnotationIndex;->getAnnotationsForPassage(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 94
    .local v0, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mComparator:Ljava/util/Comparator;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/annotations/TextLocationRange;->firstOverlappingAnnotation(Ljava/util/Collection;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    return-object v1
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mPassageIndex:I

    return v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextContext:Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    .line 47
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public loadMatchingAnnotation(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "loader"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/geo/LayerAnnotationLoader;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mPassageIndex:I

    new-instance v1, Lcom/google/android/apps/books/app/TextModeSelectionState$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/books/app/TextModeSelectionState$1;-><init>(Lcom/google/android/apps/books/app/TextModeSelectionState;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->load(ILcom/google/android/ublib/utils/Consumer;)V

    .line 116
    return-void
.end method

.method public moveSelectionHandle(IZIIIZ)I
    .locals 9
    .param p1, "passageIndex"    # I
    .param p2, "firstHandle"    # Z
    .param p3, "deltaX"    # I
    .param p4, "deltaY"    # I
    .param p5, "prevRequest"    # I
    .param p6, "isDoneMoving"    # Z

    .prologue
    const/4 v8, 0x0

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-eqz p2, :cond_0

    move v4, v8

    :goto_0
    move v1, p1

    move v3, p6

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/books/render/Renderer;->loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I

    move-result v0

    return v0

    :cond_0
    const/4 v4, 0x1

    goto :goto_0
.end method
