.class Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SortFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SortFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/books/app/SortFragment$Item;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/SortFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/SortFragment;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->this$0:Lcom/google/android/apps/books/app/SortFragment;

    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 104
    return-void
.end method


# virtual methods
.method public add(IZLandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "labelResourceId"    # I
    .param p2, "selected"    # Z
    .param p3, "onClick"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->add(Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method public add(Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "labelResourceId"    # Ljava/lang/CharSequence;
    .param p2, "selected"    # Z
    .param p3, "onClick"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 112
    new-instance v0, Lcom/google/android/apps/books/app/SortFragment$Item;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/SortFragment$Item;-><init>(Lcom/google/android/apps/books/app/SortFragment$1;)V

    .line 113
    .local v0, "item":Lcom/google/android/apps/books/app/SortFragment$Item;
    iput-object p1, v0, Lcom/google/android/apps/books/app/SortFragment$Item;->text1:Ljava/lang/CharSequence;

    .line 114
    iput-boolean p2, v0, Lcom/google/android/apps/books/app/SortFragment$Item;->selected:Z

    .line 115
    iput-object p3, v0, Lcom/google/android/apps/books/app/SortFragment$Item;->onClick:Landroid/view/View$OnClickListener;

    .line 116
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->add(Ljava/lang/Object;)V

    .line 117
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 121
    if-nez p2, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400bf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 127
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/SortFragment$Item;

    .line 129
    .local v0, "item":Lcom/google/android/apps/books/app/SortFragment$Item;
    const v1, 0x7f0e01d3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/apps/books/app/SortFragment$Item;->text1:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    const v1, 0x7f0e01d4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/SortFragment$Item;->selected:Z

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 133
    return-object p2
.end method
