.class public final Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "UploadProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;",
        "Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->buildParsed()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 1

    .prologue
    .line 723
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->create()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 770
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 774
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->buildPartial()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 3

    .prologue
    .line 732
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;-><init>()V

    .line 733
    .local v0, "builder":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    new-instance v1, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;-><init>(Lcom/google/android/apps/books/upload/proto/UploadProto$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    .line 734
    return-object v0
.end method


# virtual methods
.method public addUpload(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .prologue
    .line 850
    if-nez p1, :cond_0

    .line 851
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 853
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2002(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;Ljava/util/List;)Ljava/util/List;

    .line 856
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 857
    return-object p0
.end method

.method public build()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 765
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->buildPartial()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .locals 3

    .prologue
    .line 778
    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    if-nez v1, :cond_0

    .line 779
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 782
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 783
    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    iget-object v2, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2002(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;Ljava/util/List;)Ljava/util/List;

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    .line 787
    .local v0, "returnMe":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    .line 788
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 2

    .prologue
    .line 751
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->create()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->mergeFrom(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 723
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->clone()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 723
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->clone()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->clone()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    .prologue
    .line 792
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->getDefaultInstance()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-object p0

    .line 793
    :cond_1
    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2002(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;Ljava/util/List;)Ljava/util/List;

    .line 797
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->result:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 807
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 808
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 812
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 813
    :sswitch_0
    return-object p0

    .line 818
    :sswitch_1
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;->newBuilder()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;

    move-result-object v0

    .line 819
    .local v0, "subBuilder":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 820
    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$Builder;->buildPartial()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->addUpload(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    goto :goto_0

    .line 808
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 723
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method
