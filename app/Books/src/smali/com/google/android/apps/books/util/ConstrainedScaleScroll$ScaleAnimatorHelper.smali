.class Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;
.super Ljava/lang/Object;
.source "ConstrainedScaleScroll.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleAnimatorHelper"
.end annotation


# instance fields
.field final mAnimationCallback:Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

.field final mAnimator:Landroid/animation/ObjectAnimator;

.field final mCenterEndX:F

.field final mCenterEndY:F

.field final mCenterStartX:F

.field final mCenterStartY:F

.field final mCenterX:F

.field final mCenterY:F

.field final mScaleEnd:F

.field final mScaleStart:F

.field final synthetic this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;FFFLcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;)V
    .locals 3
    .param p2, "newScale"    # F
    .param p3, "zoomFocusX"    # F
    .param p4, "zoomFocusY"    # F
    .param p5, "callback"    # Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 296
    iput-object p1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    iput-object p5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mAnimationCallback:Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

    .line 299
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScale()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mScaleStart:F

    .line 300
    iput p2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mScaleEnd:F

    .line 302
    const-string v0, "lambda"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 305
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentLeft()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentRight()F

    move-result v1

    add-float/2addr v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterX:F

    .line 306
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentTop()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentBottom()F

    move-result v1

    add-float/2addr v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterY:F

    .line 308
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterX:F

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->transformX(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterStartX:F

    .line 309
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterY:F

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->transformY(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterStartY:F

    .line 311
    const/4 v0, 0x1

    invoke-virtual {p1, p2, p3, p4, v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setScale(FFFZ)V

    .line 313
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterX:F

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->transformX(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterEndX:F

    .line 314
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterY:F

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->transformY(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterEndY:F

    .line 316
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->setLambda(F)V

    .line 317
    return-void

    .line 302
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public setLambda(F)V
    .locals 10
    .param p1, "lambda"    # F

    .prologue
    const/4 v9, 0x0

    .line 325
    const/4 v0, 0x0

    .line 327
    .local v0, "clampScroll":Z
    iget v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mScaleStart:F

    iget v7, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mScaleEnd:F

    invoke-static {v6, v7, p1}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v3

    .line 328
    .local v3, "scale":F
    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    invoke-virtual {v6, v3, v9}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setScale(FZ)V

    .line 330
    iget v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterStartX:F

    iget v7, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterEndX:F

    invoke-static {v6, v7, p1}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v4

    .line 331
    .local v4, "targetX":F
    iget v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterStartY:F

    iget v7, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterEndY:F

    invoke-static {v6, v7, p1}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v5

    .line 333
    .local v5, "targetY":F
    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    iget v7, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterX:F

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->transformX(F)F

    move-result v1

    .line 334
    .local v1, "currentX":F
    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    iget v7, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mCenterY:F

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->transformY(F)F

    move-result v2

    .line 336
    .local v2, "currentY":F
    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    sub-float v7, v1, v4

    sub-float v8, v2, v5

    invoke-virtual {v6, v7, v8, v9}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->updateScroll(FFZ)V

    .line 338
    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mAnimationCallback:Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

    iget-object v7, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->this$0:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;->onValueChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V

    .line 339
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 321
    return-void
.end method
