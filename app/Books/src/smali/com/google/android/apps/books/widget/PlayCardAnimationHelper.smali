.class public Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;
.super Ljava/lang/Object;
.source "PlayCardAnimationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;,
        Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;,
        Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
    }
.end annotation


# instance fields
.field private final mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

.field private final mCurrentlyAnimatedFadeOut:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsAnimating:Z

.field protected final mPreviousLocations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;",
            ">;"
        }
    .end annotation
.end field

.field protected final mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;)V
    .locals 1
    .param p1, "cardGroup"    # Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 71
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mPreviousLocations:Ljava/util/Map;

    .line 72
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCurrentlyAnimatedFadeOut:Ljava/util/Set;

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    .line 78
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mIsAnimating:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCurrentlyAnimatedFadeOut:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public isAnimating()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mIsAnimating:Z

    return v0
.end method

.method protected maybeRunFadeOutAnimation(Ljava/lang/String;)V
    .locals 7
    .param p1, "dismissedVolumeId"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->getChildCount()I

    move-result v1

    .line 85
    .local v1, "childCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_0

    .line 86
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    invoke-interface {v6, v5}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 87
    .local v0, "cardToFade":Landroid/view/View;
    const v6, 0x7f0e003b

    invoke-virtual {v0, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 88
    .local v2, "childVolumeId":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 90
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCurrentlyAnimatedFadeOut:Ljava/util/Set;

    invoke-interface {v6, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 91
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->isAnimationEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 92
    new-instance v3, Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-direct {v3, v0}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    .line 94
    .local v3, "fadeAnimationController":Lcom/google/android/ublib/view/FadeAnimationController;
    new-instance v4, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;-><init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;Landroid/view/View;)V

    .line 95
    .local v4, "fadeListener":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;
    const/4 v6, 0x0

    invoke-virtual {v3, v6, v4}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    .line 96
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCurrentlyAnimatedFadeOut:Ljava/util/Set;

    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    .end local v0    # "cardToFade":Landroid/view/View;
    .end local v2    # "childVolumeId":Ljava/lang/String;
    .end local v3    # "fadeAnimationController":Lcom/google/android/ublib/view/FadeAnimationController;
    .end local v4    # "fadeListener":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$FadeListener;
    :cond_0
    :goto_1
    return-void

    .line 98
    .restart local v0    # "cardToFade":Landroid/view/View;
    .restart local v2    # "childVolumeId":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    invoke-interface {v6, v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 85
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected onDocumentsChanged(Ljava/util/List;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 11
    .param p3, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p4, "cardMetaData"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "newDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .local p2, "oldDocList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 111
    .local v2, "book":Lcom/google/android/apps/books/playcards/BookDocument;
    const/4 v8, 0x0

    .line 112
    .local v8, "stillPresent":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 113
    .local v6, "newBook":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-virtual {v6}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 114
    const/4 v8, 0x1

    goto :goto_1

    .line 117
    .end local v6    # "newBook":Lcom/google/android/apps/books/playcards/BookDocument;
    :cond_2
    if-nez v8, :cond_0

    .line 118
    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->maybeRunFadeOutAnimation(Ljava/lang/String;)V

    goto :goto_0

    .line 123
    .end local v2    # "book":Lcom/google/android/apps/books/playcards/BookDocument;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "stillPresent":Z
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 124
    .restart local v2    # "book":Lcom/google/android/apps/books/playcards/BookDocument;
    const/4 v1, 0x0

    .line 125
    .local v1, "alreadyPresent":Z
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 126
    .local v7, "oldBook":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-virtual {v7}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 127
    const/4 v1, 0x1

    goto :goto_3

    .line 130
    .end local v7    # "oldBook":Lcom/google/android/apps/books/playcards/BookDocument;
    :cond_6
    if-nez v1, :cond_4

    .line 131
    invoke-static/range {p5 .. p5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    move-object/from16 v0, p6

    invoke-virtual {p3, p4, v9, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;->getCard(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    move-result-object v3

    .line 133
    .local v3, "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-virtual {p4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v9

    invoke-virtual {v3, v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailAspectRatio(F)V

    .line 134
    invoke-virtual {p4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getFillStyle()Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setThumbnailFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 135
    iget-object v9, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    const/4 v10, 0x1

    invoke-interface {v9, v3, v2, v10}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/playcards/BookDocument;Z)V

    goto :goto_2

    .line 138
    .end local v1    # "alreadyPresent":Z
    .end local v2    # "book":Lcom/google/android/apps/books/playcards/BookDocument;
    .end local v3    # "card":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_7
    return-void
.end method

.method public playAnimators(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mIsAnimating:Z

    .line 143
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 144
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 146
    new-instance v1, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;-><init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 168
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 172
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->recordLocations()V

    goto :goto_0
.end method

.method public recordLocations()V
    .locals 6

    .prologue
    .line 178
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 179
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    invoke-interface {v4, v1}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 180
    .local v0, "child":Landroid/view/View;
    const v4, 0x7f0e003b

    invoke-virtual {v0, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 182
    .local v3, "volumeId":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 183
    new-instance v2, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;-><init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;Landroid/view/View;)V

    .line 189
    .local v2, "location":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
    iget-object v4, v2, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;->location:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, v2, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;->location:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    if-eq v4, v5, :cond_0

    .line 190
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mPreviousLocations:Ljava/util/Map;

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    .end local v2    # "location":Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 194
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "volumeId":Ljava/lang/String;
    :cond_1
    return-void
.end method
