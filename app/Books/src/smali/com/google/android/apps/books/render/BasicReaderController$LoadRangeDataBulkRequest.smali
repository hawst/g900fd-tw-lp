.class Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRangeDataBulkRequest"
.end annotation


# instance fields
.field final mPassageIndex:I

.field final mRanges:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/util/Map;IIZ)V
    .locals 6
    .param p2, "passageIndex"    # I
    .param p4, "externalRequestId"    # I
    .param p5, "taskRequestId"    # I
    .param p6, "reposted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .line 1286
    .local p3, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1287
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p5

    move v3, p4

    move v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1289
    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mPassageIndex:I

    .line 1290
    iput-object p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mRanges:Ljava/util/Map;

    .line 1291
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1295
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 1296
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mPassageIndex:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/ReaderDataModel;->isPassageReady(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1297
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mTaskRequestId:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mPassageIndex:I

    iget-object v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mRanges:Ljava/util/Map;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/books/render/TextReader;->loadRangeDataBulk(IILjava/util/Map;)V

    .line 1298
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$1;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;)V

    .line 1304
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataBulkHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mTaskRequestId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328
    .end local v0    # "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;
    :goto_0
    return-void

    .line 1310
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$2;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;)V

    .line 1324
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mTaskRequestId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1326
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mPassageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mTaskRequestId:I

    invoke-interface {v1, v2, v4, v4, v3}, Lcom/google/android/apps/books/render/TextReader;->loadPage(IIII)V

    goto :goto_0
.end method
