.class Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;
.super Ljava/lang/Object;
.source "ChaptersAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ChaptersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GroupInfo"
.end annotation


# instance fields
.field chapterIndex:I

.field childCount:I

.field containsCurrentPage:Z

.field isCurrentPage:Z

.field final synthetic this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ChaptersAdapter;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method bindChapter(ILcom/google/android/apps/books/view/LocationItemView;)V
    .locals 2
    .param p1, "chapterIndex"    # I
    .param p2, "view"    # Lcom/google/android/apps/books/view/LocationItemView;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$200(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$300(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v1

    invoke-virtual {p2, v0, v1, p1}, Lcom/google/android/apps/books/view/LocationItemView;->bind(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;I)V

    .line 100
    return-void
.end method

.method bindChildView(ILcom/google/android/apps/books/view/LocationItemView;)V
    .locals 6
    .param p1, "childIndex"    # I
    .param p2, "view"    # Lcom/google/android/apps/books/view/LocationItemView;

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->getChapterIndex(I)I

    move-result v2

    .line 87
    .local v2, "chapterIndex":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 88
    if-nez p1, :cond_0

    add-int/lit8 v1, p1, 0x1

    .line 89
    .local v1, "adjacentChildIndex":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$200(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->getChapterIndex(I)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    .line 91
    .local v0, "adjacentChapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getDepth()I

    move-result v3

    .line 92
    .local v3, "depth":I
    invoke-virtual {p0, v3, p2}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->bindCurrentPage(ILcom/google/android/apps/books/view/LocationItemView;)V

    .line 96
    .end local v0    # "adjacentChapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v1    # "adjacentChildIndex":I
    .end local v3    # "depth":I
    :goto_1
    return-void

    .line 88
    :cond_0
    add-int/lit8 v1, p1, -0x1

    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->bindChapter(ILcom/google/android/apps/books/view/LocationItemView;)V

    goto :goto_1
.end method

.method bindCurrentPage(ILcom/google/android/apps/books/view/LocationItemView;)V
    .locals 2
    .param p1, "depth"    # I
    .param p2, "view"    # Lcom/google/android/apps/books/view/LocationItemView;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$200(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentPosition:Lcom/google/android/apps/books/common/Position;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$400(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    invoke-virtual {p2, v0, v1, p1}, Lcom/google/android/apps/books/view/LocationItemView;->bindCurrentPage(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;I)V

    .line 104
    return-void
.end method

.method bindGroupView(ILcom/google/android/apps/books/view/TopLevelChapterView;)V
    .locals 3
    .param p1, "groupIndex"    # I
    .param p2, "view"    # Lcom/google/android/apps/books/view/TopLevelChapterView;

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->isCurrentPage:Z

    if-eqz v1, :cond_1

    .line 61
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->bindCurrentPage(ILcom/google/android/apps/books/view/LocationItemView;)V

    .line 66
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$000(Lcom/google/android/apps/books/app/ChaptersAdapter;)Landroid/widget/ExpandableListView;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->childCount:I

    if-lez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p2, v1, p1, v0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->setGroupState(Landroid/widget/ExpandableListView;IZ)V

    .line 67
    return-void

    .line 63
    :cond_1
    iget v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->bindChapter(ILcom/google/android/apps/books/view/LocationItemView;)V

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mView:Landroid/widget/ExpandableListView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$000(Lcom/google/android/apps/books/app/ChaptersAdapter;)Landroid/widget/ExpandableListView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/view/TopLevelChapterView;->setExpanded(Z)V

    goto :goto_0
.end method

.method getChapterIndex(I)I
    .locals 3
    .param p1, "childIndex"    # I

    .prologue
    .line 70
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->containsCurrentPage:Z

    if-nez v1, :cond_0

    .line 71
    iget v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    .line 81
    :goto_0
    return v1

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mCurrentChapterIndex:I
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$100(Lcom/google/android/apps/books/app/ChaptersAdapter;)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    sub-int v0, v1, v2

    .line 76
    .local v0, "currentPageChildIndex":I
    if-ge p1, v0, :cond_1

    .line 77
    iget v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    :cond_1
    if-ne p1, v0, :cond_2

    .line 79
    const/4 v1, -0x1

    goto :goto_0

    .line 81
    :cond_2
    iget v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    add-int/2addr v1, p1

    goto :goto_0
.end method

.method onChildClick(I)V
    .locals 2
    .param p1, "childIndex"    # I

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->getChapterIndex(I)I

    move-result v0

    .line 116
    .local v0, "chapterIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$500(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;->onCurrentPageSelected()V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$500(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;->onChapterSelected(I)V

    goto :goto_0
.end method

.method onClick()V
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->isCurrentPage:Z

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$500(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;->onCurrentPageSelected()V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->this$0:Lcom/google/android/apps/books/app/ChaptersAdapter;

    # getter for: Lcom/google/android/apps/books/app/ChaptersAdapter;->mCallbacks:Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ChaptersAdapter;->access$500(Lcom/google/android/apps/books/app/ChaptersAdapter;)Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/app/ChaptersAdapter$GroupInfo;->chapterIndex:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;->onChapterSelected(I)V

    goto :goto_0
.end method
