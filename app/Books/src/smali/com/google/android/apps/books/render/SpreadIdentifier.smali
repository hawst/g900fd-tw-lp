.class public Lcom/google/android/apps/books/render/SpreadIdentifier;
.super Ljava/lang/Object;
.source "SpreadIdentifier.java"


# instance fields
.field public final position:Lcom/google/android/apps/books/common/Position;

.field public final spreadOffset:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/Position;I)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "spreadOffset"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/Position;

    iput-object v0, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    .line 31
    iput p2, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    .line 32
    return-void
.end method


# virtual methods
.method public clearOffset()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p0, p1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 55
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 58
    check-cast v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 60
    .local v0, "that":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget v3, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget v4, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 61
    goto :goto_0

    .line 63
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v4, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 64
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->hashCode()I

    move-result v0

    .line 74
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int v0, v1, v2

    .line 75
    return v0
.end method

.method public offsetBy(I)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 3
    .param p1, "spreadOffset"    # I

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget v2, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 44
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "position"

    iget-object v2, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "spreadOffset"

    iget v2, p0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
