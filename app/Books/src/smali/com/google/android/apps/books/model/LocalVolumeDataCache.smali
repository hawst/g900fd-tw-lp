.class public Lcom/google/android/apps/books/model/LocalVolumeDataCache;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "LocalVolumeDataCache.java"


# instance fields
.field public final dismissedRecs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final localVolumeData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;"
        }
    .end annotation
.end field

.field public final volumeDownloadProgress:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    .line 22
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    .line 24
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    return-void
.end method

.method private getLastMode(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 49
    .local v0, "result":Lcom/google/android/apps/books/model/LocalVolumeData;
    if-eqz v0, :cond_0

    .line 50
    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v1

    .line 52
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 41
    .local v0, "progress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getLastMode(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getProgress(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v1

    .line 44
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public fullListenerUpdate(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeDownloadProgress(Ljava/util/Map;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/model/BooksDataListener;->onLocalVolumeData(Ljava/util/Map;)V

    .line 110
    return-void
.end method

.method public getDownloadFraction(Ljava/lang/String;)F
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v0

    .line 57
    .local v0, "progress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->getDownloadFraction()F

    move-result v1

    .line 60
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLicenseAction(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 82
    .local v0, "result":Lcom/google/android/apps/books/model/LocalVolumeData;
    if-eqz v0, :cond_0

    .line 83
    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v1

    .line 85
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPinned(Ljava/lang/String;)Z
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 32
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 33
    .local v0, "result":Lcom/google/android/apps/books/model/LocalVolumeData;
    if-eqz v0, :cond_0

    .line 34
    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v1

    .line 36
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasOfflineLicense(Ljava/lang/String;)Z
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 98
    .local v0, "result":Lcom/google/android/apps/books/model/LocalVolumeData;
    if-eqz v0, :cond_0

    .line 99
    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v1

    .line 101
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFullyDownloaded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v0

    .line 66
    .local v0, "progress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isFullyDownloaded()Z

    move-result v1

    .line 69
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 74
    .local v0, "progress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->isPartiallyOrFullyDownloaded()Z

    move-result v1

    .line 77
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDismissedRecommendations(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, "newDismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 121
    return-void
.end method

.method public onLocalVolumeData(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 115
    return-void
.end method

.method public onVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 29
    return-void
.end method
