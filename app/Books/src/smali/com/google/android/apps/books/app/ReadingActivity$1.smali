.class Lcom/google/android/apps/books/app/ReadingActivity$1;
.super Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;
.source "ReadingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadingActivity;->onRatingClick(Lcom/google/android/apps/books/model/VolumeMetadata;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReadingActivity;

.field final synthetic val$canonicalUrl:Ljava/lang/String;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadingActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->val$canonicalUrl:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onRatingInfoAvailable(Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V
    .locals 4
    .param p1, "ratingInfo"    # Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    .prologue
    const/4 v2, 0x3

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    # setter for: Lcom/google/android/apps/books/app/ReadingActivity;->mRatingInfo:Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReadingActivity;->access$502(Lcom/google/android/apps/books/app/ReadingActivity;Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    .line 544
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->maybeLaunchRating(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 546
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_RATE_THE_BOOK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 547
    const-string v0, "ReadingActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 548
    const-string v0, "ReadingActivity"

    const-string v1, "Rating via ATB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    # getter for: Lcom/google/android/apps/books/app/ReadingActivity;->mFragmentCallbacks:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->access$600(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->val$canonicalUrl:Ljava/lang/String;

    const-string v3, "books_inapp_eob_about"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->access$702(Lcom/google/android/apps/books/app/ReadingActivity;Z)Z

    goto :goto_0

    .line 554
    :cond_4
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_RATE_THE_BOOK_IN_APP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$1;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    # getter for: Lcom/google/android/apps/books/app/ReadingActivity;->mRatingInfo:Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->access$500(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->requestCode:I

    # setter for: Lcom/google/android/apps/books/app/ReadingActivity;->mRatingRequestCode:I
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->access$802(Lcom/google/android/apps/books/app/ReadingActivity;I)I

    .line 557
    const-string v0, "ReadingActivity"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    const-string v0, "ReadingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Rating in-app, ratingInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
