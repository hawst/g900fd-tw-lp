.class Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;)V
    .locals 0

    .prologue
    .line 1123
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "textPointsJson"    # Ljava/lang/String;
    .param p3, "handlesPointsJson"    # Ljava/lang/String;
    .param p4, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p5, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mExternalRequestId:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;

    iget v2, v2, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mPassageIndex:I

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/render/ReaderListener;->onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V

    .line 1129
    return-void
.end method
