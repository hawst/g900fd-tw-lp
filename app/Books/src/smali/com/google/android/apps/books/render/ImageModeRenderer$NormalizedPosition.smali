.class public Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
.super Lcom/google/android/apps/books/util/Either;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NormalizedPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/Either",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/google/android/apps/books/render/SpecialPageIdentifier;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;Z)V
    .locals 0
    .param p1, "bookPageIndex"    # Ljava/lang/Integer;
    .param p2, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;
    .param p3, "isBookPage"    # Z

    .prologue
    .line 1465
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/util/Either;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 1466
    return-void
.end method

.method public static makeNormalPage(Ljava/lang/Integer;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    .locals 3
    .param p0, "bookPageIndex"    # Ljava/lang/Integer;

    .prologue
    .line 1469
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;-><init>(Ljava/lang/Integer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;Z)V

    return-object v0
.end method

.method public static makeSpecialPage(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    .locals 3
    .param p0, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 1473
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;-><init>(Ljava/lang/Integer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;Z)V

    return-object v0
.end method


# virtual methods
.method public getBookPageIndex()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1481
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->left:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getSpecialPage()Lcom/google/android/apps/books/render/SpecialPageIdentifier;
    .locals 1

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->right:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    return-object v0
.end method

.method public isBookPage()Z
    .locals 1

    .prologue
    .line 1477
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->isLeft:Z

    return v0
.end method
