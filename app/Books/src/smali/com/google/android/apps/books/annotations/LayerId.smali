.class public enum Lcom/google/android/apps/books/annotations/LayerId;
.super Ljava/lang/Enum;
.source "LayerId.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/annotations/LayerId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/annotations/LayerId;

.field public static final enum BOOKMARKS:Lcom/google/android/apps/books/annotations/LayerId;

.field public static final enum COPY:Lcom/google/android/apps/books/annotations/LayerId;

.field public static final enum DICTIONARY:Lcom/google/android/apps/books/annotations/LayerId;

.field public static final enum GEO:Lcom/google/android/apps/books/annotations/LayerId;

.field public static final enum NOTES:Lcom/google/android/apps/books/annotations/LayerId;


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mStoreSyncedAnnotations:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 10
    new-instance v0, Lcom/google/android/apps/books/annotations/LayerId$1;

    const-string v1, "BOOKMARKS"

    const-string v2, "bookmarks"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/books/annotations/LayerId$1;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/LayerId;->BOOKMARKS:Lcom/google/android/apps/books/annotations/LayerId;

    .line 16
    new-instance v0, Lcom/google/android/apps/books/annotations/LayerId;

    const-string v1, "GEO"

    const-string v2, "geo"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/apps/books/annotations/LayerId;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/LayerId;->GEO:Lcom/google/android/apps/books/annotations/LayerId;

    .line 17
    new-instance v0, Lcom/google/android/apps/books/annotations/LayerId;

    const-string v1, "NOTES"

    const-string v2, "notes"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/books/annotations/LayerId;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/LayerId;->NOTES:Lcom/google/android/apps/books/annotations/LayerId;

    .line 18
    new-instance v0, Lcom/google/android/apps/books/annotations/LayerId;

    const-string v1, "DICTIONARY"

    const-string v2, "dictionary"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/books/annotations/LayerId;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/LayerId;->DICTIONARY:Lcom/google/android/apps/books/annotations/LayerId;

    .line 19
    new-instance v0, Lcom/google/android/apps/books/annotations/LayerId;

    const-string v1, "COPY"

    const-string v2, "copy"

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/google/android/apps/books/annotations/LayerId;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/LayerId;->COPY:Lcom/google/android/apps/books/annotations/LayerId;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/books/annotations/LayerId;

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->BOOKMARKS:Lcom/google/android/apps/books/annotations/LayerId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->GEO:Lcom/google/android/apps/books/annotations/LayerId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->NOTES:Lcom/google/android/apps/books/annotations/LayerId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->DICTIONARY:Lcom/google/android/apps/books/annotations/LayerId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->COPY:Lcom/google/android/apps/books/annotations/LayerId;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/books/annotations/LayerId;->$VALUES:[Lcom/google/android/apps/books/annotations/LayerId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "storeSyncedAnnotations"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/LayerId;->mName:Ljava/lang/String;

    .line 36
    iput-boolean p4, p0, Lcom/google/android/apps/books/annotations/LayerId;->mStoreSyncedAnnotations:Z

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/apps/books/annotations/LayerId$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Z
    .param p5, "x4"    # Lcom/google/android/apps/books/annotations/LayerId$1;

    .prologue
    .line 8
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/annotations/LayerId;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/LayerId;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    :try_start_0
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/LayerId;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/LayerId;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 30
    :goto_0
    return-object v1

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/LayerId;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/google/android/apps/books/annotations/LayerId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/LayerId;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/annotations/LayerId;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->$VALUES:[Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/annotations/LayerId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/annotations/LayerId;

    return-object v0
.end method


# virtual methods
.method public contextCanIncludeNearbyPages()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/LayerId;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public storeSyncedAnnotations()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/LayerId;->mStoreSyncedAnnotations:Z

    return v0
.end method
