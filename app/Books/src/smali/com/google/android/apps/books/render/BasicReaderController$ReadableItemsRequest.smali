.class Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReadableItemsRequest"
.end annotation


# instance fields
.field private final mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;II)V
    .locals 6
    .param p2, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    .param p3, "externalRequestId"    # I
    .param p4, "requestId"    # I

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 245
    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 247
    iput-object p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .line 248
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;)Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    return-object v0
.end method

.method private handleExactPassage(I)V
    .locals 5
    .param p1, "passageIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 341
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/render/ReaderDataModel;->isPassageReady(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 343
    new-instance v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;)V

    .line 370
    .local v1, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnReadableItemsReadyHandlers:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1000(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    iget v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/render/TextReader;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;I)V

    .line 399
    .end local v1    # "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;
    :goto_0
    return-void

    .line 375
    :cond_0
    move v0, p1

    .line 376
    .local v0, "finalPassageIndex":I
    new-instance v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$4;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;I)V

    .line 396
    .local v1, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-interface {v2, p1, v4, v4, v3}, Lcom/google/android/apps/books/render/TextReader;->loadPage(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 258
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const-string v1, "ReaderController"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Issuing ControllerReadableItemsRequest "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPassage()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPosition()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPosition()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPassage()I

    move-result v11

    .line 269
    .local v11, "passage":I
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPosition()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 270
    const/4 v7, 0x1

    .line 284
    .local v7, "exactPassage":Z
    :goto_1
    if-eqz v7, :cond_4

    .line 285
    invoke-direct {p0, v11}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->handleExactPassage(I)V

    .line 334
    :goto_2
    return-void

    .line 259
    .end local v7    # "exactPassage":Z
    .end local v11    # "passage":I
    :cond_1
    const-string v0, "position=null"

    goto :goto_0

    .line 272
    .restart local v11    # "passage":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPosition()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/ReaderDataModel;->getPassageIndex(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    .line 274
    .local v9, "foundPassage":Ljava/lang/Integer;
    if-eqz v9, :cond_3

    .line 275
    const/4 v7, 0x1

    .line 276
    .restart local v7    # "exactPassage":Z
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v11

    goto :goto_1

    .line 278
    .end local v7    # "exactPassage":Z
    :cond_3
    const/4 v7, 0x0

    .restart local v7    # "exactPassage":Z
    goto :goto_1

    .line 289
    .end local v9    # "foundPassage":Ljava/lang/Integer;
    :cond_4
    move v8, v11

    .line 290
    .local v8, "finalPassage":I
    new-instance v10, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;

    invoke-direct {v10, p0, v8}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;I)V

    .line 313
    .local v10, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    new-instance v6, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;)V

    .line 328
    .local v6, "errorHandler":Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mLoadPositionFailureHandlers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$800(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPassage()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPosition()Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    move-object v4, v3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/TextReader;->loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;I)V

    goto :goto_2
.end method
