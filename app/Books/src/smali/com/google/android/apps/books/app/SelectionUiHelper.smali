.class public Lcom/google/android/apps/books/app/SelectionUiHelper;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;,
        Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;,
        Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    }
.end annotation


# instance fields
.field private mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

.field private final mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

.field private final mDrawingColors:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

.field private mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    .param p2, "drawingColors"    # Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    .prologue
    .line 1349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1350
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    .line 1351
    iput-object p2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDrawingColors:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    .line 1352
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/SelectionUiHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->discardPopup()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/SelectionUiHelper;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getAndroidColor(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDrawingColors:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/widget/SelectionPopup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/SelectionUiHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getCurrentAndroidColor()I

    move-result v0

    return v0
.end method

.method private discardPopup()V
    .locals 1

    .prologue
    .line 1319
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    if-eqz v0, :cond_0

    .line 1320
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->hide()V

    .line 1322
    :cond_0
    return-void
.end method

.method private getAndroidColor(I)I
    .locals 1
    .param p1, "highlightColorIndex"    # I

    .prologue
    .line 1327
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1328
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getCurrentAndroidColor()I

    move-result v0

    .line 1330
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getDrawingColorFromPrefColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private getCurrentAndroidColor()I
    .locals 2

    .prologue
    .line 1335
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 1336
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHighlightColor()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getDrawingColorFromPrefColor(I)I

    move-result v1

    return v1
.end method


# virtual methods
.method public clearPopup()V
    .locals 1

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    if-eqz v0, :cond_0

    .line 1421
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->hide()V

    .line 1422
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    .line 1424
    :cond_0
    return-void
.end method

.method public endActiveSelection()Z
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    .line 1372
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->endSelection()V

    .line 1373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .line 1374
    const/4 v0, 0x1

    .line 1376
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentSelectionInstance()Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    .locals 1

    .prologue
    .line 1428
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    return-object v0
.end method

.method protected getDrawingColorFromPrefColor(I)I
    .locals 1
    .param p1, "highlightColorIndex"    # I

    .prologue
    .line 1340
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getDrawingColorFromPrefColor(I)I

    move-result v0

    return v0
.end method

.method public getInitialTextToEdit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getInitialTextToEdit()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getPopup()Lcom/google/android/apps/books/widget/SelectionPopup;
    .locals 1

    .prologue
    .line 1307
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getPopup(Z)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v0

    return-object v0
.end method

.method protected getPopup(Z)Lcom/google/android/apps/books/widget/SelectionPopup;
    .locals 2
    .param p1, "createIfNecessary"    # Z

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1313
    new-instance v0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    .line 1315
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    return-object v0
.end method

.method public highlightSelectionFromPrefColorId(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1414
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    .line 1415
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->highlightSelectionFromPrefColorId(IZ)V

    .line 1417
    :cond_0
    return-void
.end method

.method public isSelectionActive()Z
    .locals 1

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startEditingNote(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;Z)V
    .locals 2
    .param p1, "info"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;
    .param p2, "showKeyboard"    # Z

    .prologue
    .line 1383
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 1384
    new-instance v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper;Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .line 1385
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->beginSelection(Z)V

    .line 1386
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->showNoteText(Z)V
    invoke-static {v0, p2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$2900(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Z)V

    .line 1387
    return-void
.end method

.method public startTextSelection(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    .line 1365
    :goto_0
    return-void

    .line 1361
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->dismissInfoCards()V

    .line 1363
    new-instance v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper;Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .line 1364
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->beginSelection(Z)V

    goto :goto_0
.end method

.method public stopEditingNote(Ljava/lang/String;)V
    .locals 1
    .param p1, "finalText"    # Ljava/lang/String;

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    .line 1408
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->stopEditingNote(Ljava/lang/String;)V

    .line 1410
    :cond_0
    return-void
.end method

.method public textSelectionChanged()V
    .locals 1

    .prologue
    .line 1394
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    if-eqz v0, :cond_0

    .line 1395
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper;->mCurrentSelectionInstance:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->textSelectionChanged()V

    .line 1397
    :cond_0
    return-void
.end method
