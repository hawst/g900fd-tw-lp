.class public Lcom/google/android/apps/books/annotations/EventualMap;
.super Ljava/lang/Object;
.source "EventualMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mConsumers:Lcom/google/common/collect/Multimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multimap",
            "<TK;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private final mValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/apps/books/annotations/EventualMap;, "Lcom/google/android/apps/books/annotations/EventualMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mValues:Ljava/util/Map;

    .line 20
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mConsumers:Lcom/google/common/collect/Multimap;

    return-void
.end method

.method public static create()Lcom/google/android/apps/books/annotations/EventualMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/apps/books/annotations/EventualMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/apps/books/annotations/EventualMap;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/EventualMap;-><init>()V

    return-object v0
.end method


# virtual methods
.method public load(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/apps/books/annotations/EventualMap;, "Lcom/google/android/apps/books/annotations/EventualMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mConsumers:Lcom/google/common/collect/Multimap;

    invoke-interface {v2, p1}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/utils/Consumer;

    .line 28
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    invoke-interface {v0, p2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    .end local v0    # "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mConsumers:Lcom/google/common/collect/Multimap;

    invoke-interface {v2, p1}, Lcom/google/common/collect/Multimap;->removeAll(Ljava/lang/Object;)Ljava/util/Collection;

    .line 31
    return-void
.end method

.method public loadAndCache(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/apps/books/annotations/EventualMap;, "Lcom/google/android/apps/books/annotations/EventualMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mValues:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/EventualMap;->load(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method public whenLoaded(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/apps/books/annotations/EventualMap;, "Lcom/google/android/apps/books/annotations/EventualMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mValues:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mValues:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 51
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/EventualMap;->mConsumers:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 54
    const/4 v0, 0x0

    goto :goto_0
.end method
