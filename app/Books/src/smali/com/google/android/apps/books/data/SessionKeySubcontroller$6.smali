.class Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;->handleNewAccountKeyOnControlThread(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

.field final synthetic val$key:Lcom/google/android/apps/books/model/SessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;->val$key:Lcom/google/android/apps/books/model/SessionKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 6
    .param p1, "callbacks"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    const/4 v3, 0x0

    .line 236
    new-instance v2, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;
    invoke-static {v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$000(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;->val$key:Lcom/google/android/apps/books/model/SessionKey;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/books/model/LocalSessionKey;-><init>(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)V

    .line 238
    .local v2, "newLocalKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->ADD_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;
    invoke-static {}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$700()Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    move-result-object v4

    move-object v1, p1

    move-object v5, v3

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->checkAndPublishKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$800(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V

    .line 239
    return-void
.end method
