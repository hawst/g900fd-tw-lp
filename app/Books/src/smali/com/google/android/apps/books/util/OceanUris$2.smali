.class final Lcom/google/android/apps/books/util/OceanUris$2;
.super Ljava/lang/Object;
.source "OceanUris.java"

# interfaces
.implements Lcom/google/android/apps/books/util/OceanUris$BookStore;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/OceanUris;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAboutTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 147
    # invokes: Lcom/google/android/apps/books/util/OceanUris;->finskyWithCampaign(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;
    invoke-static {p1, p3}, Lcom/google/android/apps/books/util/OceanUris;->access$100(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "books"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "details"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getBuyTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 157
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/util/OceanUris$2;->getAboutTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getSearchUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 136
    # invokes: Lcom/google/android/apps/books/util/OceanUris;->finskyWithCampaign(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;
    invoke-static {p1, p3}, Lcom/google/android/apps/books/util/OceanUris;->access$100(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "so"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "c"

    const-string v2, "books"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 128
    # invokes: Lcom/google/android/apps/books/util/OceanUris;->finskyWithCampaign(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;
    invoke-static {p1, p2}, Lcom/google/android/apps/books/util/OceanUris;->access$100(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "books"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getStoreCollectionUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "collectionId"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 162
    # invokes: Lcom/google/android/apps/books/util/OceanUris;->fallbackStoreCollectionUri(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/books/util/OceanUris;->access$000(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
