.class Lcom/google/android/apps/books/app/ReaderFragment$20$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$20;->onBookmarksChanged(Ljava/util/SortedMap;Ljava/util/SortedMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/widget/DevicePageRendering;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

.field final synthetic val$newBookmarks:Ljava/util/SortedMap;

.field final synthetic val$oldBookmarks:Ljava/util/SortedMap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$20;Ljava/util/SortedMap;Ljava/util/SortedMap;)V
    .locals 0

    .prologue
    .line 3906
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->val$newBookmarks:Ljava/util/SortedMap;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->val$oldBookmarks:Ljava/util/SortedMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 3906
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->take(Ljava/util/List;)V

    return-void
.end method

.method public take(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3909
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->val$newBookmarks:Ljava/util/SortedMap;

    invoke-static {p1, v2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/List;Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v1

    .line 3911
    .local v1, "viewableBookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->val$oldBookmarks:Ljava/util/SortedMap;

    invoke-static {p1, v2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getViewableBookmarks(Ljava/util/List;Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v0

    .line 3913
    .local v0, "oldViewableBookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 3914
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v3, v4, v2}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3915
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/app/ReaderMenu;->setBookmarkIsAdd(Z)V

    .line 3917
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$20$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$20;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->handleBookmarkAccessibility(Ljava/util/List;Ljava/util/List;)V
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;Ljava/util/List;)V

    .line 3919
    return-void

    .line 3914
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
