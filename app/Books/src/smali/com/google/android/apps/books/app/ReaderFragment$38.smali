.class final Lcom/google/android/apps/books/app/ReaderFragment$38;
.super Lcom/google/android/apps/books/util/LeakSafeCallback;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->createResumeTtsHandler(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/LeakSafeCallback",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 8128
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/LeakSafeCallback;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected handleMessage(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/os/Message;)Z
    .locals 1
    .param p1, "target"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 8131
    const/4 v0, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInHoverGracePeriod:Z
    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8132
    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V
    invoke-static {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8133
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic handleMessage(Ljava/lang/Object;Landroid/os/Message;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/os/Message;

    .prologue
    .line 8128
    check-cast p1, Lcom/google/android/apps/books/app/ReaderFragment;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment$38;->handleMessage(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/os/Message;)Z

    move-result v0

    return v0
.end method
