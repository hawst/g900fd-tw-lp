.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;
.super Ljava/lang/Object;
.source "TextureEndOfBookView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;->val$activity:Landroid/app/Activity;

    const v1, 0x7f0f00b8

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnectedElseToast(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;->val$activity:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/books/app/ReadingActivity;

    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$5;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->onRatingClick(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 263
    :cond_0
    return-void
.end method
