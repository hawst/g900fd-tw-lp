.class final enum Lcom/google/android/apps/books/app/LibraryFilter$2;
.super Lcom/google/android/apps/books/app/LibraryFilter;
.source "LibraryFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LibraryFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "x0"    # I

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/app/LibraryFilter;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/LibraryFilter$1;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/books/widget/CardData;)Z
    .locals 3
    .param p1, "data"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    const/4 v1, 0x1

    .line 42
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/LibraryFilter$2;->passesCurrentDownloadFilter(Ljava/lang/String;)Z

    move-result v0

    .line 43
    .local v0, "passesCurrentDownloadFilter":Z
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/apps/books/widget/CardData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LibraryFilter$2;->apply(Lcom/google/android/apps/books/widget/CardData;)Z

    move-result v0

    return v0
.end method

.method public shouldShowWhenEmpty(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method
