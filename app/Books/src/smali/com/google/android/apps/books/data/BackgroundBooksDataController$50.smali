.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->setRequestedDictionaryLanguages(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$languages:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1369
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;->val$languages:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 1372
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$5000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/DataControllerStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;->val$languages:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/DataControllerStore;->setRequestedDictionaryLanguages(Ljava/util/List;)V

    .line 1373
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/LocalDictionarySubController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;->val$languages:Ljava/util/List;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->syncLocalDictionaries(Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 1374
    return-void
.end method
