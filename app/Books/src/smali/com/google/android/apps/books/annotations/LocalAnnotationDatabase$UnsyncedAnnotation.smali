.class public Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;
.super Ljava/lang/Object;
.source "LocalAnnotationDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnsyncedAnnotation"
.end annotation


# instance fields
.field public final annotation:Lcom/google/android/apps/books/annotations/Annotation;

.field public final volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 0
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 31
    return-void
.end method
