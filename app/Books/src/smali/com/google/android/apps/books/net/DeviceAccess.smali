.class public final Lcom/google/android/apps/books/net/DeviceAccess;
.super Ljava/lang/Object;
.source "DeviceAccess.java"


# static fields
.field static final DEFAULT_SECONDS:J = 0x78L

.field public static final sUnknown:Lcom/google/android/apps/books/net/DeviceAccess;


# instance fields
.field private final mAllowed:Z

.field private final mMaxDevices:J

.field private final mSeconds:J

.field private final mUnrestricted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 63
    new-instance v1, Lcom/google/android/apps/books/net/DeviceAccess;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const-wide/16 v6, 0x78

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/net/DeviceAccess;-><init>(ZZJJ)V

    sput-object v1, Lcom/google/android/apps/books/net/DeviceAccess;->sUnknown:Lcom/google/android/apps/books/net/DeviceAccess;

    return-void
.end method

.method private constructor <init>(ZZJJ)V
    .locals 1
    .param p1, "unrestricted"    # Z
    .param p2, "allowed"    # Z
    .param p3, "maxDevices"    # J
    .param p5, "seconds"    # J

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mUnrestricted:Z

    .line 73
    iput-boolean p2, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mAllowed:Z

    .line 74
    iput-wide p3, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mMaxDevices:J

    .line 75
    iput-wide p5, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mSeconds:J

    .line 76
    return-void
.end method

.method public static newInstance(ZZJJ)Lcom/google/android/apps/books/net/DeviceAccess;
    .locals 8
    .param p0, "unrestricted"    # Z
    .param p1, "allowed"    # Z
    .param p2, "maxDevices"    # J
    .param p4, "seconds"    # J

    .prologue
    .line 80
    new-instance v1, Lcom/google/android/apps/books/net/DeviceAccess;

    move v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/net/DeviceAccess;-><init>(ZZJJ)V

    return-object v1
.end method


# virtual methods
.method public getMaxDevices()J
    .locals 2

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mMaxDevices:J

    return-wide v0
.end method

.method public getSeconds()J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mSeconds:J

    return-wide v0
.end method

.method public isAllowed()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mAllowed:Z

    return v0
.end method

.method public isUnrestricted()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mUnrestricted:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceAccess{unrestricted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mUnrestricted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mAllowed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", devices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mMaxDevices:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/books/net/DeviceAccess;->mSeconds:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
