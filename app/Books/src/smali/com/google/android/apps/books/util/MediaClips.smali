.class public Lcom/google/android/apps/books/util/MediaClips;
.super Ljava/lang/Object;
.source "MediaClips.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/MediaClips$SegmentIdComparator;,
        Lcom/google/android/apps/books/util/MediaClips$MediaClip;,
        Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;
    }
.end annotation


# instance fields
.field private mFirstClipInSmil:Z

.field private final mSegmentIdToIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegmentToMediaClipList:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentIdToIndex:Ljava/util/Map;

    .line 35
    new-instance v0, Ljava/util/TreeMap;

    new-instance v1, Lcom/google/android/apps/books/util/MediaClips$SegmentIdComparator;

    iget-object v2, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentIdToIndex:Ljava/util/Map;

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/util/MediaClips$SegmentIdComparator;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentToMediaClipList:Ljava/util/TreeMap;

    .line 37
    return-void
.end method


# virtual methods
.method public addClip(Lcom/google/android/apps/books/util/MediaClips$MediaClip;)V
    .locals 6
    .param p1, "mediaClip"    # Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 76
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getSegmentId()Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "segmentId":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentIdToIndex:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 81
    const-string v3, "MediaClips"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    const-string v3, "MediaClips"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Discarding clip for unrecognized id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getClipEndMs()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getClipBeginMs()I

    move-result v4

    sub-int v0, v3, v4

    .line 89
    .local v0, "clipDurationMs":I
    if-gtz v0, :cond_2

    .line 90
    const-string v3, "MediaClips"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 91
    const-string v3, "MediaClips"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Discard clip with invalid duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 96
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentToMediaClipList:Ljava/util/TreeMap;

    invoke-virtual {v3, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 97
    .local v1, "segmentClipList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;"
    if-nez v1, :cond_4

    .line 99
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 100
    iget-object v3, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentToMediaClipList:Ljava/util/TreeMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string v3, "MediaClips"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 102
    const-string v3, "MediaClips"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Creating clip list for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/books/util/MediaClips;->mFirstClipInSmil:Z

    .line 112
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/books/util/MediaClips;->mFirstClipInSmil:Z

    if-eqz v3, :cond_3

    .line 108
    new-instance v3, Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/books/util/MediaClips$DuplicateClipsForSegmentException;-><init>(Lcom/google/android/apps/books/util/MediaClips;Ljava/lang/String;)V

    throw v3
.end method

.method public addSmil(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "resourceInputStream"    # Ljava/io/InputStream;

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/MediaClips;->mFirstClipInSmil:Z

    .line 49
    invoke-static {p1, p0}, Lcom/google/android/apps/books/util/MediaClipsLoader;->parseSmil(Ljava/io/InputStream;Lcom/google/android/apps/books/util/MediaClips;)V

    .line 50
    return-void
.end method

.method public getNextSegmentClipList(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 2
    .param p1, "startingSegmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentToMediaClipList:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 61
    .local v0, "firstEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;>;"
    if-eqz v0, :cond_0

    .line 63
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 65
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaClips;->mSegmentToMediaClipList:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    return v0
.end method
