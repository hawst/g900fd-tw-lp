.class Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;
.super Ljava/lang/Object;
.source "BooksCardsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setErrorOverlay(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/upload/Upload;Landroid/content/res/Resources;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

.field final synthetic val$dialogMessage:I

.field final synthetic val$neutralButton:I

.field final synthetic val$positiveButton:I

.field final synthetic val$upload:Lcom/google/android/apps/books/upload/Upload;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Lcom/google/android/apps/books/upload/Upload;III)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$upload:Lcom/google/android/apps/books/upload/Upload;

    iput p3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$dialogMessage:I

    iput p4, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$positiveButton:I

    iput p5, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$neutralButton:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 307
    new-instance v2, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4$1;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;)V

    .line 314
    .local v2, "positiveListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4$2;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;)V

    .line 322
    .local v1, "neutralListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 323
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$dialogMessage:I

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 324
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$positiveButton:I

    invoke-virtual {v0, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 325
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$neutralButton:I

    if-eqz v3, :cond_0

    .line 326
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$4;->val$neutralButton:I

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 329
    return-void
.end method
