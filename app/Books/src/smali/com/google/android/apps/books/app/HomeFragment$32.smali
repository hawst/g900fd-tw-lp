.class Lcom/google/android/apps/books/app/HomeFragment$32;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$container:Landroid/view/ViewGroup;

.field final synthetic val$surveyPrompt:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2884
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->val$container:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->val$surveyPrompt:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2894
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->val$container:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->val$surveyPrompt:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2895
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2890
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->val$container:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$32;->val$surveyPrompt:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2891
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2898
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2887
    return-void
.end method
