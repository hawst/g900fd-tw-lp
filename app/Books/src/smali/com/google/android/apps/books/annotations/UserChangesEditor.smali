.class public interface abstract Lcom/google/android/apps/books/annotations/UserChangesEditor;
.super Ljava/lang/Object;
.source "UserChangesEditor.java"


# virtual methods
.method public abstract uiAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Updateable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Updateable",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract uiEdit(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)V
.end method

.method public abstract uiRemove(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method

.method public abstract uiUpdateLastUsedTimestampToNow(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method
