.class final Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;
.super Ljava/lang/Object;
.source "TransientInfoCardsLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AnimatorTarget"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V
    .locals 0

    .prologue
    .line 917
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;

    .prologue
    .line 917
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    return-void
.end method


# virtual methods
.method public getCardsTranslation()F
    .locals 2

    .prologue
    .line 920
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public setCardsTranslation(F)V
    .locals 3
    .param p1, "translation"    # F

    .prologue
    .line 925
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$600(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1700(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->destinationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getLowerState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$900(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v0

    .line 927
    .local v0, "lowerState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # invokes: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsTranslation(FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    invoke-static {v1, p1, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$1000(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    .line 928
    return-void
.end method
