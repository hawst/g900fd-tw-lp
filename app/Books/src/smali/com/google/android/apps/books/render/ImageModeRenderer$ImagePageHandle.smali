.class Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;
.super Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImagePageHandle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;I)V
    .locals 1
    .param p2, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "bookPageIndex"    # I

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    .line 332
    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    invoke-static {p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$100(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    move-result-object v0

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;I)V

    .line 333
    return-void
.end method


# virtual methods
.method public getGridRowIndex()I
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$300(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;->getBookPageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->bookPageIndexToGridRowIndex(I)I

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageDimensions:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$200(Lcom/google/android/apps/books/render/ImageModeRenderer;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageDimensions:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$200(Lcom/google/android/apps/books/render/ImageModeRenderer;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method
