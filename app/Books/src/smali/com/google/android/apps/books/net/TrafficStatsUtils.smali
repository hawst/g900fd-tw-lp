.class public Lcom/google/android/apps/books/net/TrafficStatsUtils;
.super Ljava/lang/Object;
.source "TrafficStatsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;
    }
.end annotation


# direct methods
.method public static clearThreadFlags()V
    .locals 4

    .prologue
    .line 90
    const-string v1, "TrafficStatsUtils"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->getThreadStatsTag()I

    move-result v0

    .line 92
    .local v0, "currentTag":I
    if-nez v0, :cond_1

    .line 93
    const-string v1, "TrafficStatsUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearThreadFlags expected non zero tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    :goto_0
    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    .line 99
    return-void

    .line 95
    :cond_1
    const-string v1, "TrafficStatsUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearThreadFlags called with current tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getTag(Ljava/util/Set;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "flags":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    const/4 v2, 0x0

    .line 104
    .local v2, "value":I
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    .line 105
    .local v0, "flag":Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;
    invoke-virtual {v0}, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->getFlagValue()I

    move-result v3

    or-int/2addr v2, v3

    .line 106
    goto :goto_0

    .line 108
    .end local v0    # "flag":Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;
    :cond_0
    return v2
.end method

.method public static incrementOperationCount()V
    .locals 4

    .prologue
    .line 78
    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->getThreadStatsTag()I

    move-result v0

    .line 79
    .local v0, "currentTag":I
    const-string v1, "TrafficStatsUtils"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    const-string v1, "TrafficStatsUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "incrementOperationCount called with current int tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v4/net/TrafficStatsCompat;->incrementOperationCount(II)V

    .line 84
    return-void
.end method

.method public static setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V
    .locals 1
    .param p0, "flag"    # Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    .prologue
    .line 53
    invoke-static {p0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlags(Ljava/util/EnumSet;)V

    .line 54
    return-void
.end method

.method public static setThreadFlags(Ljava/util/EnumSet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "flags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->getTag(Ljava/util/Set;)I

    move-result v1

    .line 61
    .local v1, "tag":I
    const-string v2, "TrafficStatsUtils"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->getThreadStatsTag()I

    move-result v0

    .line 63
    .local v0, "currentTag":I
    if-eqz v0, :cond_1

    .line 65
    const-string v2, "TrafficStatsUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setThreadFlags called with int tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Current tag is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v0    # "currentTag":I
    :cond_0
    :goto_0
    invoke-static {v1}, Landroid/support/v4/net/TrafficStatsCompat;->setThreadStatsTag(I)V

    .line 72
    return-void

    .line 68
    .restart local v0    # "currentTag":I
    :cond_1
    const-string v2, "TrafficStatsUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setThreadFlags called with int tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
