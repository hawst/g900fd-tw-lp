.class public Lcom/google/android/apps/books/model/CollectionVolumesEditor;
.super Ljava/lang/Object;
.source "CollectionVolumesEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/CollectionVolumesEditor$1;,
        Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;
    }
.end annotation


# instance fields
.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 0
    .param p1, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p2, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 29
    return-void
.end method


# virtual methods
.method public backgroundRemove(JLjava/lang/String;)Landroid/os/AsyncTask;
    .locals 7
    .param p1, "collectionId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;

    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;-><init>(Lcom/google/android/apps/books/model/CollectionVolumesEditor;JLjava/lang/String;Lcom/google/android/apps/books/model/CollectionVolumesEditor$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public synchronousAdd(JLjava/lang/String;)V
    .locals 5
    .param p1, "collectionId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v0, p3, v1, v2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->getVolumeData(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/model/VolumeData;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/model/BooksDataStore;->addToCollection(JLjava/lang/String;)V

    .line 40
    return-void
.end method

.method public synchronousRemove(JLjava/lang/String;)V
    .locals 1
    .param p1, "collectionId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/model/BooksDataStore;->removeFromCollection(JLjava/lang/String;)V

    .line 44
    return-void
.end method
