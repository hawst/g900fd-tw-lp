.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getRequestedDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1349
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$5000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/DataControllerStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/DataControllerStore;->getRequestedDictionaryMetadataList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1353
    return-void
.end method
