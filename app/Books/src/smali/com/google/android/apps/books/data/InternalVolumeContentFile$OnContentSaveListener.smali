.class public interface abstract Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;
.super Ljava/lang/Object;
.source "InternalVolumeContentFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/InternalVolumeContentFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnContentSaveListener"
.end annotation


# virtual methods
.method public abstract onSaved(Lcom/google/android/apps/books/model/EncryptedContent;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
