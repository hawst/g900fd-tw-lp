.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNoteAtCharOffset(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0

    .prologue
    .line 593
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1300(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    move-result-object v0

    if-nez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1400(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1500(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->createNoteMenu(Landroid/content/Context;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1600(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/content/Context;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    move-result-object v1

    # setter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1302(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1300(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->show()V

    .line 601
    return-void
.end method
