.class Lcom/google/android/apps/books/app/HomeFragment$21;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->withShouldAutoStartOnboarding(Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$shouldAutoStartOnboardingConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1601
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$21;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$21;->val$shouldAutoStartOnboardingConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isSchoolOwned"    # Ljava/lang/Boolean;

    .prologue
    .line 1604
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1605
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$21;->val$shouldAutoStartOnboardingConsumer:Lcom/google/android/ublib/utils/Consumer;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1610
    :goto_0
    return-void

    .line 1609
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$21;->val$shouldAutoStartOnboardingConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$21;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mIsNewUserForOnboarding:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$2800(Lcom/google/android/apps/books/app/HomeFragment;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1601
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$21;->take(Ljava/lang/Boolean;)V

    return-void
.end method
