.class Lcom/google/android/apps/books/render/ReaderRenderer$5;
.super Ljava/lang/Object;
.source "ReaderRenderer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderRenderer;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderRenderer;

.field final synthetic val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

.field final synthetic val$sequenceNumber:I

.field final synthetic val$task:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderRenderer;Lcom/google/android/apps/books/render/RenderResponseConsumer;ILjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->this$0:Lcom/google/android/apps/books/render/ReaderRenderer;

    iput-object p2, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

    iput p3, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$sequenceNumber:I

    iput-object p4, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$task:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/RenderResponseConsumer;->isPurgeable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->this$0:Lcom/google/android/apps/books/render/ReaderRenderer;

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$sequenceNumber:I

    # invokes: Lcom/google/android/apps/books/render/ReaderRenderer;->shouldHonorRenderRequest(I)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/ReaderRenderer;->access$100(Lcom/google/android/apps/books/render/ReaderRenderer;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer$5;->val$task:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
