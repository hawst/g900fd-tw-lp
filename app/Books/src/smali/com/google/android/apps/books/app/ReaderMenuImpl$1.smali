.class Lcom/google/android/apps/books/app/ReaderMenuImpl$1;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;-><init>(Landroid/content/Context;Landroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/apps/books/util/Navigator;Landroid/support/v7/app/ActionBar;Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/TableOfContents;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$1;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$1;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$100(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$1;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearchesPopup:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$100(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 131
    :cond_0
    return-void
.end method
