.class public Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "SyncDisabledDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;
    }
.end annotation


# instance fields
.field private final mNegativeClick:Landroid/content/DialogInterface$OnClickListener;

.field private final mPositiveClick:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;-><init>(Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    .line 69
    new-instance v0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$2;-><init>(Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->mNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->markHaveNagged()V

    return-void
.end method

.method private markHaveNagged()V
    .locals 5

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 78
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 79
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v1, :cond_0

    .line 80
    new-instance v2, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    invoke-direct {v2, v1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    .line 81
    .local v2, "syncState":Lcom/google/android/apps/books/sync/SyncAccountsState;
    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/sync/SyncAccountsState;->setHaveNagged(Ljava/lang/String;Z)V

    .line 83
    .end local v2    # "syncState":Lcom/google/android/apps/books/sync/SyncAccountsState;
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0f00eb

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 46
    const v1, 0x7f0f00ec

    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->mPositiveClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 47
    const v1, 0x7f0f00ed

    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->mNegativeClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 49
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
