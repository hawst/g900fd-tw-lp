.class Lcom/google/android/apps/books/net/NetworkBooksServer$1;
.super Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;
.source "NetworkBooksServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/net/NetworkBooksServer;->getPageImage(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/net/NetworkBooksServer;

.field final synthetic val$entity:Lorg/apache/http/HttpEntity;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/net/NetworkBooksServer;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .param p2, "x0"    # Lorg/apache/http/HttpEntity;

    .prologue
    .line 333
    .local p3, "x1":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iput-object p1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$1;->this$0:Lcom/google/android/apps/books/net/NetworkBooksServer;

    iput-object p4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$1;->val$entity:Lorg/apache/http/HttpEntity;

    iput-object p5, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$1;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;-><init>(Lorg/apache/http/HttpEntity;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/net/NetworkBooksServer$1;)V

    return-void
.end method


# virtual methods
.method public getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 336
    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$1;->val$entity:Lorg/apache/http/HttpEntity;

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "contentType":Ljava/lang/String;
    const-string v1, "image/gif"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$1;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    if-eqz v1, :cond_0

    .line 341
    const-string v1, "BooksServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requested encrypted content, but response was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const/4 v1, 0x0

    .line 344
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$HttpEncryptedContentResponse;->getSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v1

    goto :goto_0
.end method
