.class Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1$1;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1$1;->this$2:Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1$1;->this$2:Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$700(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/model/BooksDataListener;->onUploads(Lcom/google/android/apps/books/upload/Upload$Uploads;)V

    .line 416
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 411
    check-cast p1, Lcom/google/android/apps/books/model/BooksDataListener;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1$1;->take(Lcom/google/android/apps/books/model/BooksDataListener;)V

    return-void
.end method
