.class Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$3;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->refresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$3;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 494
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$3;->take(Ljava/util/List;)V

    return-void
.end method

.method public take(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 497
    .local p1, "dictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$3;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;

    # setter for: Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mServerMetadataList:Ljava/util/List;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->access$602(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;Ljava/util/List;)Ljava/util/List;

    .line 498
    return-void
.end method
