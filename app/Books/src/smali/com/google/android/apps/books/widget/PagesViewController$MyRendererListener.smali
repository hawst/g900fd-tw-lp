.class Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/RendererListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyRendererListener"
.end annotation


# instance fields
.field private final mTempHandleRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

.field private final mTempTextRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 1

    .prologue
    .line 3347
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3349
    new-instance v0, Lcom/google/android/apps/books/widget/JsonRectWalker;

    invoke-direct {v0}, Lcom/google/android/apps/books/widget/JsonRectWalker;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->mTempHandleRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

    .line 3350
    new-instance v0, Lcom/google/android/apps/books/widget/JsonRectWalker;

    invoke-direct {v0}, Lcom/google/android/apps/books/widget/JsonRectWalker;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->mTempTextRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 3347
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    return-void
.end method


# virtual methods
.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 3434
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementHighlightRequestId:I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4200(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 3439
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementPageOffset:I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4300(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3440
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementPageOffset:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4300(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PageTurnController;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->turnToPage(IILcom/google/android/apps/books/widget/PageTurnController;)V
    invoke-static {v0, p2, v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4500(Lcom/google/android/apps/books/widget/PagesViewController;IILcom/google/android/apps/books/widget/PageTurnController;)V

    .line 3442
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4600(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 3444
    :cond_1
    return-void
.end method

.method public onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "textPointsJson"    # Ljava/lang/String;
    .param p4, "handlesPointsJson"    # Ljava/lang/String;
    .param p5, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 3391
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeHighlightTtsRectangles(ILjava/lang/String;)Z
    invoke-static {v1, p1, p3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4100(Lcom/google/android/apps/books/widget/PagesViewController;ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3406
    :cond_0
    :goto_0
    return-void

    .line 3395
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->mTempHandleRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

    invoke-virtual {v1, p4}, Lcom/google/android/apps/books/widget/JsonRectWalker;->setJson(Ljava/lang/String;)V

    .line 3396
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->mTempTextRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/books/widget/JsonRectWalker;->setJson(Ljava/lang/String;)V

    .line 3397
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->mTempHandleRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->mTempTextRects:Lcom/google/android/apps/books/widget/JsonRectWalker;

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V

    .line 3399
    if-eqz p5, :cond_0

    .line 3401
    new-instance v0, Lcom/google/android/apps/books/app/TextModeSelectionState;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getTextLocationComparator()Ljava/util/Comparator;

    move-result-object v5

    move v1, p2

    move-object v2, p5

    move-object v3, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/TextModeSelectionState;-><init>(ILcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;Lcom/google/android/apps/books/render/Renderer;Ljava/util/Comparator;)V

    .line 3404
    .local v0, "state":Lcom/google/android/apps/books/app/TextModeSelectionState;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V

    goto :goto_0
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 16
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I

    .prologue
    .line 3361
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    move/from16 v0, p1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getAnnotationsForPassage(I)Ljava/util/Iterator;

    move-result-object v6

    .line 3363
    .local v6, "annotations":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getLocale()Ljava/util/Locale;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getTextLocationComparator()Ljava/util/Comparator;

    move-result-object v7

    move-object/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p4

    move/from16 v4, p5

    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/books/util/PagesViewUtils;->findTextRange(Ljava/lang/String;ILjava/lang/String;ILjava/util/Locale;Ljava/util/Iterator;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v9

    .line 3368
    .local v9, "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->onNewSelectionBegins()V

    .line 3370
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v7

    const/4 v10, 0x1

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, -0x1

    const/4 v15, 0x1

    move/from16 v8, p1

    invoke-interface/range {v7 .. v15}, Lcom/google/android/apps/books/render/Renderer;->loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I

    .line 3374
    return-void
.end method

.method public onNewSelectionBegins()V
    .locals 1

    .prologue
    .line 3379
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1000(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 3380
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onStartedSelection()V

    .line 3381
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->invalidatePageContent()V

    .line 3382
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->setupSelectionOverlay()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1300(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 3383
    return-void
.end method

.method public onPagesNeedRedraw()V
    .locals 8

    .prologue
    .line 3466
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->showingNavigationView()Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4900(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3489
    :goto_0
    return-void

    .line 3470
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5000(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v4

    const-string v5, "re-publishing pages"

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v3

    .line 3472
    .local v3, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getRightMostVisiblePageOffsetFromBasePage()I
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5100(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v0

    .line 3473
    .local v0, "lastNearbyOffset":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->getLeftMostVisiblePageOffsetFromBasePage()I
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5200(Lcom/google/android/apps/books/widget/PagesViewController;)I

    move-result v1

    .line 3474
    .local v1, "offsetFromBasePage":I
    :goto_1
    if-gt v1, v0, :cond_2

    .line 3475
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$5300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/support/v4/util/SparseArrayCompat;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;

    .line 3480
    .local v2, "result":Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v5, v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->isCurrentRequest(Lcom/google/android/apps/books/util/RenderRequestContext;)Z
    invoke-static {v4, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2200(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v4}, Lcom/google/android/apps/books/render/PagePainter;->isUpdatable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3482
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v5, v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPageInfo:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v6, v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    iget-object v7, v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForFullView(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V
    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1900(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V

    .line 3485
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "page "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3474
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3488
    .end local v2    # "result":Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;
    :cond_2
    invoke-interface {v3}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto :goto_0
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3449
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onPassageMoListReady(IILjava/util/Map;)V

    .line 3450
    return-void
.end method

.method public onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 3428
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->setTtsPassageText(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 3430
    return-void
.end method

.method public onPassagesPurged(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3461
    .local p1, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->invalidatePassages(Ljava/util/Collection;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$4800(Lcom/google/android/apps/books/widget/PagesViewController;Ljava/util/Collection;)V

    .line 3462
    return-void
.end method

.method public onRenderError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Exception;

    .prologue
    .line 3354
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    .line 3355
    return-void
.end method

.method public onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V
    .locals 1
    .param p3, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 3411
    .local p1, "handleRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    .local p2, "textRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3414
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setHandlesRects(Lcom/google/android/apps/books/widget/Walker;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3415
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setTextRects(Lcom/google/android/apps/books/widget/Walker;I)V

    .line 3418
    :cond_0
    return-void
.end method

.method public onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
    .locals 2
    .param p1, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 3422
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/app/SelectionState;->getPassageIndex()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onSelectionChanged(ILcom/google/android/apps/books/app/SelectionState;)V

    .line 3423
    return-void
.end method
