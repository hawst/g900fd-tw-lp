.class Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReaderGestureListener"
.end annotation


# instance fields
.field private mHorizontallyLocked:Z

.field private mIgnoringScroll:Z

.field private final mMaxScale:F

.field private final mMinScale:F

.field private final mMinimumInitialFlingVelocity:F

.field private mScaling:Z

.field private final mScroller:Landroid/widget/OverScroller;

.field private final mScrollerHandler:Landroid/os/Handler;

.field private mTurning:Z

.field private mTurningDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field private mZoomFocusX:F

.field private mZoomFocusY:F

.field private mZoomingIn:Z

.field private mZoomingOut:Z

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 6641
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 6851
    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    .line 6853
    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mIgnoringScroll:Z

    .line 7042
    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScaling:Z

    .line 7044
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mMinScale:F

    .line 7045
    const/high16 v2, 0x42700000    # 60.0f

    iput v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mMaxScale:F

    .line 6642
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 6643
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 6645
    .local v0, "config":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mMinimumInitialFlingVelocity:F

    .line 6647
    new-instance v2, Landroid/widget/OverScroller;

    invoke-direct {v2, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    .line 6648
    new-instance v2, Landroid/os/Handler;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->createScrollCallback(Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)Landroid/os/Handler$Callback;
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13900(Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)Landroid/os/Handler$Callback;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScrollerHandler:Landroid/os/Handler;

    .line 6649
    return-void
.end method

.method private declared-synchronized animateScale(F)V
    .locals 4
    .param p1, "to"    # F

    .prologue
    .line 7176
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v0

    .line 7177
    .local v0, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    if-eqz v0, :cond_0

    .line 7178
    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusX:F

    iget v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusY:F

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v3, v3, Lcom/google/android/apps/books/app/ReaderFragment;->mScaleAnimationCallback:Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->animateTo(FFFLcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7180
    :cond_0
    monitor-exit p0

    return-void

    .line 7176
    .end local v0    # "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private applyScrollerUpdate()Z
    .locals 4

    .prologue
    .line 6985
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    .line 6987
    .local v0, "continuing":Z
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v1

    .line 6988
    .local v1, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    if-eqz v1, :cond_0

    .line 6989
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setScroll(FF)V

    .line 6990
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onZoomChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14900(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V

    .line 6993
    :cond_0
    return v0
.end method

.method private findTappedTouchableItem(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/render/TouchableItem;
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "type"    # I

    .prologue
    .line 6743
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6744
    const/4 v0, 0x0

    .line 6747
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->findTappedTouchableItem(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/render/TouchableItem;

    move-result-object v0

    goto :goto_0
.end method

.method private getScale()F
    .locals 2

    .prologue
    .line 7148
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v0

    .line 7149
    .local v0, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    if-eqz v0, :cond_0

    .line 7150
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScale()F

    move-result v1

    .line 7152
    :goto_0
    return v1

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method private handleTouchableTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 6751
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 6770
    :cond_0
    :goto_0
    return v1

    .line 6755
    :cond_1
    const/4 v3, -0x1

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->findTappedTouchableItem(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/render/TouchableItem;

    move-result-object v0

    .line 6756
    .local v0, "item":Lcom/google/android/apps/books/render/TouchableItem;
    if-eqz v0, :cond_0

    .line 6760
    iget v3, v0, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    if-ne v3, v2, :cond_2

    .line 6761
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->handleVideoItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;)V

    move v1, v2

    .line 6762
    goto :goto_0

    .line 6763
    :cond_2
    iget v3, v0, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 6764
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->handleLinkItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;)V

    move v1, v2

    .line 6765
    goto :goto_0

    .line 6766
    :cond_3
    iget v3, v0, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 6767
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->handleAudioItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14600(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;)V

    move v1, v2

    .line 6768
    goto :goto_0
.end method

.method private isOverScaled(F)Z
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 7109
    const/high16 v0, 0x40c00000    # 6.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isZoomed()Z
    .locals 2

    .prologue
    .line 6924
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->getScale()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3c23d70a    # 0.01f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scheduleScrollerUpdate()V
    .locals 4

    .prologue
    .line 6997
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScrollerHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 6998
    return-void
.end method

.method private screenDirectionFromHorizontalSwipeMotion(F)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 2
    .param p1, "motion"    # F

    .prologue
    const/4 v1, 0x0

    .line 6975
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    .line 6976
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 6980
    :goto_0
    return-object v0

    .line 6977
    :cond_0
    cmpl-float v0, p1, v1

    if-lez v0, :cond_1

    .line 6978
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_0

    .line 6980
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isZoomedIn()Z
    .locals 2

    .prologue
    .line 6917
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->getScale()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActionCancel(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 6943
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScaling:Z

    .line 6944
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mIgnoringScroll:Z

    .line 6946
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isZoomedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6953
    :cond_0
    :goto_0
    return-void

    .line 6949
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    if-eqz v0, :cond_0

    .line 6950
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    .line 6951
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->endTurn(Z)Z

    goto :goto_0
.end method

.method public onActionUp(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 6928
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScaling:Z

    .line 6929
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mIgnoringScroll:Z

    .line 6933
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isZoomedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6940
    :cond_0
    :goto_0
    return-void

    .line 6936
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    if-eqz v0, :cond_0

    .line 6937
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    .line 6938
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->endTurn(Z)Z

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 6777
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isEdgeTouch(Landroid/view/MotionEvent;)Z
    invoke-static {v7, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14000(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/view/MotionEvent;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 6778
    const-string v7, "ReaderFragment"

    const/4 v8, 0x2

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 6779
    const-string v7, "ReaderFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Dropping double-tap because it\'s on the horizontal edge: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 6820
    :cond_0
    :goto_0
    return v6

    .line 6784
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v7}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v3

    .line 6786
    .local v3, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mZoomEnabled:Z
    invoke-static {v7}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v7}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz v3, :cond_0

    .line 6790
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusX:F

    .line 6791
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusY:F

    .line 6794
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isZoomed()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 6796
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->animateScale(F)V

    .line 6820
    :goto_1
    const/4 v6, 0x1

    goto :goto_0

    .line 6799
    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getWidth()I

    move-result v5

    .line 6800
    .local v5, "viewWidth":I
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->displayTwoPages()Z
    invoke-static {v6}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 6801
    div-int/lit8 v0, v5, 0x2

    .line 6804
    .local v0, "centerX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    int-to-float v7, v0

    cmpg-float v6, v6, v7

    if-gez v6, :cond_3

    .line 6805
    int-to-float v6, v0

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentLeft()F

    move-result v7

    sub-float v4, v6, v7

    .line 6806
    .local v4, "pageWidth":F
    int-to-float v6, v5

    div-float v1, v6, v4

    .line 6807
    .local v1, "endScale":F
    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentLeft()F

    move-result v6

    mul-float v2, v1, v6

    .line 6813
    .local v2, "endScrollX":F
    :goto_2
    sub-float v6, v1, v8

    div-float v6, v2, v6

    iput v6, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusX:F

    .line 6818
    .end local v0    # "centerX":I
    .end local v2    # "endScrollX":F
    .end local v4    # "pageWidth":F
    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->animateScale(F)V

    goto :goto_1

    .line 6809
    .end local v1    # "endScale":F
    .restart local v0    # "centerX":I
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getContentRight()F

    move-result v6

    int-to-float v7, v0

    sub-float v4, v6, v7

    .line 6810
    .restart local v4    # "pageWidth":F
    int-to-float v6, v5

    div-float v1, v6, v4

    .line 6811
    .restart local v1    # "endScale":F
    int-to-float v6, v0

    mul-float v2, v1, v6

    .restart local v2    # "endScrollX":F
    goto :goto_2

    .line 6816
    .end local v0    # "centerX":I
    .end local v1    # "endScale":F
    .end local v2    # "endScrollX":F
    .end local v4    # "pageWidth":F
    :cond_4
    const/high16 v1, 0x40000000    # 2.0f

    .restart local v1    # "endScale":F
    goto :goto_3
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 6957
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6958
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 6959
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->applyScrollerUpdate()Z

    .line 6961
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mHorizontallyLocked:Z

    .line 6962
    return v1
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 17
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 7004
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isZoomedIn()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 7005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v12

    .line 7006
    .local v12, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    if-eqz v12, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->getScale()F

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isOverScaled(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 7008
    :cond_0
    const/4 v2, 0x1

    .line 7039
    .end local v12    # "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    :goto_0
    return v2

    .line 7012
    .restart local v12    # "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    :cond_1
    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getMinScrollX()F

    move-result v15

    .line 7013
    .local v15, "minX":F
    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getMaxScrollX()F

    move-result v13

    .line 7014
    .local v13, "maxX":F
    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getMinScrollY()F

    move-result v16

    .line 7015
    .local v16, "minY":F
    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getMaxScrollY()F

    move-result v14

    .line 7016
    .local v14, "maxY":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mHorizontallyLocked:Z

    if-eqz v2, :cond_2

    .line 7017
    const/16 p3, 0x0

    .line 7019
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScrollX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScrollY()F

    move-result v4

    float-to-int v4, v4

    move/from16 v0, p3

    float-to-int v5, v0

    neg-int v5, v5

    move/from16 v0, p4

    float-to-int v6, v0

    neg-int v6, v6

    float-to-int v7, v15

    float-to-int v8, v13

    move/from16 v0, v16

    float-to-int v9, v0

    float-to-int v10, v14

    invoke-virtual/range {v2 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 7022
    const-string v2, "ReaderFragment"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 7023
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Flung from ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScrollX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScrollY()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", velocity("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    neg-float v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    neg-float v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), min("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "), max("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 7028
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->scheduleScrollerUpdate()V

    .line 7039
    .end local v12    # "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    .end local v13    # "maxX":F
    .end local v14    # "maxY":F
    .end local v15    # "minX":F
    .end local v16    # "minY":F
    :cond_4
    :goto_1
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 7029
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    if-eqz v2, :cond_4

    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mMinimumInitialFlingVelocity:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 7032
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->screenDirectionFromHorizontalSwipeMotion(F)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v11

    .line 7034
    .local v11, "direction":Lcom/google/android/apps/books/util/ScreenDirection;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11000(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 7035
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v2

    const/4 v3, 0x1

    move/from16 v0, p3

    invoke-virtual {v2, v11, v3, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->enqueueEndTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZF)V

    .line 7036
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6825
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6843
    :cond_0
    :goto_0
    return-void

    .line 6829
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScaling:Z

    if-nez v0, :cond_0

    .line 6840
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6841
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 9
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    .line 7056
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v2

    .line 7057
    .local v2, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    if-eqz v2, :cond_1

    .line 7058
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    .line 7059
    .local v1, "detectorScale":F
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScale()F

    move-result v4

    mul-float v3, v1, v4

    .line 7061
    .local v3, "newScale":F
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingIn:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingOut:Z

    if-nez v4, :cond_0

    sub-float v4, v3, v7

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v5, 0x3c23d70a    # 0.01f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 7062
    cmpg-float v4, v3, v7

    if-gez v4, :cond_2

    .line 7063
    iput-boolean v8, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingOut:Z

    .line 7066
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 7067
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 7068
    const/4 v0, 0x0

    .line 7069
    .local v0, "animate":Z
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/books/widget/PagesViewController;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V

    .line 7070
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->scaleSkimToFull()V

    .line 7077
    .end local v0    # "animate":Z
    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingIn:Z

    if-eqz v4, :cond_3

    .line 7078
    const/high16 v4, 0x42700000    # 60.0f

    invoke-static {v3, v7, v4}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v3

    .line 7080
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v4

    iput v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusX:F

    .line 7081
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    iput v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusY:F

    .line 7083
    iget v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusX:F

    iget v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusY:F

    invoke-virtual {v2, v3, v4, v5, v8}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setScale(FFFZ)V

    .line 7084
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onZoomChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V
    invoke-static {v4, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14900(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V

    .line 7093
    .end local v1    # "detectorScale":F
    .end local v3    # "newScale":F
    :cond_1
    :goto_1
    return v8

    .line 7072
    .restart local v1    # "detectorScale":F
    .restart local v3    # "newScale":F
    :cond_2
    cmpl-float v4, v3, v7

    if-lez v4, :cond_0

    .line 7073
    iput-boolean v8, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingIn:Z

    goto :goto_0

    .line 7085
    :cond_3
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingOut:Z

    if-eqz v4, :cond_1

    .line 7087
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 7088
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->pinchToSkim(F)V

    goto :goto_1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v0, 0x1

    .line 7099
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mZoomMaxSpan:I
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15000(Lcom/google/android/apps/books/app/ReaderFragment;)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7100
    :cond_0
    const/4 v0, 0x0

    .line 7105
    :goto_0
    return v0

    .line 7103
    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScaling:Z

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 7114
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingIn:Z

    if-eqz v2, :cond_5

    .line 7119
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    .line 7121
    .local v0, "currentSpan":F
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->getScale()F

    move-result v1

    .line 7123
    .local v1, "scale":F
    cmpg-float v2, v1, v5

    if-gez v2, :cond_1

    .line 7124
    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->setScale(F)V

    .line 7144
    .end local v0    # "currentSpan":F
    .end local v1    # "scale":F
    :cond_0
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingOut:Z

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingIn:Z

    .line 7145
    return-void

    .line 7125
    .restart local v0    # "currentSpan":F
    .restart local v1    # "scale":F
    :cond_1
    const v2, 0x3f99999a    # 1.2f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    .line 7127
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->animateScale(F)V

    goto :goto_0

    .line 7128
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isOverScaled(F)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 7130
    const/high16 v2, 0x40c00000    # 6.0f

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->animateScale(F)V

    goto :goto_0

    .line 7131
    :cond_3
    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 7132
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NaN mScale seen in onScaleEnd(); current span "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 7134
    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->setScale(F)V

    goto :goto_0

    .line 7136
    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->setScale(F)V

    goto :goto_0

    .line 7138
    .end local v0    # "currentSpan":F
    .end local v1    # "scale":F
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomingOut:Z

    if-eqz v2, :cond_0

    .line 7139
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 7140
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v2

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->pinchToSkim(F)V

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 14
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 6860
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v9, 0x0

    .line 6913
    :goto_0
    return v9

    .line 6862
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isZoomedIn()Z

    move-result v8

    .line 6863
    .local v8, "zoomedIn":Z
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v5

    .line 6866
    .local v5, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mScaling:Z

    if-eqz v9, :cond_2

    if-eqz v8, :cond_3

    :cond_2
    if-nez v5, :cond_4

    :cond_3
    const/4 v9, 0x1

    goto :goto_0

    .line 6868
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    sub-float v4, v9, v10

    .line 6869
    .local v4, "dx":F
    if-eqz v8, :cond_9

    .line 6870
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mHorizontallyLocked:Z

    if-eqz v9, :cond_5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVerticalRut:I
    invoke-static {v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14800(Lcom/google/android/apps/books/app/ReaderFragment;)I

    move-result v10

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    .line 6872
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mHorizontallyLocked:Z

    .line 6875
    :cond_5
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mHorizontallyLocked:Z

    if-eqz v9, :cond_6

    .line 6876
    const/16 p3, 0x0

    .line 6879
    :cond_6
    const/4 v9, 0x1

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v5, v0, v1, v9}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->updateScroll(FFZ)V

    .line 6881
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onZoomChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V
    invoke-static {v9, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14900(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V

    .line 6888
    :cond_7
    if-nez v8, :cond_e

    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mIgnoringScroll:Z

    if-nez v9, :cond_e

    .line 6889
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    if-nez v9, :cond_d

    .line 6892
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getTop()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v10

    int-to-float v7, v9

    .line 6893
    .local v7, "topBound":F
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getBottom()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v10

    int-to-float v2, v9

    .line 6894
    .local v2, "botBound":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    cmpg-float v9, v9, v7

    if-ltz v9, :cond_8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    cmpl-float v9, v9, v2

    if-lez v9, :cond_c

    .line 6895
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    sub-float v3, v9, v10

    .line 6896
    .local v3, "deltaX":F
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x3f800000    # 1.0f

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_b

    const/4 v9, 0x0

    goto/16 :goto_0

    .line 6883
    .end local v2    # "botBound":F
    .end local v3    # "deltaX":F
    .end local v7    # "topBound":F
    :cond_9
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    const-wide/16 v12, 0x0

    cmpl-double v9, v10, v12

    if-eqz v9, :cond_a

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_7

    .line 6886
    :cond_a
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 6897
    .restart local v2    # "botBound":F
    .restart local v3    # "deltaX":F
    .restart local v7    # "topBound":F
    :cond_b
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    sub-float/2addr v9, v10

    div-float v6, v9, v3

    .line 6899
    .local v6, "tanT":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v10, 0x3f333333    # 0.7f

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_c

    .line 6900
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mIgnoringScroll:Z

    .line 6901
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 6905
    .end local v3    # "deltaX":F
    .end local v6    # "tanT":F
    :cond_c
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->screenDirectionFromHorizontalSwipeMotion(F)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurningDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 6906
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurningDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    const/4 v11, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 6907
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mTurning:Z

    .line 6910
    .end local v2    # "botBound":F
    .end local v7    # "topBound":F
    :cond_d
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->enqueueUpdateTurn(F)V

    .line 6913
    :cond_e
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method public onScrollMessage()V
    .locals 1

    .prologue
    .line 6653
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    if-nez v0, :cond_1

    .line 6660
    :cond_0
    :goto_0
    return-void

    .line 6657
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->applyScrollerUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6658
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->scheduleScrollerUpdate()V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 6664
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 6738
    :cond_0
    :goto_0
    return v3

    .line 6670
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isEdgeTouch(Landroid/view/MotionEvent;)Z
    invoke-static {v5, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14000(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 6671
    const-string v4, "ReaderFragment"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 6672
    const-string v4, "ReaderFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dropping single tap because it\'s on the horizontal edge: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6677
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 6678
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    move v3, v4

    .line 6679
    goto :goto_0

    .line 6682
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_6

    move v1, v4

    .line 6683
    .local v1, "fragmentAttached":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 6685
    const/4 v0, 0x0

    .line 6686
    .local v0, "eventualState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 6687
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getEventualState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v0

    .line 6694
    :cond_4
    sget-object v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v0, v3, :cond_7

    .line 6695
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Z

    move v3, v4

    .line 6696
    goto :goto_0

    .end local v0    # "eventualState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .end local v1    # "fragmentAttached":Z
    :cond_6
    move v1, v3

    .line 6682
    goto :goto_1

    .line 6700
    .restart local v0    # "eventualState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .restart local v1    # "fragmentAttached":Z
    :cond_7
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->handleTouchableTap(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    .line 6701
    goto :goto_0

    .line 6705
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v4

    .line 6707
    goto/16 :goto_0

    .line 6710
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    move-result v3

    if-eqz v3, :cond_a

    move v3, v4

    .line 6711
    goto/16 :goto_0

    .line 6719
    :cond_a
    sget-object v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v0, v3, :cond_b

    .line 6720
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/InfoCardsHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    move v3, v4

    .line 6721
    goto/16 :goto_0

    .line 6724
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/widget/PagesView;->getWidth()I

    move-result v5

    invoke-static {v3, v5}, Lcom/google/android/apps/books/util/PagesViewUtils;->getZone(FI)Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    move-result-object v2

    .line 6726
    .local v2, "zone":Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    sget-object v3, Lcom/google/android/apps/books/app/ReaderFragment$42;->$SwitchMap$com$google$android$apps$books$widget$PagesView$TouchZone:[I

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    :goto_2
    move v3, v4

    .line 6738
    goto/16 :goto_0

    .line 6728
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v5, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    invoke-static {v3, v5, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    goto :goto_2

    .line 6731
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v5, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    invoke-static {v3, v5, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    goto :goto_2

    .line 6735
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cycleUiMode()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_2

    .line 6726
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setScale(F)V
    .locals 4
    .param p1, "scale"    # F

    .prologue
    .line 7156
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7157
    const-string v1, "ReaderFragment"

    const-string v2, "NaN scale seen in setScale; resetting it"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 7158
    const/high16 p1, 0x3f800000    # 1.0f

    .line 7160
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v0

    .line 7161
    .local v0, "helper":Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    if-eqz v0, :cond_1

    .line 7162
    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusX:F

    iget v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->mZoomFocusY:F

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setScale(FFFZ)V

    .line 7163
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onZoomChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14900(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V

    .line 7165
    :cond_1
    return-void
.end method

.method public setScaleToMin()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 7169
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->getScale()F

    move-result v0

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3c23d70a    # 0.01f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 7173
    :goto_0
    return-void

    .line 7172
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->setScale(F)V

    goto :goto_0
.end method
