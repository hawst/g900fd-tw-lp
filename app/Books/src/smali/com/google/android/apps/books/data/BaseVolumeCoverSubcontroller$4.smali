.class Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;
.super Lcom/google/android/apps/books/data/BasePendingAction;
.source "BaseVolumeCoverSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->deliverSuccess(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/ControlTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

.field final synthetic val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;Lcom/google/android/apps/books/data/DataControllerBlob;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/books/data/BasePendingAction;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 148
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v1}, Lcom/google/android/apps/books/data/DataControllerBlob;->save()V

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->access$600(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->val$volumeId:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->access$600(Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller$4;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
