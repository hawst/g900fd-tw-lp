.class public Lcom/google/android/apps/books/tts/SentenceSegmentation;
.super Lcom/google/android/apps/books/tts/SubstringSegmentation;
.source "SentenceSegmentation.java"


# static fields
.field private static final NON_BREAKS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Mr."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Mrs."

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Ms."

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/tts/SentenceSegmentation;->NON_BREAKS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionMap"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/tts/SubstringSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;Ljava/text/BreakIterator;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionMap"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "sentenceIterator"    # Ljava/text/BreakIterator;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/tts/SubstringSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;Ljava/text/BreakIterator;)V

    .line 25
    return-void
.end method

.method private static endsWithSpecialPhrase(Ljava/lang/String;)Z
    .locals 6
    .param p0, "sentence"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "trimmed":Ljava/lang/String;
    sget-object v0, Lcom/google/android/apps/books/tts/SentenceSegmentation;->NON_BREAKS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 88
    .local v3, "specialPhrase":Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    const/4 v5, 0x1

    .line 92
    .end local v3    # "specialPhrase":Ljava/lang/String;
    :goto_1
    return v5

    .line 87
    .restart local v3    # "specialPhrase":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v3    # "specialPhrase":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected buildSegmentation(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/Range;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v7, p0, Lcom/google/android/apps/books/tts/SentenceSegmentation;->mThreadLocalBreakIterator:Ljava/text/BreakIterator;

    if-eqz v7, :cond_4

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SentenceSegmentation;->mThreadLocalBreakIterator:Ljava/text/BreakIterator;

    .line 43
    .local v0, "boundary":Ljava/text/BreakIterator;
    :goto_0
    invoke-virtual {v0, p1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 45
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v6, "ranges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/util/Range;>;"
    const/4 v3, 0x0

    .line 49
    .local v3, "endOfLastSentence":I
    :cond_0
    invoke-virtual {v0}, Ljava/text/BreakIterator;->current()I

    move-result v1

    .line 50
    .local v1, "current":I
    if-lez v1, :cond_2

    .line 51
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "currentSentence":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/tts/SentenceSegmentation;->endsWithSpecialPhrase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 55
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/tts/SentenceSegmentation;->shouldIncludeItem(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 56
    new-instance v7, Lcom/google/android/apps/books/util/Range;

    invoke-direct {v7, v3, v1}, Lcom/google/android/apps/books/util/Range;-><init>(II)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_1
    move v3, v1

    .line 61
    .end local v2    # "currentSentence":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/text/BreakIterator;->next()I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_0

    .line 65
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    .line 66
    .local v5, "length":I
    add-int/lit8 v7, v5, -0x1

    if-ge v3, v7, :cond_3

    .line 67
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 68
    .local v4, "lastSentenceText":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/tts/SentenceSegmentation;->shouldIncludeItem(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 69
    new-instance v7, Lcom/google/android/apps/books/util/Range;

    invoke-direct {v7, v3, v5}, Lcom/google/android/apps/books/util/Range;-><init>(II)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    .end local v4    # "lastSentenceText":Ljava/lang/String;
    :cond_3
    return-object v6

    .line 39
    .end local v0    # "boundary":Ljava/text/BreakIterator;
    .end local v1    # "current":I
    .end local v3    # "endOfLastSentence":I
    .end local v5    # "length":I
    .end local v6    # "ranges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/util/Range;>;"
    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/books/tts/SentenceSegmentation;->mLocale:Ljava/util/Locale;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/books/tts/SentenceSegmentation;->mLocale:Ljava/util/Locale;

    invoke-static {v7}, Ljava/text/BreakIterator;->getSentenceInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    .restart local v0    # "boundary":Ljava/text/BreakIterator;
    :goto_1
    goto :goto_0

    .end local v0    # "boundary":Ljava/text/BreakIterator;
    :cond_5
    invoke-static {}, Ljava/text/BreakIterator;->getSentenceInstance()Ljava/text/BreakIterator;

    move-result-object v0

    goto :goto_1
.end method

.method public bridge synthetic getItemCount()I
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/google/android/apps/books/tts/SubstringSegmentation;->getItemCount()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItemRange(I)Lcom/google/android/apps/books/util/Range;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/google/android/apps/books/tts/SubstringSegmentation;->getItemRange(I)Lcom/google/android/apps/books/util/Range;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItemText(I)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/google/android/apps/books/tts/SubstringSegmentation;->getItemText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
