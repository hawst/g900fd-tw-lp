.class public interface abstract Lcom/google/android/apps/books/model/VolumeMetadata;
.super Ljava/lang/Object;
.source "VolumeMetadata.java"

# interfaces
.implements Lcom/google/android/apps/books/common/Position$PageOrdering;
.implements Lcom/google/android/apps/books/model/PositionTitles;
.implements Lcom/google/android/apps/books/model/SegmentSource;
.implements Lcom/google/android/apps/books/util/PassageMap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;,
        Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
    }
.end annotation


# virtual methods
.method public abstract getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;
.end method

.method public abstract getAccount()Landroid/accounts/Account;
.end method

.method public abstract getBakedCssFilesJsonArray()Lorg/json/JSONArray;
.end method

.method public abstract getBookPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
.end method

.method public abstract getBuyUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
.end method

.method public abstract getBuyUrl()Ljava/lang/String;
.end method

.method public abstract getCanonicalUrl()Ljava/lang/String;
.end method

.method public abstract getChapterCssJsonArray()Lorg/json/JSONArray;
.end method

.method public abstract getChapterIndexForPageId(Ljava/lang/String;)I
.end method

.method public abstract getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
.end method

.method public abstract getChapterSegmentJsonArray()Lorg/json/JSONArray;
.end method

.method public abstract getChapterStartPosition(I)Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getChapterTitleForChapterIndex(I)Ljava/lang/String;
.end method

.method public abstract getChapters()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContentVersion()Ljava/lang/String;
.end method

.method public abstract getCssResourceList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDefaultPosition()Lcom/google/android/apps/books/common/Position;
.end method

.method public abstract getEndOfBookBody(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/common/Position;
.end method

.method public abstract getFirstForbiddenPassageIndex()I
.end method

.method public abstract getFirstSegmentForPassageIndex(I)Lcom/google/android/apps/books/model/Segment;
.end method

.method public abstract getFixedLayoutJsonArray()Lorg/json/JSONArray;
.end method

.method public abstract getLineHeight()F
.end method

.method public abstract getLocale()Ljava/util/Locale;
.end method

.method public abstract getManifest()Lcom/google/android/apps/books/model/VolumeManifest;
.end method

.method public abstract getMediaClips()Lcom/google/android/apps/books/util/MediaClips;
.end method

.method public abstract getMediaOverlayActiveClass()Ljava/lang/String;
.end method

.method public abstract getModeForContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
.end method

.method public abstract getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getPage(Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getPageCount()I
.end method

.method public abstract getPages()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPassageCount()I
.end method

.method public abstract getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
.end method

.method public abstract getPassageIndexForSegmentId(Ljava/lang/String;)I
.end method

.method public abstract getPassageIndexForSegmentIndex(I)I
.end method

.method public abstract getPassageSegmentIndices(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPassageSegments(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPosition()Lcom/google/android/apps/books/common/Position;
.end method

.method public abstract getPositionComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPositionRangeForPassage(I)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getResourceIdToResource()Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSegmentForPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Segment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getSegmentIdToCssIndices()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getSegments()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSharedFontsUrisPlus()Lorg/json/JSONArray;
.end method

.method public abstract getStartPageIndex(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getStartPageTitle(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getTextLocationComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTextZoom()F
.end method

.method public abstract getUserPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
.end method

.method public abstract getVolumeAnnotationRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getVolumeData()Lcom/google/android/apps/books/model/VolumeData;
.end method

.method public abstract getVolumeId()Ljava/lang/String;
.end method

.method public abstract getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;
.end method

.method public abstract hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
.end method

.method public abstract hasMediaClips()Z
.end method

.method public abstract hasMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Z
.end method

.method public abstract hasNeverBeenOpened()Z
.end method

.method public abstract hasOfflineLicense()Z
.end method

.method public abstract hasReadableSections()Z
.end method

.method public abstract isAppleFixedLayout()Z
.end method

.method public abstract isChapterViewable(ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
.end method

.method public abstract isFitWidth()Z
.end method

.method public abstract isInMyEBooksCollection()Z
.end method

.method public abstract isPageEnabled(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract isPassageForbidden(I)Z
.end method

.method public abstract isPositionEnabled(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract isRightToLeft()Z
.end method

.method public abstract isSample()Z
.end method

.method public abstract isUploaded()Z
.end method

.method public abstract isVertical()Z
.end method

.method public abstract shouldSubstringSearch()Z
.end method

.method public abstract useMinimalFontSet()Z
.end method
