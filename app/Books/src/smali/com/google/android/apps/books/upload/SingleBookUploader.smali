.class public interface abstract Lcom/google/android/apps/books/upload/SingleBookUploader;
.super Ljava/lang/Object;
.source "SingleBookUploader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    }
.end annotation


# virtual methods
.method public abstract cancelUpload()V
.end method

.method public abstract pauseUpload()V
.end method

.method public abstract processingCompleted()V
.end method

.method public abstract resumeUpload()V
.end method

.method public abstract startUpload()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
