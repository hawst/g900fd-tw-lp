.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$offerId:Ljava/lang/String;

.field final synthetic val$onResult:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeIds:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->val$offerId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->val$volumeIds:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 811
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOffersSubcontroller:Lcom/google/android/apps/books/data/OffersSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/OffersSubcontroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->val$offerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->val$volumeIds:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/data/OffersSubcontroller;->acceptOffer(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    .line 812
    return-void
.end method
