.class Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
.super Ljava/lang/Object;
.source "SegmentPositionResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SegmentPositionResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SegmentCluster"
.end annotation


# instance fields
.field private mSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalBetweenCenters:I

.field final synthetic this$0:Lcom/google/android/apps/books/widget/SegmentPositionResolver;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/SegmentPositionResolver;Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;)V
    .locals 2
    .param p2, "segment"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .prologue
    const/4 v1, 0x0

    .line 137
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->this$0:Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    .line 135
    iput v1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iput v1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    .line 140
    return-void
.end method

.method private findCenter()I
    .locals 4

    .prologue
    .line 200
    const/4 v1, 0x0

    .line 201
    .local v1, "mean":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .line 202
    .local v2, "segment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    iget v3, v2, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    add-int/2addr v1, v3

    .line 203
    goto :goto_0

    .line 204
    .end local v2    # "segment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    div-int/2addr v1, v3

    .line 205
    return v1
.end method

.method private getLeft()I
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeft()I

    move-result v0

    return v0
.end method

.method private getRight()I
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getRight()I

    move-result v0

    return v0
.end method


# virtual methods
.method public isConflicting(Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;)Z
    .locals 2
    .param p1, "cluster"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->getLeft()I

    move-result v0

    invoke-direct {p1}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->getRight()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->getRight()I

    move-result v0

    invoke-direct {p1}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->getLeft()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public merge(Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;)V
    .locals 3
    .param p1, "cluster"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;

    .prologue
    .line 147
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    iget v1, p1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    .line 150
    iget v1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    iget-object v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getRightHalfWidth()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    .line 153
    iget v1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    iget-object v0, p1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeftHalfWidth()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 155
    return-void
.end method

.method public resolve()V
    .locals 7

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->findCenter()I

    move-result v2

    .line 184
    .local v2, "mean":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .line 185
    .local v0, "firstSegment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    iget v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    div-int/lit8 v4, v4, 0x2

    sub-int v3, v2, v4

    .line 187
    .local v3, "startCenter":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->this$0:Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    # getter for: Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mLeftCenter:I
    invoke-static {v4}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->access$000(Lcom/google/android/apps/books/widget/SegmentPositionResolver;)I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 188
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->this$0:Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    # getter for: Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mLeftCenter:I
    invoke-static {v4}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->access$000(Lcom/google/android/apps/books/widget/SegmentPositionResolver;)I

    move-result v3

    .line 193
    :cond_0
    :goto_0
    iput v3, v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->center:I

    .line 194
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 195
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mSegments:Ljava/util/List;

    add-int/lit8 v6, v1, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getRight()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->setLeft(I)V

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 189
    .end local v1    # "i":I
    :cond_1
    iget v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    add-int/2addr v4, v3

    iget-object v5, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->this$0:Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    # getter for: Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mRightCenter:I
    invoke-static {v5}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->access$100(Lcom/google/android/apps/books/widget/SegmentPositionResolver;)I

    move-result v5

    if-le v4, v5, :cond_0

    .line 190
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->this$0:Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    # getter for: Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mRightCenter:I
    invoke-static {v4}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->access$100(Lcom/google/android/apps/books/widget/SegmentPositionResolver;)I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->mTotalBetweenCenters:I

    sub-int v3, v4, v5

    goto :goto_0

    .line 197
    .restart local v1    # "i":I
    :cond_2
    return-void
.end method
