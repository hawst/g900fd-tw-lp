.class public Lcom/google/android/apps/books/widget/SwipeableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "SwipeableLinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;,
        Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;
    }
.end annotation


# instance fields
.field private mDismissCallback:Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;

.field private mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->initSwiper()V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->initSwiper()V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->initSwiper()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mDismissCallback:Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)Lcom/google/android/ublib/infocards/SwipeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    return-object v0
.end method

.method private hasTextSelection()Z
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 91
    .local v0, "focusView":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 92
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "focusView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/widget/TextView;->hasSelection()Z

    move-result v1

    .line 94
    :goto_0
    return v1

    .restart local v0    # "focusView":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initSwiper()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->setClipToPadding(Z)V

    .line 100
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->setClipChildren(Z)V

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 104
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    .line 105
    .local v3, "density":F
    invoke-static {v6}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v8

    .line 107
    .local v8, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v8}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v7

    .line 109
    .local v7, "pagingSlop":I
    new-instance v0, Lcom/google/android/ublib/infocards/SwipeHelper;

    new-instance v2, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;-><init>(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;Lcom/google/android/apps/books/widget/SwipeableLinearLayout$1;)V

    int-to-float v4, v7

    int-to-float v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/infocards/SwipeHelper;-><init>(ILcom/google/android/ublib/infocards/SwipeHelper$Callback;FFF)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/infocards/SwipeHelper;->setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V

    .line 113
    new-instance v0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$1;-><init>(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 119
    return-void
.end method


# virtual methods
.method public getSwipeHelper()Lcom/google/android/ublib/infocards/SwipeHelper;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->hasTextSelection()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mSwiper:Lcom/google/android/ublib/infocards/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/infocards/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDismissCallback(Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;)V
    .locals 0
    .param p1, "dismissCallback"    # Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mDismissCallback:Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;

    .line 131
    return-void
.end method
