.class Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfferImageProviderFactory"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/app/OffersFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/OffersFragment$1;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    return-void
.end method


# virtual methods
.method public createImageProvider()Lcom/google/android/ublib/cardlib/PlayCardImageProvider;
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lcom/google/android/apps/books/util/BookCoverImageProvider;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/BookCoverImageProvider;-><init>(Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;)V

    return-object v0
.end method

.method public getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 147
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 148
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->getAccount()Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/apps/books/app/OffersFragment;->access$400(Lcom/google/android/apps/books/app/OffersFragment;)Landroid/accounts/Account;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->getBookCoverImage(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/apps/books/app/OffersFragment;->access$500(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v1

    return-object v1
.end method
