.class public Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;
.super Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;
.source "ListToRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;
    }
.end annotation


# instance fields
.field private final mAdapter:Landroid/widget/Adapter;


# direct methods
.method public constructor <init>(Landroid/widget/Adapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->setHasStableIds(Z)V

    .line 48
    new-instance v0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$1;-><init>(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;)V

    invoke-interface {p1, v0}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;)Landroid/widget/Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;
    .locals 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 59
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040067

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 61
    .local v1, "wrapper":Landroid/view/ViewGroup;
    new-instance v2, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;-><init>(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;Landroid/view/ViewGroup;Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$1;)V

    return-object v2
.end method
