.class public Lcom/google/android/apps/books/render/MarginNote;
.super Ljava/lang/Object;
.source "MarginNote.java"


# instance fields
.field public annotationId:Ljava/lang/String;

.field public color:I

.field public icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public set(Ljava/lang/String;ILcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;)V
    .locals 0
    .param p1, "annotationId"    # Ljava/lang/String;
    .param p2, "color"    # I
    .param p3, "icon"    # Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/google/android/apps/books/render/MarginNote;->annotationId:Ljava/lang/String;

    .line 15
    iput p2, p0, Lcom/google/android/apps/books/render/MarginNote;->color:I

    .line 16
    iput-object p3, p0, Lcom/google/android/apps/books/render/MarginNote;->icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .line 17
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ColoredMarginNoteIcon [annotationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/MarginNote;->annotationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/MarginNote;->color:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/MarginNote;->icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
