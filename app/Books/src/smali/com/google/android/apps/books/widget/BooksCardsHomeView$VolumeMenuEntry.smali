.class abstract Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;
.super Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "VolumeMenuEntry"
.end annotation


# instance fields
.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZ)V
    .locals 6
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "titleId"    # I
    .param p4, "async"    # Z

    .prologue
    .line 576
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZZ)V

    .line 577
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZZ)V
    .locals 2
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "titleId"    # I
    .param p4, "async"    # Z
    .param p5, "isEnabled"    # Z

    .prologue
    .line 569
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .line 570
    const/4 v0, -0x1

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4, p5}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;ZZ)V

    .line 572
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;->mVolumeId:Ljava/lang/String;

    .line 573
    return-void
.end method


# virtual methods
.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method
