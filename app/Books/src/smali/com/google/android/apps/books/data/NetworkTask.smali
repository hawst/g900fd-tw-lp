.class public abstract Lcom/google/android/apps/books/data/NetworkTask;
.super Ljava/lang/Object;
.source "NetworkTask.java"


# instance fields
.field private final mPriority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 1
    .param p1, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V
    .locals 0
    .param p1, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/data/NetworkTask;->mPriority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .line 19
    iput-object p2, p0, Lcom/google/android/apps/books/data/NetworkTask;->mVolumeId:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getPriority()Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/data/NetworkTask;->mPriority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    return-object v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/data/NetworkTask;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method abstract run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
.end method
