.class public Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;
.super Ljava/lang/Object;
.source "NetworkBooksServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/NetworkBooksServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerConfig"
.end annotation


# instance fields
.field private final mAppInfo:[B

.field private final mLongerScreenLengths:Landroid/graphics/Point;

.field private final mMemoryClass:I

.field private final mShorterScreenLengths:Landroid/graphics/Point;


# direct methods
.method public constructor <init>([BILandroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 0
    .param p1, "appInfo"    # [B
    .param p2, "memoryClass"    # I
    .param p3, "shorterScreenLengths"    # Landroid/graphics/Point;
    .param p4, "longerScreenLengths"    # Landroid/graphics/Point;

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mAppInfo:[B

    .line 185
    iput p2, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mMemoryClass:I

    .line 186
    iput-object p3, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mShorterScreenLengths:Landroid/graphics/Point;

    .line 187
    iput-object p4, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mLongerScreenLengths:Landroid/graphics/Point;

    .line 188
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)[B
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getAppInfo()[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getMemoryClass()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getShorterScreenLengths()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->getLongerScreenLengths()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public static fromContext(Landroid/content/Context;)Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 191
    const-string v6, "activity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 193
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    .line 194
    .local v3, "memoryClass":I
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 195
    .local v4, "shorterScreenLengths":Landroid/graphics/Point;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 196
    .local v2, "longerScreenLengths":Landroid/graphics/Point;
    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 198
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 199
    .local v1, "display":Landroid/view/Display;
    invoke-static {v1, v4, v2}, Lcom/google/android/apps/books/util/ReaderUtils;->getCurrentSizeRange(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 201
    new-instance v6, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;

    invoke-static {p0}, Lcom/google/android/apps/books/util/EncryptionUtils;->generateAppInfo(Landroid/content/Context;)[B

    move-result-object v7

    invoke-direct {v6, v7, v3, v4, v2}, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;-><init>([BILandroid/graphics/Point;Landroid/graphics/Point;)V

    return-object v6
.end method

.method private getAppInfo()[B
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mAppInfo:[B

    return-object v0
.end method

.method private getLongerScreenLengths()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mLongerScreenLengths:Landroid/graphics/Point;

    return-object v0
.end method

.method private getMemoryClass()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mMemoryClass:I

    return v0
.end method

.method private getShorterScreenLengths()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/books/net/NetworkBooksServer$ServerConfig;->mShorterScreenLengths:Landroid/graphics/Point;

    return-object v0
.end method
