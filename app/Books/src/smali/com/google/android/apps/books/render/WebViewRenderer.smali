.class public Lcom/google/android/apps/books/render/WebViewRenderer;
.super Lcom/google/android/apps/books/render/TextModeReaderRenderer;
.source "WebViewRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;,
        Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;,
        Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;,
        Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mCurrentPageTextView:Landroid/widget/TextView;

.field private final mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

.field private final mDisplayDensity:F

.field private final mDisplayTwoPages:Z

.field private final mFooterView:Landroid/view/ViewGroup;

.field private final mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field

.field private final mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mMarginSizeSides:I

.field private final mMarginSizeTopBottom:I

.field private mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

.field private final mPageSize:Landroid/graphics/Point;

.field private final mPagesLeftTextView:Landroid/widget/TextView;

.field private final mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

.field private final mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private final mScreenSize:Landroid/graphics/Point;

.field private mSnapshotBitmap:Landroid/graphics/Bitmap;

.field private final mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

.field private mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

.field private final mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/util/Logger;Landroid/graphics/Point;ZILandroid/view/ViewGroup;)V
    .locals 29
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p5, "screenSize"    # Landroid/graphics/Point;
    .param p6, "makePagesAccessible"    # Z
    .param p7, "marginNoteIconTapSize"    # I
    .param p8, "webViewParent"    # Landroid/view/ViewGroup;

    .prologue
    .line 141
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;I)V

    .line 309
    new-instance v3, Lcom/google/android/apps/books/render/WebViewRenderer$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/books/render/WebViewRenderer$4;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    .line 646
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/apps/books/render/PageIdentifier;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    .line 143
    const-string v3, "must have WebView"

    move-object/from16 v0, p8

    invoke-static {v0, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string v3, "missing account"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mAccount:Landroid/accounts/Account;

    .line 146
    invoke-virtual/range {p8 .. p8}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v12

    .line 148
    .local v12, "context":Landroid/content/Context;
    const/4 v3, 0x0

    invoke-static {v12, v3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->create(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    .line 156
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setBackgroundColor(I)V

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-static {v3}, Lcom/google/android/apps/books/render/JavaScriptExecutors;->newExecutor(Landroid/webkit/WebView;)Lcom/google/android/apps/books/render/JavaScriptExecutor;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v5

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 161
    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 162
    .local v21, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/WebViewRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 164
    invoke-static {v12}, Lcom/google/android/apps/books/util/ReaderUtils;->displayTwoPagesForFlowingText(Landroid/content/Context;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    .line 165
    new-instance v4, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    if-eqz v3, :cond_1

    move-object/from16 v0, p5

    iget v3, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v3, v3, 0x2

    :goto_0
    move-object/from16 v0, p5

    iget v5, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v4, v3, v5}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;

    .line 166
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/render/WebViewRenderer;->mScreenSize:Landroid/graphics/Point;

    .line 168
    const v3, 0x7f0c001b

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v17

    .line 169
    .local v17, "marginPercentSides":I
    const v3, 0x7f0c001a

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    .line 171
    .local v18, "marginPercentTopBottom":I
    move-object/from16 v0, p5

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p5

    iget v4, v0, Landroid/graphics/Point;->y:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v19

    .line 172
    .local v19, "minDimension":I
    mul-int v3, v19, v17

    div-int/lit8 v3, v3, 0x64

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginSizeSides:I

    .line 173
    mul-int v3, v19, v18

    div-int/lit8 v3, v3, 0x64

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginSizeTopBottom:I

    .line 174
    new-instance v3, Lcom/google/android/apps/books/render/PageMargins;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/books/render/PageMargins;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

    .line 176
    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayDensity:F

    .line 178
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    new-instance v4, Lcom/google/android/apps/books/render/WebViewRenderer$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/books/render/WebViewRenderer$1;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setInvalidationListener(Ljava/lang/Runnable;)V

    .line 185
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setInitialScale(I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setVerticalScrollBarEnabled(Z)V

    .line 190
    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->WEBVIEW_HARDWARE_RENDERING:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v3, v12}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v27, 0x0

    .line 193
    .local v27, "viewLayerType":I
    :goto_1
    new-instance v3, Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    new-instance v5, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6}, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/render/WebViewRenderer$1;)V

    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/android/apps/books/render/WebViewRendererModel;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;IZLcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    .line 196
    new-instance v3, Lcom/google/android/apps/books/render/BasicReaderController;

    new-instance v4, Lcom/google/android/apps/books/render/BasicTextReader;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/render/BasicTextReader;-><init>(Lcom/google/android/apps/books/render/JavaScriptExecutor;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getReaderDataModel()Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/apps/books/render/BasicReaderController;-><init>(Lcom/google/android/apps/books/render/TextReader;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/util/Logger;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setJavascriptReader(Lcom/google/android/apps/books/render/ReaderController;)V

    .line 201
    const-string v3, "JsBridge"

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/BasicReaderController;->getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/books/render/ReaderListener;

    .line 205
    .local v16, "loggingListener":Lcom/google/android/apps/books/render/ReaderListener;
    new-instance v3, Lcom/google/android/apps/books/render/WebViewRenderer$2;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mAccount:Landroid/accounts/Account;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/render/WebViewRenderer$2;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/util/Logger;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    .line 216
    new-instance v9, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    move-object/from16 v0, v16

    invoke-direct {v9, v0, v3, v4}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;-><init>(Lcom/google/android/apps/books/render/ReaderListener;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/render/ReaderDataSource;)V

    .line 218
    .local v9, "bridge":Lcom/google/android/apps/books/render/ReaderBridgeAdapter;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    const-string v4, "bridge"

    invoke-virtual {v3, v9, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    new-instance v4, Lcom/google/android/apps/books/render/WebViewRenderer$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/books/render/WebViewRenderer$3;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v28

    .line 231
    .local v28, "webSettings":Landroid/webkit/WebSettings;
    invoke-static/range {v28 .. v28}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setupGenericWebSettings(Landroid/webkit/WebSettings;)V

    .line 233
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Z)V

    .line 235
    new-instance v3, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-direct {v3}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    .line 236
    const/16 v24, 0x0

    .line 237
    .local v24, "segmentIndex":I
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v25

    .line 238
    .local v25, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v23

    .line 239
    .local v23, "segmentCount":I
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getDefaultPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v11

    .line 240
    .local v11, "contentStartPosition":Lcom/google/android/apps/books/common/Position;
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/books/common/Position;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;

    move-result-object v10

    .line 242
    .local v10, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    if-eqz v11, :cond_0

    .line 243
    :goto_2
    move/from16 v0, v24

    move/from16 v1, v23

    if-ge v0, v1, :cond_0

    .line 244
    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getStartPositionObject()Lcom/google/android/apps/books/common/Position;

    move-result-object v26

    .line 245
    .local v26, "startPosition":Lcom/google/android/apps/books/common/Position;
    move-object/from16 v0, v26

    invoke-interface {v10, v0, v11}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-ltz v3, :cond_3

    .line 249
    move/from16 v22, v24

    .line 250
    .local v22, "rowSegmentCount":I
    if-lez v22, :cond_0

    .line 251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v4, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    const/4 v5, 0x1

    sget-object v6, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    move/from16 v0, v22

    invoke-virtual {v3, v4, v5, v6, v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 260
    .end local v22    # "rowSegmentCount":I
    .end local v26    # "startPosition":Lcom/google/android/apps/books/common/Position;
    :cond_0
    sub-int v20, v23, v24

    .line 261
    .local v20, "remainingRowCount":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v4, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    sget-object v5, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    move/from16 v0, v20

    move/from16 v1, v20

    invoke-virtual {v3, v4, v0, v5, v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 264
    new-instance v3, Lcom/google/android/apps/books/render/SpreadItems;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    invoke-direct {v3, v4}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 268
    invoke-static {v12}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v15

    .line 269
    .local v15, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04004e

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mFooterView:Landroid/view/ViewGroup;

    .line 270
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginSizeSides:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v14, v3, v4

    .line 271
    .local v14, "footW":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginSizeTopBottom:I

    .line 272
    .local v13, "footH":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mFooterView:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v14, v13}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mFooterView:Landroid/view/ViewGroup;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mFooterView:Landroid/view/ViewGroup;

    const v4, 0x7f0e00fb

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mPagesLeftTextView:Landroid/widget/TextView;

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mFooterView:Landroid/view/ViewGroup;

    const v4, 0x7f0e00fa

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mCurrentPageTextView:Landroid/widget/TextView;

    .line 279
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 280
    return-void

    .line 165
    .end local v9    # "bridge":Lcom/google/android/apps/books/render/ReaderBridgeAdapter;
    .end local v10    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    .end local v11    # "contentStartPosition":Lcom/google/android/apps/books/common/Position;
    .end local v13    # "footH":I
    .end local v14    # "footW":I
    .end local v15    # "inflater":Landroid/view/LayoutInflater;
    .end local v16    # "loggingListener":Lcom/google/android/apps/books/render/ReaderListener;
    .end local v17    # "marginPercentSides":I
    .end local v18    # "marginPercentTopBottom":I
    .end local v19    # "minDimension":I
    .end local v20    # "remainingRowCount":I
    .end local v23    # "segmentCount":I
    .end local v24    # "segmentIndex":I
    .end local v25    # "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .end local v27    # "viewLayerType":I
    .end local v28    # "webSettings":Landroid/webkit/WebSettings;
    :cond_1
    move-object/from16 v0, p5

    iget v3, v0, Landroid/graphics/Point;->x:I

    goto/16 :goto_0

    .line 190
    .restart local v17    # "marginPercentSides":I
    .restart local v18    # "marginPercentTopBottom":I
    .restart local v19    # "minDimension":I
    :cond_2
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 243
    .restart local v9    # "bridge":Lcom/google/android/apps/books/render/ReaderBridgeAdapter;
    .restart local v10    # "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    .restart local v11    # "contentStartPosition":Lcom/google/android/apps/books/common/Position;
    .restart local v16    # "loggingListener":Lcom/google/android/apps/books/render/ReaderListener;
    .restart local v23    # "segmentCount":I
    .restart local v24    # "segmentIndex":I
    .restart local v25    # "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .restart local v26    # "startPosition":Lcom/google/android/apps/books/common/Position;
    .restart local v27    # "viewLayerType":I
    .restart local v28    # "webSettings":Landroid/webkit/WebSettings;
    :cond_3
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/SnapshottingWebView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/WebViewRenderer;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getFirstBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/render/WebViewRenderer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getPagesPerSpread()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/PageMargins;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

    return-object v0
.end method

.method private getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private static getFirstBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I
    .locals 2
    .param p0, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 632
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 634
    :goto_0
    return v1

    .line 633
    :catch_0
    move-exception v0

    .line 634
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getPagesPerSpread()I
    .locals 1

    .prologue
    .line 977
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getTranslateToJsX(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)I
    .locals 1
    .param p1, "bounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    .prologue
    .line 302
    iget v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    return v0
.end method

.method private getTranslateToJsY(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)I
    .locals 1
    .param p1, "bounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    .prologue
    .line 306
    iget v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    return v0
.end method

.method private setup(Lcom/google/android/apps/books/model/VolumeMetadata;Z)V
    .locals 21
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "makePagesAccessible"    # Z

    .prologue
    .line 317
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/books/render/FetchingDataSource;->configureWebView(Lcom/google/android/apps/books/render/SnapshottingWebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 321
    const-string v2, "WebViewRenderer"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    const-string v2, "WebViewRenderer"

    const-string v3, "setVolumeMetadata() kicking off content load"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    new-instance v3, Lcom/google/android/apps/books/render/LruPassagePurger;

    const/16 v4, 0x4b

    invoke-direct {v3, v4}, Lcom/google/android/apps/books/render/LruPassagePurger;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setPassagePurger(Lcom/google/android/apps/books/render/LruPassagePurger;)V

    .line 326
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getContext()Landroid/content/Context;

    move-result-object v19

    .line 327
    .local v19, "context":Landroid/content/Context;
    move/from16 v10, p2

    .line 328
    .local v10, "sendPageText":Z
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    .line 329
    .local v12, "marginTopBottom":I
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    .line 330
    .local v13, "marginSides":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getFirstContentPassageIndex()I

    move-result v17

    .line 331
    .local v17, "firstContentPassageIndex":I
    new-instance v1, Lcom/google/android/apps/books/util/JsConfiguration;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterSegmentJsonArray()Lorg/json/JSONArray;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterCssJsonArray()Lorg/json/JSONArray;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getBakedCssFilesJsonArray()Lorg/json/JSONArray;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSharedFontsUrisPlus()Lorg/json/JSONArray;

    move-result-object v7

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFixedLayoutJsonArray()Lorg/json/JSONArray;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v11, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v9, v11}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v9

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayDensity:F

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    if-eqz v14, :cond_2

    const/4 v14, 0x2

    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mScreenSize:Landroid/graphics/Point;

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getFontTestString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v1 .. v17}, Lcom/google/android/apps/books/util/JsConfiguration;-><init>(Landroid/accounts/Account;Ljava/lang/String;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;Lorg/json/JSONArray;ZZFIIILandroid/graphics/Point;Ljava/lang/String;I)V

    .line 338
    .local v1, "configData":Lcom/google/android/apps/books/util/JsConfiguration;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setJsConfiguration(Lcom/google/android/apps/books/util/JsConfiguration;)V

    .line 340
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 342
    .local v18, "compileJs":Ljava/lang/String;
    const-string v20, "file:///android_asset/"

    .line 343
    .local v20, "readerUrl":Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_COMPILED_CONFIG_VALUE:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 344
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "compiled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 351
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".html"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->loadUrl(Ljava/lang/String;)V

    .line 352
    return-void

    .line 331
    .end local v1    # "configData":Lcom/google/android/apps/books/util/JsConfiguration;
    .end local v18    # "compileJs":Ljava/lang/String;
    .end local v20    # "readerUrl":Ljava/lang/String;
    :cond_2
    const/4 v14, 0x1

    goto :goto_0

    .line 345
    .restart local v1    # "configData":Lcom/google/android/apps/books/util/JsConfiguration;
    .restart local v18    # "compileJs":Ljava/lang/String;
    .restart local v20    # "readerUrl":Ljava/lang/String;
    :cond_3
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_DEBUG_CONFIG_VALUE:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "debug"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    goto :goto_1

    .line 347
    :cond_4
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_SIDELOADED_CONFIG_VALUE:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/SnapshottingWebView;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->createSideLoadFilePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    goto :goto_1
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->activateMediaElement(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setReaderSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 842
    return-void
.end method

.method public beginSelection(FFLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p4, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 682
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v6

    .line 683
    .local v6, "bounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-eqz v6, :cond_0

    .line 684
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    .line 685
    .local v1, "chapterIndex":I
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v2

    .line 686
    .local v2, "pageIndex":I
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTranslateToJsX(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, p1

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTranslateToJsY(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, p2

    const/16 v5, 0x32

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController;->loadNearbyText(IIFFI)V

    .line 689
    .end local v1    # "chapterIndex":I
    .end local v2    # "pageIndex":I
    :cond_0
    return-void
.end method

.method public canProvideText()Z
    .locals 1

    .prologue
    .line 705
    const/4 v0, 0x1

    return v0
.end method

.method public cancelPendingRequests()V
    .locals 2

    .prologue
    .line 356
    const-string v0, "WebViewRenderer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "WebViewRenderer"

    const-string v1, "Cancelling all render requests"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->cancelPendingRequests()V

    .line 360
    return-void
.end method

.method public clearAnnotationCaches()V
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->clearAnnotationCaches()V

    .line 958
    return-void
.end method

.method public convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z
    .locals 4
    .param p1, "p"    # Landroid/graphics/Point;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 786
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 787
    .local v0, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    iget v1, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

    iget v2, v2, Lcom/google/android/apps/books/render/PageMargins;->horizontal:I

    sub-int/2addr v1, v2

    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

    iget v3, v3, Lcom/google/android/apps/books/render/PageMargins;->vertical:I

    sub-int/2addr v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->offset(II)V

    .line 789
    const/4 v1, 0x1

    return v1
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mJavaScript:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->destroy()V

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDataSource:Lcom/google/android/apps/books/render/FetchingDataSource;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/FetchingDataSource;->shutDown()V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mSnapshotBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mSnapshotBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 368
    iput-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mSnapshotBitmap:Landroid/graphics/Bitmap;

    .line 370
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->cancelPendingRequests()V

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/SnapshottingWebView;->setInvalidationListener(Ljava/lang/Runnable;)V

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->clearPaginationData()V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->destroy()V

    .line 375
    invoke-super {p0}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->destroy()V

    .line 376
    return-void
.end method

.method public displayTwoPages()Z
    .locals 1

    .prologue
    .line 973
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    return v0
.end method

.method public findTouchableItem(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;I)Lcom/google/android/apps/books/render/TouchableItem;
    .locals 15
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "rendererTouchPoint"    # Landroid/graphics/Point;
    .param p3, "typeMask"    # I

    .prologue
    .line 738
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->hasTouchables()Z

    move-result v1

    if-nez v1, :cond_1

    .line 739
    :cond_0
    const/4 v1, 0x0

    .line 779
    :goto_0
    return-object v1

    .line 742
    :cond_1
    const/4 v1, -0x1

    move/from16 v0, p3

    if-eq v0, v1, :cond_4

    const/4 v12, 0x1

    .line 744
    .local v12, "filtered":Z
    :goto_1
    const/4 v9, 0x0

    .line 745
    .local v9, "candidate":Lcom/google/android/apps/books/render/TouchableItem;
    const v10, 0x7fffffff

    .line 746
    .local v10, "candidateDistance":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 749
    .local v3, "candidateBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/books/widget/DevicePageRendering;->touchableItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/books/render/TouchableItem;

    .line 750
    .local v14, "item":Lcom/google/android/apps/books/render/TouchableItem;
    if-eqz v12, :cond_3

    iget v1, v14, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    and-int v1, v1, p3

    if-eqz v1, :cond_2

    .line 757
    :cond_3
    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Point;->y:I

    iget-object v4, v14, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/books/util/MathUtils;->manhattanDistanceFromPointToRect(IILandroid/graphics/Rect;)I

    move-result v11

    .line 759
    .local v11, "distance":I
    if-nez v11, :cond_5

    .line 761
    new-instance v1, Lcom/google/android/apps/books/render/TouchableItem;

    iget v2, v14, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    new-instance v3, Landroid/graphics/Rect;

    .end local v3    # "candidateBounds":Landroid/graphics/Rect;
    iget-object v4, v14, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v4, v14, Lcom/google/android/apps/books/render/TouchableItem;->hasControls:Ljava/lang/Boolean;

    iget-object v5, v14, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    iget-object v6, v14, Lcom/google/android/apps/books/render/TouchableItem;->linkText:Ljava/lang/String;

    iget v7, v14, Lcom/google/android/apps/books/render/TouchableItem;->isFootnote:I

    iget-object v8, v14, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/books/render/TouchableItem;-><init>(ILandroid/graphics/Rect;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 742
    .end local v9    # "candidate":Lcom/google/android/apps/books/render/TouchableItem;
    .end local v10    # "candidateDistance":I
    .end local v11    # "distance":I
    .end local v12    # "filtered":Z
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_4
    const/4 v12, 0x0

    goto :goto_1

    .line 765
    .restart local v3    # "candidateBounds":Landroid/graphics/Rect;
    .restart local v9    # "candidate":Lcom/google/android/apps/books/render/TouchableItem;
    .restart local v10    # "candidateDistance":I
    .restart local v11    # "distance":I
    .restart local v12    # "filtered":Z
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_5
    if-ge v11, v10, :cond_2

    .line 766
    move-object v9, v14

    .line 767
    iget-object v1, v14, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 768
    move v10, v11

    goto :goto_2

    .line 774
    .end local v11    # "distance":I
    .end local v14    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_6
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->linkTapThresholdInPixels()I

    move-result v1

    if-ge v10, v1, :cond_7

    .line 775
    new-instance v1, Lcom/google/android/apps/books/render/TouchableItem;

    iget v2, v9, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    iget-object v4, v9, Lcom/google/android/apps/books/render/TouchableItem;->hasControls:Ljava/lang/Boolean;

    iget-object v5, v9, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    iget-object v6, v9, Lcom/google/android/apps/books/render/TouchableItem;->linkText:Ljava/lang/String;

    iget v7, v9, Lcom/google/android/apps/books/render/TouchableItem;->isFootnote:I

    iget-object v8, v9, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/books/render/TouchableItem;-><init>(ILandroid/graphics/Rect;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 779
    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Ljava/util/List;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 933
    .local p3, "visibleUserLayers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v0

    invoke-interface {p2, v0, p3}, Lcom/google/android/apps/books/widget/AnnotationIndex;->getAnnotationsForPassage(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDecorationInsetFromPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering;)I
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isVertical()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginSizeTopBottom:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginSizeSides:I

    goto :goto_0
.end method

.method public getFirstPageAfterLastViewablePage()Lcom/google/android/apps/books/render/PageHandle;
    .locals 2

    .prologue
    .line 435
    new-instance v0, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getFirstForbiddenPassageIndex()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;-><init>(Lcom/google/android/apps/books/render/TextModeReaderRenderer;I)V

    return-object v0
.end method

.method public getGridRowCount()I
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getLeftSize()I

    move-result v0

    return v0
.end method

.method public getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;
    .locals 2
    .param p1, "rowIndex"    # I

    .prologue
    .line 429
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v0

    .line 430
    .local v0, "segmentIndex":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->getStartPositionObject()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    return-object v1
.end method

.method public getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .param p3, "selectedAnnotationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getHighlightRects(I)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    return-object v0
.end method

.method public getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "marginIconTapSize"    # I
    .param p4, "includePlainHighlights"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "IZ)",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/MarginNote;",
            ">;"
        }
    .end annotation

    .prologue
    .line 952
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getMarginNoteIcons(Ljava/util/Collection;Lcom/google/android/apps/books/widget/DevicePageRendering;Z)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;
    .locals 1
    .param p1, "x0"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/TextPageHandle;

    move-result-object v0

    return-object v0
.end method

.method public getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/TextPageHandle;
    .locals 4
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const/4 v3, 0x0

    .line 412
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 413
    .local v0, "normalized":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v0, :cond_0

    .line 414
    new-instance v1, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/render/WebViewRenderer$1;)V

    .line 419
    :goto_0
    return-object v1

    .line 416
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v1

    if-nez v1, :cond_1

    .line 417
    new-instance v1, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/WebViewRenderer$1;)V

    goto :goto_0

    .line 419
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    goto :goto_0
.end method

.method public getPageToViewMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Matrix;)Z
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "result"    # Landroid/graphics/Matrix;

    .prologue
    .line 825
    invoke-virtual {p3}, Landroid/graphics/Matrix;->reset()V

    .line 827
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 828
    .local v0, "bounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-eqz v0, :cond_0

    .line 829
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTranslateToJsX(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTranslateToJsY(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p3, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 830
    const/4 v1, 0x1

    .line 834
    :goto_0
    return v1

    .line 831
    :cond_0
    const-string v1, "WebViewRenderer"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 832
    const-string v1, "WebViewRenderer"

    const-string v2, "No PageBounds in getPageToViewMatrix"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 640
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 643
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 1
    .param p1, "p1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "p2"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    return v0
.end method

.method public getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I
    .locals 9
    .param p1, "s1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "s2"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v8, 0x0

    const v5, 0x7fffffff

    .line 385
    iget-object v6, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    if-eqz v6, :cond_1

    iget-object v6, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v7, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 386
    iget v5, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget v6, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    sub-int/2addr v5, v6

    .line 407
    :cond_0
    :goto_0
    return v5

    .line 389
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p0, p1, v6}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/SpreadItems;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/TextPageHandle;

    .line 391
    .local v0, "s1FirstPage":Lcom/google/android/apps/books/render/TextPageHandle;
    invoke-interface {v0}, Lcom/google/android/apps/books/render/TextPageHandle;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v1

    .line 392
    .local v1, "s1FirstPageIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v1, :cond_0

    .line 395
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p0, p2, v6}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/SpreadItems;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/TextPageHandle;

    .line 397
    .local v2, "s2FirstPage":Lcom/google/android/apps/books/render/TextPageHandle;
    invoke-interface {v2}, Lcom/google/android/apps/books/render/TextPageHandle;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v3

    .line 398
    .local v3, "s2FirstPageIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v3, :cond_0

    .line 401
    invoke-static {v1, v8}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(Lcom/google/android/apps/books/render/PageIndices;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    invoke-static {v3, v8}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(Lcom/google/android/apps/books/render/PageIndices;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/books/render/WebViewRenderer;->getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v4

    .line 403
    .local v4, "screenPageDifference":I
    if-eq v4, v5, :cond_0

    .line 407
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getPagesPerSpread()I

    move-result v5

    div-int v5, v4, v5

    goto :goto_0
.end method

.method public getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .locals 1
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 668
    .local p2, "result":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/render/PageHandle;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/render/SpreadItems;->set(Lcom/google/android/apps/books/render/SpreadItems;)V

    .line 670
    return-object p2
.end method

.method public getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .locals 4
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "result":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/render/TextPageHandle;>;"
    const/4 v3, 0x0

    .line 651
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getSpreadPageIdentifiers(Lcom/google/android/apps/books/render/SpreadIdentifier;[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 653
    .local v0, "pages":[Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v0, :cond_1

    .line 654
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    if-eqz v1, :cond_0

    .line 655
    aget-object v1, v0, v3

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/TextPageHandle;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/TextPageHandle;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 662
    :goto_0
    return-object p2

    .line 657
    :cond_0
    aget-object v1, v0, v3

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/TextPageHandle;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V

    goto :goto_0

    .line 660
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    iget-boolean v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mDisplayTwoPages:Z

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/books/render/SpreadItems;->fill(Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public isPassageForbidden(I)Z
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isPassageForbidden(I)Z

    move-result v0

    return v0
.end method

.method public loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I
    .locals 8
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "needSelectionData"    # Z
    .param p4, "handleIndex"    # I
    .param p5, "deltaX"    # I
    .param p6, "deltaY"    # I
    .param p7, "prevRequest"    # I
    .param p8, "startSelection"    # Z

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/books/render/WebViewRendererModel;->loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIII)I

    move-result v0

    return v0
.end method

.method public loadRangeDataBulk(ILjava/util/Map;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 732
    .local p2, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->loadRangeDataBulk(ILjava/util/Map;)I

    move-result v0

    return v0
.end method

.method public onAccessibleSelectionChanged(IILcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 698
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v1

    .line 699
    .local v1, "passageIndex":I
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v0

    .line 700
    .local v0, "pageIndex":I
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mReaderController:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-virtual {v2, v1, v0, p1, p2}, Lcom/google/android/apps/books/render/BasicReaderController;->loadNotNormalizedRangeData(IIII)I

    .line 701
    return-void
.end method

.method public onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;)V
    .locals 3
    .param p1, "annotationRect"    # Lcom/google/android/apps/books/render/LabeledRect;

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mMarginNoteIconTapSize:I

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isVertical()Z

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;IZ)V

    .line 964
    return-void
.end method

.method public pageBoundsInRendererCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    const/4 v3, 0x0

    .line 805
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 806
    .local v0, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-eqz v0, :cond_0

    .line 807
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    iget v3, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    iget v4, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v5, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 813
    :goto_0
    return-object v1

    .line 810
    :cond_0
    const-string v1, "WebViewRenderer"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 811
    const-string v1, "WebViewRenderer"

    const-string v2, "pageBoundsInRendererCoordinates: no bounds yet"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z
    .locals 1
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z

    move-result v0

    return v0
.end method

.method public rendererCoordinatesToPagePointsMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Matrix;
    .locals 5
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "canvasDims"    # Landroid/graphics/Point;

    .prologue
    .line 795
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v1

    .line 796
    .local v1, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 800
    :goto_0
    return-object v0

    .line 797
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 798
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

    iget v2, v2, Lcom/google/android/apps/books/render/PageMargins;->horizontal:I

    iget v3, v1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;

    iget v3, v3, Lcom/google/android/apps/books/render/PageMargins;->vertical:I

    iget v4, v1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    goto :goto_0
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v0

    return v0
.end method

.method public requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    .line 676
    return-void
.end method

.method public setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V
    .locals 1
    .param p1, "range"    # Lcom/google/android/apps/books/render/Renderer$PageRange;

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V

    .line 918
    return-void
.end method

.method public setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache",
            "<+",
            "Lcom/google/android/apps/books/widget/WalkableHighlightRects;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 939
    .local p1, "highlightsRectsCache":Lcom/google/android/apps/books/widget/PaintableRectsCache;, "Lcom/google/android/apps/books/widget/PaintableRectsCache<+Lcom/google/android/apps/books/widget/WalkableHighlightRects;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V

    .line 940
    return-void
.end method

.method public setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 288
    invoke-super {p0, p1}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 290
    return-void
.end method

.method public weaklyAddWebLoadListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->weaklyAddOnLoadedRangeDataBulkListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V

    .line 969
    return-void
.end method
