.class Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;
.super Ljava/lang/Object;
.source "RecommendationsUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/RecommendationsUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KitkatSchoolOwnershipServiceConnection"
.end annotation


# instance fields
.field private final mConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/google/android/ublib/utils/Consumer;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "c":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/ublib/utils/Consumer;Landroid/content/Context;Lcom/google/android/apps/books/app/RecommendationsUtil$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/ublib/utils/Consumer;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Lcom/google/android/apps/books/app/RecommendationsUtil$1;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;-><init>(Lcom/google/android/ublib/utils/Consumer;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 37
    invoke-static {p2}, Lcom/google/android/nfcprovision/ISchoolOwnedService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/nfcprovision/ISchoolOwnedService;

    move-result-object v0

    .line 39
    .local v0, "ownedService":Lcom/google/android/nfcprovision/ISchoolOwnedService;
    new-instance v2, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection$1;-><init>(Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;Lcom/google/android/nfcprovision/ISchoolOwnedService;)V

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Void;

    const/4 v4, 0x0

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Void;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 64
    return-void
.end method
