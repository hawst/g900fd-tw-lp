.class Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;
.super Ljava/lang/Object;
.source "MediaOverlaysController.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/mo/MediaOverlaysController;-><init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;ZLcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;Lcom/google/android/apps/books/data/BooksDataController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClipFinishedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 6
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    const/4 v5, 0x0

    .line 246
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->isSpeaking()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->batchId:I

    iget-object v3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipBatchId:I
    invoke-static {v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$400(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 247
    const-string v2, "MediaOverlaysController"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 248
    const-string v2, "MediaOverlaysController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClipFinishedPlaying changed lastRead from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I
    invoke-static {v4}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$800(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    iget v3, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    # setter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$802(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;I)I

    .line 252
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$500(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 253
    .local v0, "finalClipInPassage":I
    iget v2, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    if-ne v0, v2, :cond_1

    .line 254
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$500(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Ljava/util/List;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    .line 256
    .local v1, "mediaClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$700(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    move-result-object v2

    iget v3, v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->passageIndex:I

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->activateMediaElement(IILjava/lang/String;)V

    .line 257
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # invokes: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->prepStateForNextPassage()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$900(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    .line 258
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # invokes: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybePlayNextClip(Ljava/lang/String;)V
    invoke-static {v2, v5}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$1000(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Ljava/lang/String;)V

    .line 261
    .end local v0    # "finalClipInPassage":I
    .end local v1    # "mediaClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    :cond_1
    return-void
.end method

.method public onClipStartedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 6
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 220
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->isSpeaking()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->batchId:I

    iget-object v3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipBatchId:I
    invoke-static {v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$400(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$500(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Ljava/util/List;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    .line 222
    .local v1, "mediaClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    invoke-virtual {v1}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getElementId()Ljava/lang/String;

    move-result-object v0

    .line 223
    .local v0, "elementId":Ljava/lang/String;
    const-string v2, "MediaOverlaysController"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 224
    const-string v2, "MediaOverlaysController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClipStartedPlaying "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I
    invoke-static {v4}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$600(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_0
    iget-boolean v2, v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->foundInPassageContent:Z

    if-eqz v2, :cond_2

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$700(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    move-result-object v2

    iget v3, v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->passageIndex:I

    iget v4, v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->devicePage:I

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->activateMediaElement(IILjava/lang/String;)V

    .line 239
    .end local v0    # "elementId":Ljava/lang/String;
    .end local v1    # "mediaClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    :cond_1
    :goto_0
    return-void

    .line 229
    .restart local v0    # "elementId":Ljava/lang/String;
    .restart local v1    # "mediaClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    :cond_2
    iget v2, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    if-nez v2, :cond_1

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;
    invoke-static {v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$700(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    move-result-object v2

    iget v3, v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->passageIndex:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->activateMediaElement(IILjava/lang/String;)V

    goto :goto_0
.end method
