.class public Lcom/google/android/apps/books/api/data/JsonTranslations$Data;
.super Ljava/lang/Object;
.source "JsonTranslations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/data/JsonTranslations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Data"
.end annotation


# instance fields
.field public languages:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "languages"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/JsonTranslations$Language;",
            ">;"
        }
    .end annotation
.end field

.field public translations:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "translations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/JsonTranslations$Translation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
