.class Lcom/google/android/apps/books/app/ReaderFragment$40;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 8447
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 8450
    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isVolumeKey(I)Z
    invoke-static {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17400(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 8451
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isVolumeKeyPageTurnsEnabled()Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    .line 8452
    .local v1, "enabled":Z
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isAnyAudioPlaying()Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    .line 8454
    .local v0, "audioPlaying":Z
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 8455
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowVolumeKeyPageTurnDialog()V
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17700(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8458
    :cond_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_2

    .line 8516
    .end local v0    # "audioPlaying":Z
    .end local v1    # "enabled":Z
    :cond_1
    :goto_0
    return v3

    .line 8463
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v5, v6, :cond_3

    move v2, v4

    .line 8464
    .local v2, "useTtsControls":Z
    :goto_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 8504
    :sswitch_0
    if-eqz v2, :cond_a

    .line 8505
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TtsUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/tts/TtsUnit;->nextLargerUnit()Lcom/google/android/apps/books/tts/TtsUnit;

    move-result-object v5

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;
    invoke-static {v3, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18202(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/TtsUnit;

    .line 8506
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->restartTts()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    move v3, v4

    .line 8507
    goto :goto_0

    .end local v2    # "useTtsControls":Z
    :cond_3
    move v2, v3

    .line 8463
    goto :goto_1

    .line 8467
    .restart local v2    # "useTtsControls":Z
    :sswitch_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cycleUiMode()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    move v3, v4

    .line 8468
    goto :goto_0

    .line 8472
    :sswitch_2
    if-eqz v2, :cond_5

    .line 8473
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 8474
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->rewind()Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v5

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v3, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 8475
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->restartTts()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    :cond_4
    :goto_2
    move v3, v4

    .line 8485
    goto :goto_0

    .line 8479
    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isNotTurning()Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 8480
    :cond_6
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v6, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v7}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/ScreenDirection;->fromReadingDirection(Lcom/google/android/apps/books/util/ReadingDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v6

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    invoke-static {v5, v6, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 8482
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z
    invoke-static {v5, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    goto :goto_2

    .line 8489
    :sswitch_3
    if-eqz v2, :cond_8

    .line 8490
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 8491
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->advance()Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v5

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v3, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 8492
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->restartTts()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    :cond_7
    :goto_3
    move v3, v4

    .line 8502
    goto/16 :goto_0

    .line 8496
    :cond_8
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18100(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isNotTurning()Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 8497
    :cond_9
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v6, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v7}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/ScreenDirection;->fromReadingDirection(Lcom/google/android/apps/books/util/ReadingDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v6

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    invoke-static {v5, v6, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 8499
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z
    invoke-static {v5, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18102(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    goto :goto_3

    .line 8510
    :cond_a
    :sswitch_4
    if-eqz v2, :cond_1

    .line 8511
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TtsUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/tts/TtsUnit;->nextSmallerUnit()Lcom/google/android/apps/books/tts/TtsUnit;

    move-result-object v5

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;
    invoke-static {v3, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18202(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/TtsUnit;

    .line 8512
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->restartTts()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    move v3, v4

    .line 8513
    goto/16 :goto_0

    .line 8464
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_4
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_1
        0x18 -> :sswitch_2
        0x19 -> :sswitch_3
        0x42 -> :sswitch_1
        0x5c -> :sswitch_2
        0x5d -> :sswitch_3
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 8524
    const/16 v2, 0x18

    if-ne p1, v2, :cond_2

    .line 8525
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8526
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isVolumeKeyPageTurnsEnabled()Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isAnyAudioPlaying()Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 8535
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 8526
    goto :goto_0

    .line 8527
    :cond_2
    const/16 v2, 0x19

    if-ne p1, v2, :cond_4

    .line 8528
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18102(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8529
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isVolumeKeyPageTurnsEnabled()Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isAnyAudioPlaying()Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 8530
    :cond_4
    const/16 v2, 0x15

    if-ne p1, v2, :cond_6

    .line 8531
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    :cond_5
    :goto_1
    move v0, v1

    .line 8535
    goto :goto_0

    .line 8532
    :cond_6
    const/16 v2, 0x16

    if-ne p1, v2, :cond_5

    .line 8533
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$40;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18102(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    goto :goto_1
.end method
