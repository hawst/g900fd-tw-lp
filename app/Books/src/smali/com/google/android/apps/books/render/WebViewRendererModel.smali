.class public Lcom/google/android/apps/books/render/WebViewRendererModel;
.super Ljava/lang/Object;
.source "WebViewRendererModel.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/WebViewRendererModel$NoPositionInPassageException;,
        Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;,
        Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;,
        Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;,
        Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;
    }
.end annotation


# instance fields
.field private final mActivePages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

.field private mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

.field private mDestroyed:Z

.field private mHighlightsRectsCache:Lcom/google/android/apps/books/widget/PaintableRectsCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/PaintableRectsCache",
            "<+",
            "Lcom/google/android/apps/books/widget/WalkableHighlightRects;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/apps/books/render/RendererListener;

.field private final mMarginNoteIcons:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;",
            ">;"
        }
    .end annotation
.end field

.field private final mMarginNoteWalker:Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;

.field private final mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

.field private mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

.field private mPurgeInProgress:Z

.field private mReader:Lcom/google/android/apps/books/render/ReaderController;

.field private final mReaderListener:Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;

.field private final mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/WeakSparseArray",
            "<",
            "Lcom/google/android/apps/books/render/RenderResponseConsumer;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestIdToPosition:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/RenderPosition;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

.field private final mTwoPageSpreads:Z

.field private final mViewLayerType:I

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;IZLcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;)V
    .locals 2
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "viewLayerType"    # I
    .param p3, "twoPageSpreads"    # Z
    .param p4, "callbacks"    # Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    .line 164
    new-instance v0, Lcom/google/android/apps/books/util/WeakSparseArray;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/WeakSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    .line 167
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mActivePages:Ljava/util/Set;

    .line 170
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;

    .line 255
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/render/PageIdentifier;

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    .line 423
    new-instance v0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReaderListener:Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;

    .line 1173
    new-instance v0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/render/WebViewRendererModel$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteWalker:Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;

    .line 1175
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;

    .line 182
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 183
    iput p2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mViewLayerType:I

    .line 184
    new-instance v0, Lcom/google/android/apps/books/render/PaginationState;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/render/PaginationState;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    .line 185
    iput-boolean p3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTwoPageSpreads:Z

    .line 186
    iput-object p4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    .line 187
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/WebViewRendererModel;Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;
    .param p1, "x1"    # Landroid/graphics/Rect;
    .param p2, "x2"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->rectIntersectsPage(Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/books/render/WebViewRendererModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPurgeInProgress:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/LruPassagePurger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/util/PassagePages;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/PassagePages;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/render/WebViewRendererModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->purgeLruPassagesAndRequests()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/PaginationState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    return-object v0
.end method

.method static synthetic access$1500(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 68
    invoke-static {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOutOfBoundsMargin(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->sendOnRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/render/WebViewRendererModel;)[Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/render/WebViewRendererModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mViewLayerType:I

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mActivePages:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/ReaderController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    return-object v0
.end method

.method private calculatePreloadPassage()Lcom/google/android/apps/books/util/PassagePages;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1191
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isAppleFixedLayout()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

    if-nez v3, :cond_1

    .line 1214
    :cond_0
    :goto_0
    return-object v2

    .line 1194
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

    iget-object v4, v4, Lcom/google/android/apps/books/render/Renderer$PageRange;->centerPosition:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-static {v3, v4}, Lcom/google/android/apps/books/render/ReaderDataModelUtils;->getBookPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 1196
    .local v0, "centerIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-nez v0, :cond_2

    .line 1197
    const-string v3, "WebViewRendererModel"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1198
    const-string v3, "WebViewRendererModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preloadPassages: can\'t find indices for center position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

    iget-object v5, v5, Lcom/google/android/apps/books/render/Renderer$PageRange;->centerPosition:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1204
    :cond_2
    iget v3, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PaginationState;->getFirstForbiddenPassageIndex()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1208
    iget v3, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v1

    .line 1209
    .local v1, "passage":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/PassagePages;->hasKnownSize()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v4

    if-le v3, v4, :cond_0

    .line 1211
    iget v2, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v2

    goto :goto_0
.end method

.method private dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 2
    .param p1, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p2, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    new-instance v1, Lcom/google/android/apps/books/render/WebViewRendererModel$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel$1;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    .line 577
    return-void
.end method

.method private static getOutOfBoundsMargin(I)I
    .locals 0
    .param p0, "pageOffset"    # I

    .prologue
    .line 1490
    if-lez p0, :cond_0

    add-int/lit8 p0, p0, -0x1

    .end local p0    # "pageOffset":I
    :cond_0
    return p0
.end method

.method private getPagesPerSpread()I
    .locals 1

    .prologue
    .line 1458
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTwoPageSpreads:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V
    .locals 1
    .param p1, "passage"    # Lcom/google/android/apps/books/util/PassagePages;

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    if-nez v0, :cond_0

    .line 567
    :goto_0
    return-void

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/LruPassagePurger;->markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V

    goto :goto_0
.end method

.method private minPagesInPassage(I)I
    .locals 4
    .param p1, "passageIndex"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1299
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v3

    if-ge p1, v3, :cond_0

    if-gez p1, :cond_1

    .line 1314
    :cond_0
    :goto_0
    return v1

    .line 1303
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isPassageForbidden(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1307
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 1308
    .local v0, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v0, :cond_2

    .line 1309
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1314
    goto :goto_0
.end method

.method private preloadPassages()V
    .locals 4

    .prologue
    .line 1218
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->calculatePreloadPassage()Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 1219
    .local v0, "preloadCandidate":Lcom/google/android/apps/books/util/PassagePages;
    if-nez v0, :cond_1

    .line 1229
    :cond_0
    :goto_0
    return-void

    .line 1222
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->isPassageReady()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->isPreloading()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1225
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->requestPreloadPassage(I)V

    .line 1226
    const-string v1, "WebViewRendererModel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1227
    const-string v1, "WebViewRendererModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preload requested for passage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private purgeLruPassages()V
    .locals 15

    .prologue
    .line 459
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    if-eqz v12, :cond_0

    iget-boolean v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPurgeInProgress:Z

    if-eqz v12, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    invoke-virtual {v12}, Lcom/google/android/apps/books/render/LruPassagePurger;->getPossiblyPurgeablePassages()Ljava/util/List;

    move-result-object v9

    .line 464
    .local v9, "purgeablePassages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    .line 469
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mActivePages:Ljava/util/Set;

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;

    .line 470
    .local v5, "painter":Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;
    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;
    invoke-static {v5}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->access$300(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 474
    .end local v5    # "painter":Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->calculatePreloadPassage()Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v8

    .line 476
    .local v8, "preloadCandidate":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v8, :cond_3

    .line 477
    invoke-virtual {v8}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 480
    :cond_3
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 481
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 482
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 483
    .local v7, "passageIndex":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {p0, v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v6

    .line 484
    .local v6, "passage":Lcom/google/android/apps/books/util/PassagePages;
    invoke-virtual {v6}, Lcom/google/android/apps/books/util/PassagePages;->isPreloading()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 485
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 491
    .end local v6    # "passage":Lcom/google/android/apps/books/util/PassagePages;
    .end local v7    # "passageIndex":Ljava/lang/Integer;
    :cond_5
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/render/RenderPosition;

    .line 493
    .local v10, "renderPosition":Lcom/google/android/apps/books/render/RenderPosition;
    iget-object v12, v10, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    if-eqz v12, :cond_7

    .line 494
    iget-object v12, v10, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    invoke-direct {p0, v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->spreadPageIdentifierToPageIdentifier(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    .line 496
    .local v4, "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v4, :cond_6

    .line 504
    :goto_4
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-static {v12, v4}, Lcom/google/android/apps/books/render/ReaderDataModelUtils;->getBookPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v2

    .line 506
    .local v2, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v2, :cond_8

    .line 507
    iget v12, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v9, v12}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 502
    .end local v2    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .end local v4    # "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_7
    iget-object v4, v10, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .restart local v4    # "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    goto :goto_4

    .line 509
    .restart local v2    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    :cond_8
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-static {v12, v4}, Lcom/google/android/apps/books/render/ReaderDataModelUtils;->getBestEffortPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v2

    .line 512
    if-eqz v2, :cond_6

    .line 522
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 523
    :cond_9
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 524
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 525
    .local v7, "passageIndex":I
    iget v12, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    sub-int v0, v7, v12

    .line 534
    .local v0, "diff":I
    iget v12, v2, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-ltz v12, :cond_b

    .line 535
    if-ltz v0, :cond_a

    iget v12, v2, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-gt v0, v12, :cond_a

    const/4 v11, 0x1

    .line 540
    .local v11, "toPreserve":Z
    :goto_6
    if-eqz v11, :cond_9

    .line 541
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .line 535
    .end local v11    # "toPreserve":Z
    :cond_a
    const/4 v11, 0x0

    goto :goto_6

    .line 537
    :cond_b
    if-gez v0, :cond_c

    iget v12, v2, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-lt v0, v12, :cond_c

    const/4 v11, 0x1

    .restart local v11    # "toPreserve":Z
    :goto_7
    goto :goto_6

    .end local v11    # "toPreserve":Z
    :cond_c
    const/4 v11, 0x0

    goto :goto_7

    .line 547
    .end local v0    # "diff":I
    .end local v2    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .end local v4    # "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v7    # "passageIndex":I
    .end local v10    # "renderPosition":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_d
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_e

    .line 548
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v12, v9}, Lcom/google/android/apps/books/render/ReaderController;->maybePurgePassages(Ljava/util/Collection;)V

    .line 549
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPurgeInProgress:Z

    .line 551
    const-string v12, "WebViewRendererModel"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 552
    const-string v12, "WebViewRendererModel"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Attempting to purge: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 555
    :cond_e
    const-string v12, "WebViewRendererModel"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 556
    const-string v12, "WebViewRendererModel"

    const-string v13, "Skipping purge: no purgeable passages."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private purgeLruPassagesAndRequests()V
    .locals 0

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->purgeOldRequests()V

    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->purgeLruPassages()V

    .line 434
    return-void
.end method

.method private purgeOldRequests()V
    .locals 6

    .prologue
    .line 440
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 441
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 442
    .local v2, "purgedRequests":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 443
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 444
    .local v3, "requestId":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/util/WeakSparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 445
    .local v0, "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/books/render/RenderResponseConsumer;->isPurgeable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 446
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 447
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    .line 448
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 451
    .end local v0    # "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .end local v3    # "requestId":Ljava/lang/Integer;
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v4, v2}, Lcom/google/android/apps/books/render/ReaderController;->purgeRequests(Ljava/util/Set;)V

    .line 452
    return-void
.end method

.method private rectIntersectsPage(Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 5
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 1356
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 1357
    .local v0, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    iget v3, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v4, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private sendOnRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    new-instance v1, Lcom/google/android/apps/books/render/WebViewRendererModel$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel$2;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    invoke-interface {v0, p2, v1}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    .line 1158
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->preloadPassages()V

    .line 1159
    return-void
.end method

.method protected static setupGenericWebSettings(Landroid/webkit/WebSettings;)V
    .locals 2
    .param p0, "webSettings"    # Landroid/webkit/WebSettings;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1510
    invoke-virtual {p0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 1511
    sget-object v0, Landroid/webkit/WebSettings$LayoutAlgorithm;->NORMAL:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 1512
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 1513
    invoke-virtual {p0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 1514
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 1537
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnIcsMR1OrLater()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 1540
    return-void
.end method

.method private spreadPageIdentifierToPageIdentifier(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 3
    .param p1, "spi"    # Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .prologue
    .line 1395
    iget-object v1, p1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getSpreadPageIdentifiers(Lcom/google/android/apps/books/render/SpreadIdentifier;[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 1397
    .local v0, "pages":[Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v0, :cond_0

    .line 1398
    iget v1, p1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    aget-object v1, v0, v1

    .line 1400
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/ReaderController;->activateMediaElement(IILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public cancelPendingRequests()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/WeakSparseArray;->clear()V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderController;->clearPendingTasks()V

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPurgeInProgress:Z

    .line 253
    return-void
.end method

.method public clearAnnotationCaches()V
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1363
    return-void
.end method

.method public clearPaginationData()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PaginationState;->clear()V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/LruPassagePurger;->clearPassages()V

    .line 241
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 1498
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mDestroyed:Z

    .line 1500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    .line 1502
    return-void
.end method

.method public getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/PaginationState;->getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    return-object v0
.end method

.method public getFirstContentPassageIndex()I
    .locals 1

    .prologue
    .line 1478
    const/4 v0, 0x1

    return v0
.end method

.method public getFirstForbiddenPassageIndex()I
    .locals 1

    .prologue
    .line 1494
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PaginationState;->getFirstForbiddenPassageIndex()I

    move-result v0

    return v0
.end method

.method public getHighlightRects(I)Lcom/google/android/apps/books/widget/Walker;
    .locals 3
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1333
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/PaintableRectsCache;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/widget/PaintableRectsCache;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/WalkableHighlightRects;

    .line 1335
    .local v0, "rects":Lcom/google/android/apps/books/widget/WalkableHighlightRects;
    if-nez v0, :cond_0

    .line 1336
    invoke-static {}, Lcom/google/android/apps/books/render/Walkers;->empty()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v1

    .line 1341
    :goto_0
    return-object v1

    .line 1339
    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/WalkableHighlightRects;->getWalker()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v1

    .line 1340
    .local v1, "walker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    invoke-interface {v1}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    goto :goto_0
.end method

.method public getInitialBlankPagesCount(I)I
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    const/4 v0, 0x0

    .line 1465
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTwoPageSpreads:Z

    if-nez v1, :cond_1

    .line 1474
    :cond_0
    :goto_0
    return v0

    .line 1469
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getFirstContentPassageIndex()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1474
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getMarginNoteIcons(Ljava/util/Collection;Lcom/google/android/apps/books/widget/DevicePageRendering;Z)Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "includePlainHighlights"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Z)",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/MarginNote;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1351
    .local p1, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteWalker:Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->setup(Ljava/util/Collection;Lcom/google/android/apps/books/widget/DevicePageRendering;Z)V

    .line 1352
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteWalker:Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;

    return-object v0
.end method

.method public getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/PaginationState;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    return-object v0
.end method

.method public getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    return-object v0
.end method

.method public getReaderDataModel()Lcom/google/android/apps/books/render/ReaderDataModel;
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    return-object v0
.end method

.method public getReaderListener()Lcom/google/android/apps/books/render/ReaderListener;
    .locals 1

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReaderListener:Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;

    return-object v0
.end method

.method public getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 1
    .param p1, "p1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "p2"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 1381
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/PaginationState;->getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    return v0
.end method

.method public getSpreadPageIdentifiers(Lcom/google/android/apps/books/render/SpreadIdentifier;[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 13
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "result"    # [Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 1427
    iget-object v4, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    .line 1428
    .local v4, "basePosition":Lcom/google/android/apps/books/common/Position;
    const/4 v11, 0x0

    invoke-static {v4, v11}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 1430
    .local v0, "basePage":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v0, :cond_1

    .line 1431
    const/4 p2, 0x0

    .line 1454
    .end local p2    # "result":[Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_0
    :goto_0
    return-object p2

    .line 1433
    .restart local p2    # "result":[Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v1

    .line 1434
    .local v1, "basePageIndices":Lcom/google/android/apps/books/render/PageIndices;
    iget v11, v1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v12

    add-int v2, v11, v12

    .line 1438
    .local v2, "basePagePassagePageIndex":I
    iget v11, v1, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getInitialBlankPagesCount(I)I

    move-result v8

    .line 1439
    .local v8, "leadingBlankPagesCount":I
    add-int v3, v2, v8

    .line 1440
    .local v3, "basePageScreenPageIndex":I
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPagesPerSpread()I

    move-result v10

    .line 1443
    .local v10, "pagesPerSpread":I
    invoke-static {v3, v10}, Lcom/google/android/apps/books/util/MathUtils;->divideRoundingDown(II)I

    move-result v5

    .line 1445
    .local v5, "baseSpreadIndex":I
    iget v11, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int v9, v5, v11

    .line 1446
    .local v9, "offsetSpreadIndex":I
    mul-int v7, v9, v10

    .line 1447
    .local v7, "firstScreenPageIndexInOffsetSpread":I
    sub-int v6, v7, v3

    .line 1449
    .local v6, "firstPageInSpreadOffsetFromBasePage":I
    const/4 v11, 0x0

    invoke-static {v4, v6}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v12

    aput-object v12, p2, v11

    .line 1450
    iget-boolean v11, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mTwoPageSpreads:Z

    if-eqz v11, :cond_0

    .line 1451
    const/4 v11, 0x1

    add-int/lit8 v12, v6, 0x1

    invoke-static {v4, v12}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v12

    aput-object v12, p2, v11

    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 1505
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mDestroyed:Z

    return v0
.end method

.method public isPassageForbidden(I)Z
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isPassageForbidden(I)Z

    move-result v0

    return v0
.end method

.method public loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIII)I
    .locals 8
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "needSelectionData"    # Z
    .param p4, "handleIndex"    # I
    .param p5, "deltaX"    # I
    .param p6, "deltaY"    # I
    .param p7, "prevRequest"    # I

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/render/ReaderController;->loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIII)I

    move-result v0

    return v0
.end method

.method public loadRangeDataBulk(ILjava/util/Map;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 404
    .local p2, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/ReaderController;->loadRangeDataBulk(ILjava/util/Map;)I

    move-result v0

    return v0
.end method

.method public normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "bestEffort"    # Z

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;IZ)V
    .locals 3
    .param p1, "annotationRect"    # Lcom/google/android/apps/books/render/LabeledRect;
    .param p2, "marginNoteIconTapSize"    # I
    .param p3, "isVertical"    # Z

    .prologue
    .line 1367
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .line 1368
    .local v0, "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    if-nez v0, :cond_0

    .line 1369
    new-instance v0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .end local v0    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    iget-object v1, p1, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    invoke-direct {v0, v1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;-><init>(Landroid/graphics/Rect;I)V

    .line 1370
    .restart local v0    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374
    :goto_0
    return-void

    .line 1372
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->maybeUpdateAnchorRect(Landroid/graphics/Rect;Z)V

    goto :goto_0
.end method

.method public onViewInvalidated()V
    .locals 1

    .prologue
    .line 1236
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    if-eqz v0, :cond_0

    .line 1237
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/RendererListener;->onPagesNeedRedraw()V

    .line 1239
    :cond_0
    return-void
.end method

.method public pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z
    .locals 8
    .param p1, "identifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const/4 v4, 0x0

    .line 1247
    const-string v5, "WebViewRendererModel"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1248
    const-string v5, "WebViewRendererModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pageExists("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1254
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v3

    .line 1255
    .local v3, "passageIndex":I
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v6

    add-int v2, v5, v6

    .line 1272
    .local v2, "pageIndex":I
    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->minPagesInPassage(I)I

    move-result v1

    .line 1274
    .local v1, "minPagesInPassage":I
    :goto_1
    if-gtz v1, :cond_1

    if-gez v2, :cond_2

    if-lez v3, :cond_2

    .line 1277
    :cond_1
    if-ltz v2, :cond_4

    if-ge v2, v1, :cond_4

    .line 1278
    const/4 v4, 0x1

    .line 1290
    .end local v1    # "minPagesInPassage":I
    .end local v2    # "pageIndex":I
    .end local v3    # "passageIndex":I
    :cond_2
    return v4

    .line 1264
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/render/PaginationState;->getPageIndices(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 1265
    .local v0, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v0, :cond_2

    .line 1268
    iget v3, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    .line 1269
    .restart local v3    # "passageIndex":I
    iget v5, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v6

    add-int v2, v5, v6

    .restart local v2    # "pageIndex":I
    goto :goto_0

    .line 1279
    .end local v0    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .restart local v1    # "minPagesInPassage":I
    :cond_4
    if-gez v2, :cond_5

    .line 1280
    add-int/lit8 v3, v3, -0x1

    .line 1281
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->minPagesInPassage(I)I

    move-result v1

    .line 1282
    add-int/2addr v2, v1

    goto :goto_1

    .line 1284
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 1285
    sub-int/2addr v2, v1

    .line 1286
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->minPagesInPassage(I)I

    move-result v1

    goto :goto_1
.end method

.method public requestPreloadPassage(I)V
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    .line 1321
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 1322
    .local v0, "passage":Lcom/google/android/apps/books/util/PassagePages;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/PassagePages;->setPreloading(Z)V

    .line 1323
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/render/ReaderController;->requestPreloadPassage(I)V

    .line 1325
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V

    .line 1326
    return-void
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderController;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v0

    return v0
.end method

.method public requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 17
    .param p1, "request"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .prologue
    .line 258
    const-string v13, "WebViewRendererModel"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 259
    const-string v13, "WebViewRendererModel"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "requestRenderPage called with "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_0
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .line 263
    .local v11, "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v11, :cond_4

    .line 264
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/apps/books/render/WebViewRendererModel;->spreadPageIdentifierToPageIdentifier(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    .line 265
    .local v4, "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v4, :cond_5

    .line 266
    iget-object v12, v11, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 267
    .local v12, "spreadIdentifier":Lcom/google/android/apps/books/render/SpreadIdentifier;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v14, v12, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v13, v14}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v8

    .line 271
    .local v8, "passageIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v13}, Lcom/google/android/apps/books/render/PaginationState;->getFirstForbiddenPassageIndex()I

    move-result v13

    if-lt v8, v13, :cond_3

    .line 273
    iget v13, v12, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-ltz v13, :cond_2

    .line 274
    iget v13, v12, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPagesPerSpread()I

    move-result v14

    mul-int/2addr v13, v14

    iget v14, v11, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    add-int/2addr v13, v14

    invoke-static {v13}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v13}, Lcom/google/android/apps/books/render/WebViewRendererModel;->dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    .line 383
    .end local v8    # "passageIndex":I
    .end local v12    # "spreadIdentifier":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_1
    :goto_0
    return-void

    .line 280
    .restart local v8    # "passageIndex":I
    .restart local v12    # "spreadIdentifier":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    iget v14, v12, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget v15, v11, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    invoke-interface {v13, v14, v15}, Lcom/google/android/apps/books/render/ReaderController;->loadSpreadPageFromEob(II)I

    move-result v10

    .line 289
    .local v10, "requestId":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v13, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    move-object/from16 v0, p2

    invoke-virtual {v13, v10, v0}, Lcom/google/android/apps/books/util/WeakSparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 283
    .end local v10    # "requestId":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    iget-object v14, v12, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v14}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v14

    iget v15, v12, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget v0, v11, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-interface {v13, v8, v14, v15, v0}, Lcom/google/android/apps/books/render/ReaderController;->loadSpreadPage(ILjava/lang/String;II)I

    move-result v10

    .restart local v10    # "requestId":I
    goto :goto_1

    .line 294
    .end local v4    # "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v8    # "passageIndex":I
    .end local v10    # "requestId":I
    .end local v12    # "spreadIdentifier":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_4
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 296
    .restart local v4    # "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_5
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 297
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v8

    .line 298
    .restart local v8    # "passageIndex":I
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    .line 299
    .local v5, "pageIndex":I
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v7

    .line 300
    .local v7, "pageOffset":I
    add-int v6, v5, v7

    .line 301
    .local v6, "pageNumber":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReaderListener:Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z
    invoke-static {v13, v4, v0, v8, v6}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->access$200(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 303
    const-string v13, "WebViewRendererModel"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 304
    const-string v13, "WebViewRendererModel"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "**** bypassed JS for passageIndex "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " pageNumber "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 309
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    add-int v14, v5, v7

    const/4 v15, 0x1

    invoke-virtual {v13, v8, v14, v15}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIndices(IIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    .line 313
    .local v3, "normalizedPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v14

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v15

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v13 .. v16}, Lcom/google/android/apps/books/render/ReaderController;->loadPage(IILjava/lang/Integer;)I

    move-result v10

    .line 316
    .restart local v10    # "requestId":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v13, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    move-object/from16 v0, p2

    invoke-virtual {v13, v10, v0}, Lcom/google/android/apps/books/util/WeakSparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 319
    .end local v3    # "normalizedPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v5    # "pageIndex":I
    .end local v6    # "pageNumber":I
    .end local v7    # "pageOffset":I
    .end local v8    # "passageIndex":I
    .end local v10    # "requestId":I
    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidPosition()Z

    move-result v13

    if-eqz v13, :cond_d

    .line 322
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-static {v13, v4}, Lcom/google/android/apps/books/render/ReaderDataModelUtils;->getBookPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v2

    .line 325
    .local v2, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReaderListener:Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;

    iget v14, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v15, v2, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z
    invoke-static {v13, v4, v0, v14, v15}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->access$200(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 327
    const-string v13, "WebViewRendererModel"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 328
    const-string v13, "WebViewRendererModel"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "**** bypassed JS for "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 332
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v14

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v15

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/apps/books/render/PaginationState;->normalizePosition(Lcom/google/android/apps/books/common/Position;IZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    .line 335
    .restart local v3    # "normalizedPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v3, :cond_c

    .line 339
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/google/android/apps/books/render/PaginationState;->isForbidden(Lcom/google/android/apps/books/render/PageIndices;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 341
    const-string v13, "WebViewRendererModel"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 342
    const-string v13, "WebViewRendererModel"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "makeOutOfBounds"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " for "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_9
    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v13

    invoke-static {v13}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v13}, Lcom/google/android/apps/books/render/WebViewRendererModel;->dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    goto/16 :goto_0

    .line 350
    :cond_a
    const-string v13, "WebViewRendererModel"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 351
    const-string v13, "WebViewRendererModel"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Used normalized indices "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " instead of position "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " & offset "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v14

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v15

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-interface/range {v13 .. v16}, Lcom/google/android/apps/books/render/ReaderController;->loadPage(IILjava/lang/Integer;)I

    move-result v10

    .line 364
    .restart local v10    # "requestId":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v13, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    move-object/from16 v0, p2

    invoke-virtual {v13, v10, v0}, Lcom/google/android/apps/books/util/WeakSparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 367
    .end local v10    # "requestId":I
    :cond_c
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v9

    .line 368
    .local v9, "positionString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v14

    invoke-interface {v13, v14}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v8

    .line 370
    .restart local v8    # "passageIndex":I
    invoke-static {v9}, Lcom/google/android/apps/books/common/Position;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    invoke-static {v13}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 373
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v13, v8, v9, v14, v15}, Lcom/google/android/apps/books/render/ReaderController;->loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)I

    move-result v10

    .line 376
    .restart local v10    # "requestId":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v13, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;

    move-object/from16 v0, p2

    invoke-virtual {v13, v10, v0}, Lcom/google/android/apps/books/util/WeakSparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 381
    .end local v2    # "indices":Lcom/google/android/apps/books/render/PageIndices;
    .end local v3    # "normalizedPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v8    # "passageIndex":I
    .end local v9    # "positionString":Ljava/lang/String;
    .end local v10    # "requestId":I
    :cond_d
    new-instance v13, Ljava/lang/IllegalStateException;

    const-string v14, "request missing both indices and position"

    invoke-direct {v13, v14}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v13
.end method

.method public setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V
    .locals 0
    .param p1, "range"    # Lcom/google/android/apps/books/render/Renderer$PageRange;

    .prologue
    .line 1179
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

    .line 1180
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->purgeLruPassagesAndRequests()V

    .line 1181
    return-void
.end method

.method public setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache",
            "<+",
            "Lcom/google/android/apps/books/widget/WalkableHighlightRects;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1346
    .local p1, "highlightsRectsCache":Lcom/google/android/apps/books/widget/PaintableRectsCache;, "Lcom/google/android/apps/books/widget/PaintableRectsCache<+Lcom/google/android/apps/books/widget/WalkableHighlightRects;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/PaintableRectsCache;

    .line 1347
    return-void
.end method

.method public setJavascriptReader(Lcom/google/android/apps/books/render/ReaderController;)V
    .locals 2
    .param p1, "client"    # Lcom/google/android/apps/books/render/ReaderController;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getMediaOverlayActiveClass()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/ReaderController;->setOverlayActiveClass(Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method public setJsConfiguration(Lcom/google/android/apps/books/util/JsConfiguration;)V
    .locals 1
    .param p1, "config"    # Lcom/google/android/apps/books/util/JsConfiguration;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderController;->initializeJavascript(Lcom/google/android/apps/books/util/JsConfiguration;)V

    .line 205
    return-void
.end method

.method public setPassagePurger(Lcom/google/android/apps/books/render/LruPassagePurger;)V
    .locals 0
    .param p1, "passagePurger"    # Lcom/google/android/apps/books/render/LruPassagePurger;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;

    .line 201
    return-void
.end method

.method public setReaderSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 1
    .param p1, "rs"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->clearPaginationData()V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->cancelPendingRequests()V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/ReaderController;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 225
    return-void
.end method

.method public setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    .line 229
    return-void
.end method

.method public weaklyAddOnLoadedRangeDataBulkListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1378
    return-void
.end method
