.class public Lcom/google/android/apps/books/app/PreIcsAccountPicker;
.super Ljava/lang/Object;
.source "PreIcsAccountPicker.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AccountPicker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public pickAccount(Landroid/app/Activity;Landroid/accounts/Account;I)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "currentAccount"    # Landroid/accounts/Account;
    .param p3, "requestCode"    # I

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/apps/books/app/AccountPicker$HostActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-interface {p1}, Lcom/google/android/apps/books/app/AccountPicker$HostActivity;->getAccountPickerCallbacks()Lcom/google/android/apps/books/app/AccountPicker$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/AccountPicker$Callbacks;->showPreIcsAccountPicker()V

    .line 21
    return-void
.end method
