.class Lcom/google/android/apps/books/data/PageContentSubcontroller$5;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "PageContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$page:Lcom/google/android/apps/books/model/Page;

.field final synthetic val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

.field final synthetic val$shouldSave:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/model/Page;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p5, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p6, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$page:Lcom/google/android/apps/books/model/Page;

    iput-boolean p7, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$shouldSave:Z

    iput-object p8, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    iput-object p9, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 16
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 241
    const/4 v14, 0x0

    .line 243
    .local v14, "encryptedImage":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v15

    .line 244
    .local v15, "server":Lcom/google/android/apps/books/net/BooksServer;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, v2, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    invoke-interface {v15, v1, v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/CcBox;

    move-result-object v13

    .line 246
    .local v13, "ccBox":Lcom/google/android/apps/books/model/CcBox;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$page:Lcom/google/android/apps/books/model/Page;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-interface {v15, v1, v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getPageImage(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v14

    .line 249
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$shouldSave:Z

    if-eqz v1, :cond_1

    .line 252
    new-instance v12, Lcom/google/android/apps/books/data/LargeBlobFromServer;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    invoke-interface {v1, v14}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v1

    invoke-direct {v12, v1}, Lcom/google/android/apps/books/data/LargeBlobFromServer;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V

    .line 259
    .local v12, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    :goto_0
    new-instance v5, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->serverCcBoxSupplier(Lcom/google/android/apps/books/model/CcBox;)Lcom/google/common/base/Supplier;
    invoke-static {v2, v13}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$300(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/model/CcBox;)Lcom/google/common/base/Supplier;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v5, v12, v1, v2, v3}, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/common/base/Supplier;Lcom/google/android/apps/books/data/PageContentSubcontroller$1;)V

    .line 260
    .local v5, "result":Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$page:Lcom/google/android/apps/books/model/Page;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$shouldSave:Z

    move-object/from16 v2, p1

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$500(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    if-eqz v14, :cond_0

    .line 272
    :try_start_1
    invoke-interface {v14}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 280
    .end local v5    # "result":Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .end local v12    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v13    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    .end local v15    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :cond_0
    :goto_1
    return-void

    .line 255
    .restart local v13    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    .restart local v15    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :cond_1
    :try_start_2
    new-instance v12, Lcom/google/android/apps/books/data/SmallBlobFromServer;

    invoke-interface {v14}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v3

    invoke-direct {v12, v1, v2, v3}, Lcom/google/android/apps/books/data/SmallBlobFromServer;-><init>([BLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/model/SessionKeyId;)V
    :try_end_2
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v12    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    goto :goto_0

    .line 274
    .restart local v5    # "result":Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    :catch_0
    move-exception v10

    .line 275
    .local v10, "e":Ljava/io/IOException;
    const-string v1, "PCSC"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    const-string v1, "PCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing image response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 262
    .end local v5    # "result":Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v12    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v13    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    .end local v15    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :catch_1
    move-exception v10

    .line 263
    .local v10, "e":Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    move-object/from16 v7, p1

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    invoke-static/range {v6 .. v11}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$600(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 271
    if-eqz v14, :cond_0

    .line 272
    :try_start_4
    invoke-interface {v14}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 274
    :catch_2
    move-exception v10

    .line 275
    .local v10, "e":Ljava/io/IOException;
    const-string v1, "PCSC"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    const-string v1, "PCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing image response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 265
    .end local v10    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v10

    .line 266
    .restart local v10    # "e":Ljava/io/IOException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v1, v0, v2, v10}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 271
    if-eqz v14, :cond_0

    .line 272
    :try_start_6
    invoke-interface {v14}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_1

    .line 274
    :catch_4
    move-exception v10

    .line 275
    const-string v1, "PCSC"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    const-string v1, "PCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing image response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 267
    .end local v10    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v10

    .line 268
    .local v10, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/PageContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v1, v0, v2, v10}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$700(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 271
    if-eqz v14, :cond_0

    .line 272
    :try_start_8
    invoke-interface {v14}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_1

    .line 274
    :catch_6
    move-exception v10

    .line 275
    .local v10, "e":Ljava/io/IOException;
    const-string v1, "PCSC"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    const-string v1, "PCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing image response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 270
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 271
    if-eqz v14, :cond_2

    .line 272
    :try_start_9
    invoke-interface {v14}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 278
    :cond_2
    :goto_2
    throw v1

    .line 274
    :catch_7
    move-exception v10

    .line 275
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v2, "PCSC"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 276
    const-string v2, "PCSC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error closing image response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
