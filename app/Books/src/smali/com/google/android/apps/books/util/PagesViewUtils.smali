.class public Lcom/google/android/apps/books/util/PagesViewUtils;
.super Ljava/lang/Object;
.source "PagesViewUtils.java"


# static fields
.field private static final RELATIVE_OFFSETS_OF_LANDSCAPE_MODE_PAGES:[I

.field private static final RELATIVE_OFFSETS_OF_PORTRAIT_MODE_PAGES:[I

.field private static final mTempDrawableSize:Landroid/graphics/Point;

.field private static final mTempPoint:Landroid/graphics/Point;

.field private static final mTempRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->RELATIVE_OFFSETS_OF_PORTRAIT_MODE_PAGES:[I

    .line 67
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->RELATIVE_OFFSETS_OF_LANDSCAPE_MODE_PAGES:[I

    .line 93
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->mTempPoint:Landroid/graphics/Point;

    .line 94
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->mTempRect:Landroid/graphics/Rect;

    .line 255
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->mTempDrawableSize:Landroid/graphics/Point;

    return-void

    .line 64
    :array_0
    .array-data 4
        0x0
        0x1
        -0x1
    .end array-data

    .line 67
    :array_1
    .array-data 4
        0x0
        -0x1
        0x1
        0x2
        -0x2
        -0x3
    .end array-data
.end method

.method public static createMutableBitmapOfSameSize(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "debugString"    # Ljava/lang/String;

    .prologue
    .line 301
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/apps/books/util/BitmapUtils;->createBitmapInReader(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static findEnclosingAnnotationBounds(Ljava/lang/String;ILjava/lang/String;ILjava/util/Iterator;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 9
    .param p0, "position"    # Ljava/lang/String;
    .param p1, "offsetFromPosition"    # I
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;"
        }
    .end annotation

    .prologue
    .line 221
    .local p4, "annotations":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p5, "locationComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/annotations/TextLocation;>;"
    const/4 v7, 0x0

    invoke-static {p2, v7, p3}, Lcom/google/android/apps/books/util/ReaderUtils;->countWhiteSpaceChars(Ljava/lang/String;II)I

    move-result v6

    .line 223
    .local v6, "whitespaceBeforeTappedCharacter":I
    add-int v7, p1, p3

    sub-int v3, v7, v6

    .line 225
    .local v3, "offsetToBeforeTappedCharacter":I
    new-instance v2, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    .line 227
    .local v2, "beforeTappedCharacterLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocation;

    add-int/lit8 v7, v3, 0x1

    invoke-direct {v0, p0, v7}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    .line 229
    .local v0, "afterTappedCharacterLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    new-instance v5, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v5, v2, v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 232
    .local v5, "rangeForTappedCharacter":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :cond_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 233
    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/Annotation;

    .line 234
    .local v1, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v4

    .line 236
    .local v4, "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    if-eqz v4, :cond_0

    .line 240
    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/books/annotations/Annotation;->GEO_LAYER_ID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4, v5, p5}, Lcom/google/android/apps/books/annotations/TextLocationRange;->intersects(Lcom/google/android/apps/books/annotations/LocationRange;Ljava/util/Comparator;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 252
    .end local v1    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v4    # "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :goto_0
    return-object v4

    .line 244
    .restart local v1    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .restart local v4    # "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p5, v7, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v7

    if-lez v7, :cond_0

    .line 252
    .end local v1    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v4    # "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static findTextRange(Ljava/lang/String;ILjava/lang/String;ILjava/util/Locale;Ljava/util/Iterator;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 11
    .param p0, "position"    # Ljava/lang/String;
    .param p1, "offsetFromPosition"    # I
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "offset"    # I
    .param p4, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Locale;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;"
        }
    .end annotation

    .prologue
    .line 184
    .local p5, "annotations":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p6, "locationComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/annotations/TextLocation;>;"
    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    .line 185
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/util/PagesViewUtils;->findEnclosingAnnotationBounds(Ljava/lang/String;ILjava/lang/String;ILjava/util/Iterator;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v6

    .line 187
    .local v6, "enclosingAnnotationRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    if-eqz v6, :cond_0

    .line 207
    .end local v6    # "enclosingAnnotationRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    :goto_0
    return-object v6

    .line 192
    :cond_0
    const/4 v0, 0x5

    invoke-static {p2, p3, v0, p4}, Lcom/google/android/apps/books/util/TextSegmentation;->findNearestWord(Ljava/lang/String;IILjava/util/Locale;)Landroid/graphics/Point;

    move-result-object v10

    .line 197
    .local v10, "wordBounds":Landroid/graphics/Point;
    if-eqz v10, :cond_1

    iget v0, v10, Landroid/graphics/Point;->y:I

    iget v1, v10, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_1

    .line 198
    const/4 v0, 0x0

    iget v1, v10, Landroid/graphics/Point;->x:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/books/util/ReaderUtils;->countWhiteSpaceChars(Ljava/lang/String;II)I

    move-result v9

    .line 200
    .local v9, "whiteSpacesBeforeStart":I
    iget v0, v10, Landroid/graphics/Point;->x:I

    add-int/2addr v0, p1

    sub-int v8, v0, v9

    .line 201
    .local v8, "startOffset":I
    iget v0, v10, Landroid/graphics/Point;->y:I

    add-int/2addr v0, p1

    sub-int v7, v0, v9

    .line 207
    .end local v9    # "whiteSpacesBeforeStart":I
    .local v7, "endOffset":I
    :goto_1
    new-instance v6, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v6, p0, v8, p0, v7}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 203
    .end local v7    # "endOffset":I
    .end local v8    # "startOffset":I
    :cond_1
    add-int v0, p1, p3

    const/4 v1, 0x0

    invoke-static {p2, v1, p3}, Lcom/google/android/apps/books/util/ReaderUtils;->countWhiteSpaceChars(Ljava/lang/String;II)I

    move-result v1

    sub-int v8, v0, v1

    .line 205
    .restart local v8    # "startOffset":I
    add-int/lit8 v7, v8, 0x1

    .restart local v7    # "endOffset":I
    goto :goto_1
.end method

.method public static fitInto(Landroid/graphics/Point;II)V
    .locals 1
    .param p0, "ioSize"    # Landroid/graphics/Point;
    .param p1, "pageWidth"    # I
    .param p2, "pageHeight"    # I

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;IILcom/google/android/apps/books/util/Holder;)V

    .line 114
    return-void
.end method

.method public static fitInto(Landroid/graphics/Point;IILcom/google/android/apps/books/util/Holder;)V
    .locals 4
    .param p0, "ioSize"    # Landroid/graphics/Point;
    .param p1, "availableWidth"    # I
    .param p2, "availableHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Point;",
            "II",
            "Lcom/google/android/apps/books/util/Holder",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p3, "outScale":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Float;>;"
    sget-object v1, Lcom/google/android/apps/books/util/PagesViewUtils;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 107
    sget-object v1, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    sget-object v2, Lcom/google/android/apps/books/util/PagesViewUtils;->mTempPoint:Landroid/graphics/Point;

    sget-object v3, Lcom/google/android/apps/books/util/PagesViewUtils;->mTempRect:Landroid/graphics/Rect;

    invoke-static {v1, v2, p0, v3, p3}, Lcom/google/android/apps/books/util/PagesViewUtils;->getPageBounds(Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Rect;Lcom/google/android/apps/books/util/Holder;)Landroid/graphics/Rect;

    move-result-object v0

    .line 109
    .local v0, "pageBounds":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 110
    return-void
.end method

.method public static getPageBounds(Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Rect;Lcom/google/android/apps/books/util/Holder;)Landroid/graphics/Rect;
    .locals 11
    .param p0, "position"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p1, "availableSize"    # Landroid/graphics/Point;
    .param p2, "pageSize"    # Landroid/graphics/Point;
    .param p3, "out"    # Landroid/graphics/Rect;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;",
            "Landroid/graphics/Point;",
            "Landroid/graphics/Point;",
            "Landroid/graphics/Rect;",
            "Lcom/google/android/apps/books/util/Holder",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Landroid/graphics/Rect;"
        }
    .end annotation

    .prologue
    .line 123
    .local p4, "outScale":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Float;>;"
    iget v7, p1, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget v8, p1, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    div-float v5, v7, v8

    .line 124
    .local v5, "spaceAspectRatio":F
    iget v7, p2, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget v8, p2, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    div-float v4, v7, v8

    .line 125
    .local v4, "pageAspectRatio":F
    iget v7, p1, Landroid/graphics/Point;->x:I

    iget v8, p2, Landroid/graphics/Point;->y:I

    mul-int/2addr v7, v8

    iget v8, p2, Landroid/graphics/Point;->x:I

    iget v9, p1, Landroid/graphics/Point;->y:I

    mul-int/2addr v8, v9

    if-ne v7, v8, :cond_2

    .line 127
    if-eqz p4, :cond_0

    .line 128
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {p4, v7}, Lcom/google/android/apps/books/util/Holder;->setValue(Ljava/lang/Object;)V

    .line 130
    :cond_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v9, p1, Landroid/graphics/Point;->x:I

    iget v10, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {p3, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 159
    :cond_1
    :goto_0
    return-object p3

    .line 133
    :cond_2
    cmpg-float v7, v4, v5

    if-gez v7, :cond_3

    const/4 v6, 0x1

    .line 134
    .local v6, "tallContent":Z
    :goto_1
    if-eqz v6, :cond_4

    .line 141
    iget v7, p1, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    mul-float/2addr v7, v4

    const v8, 0x3f666666    # 0.9f

    add-float/2addr v7, v8

    float-to-int v1, v7

    .line 142
    .local v1, "contentWidth":I
    const/4 v7, 0x0

    iput v7, p3, Landroid/graphics/Rect;->top:I

    .line 143
    iget v7, p1, Landroid/graphics/Point;->y:I

    iput v7, p3, Landroid/graphics/Rect;->bottom:I

    .line 144
    iget v7, p1, Landroid/graphics/Point;->x:I

    sub-int v3, v7, v1

    .line 145
    .local v3, "extraWidth":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getLeftLetterboxFraction()F

    move-result v7

    int-to-float v8, v3

    mul-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, p3, Landroid/graphics/Rect;->left:I

    .line 146
    iget v7, p3, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v1

    iput v7, p3, Landroid/graphics/Rect;->right:I

    .line 156
    .end local v1    # "contentWidth":I
    .end local v3    # "extraWidth":I
    :goto_2
    if-eqz p4, :cond_1

    .line 157
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    iget v8, p2, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {p4, v7}, Lcom/google/android/apps/books/util/Holder;->setValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    .end local v6    # "tallContent":Z
    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    .line 149
    .restart local v6    # "tallContent":Z
    :cond_4
    iget v7, p1, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    div-float/2addr v7, v4

    const v8, 0x3f666666    # 0.9f

    add-float/2addr v7, v8

    float-to-int v0, v7

    .line 150
    .local v0, "contentHeight":I
    const/4 v7, 0x0

    iput v7, p3, Landroid/graphics/Rect;->left:I

    .line 151
    iget v7, p1, Landroid/graphics/Point;->x:I

    iput v7, p3, Landroid/graphics/Rect;->right:I

    .line 152
    iget v7, p1, Landroid/graphics/Point;->y:I

    sub-int v2, v7, v0

    .line 153
    .local v2, "extraHeight":I
    div-int/lit8 v7, v2, 0x2

    iput v7, p3, Landroid/graphics/Rect;->top:I

    .line 154
    iget v7, p3, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v0

    iput v7, p3, Landroid/graphics/Rect;->bottom:I

    goto :goto_2
.end method

.method public static getPageWidth(IZ)I
    .locals 0
    .param p0, "width"    # I
    .param p1, "displayTwoPages"    # Z

    .prologue
    .line 82
    if-eqz p1, :cond_0

    div-int/lit8 p0, p0, 0x2

    .end local p0    # "width":I
    :cond_0
    return p0
.end method

.method public static getRelativePageOffsets(Z)[I
    .locals 1
    .param p0, "displayTwoPages"    # Z

    .prologue
    .line 74
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->RELATIVE_OFFSETS_OF_LANDSCAPE_MODE_PAGES:[I

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/PagesViewUtils;->RELATIVE_OFFSETS_OF_PORTRAIT_MODE_PAGES:[I

    goto :goto_0
.end method

.method public static getSideOfSpine(ZI)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .locals 1
    .param p0, "twoPagesVisible"    # Z
    .param p1, "offsetFromMainPage"    # I

    .prologue
    .line 309
    if-nez p0, :cond_0

    .line 311
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 313
    :goto_0
    return-object v0

    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0
.end method

.method public static getZone(FI)Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    .locals 2
    .param p0, "x"    # F
    .param p1, "width"    # I

    .prologue
    .line 44
    div-int/lit8 v0, p1, 0x5

    .line 46
    .local v0, "marginWidth":I
    int-to-float v1, v0

    cmpg-float v1, p0, v1

    if-gez v1, :cond_0

    .line 47
    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->LEFT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 51
    :goto_0
    return-object v1

    .line 48
    :cond_0
    sub-int v1, p1, v0

    int-to-float v1, v1

    cmpl-float v1, p0, v1

    if-lez v1, :cond_1

    .line 49
    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->RIGHT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    goto :goto_0

    .line 51
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->CENTER:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    goto :goto_0
.end method

.method public static maxIgnoringNull(Ljava/lang/Integer;I)I
    .locals 1
    .param p0, "a"    # Ljava/lang/Integer;
    .param p1, "b"    # I

    .prologue
    .line 60
    if-nez p0, :cond_0

    .end local p1    # "b":I
    :goto_0
    return p1

    .restart local p1    # "b":I
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0
.end method

.method public static minIgnoringNull(Ljava/lang/Integer;I)I
    .locals 1
    .param p0, "a"    # Ljava/lang/Integer;
    .param p1, "b"    # I

    .prologue
    .line 56
    if-nez p0, :cond_0

    .end local p1    # "b":I
    :goto_0
    return p1

    .restart local p1    # "b":I
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0
.end method
