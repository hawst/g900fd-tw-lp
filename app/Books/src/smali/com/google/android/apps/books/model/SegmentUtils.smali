.class public Lcom/google/android/apps/books/model/SegmentUtils;
.super Ljava/lang/Object;
.source "SegmentUtils.java"


# direct methods
.method public static equals(Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/Segment;)Z
    .locals 2
    .param p0, "segment1"    # Lcom/google/android/apps/books/model/Segment;
    .param p1, "segment2"    # Lcom/google/android/apps/books/model/Segment;

    .prologue
    .line 19
    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getPageCount()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getPageCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getFixedLayoutVersion()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getFixedLayoutVersion()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Segment;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hashCode(Lcom/google/android/apps/books/model/Segment;)I
    .locals 3
    .param p0, "segment"    # Lcom/google/android/apps/books/model/Segment;

    .prologue
    .line 12
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getPageCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getFixedLayoutVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/Segment;->getUrl()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
