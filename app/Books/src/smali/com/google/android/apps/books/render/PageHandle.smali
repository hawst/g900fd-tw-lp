.class public interface abstract Lcom/google/android/apps/books/render/PageHandle;
.super Ljava/lang/Object;
.source "PageHandle.java"


# virtual methods
.method public abstract getFirstBookPage()Lcom/google/android/apps/books/model/Page;
.end method

.method public abstract getFirstBookPageIndex()I
.end method

.method public abstract getFirstChapter()Lcom/google/android/apps/books/model/Chapter;
.end method

.method public abstract getFirstChapterIndex()I
.end method

.method public abstract getGridRowIndex()I
.end method

.method public abstract getHeight()I
.end method

.method public abstract getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
.end method

.method public abstract getPosition()Lcom/google/android/apps/books/common/Position;
.end method

.method public abstract getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
.end method

.method public abstract getWidth()I
.end method
