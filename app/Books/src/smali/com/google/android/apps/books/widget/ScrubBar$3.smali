.class Lcom/google/android/apps/books/widget/ScrubBar$3;
.super Ljava/lang/Object;
.source "ScrubBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ScrubBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ScrubBar;)V
    .locals 0

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar$3;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1086
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_QUICK_BOOKMARK_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logScrubberAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;)V

    .line 1087
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$3;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->access$800(Lcom/google/android/apps/books/widget/ScrubBar;)Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$3;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    # getter for: Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->access$800(Lcom/google/android/apps/books/widget/ScrubBar;)Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;->onScrubBookmarkTap(I)V

    .line 1090
    :cond_0
    return-void
.end method
