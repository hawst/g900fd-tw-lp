.class public Lcom/google/android/apps/books/util/Logging;
.super Ljava/lang/Object;
.source "Logging.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;,
        Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;,
        Lcom/google/android/apps/books/util/Logging$CompletionCallback;,
        Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    }
.end annotation


# static fields
.field public static final DISABLED_TRACKER:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

.field private static sCurrentTrackerCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I

    .line 153
    new-instance v0, Lcom/google/android/apps/books/util/Logging$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/Logging$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/Logging;->DISABLED_TRACKER:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    return-void
.end method

.method static synthetic access$004()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I

    return v0
.end method

.method static synthetic access$006()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/apps/books/util/Logging;->sCurrentTrackerCount:I

    return v0
.end method

.method public static startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .locals 1
    .param p0, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 187
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v0

    return-object v0
.end method

.method public static startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .locals 3
    .param p0, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/google/android/apps/books/util/Logging$CompletionCallback;

    .prologue
    .line 168
    if-nez p0, :cond_0

    .line 169
    sget-object v0, Lcom/google/android/apps/books/util/Logging;->DISABLED_TRACKER:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 179
    :goto_0
    return-object v0

    .line 171
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {p0, v0}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    sget-object v0, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 173
    new-instance v0, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/apps/books/util/Logging$ExpensivePerformanceTracker;-><init>(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;Lcom/google/android/apps/books/util/Logging$1;)V

    goto :goto_0

    .line 175
    :cond_1
    if-eqz p2, :cond_2

    .line 177
    new-instance v0, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/util/Logging$BasePerformanceTracker;-><init>(Lcom/google/android/apps/books/util/Logging$CompletionCallback;)V

    goto :goto_0

    .line 179
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/util/Logging;->DISABLED_TRACKER:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    goto :goto_0
.end method
