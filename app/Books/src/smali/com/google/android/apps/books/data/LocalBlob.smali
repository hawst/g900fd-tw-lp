.class public Lcom/google/android/apps/books/data/LocalBlob;
.super Ljava/lang/Object;
.source "LocalBlob.java"

# interfaces
.implements Lcom/google/android/apps/books/data/DataControllerBlob;


# instance fields
.field private final mByteArrayCache:Lcom/google/android/apps/books/data/ByteArrayCache;

.field private final mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/InputStreamSource;)V
    .locals 2
    .param p1, "file"    # Lcom/google/android/apps/books/data/InputStreamSource;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/books/data/LocalBlob;->mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;

    .line 22
    new-instance v0, Lcom/google/android/apps/books/data/ByteArrayCache;

    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalBlob;->mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/data/ByteArrayCache;-><init>(Lcom/google/android/apps/books/data/InputStreamSource;)V

    iput-object v0, p0, Lcom/google/android/apps/books/data/LocalBlob;->mByteArrayCache:Lcom/google/android/apps/books/data/ByteArrayCache;

    .line 23
    return-void
.end method


# virtual methods
.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalBlob;->mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/InputStreamSource;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalBlob;->mByteArrayCache:Lcom/google/android/apps/books/data/ByteArrayCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/data/ByteArrayCache;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public save()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    return-void
.end method
