.class public Lcom/google/android/apps/books/render/ResourceCollector;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "ResourceCollector.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/model/StubBooksDataListener;",
        "Lcom/google/common/base/Function",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/apps/books/model/Resource;",
        ">;"
    }
.end annotation


# instance fields
.field private final mResourceIdToResource:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    .line 20
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/ResourceCollector;->mResourceIdToResource:Ljava/util/Map;

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/books/render/ResourceCollector;->mVolumeId:Ljava/lang/String;

    .line 24
    return-void
.end method

.method private declared-synchronized addResources(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/ResourceCollector;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Resource;

    .line 40
    .local v1, "resource":Lcom/google/android/apps/books/model/Resource;
    iget-object v2, p0, Lcom/google/android/apps/books/render/ResourceCollector;->mResourceIdToResource:Ljava/util/Map;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 38
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "resource":Lcom/google/android/apps/books/model/Resource;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 43
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized apply(Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 1
    .param p1, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/ResourceCollector;->mResourceIdToResource:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Resource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/ResourceCollector;->apply(Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method public onNewResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/books/render/ResourceCollector;->addResources(Ljava/lang/String;Ljava/util/List;)V

    .line 35
    return-void
.end method

.method public onNewSegmentResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/books/render/ResourceCollector;->addResources(Ljava/lang/String;Ljava/util/List;)V

    .line 29
    return-void
.end method
