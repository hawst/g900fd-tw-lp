.class Lcom/google/android/apps/books/preference/PlusMinusPreference$1;
.super Ljava/lang/Object;
.source "PlusMinusPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/preference/PlusMinusPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/preference/PlusMinusPreference;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    # getter for: Lcom/google/android/apps/books/preference/PlusMinusPreference;->mBinding:Z
    invoke-static {v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->access$000(Lcom/google/android/apps/books/preference/PlusMinusPreference;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    # getter for: Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F
    invoke-static {v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->access$100(Lcom/google/android/apps/books/preference/PlusMinusPreference;)F

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    # getter for: Lcom/google/android/apps/books/preference/PlusMinusPreference;->mMinus:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->access$200(Lcom/google/android/apps/books/preference/PlusMinusPreference;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;->DOWN:Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->stepValue(FLcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;)F

    move-result v0

    # invokes: Lcom/google/android/apps/books/preference/PlusMinusPreference;->updateValue(F)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->access$300(Lcom/google/android/apps/books/preference/PlusMinusPreference;F)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/apps/books/preference/PlusMinusPreference;->setMinusPlusButtonsEnabled(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->access$400(Lcom/google/android/apps/books/preference/PlusMinusPreference;Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->persistPreference()V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;->this$0:Lcom/google/android/apps/books/preference/PlusMinusPreference;

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->getSettingTextString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/google/android/ublib/utils/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;->UP:Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    goto :goto_1
.end method
