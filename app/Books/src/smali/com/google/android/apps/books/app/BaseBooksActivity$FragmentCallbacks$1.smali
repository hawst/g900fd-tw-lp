.class Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;
.super Ljava/lang/Object;
.source "BaseBooksActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->addVolumeToMyEBooks(Landroid/accounts/Account;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 391
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;, "Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks.1;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->this$1:Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;

    iput-object p2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 394
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;, "Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks.1;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->this$1:Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;

    iget-object v2, v2, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    :goto_0
    return-void

    .line 397
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->this$1:Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;

    iget-object v2, v2, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 398
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 399
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-class v2, Lcom/google/android/apps/books/app/PromptAddVolumeDialog;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->val$account:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks$1;->val$volumeId:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$Arguments;->create(Landroid/accounts/Account;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v1, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 402
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method
