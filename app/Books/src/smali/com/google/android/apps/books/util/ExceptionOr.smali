.class public Lcom/google/android/apps/books/util/ExceptionOr;
.super Lcom/google/android/apps/books/util/Either;
.source "ExceptionOr.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/books/util/Either",
        "<TT;",
        "Ljava/lang/Exception;",
        ">;"
    }
.end annotation


# static fields
.field public static final OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;Ljava/lang/Exception;Z)V
    .locals 0
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "isSuccess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Exception;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/util/Either;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 19
    return-void
.end method

.method public static varargs firstFailure([Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<*>;)",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "values":[Lcom/google/android/apps/books/util/ExceptionOr;, "[Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/apps/books/util/ExceptionOr;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 70
    .local v3, "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 74
    .end local v3    # "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    :goto_1
    return-object v3

    .line 69
    .restart local v3    # "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    .end local v3    # "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getNonNullValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    invoke-static {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;

    move-result-object v0

    .line 118
    .local v0, "result":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_0

    .line 119
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Unexpected null value"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 121
    :cond_0
    return-object v0
.end method

.method public static getValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    .local p0, "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    if-nez p0, :cond_0

    .line 98
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Unexpected null ExceptionOr"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    if-nez v0, :cond_2

    .line 104
    new-instance v0, Ljava/lang/NullPointerException;

    .end local v0    # "e":Ljava/lang/Exception;
    const-string v1, "Unexpected null exception"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 106
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_2
    throw v0
.end method

.method public static isFailure(Lcom/google/android/apps/books/util/ExceptionOr;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 3
    .param p0, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/books/util/ExceptionOr;-><init>(Ljava/lang/Object;Ljava/lang/Exception;Z)V

    return-object v0
.end method

.method public static makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(TU;)",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "value":Ljava/lang/Object;, "TU;"
    new-instance v0, Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/util/ExceptionOr;-><init>(Ljava/lang/Object;Ljava/lang/Exception;Z)V

    return-object v0
.end method

.method public static maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TT;>;>;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 137
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TT;>;>;"
    if-eqz p0, :cond_0

    .line 138
    invoke-static {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 140
    :cond_0
    return-void
.end method

.method public static maybeDeliverOpaqueSuccess(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    if-eqz p0, :cond_0

    .line 132
    sget-object v0, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p0, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 134
    :cond_0
    return-void
.end method

.method public static maybeDeliverSuccess(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TT;>;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TT;>;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-eqz p0, :cond_0

    .line 126
    invoke-static {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 128
    :cond_0
    return-void
.end method

.method public static replaceValueIfSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<*>;)",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "value":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    .line 86
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/ExceptionOr;->right:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Exception;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/ExceptionOr;->left:Ljava/lang/Object;

    return-object v0
.end method

.method public isFailure()Z
    .locals 1

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/ExceptionOr;->isLeft:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/ExceptionOr;->isLeft:Z

    return v0
.end method

.method public throwIfFailure()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/ExceptionOr;->right:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/util/ExceptionOr;->right:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Exception;

    throw v0

    .line 49
    :cond_0
    return-void
.end method
