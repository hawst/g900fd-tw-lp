.class public Lcom/google/android/apps/books/model/CachedOffersImpl;
.super Ljava/lang/Object;
.source "CachedOffersImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;


# instance fields
.field private final mLastModifiedMillis:J

.field private final mOffers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/api/data/ApiaryOffers;J)V
    .locals 2
    .param p1, "apiaryOffers"    # Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .param p2, "lastModifiedMillis"    # J

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferData;->fromApiaryOffers(Lcom/google/android/apps/books/api/data/ApiaryOffers;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/CachedOffersImpl;->mOffers:Ljava/util/List;

    .line 14
    iput-wide p2, p0, Lcom/google/android/apps/books/model/CachedOffersImpl;->mLastModifiedMillis:J

    .line 15
    return-void
.end method


# virtual methods
.method public getLastModifiedMillis()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/google/android/apps/books/model/CachedOffersImpl;->mLastModifiedMillis:J

    return-wide v0
.end method

.method public getOffers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/model/CachedOffersImpl;->mOffers:Ljava/util/List;

    return-object v0
.end method
