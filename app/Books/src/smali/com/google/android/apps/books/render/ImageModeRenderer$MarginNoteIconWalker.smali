.class public Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;
.super Ljava/lang/Object;
.source "ImageModeRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MarginNoteIconWalker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Lcom/google/android/apps/books/render/MarginNote;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnnotationIndex:I

.field private mAnnotations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final mIconCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;",
            ">;"
        }
    .end annotation
.end field

.field private mIncludePlainHighlights:Z

.field private mMarginIconTapSize:I

.field private mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field private final mTempRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotationIndex:I

    .line 1089
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mIconCache:Ljava/util/Map;

    .line 1090
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mTempRect:Landroid/graphics/Rect;

    return-void
.end method

.method private getMarginNoteIcon(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/widget/DevicePageRendering;I)Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    .locals 5
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "marginIconTapSize"    # I

    .prologue
    .line 1129
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mIconCache:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .line 1130
    .local v0, "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    if-nez v0, :cond_2

    .line 1131
    new-instance v1, Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/PageStructureSelection;-><init>()V

    .line 1132
    .local v1, "selection":Lcom/google/android/apps/books/render/PageStructureSelection;
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/books/render/PageStructureSelection;->setTo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Lcom/google/android/apps/books/render/PageStructureLocationRange;)V

    .line 1134
    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->textRects()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v2

    .line 1135
    .local v2, "walker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    invoke-interface {v2}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 1136
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mTempRect:Landroid/graphics/Rect;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1137
    if-nez v0, :cond_0

    .line 1138
    new-instance v0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .end local v0    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {v0, v3, p3}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;-><init>(Landroid/graphics/Rect;I)V

    .restart local v0    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    goto :goto_0

    .line 1142
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mTempRect:Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->maybeUpdateAnchorRect(Landroid/graphics/Rect;Z)V

    goto :goto_0

    .line 1145
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mIconCache:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147
    .end local v1    # "selection":Lcom/google/android/apps/books/render/PageStructureSelection;
    .end local v2    # "walker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    :cond_2
    return-object v0
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/render/MarginNote;)Z
    .locals 6
    .param p1, "note"    # Lcom/google/android/apps/books/render/MarginNote;

    .prologue
    .line 1099
    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v3

    .line 1101
    .local v3, "pageId":Ljava/lang/String;
    :cond_0
    iget v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotationIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotationIndex:I

    iget-object v5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotations:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 1102
    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotations:Ljava/util/List;

    iget v5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotationIndex:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 1104
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v2

    .line 1112
    .local v2, "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v4}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1117
    iget-boolean v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mIncludePlainHighlights:Z

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1118
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget v5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mMarginIconTapSize:I

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->getMarginNoteIcon(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/widget/DevicePageRendering;I)Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    move-result-object v1

    .line 1120
    .local v1, "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v5

    invoke-virtual {p1, v4, v5, v1}, Lcom/google/android/apps/books/render/MarginNote;->set(Ljava/lang/String;ILcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;)V

    .line 1121
    const/4 v4, 0x1

    .line 1124
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v1    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    .end local v2    # "imageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1081
    check-cast p1, Lcom/google/android/apps/books/render/MarginNote;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->next(Lcom/google/android/apps/books/render/MarginNote;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 1094
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotationIndex:I

    .line 1095
    return-void
.end method

.method public setup(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)V
    .locals 0
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "marginIconTapSize"    # I
    .param p4, "includePlainHighlights"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 1152
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mAnnotations:Ljava/util/List;

    .line 1153
    iput-object p2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 1154
    iput p3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mMarginIconTapSize:I

    .line 1155
    iput-boolean p4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->mIncludePlainHighlights:Z

    .line 1156
    return-void
.end method
