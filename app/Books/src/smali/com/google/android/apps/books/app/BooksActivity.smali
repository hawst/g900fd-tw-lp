.class public Lcom/google/android/apps/books/app/BooksActivity;
.super Lcom/google/android/apps/books/app/BaseBooksActivity;
.source "BooksActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/HomeFragment$Callbacks;",
        ">;"
    }
.end annotation


# static fields
.field private static final sStubHomeCallbacks:Lcom/google/android/apps/books/app/HomeFragment$Callbacks;


# instance fields
.field private mAddedFragments:Z

.field private final mHomeCallbacks:Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 260
    new-instance v0, Lcom/google/android/apps/books/app/StubHomeCallbacks;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/BooksActivity;->sStubHomeCallbacks:Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;-><init>()V

    .line 258
    new-instance v0, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;-><init>(Lcom/google/android/apps/books/app/BooksActivity;Lcom/google/android/apps/books/app/BooksActivity$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/BooksActivity;->mHomeCallbacks:Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/BooksActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksActivity;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksActivity;->creatingShortcut()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/BooksActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BooksActivity;->createShortcut(Ljava/lang/String;)V

    return-void
.end method

.method private addFragments()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 445
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    .line 446
    .local v6, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 448
    .local v4, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-string v0, "HomeActivity.volumes"

    invoke-static {v6, v4, v0}, Lcom/google/android/apps/books/app/BooksActivity;->removeFragment(Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->buildFrom(Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v3

    .line 452
    .local v3, "args":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksActivity;->creatingShortcut()Z

    move-result v0

    invoke-static {v3, v0}, Lcom/google/android/apps/books/util/LoaderParams;->setCreatingShortcut(Landroid/os/Bundle;Z)V

    .line 454
    const v0, 0x7f0e00a8

    const-class v1, Lcom/google/android/apps/books/app/HomeFragment;

    const-string v2, "HomeActivity.volumes"

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BooksActivity;->createAndAddFragment(ILjava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 456
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 458
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/BooksActivity;->mAddedFragments:Z

    .line 459
    return-void
.end method

.method private createShortcut(Ljava/lang/String;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 284
    const v2, 0x7f0f0136

    invoke-static {v2}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->newInstance(I)Lcom/google/android/apps/books/app/ProgressDialogFragment;

    move-result-object v0

    .line 286
    .local v0, "progressDialog":Lcom/google/android/apps/books/app/ProgressDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "ProgressDialogFragment"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I

    .line 289
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/BooksActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v1

    .line 292
    .local v1, "shortcutIconSize":I
    new-instance v2, Lcom/google/android/apps/books/app/BooksActivity$1;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/apps/books/app/BooksActivity$1;-><init>(Lcom/google/android/apps/books/app/BooksActivity;Ljava/lang/String;I)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-static {v2, v3}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 357
    return-void
.end method

.method private creatingShortcut()Z
    .locals 2

    .prologue
    .line 428
    const-string v0, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 267
    instance-of v0, p0, Lcom/google/android/apps/books/app/BooksActivity;

    if-eqz v0, :cond_0

    .line 268
    check-cast p0, Lcom/google/android/apps/books/app/BooksActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    .line 270
    .restart local p0    # "context":Landroid/content/Context;
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksActivity;->sStubHomeCallbacks:Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    goto :goto_0
.end method

.method public static getHomeCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 263
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private static removeFragment(Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)V
    .locals 1
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "ft"    # Landroid/support/v4/app/FragmentTransaction;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 438
    invoke-virtual {p0, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 439
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 440
    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 442
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method public getFragmentCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    sget-object v0, Lcom/google/android/apps/books/app/BooksActivity;->sStubHomeCallbacks:Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    .line 279
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksActivity;->mHomeCallbacks:Lcom/google/android/apps/books/app/BooksActivity$HomeCallbacks;

    goto :goto_0
.end method

.method protected getThemeId(Z)I
    .locals 1
    .param p1, "dark"    # Z

    .prologue
    .line 434
    const v0, 0x7f0a01ed

    return v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 390
    const-string v2, "HomeActivity.volumes"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/BooksActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/HomeFragment;

    .line 392
    .local v1, "homeFragment":Lcom/google/android/apps/books/app/HomeFragment;
    if-eqz v1, :cond_1

    .line 393
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->onBackPressed()Z

    move-result v0

    .line 398
    .local v0, "handled":Z
    :goto_0
    if-nez v0, :cond_0

    .line 399
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onBackPressed()V

    .line 401
    :cond_0
    return-void

    .line 395
    .end local v0    # "handled":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "handled":Z
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 361
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onCreate(Landroid/os/Bundle;)V

    .line 363
    if-eqz p1, :cond_0

    .line 364
    const-string v2, "HomeActivity.addedFragments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/BooksActivity;->mAddedFragments:Z

    .line 367
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 368
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040021

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 369
    .local v1, "view":Landroid/view/ViewGroup;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/BooksActivity;->setContentView(Landroid/view/View;)V

    .line 370
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksActivity;->finish()V

    .line 378
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksActivity;->startActivity(Landroid/content/Intent;)V

    .line 380
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 463
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->clearImageCache()V

    .line 464
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onPause()V

    .line 465
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 384
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 385
    const-string v0, "HomeActivity.addedFragments"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/BooksActivity;->mAddedFragments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 386
    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 405
    const-string v1, "HomeActivity.volumes"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/BooksActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/HomeFragment;

    .line 406
    .local v0, "homeFragment":Lcom/google/android/apps/books/app/HomeFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->onSearchRequested()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onSelectedAccount(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BooksActivity;->mAddedFragments:Z

    if-nez v0, :cond_1

    .line 413
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksActivity;->creatingShortcut()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const v0, 0x7f0f0134

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/BooksActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 423
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksActivity;->addFragments()V

    .line 425
    :cond_1
    return-void
.end method
