.class public Lcom/google/android/apps/books/app/ReadingActivity;
.super Lcom/google/android/apps/books/app/BaseBooksActivity;
.source "ReadingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;",
        ">;"
    }
.end annotation


# static fields
.field private static ACCESSIBILITY_OVERRIDE_DURATION_MILLIS:J

.field static final sStubReaderCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;


# instance fields
.field private mAccessibilityOverrideString:Ljava/lang/String;

.field private mAddedFragments:Z

.field private final mFragmentCallbacks:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

.field private mRatingInfo:Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

.field private mRatingIsInProgress:Z

.field private mRatingRequestCode:I

.field private mSystemUi:Lcom/google/android/ublib/view/SystemUi;

.field private mTimestampStartOfAccessibilityOverride:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 210
    const-wide/16 v0, 0x1f4

    sput-wide v0, Lcom/google/android/apps/books/app/ReadingActivity;->ACCESSIBILITY_OVERRIDE_DURATION_MILLIS:J

    .line 216
    new-instance v0, Lcom/google/android/apps/books/app/StubReaderCallbacks;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/ReadingActivity;->sStubReaderCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;-><init>()V

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingRequestCode:I

    .line 211
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mTimestampStartOfAccessibilityOverride:J

    .line 212
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAccessibilityOverrideString:Ljava/lang/String;

    .line 214
    new-instance v0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;-><init>(Lcom/google/android/apps/books/app/ReadingActivity;Lcom/google/android/apps/books/app/ReadingActivity$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mFragmentCallbacks:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/apps/books/widget/PagesView3D;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/app/ReadingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAccessibilityOverrideString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/app/ReadingActivity;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;
    .param p1, "x1"    # J

    .prologue
    .line 51
    iput-wide p1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mTimestampStartOfAccessibilityOverride:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/ublib/view/SystemUi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingInfo:Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/app/ReadingActivity;Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;
    .param p1, "x1"    # Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingInfo:Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mFragmentCallbacks:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/app/ReadingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/app/ReadingActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingRequestCode:I

    return p1
.end method

.method private getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;
    .locals 1

    .prologue
    .line 342
    const v0, 0x7f0e0157

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ReadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesView3D;

    return-object v0
.end method

.method public static getReaderCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .locals 1
    .param p0, "maybeReadingActivity"    # Landroid/content/Context;

    .prologue
    .line 223
    instance-of v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;

    if-eqz v0, :cond_0

    .line 224
    check-cast p0, Lcom/google/android/apps/books/app/ReadingActivity;

    .end local p0    # "maybeReadingActivity":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    .line 226
    .restart local p0    # "maybeReadingActivity":Landroid/content/Context;
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/ReadingActivity;->sStubReaderCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    goto :goto_0
.end method

.method public static getReaderCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 219
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method public static getReaderTheme(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 346
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getReaderTheme()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loaderParamsFromIntent()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 441
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->volumeIdFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 442
    .local v2, "volumeId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/apps/books/util/LoaderParams;->buildFrom(Landroid/accounts/Account;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 444
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {v1}, Lcom/google/android/apps/books/app/BookOpeningFlags;->fromIntent(Landroid/content/Intent;)Lcom/google/android/apps/books/app/BookOpeningFlags;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/app/BookOpeningFlags;->setArgs(Landroid/os/Bundle;)V

    .line 450
    .end local v0    # "args":Landroid/os/Bundle;
    :goto_0
    return-object v0

    .line 447
    :cond_0
    const-string v3, "ReadingActivity"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 448
    const-string v3, "ReadingActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t find volume ID in intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private prepareOpenGlViews()V
    .locals 5

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v1

    .line 362
    .local v1, "reader":Lcom/google/android/apps/books/widget/PagesView3D;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/widget/PagesView3D;->setVisibility(I)V

    .line 368
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 371
    .local v0, "coverView":Landroid/view/View;
    const v3, 0x7f0e00aa

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/ReadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 374
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->setCoverView(Landroid/view/View;)V

    .line 376
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderTheme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 377
    .local v2, "readerTheme":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 378
    return-void
.end method

.method private setupActionBar()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x6

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 254
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 255
    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/StyleUtils;->setThemedActionBarDrawable(Landroid/content/Context;Landroid/support/v7/app/ActionBar;)V

    .line 267
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    invoke-virtual {v0, v2, v2}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_0
.end method

.method private setupStatusBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 281
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    invoke-virtual {p0, v2, v3, v5, v5}, Lcom/google/android/apps/books/app/ReadingActivity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 283
    .local v1, "ta":Landroid/content/res/TypedArray;
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 284
    .local v0, "statusBarColor":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 285
    if-eq v0, v4, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 289
    .end local v0    # "statusBarColor":I
    .end local v1    # "ta":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method

.method private static volumeIdFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 429
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 431
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 432
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 436
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v2

    .line 433
    :cond_0
    const-string v2, "com.google.android.apps.books.intent.action.READ"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 434
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getVolumeIdFromIntent(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 436
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 569
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 571
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAccessibilityOverrideString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 573
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mTimestampStartOfAccessibilityOverride:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/google/android/apps/books/app/ReadingActivity;->ACCESSIBILITY_OVERRIDE_DURATION_MILLIS:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 580
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAccessibilityOverrideString:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 585
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAccessibilityOverrideString:Ljava/lang/String;

    .line 586
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mTimestampStartOfAccessibilityOverride:J

    .line 590
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method public getFragmentCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    sget-object v0, Lcom/google/android/apps/books/app/ReadingActivity;->sStubReaderCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    .line 235
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mFragmentCallbacks:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    goto :goto_0
.end method

.method public getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    if-nez v0, :cond_0

    .line 245
    invoke-static {p1}, Lcom/google/android/ublib/view/SystemUiUtils;->makeSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    return-object v0
.end method

.method protected getThemeId(Z)I
    .locals 1
    .param p1, "dark"    # Z

    .prologue
    .line 481
    if-eqz p1, :cond_0

    const v0, 0x7f0a01e1

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0a01e2

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "result"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 492
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 497
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingRequestCode:I

    if-ne p1, v2, :cond_2

    .line 498
    const-string v2, "ReadingActivity"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 499
    const-string v2, "ReadingActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityResult, rating result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    .line 502
    const-string v2, "ReadingActivity.reader"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 503
    .local v1, "reader":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 504
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->requestRatingInfoUpdate()V

    .line 505
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLoadEndOfBookPage()V

    .line 516
    .end local v1    # "reader":Lcom/google/android/apps/books/app/ReaderFragment;
    :cond_1
    :goto_0
    return-void

    .line 507
    :cond_2
    if-ne p1, v3, :cond_1

    if-ne p2, v3, :cond_1

    .line 509
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 510
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "ReadingActivity.reader"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 511
    .restart local v1    # "reader":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 512
    new-instance v2, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-static {v0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getPosition(Landroid/os/Bundle;)Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-direct {v2, v3, v5}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    invoke-static {v0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getMoveType(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/MoveType;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 402
    const-string v2, "ReadingActivity.reader"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 403
    .local v1, "readerFragment":Lcom/google/android/apps/books/app/ReaderFragment;
    const/4 v0, 0x0

    .line 404
    .local v0, "handled":Z
    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->onBackPressed()Z

    move-result v0

    .line 407
    :cond_0
    if-nez v0, :cond_1

    .line 408
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logClosedBookUsingBackButton()V

    .line 409
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onBackPressed()V

    .line 411
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 293
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onCreate(Landroid/os/Bundle;)V

    .line 295
    if-eqz p1, :cond_0

    .line 296
    const-string v5, "ReadingActivity.addedFragments"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAddedFragments:Z

    .line 297
    const-string v5, "ReadingActivity.ratingInProgress"

    invoke-virtual {p1, v5, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    .line 298
    const-string v5, "ReadingActivity.ratingRequestCode"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingRequestCode:I

    .line 301
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 302
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040023

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 304
    .local v4, "view":Landroid/view/ViewGroup;
    const v5, 0x7f0e00ac

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 305
    .local v1, "colorFilter":Landroid/view/View;
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 307
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/BooksApplication;->hasTransitionBitmap()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 309
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_1

    .line 311
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 313
    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/app/ReadingActivity;->getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v5

    invoke-interface {v5, v7}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    .line 316
    .end local v0    # "actionBar":Landroid/support/v7/app/ActionBar;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->setupActionBar()V

    .line 317
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->setupStatusBar()V

    .line 318
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/app/ReadingActivity;->setContentView(Landroid/view/View;)V

    .line 320
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->supportsOpenGLES2()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 321
    const v5, 0x7f040076

    invoke-virtual {v2, v5, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 326
    .local v3, "pagesView":Landroid/view/View;
    invoke-virtual {v4, v3, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->prepareOpenGlViews()V

    .line 329
    .end local v3    # "pagesView":Landroid/view/View;
    :cond_3
    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 2
    .param p1, "featureId"    # I
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 393
    const-string v1, "ReadingActivity.reader"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 394
    .local v0, "readerFragment":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onMenuOpened()V

    .line 397
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->finish()V

    .line 337
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReadingActivity;->startActivity(Landroid/content/Intent;)V

    .line 339
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 486
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onPause()V

    .line 487
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->flushPageFlips()V

    .line 488
    return-void
.end method

.method public onRatingClick(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 4
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 526
    iget-boolean v3, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    if-eqz v3, :cond_0

    .line 565
    :goto_0
    return-void

    .line 529
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    .line 530
    .local v2, "volumeId":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v0, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 531
    .local v0, "accountName":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v1

    .line 532
    .local v1, "canonicalUrl":Ljava/lang/String;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    .line 533
    new-instance v3, Lcom/google/android/apps/books/app/ReadingActivity$1;

    invoke-direct {v3, p0, v2, v1}, Lcom/google/android/apps/books/app/ReadingActivity$1;-><init>(Lcom/google/android/apps/books/app/ReadingActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2, p0, v3}, Lcom/google/android/apps/books/eob/RatingHelper;->requestRatingInfo(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;)Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 351
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 352
    const-string v0, "ReadingActivity.addedFragments"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAddedFragments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 353
    const-string v0, "ReadingActivity.ratingInProgress"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingIsInProgress:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 354
    const-string v0, "ReadingActivity.ratingRequestCode"

    iget v1, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mRatingRequestCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 355
    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 423
    const-string v1, "ReadingActivity.reader"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 425
    .local v0, "readerFragment":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onSearchRequested()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onSelectedAccount(Landroid/accounts/Account;)V
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 456
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAddedFragments:Z

    if-nez v0, :cond_1

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    .line 458
    .local v11, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v11}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 461
    .local v4, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->loaderParamsFromIntent()Landroid/os/Bundle;

    move-result-object v3

    .line 462
    .local v3, "args":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 463
    const v0, 0x7f0e00ab

    const-class v1, Lcom/google/android/apps/books/app/ReaderFragment;

    const-string v2, "ReadingActivity.reader"

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReadingActivity;->createAndAddFragment(ILjava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 465
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 471
    :goto_0
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mAddedFragments:Z

    .line 477
    .end local v3    # "args":Landroid/os/Bundle;
    .end local v4    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v11    # "fm":Landroid/support/v4/app/FragmentManager;
    :goto_1
    return-void

    .line 468
    .restart local v3    # "args":Landroid/os/Bundle;
    .restart local v4    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .restart local v11    # "fm":Landroid/support/v4/app/FragmentManager;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity;->mFragmentCallbacks:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->onHomePressed()V

    goto :goto_0

    .line 474
    .end local v3    # "args":Landroid/os/Bundle;
    .end local v4    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v11    # "fm":Landroid/support/v4/app/FragmentManager;
    :cond_1
    sget-object v5, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_ORIENTATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    move-object v10, p0

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public onUserInteraction()V
    .locals 2

    .prologue
    .line 415
    const-string v1, "ReadingActivity.reader"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 416
    .local v0, "reader":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onUserInteraction()V

    .line 419
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 382
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onWindowFocusChanged(Z)V

    .line 384
    const-string v1, "ReadingActivity.reader"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 385
    .local v0, "reader":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onWindowFocusChanged(Z)V

    .line 389
    :cond_0
    return-void
.end method
