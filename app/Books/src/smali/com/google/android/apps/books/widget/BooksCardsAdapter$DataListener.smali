.class Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "BooksCardsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V
    .locals 0

    .prologue
    .line 605
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Lcom/google/android/apps/books/widget/BooksCardsAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter$1;

    .prologue
    .line 605
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V

    return-void
.end method

.method private updatePinViews(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 618
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    .line 619
    .local v0, "deviceIsConnected":Z
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 620
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/widget/BooksCardsAdapter;->updatePinViewForVolume(Ljava/lang/String;Z)V
    invoke-static {v4, v3, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->access$300(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Ljava/lang/String;Z)V

    goto :goto_0

    .line 622
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    :cond_0
    return-void
.end method


# virtual methods
.method public onLocalVolumeData(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 609
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;->updatePinViews(Ljava/util/Map;)V

    .line 610
    return-void
.end method

.method public onVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 614
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$DataListener;->updatePinViews(Ljava/util/Map;)V

    .line 615
    return-void
.end method
