.class Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;
.super Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;
.source "ReadingActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;",
        ">.FragmentCallbacks;",
        "Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReadingActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReadingActivity;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReadingActivity;Lcom/google/android/apps/books/app/ReadingActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReadingActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReadingActivity$1;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;-><init>(Lcom/google/android/apps/books/app/ReadingActivity;)V

    return-void
.end method


# virtual methods
.method public acceptNewPosition(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 162
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    const-string v2, "ReadingActivity.reader"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/ReadingActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment;

    .line 163
    .local v0, "reader":Lcom/google/android/apps/books/app/ReaderFragment;
    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/app/ReaderFragment;->acceptNewPosition(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;)V

    .line 166
    :cond_0
    return-void
.end method

.method public closeBook()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->finish()V

    .line 171
    return-void
.end method

.method public getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    # invokes: Lcom/google/android/apps/books/app/ReadingActivity;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->access$000(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v0

    return-object v0
.end method

.method public getSystemUi()Lcom/google/android/ublib/view/SystemUi;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    # getter for: Lcom/google/android/apps/books/app/ReadingActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->access$300(Lcom/google/android/apps/books/app/ReadingActivity;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v0

    return-object v0
.end method

.method public getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/ReadingActivity;->getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v0

    return-object v0
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadingActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/books/app/BooksApplication;->buildInternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 87
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/app/BookOpeningFlags;->setExtras(Landroid/content/Intent;)V

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/ReadingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onHomePressed()V
    .locals 3

    .prologue
    .line 175
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    const-class v2, Lcom/google/android/apps/books/app/BooksActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    .local v0, "homeIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/ReadingActivity;->startActivity(Landroid/content/Intent;)V

    .line 180
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logClosedBookUsingUpButton()V

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->finish()V

    .line 183
    return-void
.end method

.method public populateReaderActionBar(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "author"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v3, 0x8

    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 118
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    const v1, 0x7f0f005a

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeActionContentDescription(I)V

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/app/ReadingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->setActionBarDisplayOptions(II)V

    .line 129
    :cond_0
    return-void
.end method

.method public restartCurrentActivity(Z)V
    .locals 2
    .param p1, "showDisplaySettingsOnRecreate"    # Z

    .prologue
    .line 109
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->finish()V

    .line 110
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 111
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "books:showDisplaySettings"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/ReadingActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    return-void
.end method

.method public setActionBarElevation(F)V
    .locals 1
    .param p1, "elevation"    # F

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 134
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/StyleUtils;->setActionBarElevation(Landroid/support/v7/app/ActionBar;F)V

    .line 135
    return-void
.end method

.method public showNewPositionAvailableDialog(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p4, "chapterTitle"    # Ljava/lang/String;
    .param p5, "pageTitle"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;-><init>(Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 157
    return-void
.end method

.method public startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "helpContext"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "screenshot"    # Landroid/graphics/Bitmap;

    .prologue
    .line 204
    invoke-static {p3}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/google/android/apps/books/app/HelpUtils;->startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;Lcom/google/android/apps/books/util/Config;)V

    .line 206
    return-void
.end method

.method public temporarilyOverrideWindowAccessibilityAnnouncements(Ljava/lang/String;)V
    .locals 4
    .param p1, "stringToSay"    # Ljava/lang/String;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    # setter for: Lcom/google/android/apps/books/app/ReadingActivity;->mAccessibilityOverrideString:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReadingActivity;->access$102(Lcom/google/android/apps/books/app/ReadingActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/google/android/apps/books/app/ReadingActivity;->mTimestampStartOfAccessibilityOverride:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/app/ReadingActivity;->access$202(Lcom/google/android/apps/books/app/ReadingActivity;J)J

    .line 194
    return-void
.end method

.method public updateTheme(Ljava/lang/String;)Z
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingActivity;->getCurrentReaderTheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->shouldUseDarkTheme(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderTheme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/ReaderUtils;->shouldUseDarkTheme(Ljava/lang/String;)Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 101
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->restartCurrentActivity(Z)V

    .line 104
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
