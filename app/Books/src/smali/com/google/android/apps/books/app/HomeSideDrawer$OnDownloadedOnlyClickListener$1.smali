.class Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;
.super Ljava/lang/Object;
.source "HomeSideDrawer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;

.field final synthetic val$activity:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;->this$1:Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;->val$activity:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 282
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;->val$activity:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setDownloadedOnlyMode(Z)V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;->val$activity:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;->val$activity:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener$1;->this$1:Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;

    iget-object v0, v0, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # invokes: Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDownloadedOnlyViews()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$100(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    goto :goto_0
.end method
