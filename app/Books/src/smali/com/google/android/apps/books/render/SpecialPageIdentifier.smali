.class public Lcom/google/android/apps/books/render/SpecialPageIdentifier;
.super Ljava/lang/Object;
.source "SpecialPageIdentifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;
    }
.end annotation


# instance fields
.field public final margin:I

.field public final type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;I)V
    .locals 0
    .param p1, "type"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;
    .param p2, "margin"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    .line 39
    iput p2, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    .line 40
    return-void
.end method

.method public static makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;
    .locals 2
    .param p0, "margin"    # I

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    sget-object v1, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;I)V

    return-object v0
.end method

.method public static makePreviewGap()Lcom/google/android/apps/books/render/SpecialPageIdentifier;
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    sget-object v1, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->PREVIEW_GAP:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;I)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p0, p1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v1

    .line 72
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 73
    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 75
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .line 77
    .local v0, "other":Lcom/google/android/apps/books/render/SpecialPageIdentifier;
    iget v3, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    iget v4, v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 78
    goto :goto_0

    .line 79
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    iget-object v4, v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 80
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 61
    const/16 v0, 0x1f

    .line 62
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 63
    .local v1, "result":I
    iget v2, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    add-int/lit8 v1, v2, 0x1f

    .line 64
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 65
    return v1

    .line 64
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 52
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "type"

    iget-object v3, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 53
    .local v0, "helper":Lcom/google/common/base/Objects$ToStringHelper;
    iget-object v1, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    sget-object v2, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    if-ne v1, v2, :cond_0

    .line 54
    const-string v1, "margin"

    iget v2, p0, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    .line 56
    :cond_0
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
