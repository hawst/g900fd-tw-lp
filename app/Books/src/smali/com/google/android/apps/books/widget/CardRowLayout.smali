.class public Lcom/google/android/apps/books/widget/CardRowLayout;
.super Landroid/widget/LinearLayout;
.source "CardRowLayout.java"


# instance fields
.field protected mAnimationEnabled:Z

.field private mColumnCount:I

.field private mExtraInnerPadding:I

.field protected final mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mColumnCount:I

    .line 31
    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mExtraInnerPadding:I

    .line 32
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mAnimationEnabled:Z

    .line 34
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->init()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mColumnCount:I

    .line 31
    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mExtraInnerPadding:I

    .line 32
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mAnimationEnabled:Z

    .line 34
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->init()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mColumnCount:I

    .line 31
    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mExtraInnerPadding:I

    .line 32
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mAnimationEnabled:Z

    .line 34
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->init()V

    .line 39
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 52
    const/16 v0, 0x50

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CardRowLayout;->setGravity(I)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mExtraInnerPadding:I

    .line 55
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mColumnCount:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getChildCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mColumnCount:I

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 120
    const/4 v10, 0x0

    .line 122
    .local v10, "extraPixels":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getPaddingLeft()I

    move-result v11

    .line 123
    .local v11, "previousChildRight":I
    const/4 v6, 0x0

    .local v6, "childIndex":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getChildCount()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 124
    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/widget/CardRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 125
    .local v1, "child":Landroid/view/View;
    if-gez v6, :cond_0

    const/4 v9, 0x1

    .line 126
    .local v9, "extraPixel":I
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 127
    .local v5, "childHeight":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 128
    .local v8, "childWidth":I
    iget v7, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mExtraInnerPadding:I

    .line 129
    .local v7, "childMargin":I
    add-int v2, v11, v7

    .line 130
    .local v2, "left":I
    add-int v0, v2, v8

    add-int v4, v0, v9

    .line 131
    .local v4, "right":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    const/4 v3, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    .line 132
    add-int v11, v4, v7

    .line 123
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 125
    .end local v2    # "left":I
    .end local v4    # "right":I
    .end local v5    # "childHeight":I
    .end local v7    # "childMargin":I
    .end local v8    # "childWidth":I
    .end local v9    # "extraPixel":I
    :cond_0
    const/4 v9, 0x0

    goto :goto_1

    .line 134
    .end local v1    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 71
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 72
    .local v8, "width":I
    if-nez v8, :cond_0

    .line 73
    new-instance v11, Ljava/lang/IllegalStateException;

    const-string v12, "CardRowLayout requires non-zero width"

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 76
    :cond_0
    const/4 v6, 0x0

    .line 78
    .local v6, "maxChildHeight":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getChildCount()I

    move-result v1

    .line 84
    .local v1, "childCount":I
    if-lez v1, :cond_1

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getPaddingLeft()I

    move-result v11

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getPaddingRight()I

    move-result v12

    add-int/2addr v11, v12

    sub-int v9, v8, v11

    .line 86
    .local v9, "widthForChildren":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getColumnCount()I

    move-result v11

    div-int v4, v9, v11

    .line 89
    .local v4, "childWidth":I
    const/4 v3, 0x0

    .local v3, "childIndex":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 90
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/CardRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 96
    .local v0, "child":Landroid/view/View;
    iget v11, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mExtraInnerPadding:I

    mul-int/lit8 v2, v11, 0x2

    .line 97
    .local v2, "childHorizontalMargins":I
    sub-int v10, v4, v2

    .line 98
    .local v10, "widthWithoutMargins":I
    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 100
    .local v5, "childWidthSpec":I
    invoke-virtual {v0, v5, p2}, Landroid/view/View;->measure(II)V

    .line 102
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-static {v0}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getVerticalMargins(Landroid/view/View;)I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v6, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 89
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childHorizontalMargins":I
    .end local v3    # "childIndex":I
    .end local v4    # "childWidth":I
    .end local v5    # "childWidthSpec":I
    .end local v9    # "widthForChildren":I
    .end local v10    # "widthWithoutMargins":I
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getPaddingTop()I

    move-result v11

    add-int/2addr v11, v6

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/CardRowLayout;->getPaddingBottom()I

    move-result v12

    add-int v7, v11, v12

    .line 108
    .local v7, "paddedHeight":I
    invoke-virtual {p0, v8, v7}, Lcom/google/android/apps/books/widget/CardRowLayout;->setMeasuredDimension(II)V

    .line 109
    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/apps/books/widget/CardRowLayout;->mColumnCount:I

    .line 63
    return-void
.end method
