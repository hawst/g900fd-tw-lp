.class public Lcom/google/android/apps/books/model/LocalSessionKey;
.super Ljava/lang/Object;
.source "LocalSessionKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/model/SessionKeyId;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mId:Lcom/google/android/apps/books/model/SessionKeyId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mKey:Lcom/google/android/apps/books/model/SessionKey;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 1
    .param p2, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<TT;>;"
    .local p1, "id":Lcom/google/android/apps/books/model/SessionKeyId;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "missing id"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/SessionKeyId;

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mId:Lcom/google/android/apps/books/model/SessionKeyId;

    .line 22
    const-string v0, "missing key"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/SessionKey;

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mKey:Lcom/google/android/apps/books/model/SessionKey;

    .line 23
    return-void
.end method

.method public static create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            ">(TT;",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "id":Lcom/google/android/apps/books/model/SessionKeyId;, "TT;"
    new-instance v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/model/LocalSessionKey;-><init>(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)V

    return-object v0
.end method

.method public static load(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 2
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            ">(TT;",
            "Lcom/google/android/apps/books/model/BooksDataStore;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "identifier":Lcom/google/android/apps/books/model/SessionKeyId;, "TT;"
    const/4 v1, 0x0

    .line 35
    if-nez p0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-object v1

    .line 38
    :cond_1
    invoke-interface {p0, p1}, Lcom/google/android/apps/books/model/SessionKeyId;->load(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    .line 39
    .local v0, "key":Lcom/google/android/apps/books/model/SessionKey;
    if-eqz v0, :cond_0

    .line 40
    invoke-static {p0, v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<TT;>;"
    const/4 v1, 0x0

    .line 55
    instance-of v2, p1, Lcom/google/android/apps/books/model/LocalSessionKey;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 57
    .local v0, "o":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mId:Lcom/google/android/apps/books/model/SessionKeyId;

    iget-object v3, v0, Lcom/google/android/apps/books/model/LocalSessionKey;->mId:Lcom/google/android/apps/books/model/SessionKeyId;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mKey:Lcom/google/android/apps/books/model/SessionKey;

    iget-object v3, v0, Lcom/google/android/apps/books/model/LocalSessionKey;->mKey:Lcom/google/android/apps/books/model/SessionKey;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 59
    .end local v0    # "o":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :cond_0
    return v1
.end method

.method public getId()Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mId:Lcom/google/android/apps/books/model/SessionKeyId;

    return-object v0
.end method

.method public getKey()Lcom/google/android/apps/books/model/SessionKey;
    .locals 1

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mKey:Lcom/google/android/apps/books/model/SessionKey;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 64
    .local p0, "this":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<TT;>;"
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mId:Lcom/google/android/apps/books/model/SessionKeyId;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSessionKey;->mKey:Lcom/google/android/apps/books/model/SessionKey;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
