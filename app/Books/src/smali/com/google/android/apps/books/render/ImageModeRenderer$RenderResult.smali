.class Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;
.super Ljava/lang/Object;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderResult"
.end annotation


# instance fields
.field final bitmap:Landroid/graphics/Bitmap;

.field final error:Ljava/lang/Exception;

.field final page:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final request:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Landroid/graphics/Bitmap;Ljava/lang/Exception;)V
    .locals 5
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;
    .param p5, "error"    # Ljava/lang/Exception;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 680
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 681
    const-string v2, "missing position"

    invoke-static {p2, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/DevicePageRendering;

    iput-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 682
    const-string v2, "missing consumer"

    invoke-static {p3, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    iput-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->request:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .line 683
    iput-object p4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->bitmap:Landroid/graphics/Bitmap;

    .line 684
    iput-object p5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->error:Ljava/lang/Exception;

    .line 687
    if-eqz p4, :cond_0

    move v0, v3

    .line 688
    .local v0, "hasBitmap":Z
    :goto_0
    if-eqz p5, :cond_1

    move v1, v3

    .line 689
    .local v1, "hasError":Z
    :goto_1
    if-eq v0, v1, :cond_2

    move v2, v3

    :goto_2
    const-string v3, "must specify exactly one of bitmap or error"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 691
    return-void

    .end local v0    # "hasBitmap":Z
    .end local v1    # "hasError":Z
    :cond_0
    move v0, v4

    .line 687
    goto :goto_0

    .restart local v0    # "hasBitmap":Z
    :cond_1
    move v1, v4

    .line 688
    goto :goto_1

    .restart local v1    # "hasError":Z
    :cond_2
    move v2, v4

    .line 689
    goto :goto_2
.end method
