.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;
.super Lcom/google/android/apps/books/data/BasePendingAction;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeManifestInternal(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$fromServer:Z

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$resourceTypes:Ljava/util/Set;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$resourceTypes:Ljava/util/Set;

    iput-boolean p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$fromServer:Z

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Lcom/google/android/apps/books/data/BasePendingAction;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$resourceTypes:Ljava/util/Set;

    iget-boolean v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$fromServer:Z

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeManifestInternal(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 538
    return-void
.end method
