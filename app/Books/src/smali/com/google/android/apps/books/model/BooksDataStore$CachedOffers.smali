.class public interface abstract Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
.super Ljava/lang/Object;
.source "BooksDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CachedOffers"
.end annotation


# virtual methods
.method public abstract getLastModifiedMillis()J
.end method

.method public abstract getOffers()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation
.end method
