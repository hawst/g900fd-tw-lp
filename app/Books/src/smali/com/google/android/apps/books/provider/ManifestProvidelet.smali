.class Lcom/google/android/apps/books/provider/ManifestProvidelet;
.super Lcom/google/android/apps/books/provider/NotifyingProvidelet;
.source "ManifestProvidelet.java"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V
    .locals 0
    .param p1, "notifier"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
    .param p2, "dbHelper"    # Lcom/google/android/apps/books/provider/database/BooksDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;",
            "Lcom/google/android/apps/books/provider/database/BooksDatabase;",
            "Lcom/google/android/apps/books/util/pool/Pool",
            "<",
            "Lcom/google/android/apps/books/util/SelectionBuilder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p3, "builderPool":Lcom/google/android/apps/books/util/pool/Pool;, "Lcom/google/android/apps/books/util/pool/Pool<Lcom/google/android/apps/books/util/SelectionBuilder;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    .line 47
    return-void
.end method

.method private augmentBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "sectionsTable"    # Ljava/lang/String;
    .param p5, "chaptersTable"    # Ljava/lang/String;
    .param p6, "resourcesTable"    # Ljava/lang/String;
    .param p7, "pagesTable"    # Ljava/lang/String;
    .param p8, "resourceResourcesTable"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 141
    sparse-switch p2, :sswitch_data_0

    .line 239
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 143
    :sswitch_0
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;

    move-result-object v0

    .line 144
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
    invoke-virtual {p1, p4}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 242
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
    :goto_0
    return-void

    .line 150
    :sswitch_1
    invoke-static {p3, v6}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;

    move-result-object v0

    .line 151
    .restart local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
    invoke-virtual {p1, p4}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "segment_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->sectionId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto :goto_0

    .line 158
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
    :sswitch_2
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;

    move-result-object v0

    .line 159
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;
    invoke-virtual {p1, p5}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto :goto_0

    .line 165
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;
    :sswitch_3
    invoke-static {p3, v6}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;

    move-result-object v0

    .line 166
    .restart local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;
    invoke-virtual {p1, p5}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "chapter_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;->chapterId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto :goto_0

    .line 173
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;
    :sswitch_4
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;

    move-result-object v0

    .line 174
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    invoke-virtual {p1, p6}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 180
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    :sswitch_5
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;

    move-result-object v0

    .line 181
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;
    const-string v1, "segment_resources"

    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 187
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;
    :sswitch_6
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;

    move-result-object v0

    .line 188
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;
    const-string v1, "resource_resources"

    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 194
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;
    :sswitch_7
    invoke-static {p3, v6}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;

    move-result-object v0

    .line 195
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    invoke-virtual {p1, p6}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "resource_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;->resId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 202
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Resources$Info;
    :sswitch_8
    invoke-static {p3, v6}, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;

    move-result-object v0

    .line 203
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;
    const-string v1, "segment_resources"

    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "resource_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->resourceId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "segment_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->segmentId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 212
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;
    :sswitch_9
    invoke-static {p3, v6}, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;

    move-result-object v0

    .line 213
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;
    invoke-virtual {p1, p8}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "resource_resources.account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "resource_resources.volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "resource_resources.compound_res_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;->compoundResourceId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 224
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$ResourceResources$Info;
    :sswitch_a
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;

    move-result-object v0

    .line 225
    .local v0, "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    invoke-virtual {p1, p7}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 231
    .end local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    :sswitch_b
    invoke-static {p3, v6}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;

    move-result-object v0

    .line 232
    .restart local v0    # "info":Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;
    invoke-virtual {p1, p7}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "account_name=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->accountName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "volume_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->volumeId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v1

    const-string v2, "page_id=?"

    new-array v3, v6, [Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/provider/BooksContract$Pages$Info;->pageId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto/16 :goto_0

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_1
        0xcd -> :sswitch_2
        0xce -> :sswitch_3
        0x12c -> :sswitch_4
        0x12d -> :sswitch_7
        0x145 -> :sswitch_6
        0x146 -> :sswitch_9
        0x15e -> :sswitch_5
        0x15f -> :sswitch_8
        0x190 -> :sswitch_a
        0x191 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method protected deleteWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 120
    const-string v4, "segments"

    const-string v5, "chapters"

    const-string v6, "resources"

    const-string v7, "pages"

    const-string v8, "resource_resources"

    move-object v0, p0

    move-object v1, p4

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->augmentBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p4, p1}, Lcom/google/android/apps/books/util/SelectionBuilder;->delete(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    return v0
.end method

.method protected insertWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v8, 0x0

    .line 51
    sparse-switch p2, :sswitch_data_0

    .line 96
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v6}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v6

    .line 53
    :sswitch_0
    const-string v6, "segment_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 54
    .local v5, "sectionId":Ljava/lang/String;
    invoke-static {p3, v5}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 59
    .local v2, "newUri":Landroid/net/Uri;
    const-string v6, "chapter_id"

    const-string v7, ""

    invoke-virtual {p4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v6, "segments"

    invoke-virtual {p1, v6, v8, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 93
    .end local v5    # "sectionId":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 64
    .end local v2    # "newUri":Landroid/net/Uri;
    :sswitch_1
    const-string v6, "chapter_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "chapterId":Ljava/lang/String;
    invoke-static {p3, v0}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->buildChapterUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 66
    .restart local v2    # "newUri":Landroid/net/Uri;
    const-string v6, "chapters"

    invoke-virtual {p1, v6, v8, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 70
    .end local v0    # "chapterId":Ljava/lang/String;
    .end local v2    # "newUri":Landroid/net/Uri;
    :sswitch_2
    const-string v6, "resource_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "resId":Ljava/lang/String;
    invoke-static {p3, v4}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 72
    .restart local v2    # "newUri":Landroid/net/Uri;
    const-string v6, "resources"

    invoke-virtual {p1, v6, v8, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 76
    .end local v2    # "newUri":Landroid/net/Uri;
    .end local v4    # "resId":Ljava/lang/String;
    :sswitch_3
    const-string v6, "resource_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 77
    .restart local v4    # "resId":Ljava/lang/String;
    const-string v6, "segment_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .restart local v5    # "sectionId":Ljava/lang/String;
    invoke-static {p3, v4, v5}, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;->buildUri(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 79
    .restart local v2    # "newUri":Landroid/net/Uri;
    const-string v6, "segment_resources"

    invoke-virtual {p1, v6, v8, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 83
    .end local v2    # "newUri":Landroid/net/Uri;
    .end local v4    # "resId":Ljava/lang/String;
    .end local v5    # "sectionId":Ljava/lang/String;
    :sswitch_4
    const-string v6, "compound_res_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "compoundRes":Ljava/lang/String;
    invoke-static {p3, v1}, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 86
    .restart local v2    # "newUri":Landroid/net/Uri;
    const-string v6, "resource_resources"

    invoke-virtual {p1, v6, v8, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 90
    .end local v1    # "compoundRes":Ljava/lang/String;
    .end local v2    # "newUri":Landroid/net/Uri;
    :sswitch_5
    const-string v6, "page_id"

    invoke-virtual {p4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 91
    .local v3, "pageId":Ljava/lang/String;
    invoke-static {p3, v3}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 92
    .restart local v2    # "newUri":Landroid/net/Uri;
    const-string v6, "pages"

    invoke-virtual {p1, v6, v8, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 51
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xcd -> :sswitch_1
        0x12c -> :sswitch_2
        0x145 -> :sswitch_4
        0x15e -> :sswitch_3
        0x190 -> :sswitch_5
    .end sparse-switch
.end method

.method protected queryWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/util/SelectionBuilder;)Landroid/database/Cursor;
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 104
    const-string v4, "view_segments"

    const-string v5, "chapters"

    const-string v6, "view_resources"

    const-string v7, "view_pages"

    const-string v8, "resource_resources INNER JOIN resources ON (resource_resources.account_name = resources.account_name AND resource_resources.volume_id = resources.volume_id AND resource_resources.referenced_res_id = resources.resource_id)"

    move-object v0, p0

    move-object v1, p6

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->augmentBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p6

    move-object v1, p1

    move-object v2, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/util/SelectionBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected updateWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "values"    # Landroid/content/ContentValues;
    .param p5, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 112
    const-string v4, "segments"

    const-string v5, "chapters"

    const-string v6, "resources"

    const-string v7, "pages"

    const-string v8, "resource_resources"

    move-object v0, p0

    move-object v1, p5

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->augmentBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p5, p1, p4}, Lcom/google/android/apps/books/util/SelectionBuilder;->update(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method
