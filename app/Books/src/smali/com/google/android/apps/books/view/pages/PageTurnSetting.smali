.class Lcom/google/android/apps/books/view/pages/PageTurnSetting;
.super Ljava/lang/Object;
.source "PageTurnSetting.java"


# static fields
.field private static final black:[I

.field private static final sepia:[I

.field private static final white:[I


# instance fields
.field final mEmptyBitmapDark:Landroid/graphics/Bitmap;

.field final mEmptyBitmapLight:Landroid/graphics/Bitmap;

.field final mEmptyBitmapSepia:Landroid/graphics/Bitmap;

.field mLoadingBitmap:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-array v0, v3, [I

    const-string v1, "0"

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->white:[I

    .line 17
    new-array v0, v3, [I

    const-string v1, "1"

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->black:[I

    .line 18
    new-array v0, v3, [I

    const-string v1, "2"

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->sepia:[I

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->white:[I

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mEmptyBitmapLight:Landroid/graphics/Bitmap;

    .line 20
    sget-object v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->black:[I

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mEmptyBitmapDark:Landroid/graphics/Bitmap;

    .line 21
    sget-object v0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->sepia:[I

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v2, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mEmptyBitmapSepia:Landroid/graphics/Bitmap;

    .line 22
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    return-void
.end method
