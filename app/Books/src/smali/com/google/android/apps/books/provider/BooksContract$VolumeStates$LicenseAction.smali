.class public final enum Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
.super Ljava/lang/Enum;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract$VolumeStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LicenseAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

.field public static final enum NONE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

.field public static final enum READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

.field public static final enum RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;


# instance fields
.field private final mDbValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2909
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->NONE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .line 2913
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    const-string v1, "READ"

    const-string v2, "Read"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .line 2918
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    const-string v1, "RELEASE"

    const-string v2, "Release"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .line 2904
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->NONE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "dbValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2922
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2923
    iput-object p3, p0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->mDbValue:Ljava/lang/String;

    .line 2924
    return-void
.end method

.method public static fromDbValue(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    .locals 7
    .param p0, "dbValue"    # Ljava/lang/String;

    .prologue
    .line 2931
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->values()[Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v1

    .local v1, "arr$":[Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 2932
    .local v0, "action":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    invoke-virtual {v0}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->getDbValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2939
    .end local v0    # "action":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    :goto_1
    return-object v0

    .line 2931
    .restart local v0    # "action":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2936
    .end local v0    # "action":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    :cond_1
    const-string v4, "BooksContract"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2937
    const-string v4, "BooksContract"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected license action "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2939
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2904
    const-class v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    .locals 1

    .prologue
    .line 2904
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    return-object v0
.end method


# virtual methods
.method public getDbValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2927
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->mDbValue:Ljava/lang/String;

    return-object v0
.end method
