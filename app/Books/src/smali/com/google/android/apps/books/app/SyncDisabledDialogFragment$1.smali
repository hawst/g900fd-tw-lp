.class Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;
.super Ljava/lang/Object;
.source "SyncDisabledDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x0

    .line 55
    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 56
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 57
    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 59
    .local v0, "account":Landroid/accounts/Account;
    new-instance v2, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;-><init>(Lcom/google/android/apps/books/sync/SyncController;Z)V

    new-array v3, v5, [Ljava/lang/Void;

    invoke-static {v2, v3}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 61
    const v2, 0x7f0f00ee

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 64
    iget-object v2, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;

    # invokes: Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->markHaveNagged()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;->access$000(Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;)V

    .line 66
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    return-void
.end method
