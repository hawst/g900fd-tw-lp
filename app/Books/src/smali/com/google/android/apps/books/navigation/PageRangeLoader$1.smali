.class Lcom/google/android/apps/books/navigation/PageRangeLoader$1;
.super Ljava/lang/Object;
.source "PageRangeLoader.java"

# interfaces
.implements Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

.field final synthetic val$off:I

.field final synthetic val$page:Lcom/google/android/apps/books/navigation/SnapshottingPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;ILcom/google/android/apps/books/navigation/SnapshottingPage;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    iput p2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$off:I

    iput-object p3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x1

    return v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mSnapshotRequested:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$002(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$100(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$200(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$off:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->needsSnapshot()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$off:I

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$300(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$400(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 356
    const-string v0, "PageRangeLoader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "PageRangeLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Snapshotting page "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$off:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->val$page:Lcom/google/android/apps/books/navigation/SnapshottingPage;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->snapshot()V

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$500(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    .line 362
    return-void
.end method
