.class Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$1;
.super Ljava/lang/Object;
.source "GridRowInvalidationManager.java"

# interfaces
.implements Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 3

    .prologue
    .line 35
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mScrollingViews:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$000(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 36
    .local v1, "view":Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-virtual {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 37
    const/4 v2, 0x1

    .line 40
    .end local v1    # "view":Lcom/google/android/ublib/widget/AbsWarpListView;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
