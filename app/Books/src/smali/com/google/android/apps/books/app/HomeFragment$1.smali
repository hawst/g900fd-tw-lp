.class Lcom/google/android/apps/books/app/HomeFragment$1;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 357
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_RECENT_HEADER_SEE_ALL:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 359
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const-string v3, "mylibrary"

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->onUserSelectedViewMode(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/HomeFragment;->access$200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    .line 360
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/HomeFragment;->access$102(Lcom/google/android/apps/books/app/HomeFragment;Z)Z

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$700(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/LibraryFilter;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    if-eq v2, v3, :cond_0

    .line 362
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getMyLibraryFilterSource()Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->getPossibleFilters()Ljava/util/List;

    move-result-object v1

    .line 363
    .local v1, "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    sget-object v2, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 364
    .local v0, "allBooksPos":I
    if-ltz v0, :cond_1

    .line 365
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mSelectedLibraryFilter:I
    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$802(Lcom/google/android/apps/books/app/HomeFragment;I)I

    .line 366
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    sget-object v3, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/HomeFragment;->access$702(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/LibraryFilter;)Lcom/google/android/apps/books/app/LibraryFilter;

    .line 367
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->setOverrideUploadsComparator(Lcom/google/android/apps/books/app/LibraryComparator;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/HomeFragment;->access$900(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 368
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$1000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onHomeFragmentLibraryFilterChanged()V

    .line 373
    .end local v0    # "allBooksPos":I
    .end local v1    # "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$1;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->moveHomeViewToTop()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$1100(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 374
    return-void

    .line 369
    .restart local v0    # "allBooksPos":I
    .restart local v1    # "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    :cond_1
    const-string v2, "HomeFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 370
    const-string v2, "HomeFragment"

    const-string v3, "Could not find the all books filter !?"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
