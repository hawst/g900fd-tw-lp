.class abstract Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "PageDecorator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 2323
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 2323
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    return-void
.end method


# virtual methods
.method abstract draw(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Rect;)V
.end method

.method drawOnPageCanvas(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "pageSize"    # Landroid/graphics/Point;
    .param p4, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .prologue
    .line 2334
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    .line 2336
    .local v0, "checkpoint":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 2337
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v3

    invoke-interface {v3, p2, p3}, Lcom/google/android/apps/books/render/Renderer;->rendererCoordinatesToPagePointsMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 2339
    .local v2, "transform":Landroid/graphics/Matrix;
    if-eqz v2, :cond_0

    .line 2340
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 2348
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectAfterTransform:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 2349
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectAfterTransform:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 2350
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectF:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/RectF;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 2351
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReuseableInverseMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3400(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 2352
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReuseableInverseMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3400(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Matrix;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectF:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 2353
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectF:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/RectF;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 2359
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v1

    .line 2361
    .local v1, "clipRect":Landroid/graphics/Rect;
    invoke-virtual {p0, p1, p2, p4, v1}, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->draw(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Rect;)V

    .line 2363
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2364
    return-void

    .line 2355
    .end local v1    # "clipRect":Landroid/graphics/Rect;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    goto :goto_0
.end method
