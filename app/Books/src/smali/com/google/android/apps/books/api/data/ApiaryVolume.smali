.class public Lcom/google/android/apps/books/api/data/ApiaryVolume;
.super Ljava/lang/Object;
.source "ApiaryVolume.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;,
        Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;,
        Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;,
        Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;,
        Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;,
        Lcom/google/android/apps/books/api/data/ApiaryVolume$ImageLinks;,
        Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;
    }
.end annotation


# instance fields
.field public accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public etag:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public layerInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 212
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "eolm"

    iget-object v2, p0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-boolean v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->explicitOfflineLicenseManagement:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
