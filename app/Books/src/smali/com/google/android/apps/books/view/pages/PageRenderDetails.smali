.class public Lcom/google/android/apps/books/view/pages/PageRenderDetails;
.super Ljava/lang/Object;
.source "PageRenderDetails.java"


# instance fields
.field public final centerBitmapOnPage:Z

.field public final displaySize:Landroid/graphics/Point;

.field public final hasBookmark:Z

.field public final transitionStartTime:J


# direct methods
.method public constructor <init>(ZZLandroid/graphics/Point;J)V
    .locals 0
    .param p1, "hasBookmark"    # Z
    .param p2, "centerBitmapOnPage"    # Z
    .param p3, "displaySize"    # Landroid/graphics/Point;
    .param p4, "transitionStartTime"    # J

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->hasBookmark:Z

    .line 30
    iput-boolean p2, p0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->centerBitmapOnPage:Z

    .line 31
    iput-object p3, p0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->displaySize:Landroid/graphics/Point;

    .line 32
    iput-wide p4, p0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->transitionStartTime:J

    .line 33
    return-void
.end method
