.class public Lcom/google/android/apps/books/model/SearchMatchTextRange;
.super Ljava/lang/Object;
.source "SearchMatchTextRange.java"

# interfaces
.implements Lcom/google/android/apps/books/model/PaintableTextRange;


# static fields
.field private static mIdCounter:J


# instance fields
.field private final mColor:I

.field private final mMatchIndex:Ljava/lang/String;

.field private final mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mIdCounter:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;I)V
    .locals 0
    .param p1, "matchIndex"    # Ljava/lang/String;
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "color"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mMatchIndex:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 26
    iput p3, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mColor:I

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 61
    instance-of v2, p1, Lcom/google/android/apps/books/model/SearchMatchTextRange;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 62
    check-cast v0, Lcom/google/android/apps/books/model/SearchMatchTextRange;

    .line 63
    .local v0, "otherRange":Lcom/google/android/apps/books/model/SearchMatchTextRange;
    iget-object v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mMatchIndex:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mMatchIndex:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-object v3, v0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mColor:I

    iget v3, v0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mColor:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 67
    .end local v0    # "otherRange":Lcom/google/android/apps/books/model/SearchMatchTextRange;
    :cond_0
    return v1
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mColor:I

    return v0
.end method

.method public getPaintableRangeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mMatchIndex:Ljava/lang/String;

    return-object v0
.end method

.method public getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mMatchIndex:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mPositionRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mColor:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "matchIndex"

    iget-object v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mMatchIndex:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "color"

    iget v2, p0, Lcom/google/android/apps/books/model/SearchMatchTextRange;->mColor:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
