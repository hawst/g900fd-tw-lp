.class public interface abstract Lcom/google/android/apps/books/render/PageCanvasPainter;
.super Ljava/lang/Object;
.source "PageCanvasPainter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;
    }
.end annotation


# virtual methods
.method public abstract minimumMarginNoteIconSize()Landroid/graphics/Point;
.end method

.method public abstract paintHighlightRects(Landroid/graphics/Canvas;Ljava/lang/String;ILcom/google/android/apps/books/widget/Walker;ZLandroid/graphics/Rect;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;Z",
            "Landroid/graphics/Rect;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract paintMarginNoteIcon(Landroid/graphics/Canvas;Ljava/lang/String;Landroid/graphics/Rect;IZ)V
.end method

.method public abstract paintMediaViews(Landroid/graphics/Canvas;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract paintPressedVolumeAnnotationRect(Landroid/graphics/Canvas;Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;)V
.end method
