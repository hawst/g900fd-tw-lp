.class abstract Lcom/google/android/apps/books/render/BasicReaderController$Request;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "Request"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/render/BasicReaderController$Request;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final mExternalRequestId:I

.field private final mPriority:I

.field final mReposted:Z

.field final mTaskRequestId:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V
    .locals 0
    .param p2, "requestId"    # I
    .param p3, "externalRequestId"    # I
    .param p4, "reposted"    # Z
    .param p5, "priority"    # I

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mTaskRequestId:I

    .line 187
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mExternalRequestId:I

    .line 188
    iput-boolean p4, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mReposted:Z

    .line 189
    iput p5, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mPriority:I

    .line 190
    return-void
.end method

.method private relativeOrder()I
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mReposted:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getOrder()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getOrder()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/render/BasicReaderController$Request;)I
    .locals 3
    .param p1, "another"    # Lcom/google/android/apps/books/render/BasicReaderController$Request;

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getPriority()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->getPriority()I

    move-result v2

    sub-int v0, v1, v2

    .line 223
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->relativeOrder()I

    move-result v1

    invoke-direct {p1}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->relativeOrder()I

    move-result v2

    sub-int v0, v1, v2

    .line 226
    :cond_0
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 160
    check-cast p1, Lcom/google/android/apps/books/render/BasicReaderController$Request;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/BasicReaderController$Request;->compareTo(Lcom/google/android/apps/books/render/BasicReaderController$Request;)I

    move-result v0

    return v0
.end method

.method getOrder()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mTaskRequestId:I

    return v0
.end method

.method getOriginalOrder()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mExternalRequestId:I

    return v0
.end method

.method getPriority()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$Request;->mPriority:I

    return v0
.end method
