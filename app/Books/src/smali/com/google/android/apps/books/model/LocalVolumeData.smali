.class public interface abstract Lcom/google/android/apps/books/model/LocalVolumeData;
.super Ljava/lang/Object;
.source "LocalVolumeData.java"


# virtual methods
.method public abstract getFitWidth()Z
.end method

.method public abstract getForceDownload()Z
.end method

.method public abstract getLastLocalAccess()J
.end method

.method public abstract getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
.end method

.method public abstract getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
.end method

.method public abstract getLineHeight()F
.end method

.method public abstract getPinned()Z
.end method

.method public abstract getTextZoom()F
.end method

.method public abstract getTimestamp()J
.end method

.method public abstract hasOfflineLicense()Z
.end method
