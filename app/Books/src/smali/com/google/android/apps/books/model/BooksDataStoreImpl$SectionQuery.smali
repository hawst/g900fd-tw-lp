.class interface abstract Lcom/google/android/apps/books/model/BooksDataStoreImpl$SectionQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "SectionQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2015
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "segment_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "segment_order"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "start_position"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "page_count"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "content_status"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "session_key_version"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "root_key_version"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "session_key_blob"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "fixed_layout_version"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "fixed_viewport_width"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "fixed_viewport_height"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "remote_url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SectionQuery;->PROJECTION:[Ljava/lang/String;

    .line 2033
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SectionQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SectionQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method
