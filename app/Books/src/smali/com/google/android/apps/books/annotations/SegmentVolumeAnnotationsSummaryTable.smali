.class public Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;
.super Ljava/lang/Object;
.source "SegmentVolumeAnnotationsSummaryTable.java"


# direct methods
.method public static getAllAnnotationsPresent(Lcom/google/android/apps/books/annotations/Layer;ILandroid/database/sqlite/SQLiteDatabase;)Z
    .locals 10
    .param p0, "layer"    # Lcom/google/android/apps/books/annotations/Layer;
    .param p1, "segmentIndex"    # I
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 86
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "all_present"

    aput-object v1, v2, v0

    .line 89
    .local v2, "projection":[Ljava/lang/String;
    const-string v0, "%s=? AND %s=? AND %s=? AND %s=? AND %s=?"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "volume_id"

    aput-object v6, v1, v5

    const/4 v5, 0x1

    const-string v6, "content_version"

    aput-object v6, v1, v5

    const/4 v5, 0x2

    const-string v6, "layer_id"

    aput-object v6, v1, v5

    const/4 v5, 0x3

    const-string v6, "layer_version"

    aput-object v6, v1, v5

    const/4 v5, 0x4

    const-string v6, "segment_index"

    aput-object v6, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 92
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v9

    .line 93
    .local v9, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, v9, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, v9, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getLayerId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 97
    .local v4, "args":[Ljava/lang/String;
    const-string v1, "volume_annotation_segment_summary"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 100
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->getAllPresentValue(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 106
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return v0

    .line 103
    :cond_0
    const/4 v0, 0x0

    .line 106
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getAllPresentValue(Z)I
    .locals 1
    .param p0, "allPresent"    # Z

    .prologue
    .line 58
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getAllPresentValue(I)Z
    .locals 1
    .param p0, "allPresent"    # I

    .prologue
    .line 62
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCreationSql()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    const-string v0, "CREATE TABLE %s (%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER DEFAULT 0, UNIQUE(%s, %s, %s, %s, %s));"

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "volume_annotation_segment_summary"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "volume_id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "content_version"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "layer_id"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "layer_version"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "segment_index"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "all_present"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "volume_id"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "content_version"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "layer_id"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "layer_version"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "segment_index"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDropSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "DROP TABLE IF EXISTS volume_annotation_segment_summary"

    return-object v0
.end method

.method public static removeVolumeAnnotationsForLayer(Lcom/google/android/apps/books/annotations/Layer;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11
    .param p0, "layer"    # Lcom/google/android/apps/books/annotations/Layer;
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 152
    const-string v3, "%s=? AND %s=? AND %s=? AND %s=?"

    new-array v4, v10, [Ljava/lang/Object;

    const-string v5, "volume_id"

    aput-object v5, v4, v6

    const-string v5, "content_version"

    aput-object v5, v4, v7

    const-string v5, "layer_id"

    aput-object v5, v4, v8

    const-string v5, "layer_version"

    aput-object v5, v4, v9

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v2

    .line 156
    .local v2, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    new-array v0, v10, [Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    aput-object v3, v0, v6

    iget-object v3, v2, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    aput-object v3, v0, v7

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getLayerId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v8

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    aput-object v3, v0, v9

    .line 161
    .local v0, "args":[Ljava/lang/String;
    const-string v3, "volume_annotation_segment_summary"

    invoke-virtual {p1, v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 162
    return-void
.end method

.method public static removeVolumeAnnotationsForVolume(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 117
    invoke-static {}, Lcom/google/android/apps/books/annotations/Annotation;->getAllVolumeLayerIds()Ljava/util/List;

    move-result-object v5

    .line 118
    .local v5, "volumeLayerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v4, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_3

    .line 127
    if-lez v0, :cond_2

    .line 128
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_2
    const-string v6, "?"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 135
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "volume_id=? AND layer_id IN ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    .local v1, "query":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 139
    .local v2, "queryParams":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-interface {v2, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 142
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    new-array v3, v6, [Ljava/lang/String;

    .line 145
    .local v3, "queryParamsAsArray":[Ljava/lang/String;
    const-string v7, "volume_annotation_segment_summary"

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {p1, v7, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setAllAnnotationsPresent(Lcom/google/android/apps/books/annotations/Layer;ILandroid/database/sqlite/SQLiteDatabase;Z)V
    .locals 5
    .param p0, "layer"    # Lcom/google/android/apps/books/annotations/Layer;
    .param p1, "segmentIndex"    # I
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "allPresent"    # Z

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v1

    .line 71
    .local v1, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 72
    .local v0, "values":Landroid/content/ContentValues;
    const-string v2, "volume_id"

    iget-object v3, v1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v2, "content_version"

    iget-object v3, v1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v2, "layer_id"

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getLayerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v2, "layer_version"

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v2, "segment_index"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 77
    const-string v2, "all_present"

    invoke-static {p3}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->getAllPresentValue(Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 78
    const-string v2, "volume_annotation_segment_summary"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {p2, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 79
    return-void
.end method
