.class public Lcom/google/android/apps/books/model/OfferData$Builder;
.super Ljava/lang/Object;
.source "OfferData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/OfferData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mArtUri:Landroid/net/Uri;

.field private mOfferId:Ljava/lang/String;

.field private final mOfferedBooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferedBookData;",
            ">;"
        }
    .end annotation
.end field

.field private mQuota:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferedBooks:Ljava/util/List;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/model/OfferData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferData$Builder;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/model/OfferData$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferData$Builder;

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mQuota:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/model/OfferData$Builder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferData$Builder;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mArtUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/model/OfferData$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/OfferData$Builder;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferedBooks:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addOfferedBook(Lcom/google/android/apps/books/model/OfferedBookData;)Lcom/google/android/apps/books/model/OfferData$Builder;
    .locals 1
    .param p1, "book"    # Lcom/google/android/apps/books/model/OfferedBookData;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferedBooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    return-object p0
.end method

.method public build()Lcom/google/android/apps/books/model/OfferData;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/apps/books/model/OfferData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/model/OfferData;-><init>(Lcom/google/android/apps/books/model/OfferData$Builder;Lcom/google/android/apps/books/model/OfferData$1;)V

    return-object v0
.end method

.method public setArtUri(Landroid/net/Uri;)Lcom/google/android/apps/books/model/OfferData$Builder;
    .locals 0
    .param p1, "artUri"    # Landroid/net/Uri;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mArtUri:Landroid/net/Uri;

    .line 110
    return-object p0
.end method

.method public setArtUri(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferData$Builder;
    .locals 1
    .param p1, "artUri"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mArtUri:Landroid/net/Uri;

    .line 106
    return-object p0
.end method

.method public setOfferId(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferData$Builder;
    .locals 0
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mOfferId:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public setQuota(I)Lcom/google/android/apps/books/model/OfferData$Builder;
    .locals 0
    .param p1, "quota"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/google/android/apps/books/model/OfferData$Builder;->mQuota:I

    .line 102
    return-object p0
.end method
