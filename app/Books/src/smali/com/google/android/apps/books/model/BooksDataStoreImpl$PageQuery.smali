.class interface abstract Lcom/google/android/apps/books/model/BooksDataStoreImpl$PageQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PageQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1995
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "page_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "content_status"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "page_order"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "remote_url"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "structure_status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$PageQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
