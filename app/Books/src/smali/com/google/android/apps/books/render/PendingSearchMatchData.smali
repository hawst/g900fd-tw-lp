.class public Lcom/google/android/apps/books/render/PendingSearchMatchData;
.super Ljava/lang/Object;
.source "PendingSearchMatchData.java"


# instance fields
.field private mLoadedOrLoading:Z

.field private final mLocation:Lcom/google/android/apps/books/annotations/TextLocation;

.field private final mPassageIndex:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/TextLocation;IZ)V
    .locals 0
    .param p1, "pendingSearchMatch"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .param p2, "searchMatchPassage"    # I
    .param p3, "loadedOrLoading"    # Z

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mLocation:Lcom/google/android/apps/books/annotations/TextLocation;

    .line 29
    iput p2, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mPassageIndex:I

    .line 30
    iput-boolean p3, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mLoadedOrLoading:Z

    .line 31
    return-void
.end method


# virtual methods
.method public getLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mLocation:Lcom/google/android/apps/books/annotations/TextLocation;

    return-object v0
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mPassageIndex:I

    return v0
.end method

.method public isLoadedOrLoading()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mLoadedOrLoading:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "location"

    iget-object v2, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mLocation:Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "passageIndex"

    iget v2, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mPassageIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "loadedOrLoading"

    iget-boolean v2, p0, Lcom/google/android/apps/books/render/PendingSearchMatchData;->mLoadedOrLoading:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
