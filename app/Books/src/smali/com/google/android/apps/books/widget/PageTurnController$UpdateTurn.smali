.class Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;
.super Ljava/lang/Object;
.source "PageTurnController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTurn"
.end annotation


# instance fields
.field private final mNewFraction:F

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PageTurnController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PageTurnController;F)V
    .locals 2
    .param p2, "fraction"    # F

    .prologue
    .line 448
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;->mNewFraction:F

    .line 451
    return-void
.end method


# virtual methods
.method public canBeCanceled()Z
    .locals 1

    .prologue
    .line 481
    const/4 v0, 0x0

    return v0
.end method

.method public canRun()Z
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x1

    return v0
.end method

.method public getDirection()Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1

    .prologue
    .line 476
    const/4 v0, 0x0

    return-object v0
.end method

.method public run()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    if-nez v0, :cond_1

    .line 462
    const-string v0, "PageTurnController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    const-string v0, "PageTurnController"

    const-string v1, "update turn exiting early: no turn in progress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 471
    :goto_0
    return-object v0

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;->mNewFraction:F

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$502(Lcom/google/android/apps/books/widget/PageTurnController;F)F

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$UpdateTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # invokes: Lcom/google/android/apps/books/widget/PageTurnController;->publishTurnState()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$600(Lcom/google/android/apps/books/widget/PageTurnController;)V

    .line 471
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
