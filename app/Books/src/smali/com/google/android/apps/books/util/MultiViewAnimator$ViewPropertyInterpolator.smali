.class Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;
.super Ljava/lang/Object;
.source "MultiViewAnimator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/MultiViewAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewPropertyInterpolator"
.end annotation


# instance fields
.field final mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

.field final mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

.field final mViewRef:Ljava/lang/ref/Reference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/Reference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "start"    # Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    .param p3, "end"    # Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mViewRef:Ljava/lang/ref/Reference;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .line 71
    iput-object p3, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .line 72
    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 7
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 76
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mViewRef:Ljava/lang/ref/Reference;

    invoke-virtual {v5}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 78
    .local v4, "view":Landroid/view/View;
    if-nez v4, :cond_0

    .line 111
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 86
    .local v3, "progress":F
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    # invokes: Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->needsPivot()Z
    invoke-static {v5}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->access$000(Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 87
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v1, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotX:F

    .line 88
    .local v1, "pivotX":F
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v2, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotY:F

    .line 97
    .local v2, "pivotY":F
    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setPivotX(F)V

    .line 98
    invoke-virtual {v4, v2}, Landroid/view/View;->setPivotY(F)V

    .line 100
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleX:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleX:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setScaleX(F)V

    .line 101
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleY:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleY:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setScaleY(F)V

    .line 103
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->translationX:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->translationX:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 104
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->translationY:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->translationY:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 107
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->rotation:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->rotation:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setRotation(F)V

    .line 109
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->alpha:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->alpha:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v0

    .line 110
    .local v0, "alpha":F
    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_0

    .line 89
    .end local v0    # "alpha":F
    .end local v1    # "pivotX":F
    .end local v2    # "pivotY":F
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    # invokes: Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->needsPivot()Z
    invoke-static {v5}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->access$000(Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 90
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v1, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotX:F

    .line 91
    .restart local v1    # "pivotX":F
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v2, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotY:F

    .restart local v2    # "pivotY":F
    goto :goto_1

    .line 93
    .end local v1    # "pivotX":F
    .end local v2    # "pivotY":F
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotX:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotX:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v1

    .line 94
    .restart local v1    # "pivotX":F
    iget-object v5, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mStart:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v5, v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotY:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;->mEnd:Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    iget v6, v6, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotY:F

    invoke-static {v5, v6, v3}, Lcom/google/android/ublib/utils/Math2;->mix(FFF)F

    move-result v2

    .restart local v2    # "pivotY":F
    goto/16 :goto_1
.end method
