.class public Lcom/google/android/apps/books/app/ProgressDialogFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "ProgressDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/google/android/apps/books/app/ProgressDialogFragment;
    .locals 3
    .param p0, "messageResId"    # I

    .prologue
    .line 15
    new-instance v1, Lcom/google/android/apps/books/app/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/books/app/ProgressDialogFragment;-><init>()V

    .line 16
    .local v1, "f":Lcom/google/android/apps/books/app/ProgressDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 17
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "messageResId"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 18
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 20
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "messageResId"

    const v4, 0x7f0f005d

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 27
    .local v1, "messageResId":I
    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->setCancelable(Z)V

    .line 29
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 30
    .local v0, "dialog":Landroid/app/ProgressDialog;
    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ProgressDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 32
    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 33
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 35
    return-object v0
.end method
