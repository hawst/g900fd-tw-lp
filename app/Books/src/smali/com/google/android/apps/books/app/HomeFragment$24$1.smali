.class Lcom/google/android/apps/books/app/HomeFragment$24$1;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment$24;->onOffersData(Ljava/util/List;ZJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/HomeFragment$24;

.field final synthetic val$data:Ljava/util/List;

.field final synthetic val$fromServer:Z

.field final synthetic val$lastUpdateTimeMillis:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment$24;Ljava/util/List;ZJ)V
    .locals 0

    .prologue
    .line 1994
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$24;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$data:Ljava/util/List;

    iput-boolean p3, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$fromServer:Z

    iput-wide p4, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$lastUpdateTimeMillis:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1997
    new-instance v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$data:Ljava/util/List;

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$fromServer:Z

    iget-wide v4, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$lastUpdateTimeMillis:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;-><init>(Ljava/util/List;ZJ)V

    .line 1999
    .local v0, "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$24;

    iget-object v1, v1, Lcom/google/android/apps/books/app/HomeFragment$24;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$3600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 2000
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$24;

    iget-object v1, v1, Lcom/google/android/apps/books/app/HomeFragment$24;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2001
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$24;

    iget-object v1, v1, Lcom/google/android/apps/books/app/HomeFragment$24;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$data:Ljava/util/List;

    iget-boolean v3, p0, Lcom/google/android/apps/books/app/HomeFragment$24$1;->val$fromServer:Z

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setOffers(Ljava/util/List;Z)V

    .line 2003
    :cond_0
    return-void
.end method
