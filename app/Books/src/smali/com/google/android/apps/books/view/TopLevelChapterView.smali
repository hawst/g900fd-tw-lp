.class public Lcom/google/android/apps/books/view/TopLevelChapterView;
.super Lcom/google/android/apps/books/view/LocationItemView;
.source "TopLevelChapterView.java"


# instance fields
.field private final mCollapseDrawableResourceId:I

.field private final mExpandDrawableResourceId:I

.field private mGroupIndex:I

.field private mToggleButton:Landroid/widget/ImageView;

.field private mView:Landroid/widget/ExpandableListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/view/LocationItemView;-><init>(Landroid/content/Context;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    .local v0, "ids":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mExpandDrawableResourceId:I

    .line 37
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mCollapseDrawableResourceId:I

    .line 38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 23
    return-void

    .line 34
    nop

    :array_0
    .array-data 4
        0x7f0101a0
        0x7f0101a1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/LocationItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    .local v0, "ids":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mExpandDrawableResourceId:I

    .line 37
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mCollapseDrawableResourceId:I

    .line 38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 27
    return-void

    .line 34
    nop

    :array_0
    .array-data 4
        0x7f0101a0
        0x7f0101a1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/view/LocationItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    .local v0, "ids":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mExpandDrawableResourceId:I

    .line 37
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mCollapseDrawableResourceId:I

    .line 38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 31
    return-void

    .line 34
    nop

    :array_0
    .array-data 4
        0x7f0101a0
        0x7f0101a1
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/view/TopLevelChapterView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/view/TopLevelChapterView;

    .prologue
    .line 13
    iget v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mGroupIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/view/TopLevelChapterView;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/view/TopLevelChapterView;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mView:Landroid/widget/ExpandableListView;

    return-object v0
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/android/apps/books/view/LocationItemView;->onFinishInflate()V

    .line 44
    const v0, 0x7f0e003f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mToggleButton:Landroid/widget/ImageView;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mToggleButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/books/view/TopLevelChapterView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/view/TopLevelChapterView$1;-><init>(Lcom/google/android/apps/books/view/TopLevelChapterView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method

.method public setExpanded(Z)V
    .locals 2
    .param p1, "expanded"    # Z

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mToggleButton:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mCollapseDrawableResourceId:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    return-void

    .line 70
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mExpandDrawableResourceId:I

    goto :goto_0
.end method

.method public setGroupState(Landroid/widget/ExpandableListView;IZ)V
    .locals 2
    .param p1, "view"    # Landroid/widget/ExpandableListView;
    .param p2, "groupIndex"    # I
    .param p3, "hasChildren"    # Z

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mView:Landroid/widget/ExpandableListView;

    .line 59
    iput p2, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mGroupIndex:I

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView;->mToggleButton:Landroid/widget/ImageView;

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 61
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected styleDescriptionView(Landroid/widget/TextView;Z)V
    .locals 2
    .param p1, "descriptionView"    # Landroid/widget/TextView;
    .param p2, "selected"    # Z

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f0a0176

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 67
    return-void

    .line 65
    :cond_1
    const v0, 0x7f0a0175

    goto :goto_0
.end method
