.class public Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
.super Ljava/lang/Object;
.source "GridRowInvalidationManager.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;


# instance fields
.field private final mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

.field private mDestroyed:Z

.field private final mHandler:Landroid/os/Handler;

.field private mInvalidatePending:Z

.field private final mManagedViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/ublib/widget/AbsWarpListView;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

.field private final mScrollingViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/ublib/widget/AbsWarpListView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mHandler:Landroid/os/Handler;

    .line 25
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mScrollingViews:Ljava/util/Set;

    .line 26
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mManagedViews:Ljava/util/Set;

    .line 31
    new-instance v0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$1;-><init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 44
    new-instance v0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;-><init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mOnScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 76
    invoke-static {}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->getTaskManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->weaklyAddBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mScrollingViews:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->getTaskManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mInvalidatePending:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mManagedViews:Ljava/util/Set;

    return-object v0
.end method

.method private static getTaskManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 81
    invoke-static {}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->getTaskManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->removeBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mDestroyed:Z

    .line 83
    return-void
.end method

.method public invalidateLists()V
    .locals 5

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mInvalidatePending:Z

    if-eqz v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mInvalidatePending:Z

    .line 106
    invoke-static {}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->getTaskManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;-><init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)V

    const-wide/16 v2, 0x1f4

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->postDelayed(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;JI)V

    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mDestroyed:Z

    return v0
.end method

.method public removeListView(Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 2
    .param p1, "listView"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mOnScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mManagedViews:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mScrollingViews:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public weaklyAddListView(Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 2
    .param p1, "listView"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 91
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mOnScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mManagedViews:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method
