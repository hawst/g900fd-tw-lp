.class public Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
.super Ljava/lang/Object;
.source "VolumeAnnotationRequest.java"


# instance fields
.field public final contentVersion:Ljava/lang/String;

.field public final endPosition:Ljava/lang/String;

.field public final layerKey:Lcom/google/android/apps/books/annotations/Layer$Key;

.field public final segmentIndex:I

.field public final startPosition:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "layerKey"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "contentVersion"    # Ljava/lang/String;
    .param p3, "startPosition"    # Ljava/lang/String;
    .param p4, "endPosition"    # Ljava/lang/String;
    .param p5, "segmentIndex"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->layerKey:Lcom/google/android/apps/books/annotations/Layer$Key;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->contentVersion:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->startPosition:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->endPosition:Ljava/lang/String;

    .line 43
    iput p5, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->segmentIndex:I

    .line 44
    return-void
.end method


# virtual methods
.method public getLayerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->layerKey:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    return-object v0
.end method

.method public getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->layerKey:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->contentVersion:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public makeLayer(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 3
    .param p1, "layerVersion"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->layerKey:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->contentVersion:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
