.class public interface abstract Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;
.super Ljava/lang/Object;
.source "BooksCardsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract acknowledgeUploadFailure(Ljava/lang/String;)V
.end method

.method public abstract getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageFuture;
.end method

.method public abstract getUploadContextMenuDelegate()Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
.end method

.method public abstract getVolumeContextMenuDelegate()Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
.end method

.method public abstract isAvailableForReading(Ljava/lang/String;)Z
.end method

.method public abstract isPinned(Ljava/lang/String;)Z
.end method

.method public abstract onCoverImageLoaded()V
.end method

.method public abstract onLearnMoreAboutUploadFailure()V
.end method
