.class final Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;
.super Ljava/lang/Object;
.source "ReadingAccessManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadingAccessManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HandlerCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReadingAccessManager;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReadingAccessManager;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;->this$0:Lcom/google/android/apps/books/app/ReadingAccessManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReadingAccessManager;Lcom/google/android/apps/books/app/ReadingAccessManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReadingAccessManager;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReadingAccessManager$1;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;-><init>(Lcom/google/android/apps/books/app/ReadingAccessManager;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 101
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;->this$0:Lcom/google/android/apps/books/app/ReadingAccessManager;

    # invokes: Lcom/google/android/apps/books/app/ReadingAccessManager;->requestAccess()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadingAccessManager;->access$100(Lcom/google/android/apps/books/app/ReadingAccessManager;)V

    .line 104
    const/4 v0, 0x1

    return v0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
