.class Lcom/google/android/apps/books/widget/ReadNowHomeView$7;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
        "<",
        "Lcom/google/android/apps/books/playcards/BookDocument;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$7;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDocumentClick(Landroid/content/Context;Lcom/google/android/apps/books/playcards/BookDocument;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 206
    invoke-virtual {p2}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "volumeId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$7;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->viewStoreLinkForRecommendedBook(Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public bridge synthetic onDocumentClick(Landroid/content/Context;Ljava/lang/Object;Landroid/view/View;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Landroid/view/View;

    .prologue
    .line 203
    check-cast p2, Lcom/google/android/apps/books/playcards/BookDocument;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/ReadNowHomeView$7;->onDocumentClick(Landroid/content/Context;Lcom/google/android/apps/books/playcards/BookDocument;Landroid/view/View;)V

    return-void
.end method
