.class public Lcom/google/android/apps/books/widget/BookmarkView;
.super Lcom/google/android/ublib/view/FrameLayoutCompat;
.source "BookmarkView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private mAnimator:Landroid/animation/ObjectAnimator;

.field private mBookmarkDrawable:Landroid/graphics/drawable/Drawable;

.field private final mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

.field private final mRibbonView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/ublib/view/FrameLayoutCompat;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkDrawable:Landroid/graphics/drawable/Drawable;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .line 39
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 40
    .local v0, "ribbonView":Landroid/widget/ImageView;
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->getBookmarkDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->createBookmarkLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/BookmarkView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mRibbonView:Landroid/view/View;

    .line 43
    return-void
.end method

.method private createBookmarkLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 3

    .prologue
    .line 117
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 119
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getHeight()I

    move-result v1

    neg-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 120
    return-object v0
.end method

.method private getBookmarkDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkDrawable:Landroid/graphics/drawable/Drawable;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getRibbonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mRibbonView:Landroid/view/View;

    return-object v0
.end method

.method private hideRibbonView()V
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->getRibbonView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationY(Landroid/view/View;F)V

    .line 111
    return-void
.end method

.method private updateRibbonView(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)Landroid/animation/ObjectAnimator;
    .locals 10
    .param p1, "bookmarkState"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p2, "now"    # J

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->getRibbonView()Landroid/view/View;

    move-result-object v7

    .line 79
    .local v7, "ribbon":Landroid/view/View;
    invoke-virtual {p1, p2, p3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->stillAnimating(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;

    move-result-object v1

    .line 81
    .local v1, "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    const-string v2, "translationY"

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getHeight()I

    move-result v3

    move-object v0, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->createAnimator(Ljava/lang/Object;Ljava/lang/String;IJ)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 83
    .local v6, "result":Landroid/animation/ObjectAnimator;
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    .line 89
    .end local v1    # "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    .end local v6    # "result":Landroid/animation/ObjectAnimator;
    :goto_0
    return-object v6

    .line 86
    :cond_0
    invoke-virtual {p1, p2, p3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->calculateBookmarkRatioOnPage(J)F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float v8, v0, v2

    .line 88
    .local v8, "translationY":F
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationY(Landroid/view/View;F)V

    .line 89
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createFrameLayoutParams(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 7
    .param p1, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p2, "letterboxing"    # Landroid/graphics/Point;

    .prologue
    .line 50
    sget-object v4, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne p1, v4, :cond_0

    const/4 v1, 0x1

    .line 51
    .local v1, "isLeftCorner":Z
    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x3

    .line 52
    .local v0, "horizontalGravity":I
    :goto_1
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v4}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v5}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getHeight()I

    move-result v5

    or-int/lit8 v6, v0, 0x30

    invoke-direct {v2, v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 55
    .local v2, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v4}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getMargin()I

    move-result v4

    iget v5, p2, Landroid/graphics/Point;->x:I

    add-int v3, v4, v5

    .line 56
    .local v3, "xMargin":I
    iget v4, p2, Landroid/graphics/Point;->y:I

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 58
    if-eqz v1, :cond_2

    .line 59
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 63
    :goto_2
    return-object v2

    .line 50
    .end local v0    # "horizontalGravity":I
    .end local v1    # "isLeftCorner":Z
    .end local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v3    # "xMargin":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 51
    .restart local v1    # "isLeftCorner":Z
    :cond_1
    const/4 v0, 0x5

    goto :goto_1

    .line 61
    .restart local v0    # "horizontalGravity":I
    .restart local v2    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v3    # "xMargin":I
    :cond_2
    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto :goto_2
.end method

.method public onBookmarkChanged(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V
    .locals 2
    .param p1, "bookmarkState"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p2, "now"    # J

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->reset()V

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BookmarkView;->updateRibbonView(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mAnimator:Landroid/animation/ObjectAnimator;

    .line 69
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BookmarkView;->mAnimator:Landroid/animation/ObjectAnimator;

    .line 98
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BookmarkView;->hideRibbonView()V

    .line 99
    return-void
.end method
