.class public Lcom/google/android/apps/books/navigation/SnapshottingPageView;
.super Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;
.source "SnapshottingPageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;,
        Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;,
        Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;
    }
.end annotation


# instance fields
.field private mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

.field private mCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

.field private final mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

.field private final mContentSize:Landroid/graphics/Point;

.field private mContentView:Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;

.field private mDrewCurrentPage:Z

.field private mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private mLetterboxPaint:Landroid/graphics/Paint;

.field private mLoadingPaint:Landroid/graphics/Paint;

.field private mOnPageTouchListener:Lcom/google/android/apps/books/widget/OnPageTouchListener;

.field private mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

.field private mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field private mScaleFromFull:F

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTmpRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 118
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTmpRect:Landroid/graphics/Rect;

    .line 59
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLoadingPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLetterboxPaint:Landroid/graphics/Paint;

    .line 61
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;

    .line 77
    new-instance v0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    .line 229
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->maybeUpdateBookmark()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getContentWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getContentHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLoadingPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLetterboxPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mDrewCurrentPage:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getBookmarkMeasurements()Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/app/BookmarkController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/widget/OnPageTouchListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mOnPageTouchListener:Lcom/google/android/apps/books/widget/OnPageTouchListener;

    return-object v0
.end method

.method private getContentHeight()I
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method private getContentWidth()I
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method private maybeUpdateBookmark()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    if-nez v0, :cond_0

    .line 182
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent;->getBookmark()Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->attachBookmarkAnimator(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    goto :goto_0
.end method


# virtual methods
.method public drewCurrentPage()Z
    .locals 1

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mDrewCurrentPage:Z

    return v0
.end method

.method protected getBookmarkMargins(ILandroid/graphics/Point;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "result"    # Landroid/graphics/Point;

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    if-eqz v1, :cond_1

    .line 305
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;

    invoke-static {p0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getBounds(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 308
    .local v0, "bounds":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTmpRect:Landroid/graphics/Rect;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageRect(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    .line 309
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne v1, v2, :cond_0

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, p2, Landroid/graphics/Point;->x:I

    .line 314
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, p2, Landroid/graphics/Point;->y:I

    .line 318
    .end local v0    # "bounds":Landroid/graphics/Rect;
    :goto_1
    return-void

    .line 312
    .restart local v0    # "bounds":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTmpRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    iput v1, p2, Landroid/graphics/Point;->x:I

    goto :goto_0

    .line 316
    .end local v0    # "bounds":Landroid/graphics/Rect;
    :cond_1
    invoke-virtual {p2, v2, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_1
.end method

.method public getPage()Lcom/google/android/apps/books/navigation/PageViewContent;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    return-object v0
.end method

.method public hasDestroyedPage()Z
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/google/android/apps/books/app/BookmarkController;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;FII)V
    .locals 1
    .param p1, "bookmarkController"    # Lcom/google/android/apps/books/app/BookmarkController;
    .param p2, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .param p3, "scale"    # F
    .param p4, "loadingColor"    # I
    .param p5, "letterboxColor"    # I

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-super {p0, p2, v0}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->init(Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;Z)V

    .line 132
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    .line 133
    iput p3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mScaleFromFull:F

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLoadingPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mLetterboxPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    return-void
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 321
    invoke-super {p0}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->invalidate()V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentView:Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentView:Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;->invalidate()V

    .line 325
    :cond_0
    return-void
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    return v0
.end method

.method protected onBookmarkLayoutChanged()V
    .locals 4

    .prologue
    .line 187
    invoke-static {p0}, Lcom/google/android/apps/books/util/ViewUtils;->immediateLayoutAtCurrentSize(Landroid/view/View;)V

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getBookmarkView(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)Landroid/view/View;

    move-result-object v0

    .line 190
    .local v0, "bookmarkView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 191
    iget v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mScaleFromFull:F

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 192
    iget v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mScaleFromFull:F

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 194
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne v2, v3, :cond_1

    .line 195
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 199
    .local v1, "pivotX":I
    :goto_0
    int-to-float v2, v1

    invoke-virtual {v0, v2}, Landroid/view/View;->setPivotX(F)V

    .line 200
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setPivotY(F)V

    .line 202
    .end local v1    # "pivotX":I
    :cond_0
    return-void

    .line 197
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "pivotX":I
    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Landroid/content/Context;Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentView:Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentView:Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageViewContentView;

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 125
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x1

    .line 218
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public prepare(Lcom/google/android/apps/books/widget/OnPageTouchListener;Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;)V
    .locals 4
    .param p1, "onPageTouchListener"    # Lcom/google/android/apps/books/widget/OnPageTouchListener;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    .prologue
    const/4 v3, 0x0

    .line 140
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mOnPageTouchListener:Lcom/google/android/apps/books/widget/OnPageTouchListener;

    .line 141
    if-eqz p1, :cond_0

    .line 142
    new-instance v0, Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;)V

    invoke-direct {v0, v1, v2}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    .line 146
    :goto_0
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    .line 147
    return-void

    .line 144
    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    goto :goto_0
.end method

.method public setPage(Lcom/google/android/apps/books/navigation/PageViewContent;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/navigation/PageViewContent;
    .param p2, "pagePositionOnScreen"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/4 v3, 0x0

    .line 150
    const-string v0, "NavPageView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string v0, "NavPageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " drawing loading color"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    if-ne p1, v0, :cond_1

    .line 172
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/navigation/PageViewContent;->detachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)V

    .line 159
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 160
    if-eqz p1, :cond_4

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/navigation/PageViewContent;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentListener:Lcom/google/android/apps/books/navigation/PageViewContent$Listener;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/navigation/PageViewContent;->setListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)V

    .line 166
    :goto_1
    iput-boolean v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mDrewCurrentPage:Z

    .line 167
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 168
    if-eqz p2, :cond_3

    .line 169
    invoke-virtual {p2}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->setSideOfSpine(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 171
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->maybeUpdateBookmark()V

    goto :goto_0

    .line 164
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    goto :goto_1
.end method
