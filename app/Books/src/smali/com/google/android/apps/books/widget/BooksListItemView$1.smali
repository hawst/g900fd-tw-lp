.class Lcom/google/android/apps/books/widget/BooksListItemView$1;
.super Ljava/lang/Object;
.source "BooksListItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BooksListItemView;->setupOverflowIconView(Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/ublib/view/SystemUi;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksListItemView;

.field final synthetic val$doc:Lcom/google/android/apps/books/playcards/BookDocument;

.field final synthetic val$menuHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

.field final synthetic val$systemUi:Lcom/google/android/ublib/view/SystemUi;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/ublib/view/SystemUi;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->this$0:Lcom/google/android/apps/books/widget/BooksListItemView;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->val$menuHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    iput-object p3, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->val$doc:Lcom/google/android/apps/books/playcards/BookDocument;

    iput-object p4, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->val$systemUi:Lcom/google/android/ublib/view/SystemUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->this$0:Lcom/google/android/apps/books/widget/BooksListItemView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->val$menuHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->val$doc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;->getHandler(Lcom/google/android/apps/books/playcards/BookDocument;)Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksListItemView$1;->val$systemUi:Lcom/google/android/ublib/view/SystemUi;

    invoke-interface {v1, p1, v2}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;->showPopupMenu(Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    move-result-object v1

    # setter for: Lcom/google/android/apps/books/widget/BooksListItemView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/BooksListItemView;->access$002(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    .line 144
    return-void
.end method
