.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onPageRangeReady(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$lastReadyPageIndex:I

.field final synthetic val$passageIndex:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;II)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;->val$passageIndex:I

    iput p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;->val$lastReadyPageIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;->val$passageIndex:I

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;->val$lastReadyPageIndex:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/render/ReaderListener;->onPageRangeReady(II)V

    .line 138
    return-void
.end method
