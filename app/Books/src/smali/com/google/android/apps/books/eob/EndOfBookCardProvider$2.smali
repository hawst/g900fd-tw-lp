.class Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;
.super Ljava/lang/Object;
.source "EndOfBookCardProvider.java"

# interfaces
.implements Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->maybeAddCards()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

.field final synthetic val$endOfBookViewGroup:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;->this$0:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;->val$endOfBookViewGroup:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyRecommendationsReady()V
    .locals 3

    .prologue
    .line 92
    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;->this$0:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    # getter for: Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->access$200(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 94
    .local v0, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;->val$endOfBookViewGroup:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;->this$0:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    # getter for: Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->access$200(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;->this$0:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->access$202(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    .line 98
    .end local v0    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_0
    return-void
.end method
