.class final Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PinningCallbacks"
.end annotation


# instance fields
.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V
    .locals 0
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2176
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2177
    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->mVolumeId:Ljava/lang/String;

    .line 2178
    return-void
.end method

.method static synthetic access$3700(Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;

    .prologue
    .line 2173
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public handleResultUi(III)V
    .locals 13
    .param p1, "outcome"    # I
    .param p2, "maxDevices"    # I
    .param p3, "devicesInUse"    # I

    .prologue
    .line 2183
    const-string v9, "HomeFragment"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2184
    const-string v9, "HomeFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "OLM handleResultUi(), outcome="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", maxDevices="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2187
    :cond_0
    const/4 v9, -0x1

    if-ne p1, v9, :cond_4

    const/4 v6, 0x1

    .line 2189
    .local v6, "licenseError":Z
    :goto_0
    const/4 v9, 0x1

    if-ne p1, v9, :cond_5

    const/4 v4, 0x1

    .line 2192
    .local v4, "grantedLicense":Z
    :goto_1
    if-nez v6, :cond_1

    .line 2193
    iget-object v9, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v9}, Lcom/google/android/apps/books/app/HomeFragment;->access$1600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;

    invoke-direct {v10, p0, v4}, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks$1;-><init>(Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;Z)V

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 2200
    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/actionbar/UBLibActivity;

    .line 2201
    .local v0, "activity":Lcom/google/android/ublib/actionbar/UBLibActivity;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->isActivityDestroyed()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2202
    :cond_2
    const-string v9, "HomeFragment"

    const/4 v10, 0x4

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2203
    const-string v9, "HomeFragment"

    const-string v10, "Could not update offline license state, activity gone/destroyed"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2239
    :cond_3
    :goto_2
    return-void

    .line 2187
    .end local v0    # "activity":Lcom/google/android/ublib/actionbar/UBLibActivity;
    .end local v4    # "grantedLicense":Z
    .end local v6    # "licenseError":Z
    :cond_4
    const/4 v6, 0x0

    goto :goto_0

    .line 2189
    .restart local v6    # "licenseError":Z
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 2207
    .restart local v0    # "activity":Lcom/google/android/ublib/actionbar/UBLibActivity;
    .restart local v4    # "grantedLicense":Z
    :cond_6
    if-nez v4, :cond_7

    .line 2208
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->NO_LICENSE_WHEN_PINNED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logBookDownloadFailed(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;Ljava/lang/Throwable;)V

    .line 2212
    iget-object v9, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->mVolumeId:Ljava/lang/String;

    const/4 v11, 0x0

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V
    invoke-static {v9, v10, v11}, Lcom/google/android/apps/books/app/HomeFragment;->access$3800(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;Z)V

    .line 2213
    const/4 v9, 0x0

    sget-object v10, Lcom/google/android/apps/books/R$styleable;->AppTheme:[I

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v0, v9, v10, v11, v12}, Lcom/google/android/ublib/actionbar/UBLibActivity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 2215
    .local v2, "attrs":Landroid/content/res/TypedArray;
    const/4 v9, 0x3

    const v10, 0x1080027

    invoke-virtual {v2, v9, v10}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 2217
    .local v5, "iconResource":I
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 2219
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2220
    .local v1, "args":Landroid/os/Bundle;
    invoke-virtual {v0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2223
    .local v7, "res":Landroid/content/res/Resources;
    if-eqz v6, :cond_8

    .line 2224
    const v8, 0x7f0f00e5

    .line 2225
    .local v8, "titleResId":I
    const v9, 0x7f0f00e6

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2231
    .local v3, "bodyString":Ljava/lang/String;
    :goto_3
    new-instance v9, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v9, v1}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v9, v3}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const v11, 0x104000a

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v10, v8}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setIconResource(I)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 2236
    iget-object v9, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v9}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v9

    const-class v10, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v1, v11}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2238
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v2    # "attrs":Landroid/content/res/TypedArray;
    .end local v3    # "bodyString":Ljava/lang/String;
    .end local v5    # "iconResource":I
    .end local v7    # "res":Landroid/content/res/Resources;
    .end local v8    # "titleResId":I
    :cond_7
    iget-object v9, p0, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateVolumeDownloadProgress()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/HomeFragment;->access$3900(Lcom/google/android/apps/books/app/HomeFragment;)V

    goto :goto_2

    .line 2227
    .restart local v1    # "args":Landroid/os/Bundle;
    .restart local v2    # "attrs":Landroid/content/res/TypedArray;
    .restart local v5    # "iconResource":I
    .restart local v7    # "res":Landroid/content/res/Resources;
    :cond_8
    const v8, 0x7f0f00e4

    .line 2228
    .restart local v8    # "titleResId":I
    const/high16 v9, 0x7f110000

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, p2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "bodyString":Ljava/lang/String;
    goto :goto_3
.end method
