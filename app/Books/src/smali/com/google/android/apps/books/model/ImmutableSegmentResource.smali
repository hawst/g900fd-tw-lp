.class public Lcom/google/android/apps/books/model/ImmutableSegmentResource;
.super Ljava/lang/Object;
.source "ImmutableSegmentResource.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SegmentResource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    }
.end annotation


# instance fields
.field private final mCssClass:Ljava/lang/String;

.field private final mResourceId:Ljava/lang/String;

.field private final mSegmentId:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "segmentId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .param p3, "cssClass"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mSegmentId:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mResourceId:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mCssClass:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mTitle:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getCssClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mCssClass:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mResourceId:Ljava/lang/String;

    return-object v0
.end method

.method public getSegmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mSegmentId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->mTitle:Ljava/lang/String;

    return-object v0
.end method
