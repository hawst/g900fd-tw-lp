.class Lcom/google/android/apps/books/widget/ReadNowHomeView$8;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeUpdateOnboardCardVisibility()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$8;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "showOnboarding"    # Ljava/lang/Boolean;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$8;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    # getter for: Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->access$200(Lcom/google/android/apps/books/widget/ReadNowHomeView;)Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 325
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$8;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    # getter for: Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->access$200(Lcom/google/android/apps/books/widget/ReadNowHomeView;)Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->showOnboardView(Z)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 318
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$8;->take(Ljava/lang/Boolean;)V

    return-void
.end method
