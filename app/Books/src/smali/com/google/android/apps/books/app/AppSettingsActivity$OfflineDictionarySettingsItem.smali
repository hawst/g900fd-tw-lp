.class Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AppSettingsItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfflineDictionarySettingsItem"
.end annotation


# instance fields
.field private final mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private final mNoDictSummary:Ljava/lang/String;

.field private mRequestedLanguages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServerMetadataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mSummary:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V
    .locals 1
    .param p2, "activity"    # Lcom/google/android/apps/books/app/AppSettingsActivity;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 455
    const v0, 0x7f0f0159

    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mNoDictSummary:Ljava/lang/String;

    .line 457
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mSummary:Ljava/lang/String;

    .line 458
    new-instance v0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$1;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;Lcom/google/android/apps/books/app/AppSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 464
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->refresh()V

    .line 465
    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 447
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->updateSummary(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 447
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mServerMetadataList:Ljava/util/List;

    return-object p1
.end method

.method private getCommaSeparatedLanguages()Ljava/lang/String;
    .locals 4

    .prologue
    .line 512
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mRequestedLanguages:Ljava/util/List;

    if-nez v3, :cond_0

    .line 513
    const-string v3, ""

    .line 524
    :goto_0
    return-object v3

    .line 515
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mRequestedLanguages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 516
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mNoDictSummary:Ljava/lang/String;

    goto :goto_0

    .line 518
    :cond_1
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 519
    .local v2, "languageTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mRequestedLanguages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 521
    .local v1, "languageCode":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/dictionary/LocalDictionaryUtils;->getDisplayLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 523
    .end local v1    # "languageCode":Ljava/lang/String;
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 524
    const-string v3, ", "

    invoke-static {v3}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private refresh()V
    .locals 3

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 474
    .local v0, "activity":Landroid/app/Activity;
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v1

    .line 477
    .local v1, "booksDataController":Lcom/google/android/apps/books/data/BooksDataController;
    if-nez v1, :cond_0

    .line 500
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V

    .line 481
    new-instance v2, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$2;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->getRequestedDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V

    .line 493
    new-instance v2, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem$3;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->getServerDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private updateSummary(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 503
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    :goto_0
    return-void

    .line 506
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mRequestedLanguages:Ljava/util/List;

    .line 507
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->getCommaSeparatedLanguages()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mSummary:Ljava/lang/String;

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/AppSettingsActivity;->notifySettingChanged()V

    goto :goto_0
.end method


# virtual methods
.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 529
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 530
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040027

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 531
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0e00af

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 532
    .local v1, "tv":Landroid/widget/TextView;
    const v4, 0x7f0f0158

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 534
    const v4, 0x7f0e00b0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 535
    .local v2, "tv_summary":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mSummary:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    return-object v3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 543
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mServerMetadataList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mRequestedLanguages:Ljava/util/List;

    if-nez v1, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mServerMetadataList:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->mRequestedLanguages:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->newInstance(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;

    move-result-object v0

    .line 549
    .local v0, "dialog":Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OfflineDictionarySettingsItem;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "LocalDictDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
