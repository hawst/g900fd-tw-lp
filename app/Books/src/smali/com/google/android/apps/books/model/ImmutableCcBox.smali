.class public Lcom/google/android/apps/books/model/ImmutableCcBox;
.super Ljava/lang/Object;
.source "ImmutableCcBox.java"

# interfaces
.implements Lcom/google/android/apps/books/model/CcBox;


# instance fields
.field private final mH:I

.field private final mW:I

.field private final mX:I

.field private final mY:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mX:I

    .line 11
    iput p2, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mY:I

    .line 12
    iput p3, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mW:I

    .line 13
    iput p4, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mH:I

    .line 14
    return-void
.end method


# virtual methods
.method public getH()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mH:I

    return v0
.end method

.method public getW()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mW:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mX:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableCcBox;->mY:I

    return v0
.end method
