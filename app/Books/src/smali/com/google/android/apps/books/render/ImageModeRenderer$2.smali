.class Lcom/google/android/apps/books/render/ImageModeRenderer$2;
.super Landroid/os/AsyncTask;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;->getHighlightRectsTask(ILjava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/common/collect/Multimap",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Rect;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

.field final synthetic val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final synthetic val$query:Ljava/lang/String;

.field final synthetic val$requestId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iput-object p2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iput-object p3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$query:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$requestId:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 577
    iget-object v6, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v6}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$500(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v6

    const-string v7, "compute image mode highlights"

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v4

    .line 581
    .local v4, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    .line 582
    .local v2, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    if-nez v2, :cond_0

    .line 583
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 593
    invoke-interface {v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .end local v2    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :goto_0
    return-object v6

    .line 585
    .restart local v2    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v3

    .line 586
    .local v3, "pageIndex":I
    new-instance v5, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;

    invoke-direct {v5}, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;-><init>()V

    .line 587
    .local v5, "walker":Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;
    iget-object v6, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$query:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v8, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageStructure(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v7, v8}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$600(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v7

    invoke-virtual {v5, v6, v3, v7}, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->getIdToRects(Ljava/lang/String;ILcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/common/collect/Multimap;

    move-result-object v1

    .line 589
    .local v1, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_1
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 593
    invoke-interface {v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto :goto_0

    .line 590
    .end local v1    # "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    .end local v2    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    .end local v3    # "pageIndex":I
    .end local v5    # "walker":Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :try_start_2
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 593
    invoke-interface {v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto :goto_0

    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :catchall_0
    move-exception v6

    invoke-interface {v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    throw v6
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 574
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 603
    .local p1, "idToRects":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 604
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 606
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$700(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .line 607
    .local v1, "listener":Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;
    iget v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->val$requestId:I

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/collect/Multimap;

    invoke-interface {v1, v3, v2}, Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;->onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V

    goto :goto_0

    .line 611
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$700(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .line 612
    .restart local v1    # "listener":Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;->onError(Ljava/lang/Exception;)V

    goto :goto_1

    .line 615
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 574
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 574
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$2;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 599
    return-void
.end method
