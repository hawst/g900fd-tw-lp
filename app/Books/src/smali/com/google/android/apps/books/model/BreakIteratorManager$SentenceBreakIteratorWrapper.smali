.class Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;
.super Lcom/google/android/apps/books/model/BreakIteratorManager$BasicBreakIteratorWrapper;
.source "BreakIteratorManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BreakIteratorManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SentenceBreakIteratorWrapper"
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BreakIteratorManager$BasicBreakIteratorWrapper;-><init>(Ljava/util/Locale;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/BreakIterator;->getSentenceInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/BreakIteratorManager$SentenceBreakIteratorWrapper;->breakIterator:Ljava/text/BreakIterator;

    .line 56
    return-void
.end method
