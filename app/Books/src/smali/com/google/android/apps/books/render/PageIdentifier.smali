.class public Lcom/google/android/apps/books/render/PageIdentifier;
.super Ljava/lang/Object;
.source "PageIdentifier.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/render/PageIdentifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final mIndices:Lcom/google/android/apps/books/render/PageIndices;

.field private final mOffsetFromIndices:I

.field private final mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/Position;ILcom/google/android/apps/books/render/PageIndices;I)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "offsetFromPosition"    # I
    .param p3, "indices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p4, "offsetFromIndices"    # I

    .prologue
    .line 51
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/apps/books/render/PageIdentifier;-><init>(Lcom/google/android/apps/books/render/PositionPageIdentifier;Lcom/google/android/apps/books/render/PageIndices;I)V

    .line 53
    return-void

    .line 51
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/books/render/PositionPageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/books/render/PositionPageIdentifier;Lcom/google/android/apps/books/render/PageIndices;I)V
    .locals 2
    .param p1, "positionPageIdentifier"    # Lcom/google/android/apps/books/render/PositionPageIdentifier;
    .param p2, "indices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p3, "offsetFromIndices"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Missing both position and indices"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    .line 46
    iput p3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    .line 47
    return-void

    .line 42
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1
    .param p0, "passageIndex"    # I
    .param p1, "pageIndex"    # I
    .param p2, "pageOffset"    # I

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/apps/books/render/PageIndices;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-static {v0, p2}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(Lcom/google/android/apps/books/render/PageIndices;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public static withIndices(Lcom/google/android/apps/books/render/PageIndices;I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 3
    .param p0, "indices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p1, "pageOffset"    # I

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/google/android/apps/books/render/PageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;ILcom/google/android/apps/books/render/PageIndices;I)V

    return-object v0
.end method

.method public static withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 3
    .param p0, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p1, "pageOffset"    # I

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/apps/books/render/PageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;ILcom/google/android/apps/books/render/PageIndices;I)V

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 7
    .param p1, "other"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v2

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v6

    if-le v5, v6, :cond_2

    move v2, v3

    .line 130
    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    add-int v0, v5, v6

    .line 133
    .local v0, "offsetPageIndex":I
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    iget v6, p1, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    add-int v1, v5, v6

    .line 134
    .local v1, "otherOffsetPageIndex":I
    if-lt v0, v1, :cond_0

    .line 136
    if-le v0, v1, :cond_3

    move v2, v3

    .line 137
    goto :goto_0

    :cond_3
    move v2, v4

    .line 139
    goto :goto_0

    .end local v0    # "offsetPageIndex":I
    .end local v1    # "otherOffsetPageIndex":I
    :cond_4
    move v2, v4

    .line 143
    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/apps/books/render/PageIdentifier;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PageIdentifier;->compareTo(Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 162
    if-ne p0, p1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v1

    .line 165
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 166
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 169
    check-cast v0, Lcom/google/android/apps/books/render/PageIdentifier;

    .line 171
    .local v0, "that":Lcom/google/android/apps/books/render/PageIdentifier;
    iget v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    iget v4, v0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 172
    goto :goto_0

    .line 174
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget-object v4, v0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PageIndices;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_5
    move v1, v2

    .line 175
    goto :goto_0

    .line 174
    :cond_6
    iget-object v3, v0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-nez v3, :cond_5

    .line 177
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    iget-object v4, v0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 179
    goto :goto_0

    .line 177
    :cond_8
    iget-object v3, v0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getIndices()Lcom/google/android/apps/books/render/PageIndices;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    return-object v0
.end method

.method public getOffsetFromIndices()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    return v0
.end method

.method public getOffsetFromPosition()I
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v0

    .line 92
    .local v0, "ppi":Lcom/google/android/apps/books/render/PositionPageIdentifier;
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->getPageOffsetFromPosition()I

    move-result v1

    .line 95
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPageIndex()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v0, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v0, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v0

    .line 84
    .local v0, "ppi":Lcom/google/android/apps/books/render/PositionPageIdentifier;
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 87
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    return-object v0
.end method

.method public hasValidIndices()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidPosition()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 188
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->hashCode()I

    move-result v0

    .line 189
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageIndices;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 190
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    add-int v0, v1, v2

    .line 191
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 188
    goto :goto_0
.end method

.method public offsetBy(I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v2

    add-int/2addr v2, p1

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v4, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    add-int/2addr v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/render/PageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;ILcom/google/android/apps/books/render/PageIndices;I)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 153
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "positionPageIdentifier"

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "indices"

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "offsetFromIndices"

    iget v2, p0, Lcom/google/android/apps/books/render/PageIdentifier;->mOffsetFromIndices:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
