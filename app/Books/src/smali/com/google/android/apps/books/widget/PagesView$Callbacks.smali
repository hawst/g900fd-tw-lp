.class public interface abstract Lcom/google/android/apps/books/widget/PagesView$Callbacks;
.super Ljava/lang/Object;
.source "PagesView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesDisplay$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract atLeastOnePositionHasBookmark(I)Z
.end method

.method public abstract flipPage(Lcom/google/android/apps/books/util/ScreenDirection;)V
.end method

.method public abstract loadPages([I)V
.end method

.method public abstract onLoadingPageVisibilityChanged(Z)V
.end method

.method public abstract onPageTurnAnimationFinished()V
.end method
