.class public Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
.super Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
.source "MediaOverlaysController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoReadableItemsRequestData"
.end annotation


# instance fields
.field private final mElementIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p3, "elementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;-><init>(ILjava/lang/String;)V

    .line 186
    iput-object p3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->mElementIds:Ljava/util/List;

    .line 187
    return-void
.end method


# virtual methods
.method public getElementIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->mElementIds:Ljava/util/List;

    return-object v0
.end method
