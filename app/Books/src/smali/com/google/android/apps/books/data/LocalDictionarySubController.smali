.class public Lcom/google/android/apps/books/data/LocalDictionarySubController;
.super Ljava/lang/Object;
.source "LocalDictionarySubController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;
    }
.end annotation


# instance fields
.field private mCurrentDictionaryDownload:Ljava/lang/String;

.field private final mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

.field private final mDictionaryMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/dictionary/LocalDictionary;",
            ">;"
        }
    .end annotation
.end field

.field private mDictionaryNotificationSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mListener:Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;

.field private mMetadataCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingDownloads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/provider/VolumeContentStore;)V
    .locals 1
    .param p1, "dictionaryNotificationListener"    # Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;
    .param p2, "dataControllerStore"    # Lcom/google/android/apps/books/model/DataControllerStore;
    .param p3, "volumeContentStore"    # Lcom/google/android/apps/books/provider/VolumeContentStore;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryMap:Ljava/util/Map;

    .line 64
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryNotificationSet:Ljava/util/Set;

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mListener:Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    .line 67
    iput-object p3, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->downloadDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->isInProgress(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "x2"    # Ljava/lang/Exception;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->downloadFailed(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/LocalDictionarySubController;)Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mListener:Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/data/LocalDictionarySubController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mCurrentDictionaryDownload:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->startNextDictionaryDownload(Lcom/google/android/apps/books/data/ControlTaskServices;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;ILcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p1, "x1"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->updateDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;ILcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method private downloadDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 3
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "controlTaskServices"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mCurrentDictionaryDownload:Ljava/lang/String;

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mVolumeContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionaryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getDictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    .line 235
    .local v0, "file":Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    new-instance v1, Lcom/google/android/apps/books/data/LocalDictionarySubController$1;

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {v1, p0, v2, p1, v0}, Lcom/google/android/apps/books/data/LocalDictionarySubController$1;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/InternalVolumeContentFile;)V

    invoke-interface {p2, v1}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 242
    return-void
.end method

.method private downloadDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 19
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "dictionaryFile"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .param p3, "networkTaskServices"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 265
    const/4 v11, 0x0

    .line 266
    .local v11, "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v10, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v10}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 267
    .local v10, "client":Lorg/apache/http/client/HttpClient;
    const/16 v16, 0x0

    .line 268
    .local v16, "response":Lorg/apache/http/HttpResponse;
    const/4 v14, 0x0

    .line 269
    .local v14, "is":Ljava/io/InputStream;
    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_DOWNLOAD_STARTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V

    .line 274
    :try_start_0
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDownloadUrl()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-static {v10, v4}, Lcom/google/android/apps/books/net/HttpUtils;->execute(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v16

    .line 279
    :try_start_1
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_0

    .line 280
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 326
    :catch_0
    move-exception v12

    .line 327
    .local v12, "e":Ljava/io/IOException;
    invoke-static {v14}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 328
    move-object v13, v11

    .line 329
    .local v13, "failedCommitter":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v4, Lcom/google/android/apps/books/data/LocalDictionarySubController$4;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v13}, Lcom/google/android/apps/books/data/LocalDictionarySubController$4;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 337
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->downloadFailed(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    .line 339
    .end local v12    # "e":Ljava/io/IOException;
    .end local v13    # "failedCommitter":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    :goto_0
    return-void

    .line 275
    :catch_1
    move-exception v12

    .line 277
    .restart local v12    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v4, Ljava/net/ConnectException;

    invoke-direct {v4}, Ljava/net/ConnectException;-><init>()V

    throw v4

    .line 282
    .end local v12    # "e":Ljava/io/IOException;
    :cond_0
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v14

    .line 283
    if-nez v14, :cond_1

    .line 284
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 287
    :cond_1
    new-instance v18, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    .line 289
    .local v18, "streamProgressListener":Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;
    new-instance v15, Lcom/google/android/apps/books/data/LocalDictionarySubController$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/books/data/LocalDictionarySubController$2;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;)V

    .line 297
    .local v15, "onContentSaveListener":Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;
    move-object v8, v14

    .line 300
    .local v8, "inputStream":Ljava/io/InputStream;
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v15, v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v17

    .line 302
    .local v17, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->startingDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    .line 303
    new-instance v4, Lcom/google/android/apps/books/model/EncryptedContentImpl;

    invoke-direct {v4, v8}, Lcom/google/android/apps/books/model/EncryptedContentImpl;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v11

    .line 304
    move-object v7, v11

    .line 305
    .local v7, "completedCommitter":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v4, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/model/BooksDataStore$Committer;Ljava/io/InputStream;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method private downloadFailed(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "networkTaskServices"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 383
    new-instance v0, Lcom/google/android/apps/books/data/LocalDictionarySubController$8;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/LocalDictionarySubController$8;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;)V

    invoke-interface {p3, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 393
    return-void
.end method

.method private finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "networkTaskServices"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 369
    new-instance v0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 379
    return-void
.end method

.method private getLocalDictionary(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/google/android/apps/books/dictionary/LocalDictionary;
    .locals 11
    .param p1, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "onKeyErrorCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 126
    iget-object v8, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryMap:Ljava/util/Map;

    invoke-interface {v8, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/dictionary/LocalDictionary;

    .line 127
    .local v2, "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 166
    :goto_0
    return-object v3

    .line 131
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->getMetadataCache()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 132
    .local v6, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    if-nez v6, :cond_1

    .line 133
    const/4 v3, 0x0

    goto :goto_0

    .line 135
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v8, v6}, Lcom/google/android/apps/books/model/DataControllerStore;->getDownloadedDictionaryFile(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Ljava/io/File;

    move-result-object v5

    .line 136
    .local v5, "file":Ljava/io/File;
    if-nez v5, :cond_2

    .line 137
    const/4 v3, 0x0

    goto :goto_0

    .line 139
    :cond_2
    invoke-virtual {v6}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getAccountName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v8}, Lcom/google/android/apps/books/model/BooksDataStore;->getAccountSessionKey(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v7

    .line 142
    .local v7, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-nez v7, :cond_3

    .line 143
    invoke-direct {p0, v6, p3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->onKeyError(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Runnable;)V

    .line 144
    const/4 v3, 0x0

    goto :goto_0

    .line 147
    :cond_3
    :try_start_0
    new-instance v8, Lcom/google/android/apps/books/data/ProductionEncryptionScheme;

    invoke-direct {v8}, Lcom/google/android/apps/books/data/ProductionEncryptionScheme;-><init>()V

    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v6}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getEncryptedDictionaryKey()[B

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/books/data/ProductionEncryptionScheme;->decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;

    move-result-object v1

    .line 150
    .local v1, "decryptedDictionaryKeyStream":Ljava/io/InputStream;
    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 151
    .local v0, "decryptedDictionaryKeyBytes":[B
    new-instance v3, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;

    invoke-direct {v3, v5, v0, p2}, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;-><init>(Ljava/io/File;[BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 165
    .end local v2    # "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    .local v3, "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    iget-object v8, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryMap:Ljava/util/Map;

    invoke-interface {v8, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v3

    .line 166
    .end local v3    # "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    .restart local v2    # "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    goto :goto_0

    .line 152
    .end local v0    # "decryptedDictionaryKeyBytes":[B
    .end local v1    # "decryptedDictionaryKeyStream":Ljava/io/InputStream;
    :catch_0
    move-exception v4

    .line 153
    .local v4, "e":Ljava/security/GeneralSecurityException;
    const-string v8, "LDSC"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 154
    const-string v8, "LDSC"

    const-string v9, "Error opening offline dictionary"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 156
    :cond_4
    invoke-direct {p0, v6, p3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->onKeyError(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Runnable;)V

    .line 157
    const/4 v3, 0x0

    goto :goto_0

    .line 158
    .end local v4    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v4

    .line 159
    .local v4, "e":Ljava/io/IOException;
    const-string v8, "LDSC"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 160
    const-string v8, "LDSC"

    const-string v9, "Error opening offline dictionary"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 162
    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getMetadataCache()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mMetadataCache:Ljava/util/Map;

    if-nez v2, :cond_0

    .line 252
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mMetadataCache:Ljava/util/Map;

    .line 253
    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/DataControllerStore;->getLocalDictionaryMetadataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 255
    .local v1, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mMetadataCache:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 258
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mMetadataCache:Ljava/util/Map;

    return-object v2
.end method

.method private isInProgress(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryNotificationSet:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private onKeyError(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "onKeyErrorCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->removeDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 175
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 176
    return-void
.end method

.method private startNextDictionaryDownload(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "controlTaskServices"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mPendingDownloads:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mPendingDownloads:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->downloadDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 248
    :cond_0
    return-void
.end method

.method private startingBatch(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 396
    .local p1, "dictionaryList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mListener:Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;->startingBatch(Ljava/util/List;)V

    .line 397
    return-void
.end method

.method private startingDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "networkTaskServices"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/apps/books/data/LocalDictionarySubController$5;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController$5;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 352
    return-void
.end method

.method private updateDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;ILcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p2, "progressPercent"    # I
    .param p3, "networkTaskServices"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 356
    new-instance v0, Lcom/google/android/apps/books/data/LocalDictionarySubController$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/LocalDictionarySubController$6;-><init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;I)V

    invoke-interface {p3, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 365
    return-void
.end method


# virtual methods
.method public clearCaches()V
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mMetadataCache:Ljava/util/Map;

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 402
    return-void
.end method

.method public insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->getMetadataCache()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    return-void
.end method

.method public lookup(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .locals 4
    .param p1, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p2, "term"    # Ljava/lang/String;
    .param p3, "language"    # Ljava/lang/String;
    .param p4, "onKeyErrorCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 105
    if-nez p3, :cond_0

    const-string v1, "en"

    .line 106
    .local v1, "nonNullLanguage":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, p1, v1, p4}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->getLocalDictionary(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/google/android/apps/books/dictionary/LocalDictionary;

    move-result-object v0

    .line 108
    .local v0, "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    if-eqz v0, :cond_2

    .line 109
    invoke-interface {v0, p2}, Lcom/google/android/apps/books/dictionary/LocalDictionary;->lookup(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/DictionaryEntry;

    move-result-object v2

    .line 110
    .local v2, "result":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    if-eqz v2, :cond_1

    .line 111
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V

    .line 120
    .end local v2    # "result":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    :goto_1
    return-object v2

    .end local v0    # "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    .end local v1    # "nonNullLanguage":Ljava/lang/String;
    :cond_0
    move-object v1, p3

    .line 105
    goto :goto_0

    .line 114
    .restart local v0    # "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    .restart local v1    # "nonNullLanguage":Ljava/lang/String;
    .restart local v2    # "result":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    :cond_1
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V

    goto :goto_1

    .line 120
    .end local v2    # "result":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public removeDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 3
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/dictionary/LocalDictionary;

    .line 89
    .local v0, "dictionary":Lcom/google/android/apps/books/dictionary/LocalDictionary;
    if-eqz v0, :cond_0

    .line 90
    invoke-interface {v0}, Lcom/google/android/apps/books/dictionary/LocalDictionary;->close()V

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 93
    return-void
.end method

.method public syncLocalDictionaries(Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 10
    .param p2, "controlTaskServices"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "requestedLanguageCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x3

    .line 180
    const-string v6, "LDSC"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 181
    const-string v6, "LDSC"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Syncing local dictionaries for the following language codes: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryNotificationSet:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 185
    iget-object v6, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDictionaryNotificationSet:Ljava/util/Set;

    invoke-interface {v6, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 189
    iget-object v6, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/DataControllerStore;->getServerDictionaryMetadataList()Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/DataControllerStore;->getLocalDictionaryMetadataList()Ljava/util/List;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/dictionary/LocalDictionaryUtils;->getUpdateLists(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;

    move-result-object v5

    .line 193
    .local v5, "updateLists":Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;
    iget-object v6, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/DataControllerStore;->getLocalDictionaryMetadataList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 194
    .local v2, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v2}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 195
    const-string v6, "LDSC"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 196
    const-string v6, "LDSC"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Removing language code for metadata: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->removeDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    goto :goto_0

    .line 202
    .end local v2    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 203
    .local v4, "pendingDownloadsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 205
    .local v3, "notificationList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-virtual {v5}, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;->getNewestVersionServerOnly()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 206
    .restart local v2    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v2}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "languageCode":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 208
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {v2}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mCurrentDictionaryDownload:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 210
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 216
    .end local v1    # "languageCode":Ljava/lang/String;
    .end local v2    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_5
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->startingBatch(Ljava/util/List;)V

    .line 217
    iput-object v4, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mPendingDownloads:Ljava/util/List;

    .line 218
    iget-object v6, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController;->mCurrentDictionaryDownload:Ljava/lang/String;

    if-nez v6, :cond_6

    .line 219
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->startNextDictionaryDownload(Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 221
    :cond_6
    return-void
.end method
