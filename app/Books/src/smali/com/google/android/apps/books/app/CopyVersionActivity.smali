.class public Lcom/google/android/apps/books/app/CopyVersionActivity;
.super Landroid/app/Activity;
.source "CopyVersionActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/CopyVersionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 27
    .local v3, "extras":Landroid/os/Bundle;
    const-string v5, "android.intent.extra.books.VERSION"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 28
    .local v4, "version":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/CopyVersionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f00dc

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "copiedMsg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/CopyVersionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 30
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/google/android/apps/books/app/CopyVersionActivity$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/app/CopyVersionActivity$1;-><init>(Lcom/google/android/apps/books/app/CopyVersionActivity;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 36
    const-string v5, "clipboard"

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/CopyVersionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ClipboardManager;

    .line 37
    .local v1, "clipboard":Landroid/content/ClipboardManager;
    invoke-static {v2, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 38
    .local v0, "clip":Landroid/content/ClipData;
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 39
    return-void
.end method
