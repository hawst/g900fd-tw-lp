.class final Lcom/google/android/apps/books/app/SelectionState$1;
.super Ljava/lang/Object;
.source "SelectionState.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SelectionState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SelectionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p1, "androidColor"    # I
    .param p2, "marginNoteText"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContainingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p1, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNormalizedSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string v0, ""

    return-object v0
.end method

.method public getOverlappingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p1, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 19
    const/4 v0, -0x1

    return v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const-string v0, ""

    return-object v0
.end method

.method public loadMatchingAnnotation(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "loader"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/geo/LayerAnnotationLoader;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/annotations/Annotation;>;"
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public moveSelectionHandle(IZIIIZ)I
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "firstHandle"    # Z
    .param p3, "deltaX"    # I
    .param p4, "deltaY"    # I
    .param p5, "prevRequest"    # I
    .param p6, "isDoneMoving"    # Z

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method
