.class Lcom/google/android/apps/books/widget/HeaderGridViewController$2;
.super Landroid/support/v7/widget/GridLayoutManager;
.source "HeaderGridViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/HeaderGridViewController;-><init>(Landroid/support/v7/widget/RecyclerView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;Landroid/content/Context;I)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0, p2, p3}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private adjustIndex(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAccessibilitySkipCount:I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$500(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v0

    sub-int v0, p1, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/support/v7/widget/GridLayoutManager;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 157
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v0

    .line 158
    .local v0, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getAdapterCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$300(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->adjustIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setItemCount(I)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->findFirstVisibleItemPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->adjustIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setFromIndex(I)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->findLastVisibleItemPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;->adjustIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setToIndex(I)V

    .line 163
    :cond_0
    return-void
.end method
