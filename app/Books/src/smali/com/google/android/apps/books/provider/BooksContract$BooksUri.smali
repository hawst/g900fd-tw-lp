.class public final enum Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
.super Ljava/lang/Enum;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BooksUri"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/provider/BooksContract$BooksUri;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNTS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNTS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNT_COLLECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNT_COLLECTIONS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNT_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNT_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ACCOUNT_VOLUME_STATES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ALL_COLLECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum ALL_COLLECTION_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum COLLECTION_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum COLLECTION_VOLUMES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum CONFIGURATION:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum FILES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum INTENT_VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SEARCH_SUGGEST:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SEARCH_SUGGEST_SHORTCUT_REFRESH:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SEARCH_SUGGEST_ZERO_QUERY:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SESSION_KEYS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SESSION_KEYS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SHARED_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum SHARED_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_CHAPTERS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_CHAPTERS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_COVER:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_COVER_THUMBNAIL:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_PAGES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_PAGES_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_PAGES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_PAGES_STRUCTURE:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_RES_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_RES_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_RES_RES_COMPOUND_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_SECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_SECTIONS_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_SECTIONS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_SEGMENT_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUMES_SEGMENT_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUME_STATES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field public static final enum VOLUME_STATES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

.field private static final pathSeparator:Ljava/util/regex/Pattern;


# instance fields
.field public final code:I

.field private final mEncodedPath:Ljava/lang/String;

.field private volatile mParts:[Ljava/lang/String;

.field private volatile mWildcardSlots:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/provider/BooksContract$PathParam;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final matchPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 105
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "INTENT_VOLUME_ID"

    const/16 v2, 0x50

    const-string v3, "volumes/*VOLUME_ID"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->INTENT_VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 107
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNTS"

    const/16 v2, 0x3e8

    const-string v3, "accounts"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNTS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 108
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES"

    const/16 v2, 0x64

    const-string v3, "accounts/volumes"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 113
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNTS_ID"

    const/16 v2, 0x3e9

    const-string v3, "accounts/*ACCOUNT_ID"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNTS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 115
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNT_VOLUMES"

    const/16 v2, 0x6e

    const-string v3, "accounts/*ACCOUNT_NAME/volumes"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 116
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_ID"

    const/4 v2, 0x5

    const/16 v3, 0x78

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 118
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_COVER"

    const/4 v2, 0x6

    const/16 v3, 0x96

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/cover"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_COVER:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 120
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_COVER_THUMBNAIL"

    const/4 v2, 0x7

    const/16 v3, 0x97

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/cover_thumbnail"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_COVER_THUMBNAIL:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 123
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_SECTIONS"

    const/16 v2, 0x8

    const/16 v3, 0xc8

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/segments"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 125
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_SECTIONS_ID"

    const/16 v2, 0x9

    const/16 v3, 0xc9

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/segments/*SEGMENT_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SECTIONS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 127
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_SECTIONS_CONTENT"

    const/16 v2, 0xa

    const/16 v3, 0xca

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/segments/*SEGMENT_ID/content"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SECTIONS_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 130
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_CHAPTERS"

    const/16 v2, 0xb

    const/16 v3, 0xcd

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/chapters"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 132
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_CHAPTERS_ID"

    const/16 v2, 0xc

    const/16 v3, 0xce

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/chapters/*CHAPTER_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 135
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_RES"

    const/16 v2, 0xd

    const/16 v3, 0x12c

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/res"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 136
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_RES_ID"

    const/16 v2, 0xe

    const/16 v3, 0x12d

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/res/*RES_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 138
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_RES_CONTENT"

    const/16 v2, 0xf

    const/16 v3, 0x12e

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/res/*RES_ID/content"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 141
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_RES_RES"

    const/16 v2, 0x10

    const/16 v3, 0x145

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/res_res"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 143
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_RES_RES_COMPOUND_ID"

    const/16 v2, 0x11

    const/16 v3, 0x146

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/res_res/compound_res/*RES_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_RES_COMPOUND_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 146
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_SEGMENT_RES"

    const/16 v2, 0x12

    const/16 v3, 0x15e

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/segment_res"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SEGMENT_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 148
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_SEGMENT_RES_ID"

    const/16 v2, 0x13

    const/16 v3, 0x15f

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/segment_res/segment/*SEGMENT_ID/res/*RES_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SEGMENT_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 151
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_PAGES"

    const/16 v2, 0x14

    const/16 v3, 0x190

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/pages"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 153
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_PAGES_ID"

    const/16 v2, 0x15

    const/16 v3, 0x191

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/pages/*PAGE_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 155
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_PAGES_CONTENT"

    const/16 v2, 0x16

    const/16 v3, 0x192

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/pages/*PAGE_ID/content"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 157
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUMES_PAGES_STRUCTURE"

    const/16 v2, 0x17

    const/16 v3, 0x193

    const-string v4, "accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID/pages/*PAGE_ID/structure"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_STRUCTURE:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 160
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SHARED_RES"

    const/16 v2, 0x18

    const/16 v3, 0x1a4

    const-string v4, "shared_res"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SHARED_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 161
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SHARED_RES_ID"

    const/16 v2, 0x19

    const/16 v3, 0x1a5

    const-string v4, "shared_res/*RES_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SHARED_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 163
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SESSION_KEYS"

    const/16 v2, 0x1a

    const/16 v3, 0x1c2

    const-string v4, "session_keys"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SESSION_KEYS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 164
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SESSION_KEYS_ID"

    const/16 v2, 0x1b

    const/16 v3, 0x1c3

    const-string v4, "session_keys/#SESSION_KEY_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SESSION_KEYS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 169
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SEARCH_SUGGEST"

    const/16 v2, 0x1c

    const/16 v3, 0x1f4

    const-string v4, "search_suggest_query/*QUERY"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SEARCH_SUGGEST:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 171
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SEARCH_SUGGEST_ZERO_QUERY"

    const/16 v2, 0x1d

    const/16 v3, 0x1f5

    const-string v4, "search_suggest_query/"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SEARCH_SUGGEST_ZERO_QUERY:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 173
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "SEARCH_SUGGEST_SHORTCUT_REFRESH"

    const/16 v2, 0x1e

    const/16 v3, 0x1f6

    const-string v4, "search_suggest_shortcut/*QUERY"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SEARCH_SUGGEST_SHORTCUT_REFRESH:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 176
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUME_STATES"

    const/16 v2, 0x1f

    const/16 v3, 0x258

    const-string v4, "states"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUME_STATES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 177
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNT_VOLUME_STATES"

    const/16 v2, 0x20

    const/16 v3, 0x259

    const-string v4, "states/accounts/*ACCOUNT_NAME"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_VOLUME_STATES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 178
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "VOLUME_STATES_ID"

    const/16 v2, 0x21

    const/16 v3, 0x25a

    const-string v4, "states/accounts/*ACCOUNT_NAME/volumes/*VOLUME_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUME_STATES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 181
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ALL_COLLECTIONS"

    const/16 v2, 0x22

    const/16 v3, 0x2bc

    const-string v4, "lib/accounts/collections"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ALL_COLLECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 182
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNT_COLLECTIONS"

    const/16 v2, 0x23

    const/16 v3, 0x2bd

    const-string v4, "lib/accounts/*ACCOUNT_NAME/collections"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_COLLECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 184
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNT_COLLECTIONS_ID"

    const/16 v2, 0x24

    const/16 v3, 0x2be

    const-string v4, "lib/accounts/*ACCOUNT_NAME/collections/#COLLECTION_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_COLLECTIONS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 186
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ALL_COLLECTION_VOLUMES"

    const/16 v2, 0x25

    const/16 v3, 0x2c6

    const-string v4, "lib/accounts/collections/volumes"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ALL_COLLECTION_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 188
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "COLLECTION_VOLUMES"

    const/16 v2, 0x26

    const/16 v3, 0x2c7

    const-string v4, "lib/accounts/*ACCOUNT_NAME/collections/#COLLECTION_ID/volumes"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->COLLECTION_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 190
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "COLLECTION_VOLUMES_ID"

    const/16 v2, 0x27

    const/16 v3, 0x2c8

    const-string v4, "lib/accounts/*ACCOUNT_NAME/collections/#COLLECTION_ID/volumes/*VOLUME_ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->COLLECTION_VOLUMES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 193
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "CONFIGURATION"

    const/16 v2, 0x28

    const/16 v3, 0x320

    const-string v4, "config"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->CONFIGURATION:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 195
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "FILES"

    const/16 v2, 0x29

    const/16 v3, 0x384

    const-string v4, "files"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->FILES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 200
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const-string v1, "ACCOUNT_CONTENT"

    const/16 v2, 0x2a

    const/16 v3, 0x3ea

    const-string v4, "accounts/*ACCOUNT_NAME/content"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 94
    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->INTENT_VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNTS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNTS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_COVER:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_COVER_THUMBNAIL:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SECTIONS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SECTIONS_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_RES_RES_COMPOUND_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SEGMENT_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_SEGMENT_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_PAGES_STRUCTURE:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SHARED_RES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SHARED_RES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SESSION_KEYS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SESSION_KEYS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SEARCH_SUGGEST:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SEARCH_SUGGEST_ZERO_QUERY:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->SEARCH_SUGGEST_SHORTCUT_REFRESH:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUME_STATES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_VOLUME_STATES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUME_STATES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ALL_COLLECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_COLLECTIONS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_COLLECTIONS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ALL_COLLECTION_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->COLLECTION_VOLUMES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->COLLECTION_VOLUMES_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->CONFIGURATION:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->FILES:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->ACCOUNT_CONTENT:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    .line 202
    const-string v0, "/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->pathSeparator:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 2
    .param p3, "matchCode"    # I
    .param p4, "encodedPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 207
    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mParts:[Ljava/lang/String;

    .line 208
    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mWildcardSlots:Ljava/util/Map;

    .line 221
    iput p3, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->code:I

    .line 222
    const-string v0, "([#*])[^/]+"

    const-string v1, "$1"

    invoke-virtual {p4, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->matchPath:Ljava/lang/String;

    .line 223
    iput-object p4, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mEncodedPath:Ljava/lang/String;

    .line 224
    # getter for: Lcom/google/android/apps/books/provider/BooksContract;->sMatchCodeToUri:Landroid/util/SparseArray;
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract;->access$000()Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->code:I

    invoke-virtual {v0, v1, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 225
    return-void
.end method

.method private getParts()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mParts:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 263
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->pathSeparator:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mEncodedPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mParts:[Ljava/lang/String;

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mParts:[Ljava/lang/String;

    return-object v0
.end method

.method private isWildcard(Ljava/lang/String;)Z
    .locals 3
    .param p1, "part"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 285
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 289
    :cond_0
    :goto_0
    return v1

    .line 288
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 289
    .local v0, "firstChar":C
    const/16 v2, 0x2a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x23

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 94
    const-class v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    return-object v0
.end method


# virtual methods
.method public varargs builder([Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 10
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 233
    array-length v7, p1

    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getWildcardSlots()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    if-eq v7, v8, :cond_0

    .line 234
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Can\'t fit "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " params into pattern "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->matchPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 237
    :cond_0
    const/4 v4, 0x0

    .line 238
    .local v4, "paramIndex":I
    sget-object v7, Lcom/google/android/apps/books/provider/BooksContract;->BASE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 239
    .local v1, "builder":Landroid/net/Uri$Builder;
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getParts()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v5, v4

    .end local v4    # "paramIndex":I
    .local v5, "paramIndex":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v6, v0, v2

    .line 240
    .local v6, "part":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->isWildcard(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 241
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "paramIndex":I
    .restart local v4    # "paramIndex":I
    aget-object v7, p1, v5

    invoke-static {v7}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 239
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "paramIndex":I
    .restart local v5    # "paramIndex":I
    goto :goto_0

    .line 243
    :cond_1
    invoke-virtual {v1, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move v4, v5

    .end local v5    # "paramIndex":I
    .restart local v4    # "paramIndex":I
    goto :goto_1

    .line 246
    .end local v4    # "paramIndex":I
    .end local v6    # "part":Ljava/lang/String;
    .restart local v5    # "paramIndex":I
    :cond_2
    return-object v1
.end method

.method public getWildcard(Landroid/net/Uri;Lcom/google/android/apps/books/provider/BooksContract$PathParam;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "key"    # Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getWildcardSlots()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 258
    .local v0, "location":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method public getWildcardSlots()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/provider/BooksContract$PathParam;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mWildcardSlots:Ljava/util/Map;

    if-nez v3, :cond_2

    .line 271
    new-instance v2, Ljava/util/EnumMap;

    const-class v3, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-direct {v2, v3}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 273
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/provider/BooksContract$PathParam;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getParts()[Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "parts":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 275
    aget-object v3, v1, v0

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->isWildcard(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 276
    aget-object v3, v1, v0

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mWildcardSlots:Ljava/util/Map;

    .line 281
    .end local v0    # "i":I
    .end local v1    # "parts":[Ljava/lang/String;
    .end local v2    # "result":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/provider/BooksContract$PathParam;Ljava/lang/Integer;>;"
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->mWildcardSlots:Ljava/util/Map;

    return-object v3
.end method
