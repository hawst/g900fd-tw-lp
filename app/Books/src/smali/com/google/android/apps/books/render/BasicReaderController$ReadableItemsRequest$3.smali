.class Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$OnReadableItemsReadyHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->handleExactPassage(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private logRequestIds()V
    .locals 3

    .prologue
    .line 364
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delivering request ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v2, v2, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mExternalRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for internal request ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v2, v2, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_0
    return-void
.end method


# virtual methods
.method public run(ILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 6
    .param p1, "passageIndex"    # I
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p4, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->logRequestIds()V

    .line 357
    if-eqz p3, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mExternalRequestId:I

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/ReaderListener;->onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 361
    :cond_0
    return-void
.end method

.method public run(ILjava/util/Map;)V
    .locals 2
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    .local p2, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->logRequestIds()V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$3;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mExternalRequestId:I

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/apps/books/render/ReaderListener;->onPassageMoListReady(IILjava/util/Map;)V

    .line 350
    return-void
.end method
