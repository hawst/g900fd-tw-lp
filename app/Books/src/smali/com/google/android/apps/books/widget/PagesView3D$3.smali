.class Lcom/google/android/apps/books/widget/PagesView3D$3;
.super Ljava/lang/Object;
.source "PagesView3D.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/PagesView3D;->onLoadingPageVisibilityChanged(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesView3D;

.field final synthetic val$loadingPageVisible:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesView3D;Z)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesView3D$3;->this$0:Lcom/google/android/apps/books/widget/PagesView3D;

    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/PagesView3D$3;->val$loadingPageVisible:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D$3;->this$0:Lcom/google/android/apps/books/widget/PagesView3D;

    # getter for: Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->access$000(Lcom/google/android/apps/books/widget/PagesView3D;)Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D$3;->this$0:Lcom/google/android/apps/books/widget/PagesView3D;

    # getter for: Lcom/google/android/apps/books/widget/PagesView3D;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->access$100(Lcom/google/android/apps/books/widget/PagesView3D;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D$3;->this$0:Lcom/google/android/apps/books/widget/PagesView3D;

    # getter for: Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesView3D;->access$000(Lcom/google/android/apps/books/widget/PagesView3D;)Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesView3D$3;->val$loadingPageVisible:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->onLoadingPageVisibilityChanged(Z)V

    .line 600
    :cond_0
    return-void
.end method
