.class Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$1;
.super Ljava/lang/Object;
.source "CountryNotSupportedDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 39
    iget-object v3, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 40
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/OceanUris;->getInternationalAvailabilityUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;

    move-result-object v1

    .line 42
    .local v1, "helpUrl":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 43
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method
