.class Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;
.super Ljava/lang/Object;
.source "BooksCardsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BooksCardsAdapter;->bindToVolume(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/model/VolumeData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

.field final synthetic val$volume:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 385
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5$1;-><init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;Lcom/google/android/apps/books/common/ImageCallback;)V

    .line 393
    .local v0, "wrappedCallback":Lcom/google/android/apps/books/common/ImageCallback;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->access$200(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$5;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1, p1, p2, v0, v2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;->getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v1

    return-object v1
.end method
