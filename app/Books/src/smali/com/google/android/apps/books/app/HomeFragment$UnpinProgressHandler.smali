.class Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnpinProgressHandler"
.end annotation


# instance fields
.field private mCanceled:Z

.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V
    .locals 0
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2348
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349
    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mVolumeId:Ljava/lang/String;

    .line 2350
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mCanceled:Z

    .line 2375
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$4200(Lcom/google/android/apps/books/app/HomeFragment;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mVolumeId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2376
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2364
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mCanceled:Z

    if-nez v0, :cond_0

    .line 2365
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mVolumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->onUnpinAnimationEnd(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$4100(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    .line 2366
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$4200(Lcom/google/android/apps/books/app/HomeFragment;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mVolumeId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2368
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2369
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mVolumeId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BooksHomeView;->volumeDownloadFractionAnimationDone(Ljava/lang/String;)V

    .line 2371
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2379
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 2361
    return-void
.end method

.method public setProgress(F)V
    .locals 2
    .param p1, "progressFraction"    # F

    .prologue
    .line 2354
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2355
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;->mVolumeId:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/widget/BooksHomeView;->setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V

    .line 2357
    :cond_0
    return-void
.end method
