.class public Lcom/google/android/apps/books/eob/EndOfBookCardProvider;
.super Lcom/google/android/apps/books/app/InfoCardProvider;
.source "EndOfBookCardProvider.java"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mDayThemeContext:Landroid/content/Context;

.field private mFromEobb:Z

.field private final mHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

.field private mLastConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private mRecommendations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 1
    .param p1, "dayThemeContext"    # Landroid/content/Context;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardProvider;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;-><init>(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mDayThemeContext:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mActivity:Landroid/app/Activity;

    .line 74
    iput-object p3, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mDayThemeContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookCardProvider;
    .param p1, "x1"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-object p1
.end method

.method private getCampaignId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "books_inapp_end_of_sample"

    .line 120
    :goto_0
    return-object v0

    .line 117
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mFromEobb:Z

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "books_inapp_end_of_book_body"

    goto :goto_0

    .line 120
    :cond_1
    const-string v0, "books_inapp_end_of_book"

    goto :goto_0
.end method

.method private getHeadingText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00b0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isOnline()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private maybeAddCards()V
    .locals 11

    .prologue
    .line 83
    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mRecommendations:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->isOnline()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const v1, 0x7f0400b6

    iget-object v2, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mDayThemeContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/ViewUtils;->inflateWithContext(ILandroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v10

    .line 86
    .local v10, "endOfBookViewGroup":Landroid/view/ViewGroup;
    invoke-static {v10}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->find(Landroid/view/View;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    move-result-object v0

    .line 89
    .local v0, "endOfBookView":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
    new-instance v8, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;

    invoke-direct {v8, p0, v10}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$2;-><init>(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;Landroid/view/ViewGroup;)V

    .line 101
    .local v8, "listener":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mRecommendations:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->getCampaignId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->getHeadingText()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    new-instance v6, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$3;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$3;-><init>(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)V

    const/4 v7, 0x0

    iget-boolean v9, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mFromEobb:Z

    if-eqz v9, :cond_1

    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->END_OF_BOOK_BODY_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    :goto_0
    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->setup(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 112
    .end local v0    # "endOfBookView":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
    .end local v8    # "listener":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    .end local v10    # "endOfBookViewGroup":Landroid/view/ViewGroup;
    :cond_0
    return-void

    .line 101
    .restart local v0    # "endOfBookView":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
    .restart local v8    # "listener":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    .restart local v10    # "endOfBookViewGroup":Landroid/view/ViewGroup;
    :cond_1
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->END_OF_BOOK_PAGE_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    goto :goto_0
.end method


# virtual methods
.method public loadEndOfBookCards(ZLcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p1, "fromEobb"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    iput-object p2, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mLastConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 133
    iput-boolean p1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mFromEobb:Z

    .line 134
    invoke-direct {p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->maybeAddCards()V

    .line 135
    return-void
.end method

.method public setRecommendations(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "t":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mRecommendations:Ljava/util/List;

    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->maybeAddCards()V

    .line 80
    return-void
.end method
