.class public Lcom/google/android/apps/books/widget/RecommendationCardViewRow;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.source "RecommendationCardViewRow.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onFinishInflate()V

    .line 80
    const v0, 0x7f0e01b5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mActionTouchArea:Landroid/view/View;

    .line 81
    const v0, 0x7f0e00eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mAction:Landroid/widget/TextView;

    .line 82
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 50
    sub-int v4, p5, p3

    .line 52
    .local v4, "height":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v8

    sub-int v8, v4, v8

    div-int/lit8 v7, v8, 0x2

    .line 53
    .local v7, "thumbnailTop":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingLeft()I

    move-result v5

    .line 54
    .local v5, "thumbnailLeft":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v8}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v8

    add-int v6, v5, v8

    .line 55
    .local v6, "thumbnailRight":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    invoke-virtual {v8, v5, v7, v6, v9}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->layout(IIII)V

    .line 58
    iget-object v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 59
    .local v2, "detailsLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 60
    .local v3, "detailsTop":I
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v1, v8, v6

    .line 64
    .local v1, "detailsLeft":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090117

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v0, v8

    .line 67
    .local v0, "detailsBottomPadding":I
    iget-object v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mDetailsView:Landroid/view/ViewGroup;

    iget-object v9, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v1

    iget-object v10, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v3

    sub-int/2addr v10, v0

    invoke-virtual {v8, v1, v3, v9, v10}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 71
    iget-object v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingLeft()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingTop()I

    move-result v10

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getMeasuredWidth()I

    move-result v11

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingRight()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getMeasuredHeight()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingBottom()I

    move-result v13

    sub-int/2addr v12, v13

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 73
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v3, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 32
    .local v3, "height":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 33
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingLeft()I

    move-result v7

    sub-int v7, v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->getPaddingRight()I

    move-result v8

    sub-int v0, v7, v8

    .line 35
    .local v0, "availableChildWidth":I
    int-to-float v7, v3

    iget v8, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mThumbnailAspectRatio:F

    div-float/2addr v7, v8

    float-to-int v4, v7

    .line 36
    .local v4, "thumbnailWidth":I
    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 38
    .local v5, "thumbnailWidthSpec":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mThumbnail:Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;

    invoke-virtual {v7, v5, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->measure(II)V

    .line 40
    sub-int v1, v0, v4

    .line 41
    .local v1, "detailsWidth":I
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 43
    .local v2, "detailsWidthSpec":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->mDetailsView:Landroid/view/ViewGroup;

    invoke-virtual {v7, v2, p2}, Landroid/view/ViewGroup;->measure(II)V

    .line 45
    invoke-virtual {p0, v6, v3}, Lcom/google/android/apps/books/widget/RecommendationCardViewRow;->setMeasuredDimension(II)V

    .line 46
    return-void
.end method
