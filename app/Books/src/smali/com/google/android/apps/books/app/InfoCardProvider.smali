.class public abstract Lcom/google/android/apps/books/app/InfoCardProvider;
.super Ljava/lang/Object;
.source "InfoCardProvider.java"


# static fields
.field public static final GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSelectableLayerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public loadCards(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "selectedText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    sget-object v0, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 39
    return-void
.end method

.method public loadCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    sget-object v0, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method public loadEndOfBookCards(ZLcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "fromEobb"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    sget-object v0, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method public loadLinkCards(Lcom/google/android/apps/books/render/TouchableItem;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "item"    # Lcom/google/android/apps/books/render/TouchableItem;
    .param p2, "textZoom"    # F
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "screenHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            "F",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p5, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    sget-object v0, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p5, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 62
    return-void
.end method
