.class public Lcom/google/android/apps/books/render/SnapshottingWebView;
.super Landroid/webkit/WebView;
.source "SnapshottingWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/SnapshottingWebView$1;,
        Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;,
        Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;,
        Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;
    }
.end annotation


# static fields
.field private static final DRAWING_CACHE_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;


# instance fields
.field private final mDrawDelegate:Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

.field private mInvalidationListener:Ljava/lang/Runnable;

.field private mNotifyOnInvalidate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/books/render/SnapshottingWebView;->DRAWING_CACHE_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "drawDelegate"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mNotifyOnInvalidate:Z

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mDrawDelegate:Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    .line 75
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/ublib/utils/AccessibilityUtils;->setImportantForAccessibility(Landroid/view/View;Z)V

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;
    .param p3, "x2"    # Lcom/google/android/apps/books/render/SnapshottingWebView$1;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/SnapshottingWebView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V

    return-void
.end method

.method public static create(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)Lcom/google/android/apps/books/render/SnapshottingWebView;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "drawDelegate"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    .prologue
    const/4 v3, 0x0

    .line 50
    const-string v1, "BOOKS_OSWV"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 51
    .local v0, "isLoggable":Z
    invoke-static {}, Lcom/google/android/apps/books/render/SnapshottingWebView;->needCustomDrawingCache()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    if-eqz v0, :cond_0

    .line 53
    const-string v1, "BOOKS_OSWV"

    const-string v2, "Creating special OSWV"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;

    invoke-direct {v1, p0, p1, v3}, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V

    .line 68
    :goto_0
    return-object v1

    .line 56
    :cond_1
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 57
    if-eqz v0, :cond_2

    .line 58
    const-string v1, "BOOKS_OSWV"

    const-string v2, "Creating KLP+ OSWV"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_2
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 61
    invoke-static {}, Landroid/webkit/WebView;->enableSlowWholeDocumentDraw()V

    .line 63
    :cond_3
    new-instance v1, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;

    invoke-direct {v1, p0, p1, v3}, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewKlpPlus;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V

    goto :goto_0

    .line 65
    :cond_4
    if-eqz v0, :cond_5

    .line 66
    const-string v1, "BOOKS_OSWV"

    const-string v2, "Creating normal OSWV"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_5
    new-instance v1, Lcom/google/android/apps/books/render/SnapshottingWebView;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/render/SnapshottingWebView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V

    goto :goto_0
.end method

.method private maybeNotifyInvalidationListener()V
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mNotifyOnInvalidate:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mInvalidationListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mInvalidationListener:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 164
    :cond_0
    return-void
.end method

.method private static needCustomDrawingCache()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public invalidate()V
    .locals 2

    .prologue
    .line 145
    const-string v0, "BOOKS_OSWV"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "BOOKS_OSWV"

    const-string v1, "invalidate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    invoke-super {p0}, Landroid/webkit/WebView;->invalidate()V

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->maybeNotifyInvalidationListener()V

    .line 150
    return-void
.end method

.method public invalidate(IIII)V
    .locals 6
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    const/4 v5, 0x3

    .line 136
    const-string v0, "BOOKS_OSWV"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "BOOKS_OSWV"

    const-string v1, "invalidate(%d, %d, %d, %d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->invalidate(IIII)V

    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->maybeNotifyInvalidationListener()V

    .line 141
    return-void
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "dirty"    # Landroid/graphics/Rect;

    .prologue
    .line 127
    const-string v0, "BOOKS_OSWV"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "BOOKS_OSWV"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalidate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->invalidate(Landroid/graphics/Rect;)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->maybeNotifyInvalidationListener()V

    .line 132
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mDrawDelegate:Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mDrawDelegate:Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;->onDraw(Landroid/graphics/Canvas;)V

    .line 117
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public setInvalidationListener(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "listener"    # Ljava/lang/Runnable;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/books/render/SnapshottingWebView;->mInvalidationListener:Ljava/lang/Runnable;

    .line 123
    return-void
.end method

.method public snapShot(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onDraw(Landroid/graphics/Canvas;)V

    .line 107
    return-void
.end method
