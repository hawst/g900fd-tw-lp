.class final Lcom/google/android/apps/books/app/ReaderFragment$28;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->createNoLinkCardsRunnable(Lcom/google/android/apps/books/render/TouchableItem;Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/LeakSafeRunnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$item:Lcom/google/android/apps/books/render/TouchableItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 0

    .prologue
    .line 5603
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$28;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 1
    .param p1, "frag"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 5606
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$28;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/TouchableItem;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->navigateToLinkPosition(Lcom/google/android/apps/books/common/Position;)V
    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12700(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/common/Position;)V

    .line 5607
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 5603
    check-cast p1, Lcom/google/android/apps/books/app/ReaderFragment;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$28;->take(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method
