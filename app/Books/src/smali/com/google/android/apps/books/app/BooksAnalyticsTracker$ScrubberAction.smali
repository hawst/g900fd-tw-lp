.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScrubberAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

.field public static final enum SCRUBBER_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

.field public static final enum SCRUBBER_QUICK_BOOKMARK_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

.field public static final enum SCRUBBER_UNDO_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 181
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    const-string v1, "SCRUBBER_CLICKED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    .line 185
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    const-string v1, "SCRUBBER_UNDO_CLICKED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_UNDO_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    .line 189
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    const-string v1, "SCRUBBER_QUICK_BOOKMARK_CLICKED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_QUICK_BOOKMARK_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    .line 177
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_UNDO_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_QUICK_BOOKMARK_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 177
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    return-object v0
.end method
