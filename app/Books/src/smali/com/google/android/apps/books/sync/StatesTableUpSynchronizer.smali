.class public Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;
.super Ljava/lang/Object;
.source "StatesTableUpSynchronizer.java"


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

.field private final mConfig:Lcom/google/android/apps/books/util/Config;

.field private final mSynchronizable:Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

.field private final mTime:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/sync/StatesServerSynchronizable;Lcom/google/android/apps/books/api/ApiaryClient;Landroid/accounts/Account;Lcom/google/android/apps/books/util/Config;)V
    .locals 2
    .param p1, "synchronizable"    # Lcom/google/android/apps/books/sync/StatesServerSynchronizable;
    .param p2, "apiaryClient"    # Lcom/google/android/apps/books/api/ApiaryClient;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, "missing synchronizable"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    .line 47
    const-string v0, "missing account"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mAccount:Landroid/accounts/Account;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    .line 50
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mTime:Landroid/text/format/Time;

    .line 51
    iput-object p4, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 52
    return-void
.end method

.method private queryForUpsyncValues()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v3, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    invoke-virtual {v3}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->queryAllForUpsync()Landroid/database/Cursor;

    move-result-object v0

    .line 83
    .local v0, "allCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 85
    .local v2, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    invoke-virtual {v3}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->getWritableColumnToClass()Ljava/util/Map;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/books/sync/SyncUtil;->cursorToValues(Landroid/database/Cursor;Ljava/util/Map;)Landroid/content/ContentValues;

    move-result-object v1

    .line 88
    .local v1, "values":Landroid/content/ContentValues;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 92
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3

    .restart local v2    # "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v2
.end method

.method private upsyncUsingApiary(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "timestamp"    # Ljava/lang/String;
    .param p4, "action"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v2, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1, p2, p3, p4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forSetReadingPosition(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 100
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v2, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-string v3, ""

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 101
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    sget-object v2, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->SET_READING_POSITION:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v2}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 103
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v4, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 104
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 108
    return-void

    .line 106
    :catchall_0
    move-exception v2

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v2
.end method


# virtual methods
.method public upsync()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->queryForUpsyncValues()Ljava/util/List;

    move-result-object v6

    .line 56
    .local v6, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    .line 57
    .local v5, "values":Landroid/content/ContentValues;
    const-string v8, "volume_id"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 58
    .local v7, "volumeId":Ljava/lang/String;
    const-string v8, "position"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "position":Ljava/lang/String;
    const-string v8, "last_access"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 60
    .local v0, "accessMillis":Ljava/lang/Long;
    const-string v8, "last_action"

    invoke-virtual {v5, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "actionString":Ljava/lang/String;
    const-string v8, "missing volumeId"

    invoke-static {v7, v8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v8, "missing position"

    invoke-static {v3, v8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-string v8, "missing accessMillis"

    invoke-static {v0, v8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v8, "missing action"

    invoke-static {v1, v8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v8, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 68
    iget-object v8, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mTime:Landroid/text/format/Time;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v4

    .line 69
    .local v4, "time3339":Ljava/lang/String;
    const-string v8, "StatesTableUpSynch"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 70
    const-string v8, "StatesTableUpSynch"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Upsync position "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->actionWithOceanNameOrDefault(Ljava/lang/String;)Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    move-result-object v8

    invoke-direct {p0, v7, v3, v4, v8}, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->upsyncUsingApiary(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;)V

    .line 76
    iget-object v8, p0, Lcom/google/android/apps/books/sync/StatesTableUpSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;->updateAfterUpsync(Landroid/content/ContentValues;)I

    goto/16 :goto_0

    .line 78
    .end local v0    # "accessMillis":Ljava/lang/Long;
    .end local v1    # "actionString":Ljava/lang/String;
    .end local v3    # "position":Ljava/lang/String;
    .end local v4    # "time3339":Ljava/lang/String;
    .end local v5    # "values":Landroid/content/ContentValues;
    .end local v7    # "volumeId":Ljava/lang/String;
    :cond_1
    return-void
.end method
