.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->uiAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Updateable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

.field final synthetic val$updateable:Lcom/google/android/apps/books/annotations/Updateable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Lcom/google/android/apps/books/annotations/Updateable;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->val$updateable:Lcom/google/android/apps/books/annotations/Updateable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 230
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->val$updateable:Lcom/google/android/apps/books/annotations/Updateable;

    .line 231
    .local v2, "improving":Lcom/google/android/apps/books/annotations/Updateable;, "Lcom/google/android/apps/books/annotations/Updateable<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :goto_0
    invoke-interface {v2}, Lcom/google/android/apps/books/annotations/Updateable;->canBeUpdated()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 232
    invoke-interface {v2}, Lcom/google/android/apps/books/annotations/Updateable;->update()Lcom/google/android/apps/books/annotations/Updateable;

    move-result-object v2

    .line 233
    invoke-interface {v2}, Lcom/google/android/apps/books/annotations/Updateable;->currentBestValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/Annotation;

    .line 234
    .local v1, "currentBestValue":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v4, v4, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v5, v5, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    new-instance v6, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1$1;

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;Lcom/google/android/apps/books/annotations/Annotation;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    goto :goto_0

    .line 243
    .end local v1    # "currentBestValue":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    invoke-interface {v2}, Lcom/google/android/apps/books/annotations/Updateable;->currentBestValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 244
    .local v0, "a":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/ServerAnnotation;->locallyCreated(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/ServerAnnotation;

    move-result-object v3

    .line 245
    .local v3, "sa":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v4, v4, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v4}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v5, v5, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    sget-object v6, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->UNKNOWN_SEGMENT_INDEX:Ljava/lang/Integer;

    invoke-interface {v4, v5, v3, v6}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->addUserAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;)V

    .line 247
    return-void
.end method
