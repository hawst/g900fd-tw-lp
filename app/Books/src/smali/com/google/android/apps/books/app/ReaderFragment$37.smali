.class Lcom/google/android/apps/books/app/ReaderFragment$37;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->createPagesViewOnHoverListener(Z)Landroid/view/View$OnHoverListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$isForEob:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 1

    .prologue
    .line 7989
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-boolean p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->val$isForEob:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7995
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    return-void
.end method

.method private handlePageZoneHover(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v4, 0x7f0f011f

    const v3, 0x7f0f011e

    .line 8064
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/android/apps/books/util/PagesViewUtils;->getZone(FI)Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    move-result-object v2

    .line 8065
    .local v2, "zone":Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    if-ne v2, v5, :cond_1

    .line 8101
    :cond_0
    :goto_0
    return-void

    .line 8068
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 8069
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 8073
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v1, 0x1

    .line 8074
    .local v1, "isRtl":Z
    :goto_1
    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$42;->$SwitchMap$com$google$android$apps$books$widget$PagesView$TouchZone:[I

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 8076
    :pswitch_0
    iget-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->val$isForEob:Z

    if-eqz v5, :cond_2

    if-nez v1, :cond_0

    .line 8077
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    if-eqz v1, :cond_4

    :goto_2
    invoke-virtual {v5, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8079
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/AccessibilityUtils;->sendHoverEvent(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 8073
    .end local v0    # "description":Ljava/lang/String;
    .end local v1    # "isRtl":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .restart local v1    # "isRtl":Z
    :cond_4
    move v3, v4

    .line 8077
    goto :goto_2

    .line 8084
    :pswitch_1
    iget-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->val$isForEob:Z

    if-eqz v5, :cond_5

    if-eqz v1, :cond_0

    .line 8085
    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    if-eqz v1, :cond_6

    :goto_3
    invoke-virtual {v5, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8088
    .restart local v0    # "description":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/AccessibilityUtils;->sendHoverEvent(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "description":Ljava/lang/String;
    :cond_6
    move v4, v3

    .line 8085
    goto :goto_3

    .line 8093
    :pswitch_2
    iget-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->val$isForEob:Z

    if-nez v3, :cond_0

    .line 8096
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const v6, 0x7f0f0120

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/util/AccessibilityUtils;->sendHoverEvent(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 8074
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private maybeHandleBookmarkHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 8038
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookmarkToggleTargetPage(Landroid/view/MotionEvent;)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 8040
    .local v0, "bookmarkTogglePage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v0, :cond_3

    .line 8041
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    sget-object v6, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->BOOKMARK:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    if-eq v3, v6, :cond_1

    .line 8042
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/Eventual;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 8043
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/Eventual;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/app/BookmarkController;

    new-array v6, v4, [Lcom/google/android/apps/books/widget/DevicePageRendering;

    aput-object v0, v6, v5

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/app/BookmarkController;->pagesContainBookmark(Ljava/util/List;)Z

    move-result v1

    .line 8046
    .local v1, "hasBookmark":Z
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    if-eqz v1, :cond_2

    const v3, 0x7f0f0144

    :goto_0
    invoke-virtual {v5, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 8049
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v5

    invoke-static {v3, v5, v2}, Lcom/google/android/apps/books/util/AccessibilityUtils;->sendHoverEvent(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 8052
    .end local v1    # "hasBookmark":Z
    .end local v2    # "message":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->BOOKMARK:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    :cond_1
    move v3, v4

    .line 8056
    :goto_1
    return v3

    .line 8046
    .restart local v1    # "hasBookmark":Z
    :cond_2
    const v3, 0x7f0f0143

    goto :goto_0

    .end local v1    # "hasBookmark":Z
    :cond_3
    move v3, v5

    .line 8056
    goto :goto_1
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v7, 0x9

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 7999
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v5

    if-nez v5, :cond_2

    :cond_0
    move v3, v4

    .line 8027
    :cond_1
    :goto_0
    return v3

    .line 8002
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInHoverGracePeriod:Z
    invoke-static {v5, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8003
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 8004
    .local v0, "action":I
    if-ne v0, v7, :cond_3

    .line 8005
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybePausePlayback(Z)V
    invoke-static {v5, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16700(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 8007
    :cond_3
    if-eq v0, v7, :cond_4

    const/4 v4, 0x7

    if-ne v0, v4, :cond_6

    .line 8009
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 8013
    .local v2, "oldZone":Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment$37;->maybeHandleBookmarkHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    .line 8015
    .local v1, "inBookmarkZone":Z
    if-nez v1, :cond_5

    .line 8016
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment$37;->handlePageZoneHover(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 8019
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    if-eq v4, v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 8021
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    .line 8023
    .end local v1    # "inBookmarkZone":Z
    .end local v2    # "oldZone":Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    :cond_6
    const/16 v4, 0xa

    if-ne v0, v4, :cond_1

    .line 8024
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->mCurrentZone:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 8025
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$37;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->endHoverGracePeriodDelayed()V
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_0
.end method
