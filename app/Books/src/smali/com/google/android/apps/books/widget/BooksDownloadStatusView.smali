.class public Lcom/google/android/apps/books/widget/BooksDownloadStatusView;
.super Landroid/view/View;
.source "BooksDownloadStatusView.java"


# static fields
.field private static final sArgbEvaluator:Landroid/animation/ArgbEvaluator;

.field protected static sSerial:I


# instance fields
.field protected final mArcRect:Landroid/graphics/RectF;

.field protected final mBackgroundRect:Landroid/graphics/RectF;

.field protected mCurrentFrameIndex:I

.field protected mDisplayedDownloadFraction:F

.field protected mDocumentId:Ljava/lang/String;

.field protected mDownloadFraction:F

.field protected mDownloadFractionIsIncreasing:Z

.field protected mDownloadFractionLastUpdatedTime:J

.field protected mDownloadFractionSet:Z

.field protected mDownloadRequested:Z

.field protected mDownloadRequestedSet:Z

.field protected mDrawableDownload:Landroid/graphics/drawable/AnimationDrawable;

.field protected mDrawableFinish:Landroid/graphics/drawable/AnimationDrawable;

.field protected mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

.field protected mHighlightColorOffline:I

.field protected mHighlightColorOnline:I

.field protected mIsOnline:Z

.field protected mMinInProgressDisplayFraction:F

.field protected mNextFrameTime:J

.field protected mProgressArcBackgroundColorDownload:I

.field protected mProgressArcBackgroundColorPress:I

.field protected mProgressArcBackgroundColorTransitionDoneTime:J

.field protected mProgressArcBackgroundColorTransitionIntervalMs:J

.field protected mProgressArcBackgroundColorTransitionStepMs:J

.field protected mProgressArcBackgroundIsColorPress:Z

.field protected mProgressArcWidth:F

.field protected mSerial:I

.field protected mStartAnimation:Z

.field protected mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->sSerial:I

    .line 144
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->sArgbEvaluator:Landroid/animation/ArgbEvaluator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 206
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 207
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 202
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 203
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 197
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114
    sget v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->sSerial:I

    iput v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mSerial:I

    .line 116
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequested:Z

    .line 117
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequestedSet:Z

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mIsOnline:Z

    .line 119
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 124
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    .line 126
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionSet:Z

    .line 127
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionIsIncreasing:Z

    .line 132
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDisplayedDownloadFraction:F

    .line 189
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    .line 193
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mBackgroundRect:Landroid/graphics/RectF;

    .line 198
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 199
    return-void
.end method

.method private drawNextFrame(Landroid/graphics/drawable/AnimationDrawable;Landroid/graphics/Canvas;Z)J
    .locals 10
    .param p1, "animationDrawable"    # Landroid/graphics/drawable/AnimationDrawable;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "advanceFrame"    # Z

    .prologue
    const-wide/16 v6, 0x0

    .line 637
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 638
    .local v4, "now":J
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    invoke-virtual {p1, v3}, Landroid/graphics/drawable/AnimationDrawable;->selectDrawable(I)Z

    .line 639
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    invoke-virtual {p1, v3}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 641
    if-eqz p3, :cond_0

    iget-wide v8, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mNextFrameTime:J

    cmp-long v3, v4, v8

    if-gez v3, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-wide v6

    .line 644
    :cond_1
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    invoke-virtual {p1, v3}, Landroid/graphics/drawable/AnimationDrawable;->getDuration(I)I

    move-result v3

    int-to-long v0, v3

    .line 645
    .local v0, "duration":J
    invoke-virtual {p1}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .line 646
    .local v2, "lastFrameIndex":I
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    if-ge v3, v2, :cond_2

    .line 647
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 655
    :goto_1
    add-long v6, v4, v0

    iput-wide v6, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mNextFrameTime:J

    .line 656
    iget-wide v6, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mNextFrameTime:J

    goto :goto_0

    .line 649
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/drawable/AnimationDrawable;->isOneShot()Z

    move-result v3

    if-nez v3, :cond_0

    .line 652
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    goto :goto_1
.end method

.method private getAnimationBackgroundPaint()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 311
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 312
    .local v0, "result":Landroid/graphics/Paint;
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getHighlightColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 313
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 314
    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 315
    return-object v0
.end method

.method private getArcBackgroundPaint()Landroid/graphics/Paint;
    .locals 8

    .prologue
    .line 342
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 344
    .local v4, "result":Landroid/graphics/Paint;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 345
    .local v2, "now":J
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundIsColorPress:Z

    if-eqz v5, :cond_0

    .line 346
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorPress:I

    .line 357
    .local v0, "color":I
    :goto_0
    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 358
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 359
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 360
    iget v5, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcWidth:F

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 361
    return-object v4

    .line 347
    .end local v0    # "color":I
    :cond_0
    iget-wide v6, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionDoneTime:J

    cmp-long v5, v2, v6

    if-gez v5, :cond_1

    .line 349
    iget-wide v6, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionDoneTime:J

    sub-long/2addr v6, v2

    long-to-float v5, v6

    iget-wide v6, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionIntervalMs:J

    long-to-float v6, v6

    div-float v1, v5, v6

    .line 352
    .local v1, "fraction":F
    sget-object v5, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->sArgbEvaluator:Landroid/animation/ArgbEvaluator;

    iget v6, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorDownload:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget v7, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorPress:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v1, v6, v7}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 354
    .restart local v0    # "color":I
    goto :goto_0

    .line 355
    .end local v0    # "color":I
    .end local v1    # "fraction":F
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorDownload:I

    .restart local v0    # "color":I
    goto :goto_0
.end method

.method private getArcProgressPaint()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 323
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 324
    .local v0, "result":Landroid/graphics/Paint;
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getHighlightColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 325
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 326
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 327
    iget v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 328
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 329
    return-object v0
.end method

.method private getHighlightColor()I
    .locals 2

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mIsOnline:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mHighlightColorOnline:I

    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mHighlightColorOffline:I

    goto :goto_0
.end method

.method private initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 34
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 212
    sget v31, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->sSerial:I

    add-int/lit8 v31, v31, 0x1

    sput v31, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->sSerial:I

    .line 213
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    .line 214
    .local v29, "resources":Landroid/content/res/Resources;
    const v31, 0x7f0c0006

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v22

    .line 222
    .local v22, "fallbackMinInProgressDisplayPercent":I
    if-eqz p2, :cond_1

    .line 223
    sget-object v31, Lcom/google/android/apps/books/R$styleable;->BooksDownloadStatusView:[I

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v31

    move/from16 v3, v32

    move/from16 v4, v33

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 225
    .local v6, "a":Landroid/content/res/TypedArray;
    const/16 v31, 0x2

    const v32, 0x7f0b008e

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mHighlightColorOnline:I

    .line 228
    const/16 v31, 0x3

    const v32, 0x7f0b008e

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mHighlightColorOffline:I

    .line 231
    const/16 v31, 0x4

    move/from16 v0, v31

    move/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v27

    .line 234
    .local v27, "minInProgressDisplayPercent":I
    const/16 v31, 0x5

    const/16 v32, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v21

    .line 237
    .local v21, "drawablePressId":I
    const/16 v31, 0x6

    const/16 v32, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v19

    .line 240
    .local v19, "drawableDownloadId":I
    const/16 v31, 0x7

    const/16 v32, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v20

    .line 243
    .local v20, "drawableFinishId":I
    const/16 v31, 0x0

    const v32, 0x7f0901c6

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 246
    .local v8, "arcInsetId":I
    const/16 v31, 0x1

    const v32, 0x7f0901c8

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v12

    .line 249
    .local v12, "arcWidthId":I
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 254
    if-eqz v21, :cond_0

    if-eqz v19, :cond_0

    if-nez v20, :cond_2

    .line 256
    :cond_0
    const-string v31, "DSV"

    const-string v32, "missing animation resource"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    .end local v6    # "a":Landroid/content/res/TypedArray;
    .end local v8    # "arcInsetId":I
    .end local v12    # "arcWidthId":I
    .end local v19    # "drawableDownloadId":I
    .end local v20    # "drawableFinishId":I
    .end local v21    # "drawablePressId":I
    .end local v27    # "minInProgressDisplayPercent":I
    :goto_0
    return-void

    .line 251
    :cond_1
    const-string v31, "DSV"

    const-string v32, "missing attributeSet"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 259
    .restart local v6    # "a":Landroid/content/res/TypedArray;
    .restart local v8    # "arcInsetId":I
    .restart local v12    # "arcWidthId":I
    .restart local v19    # "drawableDownloadId":I
    .restart local v20    # "drawableFinishId":I
    .restart local v21    # "drawablePressId":I
    .restart local v27    # "minInProgressDisplayPercent":I
    :cond_2
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 260
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 261
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 262
    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v31

    check-cast v31, Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    .line 263
    move-object/from16 v0, v29

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v31

    check-cast v31, Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableDownload:Landroid/graphics/drawable/AnimationDrawable;

    .line 264
    move-object/from16 v0, v29

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v31

    check-cast v31, Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableFinish:Landroid/graphics/drawable/AnimationDrawable;

    .line 265
    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcWidth:F

    .line 266
    const v31, 0x7f0b010f

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorPress:I

    .line 268
    const v31, 0x7f0b0110

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorDownload:I

    .line 270
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundIsColorPress:Z

    .line 271
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionDoneTime:J

    .line 272
    const v31, 0x7f0c0015

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v31

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionIntervalMs:J

    .line 274
    const v31, 0x7f0c0016

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v31

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionStepMs:J

    .line 276
    move-object/from16 v0, v29

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    .line 277
    .local v14, "backgroundInset":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcWidth:F

    move/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    add-float v28, v14, v31

    .line 279
    .local v28, "progressArcInset":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getPaddingLeft()I

    move-result v26

    .line 280
    .local v26, "left":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getPaddingTop()I

    move-result v30

    .line 281
    .local v30, "top":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 282
    .local v13, "awaitingPressImage":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v25

    .line 283
    .local v25, "iconWidth":I
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v23

    .line 284
    .local v23, "iconHeight":I
    new-instance v24, Landroid/graphics/Rect;

    add-int v31, v26, v25

    add-int v32, v30, v23

    move-object/from16 v0, v24

    move/from16 v1, v26

    move/from16 v2, v30

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 286
    .local v24, "iconRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableDownload:Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableFinish:Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 290
    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v31, v0

    add-float v9, v31, v28

    .line 291
    .local v9, "arcLeft":F
    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v31, v0

    add-float v10, v31, v28

    .line 292
    .local v10, "arcTop":F
    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    mul-float v32, v32, v28

    sub-float v11, v31, v32

    .line 293
    .local v11, "arcWidth":F
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    mul-float v32, v32, v28

    sub-float v7, v31, v32

    .line 294
    .local v7, "arcHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    move-object/from16 v31, v0

    add-float v32, v9, v11

    add-float v33, v10, v7

    move-object/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 296
    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v31, v0

    add-float v16, v31, v14

    .line 297
    .local v16, "bgLeft":F
    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v31, v0

    add-float v17, v31, v14

    .line 298
    .local v17, "bgTop":F
    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    mul-float v32, v32, v14

    sub-float v18, v31, v32

    .line 299
    .local v18, "bgWidth":F
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    mul-float v32, v32, v14

    sub-float v15, v31, v32

    .line 300
    .local v15, "bgHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mBackgroundRect:Landroid/graphics/RectF;

    move-object/from16 v31, v0

    add-float v32, v16, v18

    add-float v33, v17, v15

    move-object/from16 v0, v31

    move/from16 v1, v16

    move/from16 v2, v17

    move/from16 v3, v32

    move/from16 v4, v33

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 302
    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v31, v0

    const/high16 v32, 0x42c80000    # 100.0f

    div-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mMinInProgressDisplayFraction:F

    .line 303
    const-wide/16 v32, 0x0

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mNextFrameTime:J

    goto/16 :goto_0
.end method


# virtual methods
.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 681
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 682
    .local v2, "res":Landroid/content/res/Resources;
    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 683
    .local v1, "percentDownloaded":I
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v4, 0x7f0f0024

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 686
    .local v0, "descriptionStringBuilder":Ljava/lang/StringBuilder;
    const/16 v3, 0x64

    if-ge v1, v3, :cond_0

    .line 687
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequested:Z

    if-eqz v3, :cond_1

    const v3, 0x7f0f0025

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 687
    :cond_1
    const v3, 0x7f0f0026

    goto :goto_0
.end method

.method public getDocumentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 22
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 484
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 485
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ser="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mSerial:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 489
    .local v18, "now":J
    const-wide/16 v16, 0x0

    .line 492
    .local v16, "nextInvalidateTime":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mBackgroundRect:Landroid/graphics/RectF;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getAnimationBackgroundPaint()Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 494
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 615
    const-string v2, "DSV"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 616
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): unexpected state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    :cond_1
    :goto_0
    return-void

    .line 496
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    if-eqz v2, :cond_5

    .line 497
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 498
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") starting PRESS animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 501
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundIsColorPress:Z

    .line 502
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 505
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionIntervalMs:J

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionDoneTime:J

    .line 507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->drawNextFrame(Landroid/graphics/drawable/AnimationDrawable;Landroid/graphics/Canvas;Z)J

    move-result-wide v16

    .line 532
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getArcBackgroundPaint()Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 535
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionDoneTime:J

    cmp-long v2, v18, v2

    if-gez v2, :cond_4

    .line 536
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundColorTransitionStepMs:J

    add-long v14, v18, v2

    .line 538
    .local v14, "nextBackgroundStepTime":J
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-nez v2, :cond_9

    .line 539
    move-wide/from16 v16, v14

    .line 620
    .end local v14    # "nextBackgroundStepTime":J
    :cond_4
    :goto_2
    const-wide/16 v2, 0x0

    cmp-long v2, v16, v2

    if-lez v2, :cond_1

    .line 621
    const-wide/16 v2, 0x1

    sub-long v4, v16, v18

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    .line 623
    .local v10, "delay":J
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->postInvalidateDelayed(J)V

    goto/16 :goto_0

    .line 508
    .end local v10    # "delay":J
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequested:Z

    if-nez v2, :cond_7

    .line 509
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 510
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") drawing PRESS frame 0 (!dlrq)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 513
    const-wide/16 v16, 0x0

    goto :goto_1

    .line 515
    :cond_7
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 516
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") drawing PRESS frame "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->drawNextFrame(Landroid/graphics/drawable/AnimationDrawable;Landroid/graphics/Canvas;Z)J

    move-result-wide v16

    .line 521
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v2

    add-int/lit8 v13, v2, -0x1

    .line 522
    .local v13, "lastPressFrameIndex":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    if-lt v2, v13, :cond_3

    .line 524
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 525
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 526
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/AnimationDrawable;->getDuration(I)I

    move-result v2

    int-to-long v2, v2

    add-long v16, v18, v2

    .line 527
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 528
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): start DOWNLOAD animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 541
    .end local v13    # "lastPressFrameIndex":I
    .restart local v14    # "nextBackgroundStepTime":J
    :cond_9
    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v16

    goto/16 :goto_2

    .line 548
    .end local v14    # "nextBackgroundStepTime":J
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableDownload:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v2

    add-int/lit8 v12, v2, -0x1

    .line 549
    .local v12, "initialDownloadFrame":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    if-eqz v2, :cond_a

    .line 550
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 551
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 552
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 553
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") starting DOWNLOAD animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mMinInProgressDisplayFraction:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v20

    .line 560
    .local v20, "targetDownloadFraction":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionIsIncreasing:Z

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mIsOnline:Z

    if-eqz v2, :cond_e

    const/4 v8, 0x1

    .line 561
    .local v8, "advanceFrame":Z
    :goto_3
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 562
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): download: frac inc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDisplayedDownloadFraction:F

    const v3, 0x3d4ccccd    # 0.05f

    add-float/2addr v2, v3

    move/from16 v0, v20

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDisplayedDownloadFraction:F

    .line 567
    if-eqz v8, :cond_f

    .line 568
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    float-to-double v2, v2

    const-wide v4, 0x3fefffffca501acbL    # 0.9999999

    cmpg-double v2, v2, v4

    if-gez v2, :cond_d

    .line 569
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionLastUpdatedTime:J

    sub-long v2, v18, v2

    long-to-float v0, v2

    move/from16 v21, v0

    .line 571
    .local v21, "timeSinceLatestDownloadFraction":F
    const v2, 0x469c4000    # 20000.0f

    cmpl-float v2, v21, v2

    if-lez v2, :cond_d

    .line 572
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 573
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") pausing animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    :cond_c
    const/4 v8, 0x0

    .line 584
    .end local v21    # "timeSinceLatestDownloadFraction":F
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableDownload:Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v8}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->drawNextFrame(Landroid/graphics/drawable/AnimationDrawable;Landroid/graphics/Canvas;Z)J

    move-result-wide v16

    .line 585
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDisplayedDownloadFraction:F

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    const v3, 0x3f7851ec    # 0.97f

    mul-float v9, v2, v3

    .line 588
    .local v9, "downloadAngle":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    add-float v4, v2, v9

    const/high16 v2, 0x43b40000    # 360.0f

    sub-float v5, v2, v9

    const/4 v6, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getArcBackgroundPaint()Landroid/graphics/Paint;

    move-result-object v7

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 590
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    const/high16 v4, 0x43870000    # 270.0f

    const/4 v6, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getArcProgressPaint()Landroid/graphics/Paint;

    move-result-object v7

    move-object/from16 v2, p1

    move v5, v9

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 592
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    float-to-double v2, v2

    const-wide v4, 0x3fefffffca501acbL    # 0.9999999

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_4

    .line 594
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 595
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 596
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 597
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): start FINISH animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 560
    .end local v8    # "advanceFrame":Z
    .end local v9    # "downloadAngle":F
    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 579
    .restart local v8    # "advanceFrame":Z
    :cond_f
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 580
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") freezing DOWNLOAD animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    :cond_10
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    goto/16 :goto_4

    .line 603
    .end local v8    # "advanceFrame":Z
    .end local v12    # "initialDownloadFrame":I
    .end local v20    # "targetDownloadFraction":F
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    if-eqz v2, :cond_12

    .line 604
    const-string v2, "DSV"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 605
    const-string v2, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onDraw("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): starting FINISH animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_11
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 608
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 610
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableFinish:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->drawNextFrame(Landroid/graphics/drawable/AnimationDrawable;Landroid/graphics/Canvas;Z)J

    move-result-wide v16

    .line 611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getArcProgressPaint()Landroid/graphics/Paint;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 494
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onMeasure(II)V
    .locals 9
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 661
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getPaddingLeft()I

    move-result v3

    .line 662
    .local v3, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getPaddingTop()I

    move-result v4

    .line 664
    .local v4, "paddingTop":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawablePress:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 665
    .local v2, "frame":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 666
    .local v1, "drawableWidth":I
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 668
    .local v0, "drawableHeight":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    float-to-int v7, v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getPaddingRight()I

    move-result v8

    add-int v6, v7, v8

    .line 670
    .local v6, "viewWidth":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mArcRect:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    float-to-int v7, v7

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->getPaddingBottom()I

    move-result v8

    add-int v5, v7, v8

    .line 673
    .local v5, "viewHeight":I
    invoke-virtual {p0, v6, v5}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setMeasuredDimension(II)V

    .line 674
    return-void
.end method

.method public setDocumentId(Ljava/lang/String;)V
    .locals 5
    .param p1, "documentId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 698
    const-string v0, "DSV"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    const-string v0, "DSV"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDocumentId("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ser="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mSerial:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 703
    const-string v0, "DSV"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 704
    const-string v0, "DSV"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setDocumentId("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    .line 707
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequestedSet:Z

    .line 708
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionSet:Z

    .line 710
    :cond_2
    return-void
.end method

.method public setDownloadFraction(F)V
    .locals 9
    .param p1, "downloadFraction"    # F

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 416
    const-string v0, "DSV"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDownloadFraction("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") df="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ser="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mSerial:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequestedSet:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    .line 422
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionIsIncreasing:Z

    .line 424
    :cond_1
    iput p1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    .line 426
    float-to-double v4, p1

    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    cmpg-double v0, v4, v6

    if-gtz v0, :cond_4

    .line 427
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionSet:Z

    if-nez v0, :cond_2

    .line 429
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 430
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 431
    const-string v0, "DSV"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 432
    const-string v0, "DSV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " setDownloadFraction("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") init state to press"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :cond_2
    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionSet:Z

    .line 471
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->invalidate()V

    .line 472
    return-void

    :cond_3
    move v0, v2

    .line 422
    goto :goto_0

    .line 435
    :cond_4
    float-to-double v4, p1

    const-wide v6, 0x3fefffffca501acbL    # 0.9999999

    cmpg-double v0, v4, v6

    if-gez v0, :cond_8

    .line 437
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    if-nez v0, :cond_5

    .line 438
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 439
    const-string v0, "DSV"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 440
    const-string v0, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setDownloadFraction("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") start dl animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_5
    iput v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 444
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundIsColorPress:Z

    .line 445
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableDownload:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v3

    if-lt v0, v3, :cond_6

    .line 446
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 448
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionSet:Z

    if-nez v0, :cond_7

    .line 451
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionLastUpdatedTime:J

    goto :goto_1

    .line 453
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFractionLastUpdatedTime:J

    goto :goto_1

    .line 457
    :cond_8
    iput p1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDisplayedDownloadFraction:F

    .line 459
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    if-ne v0, v1, :cond_a

    .line 460
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 461
    const-string v0, "DSV"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 462
    const-string v0, "DSV"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setDownloadFraction("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): start FINISH animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    :cond_9
    :goto_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 468
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mProgressArcBackgroundIsColorPress:Z

    goto/16 :goto_1

    .line 465
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDrawableFinish:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    goto :goto_2
.end method

.method public setDownloadRequested(Z)V
    .locals 7
    .param p1, "downloadRequested"    # Z

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372
    const-string v3, "DSV"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 373
    const-string v3, "DSV"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDownloadRequested("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ser="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mSerial:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    if-eqz p1, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequested:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequestedSet:Z

    if-eqz v3, :cond_2

    move v0, v1

    .line 380
    .local v0, "downloadRequestedTransitionedToTrue":Z
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequested:Z

    .line 381
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadRequestedSet:Z

    .line 383
    if-eqz v0, :cond_3

    .line 384
    const-string v3, "DSV"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 385
    const-string v3, "DSV"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " setDownloadRequested("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): start press animation"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_1
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    .line 388
    iput v2, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mCurrentFrameIndex:I

    .line 389
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mStartAnimation:Z

    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->invalidate()V

    .line 397
    :goto_1
    return-void

    .end local v0    # "downloadRequestedTransitionedToTrue":Z
    :cond_2
    move v0, v2

    .line 378
    goto :goto_0

    .line 391
    .restart local v0    # "downloadRequestedTransitionedToTrue":Z
    :cond_3
    if-eqz p1, :cond_4

    .line 393
    iget v1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->setDownloadFraction(F)V

    goto :goto_1

    .line 395
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->invalidate()V

    goto :goto_1
.end method

.method public setOnline(Z)V
    .locals 0
    .param p1, "isOnline"    # Z

    .prologue
    .line 478
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mIsOnline:Z

    .line 479
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->invalidate()V

    .line 480
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 720
    const-string v0, "DSV(%s): state=%d, frac=%4.2f"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDocumentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mState:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/apps/books/widget/BooksDownloadStatusView;->mDownloadFraction:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
