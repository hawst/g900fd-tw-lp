.class public interface abstract Lcom/google/android/apps/books/sync/SyncAccountsState;
.super Ljava/lang/Object;
.source "SyncAccountsState.java"


# virtual methods
.method public abstract clear()V
.end method

.method public abstract getLastMyEbooksFetchTime(Ljava/lang/String;)J
.end method

.method public abstract getLastSyncTime(Ljava/lang/String;J)J
.end method

.method public abstract haveNagged(Ljava/lang/String;)Z
.end method

.method public abstract setHaveNagged(Ljava/lang/String;Z)V
.end method

.method public abstract setLastMyEbooksFetchTime(Ljava/lang/String;J)V
.end method

.method public abstract setLastSyncTime(Ljava/lang/String;J)V
.end method
