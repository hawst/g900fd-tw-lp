.class public Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "BooksAppWidgetProvider.java"


# static fields
.field private static sWidgetState:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static updateWidgets([ILandroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 14
    .param p0, "appWidgetIds"    # [I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetState"    # Landroid/widget/RemoteViews;

    .prologue
    .line 31
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 38
    .local v2, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    if-eqz p2, :cond_0

    .line 39
    sput-object p2, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;->sWidgetState:Landroid/widget/RemoteViews;

    .line 42
    :cond_0
    move-object v3, p0

    .local v3, "arr$":[I
    array-length v6, v3

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_3

    aget v1, v3, v4

    .line 44
    .local v1, "appWidgetId":I
    sget-object v11, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;->sWidgetState:Landroid/widget/RemoteViews;

    if-eqz v11, :cond_2

    .line 45
    sget-object v11, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;->sWidgetState:Landroid/widget/RemoteViews;

    invoke-virtual {v11}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    move-result-object v9

    .line 54
    .local v9, "rv":Landroid/widget/RemoteViews;
    :goto_1
    const-string v11, "BooksAppWidget"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 55
    const-string v11, "BooksAppWidget"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "update widget with appWidgetId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_1
    new-instance v10, Landroid/content/Intent;

    const-class v11, Lcom/google/android/apps/books/appwidget/BooksAppWidgetService;

    invoke-direct {v10, p1, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v10, "serviceIntent":Landroid/content/Intent;
    const-string v11, "appWidgetId"

    invoke-virtual {v10, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    const v11, 0x7f0e00b7

    invoke-virtual {v9, v11, v10}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 64
    new-instance v5, Landroid/content/Intent;

    const-string v11, "android.intent.action.MAIN"

    invoke-direct {v5, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 65
    .local v5, "launchIntent":Landroid/content/Intent;
    new-instance v11, Landroid/content/ComponentName;

    const-class v12, Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {v11, p1, v12}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 66
    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p1, v11, v5, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 68
    .local v8, "pendingLaunchIntent":Landroid/app/PendingIntent;
    const v11, 0x7f0e00b5

    invoke-virtual {v9, v11, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 69
    const v11, 0x7f0e00b6

    invoke-virtual {v9, v11, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    invoke-direct {v0, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "activityIntent":Landroid/content/Intent;
    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p1, v11, v0, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 75
    .local v7, "pendingIntent":Landroid/app/PendingIntent;
    const v11, 0x7f0e00b7

    invoke-virtual {v9, v11, v7}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 77
    invoke-virtual {v2, v1, v9}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 42
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 47
    .end local v0    # "activityIntent":Landroid/content/Intent;
    .end local v5    # "launchIntent":Landroid/content/Intent;
    .end local v7    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v8    # "pendingLaunchIntent":Landroid/app/PendingIntent;
    .end local v9    # "rv":Landroid/widget/RemoteViews;
    .end local v10    # "serviceIntent":Landroid/content/Intent;
    :cond_2
    new-instance v9, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f040029

    invoke-direct {v9, v11, v12}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 51
    .restart local v9    # "rv":Landroid/widget/RemoteViews;
    const v11, 0x7f0e00b7

    const v12, 0x7f0e00b5

    invoke-virtual {v9, v11, v12}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    goto/16 :goto_1

    .line 79
    .end local v1    # "appWidgetId":I
    .end local v9    # "rv":Landroid/widget/RemoteViews;
    :cond_3
    return-void
.end method


# virtual methods
.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 27
    const/4 v0, 0x0

    invoke-static {p3, p1, v0}, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;->updateWidgets([ILandroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 28
    return-void
.end method
