.class Lcom/google/android/apps/books/app/HomeFragment$14;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->setupMyLibraryHeader()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 1005
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 1008
    const v0, 0x7f040068

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1009
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 1038
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2400(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1039
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getMyLibraryFilterSource()Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->getPossibleFilters()Ljava/util/List;

    move-result-object v0

    .line 1040
    .local v0, "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$14$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/books/app/HomeFragment$14$1;-><init>(Lcom/google/android/apps/books/app/HomeFragment$14;Ljava/util/List;)V

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mPagerAdapter:Landroid/support/v4/view/PagerAdapter;
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/HomeFragment;->access$2502(Lcom/google/android/apps/books/app/HomeFragment;Landroid/support/v4/view/PagerAdapter;)Landroid/support/v4/view/PagerAdapter;

    .line 1114
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getMyLibraryFilterSource()Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$14$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/HomeFragment$14$2;-><init>(Lcom/google/android/apps/books/app/HomeFragment$14;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->setListener(Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource$Listener;)V

    .line 1121
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2400(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mPagerAdapter:Landroid/support/v4/view/PagerAdapter;
    invoke-static {v3}, Lcom/google/android/apps/books/app/HomeFragment;->access$2500(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1123
    const v2, 0x7f040058

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1124
    .local v1, "homeProgress":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$14;->getHeaderHeight()I

    move-result v2

    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1125
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1126
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 1131
    return-void
.end method

.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getHeaderBottomMargin(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$14;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getMinimumHeaderHeight(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getHeaderSpacerId()I
    .locals 1

    .prologue
    .line 1028
    const v0, 0x7f0e0139

    return v0
.end method

.method protected getListViewId()I
    .locals 1

    .prologue
    .line 1023
    const v0, 0x7f0e013a

    return v0
.end method

.method protected getTabAccessibilityMode()I
    .locals 1

    .prologue
    .line 1159
    const/4 v0, 0x3

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 1145
    const/4 v0, 0x0

    return v0
.end method

.method protected getTabPaddingMode()I
    .locals 1

    .prologue
    .line 1150
    const/4 v0, 0x1

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 1154
    const/4 v0, 0x1

    return v0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 1013
    const v0, 0x7f0e00f5

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 1140
    const/4 v0, 0x1

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 1018
    const/4 v0, 0x1

    return v0
.end method
