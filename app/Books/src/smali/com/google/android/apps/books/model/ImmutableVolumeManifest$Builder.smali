.class public Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
.super Ljava/lang/Object;
.source "ImmutableVolumeManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutableVolumeManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mChapters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;"
        }
    .end annotation
.end field

.field private mContentVersion:Ljava/lang/String;

.field private mFirstChapterStartSegmentIndex:I

.field private mHasImageMode:Z

.field private mHasMediaOverlays:Z

.field private mHasTextMode:Z

.field private mImageModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

.field private mIsRightToLeft:Z

.field private mLanguage:Ljava/lang/String;

.field private mMediaOverlayActiveClass:Ljava/lang/String;

.field private mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferredMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field private mRenditionOrientation:Ljava/lang/String;

.field private mRenditionSpread:Ljava/lang/String;

.field private mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/IdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private mSegmentResources:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;"
        }
    .end annotation
.end field

.field private mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end field

.field private mTextModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mChapters:Ljava/util/List;

    .line 174
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->withLazyMap(Ljava/util/List;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 176
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->withLazyMap(Ljava/util/List;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 178
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/IdentifiableCollection;->withLazyMap(Ljava/util/Collection;)Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;

    .line 180
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegmentResources:Ljava/util/Collection;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 20

    .prologue
    .line 288
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mContentVersion:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mHasTextMode:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mHasImageMode:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mPreferredMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mFirstChapterStartSegmentIndex:I

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mIsRightToLeft:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mHasMediaOverlays:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mMediaOverlayActiveClass:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mLanguage:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mRenditionOrientation:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mRenditionSpread:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mChapters:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mImageModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mTextModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegmentResources:Ljava/util/Collection;

    move-object/from16 v19, v0

    invoke-direct/range {v1 .. v19}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;-><init>(Ljava/lang/String;ZZLcom/google/android/apps/books/model/VolumeManifest$Mode;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;Lcom/google/android/apps/books/util/IdentifiableCollection;Ljava/util/Collection;)V

    return-object v1
.end method

.method public setChapters(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mChapters:Ljava/util/List;

    .line 249
    return-object p0
.end method

.method public setContentVersion(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "contentVersion"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mContentVersion:Ljava/lang/String;

    .line 184
    return-object p0
.end method

.method public setFirstChapterStartSegmentIndex(I)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "firstChapterStartSegmentIndex"    # I

    .prologue
    .line 203
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mFirstChapterStartSegmentIndex:I

    .line 204
    return-object p0
.end method

.method public setHasImageMode(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "hasImageMode"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mHasImageMode:Z

    .line 194
    return-object p0
.end method

.method public setHasMediaOverlays(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "hasMediaOverlays"    # Z

    .prologue
    .line 213
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mHasMediaOverlays:Z

    .line 214
    return-object p0
.end method

.method public setHasTextMode(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "hasTextMode"    # Z

    .prologue
    .line 188
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mHasTextMode:Z

    .line 189
    return-object p0
.end method

.method public setImageModePositions(Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "imageModePositions"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mImageModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    .line 239
    return-object p0
.end method

.method public setIsRightToLeft(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "isRightToLeft"    # Z

    .prologue
    .line 208
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mIsRightToLeft:Z

    .line 209
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mLanguage:Ljava/lang/String;

    .line 224
    return-object p0
.end method

.method public setMediaOverlayActiveClass(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "mediaOverlayActiveClass"    # Ljava/lang/String;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mMediaOverlayActiveClass:Ljava/lang/String;

    .line 219
    return-object p0
.end method

.method public setPages(Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "pages":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<Lcom/google/android/apps/books/model/Page;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 264
    return-object p0
.end method

.method public setPages(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    invoke-static {p1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->withLazyMap(Ljava/util/List;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mPages:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 269
    return-object p0
.end method

.method public setPreferredMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "preferredMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mPreferredMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 199
    return-object p0
.end method

.method public setRenditionOrientation(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "orientation"    # Ljava/lang/String;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mRenditionOrientation:Ljava/lang/String;

    .line 229
    return-object p0
.end method

.method public setRenditionSpread(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "spread"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mRenditionSpread:Ljava/lang/String;

    .line 234
    return-object p0
.end method

.method public setResources(Ljava/util/Collection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 273
    .local p1, "resources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static {p1}, Lcom/google/android/apps/books/util/IdentifiableCollection;->withLazyMap(Ljava/util/Collection;)Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mResources:Lcom/google/android/apps/books/util/IdentifiableCollection;

    .line 274
    return-object p0
.end method

.method public setSegmentResources(Ljava/util/Collection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "segmentResources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SegmentResource;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegmentResources:Ljava/util/Collection;

    .line 284
    return-object p0
.end method

.method public setSegments(Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "segments":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<Lcom/google/android/apps/books/model/Segment;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 254
    return-object p0
.end method

.method public setSegments(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;)",
            "Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-static {p1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->withLazyMap(Ljava/util/List;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mSegments:Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    .line 259
    return-object p0
.end method

.method public setTextModePositions(Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .locals 0
    .param p1, "textModePositions"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->mTextModePositions:Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    .line 244
    return-object p0
.end method
