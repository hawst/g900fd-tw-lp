.class Lcom/google/android/apps/books/provider/database/ChaptersTable;
.super Ljava/lang/Object;
.source "ChaptersTable.java"


# direct methods
.method public static getCreationSql()Ljava/lang/String;
    .locals 3

    .prologue
    .line 22
    const-string v1, "REFERENCES segments(segment_id)"

    .line 24
    .local v1, "refSectionId":Ljava/lang/String;
    const-string v0, "REFERENCES pages(page_id)"

    .line 27
    .local v0, "refPageId":Ljava/lang/String;
    const-string v2, "CREATE TABLE chapters (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL, chapter_id TEXT NOT NULL, chapter_order INTEGER NOT NULL, title TEXT NOT NULL, start_section_id TEXT NOT NULL REFERENCES segments(segment_id), start_page_id TEXT NOT NULL REFERENCES pages(page_id), depth INTEGER NOT NULL DEFAULT 0, reading_position TEXT, FOREIGN KEY(account_name, volume_id) REFERENCES volumes(account_name, volume_id), UNIQUE (account_name, volume_id, chapter_id) ON CONFLICT REPLACE)"

    return-object v2
.end method
