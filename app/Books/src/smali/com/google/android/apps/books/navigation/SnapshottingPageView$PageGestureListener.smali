.class Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SnapshottingPageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/SnapshottingPageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageGestureListener"
.end annotation


# instance fields
.field private final mTempPoint:Landroid/graphics/Point;

.field private final mTempPointF:Landroid/graphics/PointF;

.field final synthetic this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)V
    .locals 1

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 248
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    .line 249
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPointF:Landroid/graphics/PointF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    .param p2, "x1"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView$1;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)V

    return-void
.end method

.method private onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Z)Z
    .locals 12
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "pagePositionOnScreen"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "confirmed"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 253
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v3

    if-nez v3, :cond_1

    .line 298
    :cond_0
    :goto_0
    return v0

    .line 256
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageRendering()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v1

    .line 257
    .local v1, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v1, :cond_0

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v7, v3

    .line 262
    .local v7, "intX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v8, v3

    .line 265
    .local v8, "intY":I
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {v3, v0, v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getBookmarkMargins(ILandroid/graphics/Point;)V

    .line 266
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # invokes: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getBookmarkMeasurements()Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$500(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    iget-object v10, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {v3, v5, v10}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getTapTargetSize(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 268
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$400(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne v3, v5, :cond_3

    .line 269
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v10, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v0, v0, v5, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 273
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 274
    if-eqz p3, :cond_2

    .line 275
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$700(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/app/BookmarkController;

    move-result-object v3

    new-array v5, v2, [Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v10, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v10}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageRendering()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v10

    aput-object v10, v5, v0

    invoke-static {v5}, Lcom/google/android/play/utils/collections/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v3, v5, v0}, Lcom/google/android/apps/books/app/BookmarkController;->toggleBookmarks(Ljava/util/List;Z)V

    :cond_2
    move v0, v2

    .line 278
    goto :goto_0

    .line 271
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getWidth()I

    move-result v5

    iget-object v10, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    sub-int/2addr v5, v10

    iget-object v10, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v10}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getWidth()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v5, v0, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    .line 281
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/ViewUtils;->getBounds(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v6

    .line 282
    .local v6, "bounds":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Point;->set(II)V

    .line 283
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPoint:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$800(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Point;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v5

    const/4 v10, 0x0

    invoke-static {p2, v2, v3, v5, v10}, Lcom/google/android/apps/books/util/PagesViewUtils;->getPageBounds(Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Rect;Lcom/google/android/apps/books/util/Holder;)Landroid/graphics/Rect;

    .line 286
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 288
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->linkTapThresholdInPixels()I

    move-result v0

    int-to-float v9, v0

    .line 289
    .local v9, "screenThreshold":F
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float v4, v9, v0

    .line 292
    .local v4, "threshold":F
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPointF:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$800(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$600(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    sub-float/2addr v3, v5

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mContentSize:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$800(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    div-float/2addr v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mOnPageTouchListener:Lcom/google/android/apps/books/widget/OnPageTouchListener;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$900(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/widget/OnPageTouchListener;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->mTempPointF:Landroid/graphics/PointF;

    move-object v2, p2

    move v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/widget/OnPageTouchListener;->onPageTouch(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/PointF;FZ)Z

    move-result v0

    goto/16 :goto_0
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 235
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPage:Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$300(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 236
    const/4 v0, 0x0

    .line 237
    .local v0, "confirmed":Z
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$400(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    move-result-object v2

    invoke-direct {p0, p1, v2, v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Z)Z

    move-result v1

    .line 239
    .end local v0    # "confirmed":Z
    :cond_0
    return v1
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 244
    const/4 v0, 0x1

    .line 245
    .local v0, "confirmed":Z
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingPageView;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->access$400(Lcom/google/android/apps/books/navigation/SnapshottingPageView;)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView$PageGestureListener;->onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Z)Z

    move-result v1

    return v1
.end method
