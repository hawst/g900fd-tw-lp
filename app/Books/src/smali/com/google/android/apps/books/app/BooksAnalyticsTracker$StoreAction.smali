.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StoreAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum END_OF_BOOK_BODY_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum END_OF_BOOK_PAGE_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum EOB_BUY_AFTER_SAMPLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum EOB_COVER_PRESSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum EOB_RATE_THE_BOOK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum EOB_RATE_THE_BOOK_IN_APP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum HOME_SIDE_DRAWER_SHOP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum HOME_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_ACCEPTANCE_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_ACCEPTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_DISMISSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_SAW_READ_NOW_CARD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_SHOW_REDEEM_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_VIEWED_FROM_HOME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum OFFERS_VIEWED_FROM_WIDGET:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum READER_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum READER_ACTION_BUY_ICON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum READER_ACTION_SHARE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

.field public static final enum READER_SKIM_BUY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 271
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "EOB_BUY_AFTER_SAMPLE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_BUY_AFTER_SAMPLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 275
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "END_OF_BOOK_PAGE_VIEW_RECOMMENDATION"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->END_OF_BOOK_PAGE_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 279
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "END_OF_BOOK_BODY_VIEW_RECOMMENDATION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->END_OF_BOOK_BODY_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 283
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "EOB_RATE_THE_BOOK"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_RATE_THE_BOOK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 287
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "EOB_RATE_THE_BOOK_IN_APP"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_RATE_THE_BOOK_IN_APP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 291
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "EOB_COVER_PRESSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_COVER_PRESSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 295
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "HOME_VIEW_RECOMMENDATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->HOME_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 299
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "READER_ACTION_ABOUT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 303
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "READER_ACTION_BUY_ICON"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_BUY_ICON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 307
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "READER_ACTION_SHARE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_SHARE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 311
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "READER_SKIM_BUY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_SKIM_BUY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 315
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "HOME_SIDE_DRAWER_SHOP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->HOME_SIDE_DRAWER_SHOP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 319
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_SAW_READ_NOW_CARD"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_SAW_READ_NOW_CARD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 323
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_VIEWED_FROM_HOME"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_VIEWED_FROM_HOME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 327
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_VIEWED_FROM_WIDGET"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_VIEWED_FROM_WIDGET:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 331
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_SHOW_REDEEM_DIALOG"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_SHOW_REDEEM_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 335
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_ACCEPTED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACCEPTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 339
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_DISMISSED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_DISMISSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 343
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_ACTION_ABOUT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 347
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    const-string v1, "OFFERS_ACCEPTANCE_FAILED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACCEPTANCE_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 267
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_BUY_AFTER_SAMPLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->END_OF_BOOK_PAGE_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->END_OF_BOOK_BODY_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_RATE_THE_BOOK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_RATE_THE_BOOK_IN_APP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_COVER_PRESSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->HOME_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_BUY_ICON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_SHARE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_SKIM_BUY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->HOME_SIDE_DRAWER_SHOP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_SAW_READ_NOW_CARD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_VIEWED_FROM_HOME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_VIEWED_FROM_WIDGET:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_SHOW_REDEEM_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACCEPTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_DISMISSED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACCEPTANCE_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 267
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    return-object v0
.end method
