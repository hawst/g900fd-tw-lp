.class Lcom/google/android/apps/books/appwidget/BooksRVFactory$1;
.super Landroid/content/BroadcastReceiver;
.source "BooksAppWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/appwidget/BooksRVFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$1;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 77
    const-string v0, "BooksAppWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "BooksAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received broadcast intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$1;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    # invokes: Lcom/google/android/apps/books/appwidget/BooksRVFactory;->update()V
    invoke-static {v0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->access$000(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V

    .line 83
    :cond_1
    return-void
.end method
