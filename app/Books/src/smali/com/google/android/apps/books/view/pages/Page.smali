.class Lcom/google/android/apps/books/view/pages/Page;
.super Ljava/lang/Object;
.source "Page.java"


# static fields
.field private static final sBackPageCoord:[F

.field private static final sFrontPageCoord:[F

.field private static final tmpStretchMatrix:[F

.field private static final tmpTargetSize:Landroid/graphics/Point;


# instance fields
.field backFacing:Z

.field final mBookmarkAnimator:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

.field private final mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

.field private mBookmarkTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

.field private mCenterBitmapOnPage:Z

.field private mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

.field private final mDebugLog:Z

.field private mDisplaySize:Landroid/graphics/Point;

.field private mIsLoading:Z

.field private mPreviousBookmarkPercent:F

.field private mPreviousTextureSize:Landroid/graphics/Point;

.field private final mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

.field private mShowSpinnerWhenLoading:Z

.field final mTexture:Lcom/google/android/apps/books/view/pages/Texture;

.field timestampData:J

.field timestampDisplaying:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 92
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/view/pages/Page;->sFrontPageCoord:[F

    .line 99
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/books/view/pages/Page;->sBackPageCoord:[F

    .line 312
    const/16 v0, 0x10

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/apps/books/view/pages/Page;->tmpStretchMatrix:[F

    .line 313
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/Page;->tmpTargetSize:Landroid/graphics/Point;

    return-void

    .line 92
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 99
    :array_1
    .array-data 4
        -0x3fc00000    # -3.0f
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        -0x3fc00000    # -3.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 3
    .param p1, "scene"    # Lcom/google/android/apps/books/view/pages/PageTurnScene;
    .param p2, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    .line 30
    new-instance v0, Lcom/google/android/apps/books/view/pages/Texture;

    invoke-direct {v0}, Lcom/google/android/apps/books/view/pages/Texture;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    .line 59
    new-instance v0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkAnimator:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 62
    iput-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 65
    iput-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 68
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousBookmarkPercent:F

    .line 69
    iput-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousTextureSize:Landroid/graphics/Point;

    .line 72
    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mShowSpinnerWhenLoading:Z

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .line 85
    const-string v0, "books.view.Page"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mDebugLog:Z

    .line 86
    return-void
.end method

.method private bookmarkTexturePlacement(FLandroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .locals 3
    .param p1, "bookmarkPercent"    # F
    .param p2, "textureSize"    # Landroid/graphics/Point;

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Lcom/google/android/apps/books/view/pages/TexturePlacement;

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mBookmark:I

    const-string v2, "Bookmark"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 407
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousTextureSize:Landroid/graphics/Point;

    .line 411
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousBookmarkPercent:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousTextureSize:Landroid/graphics/Point;

    invoke-virtual {v0, p2}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mutableTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->setBookmarkTransformationMatrix(FZLandroid/graphics/Matrix;Landroid/graphics/Point;)V

    .line 415
    iput p1, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousBookmarkPercent:F

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousTextureSize:Landroid/graphics/Point;

    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    return-object v0
.end method

.method private calculateBookmarkFadeInAlpha(J)F
    .locals 7
    .param p1, "now"    # J

    .prologue
    .line 428
    iget-wide v2, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    const-wide/16 v4, 0x12c

    add-long/2addr v2, v4

    sub-long v0, p1, v2

    .line 429
    .local v0, "sinceFadeInCompleted":J
    const/high16 v2, 0x3f800000    # 1.0f

    long-to-float v3, v0

    const/high16 v4, 0x43480000    # 200.0f

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    return v2
.end method

.method private chooseProgram(Landroid/graphics/Point;FFFFZ)V
    .locals 16
    .param p1, "textureSize"    # Landroid/graphics/Point;
    .param p2, "fadeinOpacity"    # F
    .param p3, "pageOpacity"    # F
    .param p4, "bookmarkPercent"    # F
    .param p5, "bookmarkFadeInAlpha"    # F
    .param p6, "curved"    # Z

    .prologue
    .line 330
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v2, v5, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    .line 331
    .local v2, "renderer":Lcom/google/android/apps/books/view/pages/PageTurnRenderer;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/Page;->getContentTexturePlacement()Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v4

    .line 332
    .local v4, "contentTexturePlacement":Lcom/google/android/apps/books/view/pages/TexturePlacement;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v5}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getBackgroundColorFloats()[F

    move-result-object v8

    .line 340
    .local v8, "rgba":[F
    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/view/pages/Page;->tmpStretchMatrix:[F

    sget-object v7, Lcom/google/android/apps/books/view/pages/Page;->tmpTargetSize:Landroid/graphics/Point;

    move-object/from16 v0, p1

    invoke-virtual {v5, v6, v0, v7}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->constructPageStretchContentTransformMatrix([FLandroid/graphics/Point;Landroid/graphics/Point;)Z

    .line 342
    sget-object v5, Lcom/google/android/apps/books/view/pages/Page;->tmpStretchMatrix:[F

    invoke-virtual {v2, v5}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setPageStretchMatrix([F)V

    .line 344
    if-nez v4, :cond_0

    .line 346
    sget-object v5, Lcom/google/android/apps/books/view/pages/Page;->tmpTargetSize:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/books/view/pages/Page;->getEmptyTexturePlacement(Landroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    move/from16 v0, p6

    invoke-virtual {v2, v5, v6, v0, v8}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpOneTexture(Lcom/google/android/apps/books/view/pages/TexturePlacement;FZ[F)V

    .line 367
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setMVPUniform()V

    .line 368
    return-void

    .line 347
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/Page;->getCenterBitmapOnPage()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 348
    const/high16 v5, 0x3f800000    # 1.0f

    move/from16 v0, p6

    invoke-virtual {v2, v4, v5, v0, v8}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpOneTexture(Lcom/google/android/apps/books/view/pages/TexturePlacement;FZ[F)V

    goto :goto_0

    .line 350
    :cond_1
    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v5, p2, v5

    if-gez v5, :cond_2

    .line 352
    sget-object v5, Lcom/google/android/apps/books/view/pages/Page;->tmpTargetSize:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/books/view/pages/Page;->getEmptyTexturePlacement(Landroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v3

    .local v3, "emptyTexturePlacement":Lcom/google/android/apps/books/view/pages/TexturePlacement;
    move/from16 v5, p2

    move/from16 v6, p6

    move/from16 v7, p3

    .line 354
    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpTwoTextures(Lcom/google/android/apps/books/view/pages/TexturePlacement;Lcom/google/android/apps/books/view/pages/TexturePlacement;FZF[F)V

    goto :goto_0

    .line 356
    .end local v3    # "emptyTexturePlacement":Lcom/google/android/apps/books/view/pages/TexturePlacement;
    :cond_2
    move/from16 v0, p4

    float-to-double v6, v0

    const-wide/16 v10, 0x0

    cmpl-double v5, v6, v10

    if-lez v5, :cond_3

    .line 358
    sget-object v5, Lcom/google/android/apps/books/view/pages/Page;->tmpTargetSize:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/view/pages/Page;->bookmarkTexturePlacement(FLandroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v11

    move-object v9, v2

    move-object v10, v4

    move/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p3

    move-object v15, v8

    invoke-virtual/range {v9 .. v15}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpTwoTextures(Lcom/google/android/apps/books/view/pages/TexturePlacement;Lcom/google/android/apps/books/view/pages/TexturePlacement;FZF[F)V

    goto :goto_0

    .line 363
    :cond_3
    move/from16 v0, p3

    move/from16 v1, p6

    invoke-virtual {v2, v4, v0, v1, v8}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setUpOneTexture(Lcom/google/android/apps/books/view/pages/TexturePlacement;FZ[F)V

    goto :goto_0
.end method

.method private getCenterBitmapOnPage()Z
    .locals 1

    .prologue
    .line 471
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mCenterBitmapOnPage:Z

    return v0
.end method

.method private getContentTexturePlacement()Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/Texture;->isTextureIdAllocated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-object v2

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    if-nez v0, :cond_1

    .line 375
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/Page;->getCenterBitmapOnPage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 376
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 377
    .local v5, "matrix":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 378
    .local v1, "gapSize":Landroid/graphics/Point;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/view/pages/Page;->getTextureDimensions(Landroid/graphics/Point;)Z

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;

    move-result-object v0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->calculateCenteredTextureTransformationMatrix(Landroid/graphics/Point;Landroid/graphics/Point;ZILandroid/graphics/Matrix;)V

    .line 381
    new-instance v0, Lcom/google/android/apps/books/view/pages/TexturePlacement;

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/Texture;->getTextureId()I

    move-result v2

    const-string v3, "Gap"

    invoke-direct {v0, v2, v5, v3}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILandroid/graphics/Matrix;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 388
    .end local v1    # "gapSize":Landroid/graphics/Point;
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    goto :goto_0

    .line 384
    :cond_2
    new-instance v0, Lcom/google/android/apps/books/view/pages/TexturePlacement;

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/Texture;->getTextureId()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/Page;->pageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    const-string v4, "Content"

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILandroid/graphics/Matrix;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    goto :goto_1
.end method

.method private getEmptyTexturePlacement(Landroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .locals 1
    .param p1, "targetSize"    # Landroid/graphics/Point;

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/Page;->shouldShowLoadingSpinner()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getLoadingTexturePlacement(Landroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getEmptyTexturePlacement()Lcom/google/android/apps/books/view/pages/TexturePlacement;

    move-result-object v0

    goto :goto_0
.end method

.method private getFadeInOpacity(J)F
    .locals 7
    .param p1, "now"    # J

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 260
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isExecutingInitialLoadTransition()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    :goto_0
    return v1

    .line 266
    :cond_0
    const/4 v0, 0x0

    .line 268
    .local v0, "dataFading":F
    iget-wide v2, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 269
    iget-wide v2, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    sub-long v2, p1, v2

    long-to-float v2, v2

    const/high16 v3, 0x43960000    # 300.0f

    div-float v0, v2, v3

    .line 272
    :cond_1
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v1

    goto :goto_0
.end method

.method private getLoadingPresence(J)F
    .locals 9
    .param p1, "now"    # J

    .prologue
    const/4 v2, 0x0

    .line 250
    iget-wide v4, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 251
    iget-wide v4, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    sub-long v0, p1, v4

    .line 252
    .local v0, "timeDisplayedMs":J
    const-wide/16 v4, 0x1f4

    sub-long v4, v0, v4

    long-to-float v3, v4

    const/high16 v4, 0x43fa0000    # 500.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v2, v4}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v2

    .line 256
    .end local v0    # "timeDisplayedMs":J
    :cond_0
    return v2
.end method

.method private isLoadingSpinnerSpinning(J)Z
    .locals 5
    .param p1, "now"    # J

    .prologue
    .line 289
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/Page;->getFadeInOpacity(J)F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/Page;->shouldShowLoadingSpinner()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logError(Ljava/lang/String;)V
    .locals 4
    .param p1, "op"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 200
    .local v0, "error":I
    if-eqz v0, :cond_0

    .line 201
    const-string v1, "books.view.Page"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const-string v1, "books.view.Page"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :cond_0
    return-void
.end method

.method private pageMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 397
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    return-object v0
.end method

.method private setBookmarkPresent(Z)V
    .locals 1
    .param p1, "hasBookmark"    # Z

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkAnimator:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->setBookmarkInstantlyPresent(Z)V

    .line 460
    return-void
.end method


# virtual methods
.method draw(ZLandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;FJ)Z
    .locals 15
    .param p1, "isRtlOneUp"    # Z
    .param p2, "size"    # Landroid/graphics/Point;
    .param p3, "mesh"    # Lcom/google/android/apps/books/view/pages/PageMesh;
    .param p4, "pageOpacity"    # F
    .param p5, "now"    # J

    .prologue
    .line 227
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkAnimator:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->calculateBookmarkRatioOnPage(J)F

    move-result v6

    .line 228
    .local v6, "bookmarkPercentOnPage":F
    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/view/pages/Page;->calculateBookmarkFadeInAlpha(J)F

    move-result v7

    .line 230
    .local v7, "bookmarkFadeInAlpha":F
    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/view/pages/Page;->getFadeInOpacity(J)F

    move-result v4

    .line 232
    .local v4, "fadeinOpacity":F
    iget-boolean v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mDebugLog:Z

    if-eqz v2, :cond_0

    .line 233
    const-string v2, "books.view.Page"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "transition elapsed="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v12, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    sub-long v12, p5, v12

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " opacity="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_0
    if-nez p3, :cond_1

    const/4 v9, 0x1

    .line 237
    .local v9, "isFlat":Z
    :goto_0
    if-nez v9, :cond_2

    const/4 v8, 0x1

    :goto_1
    move-object v2, p0

    move-object/from16 v3, p2

    move/from16 v5, p4

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/view/pages/Page;->chooseProgram(Landroid/graphics/Point;FFFFZ)V

    .line 239
    if-eqz v9, :cond_4

    .line 240
    iget-boolean v2, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/books/view/pages/Page;->sBackPageCoord:[F

    :goto_2
    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->drawQuadTexCoords([F)Z

    .line 246
    :goto_3
    move-wide/from16 v0, p5

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/view/pages/Page;->isLoadingSpinnerSpinning(J)Z

    move-result v2

    return v2

    .line 236
    .end local v9    # "isFlat":Z
    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    .line 237
    .restart local v9    # "isFlat":Z
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .line 240
    :cond_3
    sget-object v2, Lcom/google/android/apps/books/view/pages/Page;->sFrontPageCoord:[F

    goto :goto_2

    .line 242
    :cond_4
    if-nez p1, :cond_5

    iget-boolean v2, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    if-eqz v2, :cond_6

    :cond_5
    const/4 v10, 0x1

    .line 243
    .local v10, "spineIsToRightOfPage":Z
    :goto_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/google/android/apps/books/view/pages/PageMesh;->draw(Z)V

    goto :goto_3

    .line 242
    .end local v10    # "spineIsToRightOfPage":Z
    :cond_6
    const/4 v10, 0x0

    goto :goto_4
.end method

.method public getSize(Landroid/graphics/Point;)Z
    .locals 3
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    const/4 v0, 0x1

    .line 478
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mDisplaySize:Landroid/graphics/Point;

    if-eqz v1, :cond_0

    .line 479
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mDisplaySize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 489
    :goto_0
    return v0

    .line 482
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/Page;->getCenterBitmapOnPage()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 485
    const/4 v0, 0x0

    goto :goto_0

    .line 488
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/Page;->getTextureDimensions(Landroid/graphics/Point;)Z

    goto :goto_0
.end method

.method public getTextureDimensions(Landroid/graphics/Point;)Z
    .locals 1
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/Texture;->getTextureDimensions(Landroid/graphics/Point;)Z

    move-result v0

    return v0
.end method

.method public invalidateTexture()V
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/Texture;->release()V

    .line 443
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 444
    return-void
.end method

.method isOccluded(F)Z
    .locals 4
    .param p1, "time"    # F

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 293
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingLeftPageOfTwo()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 294
    iget-boolean v2, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    if-eqz v2, :cond_2

    .line 295
    const v2, 0x3e0f5c29    # 0.14f

    cmpg-float v2, p1, v2

    if-gez v2, :cond_1

    .line 306
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 295
    goto :goto_0

    .line 297
    :cond_2
    const v2, 0x3f7ae148    # 0.98f

    cmpl-float v2, p1, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 299
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 301
    const v2, 0x3c23d70a    # 0.01f

    cmpl-float v2, p1, v2

    if-lez v2, :cond_4

    cmpg-float v2, p1, v3

    if-ltz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 303
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    if-eqz v2, :cond_6

    .line 304
    cmpg-float v2, p1, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 306
    :cond_6
    const v2, 0x3f5eb852    # 0.87f

    cmpl-float v2, p1, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method isUpdating(J)Z
    .locals 13
    .param p1, "now"    # J

    .prologue
    const-wide/16 v10, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 277
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/Page;->getFadeInOpacity(J)F

    move-result v1

    .line 278
    .local v1, "fadeInOpacity":F
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/Page;->getLoadingPresence(J)F

    move-result v2

    .line 279
    .local v2, "loadingPresence":F
    float-to-double v6, v1

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    iget-wide v6, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    cmp-long v6, v6, v10

    if-gtz v6, :cond_0

    iget-wide v6, p0, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    cmp-long v6, v6, v10

    if-lez v6, :cond_3

    float-to-double v6, v2

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    :cond_0
    move v0, v4

    .line 281
    .local v0, "currentPageUpdating":Z
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/Page;->isLoadingSpinnerSpinning(J)Z

    move-result v3

    .line 282
    .local v3, "loadingSpinnerSpinning":Z
    if-nez v0, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkAnimator:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-virtual {v6, p1, p2}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->stillAnimating(J)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/Page;->calculateBookmarkFadeInAlpha(J)F

    move-result v6

    float-to-double v6, v6

    cmpg-double v6, v6, v8

    if-ltz v6, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v5, v4

    :cond_2
    return v5

    .end local v0    # "currentPageUpdating":Z
    .end local v3    # "loadingSpinnerSpinning":Z
    :cond_3
    move v0, v5

    .line 279
    goto :goto_0
.end method

.method public setBackFacing(Z)V
    .locals 1
    .param p1, "backFacing"    # Z

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    if-eq v0, p1, :cond_0

    .line 464
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    .line 466
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mPreviousBookmarkPercent:F

    .line 468
    :cond_0
    return-void
.end method

.method public setDisplaySize(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 455
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/Page;->mDisplaySize:Landroid/graphics/Point;

    .line 456
    return-void
.end method

.method public setLoading(Z)V
    .locals 0
    .param p1, "isLoading"    # Z

    .prologue
    .line 447
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/Page;->mIsLoading:Z

    .line 448
    return-void
.end method

.method public setShowSpinnerWhenLoading(Z)V
    .locals 0
    .param p1, "showSpinnerWhenLoading"    # Z

    .prologue
    .line 451
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/Page;->mShowSpinnerWhenLoading:Z

    .line 452
    return-void
.end method

.method public setupPageStretch(Landroid/graphics/Point;)V
    .locals 4
    .param p1, "blendedTextureDims"    # Landroid/graphics/Point;

    .prologue
    .line 320
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v0, v1, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    .line 321
    .local v0, "renderer":Lcom/google/android/apps/books/view/pages/PageTurnRenderer;
    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/view/pages/Page;->tmpStretchMatrix:[F

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->constructPageStretchContentTransformMatrix([FLandroid/graphics/Point;Landroid/graphics/Point;)Z

    .line 323
    sget-object v1, Lcom/google/android/apps/books/view/pages/Page;->tmpStretchMatrix:[F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setPageStretchMatrix([F)V

    .line 325
    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setMVPUniform()V

    .line 326
    return-void
.end method

.method public shouldShowLoadingSpinner()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mShowSpinnerWhenLoading:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/Page;->mIsLoading:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateTexture(Landroid/graphics/Bitmap;ZLcom/google/android/apps/books/view/pages/PageRenderDetails;)V
    .locals 14
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "potentialPadding"    # Z
    .param p3, "pageRenderDetails"    # Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    .prologue
    .line 119
    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->hasBookmark:Z

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/view/pages/Page;->setBookmarkPresent(Z)V

    .line 121
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    :cond_0
    const-string v1, "books.view.Page"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    const-string v1, "books.view.Page"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Null or recycled bitmap provided for page texture: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    move-object/from16 v0, p3

    iget-boolean v1, v0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->centerBitmapOnPage:Z

    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mCenterBitmapOnPage:Z

    .line 129
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->displaySize:Landroid/graphics/Point;

    iput-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mDisplaySize:Landroid/graphics/Point;

    .line 130
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mIsLoading:Z

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/Texture;->getTextureId()I

    move-result v1

    if-eqz v1, :cond_3

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/Texture;->deleteTextureId()V

    .line 135
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mContentTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 137
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/Texture;->genTextureId()V

    .line 138
    const/16 v1, 0xde1

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/Texture;->getTextureId()I

    move-result v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 139
    invoke-static {}, Lcom/google/android/opengl/carousel/GL2Helper;->setDefaultNPOTTextureState()V

    .line 141
    if-eqz p2, :cond_a

    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/Page;->getCenterBitmapOnPage()Z

    move-result v1

    if-nez v1, :cond_a

    .line 142
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v9, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v9}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->displayTwoPages()Z

    move-result v9

    iget-boolean v13, p0, Lcom/google/android/apps/books/view/pages/Page;->backFacing:Z

    invoke-virtual {v1, v2, v6, v9, v13}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->calculateNeededBorders(IIZZ)Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;

    move-result-object v11

    .line 145
    .local v11, "borders":Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->left:I

    add-int/2addr v1, v2

    iget v2, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->right:I

    add-int v4, v1, v2

    .line 146
    .local v4, "textureWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->top:I

    add-int/2addr v1, v2

    iget v2, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->bottom:I

    add-int v5, v1, v2

    .line 147
    .local v5, "textureHeight":I
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/books/view/pages/Texture;->setExtent(II)V

    .line 149
    invoke-static {p1}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v3

    .line 150
    .local v3, "int_format":I
    invoke-static {p1}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v8

    .line 151
    .local v8, "type":I
    const v1, 0x8363

    if-ne v8, v1, :cond_9

    const/16 v7, 0x1907

    .line 154
    .local v7, "format":I
    :goto_1
    const-string v1, "books.view.Page"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 155
    const-string v1, "books.view.Page"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateTexture: width: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " height: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_4
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 163
    const/16 v1, 0xde1

    const/4 v2, 0x0

    iget v6, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->left:I

    iget v9, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->top:I

    invoke-static {v1, v2, v6, v9, p1}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 166
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v12

    .line 167
    .local v12, "config":Landroid/graphics/Bitmap$Config;
    iget v1, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->left:I

    if-lez v1, :cond_5

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5, v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getBorderBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 169
    .local v10, "borderBitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-static {v1, v2, v6, v9, v10}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 172
    .end local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    :cond_5
    iget v1, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->right:I

    if-lez v1, :cond_6

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5, v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getBorderBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 174
    .restart local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0xde1

    const/4 v2, 0x0

    add-int/lit8 v6, v4, -0x1

    const/4 v9, 0x0

    invoke-static {v1, v2, v6, v9, v10}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 177
    .end local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    :cond_6
    iget v1, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->top:I

    if-lez v1, :cond_7

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    const/4 v2, 0x1

    invoke-virtual {v1, v4, v2, v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getBorderBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 179
    .restart local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-static {v1, v2, v6, v9, v10}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 182
    .end local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    :cond_7
    iget v1, v11, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->bottom:I

    if-lez v1, :cond_8

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    const/4 v2, 0x1

    invoke-virtual {v1, v4, v2, v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getBorderBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 184
    .restart local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/4 v6, 0x0

    add-int/lit8 v9, v5, -0x1

    invoke-static {v1, v2, v6, v9, v10}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;)V

    .line 195
    .end local v3    # "int_format":I
    .end local v4    # "textureWidth":I
    .end local v5    # "textureHeight":I
    .end local v7    # "format":I
    .end local v8    # "type":I
    .end local v10    # "borderBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "borders":Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    .end local v12    # "config":Landroid/graphics/Bitmap$Config;
    :cond_8
    :goto_2
    const-string v1, "updateTexture"

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/view/pages/Page;->logError(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 151
    .restart local v3    # "int_format":I
    .restart local v4    # "textureWidth":I
    .restart local v5    # "textureHeight":I
    .restart local v8    # "type":I
    .restart local v11    # "borders":Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    :cond_9
    const/16 v7, 0x1908

    goto/16 :goto_1

    .line 189
    .end local v3    # "int_format":I
    .end local v4    # "textureWidth":I
    .end local v5    # "textureHeight":I
    .end local v8    # "type":I
    .end local v11    # "borders":Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/Page;->mTexture:Lcom/google/android/apps/books/view/pages/Texture;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v1, v2, v6}, Lcom/google/android/apps/books/view/pages/Texture;->setExtent(II)V

    .line 190
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-static {v1, v2, p1, v6}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto :goto_2
.end method
