.class Lcom/google/android/apps/books/annotations/AnnotationServerController$1;
.super Ljava/lang/Object;
.source "AnnotationServerController.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationServerController;->downloadUserAnnotations(Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field requestInUse:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationServerController;

.field final synthetic val$processor:Lcom/google/android/apps/books/annotations/AnnotationProcessor;

.field final synthetic val$request:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationServerController;Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V
    .locals 1

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->val$request:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->val$processor:Lcom/google/android/apps/books/annotations/AnnotationProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->val$request:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->requestInUse:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    return-void
.end method


# virtual methods
.method public getPage(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 4
    .param p1, "continuation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;
    invoke-static {v1}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->access$000(Lcom/google/android/apps/books/annotations/AnnotationServerController;)Lcom/google/android/apps/books/annotations/AnnotationServer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->requestInUse:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    const/16 v3, 0x28

    invoke-interface {v1, v2, v3, p1}, Lcom/google/android/apps/books/annotations/AnnotationServer;->getUserAnnotations(Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    :try_end_0
    .catch Lcom/google/android/apps/books/annotations/LastSyncTooOldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 113
    :goto_0
    return-object v1

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Lcom/google/android/apps/books/annotations/LastSyncTooOldException;
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->val$processor:Lcom/google/android/apps/books/annotations/AnnotationProcessor;

    invoke-interface {v1}, Lcom/google/android/apps/books/annotations/AnnotationProcessor;->reset()V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->requestInUse:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->fullRefresh()Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->requestInUse:Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    .line 113
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;->getPage(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v1

    goto :goto_0
.end method
