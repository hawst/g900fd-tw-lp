.class public Lcom/google/android/apps/books/widget/RecommendationCardColumnView;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardView;
.source "RecommendationCardColumnView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/RecommendationCardColumnView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->onFinishInflate()V

    .line 28
    const v0, 0x7f0e01b5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RecommendationCardColumnView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendationCardColumnView;->mActionTouchArea:Landroid/view/View;

    .line 29
    const v0, 0x7f0e00eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RecommendationCardColumnView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendationCardColumnView;->mAction:Landroid/widget/TextView;

    .line 30
    return-void
.end method
