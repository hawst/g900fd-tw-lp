.class public Lcom/google/android/apps/books/widget/RecommendedAdapter;
.super Landroid/widget/BaseAdapter;
.source "RecommendedAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/DescribingListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    }
.end annotation


# instance fields
.field final mBooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

.field private final mContext:Landroid/content/Context;

.field private mIsOnline:Z

.field private final mMaxResults:I

.field private final mPopulator:Lcom/google/android/apps/books/widget/RecommendationViewPopulator;

.field private final mShowReasons:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/widget/RecommendationViewPopulator;ZIZLcom/google/android/apps/books/app/BooksFragmentCallbacks;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "populator"    # Lcom/google/android/apps/books/widget/RecommendationViewPopulator;
    .param p3, "isOnline"    # Z
    .param p4, "maxResults"    # I
    .param p5, "showReasons"    # Z
    .param p6, "callbacks"    # Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 57
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mBooks:Ljava/util/List;

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mContext:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mPopulator:Lcom/google/android/apps/books/widget/RecommendationViewPopulator;

    .line 70
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mIsOnline:Z

    .line 71
    iput p4, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mMaxResults:I

    .line 72
    iput-boolean p5, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mShowReasons:Z

    .line 73
    iput-object p6, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    .line 74
    return-void
.end method

.method private trimToSize(Ljava/util/List;I)Ljava/util/List;
    .locals 1
    .param p2, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p2, :cond_0

    .line 123
    .end local p1    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :goto_0
    return-object p1

    .restart local p1    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mIsOnline:Z

    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mBooks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mBooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 92
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 97
    invoke-static {p2, p3}, Lcom/google/android/apps/books/widget/BooksListItemView;->createOrReuseView(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/books/widget/BooksListItemView;

    move-result-object v0

    .line 98
    .local v0, "bliView":Lcom/google/android/apps/books/widget/BooksListItemView;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mPopulator:Lcom/google/android/apps/books/widget/RecommendationViewPopulator;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mBooks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mShowReasons:Z

    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mCallbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->fillView(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;ZLcom/google/android/apps/books/app/BooksFragmentCallbacks;)V

    .line 100
    if-nez p1, :cond_0

    .line 101
    const v1, 0x7f0e00e7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    :cond_0
    return-object v0
.end method

.method public setRecommendations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mBooks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mBooks:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter;->mMaxResults:I

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/widget/RecommendedAdapter;->trimToSize(Ljava/util/List;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RecommendedAdapter;->notifyDataSetChanged()V

    .line 117
    return-void
.end method
