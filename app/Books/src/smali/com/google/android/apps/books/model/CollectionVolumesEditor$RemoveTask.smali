.class final Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;
.super Landroid/os/AsyncTask;
.source "CollectionVolumesEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/CollectionVolumesEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RemoveTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCollectionId:J

.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/model/CollectionVolumesEditor;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/model/CollectionVolumesEditor;JLjava/lang/String;)V
    .locals 0
    .param p2, "collectionId"    # J
    .param p4, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->this$0:Lcom/google/android/apps/books/model/CollectionVolumesEditor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 65
    iput-wide p2, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->mCollectionId:J

    .line 66
    iput-object p4, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->mVolumeId:Ljava/lang/String;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/CollectionVolumesEditor;JLjava/lang/String;Lcom/google/android/apps/books/model/CollectionVolumesEditor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/CollectionVolumesEditor;
    .param p2, "x1"    # J
    .param p4, "x2"    # Ljava/lang/String;
    .param p5, "x3"    # Lcom/google/android/apps/books/model/CollectionVolumesEditor$1;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;-><init>(Lcom/google/android/apps/books/model/CollectionVolumesEditor;JLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 59
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->this$0:Lcom/google/android/apps/books/model/CollectionVolumesEditor;

    iget-wide v2, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->mCollectionId:J

    iget-object v1, p0, Lcom/google/android/apps/books/model/CollectionVolumesEditor$RemoveTask;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->synchronousRemove(JLjava/lang/String;)V

    .line 72
    const/4 v0, 0x0

    return-object v0
.end method
