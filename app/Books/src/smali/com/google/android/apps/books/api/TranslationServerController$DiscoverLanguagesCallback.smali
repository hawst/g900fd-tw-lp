.class public interface abstract Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;
.super Ljava/lang/Object;
.source "TranslationServerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/TranslationServerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DiscoverLanguagesCallback"
.end annotation


# virtual methods
.method public abstract onCompleted(Lcom/google/common/collect/BiMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onError(Ljava/lang/Exception;)V
.end method
