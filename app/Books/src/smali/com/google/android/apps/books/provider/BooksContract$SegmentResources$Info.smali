.class public Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;
.super Ljava/lang/Object;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# instance fields
.field public final accountName:Ljava/lang/String;

.field public final resourceId:Ljava/lang/String;

.field public final segmentId:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "acct"    # Ljava/lang/String;
    .param p2, "vol"    # Ljava/lang/String;
    .param p3, "seg"    # Ljava/lang/String;
    .param p4, "res"    # Ljava/lang/String;

    .prologue
    .line 2549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2550
    iput-object p1, p0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->accountName:Ljava/lang/String;

    .line 2551
    iput-object p2, p0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->volumeId:Ljava/lang/String;

    .line 2552
    iput-object p3, p0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->segmentId:Ljava/lang/String;

    .line 2553
    iput-object p4, p0, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources$Info;->resourceId:Ljava/lang/String;

    .line 2554
    return-void
.end method
