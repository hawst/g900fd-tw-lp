.class Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$2;
.super Ljava/lang/Object;
.source "AndroidSpeechSynthesizer.java"

# interfaces
.implements Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$Toaster;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;ZLjava/util/Locale;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$2;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toast(ILjava/util/Locale;)V
    .locals 6
    .param p1, "messageId"    # I
    .param p2, "bookLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v5, 0x1

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$2;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    # getter for: Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->access$100(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$2;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    # getter for: Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->access$100(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Landroid/content/Context;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 221
    return-void
.end method
