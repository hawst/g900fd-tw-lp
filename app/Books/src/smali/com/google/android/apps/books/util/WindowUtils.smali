.class public Lcom/google/android/apps/books/util/WindowUtils;
.super Ljava/lang/Object;
.source "WindowUtils.java"


# direct methods
.method public static setBrightness(ILandroid/view/Window;)V
    .locals 3
    .param p0, "brightness"    # I
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    const/4 v2, -0x1

    .line 21
    if-eq p0, v2, :cond_0

    const/4 v1, 0x7

    if-ge p0, v1, :cond_0

    .line 22
    const/4 p0, 0x7

    .line 24
    :cond_0
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 25
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    if-eq p0, v2, :cond_1

    int-to-float v1, p0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    :goto_0
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 26
    invoke-virtual {p1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 27
    return-void

    .line 25
    :cond_1
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0
.end method
