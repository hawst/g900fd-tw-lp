.class Lcom/google/android/apps/books/navigation/BookNavView$12;
.super Ljava/lang/Object;
.source "BookNavView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/BookNavView;->setupPageEditor()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$12;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 781
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 782
    .local v1, "pageName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$12;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$3200(Lcom/google/android/apps/books/navigation/BookNavView;)Ljava/util/TreeMap;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x0

    .line 784
    .local v0, "nextPage":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .line 785
    .local v2, "validInput":Z
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$12;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->setPageNumberValid(Z)V
    invoke-static {v3, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$3300(Lcom/google/android/apps/books/navigation/BookNavView;Z)V

    .line 786
    return-void

    .line 782
    .end local v0    # "nextPage":Ljava/lang/String;
    .end local v2    # "validInput":Z
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView$12;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$3200(Lcom/google/android/apps/books/navigation/BookNavView;)Ljava/util/TreeMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v0, v3

    goto :goto_0

    .line 784
    .restart local v0    # "nextPage":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i2"    # I
    .param p4, "i3"    # I

    .prologue
    .line 773
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i2"    # I
    .param p4, "i3"    # I

    .prologue
    .line 777
    return-void
.end method
