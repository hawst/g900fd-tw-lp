.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;
.super Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DismissRecommendationMenuEntry"
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mActionType:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

.field private final mTaskContext:Landroid/content/Context;

.field private final mVolumeId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)V
    .locals 3
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "account"    # Landroid/accounts/Account;
    .param p5, "actionType"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .prologue
    .line 688
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .line 689
    const/4 v0, -0x1

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p5}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->getUiStringResourceId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(ILjava/lang/String;Z)V

    .line 690
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mVolumeId:Ljava/lang/String;

    .line 691
    iput-object p3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mTaskContext:Landroid/content/Context;

    .line 692
    iput-object p4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mAccount:Landroid/accounts/Account;

    .line 693
    iput-object p5, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mActionType:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .line 694
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 5

    .prologue
    .line 698
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mTaskContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mAccount:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mVolumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mActionType:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)V

    .line 701
    .local v0, "task":Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 702
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;->mVolumeId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->removeFromRecommendations(Ljava/lang/String;)V

    .line 703
    return-void
.end method
