.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->fetchAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnnotationDatas(Ljava/util/Map;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lorg/codehaus/jackson/JsonNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 438
    .local p1, "datas":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    .local p2, "jsons":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lorg/codehaus/jackson/JsonNode;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->deliverAnnotationDataResults(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Map;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$700(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Map;)V

    .line 439
    return-void
.end method
