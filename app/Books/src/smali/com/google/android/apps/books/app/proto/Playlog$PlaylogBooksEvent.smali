.class public final Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Playlog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/proto/Playlog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaylogBooksEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;,
        Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;


# instance fields
.field private clientId_:J

.field private hasClientId:Z

.field private hasType:Z

.field private memoizedSerializedSize:I

.field private type_:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->defaultInstance:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    .line 344
    invoke-static {}, Lcom/google/android/apps/books/app/proto/Playlog;->internalForceInit()V

    .line 345
    sget-object v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->defaultInstance:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->initFields()V

    .line 346
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->clientId_:J

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/proto/Playlog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/proto/Playlog$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->clientId_:J

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasClientId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .param p1, "x1"    # J

    .prologue
    .line 11
    iput-wide p1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->clientId_:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasType:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->type_:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->defaultInstance:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->UNKNOWN:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    iput-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->type_:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    .line 85
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .locals 1

    .prologue
    .line 186
    # invokes: Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->create()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->access$100()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClientId()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->clientId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->memoizedSerializedSize:I

    .line 104
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 116
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 106
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasClientId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getClientId()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 111
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasType()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getType()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_2
    iput v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->memoizedSerializedSize:I

    move v1, v0

    .line 116
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getType()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->type_:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    return-object v0
.end method

.method public hasClientId()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasClientId:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasType:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getSerializedSize()I

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasClientId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getClientId()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->hasType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->getType()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 99
    :cond_1
    return-void
.end method
