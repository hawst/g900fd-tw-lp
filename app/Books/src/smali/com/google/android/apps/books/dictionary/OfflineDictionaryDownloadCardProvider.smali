.class public Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;
.super Lcom/google/android/apps/books/app/InfoCardProvider;
.source "OfflineDictionaryDownloadCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;
    }
.end annotation


# instance fields
.field private final mBackgroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mBookLocale:Ljava/util/Locale;

.field private mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private mCallback:Ljava/lang/Runnable;

.field private final mContext:Landroid/content/Context;

.field private final mForegroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mRequestedLanguages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServerMetadataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/data/BooksDataController;Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bookLocale"    # Ljava/util/Locale;
    .param p3, "backgroundDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p4, "foregroundDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p5, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardProvider;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mContext:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBookLocale:Ljava/util/Locale;

    .line 48
    iput-object p3, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBackgroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 49
    iput-object p4, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mForegroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 50
    iput-object p5, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->maybeGetOfflineCard()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->download()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->launchDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mForegroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method private download()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mForegroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mRequestedLanguages:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->setRequestedDictionaryLanguages(Ljava/util/List;)V

    .line 208
    return-void
.end method

.method private launchDialog()V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mServerMetadataList:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mRequestedLanguages:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->newInstance(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v2, "LocalDictDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method private maybeGetOfflineCard()Landroid/view/View;
    .locals 6

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->shouldShowCardAndFetchDependencies()Z

    move-result v4

    if-nez v4, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 137
    :goto_0
    return-object v0

    .line 91
    :cond_0
    const v4, 0x7f040070

    iget-object v5, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mContext:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/ViewUtils;->inflateWithContext(ILandroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 93
    .local v0, "cardView":Landroid/view/ViewGroup;
    const v4, 0x7f0e014e

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 94
    .local v1, "contentView":Landroid/view/View;
    const v4, 0x7f0e014d

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 96
    .local v3, "progressView":Landroid/view/View;
    new-instance v2, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$1;

    invoke-direct {v2, p0, v0, v1, v3}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$1;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 115
    .local v2, "fadeout":Ljava/lang/Runnable;
    iput-object v2, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mCallback:Ljava/lang/Runnable;

    .line 117
    const v4, 0x7f0e014f

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;Landroid/view/ViewGroup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    const v4, 0x7f0e0150

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$3;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$3;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    const v4, 0x7f0e014c

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$4;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$4;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public loadCards(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    new-instance v0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$LookupTask;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;Lcom/google/android/ublib/utils/Consumer;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method public shouldShowCardAndFetchDependencies()Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 145
    new-instance v6, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mContext:Landroid/content/Context;

    invoke-direct {v6, v8}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/android/apps/books/preference/LocalPreferences;->shouldShowOfflineDictionaryCard()Z

    move-result v6

    if-nez v6, :cond_0

    move v6, v7

    .line 192
    :goto_0
    return v6

    .line 150
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBookLocale:Ljava/util/Locale;

    if-nez v6, :cond_1

    move v6, v7

    .line 151
    goto :goto_0

    .line 155
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v0

    .line 157
    .local v0, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBackgroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v6, v0}, Lcom/google/android/apps/books/data/BooksDataController;->getRequestedDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V

    .line 158
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 159
    .local v1, "dictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 160
    .local v5, "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 161
    .local v4, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v4}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    .end local v0    # "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    .end local v1    # "dictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .end local v5    # "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/InterruptedException;
    move v6, v7

    .line 190
    goto :goto_0

    .line 163
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    .restart local v1    # "dictionaryMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "requestedLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iput-object v5, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mRequestedLanguages:Ljava/util/List;

    .line 164
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mRequestedLanguages:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBookLocale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v6, v7

    .line 165
    goto :goto_0

    .line 167
    :cond_3
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v0

    .line 168
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBackgroundBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v6, v0}, Lcom/google/android/apps/books/data/BooksDataController;->getServerDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V

    .line 169
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    iput-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mServerMetadataList:Ljava/util/List;

    .line 170
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mServerMetadataList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 171
    .restart local v4    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v4}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBookLocale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 172
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mRequestedLanguages:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBookLocale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    new-instance v6, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$5;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$5;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)V

    iput-object v6, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mBooksDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 179
    new-instance v6, Landroid/os/Handler;

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v8, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$6;

    invoke-direct {v8, p0}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$6;-><init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)V

    invoke-virtual {v6, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    const/4 v6, 0x1

    goto/16 :goto_0

    .end local v4    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_5
    move v6, v7

    .line 192
    goto/16 :goto_0
.end method
