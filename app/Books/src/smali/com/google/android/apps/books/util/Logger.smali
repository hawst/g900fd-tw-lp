.class public interface abstract Lcom/google/android/apps/books/util/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/Logger$Category;
    }
.end annotation


# virtual methods
.method public abstract log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V
.end method

.method public abstract log(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z
.end method

.method public abstract shouldLog(Ljava/lang/String;)Z
.end method
