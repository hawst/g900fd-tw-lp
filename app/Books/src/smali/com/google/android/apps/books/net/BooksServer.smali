.class public interface abstract Lcom/google/android/apps/books/net/BooksServer;
.super Ljava/lang/Object;
.source "BooksServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;,
        Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    }
.end annotation


# virtual methods
.method public abstract acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/app/DeviceInfo;",
            ")",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract addCloudloadingVolume(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract addVolumeToMyEbooks(Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract deleteCloudloadedVolume(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract dismissOffer(Ljava/lang/String;Lcom/google/android/apps/books/app/DeviceInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getAudioResourceContent(Ljava/lang/String;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/CcBox;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation
.end method

.method public abstract getCoverImage(Ljava/lang/String;I)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getDictionaryMetadata(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFailedCloudeloadedVolumes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getNewSessionKey(Ljava/util/List;)Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ">;)",
            "Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method

.method public abstract getOffers(Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPageImage(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPageStructure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getResourceContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation
.end method

.method public abstract getSampleCategories(Ljava/util/Locale;)Lcom/google/android/apps/books/api/data/SampleCategories;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSampleVolumes(Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/android/apps/books/api/data/SampleVolumes;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/api/data/SampleVolumes;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSegmentContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/String;Z)Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation
.end method

.method public abstract getSharedFontContent(Ljava/lang/String;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getUserSettings()Lcom/google/android/apps/books/api/data/UserSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVolumeManifest(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVolumeOverview(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/ApiaryVolume;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract releaseOfflineLicense(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
.end method

.method public abstract removeVolumeFromMyEbooks(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract requestVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract syncVolumeLicenses(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/android/apps/books/model/SessionKey;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryVolume;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method

.method public abstract updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;)Lcom/google/android/apps/books/api/data/UserSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
