.class Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask$1;
.super Ljava/lang/Object;
.source "DictionaryCardProvider.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->addAttributionView(Landroid/view/ViewGroup;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;

.field final synthetic val$attribution:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask$1;->this$1:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;

    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask$1;->val$attribution:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 284
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask$1;->val$attribution:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 287
    return-void
.end method
