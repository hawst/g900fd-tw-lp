.class Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;
.super Ljava/lang/Object;
.source "TabletBooksApplication.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->startingVolumeDownload(Lcom/google/android/apps/books/model/VolumeData;IILandroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field final mThumbnailVolumeId:Ljava/lang/String;

.field final synthetic this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    iput-object p2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->val$volumeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->mThumbnailVolumeId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    const/4 v3, 0x3

    .line 190
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->mThumbnailVolumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # getter for: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadingVolumeId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$100(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 193
    .local v0, "thumbnail":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 194
    const-string v1, "SyncUI"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const-string v1, "SyncUI"

    const-string v2, "thumbnail is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    .end local v0    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 199
    .restart local v0    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # getter for: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {v1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$200(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    if-nez v1, :cond_2

    .line 200
    const-string v1, "SyncUI"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    const-string v1, "SyncUI"

    const-string v2, "thumbnail arrived too late"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 205
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # getter for: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {v1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$200(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->updateVolumeDownload(I)V

    goto :goto_0

    .line 208
    .end local v0    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_3
    const-string v1, "SyncUI"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209
    const-string v1, "SyncUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waiting for thumbnail for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # getter for: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadingVolumeId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$100(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but got one for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->mThumbnailVolumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 215
    :cond_4
    const-string v1, "SyncUI"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    const-string v1, "SyncUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to get thumbnail for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 185
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
