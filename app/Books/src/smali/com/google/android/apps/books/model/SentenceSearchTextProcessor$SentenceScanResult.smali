.class Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
.super Ljava/lang/Object;
.source "SentenceSearchTextProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SentenceScanResult"
.end annotation


# static fields
.field static final INCOMPLETE_MATCHING:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

.field static final INCOMPLETE_NOT_MATCHING:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;


# instance fields
.field private final mIsComplete:Z

.field private final mMatchRanges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;"
        }
    .end annotation
.end field

.field private final mSnippet:Lcom/google/android/apps/books/model/Snippet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult$1;

    invoke-direct {v0, v1, v1, v2}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult$1;-><init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V

    sput-object v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->INCOMPLETE_MATCHING:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .line 77
    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    invoke-direct {v0, v1, v1, v2}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;-><init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V

    sput-object v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->INCOMPLETE_NOT_MATCHING:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V
    .locals 0
    .param p2, "snippet"    # Lcom/google/android/apps/books/model/Snippet;
    .param p3, "isComplete"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;",
            "Lcom/google/android/apps/books/model/Snippet;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    .line 87
    iput-object p2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    .line 88
    iput-boolean p3, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mIsComplete:Z

    .line 89
    return-void
.end method

.method public static varargs buildSearchResult([Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;)Lcom/google/android/apps/books/model/SearchResult;
    .locals 9
    .param p0, "sentences"    # [Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .prologue
    .line 111
    sget-object v5, Lcom/google/android/apps/books/model/Snippet;->EMPTY:Lcom/google/android/apps/books/model/Snippet;

    .line 112
    .local v5, "snippet":Lcom/google/android/apps/books/model/Snippet;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 113
    .local v3, "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v4, v0, v1

    .line 114
    .local v4, "sentence":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    if-eqz v4, :cond_1

    .line 115
    iget-object v6, v4, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    if-eqz v6, :cond_0

    .line 116
    iget-object v6, v4, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/model/Snippet;->concat(Lcom/google/android/apps/books/model/Snippet;)Lcom/google/android/apps/books/model/Snippet;

    move-result-object v5

    .line 118
    :cond_0
    iget-object v6, v4, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    if-eqz v6, :cond_1

    .line 119
    iget-object v6, v4, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 113
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    .end local v4    # "sentence":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    :cond_2
    new-instance v6, Lcom/google/android/apps/books/model/SearchResult;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct {v6, v5, v3, v7, v8}, Lcom/google/android/apps/books/model/SearchResult;-><init>(Lcom/google/android/apps/books/model/Snippet;Ljava/util/List;ZI)V

    return-object v6
.end method


# virtual methods
.method public getSnippet()Lcom/google/android/apps/books/model/Snippet;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    return-object v0
.end method

.method public hasMatch()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mIsComplete:Z

    return v0
.end method

.method public matchStartLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SentenceScanResult{mMatchRanges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mMatchRanges:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSnippet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isComplete()="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->isComplete()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasMatch()="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->hasMatch()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withNoResults()Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    .locals 4

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->hasMatch()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    .end local p0    # "this":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/Snippet;->withoutSpans()Lcom/google/android/apps/books/model/Snippet;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->mIsComplete:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;-><init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V

    move-object p0, v0

    goto :goto_0
.end method
