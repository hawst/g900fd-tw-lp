.class public Lcom/google/android/apps/books/app/StubReaderCallbacks;
.super Lcom/google/android/apps/books/app/StubFragmentCallbacks;
.source "StubReaderCallbacks.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptNewPosition(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 47
    return-void
.end method

.method public closeBook()V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 52
    return-void
.end method

.method public getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 68
    return-void
.end method

.method public onHomePressed()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 63
    return-void
.end method

.method public populateReaderActionBar(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "subtitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 20
    return-void
.end method

.method public restartCurrentActivity(Z)V
    .locals 0
    .param p1, "showDisplaySettingsOnRecreate"    # Z

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 36
    return-void
.end method

.method public setActionBarElevation(F)V
    .locals 0
    .param p1, "elevation"    # F

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 25
    return-void
.end method

.method public showNewPositionAvailableDialog(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p4, "chapterTitle"    # Ljava/lang/String;
    .param p5, "pageTitle"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 42
    return-void
.end method

.method public temporarilyOverrideWindowAccessibilityAnnouncements(Ljava/lang/String;)V
    .locals 0
    .param p1, "stringToSay"    # Ljava/lang/String;

    .prologue
    .line 77
    return-void
.end method

.method public updateTheme(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubReaderCallbacks;->maybeLogMethodName()V

    .line 30
    const/4 v0, 0x0

    return v0
.end method
