.class Lcom/google/android/apps/books/widget/GeoAnnotationView$4;
.super Ljava/lang/Object;
.source "GeoAnnotationView.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/GeoAnnotationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # setter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapPreviewImage:Lcom/google/android/apps/books/util/ExceptionOr;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$302(Lcom/google/android/apps/books/widget/GeoAnnotationView;Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;

    .line 340
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # invokes: Lcom/google/android/apps/books/widget/GeoAnnotationView;->maybeFinishedLoading()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$400(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    .line 345
    :goto_0
    return-void

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$500(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;->onFinishedLoading(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 336
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
