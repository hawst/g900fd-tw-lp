.class Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;
.super Ljava/lang/Object;
.source "SingleBookUploaderImpl.java"

# interfaces
.implements Lcom/google/android/libraries/uploader/UploadHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScottyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
    .param p2, "x1"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;-><init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V

    return-void
.end method


# virtual methods
.method public onUploadCanceled()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/Upload;->setUploadStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->deleteAndCleanUp()V
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$1200(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V

    .line 312
    return-void
.end method

.method public onUploadCompleted()V
    .locals 3

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    if-eq v0, v1, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$700(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_COMPLETED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->sendApiaryRequest()V
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$1100(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V

    .line 306
    :cond_0
    return-void
.end method

.method public onUploadError()V
    .locals 4

    .prologue
    .line 316
    const-string v1, "SingleBookUploaderImpl"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$1000(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/uploader/client/Transfer;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/uploader/AndroidTransfer;

    .line 318
    .local v0, "androidTransfer":Lcom/google/android/libraries/uploader/AndroidTransfer;
    const-string v1, "SingleBookUploaderImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "scotty error response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const-string v1, "SingleBookUploaderImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "scotty error response headers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    .end local v0    # "androidTransfer":Lcom/google/android/libraries/uploader/AndroidTransfer;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Error while uploading the file to scotty"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$400(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Ljava/lang/Exception;Z)V

    .line 323
    return-void
.end method

.method public onUploadReady()V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$700(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ACTIVE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$700(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;
    invoke-static {v2}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$1000(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/uploader/client/Transfer;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/uploader/client/Transfer;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateUploadTransferHandle(Ljava/lang/String;Lcom/google/uploader/client/ClientProto$TransferHandle;)V

    .line 298
    return-void
.end method
