.class Lcom/google/android/apps/books/data/OffersSubcontroller$2;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "OffersSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/OffersSubcontroller;->acceptOffer(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

.field final synthetic val$offerId:Ljava/lang/String;

.field final synthetic val$onResult:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeIds:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$offerId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$volumeIds:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 186
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$offerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$volumeIds:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;
    invoke-static {v5}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$100(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/app/DeviceInfo;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/net/BooksServer;->acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;

    move-result-object v1

    .line 188
    .local v1, "response":Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    iget-object v2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$offerId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->saveDismissedOfferOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V
    invoke-static {v2, p1, v3}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$000(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .end local v1    # "response":Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;
    :goto_0
    return-void

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;->val$onResult:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method
