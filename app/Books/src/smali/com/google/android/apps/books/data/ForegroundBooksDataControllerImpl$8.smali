.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getVolumeSearches(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

.field final synthetic val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 645
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 646
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$502(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/List;)Ljava/util/List;

    .line 651
    :goto_0
    return-void

    .line 649
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$502(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/List;)Ljava/util/List;

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 642
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
