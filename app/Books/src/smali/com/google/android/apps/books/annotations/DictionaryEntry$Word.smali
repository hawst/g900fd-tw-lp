.class public Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/DictionaryEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Word"
.end annotation


# instance fields
.field public final derivatives:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;"
        }
    .end annotation
.end field

.field public final phrases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;"
        }
    .end annotation
.end field

.field public final senses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;",
            ">;"
        }
    .end annotation
.end field

.field public final source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V
    .locals 0
    .param p4, "source"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "senses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;>;"
    .local p2, "phrases":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    .local p3, "derivatives":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->phrases:Ljava/util/List;

    .line 50
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->derivatives:Ljava/util/List;

    .line 51
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .line 52
    return-void
.end method
