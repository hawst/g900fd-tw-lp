.class public Lcom/google/android/apps/books/app/TabletBooksApplication;
.super Lcom/google/android/apps/books/app/BooksApplication;
.source "TabletBooksApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;
    }
.end annotation


# instance fields
.field private final mCarouselCoverCache:Lcom/google/android/apps/books/util/GenerationCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/GenerationCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mNumResumedActivities:I

.field private final mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksApplication;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/apps/books/util/GenerationCache;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/util/GenerationCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mCarouselCoverCache:Lcom/google/android/apps/books/util/GenerationCache;

    .line 78
    new-instance v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;-><init>(Lcom/google/android/apps/books/app/TabletBooksApplication;Lcom/google/android/apps/books/app/TabletBooksApplication$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;

    .line 90
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/TabletBooksApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->isAnyActivityResumed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/TabletBooksApplication;Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication;
    .param p1, "x1"    # Landroid/content/res/Resources;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Z

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/app/TabletBooksApplication;->getBookAddedNotificationTicker(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static decrementNumResumedActivities(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/TabletBooksApplication;

    iget v1, v0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mNumResumedActivities:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mNumResumedActivities:I

    .line 64
    return-void
.end method

.method private getBookAddedNotificationTicker(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "volumeCount"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "author"    # Ljava/lang/String;
    .param p5, "alwaysPluralize"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 526
    if-nez p5, :cond_1

    if-ne p2, v2, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p3    # "title":Ljava/lang/String;
    :goto_0
    return-object p3

    .restart local p3    # "title":Ljava/lang/String;
    :cond_0
    const v0, 0x7f0f0188

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v3

    aput-object p4, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_1
    const v0, 0x7f110004

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0
.end method

.method public static incrementNumResumedActivities(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 59
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/TabletBooksApplication;

    iget v1, v0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mNumResumedActivities:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mNumResumedActivities:I

    .line 60
    return-void
.end method

.method private isAnyActivityResumed()Z
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mNumResumedActivities:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static maybeRecommendUninstallApp(Landroid/app/Activity;)Z
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 541
    if-eqz p0, :cond_0

    .line 542
    invoke-virtual {p0}, Landroid/app/Activity;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 543
    .local v0, "filesDir":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-nez v1, :cond_0

    .line 544
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->recommendUninstallRunnable(Landroid/app/Activity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 545
    const/4 v1, 0x1

    .line 548
    .end local v0    # "filesDir":Ljava/io/File;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static recommendUninstallRunnable(Landroid/app/Activity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 552
    new-instance v0, Lcom/google/android/apps/books/app/TabletBooksApplication$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$1;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method


# virtual methods
.method public getSyncUi()Lcom/google/android/apps/books/service/SyncService$SyncUi;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication;->mSyncUi:Lcom/google/android/apps/books/service/SyncService$SyncUi;

    return-object v0
.end method
