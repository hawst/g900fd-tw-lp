.class public Lcom/google/android/apps/books/widget/RemoteCardImageView;
.super Lcom/google/android/apps/books/widget/RemoteImageView;
.source "RemoteCardImageView.java"


# instance fields
.field private mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/RemoteCardImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/RemoteCardImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "set"    # Landroid/util/AttributeSet;
    .param p3, "defaultStyle"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/RemoteImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    sget-object v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RemoteCardImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 22
    return-void
.end method

.method private findCardImageView()Lcom/google/android/ublib/cardlib/PlayCardArtImageView;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->getImageViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    return-object v0
.end method


# virtual methods
.method protected handleImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->findCardImageView()Lcom/google/android/ublib/cardlib/PlayCardArtImageView;

    move-result-object v0

    .line 42
    .local v0, "imageView":Lcom/google/android/ublib/cardlib/PlayCardArtImageView;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/RemoteCardImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 43
    invoke-virtual {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardArtImageView;->onImage(Landroid/graphics/Bitmap;)V

    .line 44
    return-void
.end method

.method public setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V
    .locals 0
    .param p1, "fillStyle"    # Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RemoteCardImageView;->mFillStyle:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    .line 37
    return-void
.end method
