.class public interface abstract Lcom/google/android/apps/books/model/SegmentSource;
.super Ljava/lang/Object;
.source "SegmentSource.java"


# virtual methods
.method public abstract getNumberOfSegments()I
.end method

.method public abstract getSegmentIdAtIndex(I)Ljava/lang/String;
.end method

.method public abstract getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
.end method
