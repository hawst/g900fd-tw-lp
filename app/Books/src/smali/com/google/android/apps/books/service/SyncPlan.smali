.class public Lcom/google/android/apps/books/service/SyncPlan;
.super Ljava/lang/Object;
.source "SyncPlan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    }
.end annotation


# instance fields
.field public final volumesAddedToLibrary:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end field

.field public final volumesToDownload:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/service/SyncPlan;->volumesToDownload:Ljava/util/List;

    .line 45
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/service/SyncPlan;->volumesAddedToLibrary:Ljava/util/List;

    return-void
.end method

.method public static buildSyncPlan(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;JZ)Lcom/google/android/apps/books/service/SyncPlan;
    .locals 11
    .param p0, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .param p1, "lastSync"    # J
    .param p3, "downloadContentSilently"    # Z

    .prologue
    .line 50
    new-instance v10, Lcom/google/android/apps/books/service/SyncPlan;

    invoke-direct {v10}, Lcom/google/android/apps/books/service/SyncPlan;-><init>()V

    .line 51
    .local v10, "plan":Lcom/google/android/apps/books/service/SyncPlan;
    iget-object v8, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->localVolumeData:Ljava/util/Map;

    .line 52
    .local v8, "localData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->downloadProgress:Ljava/util/Map;

    .line 54
    .local v0, "downloadProgress":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/VolumeData;

    .line 55
    .local v1, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v8, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 56
    .local v2, "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .local v3, "volumeDownloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    move-wide v4, p1

    move v6, p3

    .line 58
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/books/service/SyncPlan;->shouldDownload(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;JZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 60
    iget-object v4, v10, Lcom/google/android/apps/books/service/SyncPlan;->volumesToDownload:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    invoke-direct {v5, v1, v2, v3}, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;-><init>(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_1
    invoke-static {v2, p1, p2}, Lcom/google/android/apps/books/service/SyncPlan;->isNew(Lcom/google/android/apps/books/model/LocalVolumeData;J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    iget-object v4, v10, Lcom/google/android/apps/books/service/SyncPlan;->volumesAddedToLibrary:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    .end local v1    # "volume":Lcom/google/android/apps/books/model/VolumeData;
    .end local v2    # "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    .end local v3    # "volumeDownloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    :cond_2
    iget-object v4, v10, Lcom/google/android/apps/books/service/SyncPlan;->volumesToDownload:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    .line 70
    .local v9, "numDownloads":I
    if-lez v9, :cond_3

    .line 71
    const-string v4, "SyncPlan"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 72
    const-string v4, "SyncPlan"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Planning to download "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " volumes."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_3
    return-object v10
.end method

.method private static isNew(Lcom/google/android/apps/books/model/LocalVolumeData;J)Z
    .locals 5
    .param p0, "localVolumeData"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "lastSync"    # J

    .prologue
    .line 135
    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static shouldDownload(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;JZ)Z
    .locals 9
    .param p0, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p1, "localVolumeData"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p2, "volumeDownloadProgress"    # Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .param p3, "lastSync"    # J
    .param p5, "downloadContentSilently"    # Z

    .prologue
    const/4 v4, 0x6

    const/4 v6, 0x0

    const/4 v7, 0x3

    .line 82
    if-nez p2, :cond_0

    .line 83
    const-string v1, "SyncPlan"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const-string v1, "SyncPlan"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Skipping "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", missing download progress"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    if-nez p1, :cond_1

    .line 88
    const-string v1, "SyncPlan"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    const-string v1, "SyncPlan"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Skipping "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", missing localVolumeData"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getProgress(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isFullyDownloaded()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    const-string v1, "SyncPlan"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    const-string v1, "SyncPlan"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Skipping "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isFullyDownloaded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_2
    :goto_0
    return v6

    .line 99
    :cond_3
    const-string v1, "SyncPlan"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 100
    const-string v2, "SyncPlan"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "volume "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " downloaded: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v1, 0x0

    check-cast v1, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getProgress(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->getDownloadFraction()F

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_4
    invoke-interface {p1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 106
    const-string v1, "SyncPlan"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 107
    const-string v1, "SyncPlan"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "force download "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_5
    const/4 v6, 0x1

    goto :goto_0

    .line 111
    :cond_6
    if-nez p5, :cond_7

    .line 114
    const-string v1, "SyncPlan"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 115
    const-string v1, "SyncPlan"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "should download "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but device is on metered net"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    .line 125
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/books/model/VolumeDataUtils;->isVolumeOfNotableInterest(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;JZ)Z

    move-result v0

    .line 127
    .local v0, "result":Z
    const-string v1, "SyncPlan"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 128
    const-string v1, "SyncPlan"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Need to download "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move v6, v0

    .line 131
    goto/16 :goto_0
.end method
