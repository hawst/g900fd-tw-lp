.class public abstract Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelperClient;
.super Ljava/lang/Object;
.source "BooksBrowserAuthenticationHelperClient.java"

# interfaces
.implements Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract finished(Landroid/content/Intent;Ljava/lang/Exception;)V
.end method

.method public finished(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 29
    if-eqz p1, :cond_0

    .line 30
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 31
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 35
    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelperClient;->finished(Landroid/content/Intent;Ljava/lang/Exception;)V

    .line 36
    return-void

    .line 33
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method public getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelperClient;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 41
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-object v2

    .line 44
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/common/BooksContext;

    .line 46
    .local v1, "booksContext":Lcom/google/android/apps/books/common/BooksContext;
    if-eqz v1, :cond_0

    .line 50
    invoke-interface {v1}, Lcom/google/android/apps/books/common/BooksContext;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v2

    goto :goto_0
.end method
