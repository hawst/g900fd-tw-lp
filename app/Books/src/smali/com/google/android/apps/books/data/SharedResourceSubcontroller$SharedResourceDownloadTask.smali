.class Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;
.super Ljava/lang/Object;
.source "SharedResourceSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SharedResourceDownloadTask"
.end annotation


# instance fields
.field public final md5Saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field public final resSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field public final resource:Lcom/google/android/apps/books/model/Resource;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;)V
    .locals 0
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p2, "resSaver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p3, "md5Saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->resource:Lcom/google/android/apps/books/model/Resource;

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->resSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    .line 61
    iput-object p3, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->md5Saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    .line 62
    return-void
.end method
