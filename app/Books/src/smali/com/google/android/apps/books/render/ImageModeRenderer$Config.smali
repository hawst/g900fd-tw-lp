.class public Lcom/google/android/apps/books/render/ImageModeRenderer$Config;
.super Ljava/lang/Object;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Config"
.end annotation


# instance fields
.field private final mLandscape:Z

.field private final mMarginPercentSides:I

.field private final mMemoryClass:I

.field private final mPaintDebugWordBoxes:Z


# direct methods
.method public constructor <init>(ZIZI)V
    .locals 0
    .param p1, "isLandscape"    # Z
    .param p2, "memoryClass"    # I
    .param p3, "paintDebugWordBoxes"    # Z
    .param p4, "marginPercentSides"    # I

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mLandscape:Z

    .line 155
    iput p2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mMemoryClass:I

    .line 156
    iput-boolean p3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mPaintDebugWordBoxes:Z

    .line 157
    iput p4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mMarginPercentSides:I

    .line 158
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getLandscape()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getPaintDebugWordBoxes()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getSideMarginPercent()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getMemoryClass()I

    move-result v0

    return v0
.end method

.method public static fromContext(Landroid/content/Context;)Lcom/google/android/apps/books/render/ImageModeRenderer$Config;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    const/4 v5, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v6, :cond_0

    const/4 v1, 0x1

    .line 180
    .local v1, "isLandscape":Z
    :goto_0
    sget-object v5, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_DEBUG_WORD_BOXES:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v4

    .line 182
    .local v4, "paintDebugWordBoxes":Z
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 185
    .local v0, "am":Landroid/app/ActivityManager;
    if-eqz v0, :cond_1

    .line 186
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    .line 191
    .local v3, "memoryClass":I
    :goto_1
    invoke-static {p0}, Lcom/google/android/apps/books/util/ReaderUtils;->getMarginPercentSides(Landroid/content/Context;)I

    move-result v2

    .line 193
    .local v2, "marginPercentSides":I
    new-instance v5, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    invoke-direct {v5, v1, v3, v4, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;-><init>(ZIZI)V

    return-object v5

    .line 178
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "isLandscape":Z
    .end local v2    # "marginPercentSides":I
    .end local v3    # "memoryClass":I
    .end local v4    # "paintDebugWordBoxes":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 188
    .restart local v0    # "am":Landroid/app/ActivityManager;
    .restart local v1    # "isLandscape":Z
    .restart local v4    # "paintDebugWordBoxes":Z
    :cond_1
    const-string v5, "ImageModeRenderer"

    const-string v6, "Missing ActivityManager"

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/16 v3, 0x10

    .restart local v3    # "memoryClass":I
    goto :goto_1
.end method

.method private getLandscape()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mLandscape:Z

    return v0
.end method

.method private getMemoryClass()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mMemoryClass:I

    return v0
.end method

.method private getPaintDebugWordBoxes()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mPaintDebugWordBoxes:Z

    return v0
.end method

.method private getSideMarginPercent()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->mMarginPercentSides:I

    return v0
.end method
