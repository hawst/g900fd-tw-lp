.class public Lcom/google/android/apps/books/app/SortFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "SortFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;,
        Lcom/google/android/apps/books/app/SortFragment$Item;,
        Lcom/google/android/apps/books/app/SortFragment$Arguments;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 101
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/SortFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SortFragment;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SortFragment;->getHomeCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private addToAdapter(Landroid/content/Context;Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;Lcom/google/android/apps/books/app/LibraryComparator;Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;
    .param p3, "thisComparator"    # Lcom/google/android/apps/books/app/LibraryComparator;
    .param p4, "startingComparator"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 86
    invoke-virtual {p3}, Lcom/google/android/apps/books/app/LibraryComparator;->getLabelResourceId()I

    move-result v1

    if-ne p4, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lcom/google/android/apps/books/app/SortFragment$2;

    invoke-direct {v2, p0, p3}, Lcom/google/android/apps/books/app/SortFragment$2;-><init>(Lcom/google/android/apps/books/app/SortFragment;Lcom/google/android/apps/books/app/LibraryComparator;)V

    invoke-virtual {p2, v1, v0, v2}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;->add(IZLandroid/view/View$OnClickListener;)V

    .line 93
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHomeCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SortFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 140
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 141
    const/4 v1, 0x0

    .line 143
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SortFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 52
    .local v3, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;-><init>(Lcom/google/android/apps/books/app/SortFragment;Landroid/content/Context;)V

    .line 53
    .local v0, "adapter":Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SortFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    # invokes: Lcom/google/android/apps/books/app/SortFragment$Arguments;->getSortOrder(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/LibraryComparator;
    invoke-static {v6}, Lcom/google/android/apps/books/app/SortFragment$Arguments;->access$000(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v5

    .line 55
    .local v5, "startingSortOrder":Lcom/google/android/apps/books/app/LibraryComparator;
    sget-object v6, Lcom/google/android/apps/books/app/LibraryComparator;->BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

    invoke-direct {p0, v3, v0, v6, v5}, Lcom/google/android/apps/books/app/SortFragment;->addToAdapter(Landroid/content/Context;Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;Lcom/google/android/apps/books/app/LibraryComparator;Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 56
    sget-object v6, Lcom/google/android/apps/books/app/LibraryComparator;->BY_TITLE:Lcom/google/android/apps/books/app/LibraryComparator;

    invoke-direct {p0, v3, v0, v6, v5}, Lcom/google/android/apps/books/app/SortFragment;->addToAdapter(Landroid/content/Context;Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;Lcom/google/android/apps/books/app/LibraryComparator;Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 57
    sget-object v6, Lcom/google/android/apps/books/app/LibraryComparator;->BY_AUTHOR:Lcom/google/android/apps/books/app/LibraryComparator;

    invoke-direct {p0, v3, v0, v6, v5}, Lcom/google/android/apps/books/app/SortFragment;->addToAdapter(Landroid/content/Context;Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;Lcom/google/android/apps/books/app/LibraryComparator;Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 62
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0f01aa

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/books/app/SortFragment$1;

    invoke-direct {v7, p0, v0}, Lcom/google/android/apps/books/app/SortFragment$1;-><init>(Lcom/google/android/apps/books/app/SortFragment;Lcom/google/android/apps/books/app/SortFragment$ItemAdapter;)V

    invoke-virtual {v6, v0, v7}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 76
    .local v4, "dialog":Landroid/app/AlertDialog;
    new-array v1, v9, [I

    const v6, 0x1010031

    aput v6, v1, v8

    .line 77
    .local v1, "attributeIds":[I
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 78
    .local v2, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v4}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v6

    const/high16 v7, -0x1000000

    invoke-virtual {v2, v8, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 80
    return-object v4
.end method
