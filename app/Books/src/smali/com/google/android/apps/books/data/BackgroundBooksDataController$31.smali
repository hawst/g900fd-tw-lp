.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->uploadCollectionChanges(JLcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$collectionId:J

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$key:Lcom/google/android/apps/books/model/LocalSessionKey;

.field final synthetic val$ops:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1033
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$key:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$ops:Ljava/util/Collection;

    iput-wide p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$collectionId:J

    iput-object p8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$key:Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$ops:Ljava/util/Collection;

    iget-wide v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$collectionId:J

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    move-object v1, p1

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->uploadCollectionChanges(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V

    .line 1037
    return-void
.end method
