.class public abstract Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
.super Ljava/lang/Object;
.source "ReadAlongController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadAlongController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReadableItemsRequestData"
.end annotation


# instance fields
.field private final mPassageIndex:I

.field private final mPosition:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->mPassageIndex:I

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->mPosition:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getPassage()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->mPassageIndex:I

    return v0
.end method

.method public getPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->mPosition:Ljava/lang/String;

    return-object v0
.end method
