.class public Lcom/google/android/apps/books/app/BooksUserSurveys;
.super Ljava/lang/Object;
.source "BooksUserSurveys.java"


# direct methods
.method private static getDeviceId(Lcom/google/android/apps/books/preference/LocalPreferences;)I
    .locals 2
    .param p0, "prefs"    # Lcom/google/android/apps/books/preference/LocalPreferences;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/LocalPreferences;->hasHatsDeviceId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHatsDeviceId()I

    move-result v0

    .line 81
    :goto_0
    return v0

    .line 79
    :cond_0
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 80
    .local v0, "result":I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->setHatsDeviceId(I)V

    goto :goto_0
.end method

.method private static getSamplePeriod(JI)I
    .locals 6
    .param p0, "millisSinceEpoch"    # J
    .param p2, "daysPerSamplePeriod"    # I

    .prologue
    .line 70
    int-to-long v2, p2

    const-wide/32 v4, 0x5265c00

    mul-long v0, v2, v4

    .line 71
    .local v0, "samplePeriodInMillis":J
    div-long v2, p0, v0

    long-to-int v2, v2

    return v2
.end method

.method public static setDismissedSurvey(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->setDismissedHatsSurveyTimestamp(J)V

    .line 86
    return-void
.end method

.method public static final shouldOfferHatsSurvey(Landroid/content/Context;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 22
    sget-object v11, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v11, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_1

    move v10, v9

    .line 61
    :cond_0
    :goto_0
    return v10

    .line 26
    :cond_1
    sget-object v11, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v11, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 30
    invoke-static {p0}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getHatsSampleGroups(Landroid/content/Context;)I

    move-result v8

    .line 31
    .local v8, "sampleGroups":I
    invoke-static {p0}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getHatsSampleDays(Landroid/content/Context;)I

    move-result v2

    .line 33
    .local v2, "daysPerGroup":I
    new-instance v7, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 36
    .local v7, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13, v2}, Lcom/google/android/apps/books/app/BooksUserSurveys;->getSamplePeriod(JI)I

    move-result v1

    .line 39
    .local v1, "currentSamplePeriod":I
    invoke-virtual {v7}, Lcom/google/android/apps/books/preference/LocalPreferences;->hasDismissedHatsSurveyTimestamp()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 41
    invoke-virtual {v7}, Lcom/google/android/apps/books/preference/LocalPreferences;->getDismissedHatsSurveyTimestamp()J

    move-result-wide v12

    invoke-static {v12, v13, v2}, Lcom/google/android/apps/books/app/BooksUserSurveys;->getSamplePeriod(JI)I

    move-result v5

    .line 43
    .local v5, "dismissedSamplePeriod":I
    if-eq v5, v1, :cond_0

    .line 50
    .end local v5    # "dismissedSamplePeriod":I
    :cond_2
    :try_start_0
    invoke-static {v7}, Lcom/google/android/apps/books/app/BooksUserSurveys;->getDeviceId(Lcom/google/android/apps/books/preference/LocalPreferences;)I

    move-result v3

    .line 53
    .local v3, "deviceId":I
    rem-int v4, v3, v8

    .line 56
    .local v4, "deviceSampleGroup":I
    rem-int v0, v1, v8
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .local v0, "currentSampleGroup":I
    if-ne v4, v0, :cond_3

    :goto_1
    move v10, v9

    goto :goto_0

    :cond_3
    move v9, v10

    goto :goto_1

    .line 59
    .end local v0    # "currentSampleGroup":I
    .end local v3    # "deviceId":I
    .end local v4    # "deviceSampleGroup":I
    :catch_0
    move-exception v6

    .line 60
    .local v6, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v9, "BooksUserSurveys"

    const-string v11, "Error generating device ID"

    invoke-static {v9, v11, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
