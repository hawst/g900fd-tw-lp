.class Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$23$1;->take(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$23$1;)V
    .locals 0

    .prologue
    .line 4544
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 4547
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$23;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchSequenceNumber:I
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11200(Lcom/google/android/apps/books/app/ReaderFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$thisSearchSequenceNumber:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$23;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4549
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$23;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$newResults:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget-boolean v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$isDone:Z

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;->this$2:Lcom/google/android/apps/books/app/ReaderFragment$23$1;

    iget-object v3, v3, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$chapterHeading:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->addSearchResultBatch(Ljava/util/List;ZLjava/lang/String;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;ZLjava/lang/String;)V

    .line 4551
    :cond_0
    return-void
.end method
