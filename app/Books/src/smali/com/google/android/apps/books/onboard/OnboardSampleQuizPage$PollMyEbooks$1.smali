.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "resultsExceptionOr":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;"
    const/4 v10, 0x3

    .line 362
    const-string v6, "OnboardSampleQuizPage"

    invoke-static {v6, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 363
    const-string v6, "OnboardSampleQuizPage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Requesting samples reqId "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    iget-object v8, v8, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I
    invoke-static {v8}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_0
    const/4 v3, 0x0

    .line 366
    .local v3, "sawSelectedVolume":Z
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 367
    const-string v6, "OnboardSampleQuizPage"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 368
    const-string v6, "OnboardSampleQuizPage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unable to get volumes: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I
    invoke-static {v6}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1400(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)I

    move-result v6

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mScheduler:Lcom/google/android/ublib/utils/Scheduler;
    invoke-static {v6}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1500(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)Lcom/google/android/ublib/utils/Scheduler;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/ublib/utils/Scheduler;->getTime()J

    move-result-wide v6

    iget-object v8, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mExitByTime:J
    invoke-static {v8}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1600(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_7

    .line 389
    :cond_2
    const-string v6, "OnboardSampleQuizPage"

    invoke-static {v6, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 390
    const-string v6, "OnboardSampleQuizPage"

    const-string v7, "take: exiting"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mScheduler:Lcom/google/android/ublib/utils/Scheduler;
    invoke-static {v6}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1500(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)Lcom/google/android/ublib/utils/Scheduler;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1$1;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1$1;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;)V

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->SAMPLE_POLL_DELAY_MS:J
    invoke-static {}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1800()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, Lcom/google/android/ublib/utils/Scheduler;->schedule(Ljava/lang/Runnable;J)V

    .line 419
    :goto_1
    return-void

    .line 372
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    iget-object v1, v6, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    .line 374
    .local v1, "myEbooksVolumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 375
    .local v2, "numVolumes":I
    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    iget-object v6, v6, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # invokes: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getSelectedItemIds()Ljava/util/Set;
    invoke-static {v6}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1300(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;

    move-result-object v4

    .line 376
    .local v4, "selectedVolumeIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/model/VolumeData;

    .line 377
    .local v5, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 378
    const/4 v3, 0x1

    .line 382
    .end local v5    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_6
    const-string v6, "OnboardSampleQuizPage"

    invoke-static {v6, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 383
    const-string v6, "OnboardSampleQuizPage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "take: numVolumes="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", sawSelectedVolume="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 413
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "myEbooksVolumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    .end local v2    # "numVolumes":I
    .end local v4    # "selectedVolumeIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_7
    const-string v6, "OnboardSampleQuizPage"

    invoke-static {v6, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 414
    const-string v6, "OnboardSampleQuizPage"

    const-string v7, "take: rescheduling"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_8
    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # --operator for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I
    invoke-static {v6}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1406(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)I

    .line 417
    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mScheduler:Lcom/google/android/ublib/utils/Scheduler;
    invoke-static {v6}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1500(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)Lcom/google/android/ublib/utils/Scheduler;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->SAMPLE_POLL_DELAY_MS:J
    invoke-static {}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1800()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, Lcom/google/android/ublib/utils/Scheduler;->schedule(Ljava/lang/Runnable;J)V

    goto :goto_1
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 358
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
