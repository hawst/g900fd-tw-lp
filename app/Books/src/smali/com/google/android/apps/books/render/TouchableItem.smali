.class public Lcom/google/android/apps/books/render/TouchableItem;
.super Ljava/lang/Object;
.source "TouchableItem.java"


# instance fields
.field public final bounds:Landroid/graphics/Rect;

.field public final hasControls:Ljava/lang/Boolean;

.field public final isFootnote:I

.field public final linkText:Ljava/lang/String;

.field private mCachedPosition:Lcom/google/android/apps/books/common/Position;

.field private mCachedUri:Landroid/net/Uri;

.field public final originalId:Ljava/lang/String;

.field public final source:Ljava/lang/String;

.field public final type:I


# direct methods
.method public constructor <init>(ILandroid/graphics/Rect;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "bounds"    # Landroid/graphics/Rect;
    .param p3, "hasControls"    # Ljava/lang/Boolean;
    .param p4, "source"    # Ljava/lang/String;
    .param p5, "linkText"    # Ljava/lang/String;
    .param p6, "isFootnote"    # I
    .param p7, "originalId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedUri:Landroid/net/Uri;

    .line 54
    iput-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedPosition:Lcom/google/android/apps/books/common/Position;

    .line 58
    iput p1, p0, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    .line 60
    iput-object p3, p0, Lcom/google/android/apps/books/render/TouchableItem;->hasControls:Ljava/lang/Boolean;

    .line 61
    iput-object p4, p0, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    .line 62
    iput-object p5, p0, Lcom/google/android/apps/books/render/TouchableItem;->linkText:Ljava/lang/String;

    .line 63
    iput p6, p0, Lcom/google/android/apps/books/render/TouchableItem;->isFootnote:I

    .line 64
    iput-object p7, p0, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public static typeFromString(Ljava/lang/String;)I
    .locals 1
    .param p0, "typeString"    # Ljava/lang/String;

    .prologue
    .line 73
    const-string v0, "video"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    .line 75
    :cond_0
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    const/4 v0, 0x2

    goto :goto_0

    .line 77
    :cond_1
    const-string v0, "a"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    const/4 v0, 0x4

    goto :goto_0

    .line 79
    :cond_2
    const-string v0, "img"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    const/16 v0, 0x8

    goto :goto_0

    .line 82
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedPosition:Lcom/google/android/apps/books/common/Position;

    if-nez v0, :cond_0

    .line 121
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/TouchableItem;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/common/Position;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedPosition:Lcom/google/android/apps/books/common/Position;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedPosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/UriUtils;->normalizeScheme(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedUri:Landroid/net/Uri;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->mCachedUri:Landroid/net/Uri;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    const-string v1, "{%s%s: %d,%d,%d,%d}"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/TouchableItem;->typeAsString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->hasControls:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, " controls"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    iget-object v3, p0, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public typeAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    packed-switch v0, :pswitch_data_0

    .line 97
    :pswitch_0
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 89
    :pswitch_1
    const-string v0, "video"

    goto :goto_0

    .line 91
    :pswitch_2
    const-string v0, "audio"

    goto :goto_0

    .line 93
    :pswitch_3
    const-string v0, "a"

    goto :goto_0

    .line 95
    :pswitch_4
    const-string v0, "img"

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
