.class Lcom/google/android/apps/books/app/ReaderFragment$12;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->onAttach(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 1898
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$12;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResultSelected(Lcom/google/android/apps/books/annotations/TextLocation;)V
    .locals 4
    .param p1, "matchLocation"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$12;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SearchScrubBar;->hideSearchBarMatchText()V

    .line 1906
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$12;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    .line 1907
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$12;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 1908
    return-void
.end method

.method public onSearchWebSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1912
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$12;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/ReaderUtils;->searchWebSelectedText(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 1913
    return-void
.end method
