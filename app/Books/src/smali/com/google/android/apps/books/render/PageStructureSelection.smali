.class public Lcom/google/android/apps/books/render/PageStructureSelection;
.super Ljava/lang/Object;
.source "PageStructureSelection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/PageStructureSelection$1;,
        Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;,
        Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;
    }
.end annotation


# instance fields
.field private mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

.field private mHandleDragStartX:I

.field private mHandleDragStartY:I

.field private final mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

.field private mMoving:Lcom/google/android/apps/books/render/PageStructureHandle;

.field private mRequestId:I

.field private final mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTextRectWalker:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 66
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 68
    iput-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mMoving:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 71
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 72
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;-><init>(Lcom/google/android/apps/books/render/PageStructureSelection;Lcom/google/android/apps/books/render/PageStructureSelection$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;

    .line 73
    new-instance v0, Lcom/google/android/apps/books/render/LineRectWalker;

    new-instance v1, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;-><init>(Lcom/google/android/apps/books/render/PageStructureSelection;Lcom/google/android/apps/books/render/PageStructureSelection$1;)V

    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/render/LineRectWalker;-><init>(Lcom/google/android/apps/books/widget/Walker;FF)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTextRectWalker:Lcom/google/android/apps/books/widget/Walker;

    .line 81
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mRequestId:I

    .line 166
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTempRect:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/PageStructureSelection;)Lcom/google/android/apps/books/render/PageStructureHandle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/PageStructureSelection;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/PageStructureSelection;)Lcom/google/android/apps/books/render/PageStructureHandle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/PageStructureSelection;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    return-object v0
.end method

.method private getHandleRect(ZLandroid/graphics/Rect;)V
    .locals 1
    .param p1, "firstHandle"    # Z
    .param p2, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 204
    if-nez p1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    .line 208
    return-void
.end method


# virtual methods
.method public getAfterText(I)Ljava/lang/String;
    .locals 3
    .param p1, "numContextCharacters"    # I

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->increment()V

    .line 224
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->inPageBounds()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWord()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->increment()V

    goto :goto_0

    .line 229
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 230
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 232
    :goto_1
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getBeforeText(I)Ljava/lang/String;
    .locals 4
    .param p1, "numContextCharacters"    # I

    .prologue
    const/4 v3, 0x0

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->decrement()V

    .line 240
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->inPageBounds()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    const-string v1, " "

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWord()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->decrement()V

    goto :goto_0

    .line 245
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 246
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    :goto_1
    return-object v1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getRange(ILjava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .locals 3
    .param p1, "pageIndex"    # I
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 216
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureLocationRange;

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/books/render/PageStructureHandle;->toLocation(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/books/render/PageStructureHandle;->toLocation(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;-><init>(Lcom/google/android/apps/books/annotations/PageStructureLocation;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V

    return-object v0
.end method

.method public getRequestId()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mRequestId:I

    return v0
.end method

.method public getSelectedText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 148
    const/4 v1, 0x1

    .line 149
    .local v1, "firstWord":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->isAfter(Lcom/google/android/apps/books/render/PageStructureHandle;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 152
    if-nez v1, :cond_0

    .line 153
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_0
    const/4 v1, 0x0

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWord()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->increment()V

    goto :goto_0

    .line 159
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method handleRects()Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleRectWalker:Lcom/google/android/apps/books/widget/Walker;

    return-object v0
.end method

.method public initToClosestWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;FF)Z
    .locals 3
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/apps/books/render/PageStructureHandle;->setToClosestWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;FF)Z

    move-result v0

    .line 90
    .local v0, "found":Z
    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 93
    :cond_0
    return v0
.end method

.method public selectAll(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V
    .locals 1
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->setBeforeFirstWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->setAfterLastWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V

    .line 86
    return-void
.end method

.method public setTo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Lcom/google/android/apps/books/render/PageStructureLocationRange;)V
    .locals 2
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p2, "range"    # Lcom/google/android/apps/books/render/PageStructureLocationRange;

    .prologue
    .line 253
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->setTo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V

    .line 254
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->setTo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V

    .line 255
    return-void
.end method

.method public setToCharacterRange(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;II)V
    .locals 5
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 119
    if-ltz p2, :cond_2

    if-ge p2, p3, :cond_2

    .line 120
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->setBeforeFirstWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V

    .line 121
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->setAfterLastWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V

    .line 122
    const/4 v2, 0x0

    .line 123
    .local v2, "wordStart":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v4, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PageStructureHandle;->isAfter(Lcom/google/android/apps/books/render/PageStructureHandle;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 124
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWord()Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "currWord":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int v1, v2, v3

    .line 127
    .local v1, "wordEnd":I
    add-int/lit8 v3, v2, -0x1

    if-gt v3, p2, :cond_0

    if-ge p2, v1, :cond_0

    .line 128
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v4, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 130
    :cond_0
    add-int/lit8 v3, v1, 0x1

    if-gt p3, v3, :cond_1

    .line 131
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v4, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 134
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTemp:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->increment()V

    .line 136
    add-int/lit8 v2, v1, 0x1

    .line 137
    goto :goto_0

    .line 139
    .end local v0    # "currWord":Ljava/lang/String;
    .end local v1    # "wordEnd":I
    .end local v2    # "wordStart":I
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->setInvalid()V

    .line 140
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->setInvalid()V

    .line 142
    :cond_3
    return-void
.end method

.method textRects()Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTextRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTextRectWalker:Lcom/google/android/apps/books/widget/Walker;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(ZIIZ)Z
    .locals 4
    .param p1, "firstHandle"    # Z
    .param p2, "deltaX"    # I
    .param p3, "deltaY"    # I
    .param p4, "isDoneMoving"    # Z

    .prologue
    .line 175
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mMoving:Lcom/google/android/apps/books/render/PageStructureHandle;

    if-nez v2, :cond_0

    .line 176
    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mMoving:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 177
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/render/PageStructureSelection;->getHandleRect(ZLandroid/graphics/Rect;)V

    .line 179
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTempRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleDragStartX:I

    .line 180
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mTempRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleDragStartY:I

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mMoving:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 183
    .local v0, "moving":Lcom/google/android/apps/books/render/PageStructureHandle;
    if-eqz p4, :cond_1

    .line 184
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mMoving:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 186
    :cond_1
    iget v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleDragStartX:I

    add-int/2addr v2, p2

    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mHandleDragStartY:I

    add-int/2addr v3, p3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->update(II)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 187
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->isAfter(Lcom/google/android/apps/books/render/PageStructureHandle;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 189
    .local v1, "temp":Lcom/google/android/apps/books/render/PageStructureHandle;
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    iput-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 190
    iput-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 196
    .end local v1    # "temp":Lcom/google/android/apps/books/render/PageStructureHandle;
    :cond_2
    iget v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mRequestId:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mRequestId:I

    .line 197
    const/4 v2, 0x1

    .line 199
    :goto_1
    return v2

    .line 176
    .end local v0    # "moving":Lcom/google/android/apps/books/render/PageStructureHandle;
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;

    goto :goto_0

    .line 199
    .restart local v0    # "moving":Lcom/google/android/apps/books/render/PageStructureHandle;
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method
