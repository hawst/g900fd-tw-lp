.class public Lcom/google/android/apps/books/provider/database/BooksDatabase;
.super Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelper;
.source "BooksDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/database/BooksDatabase$UpgradeException;
    }
.end annotation


# static fields
.field private static final FACTORY:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

.field private static final sProdFrontends:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContentApiFrontend:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mFileManager:Lcom/google/android/apps/books/provider/database/ContentFileManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->FACTORY:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    .line 88
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->builder()Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "http://books.google.com/"

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "http://www.google.com/"

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    const-string v1, "https://encrypted.google.com/"

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet$Builder;->build()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->sProdFrontends:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/provider/database/ContentFileManager;[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileDeleter"    # Lcom/google/android/apps/books/provider/database/ContentFileManager;
    .param p3, "tableStorageSpecs"    # [Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    .param p4, "tableChangeListener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 131
    const-string v2, "books.db"

    sget-object v3, Lcom/google/android/apps/books/provider/database/BooksDatabase;->FACTORY:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    const/16 v4, 0xae

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDbOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V

    .line 132
    const-string v0, "Missing context"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    .line 133
    const-string v0, "Missing fileDeleter"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/database/ContentFileManager;

    iput-object v0, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mFileManager:Lcom/google/android/apps/books/provider/database/ContentFileManager;

    .line 135
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContentApiFrontend:Ljava/lang/String;

    .line 136
    return-void
.end method

.method private areCompatible(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "dbFrontend"    # Ljava/lang/String;
    .param p2, "frontend"    # Ljava/lang/String;

    .prologue
    .line 780
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->sProdFrontends:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->sProdFrontends:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private deleteOrphanedTempFiles()V
    .locals 5

    .prologue
    .line 720
    const-string v0, "temp"

    .line 722
    .local v0, "KIPLING_TEMP_FILES_DIR":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "temp"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    .line 723
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/books/util/StorageUtils;->getExternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    const-string v4, "temp"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 725
    :catch_0
    move-exception v1

    .line 726
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "BooksDatabase"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 727
    const-string v2, "BooksDatabase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error deleting legacy temp dir: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCollectionColumnToClass()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-static {}, Lcom/google/android/apps/books/provider/database/CollectionsTable;->getColumnToClass()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static getCollectionVolumeColumnToClass()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 164
    invoke-static {}, Lcom/google/android/apps/books/provider/database/CollectionVolumesTable;->getWritableColumnToClass()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static getStateColumnToClass()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Lcom/google/android/apps/books/provider/database/StatesTable;->getColumnToClass()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static getVolumeColumnToClass()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 148
    invoke-static {}, Lcom/google/android/apps/books/provider/database/VolumesTable;->getColumnToClass()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private recreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 742
    const-string v1, "BooksDatabase"

    const-string v2, "Re-creating database"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    new-instance v0, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    .line 750
    .local v0, "syncState":Lcom/google/android/apps/books/sync/SyncAccountsState;
    invoke-interface {v0}, Lcom/google/android/apps/books/sync/SyncAccountsState;->clear()V

    .line 752
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->wipeEntities(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 753
    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mFileManager:Lcom/google/android/apps/books/provider/database/ContentFileManager;

    invoke-interface {v1}, Lcom/google/android/apps/books/provider/database/ContentFileManager;->deleteAllFiles()V

    .line 755
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 756
    return-void
.end method

.method private recreateIfFrontendChanged(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 763
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getFrontend()Ljava/lang/String;

    move-result-object v0

    .line 764
    .local v0, "dbFrontend":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContentApiFrontend:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->areCompatible(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 765
    const-string v1, "BooksDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Frontend changed from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContentApiFrontend:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", recreating DB"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 768
    new-instance v1, Landroid/database/sqlite/SQLiteException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempt to change frontend from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContentApiFrontend:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but only have a read-only database connection."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 772
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 774
    :cond_1
    return-void
.end method

.method private recreateViews(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 223
    const-string v0, "type=\'view\' OR type=\'trigger\'"

    .line 224
    .local v0, "selection":Ljava/lang/String;
    const-string v1, "type=\'view\' OR type=\'trigger\'"

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->wipeEntities(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 226
    invoke-static {}, Lcom/google/android/apps/books/provider/database/VolumesTable;->getViewSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 227
    invoke-static {}, Lcom/google/android/apps/books/provider/database/CollectionVolumesTable;->getViewSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 228
    invoke-static {}, Lcom/google/android/apps/books/provider/database/SegmentsTable;->getViewSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 229
    invoke-static {}, Lcom/google/android/apps/books/provider/database/ResourcesTable;->getViewSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/google/android/apps/books/provider/database/PagesTable;->getViewSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method private wipeEntities(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 790
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v1

    const-string v0, "name"

    aput-object v0, v2, v3

    .line 791
    .local v2, "columns":[Ljava/lang/String;
    const-string v1, "sqlite_master"

    move-object v0, p1

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 793
    .local v8, "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 794
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 795
    .local v12, "type":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 796
    .local v10, "name":Ljava/lang/String;
    const-string v0, "sqlite_sequence"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 798
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DROP "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IF EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 799
    .local v11, "sql":Ljava/lang/String;
    const-string v0, "BooksDatabase"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 800
    const-string v0, "BooksDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Executing: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803
    :cond_1
    :try_start_1
    invoke-virtual {p1, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 804
    :catch_0
    move-exception v9

    .line 805
    .local v9, "e":Landroid/database/SQLException;
    :try_start_2
    const-string v0, "BooksDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "When executing "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 810
    .end local v9    # "e":Landroid/database/SQLException;
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "sql":Ljava/lang/String;
    .end local v12    # "type":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 812
    return-void
.end method


# virtual methods
.method public doOnCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 178
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContentApiFrontend:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->setFrontend(Ljava/lang/String;)V

    .line 182
    const-string v0, "REFERENCES volumes(volume_id)"

    .line 185
    .local v0, "refVolumeId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/books/provider/database/VolumesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/google/android/apps/books/provider/database/StatesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 187
    invoke-static {}, Lcom/google/android/apps/books/provider/database/CollectionsTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/google/android/apps/books/provider/database/CollectionVolumesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/google/android/apps/books/provider/database/ChaptersTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 190
    invoke-static {}, Lcom/google/android/apps/books/provider/database/SegmentsTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/google/android/apps/books/provider/database/ResourcesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 192
    invoke-static {}, Lcom/google/android/apps/books/provider/database/SegmentResourcesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 193
    invoke-static {}, Lcom/google/android/apps/books/provider/database/ResourceResourcesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 194
    invoke-static {}, Lcom/google/android/apps/books/provider/database/PagesTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 195
    invoke-static {}, Lcom/google/android/apps/books/provider/database/PagesTable;->getIndexSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 196
    invoke-static {}, Lcom/google/android/apps/books/provider/database/ConfigurationTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 201
    const-string v1, "CREATE TABLE session_keys (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL REFERENCES volumes(volume_id), session_key_version TEXT NOT NULL, root_key_version INTEGER NOT NULL, session_key_blob BLOB NOT NULL, FOREIGN KEY(account_name, volume_id) REFERENCES volumes(account_name, volume_id))"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 213
    invoke-static {}, Lcom/google/android/apps/books/provider/database/AccountsTable;->getCreationSql()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 215
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreateViews(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 216
    return-void
.end method

.method public doOnOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 734
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreateIfFrontendChanged(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 735
    return-void
.end method

.method public doOnUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 255
    if-ne p2, p3, :cond_1

    .line 716
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 259
    .local v6, "startTime":J
    const-string v5, "BooksDatabase"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Upgrading DB from version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    move v0, p2

    .line 264
    .local v0, "currentVersion":I
    const/16 v5, 0x4e

    if-ge p2, v5, :cond_5

    .line 265
    :try_start_0
    const-string v5, "BooksDatabase"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " too old to upgrade, recreating DB."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreate(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/apps/books/provider/database/BooksDatabase$UpgradeException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 686
    :catch_0
    move-exception v1

    .line 688
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v5, "BooksDatabase"

    const/4 v8, 0x6

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 689
    const-string v5, "BooksDatabase"

    const-string v8, "onUpgrade: SQLiteException, recreating db"

    invoke-static {v5, v8, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 691
    :cond_2
    const/4 v0, -0x1

    .line 704
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_1
    if-eq v0, p3, :cond_4

    .line 705
    const-string v5, "BooksDatabase"

    const/4 v8, 0x6

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 706
    const-string v5, "BooksDatabase"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Database upgrade from version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ended at version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", dropping existing data"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 712
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 713
    .local v2, "endTime":J
    const-string v5, "BooksDatabase"

    const/4 v8, 0x4

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 714
    const-string v5, "BooksDatabase"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Database upgrade took "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v10, v2, v6

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " millis"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 269
    .end local v2    # "endTime":J
    :cond_5
    const/16 v5, 0x4e

    if-ne v0, v5, :cond_6

    .line 272
    const/16 v0, 0x50

    .line 274
    :cond_6
    const/16 v5, 0x4f

    if-ne v0, v5, :cond_7

    .line 277
    :try_start_1
    const-string v5, "DROP VIEW IF EXISTS view_chapters"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 278
    const/16 v0, 0x50

    .line 280
    :cond_7
    const/16 v5, 0x50

    if-ne v0, v5, :cond_8

    .line 281
    const-string v5, "ALTER TABLE volume_states ADD COLUMN last_mode INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 283
    const/16 v0, 0x51

    .line 285
    :cond_8
    const/16 v5, 0x51

    if-ne v0, v5, :cond_9

    .line 290
    const-string v5, "UPDATE resources SET session_key_id = NULL"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 291
    const/16 v0, 0x52

    .line 293
    :cond_9
    const/16 v5, 0x52

    if-ne v0, v5, :cond_a

    .line 298
    const-string v5, "UPDATE volumes SET cover_content_status = 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 299
    const/16 v0, 0x53

    .line 301
    :cond_a
    const/16 v5, 0x53

    if-ne v0, v5, :cond_b

    .line 303
    const/16 v0, 0x54

    .line 305
    :cond_b
    const/16 v5, 0x54

    if-ne v0, v5, :cond_c

    .line 307
    const/16 v0, 0x68

    .line 309
    :cond_c
    const/16 v5, 0x68

    if-ne v0, v5, :cond_d

    .line 312
    const-string v5, "CREATE INDEX IF NOT EXISTS pages_chapter_index ON pages (volume_id, first_chapter_id)"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 314
    const/16 v0, 0x69

    .line 315
    const/16 v5, 0x69

    if-eq p3, v5, :cond_0

    .line 323
    :cond_d
    const/16 v5, 0x69

    if-ne v0, v5, :cond_e

    .line 324
    const-string v5, "ALTER TABLE volumes ADD COLUMN content_version TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 325
    const/16 v0, 0x6a

    .line 327
    :cond_e
    const/16 v5, 0x6a

    if-ne v0, v5, :cond_f

    .line 332
    const-string v5, "UPDATE sections SET style_css = NULL"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 333
    const/16 v0, 0x6b

    .line 335
    :cond_f
    const/16 v5, 0x6b

    if-ne v0, v5, :cond_10

    .line 342
    const-string v5, "UPDATE volume_states SET last_mode = -1"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 343
    const/16 v0, 0x6c

    .line 344
    const/16 v5, 0x6c

    if-eq p3, v5, :cond_0

    .line 352
    :cond_10
    const/16 v5, 0x6c

    if-ne v0, v5, :cond_11

    .line 354
    const/16 v0, 0x78

    .line 356
    :cond_11
    const/16 v5, 0x78

    if-ne v0, v5, :cond_12

    .line 361
    new-instance v5, Lcom/google/android/apps/books/provider/database/VolumeAccountUpgrader;

    iget-object v8, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mContext:Landroid/content/Context;

    invoke-direct {v5, v8, p1}, Lcom/google/android/apps/books/provider/database/VolumeAccountUpgrader;-><init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v5}, Lcom/google/android/apps/books/provider/database/VolumeAccountUpgrader;->upgrade()V

    .line 362
    const/16 v0, 0x7a

    .line 364
    :cond_12
    const/16 v5, 0x7a

    if-ne v0, v5, :cond_13

    .line 366
    const-string v5, "ALTER TABLE resources ADD COLUMN resource_type TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 369
    const-string v5, "UPDATE resources SET resource_type = \'image\'"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/google/android/apps/books/provider/database/SegmentResourcesTable;->getCreationSqlVersion123()Ljava/lang/String;

    move-result-object v4

    .line 373
    .local v4, "sqlCreate":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 375
    const/16 v0, 0x7b

    .line 376
    const/16 v5, 0x7b

    if-eq p3, v5, :cond_0

    .line 384
    .end local v4    # "sqlCreate":Ljava/lang/String;
    :cond_13
    const/16 v5, 0x7b

    if-ne v0, v5, :cond_14

    .line 385
    new-instance v5, Lcom/google/android/apps/books/provider/database/VolumesUrlRelativizer;

    invoke-direct {v5, p1}, Lcom/google/android/apps/books/provider/database/VolumesUrlRelativizer;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v5}, Lcom/google/android/apps/books/provider/database/VolumesUrlRelativizer;->execute()V

    .line 386
    const/16 v0, 0x7c

    .line 388
    :cond_14
    const/16 v5, 0x7c

    if-ne v0, v5, :cond_15

    .line 390
    const-string v5, "ALTER TABLE volume_states ADD COLUMN license_action TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 391
    const/16 v0, 0x7d

    .line 393
    :cond_15
    const/16 v5, 0x7d

    if-ne v0, v5, :cond_16

    .line 394
    const-string v5, "ALTER TABLE volume_states ADD COLUMN text_zoom REAL"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 396
    const/16 v0, 0x7e

    .line 397
    const/16 v5, 0x7e

    if-eq p3, v5, :cond_0

    .line 405
    :cond_16
    const/16 v5, 0x7e

    if-ne v0, v5, :cond_17

    .line 406
    const-string v5, "DROP INDEX IF EXISTS pages_chapter_index"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 407
    const-string v5, "CREATE INDEX pages_chapter_index ON pages (account_name, volume_id, first_chapter_id)"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 409
    const/16 v0, 0x7f

    .line 411
    :cond_17
    const/16 v5, 0x7f

    if-ne v0, v5, :cond_18

    .line 413
    const/16 v0, 0x80

    .line 415
    :cond_18
    const/16 v5, 0x80

    if-ne v0, v5, :cond_19

    .line 416
    iget-object v5, p0, Lcom/google/android/apps/books/provider/database/BooksDatabase;->mFileManager:Lcom/google/android/apps/books/provider/database/ContentFileManager;

    invoke-interface {v5}, Lcom/google/android/apps/books/provider/database/ContentFileManager;->migrateResourceFiles()V

    .line 417
    const/16 v0, 0x81

    .line 419
    :cond_19
    const/16 v5, 0x81

    if-ne v0, v5, :cond_1a

    .line 421
    invoke-static {}, Lcom/google/android/apps/books/provider/database/AccountsTable;->getCreationSqlVersion130()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 422
    const/16 v0, 0x82

    .line 424
    :cond_1a
    const/16 v5, 0x82

    if-ne v0, v5, :cond_1b

    .line 425
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ALTER TABLE volumes ADD COLUMN tts_permission TEXT DEFAULT \'"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v8, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->UNKNOWN:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\'"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 427
    const/16 v0, 0x83

    .line 429
    :cond_1b
    const/16 v5, 0x83

    if-ne v0, v5, :cond_1c

    .line 430
    const-string v5, "ALTER TABLE volumes ADD COLUMN canonical_url TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 431
    const/16 v0, 0x84

    .line 433
    :cond_1c
    const/16 v5, 0x84

    if-ne v0, v5, :cond_1d

    .line 434
    const-string v5, "ALTER TABLE segments ADD COLUMN fixed_layout_version INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 436
    const-string v5, "ALTER TABLE segments ADD COLUMN fixed_viewport_width INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 438
    const-string v5, "ALTER TABLE segments ADD COLUMN fixed_viewport_height INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 440
    const/16 v0, 0x85

    .line 442
    :cond_1d
    const/16 v5, 0x85

    if-ne v0, v5, :cond_1e

    .line 443
    invoke-static {}, Lcom/google/android/apps/books/provider/database/ResourceResourcesTable;->getCreationSqlVersion134()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 444
    const/16 v0, 0x86

    .line 447
    :cond_1e
    const/16 v5, 0x86

    if-ne v0, v5, :cond_1f

    .line 449
    const/16 v0, 0x87

    .line 452
    :cond_1f
    const/16 v5, 0x87

    if-ne v0, v5, :cond_20

    .line 453
    const-string v5, "ALTER TABLE volume_states ADD COLUMN force_download INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 455
    const/16 v0, 0x88

    .line 458
    :cond_20
    const/16 v5, 0x8a

    if-ge v0, v5, :cond_21

    .line 460
    const/16 v0, 0x8a

    .line 463
    :cond_21
    const/16 v5, 0x8a

    if-ne v0, v5, :cond_22

    .line 464
    const-string v5, "ALTER TABLE resources ADD COLUMN language TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 466
    const-string v5, "ALTER TABLE resources ADD COLUMN md5_hash TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 468
    const-string v5, "ALTER TABLE resources ADD COLUMN is_shared INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 470
    const-string v5, "ALTER TABLE resources ADD COLUMN is_default INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 472
    const-string v5, "ALTER TABLE resources ADD COLUMN overlay TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 474
    const/16 v0, 0x8b

    .line 477
    :cond_22
    const/16 v5, 0x96

    if-ge v0, v5, :cond_23

    .line 479
    const-string v5, "ALTER TABLE segment_resources ADD COLUMN css_class TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 480
    const-string v5, "ALTER TABLE segment_resources ADD COLUMN title TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 481
    const-string v5, "ALTER TABLE segment_resources ADD COLUMN resource_order INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 483
    const/16 v0, 0x96

    .line 486
    :cond_23
    const/16 v5, 0x97

    if-ge v0, v5, :cond_24

    .line 491
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ALTER TABLE volumes ADD COLUMN language TEXT DEFAULT \'"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\'"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 493
    const-string v5, "ALTER TABLE volumes ADD COLUMN is_right_to_left INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 495
    const/16 v0, 0x97

    .line 498
    :cond_24
    const/16 v5, 0x98

    if-ge v0, v5, :cond_25

    .line 500
    const-string v5, "ALTER TABLE volumes ADD COLUMN media_overlay_active_class TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 501
    const/16 v0, 0x98

    .line 504
    :cond_25
    const/16 v5, 0x9a

    if-ge v0, v5, :cond_26

    .line 507
    const-string v5, "ALTER TABLE volumes ADD COLUMN has_media_overlays INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 510
    const-string v5, "UPDATE volumes SET has_media_overlays = IFNULL((SELECT CAST(SUM(resources.resource_type=\'smil\') > 0 AS BOOLEAN) FROM resources WHERE resources.account_name = volumes.account_name AND resources.volume_id = volumes.volume_id), 0)"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 514
    const/16 v0, 0x9a

    .line 517
    :cond_26
    const/16 v5, 0x9b

    if-ge v0, v5, :cond_27

    .line 524
    const-string v5, "ALTER TABLE volume_states ADD COLUMN line_height REAL"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 526
    const/16 v0, 0x9b

    .line 533
    :cond_27
    const/16 v5, 0x9c

    if-ge v0, v5, :cond_28

    .line 541
    const-string v5, "ALTER TABLE volumes ADD COLUMN is_uploaded INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 544
    const/16 v0, 0x9c

    .line 547
    :cond_28
    const/16 v5, 0x9c

    if-ne v0, v5, :cond_29

    .line 552
    const/16 v0, 0x9d

    .line 555
    :cond_29
    const/16 v5, 0x9d

    if-ne v0, v5, :cond_2a

    .line 556
    const-string v5, "ALTER TABLE pages ADD COLUMN structure_status INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 558
    const/16 v0, 0x9e

    .line 561
    :cond_2a
    const/16 v5, 0x9e

    if-ne v0, v5, :cond_2b

    .line 562
    const-string v5, "ALTER TABLE volumes ADD COLUMN rental_expiration INTEGER"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 564
    const/16 v0, 0x9f

    .line 567
    :cond_2b
    const/16 v5, 0x9f

    if-ne v0, v5, :cond_2c

    .line 572
    const-string v5, "DROP INDEX IF EXISTS pages_chapter_index"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 573
    invoke-static {}, Lcom/google/android/apps/books/provider/database/PagesTable;->getIndexSql()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 574
    const/16 v0, 0xa0

    .line 577
    :cond_2c
    const/16 v5, 0xa0

    if-ne v0, v5, :cond_2d

    .line 579
    const/16 v0, 0xa1

    .line 582
    :cond_2d
    const/16 v5, 0xa1

    if-ne v0, v5, :cond_2e

    .line 584
    const/16 v0, 0xa2

    .line 587
    :cond_2e
    const/16 v5, 0xa2

    if-ne v0, v5, :cond_2f

    .line 588
    const-string v5, "ALTER TABLE volumes ADD COLUMN rental_start INTEGER"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 590
    const-string v5, "ALTER TABLE volumes ADD COLUMN rental_state TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 592
    const/16 v0, 0xa3

    .line 595
    :cond_2f
    const/16 v5, 0xa3

    if-ne v0, v5, :cond_30

    .line 597
    const/16 v0, 0xa4

    .line 600
    :cond_30
    const/16 v5, 0xa4

    if-ne v0, v5, :cond_31

    .line 601
    const-string v5, "ALTER TABLE volumes ADD COLUMN explicit_offline_license INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 604
    const-string v5, "ALTER TABLE volumes ADD COLUMN max_offline_devices INTEGER"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 606
    const-string v5, "ALTER TABLE volume_states ADD COLUMN has_offline_license INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 608
    const/16 v0, 0xa5

    .line 611
    :cond_31
    const/16 v5, 0xa5

    if-ne v0, v5, :cond_32

    .line 612
    const-string v5, "ALTER TABLE volumes ADD COLUMN image_mode_first_book_body_page TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 614
    const-string v5, "ALTER TABLE volumes ADD COLUMN image_mode_last_book_body_page TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 616
    const-string v5, "ALTER TABLE volumes ADD COLUMN text_mode_first_book_body_page TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 618
    const-string v5, "ALTER TABLE volumes ADD COLUMN text_mode_last_book_body_page TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 620
    const/16 v0, 0xa6

    .line 623
    :cond_32
    const/16 v5, 0xa6

    if-ne v0, v5, :cond_33

    .line 624
    const-string v5, "ALTER TABLE segments ADD COLUMN storage_format INTEGER"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 626
    const-string v5, "ALTER TABLE resources ADD COLUMN storage_format INTEGER"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 628
    const-string v5, "ALTER TABLE pages ADD COLUMN storage_format INTEGER"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 630
    const/16 v0, 0xa7

    .line 633
    :cond_33
    const/16 v5, 0xa7

    if-ne v0, v5, :cond_34

    .line 634
    const-string v5, "ALTER TABLE volumes ADD COLUMN orientation TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 636
    const-string v5, "ALTER TABLE volumes ADD COLUMN spread TEXT"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 638
    const/16 v0, 0xa8

    .line 641
    :cond_34
    const/16 v5, 0xa8

    if-ne v0, v5, :cond_35

    .line 649
    const/16 v0, 0xa9

    .line 652
    :cond_35
    const/16 v5, 0xa9

    if-ne v0, v5, :cond_36

    .line 653
    const-string v5, "ALTER TABLE volumes ADD COLUMN quote_sharing_allowed INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 656
    const/16 v0, 0xaa

    .line 659
    :cond_36
    const/16 v5, 0xaa

    if-ne v0, v5, :cond_37

    .line 661
    const-string v5, "ALTER TABLE volume_states ADD COLUMN fit_width INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 663
    const/16 v0, 0xab

    .line 666
    :cond_37
    const/16 v5, 0xab

    if-ne v0, v5, :cond_38

    .line 667
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->deleteOrphanedTempFiles()V

    .line 668
    const/16 v0, 0xac

    .line 671
    :cond_38
    const/16 v5, 0xac

    if-ne v0, v5, :cond_39

    .line 673
    const/16 v0, 0xad

    .line 676
    :cond_39
    const/16 v5, 0xad

    if-ne v0, v5, :cond_3a

    .line 677
    const-string v5, "ALTER TABLE chapters ADD COLUMN depth INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 679
    const-string v5, "ALTER TABLE chapters ADD COLUMN reading_position TEXT;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 681
    const/16 v0, 0xae

    .line 685
    :cond_3a
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreateViews(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/apps/books/provider/database/BooksDatabase$UpgradeException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_1

    .line 692
    :catch_1
    move-exception v1

    .line 693
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "BooksDatabase"

    const/4 v8, 0x6

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 694
    const-string v5, "BooksDatabase"

    const-string v8, "onUpgrade: IOException, recreating db"

    invoke-static {v5, v8, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 696
    :cond_3b
    const/4 v0, -0x1

    .line 702
    goto/16 :goto_1

    .line 697
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 698
    .local v1, "e":Lcom/google/android/apps/books/provider/database/BooksDatabase$UpgradeException;
    const-string v5, "BooksDatabase"

    const/4 v8, 0x6

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 699
    const-string v5, "BooksDatabase"

    const-string v8, "onUpgrade: UpgradeException, recreating db"

    invoke-static {v5, v8, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 701
    :cond_3c
    const/4 v0, -0x1

    goto/16 :goto_1
.end method

.method public notifyWatchedTableChanged(Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)Z
    .locals 1
    .param p1, "tableSpec"    # Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .prologue
    .line 815
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->notifyTableChanged(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    move-result v0

    return v0
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 242
    const-string v0, "BooksDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wiping data to downgrade DB from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->recreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 244
    return-void
.end method
