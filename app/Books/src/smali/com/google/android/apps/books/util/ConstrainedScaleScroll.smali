.class public Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
.super Ljava/lang/Object;
.source "ConstrainedScaleScroll.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;,
        Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;
    }
.end annotation


# instance fields
.field private final mContainerSize:Landroid/graphics/Point;

.field private final mContentRect:Landroid/graphics/Rect;

.field private mMaxScrollX:F

.field private mMaxScrollY:F

.field private mMinScrollX:F

.field private mMinScrollY:F

.field private mScale:F

.field private final mScroll:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    .line 32
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    .line 34
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    .line 39
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    .line 42
    iput v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollX:F

    .line 43
    iput v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollY:F

    .line 44
    iput v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollX:F

    .line 45
    iput v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollY:F

    .line 280
    return-void
.end method

.method private clampScroll()V
    .locals 4

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollX:F

    iget v3, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollX:F

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollY:F

    iget v3, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollY:F

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 256
    return-void
.end method

.method private updateConstraints()V
    .locals 9

    .prologue
    const v8, -0x457ced91    # -0.001f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 196
    iget-object v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 197
    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollX:F

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollX:F

    .line 248
    :goto_0
    return-void

    .line 201
    :cond_0
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float v1, v5, v6

    .line 202
    .local v1, "overflowX":F
    cmpl-float v5, v1, v8

    if-ltz v5, :cond_1

    .line 205
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollX:F

    .line 206
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollX:F

    .line 225
    :goto_1
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float v2, v5, v6

    .line 226
    .local v2, "overflowY":F
    cmpl-float v5, v2, v8

    if-ltz v5, :cond_2

    .line 229
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollY:F

    .line 230
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollY:F

    goto :goto_0

    .line 210
    .end local v2    # "overflowY":F
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 214
    .local v4, "scaleFitW":F
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    sub-float/2addr v5, v7

    sub-float v6, v4, v7

    div-float v0, v5, v6

    .line 222
    .local v0, "lambda":F
    iget-object v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    sub-float/2addr v6, v7

    add-float/2addr v6, v0

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollX:F

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollX:F

    goto :goto_1

    .line 234
    .end local v0    # "lambda":F
    .end local v4    # "scaleFitW":F
    .restart local v2    # "overflowY":F
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 238
    .local v3, "scaleFitH":F
    iget v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    sub-float/2addr v5, v7

    sub-float v6, v3, v7

    div-float v0, v5, v6

    .line 246
    .restart local v0    # "lambda":F
    iget-object v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    sub-float/2addr v6, v7

    add-float/2addr v6, v0

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollY:F

    iput v5, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollY:F

    goto/16 :goto_0
.end method


# virtual methods
.method public animateTo(FFFLcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;)V
    .locals 6
    .param p1, "newScale"    # F
    .param p2, "zoomFocusX"    # F
    .param p3, "zoomFocusY"    # F
    .param p4, "callback"    # Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;-><init>(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;FFFLcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll$ScaleAnimatorHelper;->start()V

    .line 189
    return-void
.end method

.method public getContentBottom()F
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    return v0
.end method

.method public getContentLeft()F
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    return v0
.end method

.method public getContentRight()F
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    return v0
.end method

.method public getContentTop()F
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    return v0
.end method

.method public getMaxScrollX()F
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollX:F

    return v0
.end method

.method public getMaxScrollY()F
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMaxScrollY:F

    return v0
.end method

.method public getMinScrollX()F
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollX:F

    return v0
.end method

.method public getMinScrollY()F
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mMinScrollY:F

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    return v0
.end method

.method public getScrollX()F
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    return v0
.end method

.method public getScrollY()F
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    return v0
.end method

.method public setContainerSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContainerSize:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->updateConstraints()V

    .line 56
    return-void
.end method

.method public setContentRect(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->updateConstraints()V

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->clampScroll()V

    .line 67
    return-void
.end method

.method public setScale(FFFZ)V
    .locals 4
    .param p1, "newScale"    # F
    .param p2, "focusX"    # F
    .param p3, "focusY"    # F
    .param p4, "clampScroll"    # Z

    .prologue
    .line 168
    iget-object v2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, p2

    iget v3, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    div-float v0, v2, v3

    .line 169
    .local v0, "srcFocusX":F
    iget-object v2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, p3

    iget v3, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    div-float v1, v2, v3

    .line 172
    .local v1, "srcFocusY":F
    iget-object v2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    mul-float v3, p1, v0

    sub-float/2addr v3, p2

    iput v3, v2, Landroid/graphics/PointF;->x:F

    .line 173
    iget-object v2, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    mul-float v3, p1, v1

    sub-float/2addr v3, p3

    iput v3, v2, Landroid/graphics/PointF;->y:F

    .line 174
    iput p1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->updateConstraints()V

    .line 178
    if-eqz p4, :cond_0

    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->clampScroll()V

    .line 181
    :cond_0
    return-void
.end method

.method public setScale(FZ)V
    .locals 0
    .param p1, "newScale"    # F
    .param p2, "clampScroll"    # Z

    .prologue
    .line 153
    iput p1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->updateConstraints()V

    .line 155
    if-eqz p2, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->clampScroll()V

    .line 158
    :cond_0
    return-void
.end method

.method public setScroll(FF)V
    .locals 1
    .param p1, "scrollX"    # F
    .param p2, "scrollY"    # F

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 138
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->clampScroll()V

    .line 139
    return-void
.end method

.method public transformX(F)F
    .locals 2
    .param p1, "x"    # F

    .prologue
    .line 126
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    mul-float/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public transformY(F)F
    .locals 2
    .param p1, "y"    # F

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScale:F

    mul-float/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public updateScroll(FFZ)V
    .locals 2
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F
    .param p3, "clampScroll"    # Z

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->mScroll:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, p2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 146
    if-eqz p3, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->clampScroll()V

    .line 149
    :cond_0
    return-void
.end method
