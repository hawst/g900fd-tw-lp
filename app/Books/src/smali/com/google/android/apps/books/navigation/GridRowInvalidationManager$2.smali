.class Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;
.super Ljava/lang/Object;
.source "GridRowInvalidationManager.java"

# interfaces
.implements Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Lcom/google/android/ublib/widget/AbsWarpListView;III)V
    .locals 0
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 68
    return-void
.end method

.method public onScrollStateChanged(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 2
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 47
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mScrollingViews:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$000(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$200(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2$1;-><init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;Lcom/google/android/ublib/widget/AbsWarpListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 59
    # invokes: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->getTaskManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;
    invoke-static {}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$300()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->onBusyEnded()V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$2;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mScrollingViews:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$000(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
