.class public interface abstract Lcom/google/android/apps/books/render/ReaderController;
.super Ljava/lang/Object;
.source "ReaderController.java"


# virtual methods
.method public abstract activateMediaElement(IILjava/lang/String;)I
.end method

.method public abstract applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
.end method

.method public abstract clearPendingTasks()V
.end method

.method public abstract initializeJavascript(Lcom/google/android/apps/books/util/JsConfiguration;)V
.end method

.method public abstract loadPage(IILjava/lang/Integer;)I
.end method

.method public abstract loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)I
.end method

.method public abstract loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIII)I
.end method

.method public abstract loadRangeDataBulk(ILjava/util/Map;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract loadSpreadPage(ILjava/lang/String;II)I
.end method

.method public abstract loadSpreadPageFromEob(II)I
.end method

.method public abstract maybePurgePassages(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract purgeRequests(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract requestPreloadPassage(I)V
.end method

.method public abstract requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
.end method

.method public abstract setOverlayActiveClass(Ljava/lang/String;)V
.end method

.method public abstract transitionToReaderReady()V
.end method
