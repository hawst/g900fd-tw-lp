.class public Lcom/google/android/apps/books/app/BooksEventLogger;
.super Ljava/lang/Object;
.source "BooksEventLogger.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/books/app/BooksEventLogger;


# instance fields
.field private final mEventLogger:Lcom/google/android/play/analytics/EventLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/books/app/BooksEventLogger;->sInstance:Lcom/google/android/apps/books/app/BooksEventLogger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 16
    .param p1, "applicationContext"    # Landroid/content/Context;
    .param p2, "debugUpload"    # Z

    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v4, 0x0

    .line 54
    .local v4, "loggingId":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/play/utils/PlayUtils;->getDefaultUserAgentString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 55
    .local v7, "ua":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/books/app/BooksEventLogger;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 56
    .local v10, "version":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/books/app/BooksEventLogger;->getMccMnc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 57
    .local v11, "mccmnc":Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 58
    .local v14, "androidId":J
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/BooksEventLogger;->getConfiguration(Z)Lcom/google/android/play/analytics/EventLogger$Configuration;

    move-result-object v12

    .line 60
    .local v12, "configuration":Lcom/google/android/play/analytics/EventLogger$Configuration;
    new-instance v2, Lcom/google/android/play/analytics/EventLogger;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/play/analytics/EventLogger$LogSource;->BOOKS:Lcom/google/android/play/analytics/EventLogger$LogSource;

    const-wide/16 v8, 0x0

    const/4 v13, 0x0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v13}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/BooksEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    .line 63
    return-void
.end method

.method private getConfiguration(Z)Lcom/google/android/play/analytics/EventLogger$Configuration;
    .locals 4
    .param p1, "debugUpload"    # Z

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {v0}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>()V

    .line 84
    .local v0, "configuration":Lcom/google/android/play/analytics/EventLogger$Configuration;
    if-eqz p1, :cond_0

    .line 85
    const-wide/16 v2, 0x3e8

    iput-wide v2, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    .line 86
    const-wide/16 v2, 0x1

    iput-wide v2, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    .line 89
    :cond_0
    const-string v1, "https://play.googleapis.com/play/log"

    iput-object v1, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->mServerUrl:Ljava/lang/String;

    .line 90
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Z)Lcom/google/android/apps/books/app/BooksEventLogger;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "debugUpload"    # Z

    .prologue
    .line 34
    const-class v2, Lcom/google/android/apps/books/app/BooksEventLogger;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/apps/books/app/BooksEventLogger;->sInstance:Lcom/google/android/apps/books/app/BooksEventLogger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 37
    :try_start_1
    new-instance v1, Lcom/google/android/apps/books/app/BooksEventLogger;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, p1}, Lcom/google/android/apps/books/app/BooksEventLogger;-><init>(Landroid/content/Context;Z)V

    sput-object v1, Lcom/google/android/apps/books/app/BooksEventLogger;->sInstance:Lcom/google/android/apps/books/app/BooksEventLogger;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 47
    :cond_0
    :try_start_2
    sget-object v1, Lcom/google/android/apps/books/app/BooksEventLogger;->sInstance:Lcom/google/android/apps/books/app/BooksEventLogger;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit v2

    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 41
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_3
    const-string v1, "BooksEventLog"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    const-string v1, "BooksEventLog"

    const-string v3, "Exception creating BooksEventLogger"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 44
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 34
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static getMccMnc(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 66
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 68
    .local v0, "tel":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getVersionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 73
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 75
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v2

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "BooksEventLog"

    const-string v3, "Exception getting version name"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public trackEvent(Ljava/lang/String;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "playlogBooksEvent"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[B[Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public varargs trackEvent(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "extras"    # [Ljava/lang/String;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksEventLogger;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[Ljava/lang/String;)V

    .line 105
    return-void
.end method
