.class public abstract Lcom/google/android/apps/books/widget/BaseBooksHomeView;
.super Ljava/lang/Object;
.source "BaseBooksHomeView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/BooksHomeView;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mIsDeviceConnected:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isDeviceConnected"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BaseBooksHomeView;->mContext:Landroid/content/Context;

    .line 18
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/BaseBooksHomeView;->mIsDeviceConnected:Z

    .line 19
    return-void
.end method


# virtual methods
.method protected isDeviceConnected()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BaseBooksHomeView;->mIsDeviceConnected:Z

    return v0
.end method

.method public onDeviceConnectionChanged(Z)V
    .locals 0
    .param p1, "connected"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BaseBooksHomeView;->mIsDeviceConnected:Z

    .line 24
    return-void
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 40
    return-void
.end method

.method public setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fraction"    # F

    .prologue
    .line 32
    return-void
.end method

.method public volumeDownloadFractionAnimationDone(Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 36
    return-void
.end method
