.class public Lcom/google/android/apps/books/provider/BooksProvider;
.super Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;
.source "BooksProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/BooksProvider$PublicProvider;
    }
.end annotation


# static fields
.field private static final mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/pool/Pool",
            "<",
            "Lcom/google/android/apps/books/util/SelectionBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private static sIsIcingEnabled:Z


# instance fields
.field private mAccountsProv:Lcom/google/android/apps/books/provider/AccountsProvidelet;

.field private mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

.field private mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

.field private mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

.field private mLocalCollectionsProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

.field private mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

.field private mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

.field private mManifestProv:Lcom/google/android/apps/books/provider/ManifestProvidelet;

.field private mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

.field private final mNotifyWatchedTableRunnable:Ljava/lang/Runnable;

.field private mSessionKeysProv:Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

.field private mUtilityHandler:Landroid/os/Handler;

.field private mVolumeContentProv:Lcom/google/android/apps/books/provider/VolumeContentProvidelet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lcom/google/android/apps/books/provider/BooksProvider$2;

    invoke-direct {v0}, Lcom/google/android/apps/books/provider/BooksProvider$2;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/books/util/pool/Pools;->simplePool(Lcom/google/android/apps/books/util/pool/PoolableManager;)Lcom/google/android/apps/books/util/pool/Pool;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/pool/Pools;->synchronizedPool(Lcom/google/android/apps/books/util/pool/Pool;)Lcom/google/android/apps/books/util/pool/Pool;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksProvider;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    .line 219
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/books/provider/BooksProvider;->sIsIcingEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;-><init>()V

    .line 141
    new-instance v0, Lcom/google/android/apps/books/provider/BooksProvider$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/provider/BooksProvider$1;-><init>(Lcom/google/android/apps/books/provider/BooksProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifyWatchedTableRunnable:Ljava/lang/Runnable;

    .line 811
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/provider/BooksProvider;)Lcom/google/android/apps/books/provider/database/BooksDatabase;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/BooksProvider;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/provider/BooksProvider;)Lcom/google/android/apps/books/provider/ConfigurationProvidelet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/BooksProvider;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    return-object v0
.end method

.method private clearAccountContent(Landroid/net/Uri;)I
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x0

    .line 770
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract$AccountContent;->getAccount(Landroid/net/Uri;)Landroid/accounts/Account;

    move-result-object v0

    .line 772
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v7}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 773
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 775
    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    .line 778
    .local v6, "whereArgs":[Ljava/lang/String;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 779
    .local v4, "deleteKeyValues":Landroid/content/ContentValues;
    const-string v7, "root_key_version"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 780
    const-string v7, "session_key_version"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 781
    const-string v7, "session_key_blob"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 782
    const-string v7, "accounts"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v4, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 785
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getValuesToClearManifestContentColumns()Landroid/content/ContentValues;

    move-result-object v2

    .line 786
    .local v2, "clearManifestValues":Landroid/content/ContentValues;
    const-string v7, "volumes"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v2, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 790
    const-string v7, "chapters"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 791
    const-string v7, "pages"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 792
    const-string v7, "segments"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 793
    const-string v7, "resources"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 794
    const-string v7, "segment_resources"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 795
    const-string v7, "resource_resources"

    const-string v8, "account_name=?"

    invoke-virtual {v3, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 797
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 798
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 801
    iget-object v7, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    invoke-virtual {v7}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;->getBaseDir()Ljava/io/File;

    move-result-object v1

    .line 802
    .local v1, "baseDir":Ljava/io/File;
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v7}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumesDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 803
    .local v5, "volumesDir":Ljava/io/File;
    invoke-static {v5}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    .line 805
    return v9
.end method

.method private dispatchQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 414
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v1

    .line 415
    .local v1, "match":I
    sparse-switch v1, :sswitch_data_0

    .line 465
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 462
    :goto_0
    return-object v0

    .line 424
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 438
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mManifestProv:Lcom/google/android/apps/books/provider/ManifestProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 442
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mSessionKeysProv:Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/SessionKeysProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 447
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionsProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :sswitch_5
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 450
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/provider/BooksProvider;->queryCollectionVolumes(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 454
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 458
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 462
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mAccountsProv:Lcom/google/android/apps/books/provider/AccountsProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/AccountsProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 415
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x6e -> :sswitch_1
        0x78 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xcd -> :sswitch_2
        0xce -> :sswitch_2
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x145 -> :sswitch_2
        0x146 -> :sswitch_2
        0x15e -> :sswitch_2
        0x15f -> :sswitch_2
        0x190 -> :sswitch_2
        0x191 -> :sswitch_2
        0x1c2 -> :sswitch_3
        0x1c3 -> :sswitch_3
        0x258 -> :sswitch_0
        0x259 -> :sswitch_0
        0x25a -> :sswitch_0
        0x2bc -> :sswitch_4
        0x2bd -> :sswitch_4
        0x2be -> :sswitch_4
        0x2c6 -> :sswitch_6
        0x2c7 -> :sswitch_5
        0x2c8 -> :sswitch_6
        0x320 -> :sswitch_7
        0x3e8 -> :sswitch_8
        0x3e9 -> :sswitch_8
    .end sparse-switch
.end method

.method public static externalStorageIsAvailable(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 694
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/books/util/StorageUtils;->getExternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 698
    .local v1, "result":Z
    :goto_0
    return v1

    .line 695
    .end local v1    # "result":Z
    :catch_0
    move-exception v0

    .line 696
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    .restart local v1    # "result":Z
    goto :goto_0
.end method

.method public static fromPublicUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 766
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.apps.books"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getAllAccountNames(Landroid/content/Context;)Ljava/util/Set;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 650
    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$Collections;->CONTENT_URI:Landroid/net/Uri;

    .line 651
    .local v1, "uri":Landroid/net/Uri;
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "DISTINCT(account_name)"

    aput-object v4, v2, v5

    .line 654
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .local v0, "resolver":Landroid/content/ContentResolver;
    move-object v4, v3

    move-object v5, v3

    .line 655
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 656
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 658
    .local v8, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 659
    const/4 v3, 0x0

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 660
    .local v7, "name":Ljava/lang/String;
    invoke-interface {v8, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 663
    .end local v7    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 665
    return-object v8
.end method

.method public static getFileStorageDirectory(Landroid/content/ContentResolver;)Ljava/io/File;
    .locals 9
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 706
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "value"

    aput-object v0, v2, v1

    .line 707
    .local v2, "columns":[Ljava/lang/String;
    const-string v8, "key=?"

    .line 708
    .local v8, "where":Ljava/lang/String;
    new-array v4, v3, [Ljava/lang/String;

    const-string v0, "base_path"

    aput-object v0, v4, v1

    .line 709
    .local v4, "whereArgs":[Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$Configuration;->CONFIGURATION_URI:Landroid/net/Uri;

    const-string v3, "key=?"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 715
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 716
    new-instance v7, Ljava/io/File;

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 718
    .local v7, "storageDir":Ljava/io/File;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 720
    return-object v7

    .line 718
    .end local v7    # "storageDir":Ljava/io/File;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private notifyVolumesTableChanged()V
    .locals 4

    .prologue
    .line 594
    sget-boolean v0, Lcom/google/android/apps/books/provider/BooksProvider;->sIsIcingEnabled:Z

    if-nez v0, :cond_0

    .line 610
    :goto_0
    return-void

    .line 597
    :cond_0
    const-string v0, "BooksProvider"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    const-string v0, "BooksProvider"

    const-string v1, "notifyVolumesTableChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    :cond_1
    monitor-enter p0

    .line 603
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mUtilityHandler:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 604
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getUtilityHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mUtilityHandler:Landroid/os/Handler;

    .line 607
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mUtilityHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifyWatchedTableRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mUtilityHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifyWatchedTableRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 609
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private queryCollectionVolumes(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 370
    const/16 v0, 0x2c7

    if-eq p1, v0, :cond_0

    .line 371
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static resetFileStorageLocation(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 744
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;->resetBasePath(Landroid/content/ContentResolver;)V

    .line 745
    return-void
.end method

.method public static declared-synchronized setShouldRegisterCorpora(Z)V
    .locals 2
    .param p0, "shouldRegisterCorpora"    # Z

    .prologue
    .line 231
    const-class v0, Lcom/google/android/apps/books/provider/BooksProvider;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lcom/google/android/apps/books/provider/BooksProvider;->sIsIcingEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    monitor-exit v0

    return-void

    .line 231
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static toPublicUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 759
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.apps.books.public"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 674
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v2}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 675
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 676
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    invoke-virtual {v2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->beginBatch()V

    .line 678
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v1

    .line 679
    .local v1, "results":[Landroid/content/ContentProviderResult;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 680
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    invoke-virtual {v2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->setBatchSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 684
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    invoke-virtual {v2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->endBatch()V

    return-object v1

    .line 683
    .end local v1    # "results":[Landroid/content/ContentProviderResult;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 684
    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    invoke-virtual {v3}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->endBatch()V

    throw v2
.end method

.method protected createDataManager(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/apps/books/provider/database/BooksDatabase;
    .locals 4
    .param p1, "listener"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 892
    .local v0, "context":Landroid/content/Context;
    const-string v1, "BooksProvider"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 893
    const-string v1, "BooksProvider"

    const-string v2, "createDataManager() creating db helper"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/provider/database/BooksDatabase;

    new-instance v2, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/apps/books/provider/IcingGlobalSearchHelper;->TABLE_STORAGE_SPECS:[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    invoke-direct {v1, v0, v2, v3, p1}, Lcom/google/android/apps/books/provider/database/BooksDatabase;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/provider/database/ContentFileManager;[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)V

    iput-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    .line 897
    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    return-object v1
.end method

.method protected bridge synthetic createDataManager(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;
    .locals 1
    .param p1, "x0"    # Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/provider/BooksProvider;->createDataManager(Lcom/google/android/gms/appdatasearch/util/AppDataSearchDatabase$TableChangeListener;)Lcom/google/android/apps/books/provider/database/BooksDatabase;

    move-result-object v0

    return-object v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 529
    const-string v2, "BooksProvider"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 530
    const-string v2, "BooksProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delete(uri="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", selection="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", selectionArgs="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/SecurityUtils;->checkCallerSignature(Landroid/content/Context;)V

    .line 535
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/apps/books/util/ProviderUtils;->ensureNotOnMainThread(Landroid/content/Context;Landroid/net/Uri;)V

    .line 536
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 537
    .local v0, "match":I
    sparse-switch v0, :sswitch_data_0

    .line 582
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported URI for delete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 541
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 580
    :goto_0
    return v1

    .line 544
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 546
    .local v1, "rowsDeleted":I
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->notifyVolumesTableChanged()V

    goto :goto_0

    .line 560
    .end local v1    # "rowsDeleted":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mManifestProv:Lcom/google/android/apps/books/provider/ManifestProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 563
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mSessionKeysProv:Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/SessionKeysProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 567
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionsProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 571
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 574
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    invoke-virtual {v2}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;->deleteAllFiles()V

    .line 575
    const/4 v1, 0x0

    goto :goto_0

    .line 578
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mAccountsProv:Lcom/google/android/apps/books/provider/AccountsProvidelet;

    invoke-virtual {v2, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/AccountsProvidelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 580
    :sswitch_8
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/BooksProvider;->clearAccountContent(Landroid/net/Uri;)I

    move-result v1

    goto :goto_0

    .line 537
    nop

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_1
        0x78 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xcd -> :sswitch_2
        0xce -> :sswitch_2
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x145 -> :sswitch_2
        0x146 -> :sswitch_2
        0x15e -> :sswitch_2
        0x15f -> :sswitch_2
        0x190 -> :sswitch_2
        0x191 -> :sswitch_2
        0x1c2 -> :sswitch_3
        0x1c3 -> :sswitch_3
        0x258 -> :sswitch_0
        0x259 -> :sswitch_0
        0x25a -> :sswitch_0
        0x2bc -> :sswitch_4
        0x2bd -> :sswitch_4
        0x2be -> :sswitch_4
        0x2c6 -> :sswitch_5
        0x2c7 -> :sswitch_5
        0x2c8 -> :sswitch_5
        0x384 -> :sswitch_6
        0x3e8 -> :sswitch_7
        0x3e9 -> :sswitch_7
        0x3ea -> :sswitch_8
    .end sparse-switch
.end method

.method public doGetType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 238
    const-string v1, "BooksProvider"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    const-string v1, "BooksProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doGetType() uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 243
    .local v0, "match":I
    sparse-switch v0, :sswitch_data_0

    .line 315
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 245
    :sswitch_0
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.volume"

    goto :goto_0

    .line 248
    :sswitch_1
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.volume"

    goto :goto_0

    .line 251
    :sswitch_2
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.state"

    goto :goto_0

    .line 253
    :sswitch_3
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.state"

    goto :goto_0

    .line 256
    :sswitch_4
    const-string v1, "image/png"

    goto :goto_0

    .line 258
    :sswitch_5
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.section"

    goto :goto_0

    .line 260
    :sswitch_6
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.section"

    goto :goto_0

    .line 262
    :sswitch_7
    const-string v1, "text/html"

    goto :goto_0

    .line 264
    :sswitch_8
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.chapter"

    goto :goto_0

    .line 266
    :sswitch_9
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.chapter"

    goto :goto_0

    .line 268
    :sswitch_a
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.resource"

    goto :goto_0

    .line 270
    :sswitch_b
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.resource"

    goto :goto_0

    .line 272
    :sswitch_c
    const-string v1, "application/octet-stream"

    goto :goto_0

    .line 274
    :sswitch_d
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.segment_resource"

    goto :goto_0

    .line 277
    :sswitch_e
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.resource_resource"

    goto :goto_0

    .line 279
    :sswitch_f
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.segment_resource"

    goto :goto_0

    .line 281
    :sswitch_10
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.shared_resource"

    goto :goto_0

    .line 283
    :sswitch_11
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.shared_resource"

    goto :goto_0

    .line 285
    :sswitch_12
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.page"

    goto :goto_0

    .line 287
    :sswitch_13
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.page"

    goto :goto_0

    .line 289
    :sswitch_14
    const-string v1, "image/png"

    goto :goto_0

    .line 293
    :sswitch_15
    const-string v1, "BooksProvider"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 294
    const-string v1, "BooksProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getType() still seeing global search uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_1
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.volume"

    goto :goto_0

    .line 299
    :sswitch_16
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.account.collection"

    goto :goto_0

    .line 301
    :sswitch_17
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.account.collection"

    goto :goto_0

    .line 303
    :sswitch_18
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.account.collection.volume"

    goto :goto_0

    .line 305
    :sswitch_19
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.volume"

    goto :goto_0

    .line 307
    :sswitch_1a
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.volume"

    goto :goto_0

    .line 309
    :sswitch_1b
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.session_key"

    goto :goto_0

    .line 311
    :sswitch_1c
    const-string v1, "vnd.android.cursor.item/com.google.android.apps.books.session_key"

    goto :goto_0

    .line 313
    :sswitch_1d
    const-string v1, "vnd.android.cursor.dir/com.google.android.apps.books.configuration"

    goto :goto_0

    .line 243
    nop

    :sswitch_data_0
    .sparse-switch
        0x50 -> :sswitch_1
        0x64 -> :sswitch_0
        0x78 -> :sswitch_1
        0x96 -> :sswitch_4
        0x97 -> :sswitch_4
        0xc8 -> :sswitch_5
        0xc9 -> :sswitch_6
        0xca -> :sswitch_7
        0xcd -> :sswitch_8
        0xce -> :sswitch_9
        0x12c -> :sswitch_a
        0x12d -> :sswitch_b
        0x12e -> :sswitch_c
        0x145 -> :sswitch_e
        0x146 -> :sswitch_e
        0x15e -> :sswitch_d
        0x15f -> :sswitch_f
        0x190 -> :sswitch_12
        0x191 -> :sswitch_13
        0x192 -> :sswitch_14
        0x1a4 -> :sswitch_10
        0x1a5 -> :sswitch_11
        0x1c2 -> :sswitch_1b
        0x1c3 -> :sswitch_1c
        0x1f4 -> :sswitch_15
        0x1f5 -> :sswitch_15
        0x1f6 -> :sswitch_15
        0x258 -> :sswitch_2
        0x259 -> :sswitch_2
        0x25a -> :sswitch_3
        0x2bc -> :sswitch_16
        0x2bd -> :sswitch_16
        0x2be -> :sswitch_17
        0x2c6 -> :sswitch_18
        0x2c7 -> :sswitch_19
        0x2c8 -> :sswitch_1a
        0x320 -> :sswitch_1d
    .end sparse-switch
.end method

.method public doOnCreate()Z
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 176
    .local v1, "context":Landroid/content/Context;
    const-string v2, "BooksProvider"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    const-string v2, "BooksProvider"

    const-string v3, "doOnCreate() starts"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getDataManager()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/provider/database/BooksDatabase;

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    .line 182
    invoke-static {}, Lcom/google/android/apps/books/provider/database/DbAnalyzer;->shouldLogSizes()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/android/apps/books/provider/database/DbAnalyzer;->shouldLogColumns()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 183
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/provider/database/DbAnalyzer;

    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v2}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/provider/database/DbAnalyzer;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 184
    .local v0, "analyzer":Lcom/google/android/apps/books/provider/database/DbAnalyzer;
    invoke-virtual {v0}, Lcom/google/android/apps/books/provider/database/DbAnalyzer;->maybeLogSizes()V

    .line 185
    invoke-virtual {v0}, Lcom/google/android/apps/books/provider/database/DbAnalyzer;->maybeLogColumns()V

    .line 188
    .end local v0    # "analyzer":Lcom/google/android/apps/books/provider/database/DbAnalyzer;
    :cond_2
    new-instance v2, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;-><init>(Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    .line 190
    new-instance v2, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/provider/database/BooksDatabase;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    .line 192
    new-instance v2, Lcom/google/android/apps/books/provider/ManifestProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    iget-object v4, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    sget-object v5, Lcom/google/android/apps/books/provider/BooksProvider;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/ManifestProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mManifestProv:Lcom/google/android/apps/books/provider/ManifestProvidelet;

    .line 193
    new-instance v2, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;

    new-instance v3, Lcom/google/android/apps/books/provider/BooksProvider$3;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/provider/BooksProvider$3;-><init>(Lcom/google/android/apps/books/provider/BooksProvider;)V

    invoke-static {v1, v3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->createFromContext(Landroid/content/Context;Lcom/google/android/apps/books/provider/VolumeContentStore$BaseDirectorySource;)Lcom/google/android/apps/books/provider/VolumeContentStore;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;-><init>(Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mVolumeContentProv:Lcom/google/android/apps/books/provider/VolumeContentProvidelet;

    .line 200
    new-instance v2, Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    iget-object v4, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    sget-object v5, Lcom/google/android/apps/books/provider/BooksProvider;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/SessionKeysProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mSessionKeysProv:Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

    .line 201
    new-instance v2, Lcom/google/android/apps/books/provider/AccountsProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    iget-object v4, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    sget-object v5, Lcom/google/android/apps/books/provider/BooksProvider;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/AccountsProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mAccountsProv:Lcom/google/android/apps/books/provider/AccountsProvidelet;

    .line 203
    new-instance v2, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    iget-object v4, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    sget-object v5, Lcom/google/android/apps/books/provider/BooksProvider;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    .line 205
    new-instance v2, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/provider/database/BooksDatabase;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    .line 207
    new-instance v2, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    iget-object v4, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    sget-object v5, Lcom/google/android/apps/books/provider/BooksProvider;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionsProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

    .line 209
    new-instance v2, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    iget-object v3, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/provider/database/BooksDatabase;)V

    iput-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    .line 212
    const-string v2, "BooksProvider"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 213
    const-string v2, "BooksProvider"

    const-string v3, "doOnCreate() ends"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_3
    const/4 v2, 0x1

    return v2
.end method

.method public doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 380
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/BooksProvider;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 388
    const-string v1, "BooksProvider"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 389
    const-string v1, "BooksProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doQuery(uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", projection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", selection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", selectionArgs="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sortOrder="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/SecurityUtils;->checkCallerSignature(Landroid/content/Context;)V

    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/ProviderUtils;->ensureNotOnMainThread(Landroid/content/Context;Landroid/net/Uri;)V

    .line 396
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/provider/BooksProvider;->dispatchQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    .line 398
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 402
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 404
    :cond_1
    return-object v0
.end method

.method protected getContentProviderAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 902
    const-string v0, "com.google.android.apps.books"

    return-object v0
.end method

.method public getDatabaseHelper()Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    return-object v0
.end method

.method protected getGlobalSearchableAppInfo()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 7

    .prologue
    .line 907
    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    const v1, 0x7f0f0058

    const v2, 0x7f0f01f7

    const/high16 v3, 0x7f030000

    const-string v4, "com.google.android.apps.books.intent.action.READ"

    const-string v5, "gsa://com.google.android.apps.books/volumes"

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 321
    const-string v2, "BooksProvider"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    const-string v2, "BooksProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert(uri="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", values="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/SecurityUtils;->checkCallerSignature(Landroid/content/Context;)V

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/apps/books/util/ProviderUtils;->ensureNotOnMainThread(Landroid/content/Context;Landroid/net/Uri;)V

    .line 327
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v1

    .line 328
    .local v1, "match":I
    sparse-switch v1, :sswitch_data_0

    .line 355
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported URI for insert: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 330
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 353
    :goto_0
    return-object v0

    .line 332
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 333
    .local v0, "localVolumesUri":Landroid/net/Uri;
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->notifyVolumesTableChanged()V

    goto :goto_0

    .line 341
    .end local v0    # "localVolumesUri":Landroid/net/Uri;
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mManifestProv:Lcom/google/android/apps/books/provider/ManifestProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 344
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mSessionKeysProv:Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/SessionKeysProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 346
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionsProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 348
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 350
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 353
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mAccountsProv:Lcom/google/android/apps/books/provider/AccountsProvidelet;

    invoke-virtual {v2, v1, p1, p2}, Lcom/google/android/apps/books/provider/AccountsProvidelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 328
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xcd -> :sswitch_2
        0x12c -> :sswitch_2
        0x145 -> :sswitch_2
        0x15e -> :sswitch_2
        0x190 -> :sswitch_2
        0x1c2 -> :sswitch_3
        0x1c3 -> :sswitch_3
        0x258 -> :sswitch_0
        0x2bc -> :sswitch_4
        0x2c6 -> :sswitch_5
        0x320 -> :sswitch_6
        0x3e8 -> :sswitch_7
        0x3e9 -> :sswitch_7
    .end sparse-switch
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 614
    const-string v1, "BooksProvider"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 615
    const-string v1, "BooksProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "openFile(uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/SecurityUtils;->checkCallerSignature(Landroid/content/Context;)V

    .line 619
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/ProviderUtils;->ensureNotOnMainThread(Landroid/content/Context;Landroid/net/Uri;)V

    .line 620
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 621
    .local v0, "match":I
    sparse-switch v0, :sswitch_data_0

    .line 641
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    throw v1

    .line 625
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->openFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 638
    :goto_0
    return-object v1

    .line 629
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;->openFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0

    .line 636
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mVolumeContentProv:Lcom/google/android/apps/books/provider/VolumeContentProvidelet;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->openFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0

    .line 638
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mVolumeContentProv:Lcom/google/android/apps/books/provider/VolumeContentProvidelet;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentProvidelet;->openFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0

    .line 621
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x6e -> :sswitch_0
        0x78 -> :sswitch_0
        0x96 -> :sswitch_2
        0x97 -> :sswitch_2
        0xca -> :sswitch_2
        0x12e -> :sswitch_3
        0x192 -> :sswitch_2
        0x193 -> :sswitch_2
        0x1a5 -> :sswitch_2
        0x258 -> :sswitch_1
        0x259 -> :sswitch_1
        0x25a -> :sswitch_1
    .end sparse-switch
.end method

.method protected final shouldRegisterCorporaOnCreate()Z
    .locals 1

    .prologue
    .line 223
    sget-boolean v0, Lcom/google/android/apps/books/provider/BooksProvider;->sIsIcingEnabled:Z

    return v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 471
    const-string v0, "BooksProvider"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    const-string v0, "BooksProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update(uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", selection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", selectionArgs="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", values="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/SecurityUtils;->checkCallerSignature(Landroid/content/Context;)V

    .line 477
    invoke-virtual {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/ProviderUtils;->ensureNotOnMainThread(Landroid/content/Context;Landroid/net/Uri;)V

    .line 478
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v1

    .line 479
    .local v1, "match":I
    sparse-switch v1, :sswitch_data_0

    .line 522
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported URI for update: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 483
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalStatesProvidelet:Lcom/google/android/apps/books/provider/LocalStatesProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/LocalStatesProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 520
    :goto_0
    return v6

    .line 485
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 487
    .local v6, "rowsUpdated":I
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/BooksProvider;->notifyVolumesTableChanged()V

    goto :goto_0

    .line 501
    .end local v6    # "rowsUpdated":I
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mManifestProv:Lcom/google/android/apps/books/provider/ManifestProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/ManifestProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 504
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mSessionKeysProv:Lcom/google/android/apps/books/provider/SessionKeysProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/SessionKeysProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 508
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionsProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 513
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mLocalCollectionVolumesProvidelet:Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/LocalCollectionVolumesProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 516
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mConfigurationProvidelet:Lcom/google/android/apps/books/provider/ConfigurationProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/ConfigurationProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 520
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/books/provider/BooksProvider;->mAccountsProv:Lcom/google/android/apps/books/provider/AccountsProvidelet;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/AccountsProvidelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    .line 479
    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xcd -> :sswitch_2
        0xce -> :sswitch_2
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x145 -> :sswitch_2
        0x146 -> :sswitch_2
        0x15e -> :sswitch_2
        0x15f -> :sswitch_2
        0x190 -> :sswitch_2
        0x191 -> :sswitch_2
        0x1c2 -> :sswitch_3
        0x1c3 -> :sswitch_3
        0x258 -> :sswitch_0
        0x259 -> :sswitch_0
        0x25a -> :sswitch_0
        0x2bc -> :sswitch_4
        0x2bd -> :sswitch_4
        0x2be -> :sswitch_4
        0x2c6 -> :sswitch_5
        0x2c7 -> :sswitch_5
        0x2c8 -> :sswitch_5
        0x320 -> :sswitch_6
        0x3e8 -> :sswitch_7
        0x3e9 -> :sswitch_7
    .end sparse-switch
.end method
