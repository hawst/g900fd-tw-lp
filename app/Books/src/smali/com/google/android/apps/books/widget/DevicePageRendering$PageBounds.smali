.class public Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
.super Ljava/lang/Object;
.source "DevicePageRendering.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/DevicePageRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PageBounds"
.end annotation


# instance fields
.field public final pageBottom:I

.field public final pageLeft:I

.field public final pageRight:I

.field public final pageTop:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "pageLeft"    # I
    .param p2, "pageTop"    # I
    .param p3, "pageRight"    # I
    .param p4, "pageBottom"    # I

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    iput p1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    .line 259
    iput p2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    .line 260
    iput p3, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    .line 261
    iput p4, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    .line 262
    return-void
.end method


# virtual methods
.method public denormalizeX(F)I
    .locals 2
    .param p1, "x"    # F

    .prologue
    .line 276
    iget v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public denormalizeY(F)I
    .locals 2
    .param p1, "y"    # F

    .prologue
    .line 283
    iget v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getHeight()I
    .locals 2

    .prologue
    .line 269
    iget v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 265
    iget v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public intersects(Landroid/graphics/Rect;)Z
    .locals 4
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 290
    iget v0, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    iget v2, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    iget v3, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
