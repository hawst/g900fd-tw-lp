.class public Lcom/google/android/apps/books/widget/BooksVideoView;
.super Landroid/widget/VideoView;
.source "BooksVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 1
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksVideoView;->mCallbacks:Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksVideoView;->mCallbacks:Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;->onSystemWindowInsetsChanged(Landroid/graphics/Rect;)V

    .line 44
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setCallbacks(Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksVideoView;->mCallbacks:Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;

    .line 34
    return-void
.end method
