.class Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;
.super Ljava/lang/Object;
.source "SwipeableLinearLayout.java"

# interfaces
.implements Lcom/google/android/ublib/infocards/SwipeHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SwipeableLinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwipeCallback"
.end annotation


# instance fields
.field private final hitRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)V
    .locals 1

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->hitRect:Landroid/graphics/Rect;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;Lcom/google/android/apps/books/widget/SwipeableLinearLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/SwipeableLinearLayout;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/SwipeableLinearLayout$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;-><init>(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)V

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->getScrollX()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    float-to-int v3, v5

    .line 41
    .local v3, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->getScrollY()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    float-to-int v4, v5

    .line 42
    .local v4, "y":I
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->getChildCount()I

    move-result v1

    .line 44
    .local v1, "childCount":I
    add-int/lit8 v2, v1, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 45
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 46
    .local v0, "child":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->hitRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 47
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->hitRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 52
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return-object v0

    .line 44
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 52
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 57
    return-void
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    # getter for: Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mDismissCallback:Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->access$000(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$SwipeCallback;->this$0:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    # getter for: Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->mDismissCallback:Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->access$000(Lcom/google/android/apps/books/widget/SwipeableLinearLayout;)Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;->childDismissed(Landroid/view/View;)V

    .line 64
    :cond_0
    return-void
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 72
    return-void
.end method

.method public onSnapBackCompleted(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 68
    return-void
.end method
