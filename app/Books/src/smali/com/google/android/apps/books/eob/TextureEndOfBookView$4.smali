.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;
.super Ljava/lang/Object;
.source "TextureEndOfBookView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$buyUrl:Ljava/lang/String;

.field final synthetic val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$buyUrl:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    iput-object p5, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 228
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$activity:Landroid/app/Activity;

    const v2, 0x7f0f00b6

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnectedElseToast(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->EOB_BUY_AFTER_SAMPLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$buyUrl:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$buyUrl:Ljava/lang/String;

    .line 245
    .local v0, "localBuyUrl":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_3

    .line 246
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$volumeId:Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v4}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$400(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v4

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 253
    .end local v0    # "localBuyUrl":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$400(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$400(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    if-eqz v1, :cond_2

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "localBuyUrl":Ljava/lang/String;
    goto :goto_0

    .line 242
    .end local v0    # "localBuyUrl":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "localBuyUrl":Ljava/lang/String;
    goto :goto_0

    .line 249
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$callbacks:Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$4;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "books_inapp_eob_about"

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
