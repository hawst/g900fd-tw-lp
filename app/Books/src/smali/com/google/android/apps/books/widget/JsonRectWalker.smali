.class public Lcom/google/android/apps/books/widget/JsonRectWalker;
.super Ljava/lang/Object;
.source "JsonRectWalker.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field private final mIntArrayParser:Lcom/google/android/apps/books/util/IntArrayParser;

.field private mParsedString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/google/android/apps/books/util/IntArrayParser;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/IntArrayParser;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mIntArrayParser:Lcom/google/android/apps/books/util/IntArrayParser;

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mParsedString:Ljava/lang/String;

    return-void
.end method

.method private nextInt()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mIntArrayParser:Lcom/google/android/apps/books/util/IntArrayParser;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/IntArrayParser;->nextInt()I

    move-result v0

    return v0
.end method


# virtual methods
.method public next(Landroid/graphics/Rect;)Z
    .locals 4
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mIntArrayParser:Lcom/google/android/apps/books/util/IntArrayParser;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/IntArrayParser;->hasMore()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 30
    :goto_0
    return v0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/JsonRectWalker;->nextInt()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/JsonRectWalker;->nextInt()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/JsonRectWalker;->nextInt()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/JsonRectWalker;->nextInt()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 30
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 10
    check-cast p1, Landroid/graphics/Rect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/JsonRectWalker;->next(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mIntArrayParser:Lcom/google/android/apps/books/util/IntArrayParser;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mParsedString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/IntArrayParser;->init(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public setJson(Ljava/lang/String;)V
    .locals 0
    .param p1, "json"    # Ljava/lang/String;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/google/android/apps/books/widget/JsonRectWalker;->mParsedString:Ljava/lang/String;

    .line 16
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/JsonRectWalker;->reset()V

    .line 17
    return-void
.end method
