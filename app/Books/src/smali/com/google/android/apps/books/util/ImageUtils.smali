.class public final Lcom/google/android/apps/books/util/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# static fields
.field private static BLACK_20_PERCENT_SHADOW:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/high16 v0, 0x33000000

    sput v0, Lcom/google/android/apps/books/util/ImageUtils;->BLACK_20_PERCENT_SHADOW:I

    return-void
.end method

.method public static frameBitmapInCircle(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "input"    # Landroid/graphics/Bitmap;
    .param p1, "frameWidth"    # F

    .prologue
    .line 72
    if-nez p0, :cond_0

    .line 73
    const/4 v3, 0x0

    .line 114
    :goto_0
    return-object v3

    .line 77
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 78
    .local v2, "inputWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 80
    .local v1, "inputHeight":I
    if-lt v2, v1, :cond_1

    .line 81
    div-int/lit8 v9, v2, 0x2

    div-int/lit8 v10, v1, 0x2

    sub-int v7, v9, v10

    .line 82
    .local v7, "targetX":I
    const/4 v8, 0x0

    .line 83
    .local v8, "targetY":I
    move v6, v1

    .line 91
    .local v6, "targetSize":I
    :goto_1
    float-to-int v9, p1

    add-int/2addr v9, v6

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 93
    .local v3, "output":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 96
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/graphics/Paint;

    const/4 v9, 0x1

    invoke-direct {v4, v9}, Landroid/graphics/Paint;-><init>(I)V

    .line 97
    .local v4, "paint":Landroid/graphics/Paint;
    sget v9, Lcom/google/android/apps/books/util/ImageUtils;->BLACK_20_PERCENT_SHADOW:I

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    div-int/lit8 v9, v6, 0x2

    int-to-float v5, v9

    .line 99
    .local v5, "radius":F
    add-float v9, v5, p1

    invoke-virtual {v0, v5, v9, v5, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 102
    const/high16 v9, -0x1000000

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    invoke-virtual {v0, v5, v5, v5, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 106
    new-instance v9, Landroid/graphics/PorterDuffXfermode;

    sget-object v10, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v9, v10}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 107
    int-to-float v9, v7

    int-to-float v10, v8

    invoke-virtual {v0, p0, v9, v10, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 109
    sget-object v9, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 110
    invoke-virtual {v4, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 111
    const/4 v9, -0x1

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    invoke-virtual {v0, v5, v5, v5, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 85
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "output":Landroid/graphics/Bitmap;
    .end local v4    # "paint":Landroid/graphics/Paint;
    .end local v5    # "radius":F
    .end local v6    # "targetSize":I
    .end local v7    # "targetX":I
    .end local v8    # "targetY":I
    :cond_1
    const/4 v7, 0x0

    .line 86
    .restart local v7    # "targetX":I
    div-int/lit8 v9, v1, 0x2

    div-int/lit8 v10, v2, 0x2

    sub-int v8, v9, v10

    .line 87
    .restart local v8    # "targetY":I
    move v6, v2

    .restart local v6    # "targetSize":I
    goto :goto_1
.end method

.method public static getCover(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "original"    # Landroid/graphics/Bitmap;
    .param p1, "size"    # I

    .prologue
    const/4 v9, 0x1

    .line 28
    const/4 v6, 0x0

    .line 29
    .local v6, "widthBuffer":I
    const/4 v4, 0x0

    .line 30
    .local v4, "heightBuffer":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-le v7, v8, :cond_0

    .line 32
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    mul-int/2addr v7, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    div-int/2addr v7, v8

    invoke-static {p0, v7, p1, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 34
    .local v3, "cover":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int v7, p1, v7

    div-int/lit8 v6, v7, 0x2

    .line 44
    :goto_0
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v7, v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-direct {v5, v6, v4, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 47
    .local v5, "newRect":Landroid/graphics/Rect;
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p1, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 48
    .local v2, "canvasCover":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 50
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 53
    .local v0, "antiAliasPaint":Landroid/graphics/Paint;
    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 55
    const/4 v7, 0x0

    invoke-virtual {v1, v3, v7, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 57
    return-object v2

    .line 35
    .end local v0    # "antiAliasPaint":Landroid/graphics/Paint;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "canvasCover":Landroid/graphics/Bitmap;
    .end local v3    # "cover":Landroid/graphics/Bitmap;
    .end local v5    # "newRect":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ge v7, v8, :cond_1

    .line 37
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int/2addr v7, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    div-int/2addr v7, v8

    invoke-static {p0, p1, v7, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 39
    .restart local v3    # "cover":Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int v7, p1, v7

    div-int/lit8 v4, v7, 0x2

    goto :goto_0

    .line 42
    .end local v3    # "cover":Landroid/graphics/Bitmap;
    :cond_1
    invoke-static {p0, p1, p1, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .restart local v3    # "cover":Landroid/graphics/Bitmap;
    goto :goto_0
.end method
