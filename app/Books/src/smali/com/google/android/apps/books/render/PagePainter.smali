.class public interface abstract Lcom/google/android/apps/books/render/PagePainter;
.super Ljava/lang/Object;
.source "PagePainter.java"

# interfaces
.implements Lcom/google/android/apps/books/util/SimpleDrawable;


# virtual methods
.method public abstract canStillDraw()Z
.end method

.method public abstract getOwnedBitmap(Z)Landroid/graphics/Bitmap;
.end method

.method public abstract getSharedBitmap(Z)Landroid/graphics/Bitmap;
.end method

.method public abstract isMutableBitmap()Z
.end method

.method public abstract isUpdatable()Z
.end method

.method public abstract recycle()V
.end method
