.class public Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;
.super Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
.source "ImageModeSearchMatchRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/SearchMatchRectsCache",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/render/LabeledRect;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mRenderer:Lcom/google/android/apps/books/render/ImageModeRenderer;

.field private final mSearchHighlightColor:I

.field private final mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;ILcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ZLcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V
    .locals 6
    .param p1, "renderer"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p2, "searchHighlightColor"    # I
    .param p3, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p4, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p5, "displayTwoPages"    # Z
    .param p6, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    .prologue
    .line 31
    invoke-interface {p3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move v3, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ZZLcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    .line 70
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mDisplayTwoPages:Z

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mRenderer:Lcom/google/android/apps/books/render/ImageModeRenderer;

    .line 33
    iput p2, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mSearchHighlightColor:I

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 35
    return-void
.end method

.method private getFirstTextLocationAfterSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 5
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v4, 0x0

    .line 89
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v2, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget v3, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 91
    .local v0, "nextSpreadIdentifier":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mRenderer:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 92
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v2}, Lcom/google/android/apps/books/render/PageHandle;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 93
    .local v1, "position":Lcom/google/android/apps/books/common/Position;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v2, v1, v4}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    goto :goto_0
.end method


# virtual methods
.method protected finishProcessRequestResult()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onCacheUpdated()V

    .line 60
    return-void
.end method

.method protected getHighlightRectsWalker(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/widget/Walker;
    .locals 8
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 186
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v2

    .line 187
    .local v2, "pageIndex":I
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 188
    .local v3, "queryHighlights":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    if-eqz v3, :cond_0

    .line 189
    new-instance v1, Lcom/google/android/apps/books/widget/SearchHighlightRects;

    iget v5, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mSearchHighlightColor:I

    invoke-direct {v1, v3, v5}, Lcom/google/android/apps/books/widget/SearchHighlightRects;-><init>(Ljava/util/Collection;I)V

    .line 191
    .local v1, "highlightRectsForQuery":Lcom/google/android/apps/books/widget/SearchHighlightRects;
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/SearchHighlightRects;->getWalker()Lcom/google/android/apps/books/widget/Walker;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 199
    .end local v1    # "highlightRectsForQuery":Lcom/google/android/apps/books/widget/SearchHighlightRects;
    .end local v2    # "pageIndex":I
    .end local v3    # "queryHighlights":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    :cond_0
    :goto_0
    return-object v4

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v5, "ImageModeCache"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 197
    const-string v5, "ImageModeCache"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bad page id for loading highlights: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getMatchIndexForSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z
    .locals 10
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "positionWithinSearchResults"    # Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 100
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mRenderer:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5, p1, v8}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 101
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5, v7}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v5}, Lcom/google/android/apps/books/render/PageHandle;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 102
    .local v1, "firstPositionOfSpread":Lcom/google/android/apps/books/common/Position;
    const-string v5, "ImageModeCache"

    const/4 v8, 0x3

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 103
    const-string v5, "ImageModeCache"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "firstPositionOfSpread: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    if-nez v1, :cond_1

    .line 106
    iput-boolean v7, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    .line 107
    const/4 v5, -0x1

    iput v5, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    .line 131
    :goto_0
    return v7

    .line 114
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    new-instance v8, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v8, v1, v7}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    invoke-interface {v5, v8}, Lcom/google/android/apps/books/app/SearchResultMap;->getNumMatchesBeforeTextLocation(Lcom/google/android/apps/books/annotations/TextLocation;)I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    .line 117
    .local v4, "numMatchesBeforeSpread":I
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getFirstTextLocationAfterSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v2

    .line 119
    .local v2, "firstTextLocationAfterSpread":Lcom/google/android/apps/books/annotations/TextLocation;
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v5, v2}, Lcom/google/android/apps/books/app/SearchResultMap;->getNumMatchesBeforeTextLocation(Lcom/google/android/apps/books/annotations/TextLocation;)I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    .line 121
    .local v3, "numMatchesBeforeNextSpread":I
    if-le v3, v4, :cond_3

    move v0, v6

    .line 123
    .local v0, "currentSpreadHasMatches":Z
    :goto_1
    iput-boolean v0, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    .line 129
    if-eqz v0, :cond_2

    move v7, v6

    :cond_2
    add-int v5, v4, v7

    iput v5, p2, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    move v7, v6

    .line 131
    goto :goto_0

    .end local v0    # "currentSpreadHasMatches":Z
    :cond_3
    move v0, v7

    .line 121
    goto :goto_1
.end method

.method protected getNextSearchLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 4
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getFirstTextLocationAfterSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 78
    .local v0, "firstTextLocationAfterSpread":Lcom/google/android/apps/books/annotations/TextLocation;
    const-string v1, "ImageModeCache"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "ImageModeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "firstPositionAfterSpread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    if-nez v0, :cond_1

    .line 82
    const/4 v1, 0x0

    .line 84
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/SearchResultMap;->findNextLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    goto :goto_0
.end method

.method protected getPaintables(I)Ljava/util/List;
    .locals 2
    .param p1, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "NOT IMPLEMENTED!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected getPrevSearchLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 5
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v4, 0x0

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mRenderer:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mTempPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v1}, Lcom/google/android/apps/books/render/PageHandle;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 140
    .local v0, "nextPosition":Lcom/google/android/apps/books/common/Position;
    const-string v1, "ImageModeCache"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    const-string v1, "ImageModeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nextPosition: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    if-nez v0, :cond_1

    .line 144
    const/4 v1, 0x0

    .line 146
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    new-instance v2, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v2, v0, v4}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/app/SearchResultMap;->findPreviousLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    goto :goto_0
.end method

.method public hasNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .locals 2
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getNextSearchLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 174
    .local v0, "nextSearchMatch":Lcom/google/android/apps/books/annotations/TextLocation;
    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    goto :goto_0
.end method

.method public hasPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .locals 2
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getPrevSearchLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 180
    .local v0, "prevSearchMatch":Lcom/google/android/apps/books/annotations/TextLocation;
    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    goto :goto_0
.end method

.method protected bridge synthetic makeCachedData(ILjava/util/List;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->makeCachedData(ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected makeCachedData(ILjava/util/List;)Ljava/util/List;
    .locals 0
    .param p1, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "rects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    return-object p2
.end method

.method public maybeLoadDataForPassage(IZ)V
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "refreshPagesOnChange"    # Z

    .prologue
    .line 54
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "NOT IMPLEMENTED!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public maybeMoveToPendingMatch()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PendingSearchMatchData;->getLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->moveToImageModeSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onSearchBarNavigationStateChanged()V

    .line 156
    return-void
.end method

.method public moveToPrevOrNextMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V
    .locals 4
    .param p1, "currentSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "searchingForward"    # Z

    .prologue
    .line 160
    if-eqz p2, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getNextSearchLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 162
    .local v0, "newTextLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    :goto_0
    const-string v1, "ImageModeCache"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    const-string v1, "ImageModeCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New Text Location: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_0
    if-eqz v0, :cond_1

    .line 166
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 168
    :cond_1
    return-void

    .line 160
    .end local v0    # "newTextLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->getPrevSearchLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    goto :goto_0
.end method

.method protected onLoadedRangeDataBulkSuccess()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onSearchBarNavigationStateChanged()V

    .line 48
    return-void
.end method
