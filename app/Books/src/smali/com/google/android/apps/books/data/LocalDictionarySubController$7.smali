.class Lcom/google/android/apps/books/data/LocalDictionarySubController$7;
.super Ljava/lang/Object;
.source "LocalDictionarySubController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/LocalDictionarySubController;->finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

.field final synthetic val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/data/LocalDictionarySubController;->mCurrentDictionaryDownload:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$502(Lcom/google/android/apps/books/data/LocalDictionarySubController;Ljava/lang/String;)Ljava/lang/String;

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    # invokes: Lcom/google/android/apps/books/data/LocalDictionarySubController;->isInProgress(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$100(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    # getter for: Lcom/google/android/apps/books/data/LocalDictionarySubController;->mListener:Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;
    invoke-static {v0}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$400(Lcom/google/android/apps/books/data/LocalDictionarySubController;)Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/dictionary/DictionaryDownloadListener;->finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$7;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    # invokes: Lcom/google/android/apps/books/data/LocalDictionarySubController;->startNextDictionaryDownload(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$600(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 377
    return-void
.end method
