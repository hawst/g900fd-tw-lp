.class public Lcom/google/android/apps/books/app/StubHomeCallbacks;
.super Lcom/google/android/apps/books/app/StubFragmentCallbacks;
.source "StubHomeCallbacks.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HomeFragment$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeTransitionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLibrarySortOrderFromPrefs()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 32
    sget-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->BY_TITLE:Lcom/google/android/apps/books/app/LibraryComparator;

    return-object v0
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 17
    return-void
.end method

.method public onSelectedLibrarySortOrder(Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 0
    .param p1, "libraryComparator"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 38
    return-void
.end method

.method public setTransitionView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 48
    return-void
.end method

.method public showLibrarySortMenu(Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 0
    .param p1, "currentSort"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 43
    return-void
.end method

.method public startForcedSync()V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 22
    return-void
.end method

.method public startUnpinProcess(Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubHomeCallbacks;->maybeLogMethodName()V

    .line 27
    return-void
.end method
