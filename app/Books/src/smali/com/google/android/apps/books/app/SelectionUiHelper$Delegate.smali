.class public interface abstract Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Delegate"
.end annotation


# virtual methods
.method public abstract attachSelectionPopup(Landroid/view/View;)V
.end method

.method public abstract dismissActiveSelection()V
.end method

.method public abstract dismissInfoCards()V
.end method

.method public abstract getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
.end method

.method public abstract getAnnotationController()Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getSelectionView()Landroid/view/View;
.end method

.method public abstract getShareTextForQuote(Ljava/lang/CharSequence;)Ljava/lang/String;
.end method

.method public abstract isSample()Z
.end method

.method public abstract isVertical()Z
.end method

.method public abstract lockSelection()V
.end method

.method public abstract onSearchInBookRequested(Ljava/lang/CharSequence;)V
.end method

.method public abstract onSelectedTextCopiedToClipboard()V
.end method

.method public abstract setSystemUiVisible(Z)V
.end method

.method public abstract translateText(Ljava/lang/CharSequence;)V
.end method
