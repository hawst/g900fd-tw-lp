.class Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;
.super Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;
.source "ListToRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperViewHandle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
        "<",
        "Landroid/view/ViewGroup;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;Landroid/view/ViewGroup;)V
    .locals 0
    .param p2, "wrapper"    # Landroid/view/ViewGroup;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;->this$0:Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;

    .line 28
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;-><init>(Landroid/view/View;)V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;Landroid/view/ViewGroup;Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;
    .param p2, "x1"    # Landroid/view/ViewGroup;
    .param p3, "x2"    # Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$1;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;-><init>(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method protected onBind(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;->getItemView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 34
    .local v2, "wrapper":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "oldChild":Landroid/view/View;
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter$WrapperViewHandle;->this$0:Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;

    # getter for: Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->mAdapter:Landroid/widget/Adapter;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->access$000(Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;)Landroid/widget/Adapter;

    move-result-object v3

    invoke-interface {v3, p1, v1, v2}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 37
    .local v0, "newChild":Landroid/view/View;
    if-eq v1, v0, :cond_0

    .line 38
    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 39
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 41
    :cond_0
    return-void

    .line 34
    .end local v0    # "newChild":Landroid/view/View;
    .end local v1    # "oldChild":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
