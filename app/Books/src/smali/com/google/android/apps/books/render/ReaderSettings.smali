.class public Lcom/google/android/apps/books/render/ReaderSettings;
.super Ljava/lang/Object;
.source "ReaderSettings.java"


# instance fields
.field public final fontFamily:Ljava/lang/String;

.field private final mLineHeightFP:I

.field private final mTextZoomFP:I

.field public final readerTheme:Ljava/lang/String;

.field public final textAlign:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;FF[Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textZoom"    # F
    .param p3, "lineHeight"    # F
    .param p4, "availableFonts"    # [Ljava/lang/CharSequence;

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 52
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getReaderTheme()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    .line 53
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getTypeface()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p4, v1}, Lcom/google/android/apps/books/render/ReaderSettings;->determineFontFamily([Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    .line 54
    mul-float v1, p2, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mTextZoomFP:I

    .line 55
    mul-float v1, p3, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mLineHeightFP:I

    .line 56
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getTextAlign()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    .line 57
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;FFLjava/lang/String;Z)V
    .locals 2
    .param p1, "readerTheme"    # Ljava/lang/String;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "textZoom"    # F
    .param p4, "lineHeight"    # F
    .param p5, "textAlign"    # Ljava/lang/String;
    .param p6, "pageAnnotationsEnabled"    # Z

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    .line 64
    mul-float v0, p3, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mTextZoomFP:I

    .line 65
    mul-float v0, p4, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mLineHeightFP:I

    .line 66
    iput-object p5, p0, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    .line 67
    return-void
.end method

.method private determineFontFamily([Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "availableFonts"    # [Ljava/lang/CharSequence;
    .param p2, "baseFamily"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v2, 0x0

    .line 98
    .local v2, "family":Ljava/lang/String;
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 99
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/CharSequence;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 100
    .local v1, "f":Ljava/lang/CharSequence;
    invoke-virtual {p2, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 101
    move-object v2, p2

    .line 108
    .end local v0    # "arr$":[Ljava/lang/CharSequence;
    .end local v1    # "f":Ljava/lang/CharSequence;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_0
    :goto_1
    return-object v2

    .line 99
    .restart local v0    # "arr$":[Ljava/lang/CharSequence;
    .restart local v1    # "f":Ljava/lang/CharSequence;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 106
    .end local v0    # "arr$":[Ljava/lang/CharSequence;
    .end local v1    # "f":Ljava/lang/CharSequence;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    move-object v2, p2

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 82
    instance-of v2, p1, Lcom/google/android/apps/books/render/ReaderSettings;

    if-nez v2, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 83
    check-cast v0, Lcom/google/android/apps/books/render/ReaderSettings;

    .line 84
    .local v0, "compare":Lcom/google/android/apps/books/render/ReaderSettings;
    iget-object v2, p0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mTextZoomFP:I

    iget v3, v0, Lcom/google/android/apps/books/render/ReaderSettings;->mTextZoomFP:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mLineHeightFP:I

    iget v3, v0, Lcom/google/android/apps/books/render/ReaderSettings;->mLineHeightFP:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getLineHeight()F
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mLineHeightFP:I

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getTextZoom()F
    .locals 2

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mTextZoomFP:I

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReaderSettings{readerTheme="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fontFamily=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", textZoom=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mTextZoomFP:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lineHeight=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->mLineHeightFP:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", textAlign=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
