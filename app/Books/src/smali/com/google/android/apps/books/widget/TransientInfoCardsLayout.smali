.class public Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
.super Landroid/view/ViewGroup;
.source "TransientInfoCardsLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$5;,
        Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;,
        Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;,
        Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;,
        Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;,
        Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    }
.end annotation


# instance fields
.field private mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

.field private final mAnimatorTarget:Ljava/lang/Object;

.field private mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

.field private mCardBackgroundAlpha:F

.field private final mCardBackgroundAlphaSetter:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

.field private mCurrentTouchSequenceStartedOnCard:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mHitRect:Landroid/graphics/Rect;

.field private final mHitTestRect:Landroid/graphics/Rect;

.field private mIsScrollingVertically:Z

.field private mLastCallToCancelCardsAnimationMs:J

.field private mLastCallToOnAnimationEndedMs:J

.field private mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

.field private final mLocation:[I

.field private mMaxFlingAnimationDuration:I

.field private mMinSystemUiBottomInset:I

.field private mPeekPixels:I

.field private mSystemUiBottomInset:I

.field private mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v2, 0x0

    .line 181
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 135
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 167
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;

    .line 168
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I

    .line 267
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitTestRect:Landroid/graphics/Rect;

    .line 388
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlphaSetter:Lcom/google/android/ublib/utils/Consumer;

    .line 879
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToOnAnimationEndedMs:J

    .line 880
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToCancelCardsAnimationMs:J

    .line 931
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimatorTarget:Ljava/lang/Object;

    .line 182
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->init(Landroid/content/Context;)V

    .line 183
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-wide/16 v2, 0x0

    .line 176
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 135
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 167
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;

    .line 168
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I

    .line 267
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitTestRect:Landroid/graphics/Rect;

    .line 388
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlphaSetter:Lcom/google/android/ublib/utils/Consumer;

    .line 879
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToOnAnimationEndedMs:J

    .line 880
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToCancelCardsAnimationMs:J

    .line 931
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimatorTarget:Ljava/lang/Object;

    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->init(Landroid/content/Context;)V

    .line 178
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 171
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 135
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 167
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;

    .line 168
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I

    .line 267
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitTestRect:Landroid/graphics/Rect;

    .line 388
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlphaSetter:Lcom/google/android/ublib/utils/Consumer;

    .line 879
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToOnAnimationEndedMs:J

    .line 880
    iput-wide v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToCancelCardsAnimationMs:J

    .line 931
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimatorTarget;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimatorTarget:Ljava/lang/Object;

    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->init(Landroid/content/Context;)V

    .line 173
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlpha:F

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p1, "x1"    # F
    .param p2, "x2"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsTranslation(FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mIsScrollingVertically:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mIsScrollingVertically:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLocation:[I

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsStateNoTransition(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->onAnimationEnded()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCurrentTouchSequenceStartedOnCard:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Landroid/view/VelocityTracker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/ublib/view/TranslationHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .param p2, "x2"    # Z
    .param p3, "x3"    # F

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .param p1, "x1"    # F
    .param p2, "x2"    # Z
    .param p3, "x3"    # F
    .param p4, "x4"    # Landroid/animation/Animator$AnimatorListener;
    .param p5, "x5"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 47
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateCardsViewTop(FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 47
    invoke-static {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getLowerState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v0

    return-object v0
.end method

.method private animateCardsViewTop(FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 9
    .param p1, "newTranslation"    # F
    .param p2, "flinging"    # Z
    .param p3, "flingVelocity"    # F
    .param p4, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p5, "destinationState"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 818
    const-string v3, "ReaderLayout"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 819
    const-string v3, "ReaderLayout"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Animating cards view translation to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->cancelCardsAnimation()V

    .line 824
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    .line 825
    .local v0, "cardsView":Landroid/view/View;
    new-instance v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V

    iput-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    .line 826
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iput-object p5, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->destinationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 827
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v4, v0}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v4

    iput v4, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->startTranslation:F

    .line 828
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimatorTarget:Ljava/lang/Object;

    const-string v5, "cardsTranslation"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget v8, v8, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->startTranslation:F

    aput v8, v6, v7

    const/4 v7, 0x1

    aput p1, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    .line 830
    if-eqz p4, :cond_1

    .line 831
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v3, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    invoke-virtual {v3, p4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 833
    :cond_1
    if-eqz p2, :cond_3

    .line 834
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v3, v0}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v3

    sub-float v1, p1, v3

    .line 835
    .local v1, "distance":F
    invoke-virtual {p0, v1, p3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getFlingAnimationDuration(FF)I

    move-result v2

    .line 836
    .local v2, "durationMillis":I
    const-string v3, "ReaderLayout"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 837
    const-string v3, "ReaderLayout"

    const-string v4, "Flinging at %.2f pixels/second to cover %f pixels in %d milliseconds"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v3, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 842
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v3, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-direct {v4, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 844
    .end local v1    # "distance":F
    .end local v2    # "durationMillis":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v3, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    new-instance v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$4;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$4;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 850
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v3, v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    .line 851
    return-void
.end method

.method private animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V
    .locals 9
    .param p1, "state"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .param p2, "flinging"    # Z
    .param p3, "velocity"    # F

    .prologue
    const/high16 v3, -0x80000000

    .line 768
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v6

    .line 770
    .local v6, "cardsView":Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v0, v2, :cond_0

    .line 773
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 774
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getWidth()I

    move-result v0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0, v6, v0, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->measureChild(Landroid/view/View;II)V

    .line 776
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->positionCardsView()V

    .line 779
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v8

    .line 780
    .local v8, "finalY":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v7

    .line 781
    .local v7, "currentPreTranslationTop":I
    sub-int v0, v8, v7

    int-to-float v1, v0

    .line 782
    .local v1, "finalTranslation":F
    const-string v0, "ReaderLayout"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 783
    const-string v0, "ReaderLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Animating cards view from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (translation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v3, v6}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (translation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    :cond_1
    new-instance v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;

    invoke-direct {v4, p0, p1, v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$3;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Landroid/view/View;)V

    .local v4, "listener":Landroid/animation/Animator$AnimatorListener;
    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v5, p1

    .line 805
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateCardsViewTop(FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    .line 806
    return-void
.end method

.method private animatingCards()Z
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cancelCardsAnimation()V
    .locals 2

    .prologue
    .line 809
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animatingCards()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 810
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->animator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 811
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    .line 812
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToCancelCardsAnimationMs:J

    .line 814
    :cond_0
    return-void
.end method

.method private constrainToBelowActionBar(I)I
    .locals 2
    .param p1, "cardsViewTop"    # I

    .prologue
    .line 693
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getActionBarBottom()I

    move-result v0

    .line 694
    .local v0, "topBuffer":I
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method private eventIsInCardsView(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 270
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-eq v3, v4, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    .line 273
    .local v0, "cardsView":Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/MathUtils;->round(F)I

    move-result v1

    .line 274
    .local v1, "eventX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/MathUtils;->round(F)I

    move-result v2

    .line 276
    .local v2, "eventY":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitTestRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedBounds(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 277
    iget-object v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitTestRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v3, v1, v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mHitTestRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int v4, v2, v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->hasChildAtLocation(II)Z

    move-result v3

    .line 281
    .end local v0    # "cardsView":Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .end local v1    # "eventX":I
    .end local v2    # "eventY":I
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getActionBarBottom()I
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    if-eqz v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;->getActionBarBottom()I

    move-result v0

    .line 910
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCardsBackgroundView()Landroid/view/View;
    .locals 1

    .prologue
    .line 613
    const v0, 0x7f0e011a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    .locals 2
    .param p1, "state"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 701
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$5;->$SwitchMap$com$google$android$apps$books$widget$TransientInfoCardsLayout$CardsState:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 709
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewVisibleHeight(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->constrainToBelowActionBar(I)I

    move-result v0

    :goto_0
    return v0

    .line 705
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->constrainToBelowActionBar(I)I

    move-result v0

    goto :goto_0

    .line 701
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private getCardsViewVisibleHeight(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I
    .locals 6
    .param p1, "state"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    .line 718
    .local v0, "cardsView":Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getMeasuredHeight()I

    move-result v1

    .line 719
    .local v1, "cardsViewHeight":I
    sget-object v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$5;->$SwitchMap$com$google$android$apps$books$widget$TransientInfoCardsLayout$CardsState:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 744
    :pswitch_0
    iget v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mSystemUiBottomInset:I

    add-int/2addr v3, v1

    :goto_0
    return v3

    .line 721
    :pswitch_1
    const/4 v3, 0x0

    goto :goto_0

    .line 726
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    div-int/lit8 v3, v3, 0x6

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mSystemUiBottomInset:I

    add-int v2, v3, v4

    .line 728
    .local v2, "maxHeight":I
    sget-object v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewVisibleHeight(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_0

    .line 730
    .end local v2    # "maxHeight":I
    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getNumberOfStacks()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 739
    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->getHeightOfTopStack()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090128

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mPeekPixels:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mSystemUiBottomInset:I

    add-int/2addr v3, v4

    goto :goto_0

    .line 733
    :pswitch_4
    sget-object v3, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewVisibleHeight(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v3

    goto :goto_0

    .line 736
    :pswitch_5
    iget v3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mSystemUiBottomInset:I

    add-int/2addr v3, v1

    goto :goto_0

    .line 719
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 730
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static getLowerState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1
    .param p0, "start"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .param p1, "end"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 940
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne p1, v0, :cond_1

    .line 941
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 943
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 186
    new-instance v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;)V

    .line 187
    .local v2, "gestureListener":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$MyGestureListener;
    new-instance v4, Landroid/view/GestureDetector;

    invoke-direct {v4, p1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mGestureDetector:Landroid/view/GestureDetector;

    .line 189
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 190
    .local v3, "res":Landroid/content/res/Resources;
    const v4, 0x7f0901b6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mPeekPixels:I

    .line 192
    const v4, 0x7f0901b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mMinSystemUiBottomInset:I

    .line 195
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 197
    const/4 v4, 0x1

    new-array v0, v4, [I

    const v4, 0x1010112

    aput v4, v0, v5

    .line 198
    .local v0, "attributeIds":[I
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 199
    .local v1, "attributes":Landroid/content/res/TypedArray;
    const/16 v4, 0x12c

    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mMaxFlingAnimationDuration:I

    .line 200
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 201
    return-void
.end method

.method private onAnimationEnded()V
    .locals 8

    .prologue
    .line 883
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    if-nez v4, :cond_1

    .line 886
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 887
    .local v0, "currentTimeMs":J
    const-string v4, "ReaderLayout"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onAnimationEnded invoked with null mAnimationState. Last called "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToOnAnimationEndedMs:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms ago. Last completed call to cancelCardsAnimation "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToCancelCardsAnimationMs:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms ago"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    .end local v0    # "currentTimeMs":J
    :cond_0
    :goto_0
    return-void

    .line 893
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLastCallToOnAnimationEndedMs:J

    .line 895
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-boolean v2, v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->layoutWhenDone:Z

    .line 896
    .local v2, "layout":Z
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v3, v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->destinationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 897
    .local v3, "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    .line 898
    if-eqz v2, :cond_2

    .line 899
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->requestLayout()V

    .line 901
    :cond_2
    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v3, v4, :cond_0

    .line 902
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;->onCardsLayoutHidden()V

    goto :goto_0
.end method

.method private positionCardsView()V
    .locals 9

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v1

    .line 664
    .local v1, "cardsView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getLeft()I

    move-result v2

    .line 665
    .local v2, "left":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getRight()I

    move-result v4

    .line 666
    .local v4, "right":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v7

    .line 668
    .local v7, "height":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 671
    .local v6, "cardsViewHeight":I
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$5;->$SwitchMap$com$google$android$apps$books$widget$TransientInfoCardsLayout$CardsState:[I

    iget-object v8, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v8}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->ordinal()I

    move-result v8

    aget v0, v0, v8

    packed-switch v0, :pswitch_data_0

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v3

    .line 686
    .local v3, "cardsViewTop":I
    add-int v0, v3, v6

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 687
    .local v5, "cardsViewBottom":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    .line 690
    .end local v5    # "cardsViewBottom":I
    :goto_0
    return-void

    .line 673
    .end local v3    # "cardsViewTop":I
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v3

    .line 674
    .restart local v3    # "cardsViewTop":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    add-int v5, v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    goto :goto_0

    .line 679
    .end local v3    # "cardsViewTop":I
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v3

    .line 680
    .restart local v3    # "cardsViewTop":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    add-int v5, v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/ublib/view/TranslationHelper;->layout(Landroid/view/View;IIII)V

    goto :goto_0

    .line 671
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setCardsBackgroundAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 399
    iput p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlpha:F

    .line 400
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->updateCardsBackgroundAlpha()V

    .line 401
    return-void
.end method

.method private setCardsStateNoTransition(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 5
    .param p1, "state"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 857
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-eq p1, v1, :cond_0

    .line 861
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    .line 863
    .local v0, "cardsView":Landroid/view/View;
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 864
    iget-boolean v1, p1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->cardsVisible:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 865
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->positionCardsView()V

    .line 867
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsBackgroundView()Landroid/view/View;

    move-result-object v1

    iget-boolean v4, p1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->backgroundVisible:Z

    if-eqz v4, :cond_2

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 870
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsBackgroundView()Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->backgroundVisible:Z

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 872
    .end local v0    # "cardsView":Landroid/view/View;
    :cond_0
    return-void

    .restart local v0    # "cardsView":Landroid/view/View;
    :cond_1
    move v1, v3

    .line 864
    goto :goto_0

    :cond_2
    move v2, v3

    .line 867
    goto :goto_1
.end method

.method private setCardsTranslation(FLcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 12
    .param p1, "translation"    # F
    .param p2, "lowerState"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v4

    .line 415
    .local v4, "cardsView":Landroid/view/View;
    iget-object v10, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v10, v4, p1}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationY(Landroid/view/View;F)V

    .line 418
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v9

    .line 419
    .local v9, "lowerStateTop":I
    sget-object v10, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-direct {p0, v10}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v6

    .line 420
    .local v6, "foregroundTop":I
    iget-object v10, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v10, v4}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedTop(Landroid/view/View;)F

    move-result v5

    .line 422
    .local v5, "currentTop":F
    int-to-float v10, v9

    cmpl-float v10, v5, v10

    if-ltz v10, :cond_1

    .line 423
    const/4 v7, 0x0

    .line 431
    .local v7, "fractionForeground":F
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsBackgroundView()Landroid/view/View;

    move-result-object v3

    .line 432
    .local v3, "cardsBackgroundView":Landroid/view/View;
    sget-object v10, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iget v10, v10, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->backgroundAlpha:F

    mul-float v0, v7, v10

    .line 433
    .local v0, "backgroundAlpha":F
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-ne v10, v11, :cond_0

    .line 437
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 439
    :cond_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 443
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float v8, v10, v7

    .line 445
    .local v8, "inverseProgress":F
    sget-object v10, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iget v10, v10, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->cardBackgroundAlpha:F

    sget-object v11, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iget v11, v11, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->cardBackgroundAlpha:F

    sub-float v2, v10, v11

    .line 447
    .local v2, "cardBackgroundAlphaDelta":F
    mul-float v10, v8, v2

    sget-object v11, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iget v11, v11, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->cardBackgroundAlpha:F

    add-float v1, v10, v11

    .line 449
    .local v1, "cardBackgroundAlpha":F
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsBackgroundAlpha(F)V

    .line 450
    return-void

    .line 424
    .end local v0    # "backgroundAlpha":F
    .end local v1    # "cardBackgroundAlpha":F
    .end local v2    # "cardBackgroundAlphaDelta":F
    .end local v3    # "cardsBackgroundView":Landroid/view/View;
    .end local v7    # "fractionForeground":F
    .end local v8    # "inverseProgress":F
    :cond_1
    int-to-float v10, v6

    cmpl-float v10, v5, v10

    if-lez v10, :cond_2

    .line 425
    int-to-float v10, v9

    sub-float/2addr v10, v5

    sub-int v11, v9, v6

    int-to-float v11, v11

    div-float v7, v10, v11

    .restart local v7    # "fractionForeground":F
    goto :goto_0

    .line 427
    .end local v7    # "fractionForeground":F
    :cond_2
    const/high16 v7, 0x3f800000    # 1.0f

    .restart local v7    # "fractionForeground":F
    goto :goto_0
.end method

.method private updateCardsBackgroundAlpha()V
    .locals 2

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlphaSetter:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->forEachCardView(Lcom/google/android/ublib/utils/Consumer;)V

    .line 405
    return-void
.end method


# virtual methods
.method public fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 625
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mMinSystemUiBottomInset:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mSystemUiBottomInset:I

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;->onSystemWindowInsetsChanged(Landroid/graphics/Rect;)V

    .line 629
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCardsState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    return-object v0
.end method

.method public getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .locals 1

    .prologue
    .line 617
    const v0, 0x7f0e011b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    return-object v0
.end method

.method public getContentView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 605
    const v0, 0x7f0e0110

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getEventualState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->destinationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 249
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    goto :goto_0
.end method

.method final getFlingAnimationDuration(FF)I
    .locals 3
    .param p1, "distance"    # F
    .param p2, "velocity"    # F

    .prologue
    .line 758
    const/high16 v1, 0x40400000    # 3.0f

    div-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float v0, v1, v2

    .line 759
    .local v0, "duration":F
    iget v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mMaxFlingAnimationDuration:I

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method public onCardsAdded(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
    .locals 2
    .param p1, "animateTo"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animatingCards()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->layoutWhenDone:Z

    .line 265
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v0, v1, :cond_1

    .line 259
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V

    goto :goto_0

    .line 262
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->updateCardsBackgroundAlpha()V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->requestLayout()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 286
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 287
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsBackgroundView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$1;-><init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 314
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 315
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mIsScrollingVertically:Z

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 317
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->eventIsInCardsView(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-eq v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCurrentTouchSequenceStartedOnCard:Z

    .line 320
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCurrentTouchSequenceStartedOnCard:Z

    if-eqz v0, :cond_1

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->cancelCardsAnimation()V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 324
    :cond_1
    return v1

    :cond_2
    move v0, v1

    .line 317
    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getContentView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 649
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsBackgroundView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 651
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->positionCardsView()V

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;->onLayoutChange(Landroid/view/View;IIII)V

    .line 656
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    .line 637
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->measureChildren(II)V

    .line 640
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 641
    .local v1, "maxWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 642
    .local v0, "maxHeight":I
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setMeasuredDimension(II)V

    .line 643
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 329
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v15

    .line 332
    .local v15, "detectorHandled":Z
    if-nez v15, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationY(Landroid/view/View;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v7

    .line 338
    .local v7, "cardsView":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 339
    .local v8, "cardsViewHeight":I
    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v16

    .line 340
    .local v16, "foregroundTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v1

    sub-int v17, v1, v16

    .line 341
    .local v17, "foregroundVisibleBottom":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v1, v7}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatedTop(Landroid/view/View;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 342
    .local v10, "currentTop":I
    add-int v9, v10, v8

    .line 343
    .local v9, "currentBottom":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v1, v2, :cond_1

    move/from16 v0, v16

    if-gt v10, v0, :cond_1

    move/from16 v0, v17

    if-lt v9, v0, :cond_1

    .line 346
    const/4 v15, 0x1

    .line 374
    .end local v7    # "cardsView":Landroid/view/View;
    .end local v8    # "cardsViewHeight":I
    .end local v9    # "currentBottom":I
    .end local v10    # "currentTop":I
    .end local v15    # "detectorHandled":Z
    .end local v16    # "foregroundTop":I
    .end local v17    # "foregroundVisibleBottom":I
    :cond_0
    :goto_0
    return v15

    .line 350
    .restart local v7    # "cardsView":Landroid/view/View;
    .restart local v8    # "cardsViewHeight":I
    .restart local v9    # "currentBottom":I
    .restart local v10    # "currentTop":I
    .restart local v15    # "detectorHandled":Z
    .restart local v16    # "foregroundTop":I
    .restart local v17    # "foregroundVisibleBottom":I
    :cond_1
    sub-int v1, v10, v16

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v12

    .line 351
    .local v12, "dTopForeground":I
    sub-int v1, v9, v17

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .line 352
    .local v11, "dBottomForeground":I
    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v1

    sub-int v1, v10, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v14

    .line 353
    .local v14, "dTopPeeking":I
    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsViewTop(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)I

    move-result v1

    sub-int v1, v10, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v13

    .line 357
    .local v13, "dTopHidden":I
    invoke-static {v11, v14}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-gt v12, v1, :cond_2

    .line 358
    sget-object v6, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 367
    .local v6, "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-eq v6, v1, :cond_5

    .line 368
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V

    .line 372
    :goto_2
    const/4 v15, 0x1

    goto :goto_0

    .line 359
    .end local v6    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :cond_2
    invoke-static {v12, v14}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-gt v11, v1, :cond_3

    .line 360
    sget-object v6, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .restart local v6    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    goto :goto_1

    .line 361
    .end local v6    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :cond_3
    if-gt v14, v13, :cond_4

    .line 362
    sget-object v6, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .restart local v6    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    goto :goto_1

    .line 364
    .end local v6    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :cond_4
    sget-object v6, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .restart local v6    # "newState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    goto :goto_1

    .line 370
    :cond_5
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateCardsViewTop(FZFLandroid/animation/Animator$AnimatorListener;Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    goto :goto_2
.end method

.method public setCallbacks(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCallbacks:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;

    .line 205
    return-void
.end method

.method public setCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Z
    .locals 5
    .param p1, "state"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .prologue
    const/4 v1, 0x0

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mAnimationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;

    iget-object v0, v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$AnimationState;->destinationState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 225
    .local v0, "eventualState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :goto_0
    if-eq p1, v0, :cond_1

    .line 226
    const-string v2, "ReaderLayout"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 227
    const-string v2, "ReaderLayout"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Animating to cards state "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->animateToCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;ZF)V

    .line 230
    const/4 v1, 0x1

    .line 232
    :cond_1
    return v1

    .line 223
    .end local v0    # "eventualState":Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardsState:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    goto :goto_0
.end method

.method public setOnLayoutChangeListener(Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mLayoutChangeListener:Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;

    .line 209
    return-void
.end method

.method public setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/google/android/ublib/view/TranslationHelper;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V

    .line 214
    return-void
.end method
