.class public Lcom/google/android/apps/books/model/VolumeMetadataImpl;
.super Ljava/lang/Object;
.source "VolumeMetadataImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/VolumeMetadata;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;,
        Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mCachedPositionComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedTextLocationComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation
.end field

.field private mCssResourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultCss:Ljava/lang/String;

.field private mFlvers:I

.field private mHasReadableSections:Ljava/lang/Boolean;

.field private mHasViewableSegments:Z

.field private mIsInMyEBooksCollection:Z

.field private mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

.field private mLocale:Ljava/util/Locale;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

.field protected mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

.field private mOverrideCss:Ljava/lang/String;

.field private mResourceCollector:Lcom/google/android/apps/books/render/ResourceCollector;

.field private final mResourceIdToResource:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private mSharedFonts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private mShouldSubstringSearch:Z

.field protected mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

.field private mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/apps/books/util/Logger;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mFlvers:I

    .line 133
    iput-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedTextLocationComparator:Ljava/util/Comparator;

    .line 135
    iput-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedPositionComparator:Ljava/util/Comparator;

    .line 137
    iput-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    .line 158
    new-instance v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl$1;-><init>(Lcom/google/android/apps/books/model/VolumeMetadataImpl;)V

    iput-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mResourceIdToResource:Lcom/google/common/base/Function;

    .line 748
    iput-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mHasReadableSections:Ljava/lang/Boolean;

    .line 171
    const-string v0, "missing volumeid"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    .line 172
    const-string v0, "missing account"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mAccount:Landroid/accounts/Account;

    .line 173
    iput-object p3, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 174
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/VolumeMetadataImpl;)Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/VolumeMetadataImpl;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/model/VolumeMetadataImpl;)Lcom/google/android/apps/books/render/ResourceCollector;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/VolumeMetadataImpl;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mResourceCollector:Lcom/google/android/apps/books/render/ResourceCollector;

    return-object v0
.end method

.method private static addFontDataToJsonArray(Lorg/json/JSONArray;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "jsonArray"    # Lorg/json/JSONArray;
    .param p1, "isDefault"    # Z
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "overlay"    # Ljava/lang/String;

    .prologue
    .line 911
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 913
    .local v1, "jsonFontInfo":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "isDefault"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 914
    const-string v2, "url"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 915
    const-string v2, "overlay"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 916
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 922
    :cond_0
    :goto_0
    return-void

    .line 917
    :catch_0
    move-exception v0

    .line 918
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "VolumeMetadata"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 919
    const-string v2, "VolumeMetadata"

    const-string v3, "shared font exception"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private computeHasReadableSections()Z
    .locals 3

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Segment;

    .line 759
    .local v1, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 760
    const/4 v2, 0x1

    .line 763
    .end local v1    # "segment":Lcom/google/android/apps/books/model/Segment;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getChapter(I)Lcom/google/android/apps/books/model/Chapter;
    .locals 4
    .param p1, "chapterIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1243
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapters()Ljava/util/List;

    move-result-object v0

    .line 1244
    .local v0, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    if-ltz p1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1245
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Chapter;

    return-object v1

    .line 1247
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad chapter index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getChapterStartPosition(Lcom/google/android/apps/books/model/Chapter;)Ljava/lang/String;
    .locals 3
    .param p1, "chapter"    # Lcom/google/android/apps/books/model/Chapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 727
    invoke-interface {p1}, Lcom/google/android/apps/books/model/Chapter;->getReadingPosition()Ljava/lang/String;

    move-result-object v0

    .line 728
    .local v0, "chapterPosition":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 739
    .end local v0    # "chapterPosition":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 732
    .restart local v0    # "chapterPosition":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/Chapter;->getStartSegmentIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegment(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 733
    :catch_0
    move-exception v1

    .line 739
    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    invoke-interface {p1}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(I)Lcom/google/android/apps/books/model/Page;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getPage(I)Lcom/google/android/apps/books/model/Page;
    .locals 4
    .param p1, "pageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1235
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPages()Ljava/util/List;

    move-result-object v0

    .line 1236
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    if-ltz p1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1237
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Page;

    return-object v1

    .line 1239
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad page index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getSegment(I)Lcom/google/android/apps/books/model/Segment;
    .locals 4
    .param p1, "segmentIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1251
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v0

    .line 1252
    .local v0, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    if-ltz p1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1253
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Segment;

    return-object v1

    .line 1255
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad segment index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private isAnyPageViewable(I)Z
    .locals 8
    .param p1, "chapterIndex"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 830
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapter(I)Lcom/google/android/apps/books/model/Chapter;

    move-result-object v0

    .line 831
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v4

    .line 832
    .local v4, "startPageIndex":I
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(I)Lcom/google/android/apps/books/model/Page;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 847
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v4    # "startPageIndex":I
    :cond_0
    :goto_0
    return v5

    .line 837
    .restart local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .restart local v4    # "startPageIndex":I
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v7, v4}, Lcom/google/android/apps/books/model/TextContentMetadata;->getNextPageIndexInDifferentChapter(I)I

    move-result v2

    .line 839
    .local v2, "nextPageIndexInDifferentChapter":I
    add-int/lit8 v3, v4, 0x1

    .local v3, "pageIndex":I
    :goto_1
    if-ge v3, v2, :cond_2

    .line 841
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(I)Lcom/google/android/apps/books/model/Page;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Page;->isViewable()Z
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_0

    .line 840
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v5, v6

    .line 845
    goto :goto_0

    .line 846
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v2    # "nextPageIndexInDifferentChapter":I
    .end local v3    # "pageIndex":I
    .end local v4    # "startPageIndex":I
    :catch_0
    move-exception v1

    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    move v5, v6

    .line 847
    goto :goto_0
.end method

.method private isAnySegmentViewable(I)Z
    .locals 8
    .param p1, "chapterIndex"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 807
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapter(I)Lcom/google/android/apps/books/model/Chapter;

    move-result-object v0

    .line 808
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartSegmentIndex()I

    move-result v4

    .line 809
    .local v4, "startSegmentIndex":I
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegment(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 824
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v4    # "startSegmentIndex":I
    :cond_0
    :goto_0
    return v5

    .line 814
    .restart local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .restart local v4    # "startSegmentIndex":I
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v7, v4}, Lcom/google/android/apps/books/model/TextContentMetadata;->getNextSegmentIndexInDifferentChapter(I)I

    move-result v2

    .line 816
    .local v2, "nextSegmentIndexInDifferentChapter":I
    add-int/lit8 v3, v4, 0x1

    .local v3, "segmentIndex":I
    :goto_1
    if-ge v3, v2, :cond_2

    .line 818
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegment(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_0

    .line 817
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v5, v6

    .line 822
    goto :goto_0

    .line 823
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v2    # "nextSegmentIndexInDifferentChapter":I
    .end local v3    # "segmentIndex":I
    .end local v4    # "startSegmentIndex":I
    :catch_0
    move-exception v1

    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    move v5, v6

    .line 824
    goto :goto_0
.end method

.method private populateFrom(ZZZLcom/google/android/apps/books/model/AssetDirectory;Lcom/google/android/apps/books/model/BooksDataStore;ILcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;)Z
    .locals 30
    .param p1, "populateVolumeState"    # Z
    .param p2, "updateVolumeOverview"    # Z
    .param p3, "checkShelfMembership"    # Z
    .param p4, "assetFactory"    # Lcom/google/android/apps/books/model/AssetDirectory;
    .param p5, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p6, "maxSegmentsPerPassage"    # I
    .param p7, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p8, "myEbooksChecker"    # Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 240
    const-string v4, "missing fetchService"

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v5, "VolumeMetadata#populateFrom"

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v28

    .line 249
    .local v28, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v7, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object/from16 v0, p7

    move/from16 v1, p2

    invoke-static {v0, v4, v1, v5, v7}, Lcom/google/android/apps/books/data/DataControllerUtils;->getVolumeData(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    .line 251
    const-string v4, "#getVolumeOverview"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 255
    const/4 v6, 0x0

    .line 261
    .local v6, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v4, Lcom/google/android/apps/books/render/ResourceCollector;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/render/ResourceCollector;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mResourceCollector:Lcom/google/android/apps/books/render/ResourceCollector;

    .line 262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mResourceCollector:Lcom/google/android/apps/books/render/ResourceCollector;

    move-object/from16 v0, p7

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/data/BooksDataController;->weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object/from16 v4, p7

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/books/data/DataControllerUtils;->getVolumeManifest(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Ljava/util/Set;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    move-result-object v17

    .line 266
    .local v17, "manifestResponse":Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    .line 268
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 273
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Manifest has no pages"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 276
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getIdToIndex()Ljava/util/Map;

    move-result-object v20

    .line 277
    .local v20, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getIdToIndex()Ljava/util/Map;

    move-result-object v25

    .line 279
    .local v25, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v4, "#getVolumeManifest"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 281
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v11

    .line 282
    .local v11, "cssResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v27

    .line 283
    .local v27, "smilResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    .line 284
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/IdentifiableCollection;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/apps/books/model/Resource;

    .line 285
    .local v22, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v18

    .line 286
    .local v18, "mimeType":Ljava/lang/String;
    const-string v4, "text/css"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 287
    move-object/from16 v0, v22

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 288
    :cond_2
    const-string v4, "smil"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 289
    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 290
    :cond_3
    const-string v4, "application/vnd.ms-opentype"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "application/font-woff"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 292
    :cond_4
    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 293
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    move-object/from16 v0, v22

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 297
    .end local v18    # "mimeType":Ljava/lang/String;
    .end local v22    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_5
    const-string v4, "found CSS & SMIL & shared font resource IDs"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 305
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCssResourceList:Ljava/util/List;

    .line 308
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCssResourceList:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/apps/books/util/CollectionUtils;->buildIndexMapFromIdentifiables(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v29

    .line 311
    .local v29, "uniqueCssResIdIndexHash":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegmentResources()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isVertical()Z

    move-result v5

    move-object/from16 v0, v29

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/books/model/PublisherCssUtils;->buildSegmentIdToCssIndices(Ljava/util/Collection;Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v24

    .line 315
    .local v24, "segmentIdToCssIndices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/Integer;>;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move-object/from16 v2, v24

    move/from16 v3, p6

    invoke-static {v4, v0, v1, v2, v3}, Lcom/google/android/apps/books/model/TextContentMetadata;->from(Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;I)Lcom/google/android/apps/books/model/TextContentMetadata;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    .line 318
    const-string v4, "#buildTextContentMetadata"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 324
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v16

    .line 325
    .local v16, "language":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 326
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/books/model/Resource;

    .line 327
    .local v14, "font":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v14}, Lcom/google/android/apps/books/model/Resource;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 328
    invoke-interface {v14}, Lcom/google/android/apps/books/model/Resource;->getLanguage()Ljava/lang/String;

    move-result-object v16

    .line 333
    .end local v14    # "font":Lcom/google/android/apps/books/model/Resource;
    :cond_7
    const-string v4, "VolumeMetadata"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 334
    const-string v5, "VolumeMetadata"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "populateFromAfterEnsure: language is "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v16, :cond_10

    const-string v4, "(null)"

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_8
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/books/util/LanguageUtil;->stringToLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocale:Ljava/util/Locale;

    .line 338
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/books/util/LanguageUtil;->shouldSubstringSearch(Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mShouldSubstringSearch:Z

    .line 340
    const-string v4, "#loadVolumeOverview"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 343
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mFlvers:I

    .line 344
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_9
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/books/model/Segment;

    .line 345
    .local v23, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 350
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mHasViewableSegments:Z

    .line 351
    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/Segment;->getFixedLayoutVersion()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_9

    .line 352
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mFlvers:I

    .line 353
    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v4

    if-lez v4, :cond_9

    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v4

    if-lez v4, :cond_9

    .line 355
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mFlvers:I

    .line 363
    .end local v23    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_a
    const-string v4, "#findReadingMode"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 365
    if-eqz p1, :cond_11

    .line 366
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 367
    const-string v4, "#populateVolumeState"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 375
    :goto_2
    if-eqz p3, :cond_b

    .line 376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    move-object/from16 v0, p8

    invoke-interface {v0, v4, v5}, Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;->isInMyEbooks(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mIsInMyEBooksCollection:Z

    .line 377
    const-string v4, "#checkShelfMembership"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 380
    :cond_b
    const-string v4, "VolumeMetadata"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    .line 381
    .local v12, "debugLoggable":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->hasMediaOverlays()Z

    move-result v4

    if-eqz v4, :cond_18

    .line 382
    const-string v4, "VolumeMetadata"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 383
    const-string v4, "VolumeMetadata"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parsing "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " SMIL resources"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :cond_c
    if-eqz v12, :cond_d

    .line 386
    const-string v4, "VolumeMetadata"

    const-string v5, "find smil resources"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_d
    new-instance v4, Lcom/google/android/apps/books/util/MediaClips;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getIdToIndex()Ljava/util/Map;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/util/MediaClips;-><init>(Ljava/util/Map;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    .line 389
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_e
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/android/apps/books/model/Resource;

    .line 395
    .local v26, "smilResource":Lcom/google/android/apps/books/model/Resource;
    if-eqz v12, :cond_f

    .line 396
    :try_start_0
    const-string v4, "VolumeMetadata"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "adding smil resource "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v26 .. v26}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    move-object/from16 v0, p7

    move-object/from16 v1, v26

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 401
    .local v10, "content":Ljava/io/InputStream;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    invoke-virtual {v4, v10}, Lcom/google/android/apps/books/util/MediaClips;->addSmil(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403
    :try_start_2
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 405
    .end local v10    # "content":Ljava/io/InputStream;
    :catch_0
    move-exception v13

    .line 406
    .local v13, "e":Ljava/io/IOException;
    const-string v4, "VolumeMetadata"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 407
    const-string v4, "VolumeMetadata"

    const-string v5, "failed to access smil resource"

    invoke-static {v4, v5, v13}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .end local v12    # "debugLoggable":Z
    .end local v13    # "e":Ljava/io/IOException;
    .end local v26    # "smilResource":Lcom/google/android/apps/books/model/Resource;
    :cond_10
    move-object/from16 v4, v16

    .line 334
    goto/16 :goto_1

    .line 369
    :cond_11
    sget-object v4, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->DEFAULT:Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    goto/16 :goto_2

    .line 403
    .restart local v10    # "content":Ljava/io/InputStream;
    .restart local v12    # "debugLoggable":Z
    .restart local v26    # "smilResource":Lcom/google/android/apps/books/model/Resource;
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V

    throw v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 411
    .end local v10    # "content":Ljava/io/InputStream;
    .end local v26    # "smilResource":Lcom/google/android/apps/books/model/Resource;
    :cond_12
    const-string v4, "#loadSmilContents"

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 412
    if-eqz v12, :cond_13

    .line 413
    const-string v4, "VolumeMetadata"

    const-string v5, "loaded smil resources"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/MediaClips;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_14

    const-string v4, "VolumeMetadata"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 417
    const-string v4, "VolumeMetadata"

    const-string v5, "MO volume, but no clips found"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_14
    :goto_4
    const-string v4, ""

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mDefaultCss:Ljava/lang/String;

    .line 428
    const-string v4, ""

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mOverrideCss:Ljava/lang/String;

    .line 429
    if-eqz p1, :cond_16

    .line 430
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 431
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 432
    const-string v4, "style.css"

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->readAsset(Ljava/lang/String;Lcom/google/android/apps/books/model/AssetDirectory;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mDefaultCss:Ljava/lang/String;

    .line 434
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v4

    if-eqz v4, :cond_19

    .line 435
    const-string v4, "fixed_override.css"

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->readAsset(Ljava/lang/String;Lcom/google/android/apps/books/model/AssetDirectory;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mOverrideCss:Ljava/lang/String;

    .line 441
    :cond_16
    :goto_5
    const-string v4, "VolumeMetadata"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 442
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getBookPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v21

    .line 444
    .local v21, "preferredMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-object/from16 v0, v21

    if-ne v0, v4, :cond_1a

    .line 445
    const-string v19, "Fixed"

    .line 452
    .local v19, "mode":Ljava/lang/String;
    :goto_6
    const-string v4, "VolumeMetadata"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "## Volume:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " # Mode:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " # UniqueCss:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " # SharedFonts:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ##"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    .end local v19    # "mode":Ljava/lang/String;
    .end local v21    # "preferredMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_17
    invoke-interface/range {v28 .. v28}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 461
    move-object/from16 v0, v17

    iget-boolean v4, v0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->fromServer:Z

    return v4

    .line 420
    :cond_18
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    .line 421
    if-eqz v12, :cond_14

    .line 422
    const-string v4, "VolumeMetadata"

    const-string v5, "NO smil resources"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 437
    :cond_19
    const-string v4, "override.css"

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v4, v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->readAsset(Ljava/lang/String;Lcom/google/android/apps/books/model/AssetDirectory;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mOverrideCss:Ljava/lang/String;

    goto/16 :goto_5

    .line 446
    .restart local v21    # "preferredMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_1a
    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-object/from16 v0, v21

    if-ne v0, v4, :cond_1c

    .line 447
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v4

    if-eqz v4, :cond_1b

    const-string v19, "Flowing/Image"

    .restart local v19    # "mode":Ljava/lang/String;
    :goto_7
    goto/16 :goto_6

    .end local v19    # "mode":Ljava/lang/String;
    :cond_1b
    const-string v19, "Flowing"

    goto :goto_7

    .line 449
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v4

    if-eqz v4, :cond_1d

    const-string v19, "Image/Flowing"

    .restart local v19    # "mode":Ljava/lang/String;
    :goto_8
    goto/16 :goto_6

    .end local v19    # "mode":Ljava/lang/String;
    :cond_1d
    const-string v19, "Image"

    goto :goto_8
.end method

.method public static productionMyEbooksChecker(Landroid/content/ContentResolver;)Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;
    .locals 1
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 181
    new-instance v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl$2;-><init>(Landroid/content/ContentResolver;)V

    return-object v0
.end method

.method private readAsset(Ljava/lang/String;Lcom/google/android/apps/books/model/AssetDirectory;)Ljava/lang/String;
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "assetFactory"    # Lcom/google/android/apps/books/model/AssetDirectory;

    .prologue
    .line 469
    const/4 v1, 0x0

    .line 471
    .local v1, "inStream":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p2, p1}, Lcom/google/android/apps/books/model/AssetDirectory;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 472
    new-instance v2, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478
    if-eqz v1, :cond_0

    .line 479
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 482
    :cond_0
    :goto_0
    return-object v2

    .line 473
    :catch_0
    move-exception v0

    .line 474
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v2, "VolumeMetadata"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 475
    const-string v2, "VolumeMetadata"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to read internal style set: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    :cond_1
    if-eqz v1, :cond_2

    .line 479
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    .line 482
    :cond_2
    const-string v2, ""

    goto :goto_0

    .line 478
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_3

    .line 479
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :cond_3
    throw v2
.end method


# virtual methods
.method public getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;
    .locals 1

    .prologue
    .line 1171
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v0

    return-object v0
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getBakedCssFilesJsonArray()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 863
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mDefaultCss:Ljava/lang/String;

    const-string v2, "missing mDefaultCss"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mOverrideCss:Ljava/lang/String;

    const-string v2, "missing mCssOverrides"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 866
    .local v0, "all":Lorg/json/JSONArray;
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mDefaultCss:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 867
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mOverrideCss:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 868
    return-object v0
.end method

.method public getBookPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 2

    .prologue
    .line 544
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeManifest;->getPreferredMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    .line 545
    .local v0, "bookPreferredMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v1

    if-nez v1, :cond_3

    .line 546
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 547
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 552
    :goto_0
    return-object v1

    .line 547
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    goto :goto_0

    .line 548
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 549
    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 552
    goto :goto_0
.end method

.method public getBuyUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
    .locals 3
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 527
    new-instance v0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/apps/books/util/OceanUris;->appendCampaignId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;-><init>(Ljava/lang/String;Z)V

    .line 532
    :goto_0
    return-object v0

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 530
    new-instance v0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 532
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    invoke-static {p1, v1, p2}, Lcom/google/android/apps/books/util/OceanUris;->getBuyTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;-><init>(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public getBuyUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCanonicalUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChapterCssJsonArray()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageCssJsonArray()Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public getChapterIndexForPageId(Ljava/lang/String;)I
    .locals 1
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getChapterIndexForPageId(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method

.method public getChapterSegmentJsonArray()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 853
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageSegmentJsonArray()Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public getChapterStartPosition(I)Lcom/google/android/apps/books/common/Position;
    .locals 2
    .param p1, "chapterIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 712
    new-instance v1, Lcom/google/android/apps/books/common/Position;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapterStartPosition(Lcom/google/android/apps/books/model/Chapter;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public getChapterTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 1068
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    .line 1069
    .local v0, "chapterIndex":I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapterTitleForChapterIndex(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getChapterTitleForChapterIndex(I)Ljava/lang/String;
    .locals 1
    .param p1, "chapterIndex"    # I

    .prologue
    .line 1074
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChapters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1156
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getContentVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCssResourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCssResourceList:Ljava/util/List;

    return-object v0
.end method

.method public getDefaultPosition()Lcom/google/android/apps/books/common/Position;
    .locals 5

    .prologue
    .line 512
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->getFirstChapterStartSegmentIndex()I

    move-result v1

    .line 513
    .local v1, "startSegmentIndex":I
    new-instance v2, Lcom/google/android/apps/books/common/Position;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegment(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    .end local v1    # "startSegmentIndex":I
    :goto_0
    return-object v2

    .line 514
    :catch_0
    move-exception v0

    .line 515
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    new-instance v3, Lcom/google/android/apps/books/common/Position;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPages()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/common/Position;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_0
.end method

.method public getEndOfBookBody(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/common/Position;
    .locals 6
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    const/4 v3, 0x0

    .line 1266
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne p1, v2, :cond_1

    .line 1267
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getImageModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v1

    .line 1273
    .local v1, "positions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    :goto_0
    if-nez v1, :cond_3

    move-object v0, v3

    .line 1282
    :cond_0
    :goto_1
    return-object v0

    .line 1268
    .end local v1    # "positions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    :cond_1
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne p1, v2, :cond_2

    .line 1269
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getTextModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v1

    .restart local v1    # "positions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    goto :goto_0

    .line 1271
    .end local v1    # "positions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "positions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    goto :goto_0

    .line 1277
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->getContentEnd()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/common/Position;->createPositionOrNull(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 1278
    .local v0, "contentEnd":Lcom/google/android/apps/books/common/Position;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPages()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPageCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move-object v0, v3

    .line 1280
    goto :goto_1
.end method

.method public getFirstForbiddenPassageIndex()I
    .locals 1

    .prologue
    .line 778
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isSample()Z

    move-result v0

    if-nez v0, :cond_0

    .line 779
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPassageCount()I

    move-result v0

    .line 782
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getFirstForbiddenPassageIndex()I

    move-result v0

    goto :goto_0
.end method

.method public getFirstSegmentForPassageIndex(I)Lcom/google/android/apps/books/model/Segment;
    .locals 5
    .param p1, "passageIndex"    # I

    .prologue
    .line 944
    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v1

    .line 946
    .local v1, "segmentIndices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 947
    const/4 v2, 0x0

    .line 951
    :goto_0
    return-object v2

    .line 949
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 950
    .local v0, "segmentIndex":I
    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 951
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Segment;

    goto :goto_0

    .line 953
    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad segment index in passage "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getFixedLayoutJsonArray()Lorg/json/JSONArray;
    .locals 5

    .prologue
    .line 873
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 874
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 875
    .local v2, "outer":Lorg/json/JSONArray;
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    .line 876
    .local v3, "segment":Lcom/google/android/apps/books/model/Segment;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 877
    .local v1, "inner":Lorg/json/JSONArray;
    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    .line 878
    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    .line 879
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 883
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "inner":Lorg/json/JSONArray;
    .end local v2    # "outer":Lorg/json/JSONArray;
    .end local v3    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2
.end method

.method public getImageModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .locals 1

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getImageModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v0

    return-object v0
.end method

.method public getLineHeight()F
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v0

    return v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getManifest()Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 1

    .prologue
    .line 1260
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    return-object v0
.end method

.method public getMediaClips()Lcom/google/android/apps/books/util/MediaClips;
    .locals 1

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    return-object v0
.end method

.method public getMediaOverlayActiveClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1195
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getMediaOverlayActiveClass()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModeForContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 3
    .param p1, "format"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    const/4 v0, 0x0

    .line 1134
    if-nez p1, :cond_1

    .line 1146
    :cond_0
    :goto_0
    return-object v0

    .line 1137
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1140
    sget-object v1, Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1144
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    goto :goto_0

    .line 1142
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    goto :goto_0

    .line 1140
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNumberOfSegments()I
    .locals 1

    .prologue
    .line 926
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 612
    invoke-virtual {p1}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;

    move-result-object v0

    return-object v0
.end method

.method public getPage(Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 607
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPages()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPageIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Page;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPageIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPageTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 1080
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1082
    :goto_0
    return-object v1

    .line 1081
    :catch_0
    move-exception v0

    .line 1082
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, ""

    goto :goto_0
.end method

.method public getPages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPassageCount()I
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageCount()I

    move-result v0

    return v0
.end method

.method public getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method

.method public getPassageIndexForSegmentId(Ljava/lang/String;)I
    .locals 1
    .param p1, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageIndexForSegmentId(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPassageIndexForSegmentIndex(I)I
    .locals 1
    .param p1, "segmentIndex"    # I

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageIndexForSegmentIndex(I)I

    move-result v0

    return v0
.end method

.method public getPassageSegmentIndices(I)Ljava/util/List;
    .locals 1
    .param p1, "passage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 994
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPassageSegments(I)Ljava/util/List;
    .locals 4
    .param p1, "passage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 999
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 1000
    .local v2, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1001
    .local v1, "segmentIndex":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1003
    .end local v1    # "segmentIndex":I
    :cond_0
    return-object v2
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 503
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    .line 505
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getDefaultPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    goto :goto_0
.end method

.method public getPositionAfter(I)Ljava/lang/String;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 1045
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPassageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1047
    const/4 v0, 0x0

    .line 1050
    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPositionBefore(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPositionBefore(I)Ljava/lang/String;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageStartPosition(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPositionComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation

    .prologue
    .line 978
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedPositionComparator:Ljava/util/Comparator;

    if-nez v0, :cond_0

    .line 979
    invoke-static {p0}, Lcom/google/android/apps/books/common/Position;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedPositionComparator:Ljava/util/Comparator;

    .line 981
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedPositionComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public getPositionRangeForPassage(I)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 5
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1056
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPositionBefore(I)Ljava/lang/String;

    move-result-object v1

    .line 1057
    .local v1, "positionBefore":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPositionAfter(I)Ljava/lang/String;

    move-result-object v0

    .line 1059
    .local v0, "positionAfter":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1060
    new-instance v2, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t find positions for passage index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1063
    :cond_0
    new-instance v2, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v2, v1, v3, v0, v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-object v2
.end method

.method public getResourceIdToResource()Lcom/google/common/base/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mResourceIdToResource:Lcom/google/common/base/Function;

    return-object v0
.end method

.method public getSegmentForPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Segment;
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 653
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;

    move-result-object v0

    .line 654
    .local v0, "index":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 655
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t find segment for position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 657
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegment(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v1

    return-object v1
.end method

.method public getSegmentIdAtIndex(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 938
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentIdToCssIndices()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIdToCssIndices()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
    .locals 2
    .param p1, "p"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 931
    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;

    move-result-object v0

    .line 933
    .local v0, "index":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getSegmentIndexForSegmentId(Ljava/lang/String;)I
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIndexForSegmentId(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getSegments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSharedFontsUrisPlus()Lorg/json/JSONArray;
    .locals 6

    .prologue
    .line 895
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 896
    .local v2, "result":Lorg/json/JSONArray;
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 897
    iget-object v3, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Resource;

    .line 898
    .local v0, "font":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Resource;->getIsDefault()Z

    move-result v3

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/provider/BooksContract$SharedResources;->buildWebUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Resource;->getOverlay()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->addFontDataToJsonArray(Lorg/json/JSONArray;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 906
    .end local v0    # "font":Lcom/google/android/apps/books/model/Resource;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v2
.end method

.method public getStartPageIndex(I)I
    .locals 2
    .param p1, "chapterIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapter(I)Lcom/google/android/apps/books/model/Chapter;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Chapter;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getStartPageIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getStartPageTitle(I)Ljava/lang/String;
    .locals 2
    .param p1, "chapterIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1230
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getStartPageIndex(I)I

    move-result v0

    .line 1231
    .local v0, "pageIndex":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(I)Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getTextLocationComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedTextLocationComparator:Ljava/util/Comparator;

    if-nez v0, :cond_0

    .line 987
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/TextLocation;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedTextLocationComparator:Ljava/util/Comparator;

    .line 989
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mCachedTextLocationComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public getTextModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->getTextModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v0

    return-object v0
.end method

.method public getTextZoom()F
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v0

    return v0
.end method

.method public getUserPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeAnnotationRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .locals 10
    .param p1, "segmentId"    # Ljava/lang/String;
    .param p2, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1009
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegments()Ljava/util/List;

    move-result-object v9

    .line 1010
    .local v9, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegmentIndexForSegmentId(Ljava/lang/String;)I

    move-result v5

    .line 1011
    .local v5, "segmentIndex":I
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/model/Segment;

    .line 1012
    .local v8, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v8}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v3

    .line 1014
    .local v3, "startPosition":Ljava/lang/String;
    add-int/lit8 v7, v5, 0x1

    .line 1016
    .local v7, "nextSegmentIndex":I
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 1017
    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/Segment;

    .line 1018
    .local v6, "nextSegment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v6}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v4

    .line 1023
    .end local v6    # "nextSegment":Lcom/google/android/apps/books/model/Segment;
    .local v4, "endPosition":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/books/annotations/Layer$Type;->VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-direct {v1, v0, v2, p2}, Lcom/google/android/apps/books/annotations/Layer$Key;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;)V

    .line 1024
    .local v1, "layer":Lcom/google/android/apps/books/annotations/Layer$Key;
    new-instance v0, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0

    .line 1020
    .end local v1    # "layer":Lcom/google/android/apps/books/annotations/Layer$Key;
    .end local v4    # "endPosition":Ljava/lang/String;
    :cond_0
    const/4 v4, 0x0

    .restart local v4    # "endPosition":Ljava/lang/String;
    goto :goto_0
.end method

.method public getVolumeData()Lcom/google/android/apps/books/model/VolumeData;
    .locals 1

    .prologue
    .line 1215
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    return-object v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 3

    .prologue
    .line 973
    new-instance v0, Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    .locals 4
    .param p1, "format"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1121
    if-nez p1, :cond_0

    .line 1128
    :goto_0
    return v1

    .line 1124
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1126
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 1127
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mHasViewableSegments:Z

    if-eqz v2, :cond_2

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    .line 1124
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hasMediaClips()Z
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mMediaClips:Lcom/google/android/apps/books/util/MediaClips;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaClips;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Z
    .locals 4
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1108
    if-nez p1, :cond_0

    .line 1115
    :goto_0
    return v1

    .line 1111
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$Mode:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1112
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v1

    goto :goto_0

    .line 1113
    :pswitch_1
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 1114
    :pswitch_2
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAppleFixedLayout()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    .line 1111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public hasNeverBeenOpened()Z
    .locals 2

    .prologue
    .line 1220
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->NONE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    iget-object v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasOfflineLicense()Z
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v0

    return v0
.end method

.method public hasReadableSections()Z
    .locals 1

    .prologue
    .line 751
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mHasReadableSections:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 752
    invoke-direct {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->computeHasReadableSections()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mHasReadableSections:Ljava/lang/Boolean;

    .line 754
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mHasReadableSections:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isAppleFixedLayout()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 768
    iget v1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mFlvers:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChapterViewable(ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    .locals 2
    .param p1, "chapterIndex"    # I
    .param p2, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 787
    sget-object v0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 793
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 789
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAnySegmentViewable(I)Z

    move-result v0

    goto :goto_0

    .line 791
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isAnyPageViewable(I)Z

    move-result v0

    goto :goto_0

    .line 787
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isFitWidth()Z
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mLocalVolumeData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v0

    return v0
.end method

.method public isInMyEBooksCollection()Z
    .locals 1

    .prologue
    .line 582
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mIsInMyEBooksCollection:Z

    return v0
.end method

.method public isPageEnabled(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    .locals 3
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 624
    sget-object v1, Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 632
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 626
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v1

    goto :goto_0

    .line 628
    :pswitch_1
    invoke-static {p1}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    .line 629
    .local v0, "chapterIndex":I
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isChapterViewable(ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v1

    goto :goto_0

    .line 624
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isPassageForbidden(I)Z
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mTextContentMetadata:Lcom/google/android/apps/books/model/TextContentMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->isPassageViewable(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPositionEnabled(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z
    .locals 3
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 639
    sget-object v1, Lcom/google/android/apps/books/model/VolumeMetadataImpl$3;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$ContentFormat:[I

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 647
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 641
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v1

    goto :goto_0

    .line 643
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->getSegmentForPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Segment;

    move-result-object v0

    .line 644
    .local v0, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v1

    goto :goto_0

    .line 639
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isRightToLeft()Z
    .locals 1

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mManifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->isRightToLeft()Z

    move-result v0

    return v0
.end method

.method public isSample()Z
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v0

    return v0
.end method

.method public isUploaded()Z
    .locals 1

    .prologue
    .line 1210
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mVolumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v0

    return v0
.end method

.method public isVertical()Z
    .locals 1

    .prologue
    .line 1185
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->isRightToLeft()Z

    move-result v0

    return v0
.end method

.method public populateFrom(ZZLcom/google/android/apps/books/model/AssetDirectory;Lcom/google/android/apps/books/model/BooksDataStore;ILcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;)Z
    .locals 10
    .param p1, "updateVolumeOverview"    # Z
    .param p2, "checkShelfMembership"    # Z
    .param p3, "assetFactory"    # Lcom/google/android/apps/books/model/AssetDirectory;
    .param p4, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p5, "maxSegmentsPerPassage"    # I
    .param p6, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p7, "myEbooksChecker"    # Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 224
    const/4 v9, 0x1

    .line 225
    .local v9, "populateVolumeState":Z
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->populateFrom(ZZZLcom/google/android/apps/books/model/AssetDirectory;Lcom/google/android/apps/books/model/BooksDataStore;ILcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;)Z

    move-result v0

    return v0
.end method

.method public populateFrom(ZZLcom/google/android/apps/books/model/AssetDirectory;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;)Z
    .locals 8
    .param p1, "updateVolumeOverview"    # Z
    .param p2, "checkShelfMembership"    # Z
    .param p3, "assetFactory"    # Lcom/google/android/apps/books/model/AssetDirectory;
    .param p4, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p6, "myEbooksChecker"    # Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 213
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->populateFrom(ZZLcom/google/android/apps/books/model/AssetDirectory;Lcom/google/android/apps/books/model/BooksDataStore;ILcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;)Z

    move-result v0

    return v0
.end method

.method public shouldSubstringSearch()Z
    .locals 1

    .prologue
    .line 1190
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mShouldSubstringSearch:Z

    return v0
.end method

.method public useMinimalFontSet()Z
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl;->mSharedFonts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
