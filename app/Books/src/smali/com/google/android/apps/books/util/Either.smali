.class public Lcom/google/android/apps/books/util/Either;
.super Ljava/lang/Object;
.source "Either.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final isLeft:Z

.field public final left:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field public final right:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TB;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .param p3, "isLeft"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;TB;Z)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/google/android/apps/books/util/Either;, "Lcom/google/android/apps/books/util/Either<TA;TB;>;"
    .local p1, "left":Ljava/lang/Object;, "TA;"
    .local p2, "right":Ljava/lang/Object;, "TB;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    .line 20
    iput-object p2, p0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    .line 21
    iput-boolean p3, p0, Lcom/google/android/apps/books/util/Either;->isLeft:Z

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/Either;, "Lcom/google/android/apps/books/util/Either<TA;TB;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p0, p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 62
    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 64
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 65
    check-cast v0, Lcom/google/android/apps/books/util/Either;

    .line 66
    .local v0, "other":Lcom/google/android/apps/books/util/Either;, "Lcom/google/android/apps/books/util/Either<**>;"
    iget-boolean v3, p0, Lcom/google/android/apps/books/util/Either;->isLeft:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/util/Either;->isLeft:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 67
    goto :goto_0

    .line 68
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    if-nez v3, :cond_5

    .line 69
    iget-object v3, v0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    if-eqz v3, :cond_6

    move v1, v2

    .line 70
    goto :goto_0

    .line 71
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 72
    goto :goto_0

    .line 73
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    if-nez v3, :cond_7

    .line 74
    iget-object v3, v0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    if-eqz v3, :cond_0

    move v1, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 77
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/Either;, "Lcom/google/android/apps/books/util/Either<TA;TB;>;"
    const/4 v3, 0x0

    .line 49
    const/16 v0, 0x1f

    .line 50
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 51
    .local v1, "result":I
    iget-boolean v2, p0, Lcom/google/android/apps/books/util/Either;->isLeft:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 52
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 53
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 54
    return v1

    .line 51
    :cond_0
    const/16 v2, 0x4d5

    goto :goto_0

    .line 52
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    .line 53
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/apps/books/util/Either;, "Lcom/google/android/apps/books/util/Either<TA;TB;>;"
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 39
    .local v0, "helper":Lcom/google/common/base/Objects$ToStringHelper;
    iget-boolean v1, p0, Lcom/google/android/apps/books/util/Either;->isLeft:Z

    if-eqz v1, :cond_0

    .line 40
    const-string v1, "left"

    iget-object v2, p0, Lcom/google/android/apps/books/util/Either;->left:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    .line 44
    :goto_0
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 42
    :cond_0
    const-string v1, "right"

    iget-object v2, p0, Lcom/google/android/apps/books/util/Either;->right:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    goto :goto_0
.end method
