.class public interface abstract Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;
.super Ljava/lang/Object;
.source "AnnotationServerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationServerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AnnotationDataProcessor"
.end annotation


# virtual methods
.method public abstract onAnnotationDatas(Ljava/util/Map;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lorg/codehaus/jackson/JsonNode;",
            ">;)V"
        }
    .end annotation
.end method
