.class Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;
.super Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;
.source "HtmlSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/HtmlSpreadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDecorationsGroup"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;ZLcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "canHaveBookmarksInBothCorners"    # Z
    .param p4, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .line 338
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;-><init>(Landroid/content/Context;ZLcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    .line 339
    return-void
.end method


# virtual methods
.method protected getBookmarkMargins(ILandroid/graphics/Point;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "result"    # Landroid/graphics/Point;

    .prologue
    .line 367
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$600(Lcom/google/android/apps/books/widget/HtmlSpreadView;)[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v1

    aget-object v0, v1, p1

    .line 368
    .local v0, "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->access$700(Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->access$700(Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {p2, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 369
    return-void
.end method

.method public onBookmarkChanged(I)V
    .locals 2
    .param p1, "screenOffset"    # I

    .prologue
    .line 342
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # invokes: Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPageData(I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$400(Lcom/google/android/apps/books/widget/HtmlSpreadView;I)Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->bookmarkViewIndex(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)I

    move-result v0

    .line 343
    .local v0, "index":I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->onBookmarkStateChanged(I)V

    .line 344
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 348
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;->onLayout(ZIIII)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->getMeasuredWidth()I

    move-result v5

    .line 351
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->getMeasuredHeight()I

    move-result v1

    .line 352
    .local v1, "height":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/HtmlSpreadView;->mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    invoke-virtual {v6, v5, v1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setContainerSize(II)V

    .line 354
    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # invokes: Lcom/google/android/apps/books/widget/HtmlSpreadView;->getPagesCount()I
    invoke-static {v7}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$500(Lcom/google/android/apps/books/widget/HtmlSpreadView;)I

    move-result v7

    div-int v7, v5, v7

    iput v7, v6, Landroid/graphics/Point;->x:I

    .line 355
    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v6

    iput v1, v6, Landroid/graphics/Point;->y:I

    .line 357
    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    invoke-static {v6}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$600(Lcom/google/android/apps/books/widget/HtmlSpreadView;)[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 358
    .local v4, "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->updatePageTransform()V

    .line 357
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 361
    .end local v4    # "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # invokes: Lcom/google/android/apps/books/widget/HtmlSpreadView;->updateSpreadContentRect()V
    invoke-static {v6}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$200(Lcom/google/android/apps/books/widget/HtmlSpreadView;)V

    .line 362
    return-void
.end method
