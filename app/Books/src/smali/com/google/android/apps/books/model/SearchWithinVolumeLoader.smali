.class public abstract Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;
.super Ljava/lang/Object;
.source "SearchWithinVolumeLoader.java"


# instance fields
.field private final mQuery:Ljava/lang/String;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "missing volumeId"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->mVolumeId:Ljava/lang/String;

    .line 29
    const-string v0, "query"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->mQuery:Ljava/lang/String;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/SearchResultListener;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->loadInBackground(Lcom/google/android/apps/books/model/SearchResultListener;)V

    return-void
.end method

.method private loadInBackground(Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 10
    .param p1, "listener"    # Lcom/google/android/apps/books/model/SearchResultListener;

    .prologue
    const/4 v9, 0x3

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 58
    .local v2, "startTime":J
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->loadInBackgroundTimed(Lcom/google/android/apps/books/model/SearchResultListener;)V

    .line 59
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v2

    .line 61
    .local v0, "millis":J
    const-string v4, "SWVLoader"

    invoke-static {v4, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    const-string v4, "SWVLoader"

    const-string v5, "%s search for %s in %s: %sms"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->getSearchType()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->getQuery()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->getVolumeId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    return-void
.end method

.method private loadTask(Lcom/google/android/apps/books/model/SearchResultListener;)Landroid/os/AsyncTask;
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/SearchResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SearchResultListener;",
            ")",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader$1;-><init>(Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;Lcom/google/android/apps/books/model/SearchResultListener;)V

    return-object v0
.end method


# virtual methods
.method protected getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getSearchType()Ljava/lang/String;
.end method

.method protected getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public load(Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/apps/books/model/SearchResultListener;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;->loadTask(Lcom/google/android/apps/books/model/SearchResultListener;)Landroid/os/AsyncTask;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method protected abstract loadInBackgroundTimed(Lcom/google/android/apps/books/model/SearchResultListener;)V
.end method
