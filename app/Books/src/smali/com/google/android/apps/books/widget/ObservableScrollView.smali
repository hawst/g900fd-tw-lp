.class public Lcom/google/android/apps/books/widget/ObservableScrollView;
.super Landroid/widget/ScrollView;
.source "ObservableScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;
    }
.end annotation


# instance fields
.field private mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ObservableScrollView;->mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ObservableScrollView;->mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ObservableScrollView;->mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    .line 28
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "oldx"    # I
    .param p4, "oldy"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ObservableScrollView;->mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ObservableScrollView;->mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;->onScrollChanged(Lcom/google/android/apps/books/widget/ObservableScrollView;IIII)V

    .line 44
    :cond_0
    return-void
.end method

.method public setOnScrollListener(Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ObservableScrollView;->mOnScrollListener:Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;

    .line 36
    return-void
.end method
