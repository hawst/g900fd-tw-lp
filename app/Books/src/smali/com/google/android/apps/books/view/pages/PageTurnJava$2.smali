.class Lcom/google/android/apps/books/view/pages/PageTurnJava$2;
.super Ljava/lang/Object;
.source "PageTurnJava.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/view/pages/PageTurnJava;->setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

.field final synthetic val$direction:Lcom/google/android/apps/books/util/ScreenDirection;

.field final synthetic val$fraction:F

.field final synthetic val$movingToNewPosition:Z

.field final synthetic val$slot:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;IFLcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    iput p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$slot:I

    iput p3, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$fraction:F

    iput-object p4, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$direction:Lcom/google/android/apps/books/util/ScreenDirection;

    iput-boolean p5, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$movingToNewPosition:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    # getter for: Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;
    invoke-static {v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$000(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnScene;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$slot:I

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$fraction:F

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$direction:Lcom/google/android/apps/books/util/ScreenDirection;

    iget-boolean v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$2;->val$movingToNewPosition:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 172
    return-void
.end method
