.class public Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;
.super Ljava/lang/Object;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RecommendationLoader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;
    }
.end annotation


# instance fields
.field private final mConsumer:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;

.field protected mRecommendations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end field

.field private mRecommendationsEnabledConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1190
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191
    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mConsumer:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;

    .line 1192
    invoke-static {p2}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 1193
    .local v0, "app":Lcom/google/android/apps/books/app/BooksApplication;
    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksApplication;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendationsEnabledConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 1207
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendationsEnabledConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v1}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/RecommendationsUtil;->loadShouldShowRecommendations(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1209
    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;)Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mConsumer:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;

    return-object v0
.end method


# virtual methods
.method public mayRequireProvider()Z
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProvider(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .prologue
    .line 1212
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mConsumer:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->setProvider(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)V

    .line 1213
    return-void
.end method
