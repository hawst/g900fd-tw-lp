.class Lcom/google/android/apps/books/app/InfoCardsHelper$1;
.super Ljava/lang/Object;
.source "InfoCardsHelper.java"

# interfaces
.implements Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/InfoCardsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/InfoCardsHelper;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewsDismissed(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # getter for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$000(Lcom/google/android/apps/books/app/InfoCardsHelper;)Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    # -= operator for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$120(Lcom/google/android/apps/books/app/InfoCardsHelper;I)I

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # getter for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$200(Lcom/google/android/apps/books/app/InfoCardsHelper;)Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # getter for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I
    invoke-static {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$100(Lcom/google/android/apps/books/app/InfoCardsHelper;)I

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # getter for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$200(Lcom/google/android/apps/books/app/InfoCardsHelper;)Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;->onAllCardsDismissed()V

    .line 122
    :cond_0
    return-void
.end method
