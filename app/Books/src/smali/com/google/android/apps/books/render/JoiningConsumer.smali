.class public abstract Lcom/google/android/apps/books/render/JoiningConsumer;
.super Ljava/lang/Object;
.source "JoiningConsumer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final firstConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<TA;>;"
        }
    .end annotation
.end field

.field private mFirstIsStored:Z

.field private mFirstValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field private mSecondIsStored:Z

.field private mSecondValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TB;"
        }
    .end annotation
.end field

.field public final secondConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<TB;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/render/JoiningConsumer;, "Lcom/google/android/apps/books/render/JoiningConsumer<TA;TB;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mFirstValue:Ljava/lang/Object;

    .line 12
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mFirstIsStored:Z

    .line 13
    iput-object v1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mSecondValue:Ljava/lang/Object;

    .line 14
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mSecondIsStored:Z

    .line 16
    new-instance v0, Lcom/google/android/apps/books/render/JoiningConsumer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/JoiningConsumer$1;-><init>(Lcom/google/android/apps/books/render/JoiningConsumer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->firstConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 25
    new-instance v0, Lcom/google/android/apps/books/render/JoiningConsumer$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/JoiningConsumer$2;-><init>(Lcom/google/android/apps/books/render/JoiningConsumer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->secondConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/render/JoiningConsumer;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/JoiningConsumer;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 10
    iput-object p1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mFirstValue:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/render/JoiningConsumer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/JoiningConsumer;
    .param p1, "x1"    # Z

    .prologue
    .line 10
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mFirstIsStored:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/JoiningConsumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/JoiningConsumer;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/apps/books/render/JoiningConsumer;->maybeTakeBoth()V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/render/JoiningConsumer;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/JoiningConsumer;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 10
    iput-object p1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mSecondValue:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/render/JoiningConsumer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/JoiningConsumer;
    .param p1, "x1"    # Z

    .prologue
    .line 10
    iput-boolean p1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mSecondIsStored:Z

    return p1
.end method

.method private maybeTakeBoth()V
    .locals 2

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/apps/books/render/JoiningConsumer;, "Lcom/google/android/apps/books/render/JoiningConsumer<TA;TB;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mFirstIsStored:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mSecondIsStored:Z

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mFirstValue:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/books/render/JoiningConsumer;->mSecondValue:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/render/JoiningConsumer;->take(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract take(Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;TB;)V"
        }
    .end annotation
.end method
