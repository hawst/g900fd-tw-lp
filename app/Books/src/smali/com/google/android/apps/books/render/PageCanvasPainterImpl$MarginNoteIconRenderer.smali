.class Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
.super Ljava/lang/Object;
.source "PageCanvasPainterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/PageCanvasPainterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MarginNoteIconRenderer"
.end annotation


# instance fields
.field private final mBackgroundPaints:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field private final mBounds:Landroid/graphics/Rect;

.field private final mDogEarDrawable:Landroid/graphics/drawable/ShapeDrawable;

.field private final mFillPaint:Landroid/graphics/Paint;

.field private final mIntrinsicHeight:I

.field private final mIntrinsicWidth:I

.field private final mMinimumHeight:I

.field private final mMinimumWidth:I

.field private final mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

.field private final mOutlinePaint:Landroid/graphics/Paint;

.field private mPadding:I

.field private mTheme:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/render/PageCanvasPainterImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/PageCanvasPainterImpl;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 59
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->this$0:Lcom/google/android/apps/books/render/PageCanvasPainterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBackgroundPaints:Ljava/util/Map;

    .line 60
    # getter for: Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->access$000(Lcom/google/android/apps/books/render/PageCanvasPainterImpl;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 66
    .local v5, "r":Landroid/content/res/Resources;
    const v6, 0x7f090157

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    .line 67
    const v6, 0x7f090158

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    .line 68
    const v6, 0x7f090159

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mMinimumWidth:I

    .line 69
    const v6, 0x7f09015a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mMinimumHeight:I

    .line 70
    iput v9, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    .line 72
    new-instance v6, Landroid/graphics/Rect;

    iget v7, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    iget v8, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    invoke-direct {v6, v9, v9, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    .line 74
    const v6, 0x7f09015b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 75
    .local v2, "dogearSize":I
    const v6, 0x7f09015c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 78
    .local v4, "outlineWidth":I
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 79
    .local v3, "notePath":Landroid/graphics/Path;
    invoke-virtual {v3, v10, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 80
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    sub-int/2addr v6, v2

    int-to-float v6, v6

    invoke-virtual {v3, v6, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    int-to-float v6, v6

    int-to-float v7, v2

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 82
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    int-to-float v7, v7

    invoke-virtual {v3, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    int-to-float v6, v6

    invoke-virtual {v3, v10, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 84
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 85
    new-instance v6, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v7, Landroid/graphics/drawable/shapes/PathShape;

    iget v8, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    int-to-float v8, v8

    iget v9, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    int-to-float v9, v9

    invoke-direct {v7, v3, v8, v9}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v6, v7}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    .line 88
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mFillPaint:Landroid/graphics/Paint;

    .line 89
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mFillPaint:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 91
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    .line 92
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    int-to-float v7, v4

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 93
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->computeOutlineColor()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 95
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    iget-object v7, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->getFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setFlags(I)V

    .line 97
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 98
    .local v1, "dogEarPath":Landroid/graphics/Path;
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    sub-int/2addr v6, v2

    int-to-float v6, v6

    invoke-virtual {v1, v6, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 99
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    sub-int/2addr v6, v2

    int-to-float v6, v6

    int-to-float v7, v2

    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 100
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    int-to-float v6, v6

    int-to-float v7, v2

    invoke-virtual {v1, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 101
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 102
    new-instance v6, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v7, Landroid/graphics/drawable/shapes/PathShape;

    iget v8, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    int-to-float v8, v8

    iget v9, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    int-to-float v9, v9

    invoke-direct {v7, v1, v8, v9}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v6, v7}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mDogEarDrawable:Landroid/graphics/drawable/ShapeDrawable;

    .line 104
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mDogEarDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 105
    .local v0, "dogEarPaint":Landroid/graphics/Paint;
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    invoke-virtual {v0}, Landroid/graphics/Paint;->getFlags()I

    move-result v6

    or-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFlags(I)V

    .line 108
    return-void
.end method

.method private computeOutlineColor()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    const/4 v3, 0x1

    new-array v0, v3, [I

    const v3, 0x7f01018f

    aput v3, v0, v4

    .line 116
    .local v0, "attributeIds":[I
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->this$0:Lcom/google/android/apps/books/render/PageCanvasPainterImpl;

    # getter for: Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->access$000(Lcom/google/android/apps/books/render/PageCanvasPainterImpl;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 118
    .local v1, "attributes":Landroid/content/res/TypedArray;
    const v3, -0x777778

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 119
    .local v2, "color":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 120
    return v2
.end method

.method private getBackgroundPaint()Landroid/graphics/Paint;
    .locals 3

    .prologue
    .line 124
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBackgroundPaints:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mTheme:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    .line 125
    .local v0, "paint":Landroid/graphics/Paint;
    if-nez v0, :cond_0

    .line 126
    new-instance v0, Landroid/graphics/Paint;

    .end local v0    # "paint":Landroid/graphics/Paint;
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 127
    .restart local v0    # "paint":Landroid/graphics/Paint;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mTheme:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBackgroundPaints:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mTheme:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_0
    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->getBackgroundPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mNoteDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mDogEarDrawable:Landroid/graphics/drawable/ShapeDrawable;

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mDogEarDrawable:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 170
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicHeight:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mIntrinsicWidth:I

    return v0
.end method

.method public getMinimumSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 173
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mMinimumWidth:I

    iget v2, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mMinimumHeight:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public setBounds(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 136
    return-void
.end method

.method public setPadding(I)V
    .locals 0
    .param p1, "padding"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mPadding:I

    .line 144
    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "p"    # Landroid/graphics/Paint;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 140
    return-void
.end method

.method public setTheme(Ljava/lang/String;)V
    .locals 0
    .param p1, "theme"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->mTheme:Ljava/lang/String;

    .line 148
    return-void
.end method
