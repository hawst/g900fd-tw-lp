.class Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;
.super Ljava/lang/Object;
.source "EndOfBookRecommendationsView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecommendationsImageListener"
.end annotation


# instance fields
.field private mRemaining:I

.field final synthetic this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;I)V
    .locals 0
    .param p2, "numberOfRecommendations"    # I

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput p2, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->mRemaining:I

    .line 118
    return-void
.end method


# virtual methods
.method public onLoaded()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    iget v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->mRemaining:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->mRemaining:I

    .line 123
    iget v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->mRemaining:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    # getter for: Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    invoke-static {v0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->access$000(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    # getter for: Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    invoke-static {v0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->access$000(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;->notifyRecommendationsReady()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    # setter for: Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    invoke-static {v0, v2}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->access$002(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    .line 130
    :cond_0
    return-void

    .line 127
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    # setter for: Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->access$002(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    throw v0
.end method
