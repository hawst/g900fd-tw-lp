.class Lcom/google/android/apps/books/app/SearchResultsController$1;
.super Ljava/lang/Object;
.source "SearchResultsController.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SearchResultsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/SearchResultsController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SearchResultsController;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/apps/books/app/SearchResultsController$1;->this$0:Lcom/google/android/apps/books/app/SearchResultsController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "itemPosition"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SELECT_RESULT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    int-to-long v4, p3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 234
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;

    .line 236
    .local v1, "progressItem":Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;
    if-nez v1, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultRow;->getSearchResultMatchStart()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v0

    .line 241
    .local v0, "matchLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    if-eqz v0, :cond_0

    .line 245
    iget-object v2, p0, Lcom/google/android/apps/books/app/SearchResultsController$1;->this$0:Lcom/google/android/apps/books/app/SearchResultsController;

    # getter for: Lcom/google/android/apps/books/app/SearchResultsController;->mSearchResultCallbacks:Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SearchResultsController;->access$700(Lcom/google/android/apps/books/app/SearchResultsController;)Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;->onSearchResultSelected(Lcom/google/android/apps/books/annotations/TextLocation;)V

    goto :goto_0
.end method
