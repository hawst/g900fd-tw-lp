.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;->contentFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

.field final synthetic val$contentStatusColumn:Ljava/lang/String;

.field final synthetic val$metadataUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2632
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;->val$contentStatusColumn:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;->val$metadataUri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSaved(Lcom/google/android/apps/books/model/EncryptedContent;)V
    .locals 6
    .param p1, "content"    # Lcom/google/android/apps/books/model/EncryptedContent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2635
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2636
    .local v2, "values":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;->val$contentStatusColumn:Ljava/lang/String;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2637
    if-nez p1, :cond_0

    move-object v0, v3

    .line 2638
    .local v0, "key":Lcom/google/android/apps/books/model/SessionKeyId;
    :goto_0
    invoke-static {v0, v2}, Lcom/google/android/apps/books/model/SessionKeyIds;->addToContentValues(Lcom/google/android/apps/books/model/SessionKeyId;Landroid/content/ContentValues;)V

    .line 2640
    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    # getter for: Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->access$500(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;->val$metadataUri:Landroid/net/Uri;

    invoke-virtual {v4, v5, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2642
    .local v1, "rowCount":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_1

    .line 2648
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Unexpected row count"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2637
    .end local v0    # "key":Lcom/google/android/apps/books/model/SessionKeyId;
    .end local v1    # "rowCount":I
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/EncryptedContent;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v0

    goto :goto_0

    .line 2650
    .restart local v0    # "key":Lcom/google/android/apps/books/model/SessionKeyId;
    .restart local v1    # "rowCount":I
    :cond_1
    return-void
.end method
