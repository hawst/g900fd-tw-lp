.class public Lcom/google/android/apps/books/widget/ToolTipListener;
.super Ljava/lang/Object;
.source "ToolTipListener.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final mForActionBarItems:Z

.field private final mToolTipsText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "forActionBarItems"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ToolTipListener;->mToolTipsText:Ljava/lang/CharSequence;

    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/ToolTipListener;->mForActionBarItems:Z

    .line 38
    return-void
.end method

.method private static showActionItemToolTip(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 13
    .param p0, "button"    # Landroid/view/View;
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v12, 0x0

    .line 122
    const/4 v11, 0x2

    new-array v6, v11, [I

    .line 123
    .local v6, "screenPos":[I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 124
    .local v3, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p0, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 125
    aget v0, v6, v12

    .line 126
    .local v0, "buttonLeft":I
    const/4 v11, 0x1

    aget v1, v6, v11

    .line 128
    .local v1, "buttonTop":I
    invoke-virtual {p0, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 130
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 131
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v9

    .line 132
    .local v9, "width":I
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 133
    .local v4, "height":I
    div-int/lit8 v11, v4, 0x2

    add-int v5, v1, v11

    .line 134
    .local v5, "midy":I
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v7, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 135
    .local v7, "screenWidth":I
    invoke-static {v2, p1, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    .line 136
    .local v8, "toolTip":Landroid/widget/Toast;
    sub-int v11, v7, v0

    div-int/lit8 v12, v9, 0x2

    sub-int v10, v11, v12

    .line 137
    .local v10, "xOffset":I
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v11

    if-ge v5, v11, :cond_0

    .line 139
    const/16 v11, 0x35

    invoke-virtual {v8, v11, v10, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 144
    :goto_0
    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 145
    return-void

    .line 142
    :cond_0
    const/16 v11, 0x55

    invoke-virtual {v8, v11, v10, v4}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method

.method private static showToolTip(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 17
    .param p0, "button"    # Landroid/view/View;
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 67
    const/4 v14, 0x2

    new-array v9, v14, [I

    .line 69
    .local v9, "screenPos":[I
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 70
    const/4 v14, 0x0

    aget v1, v9, v14

    .line 71
    .local v1, "buttonLeft":I
    const/4 v14, 0x1

    aget v2, v9, v14

    .line 72
    .local v2, "buttonTop":I
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 73
    .local v3, "context":Landroid/content/Context;
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v3, v0, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    .line 75
    .local v12, "toolTip":Landroid/widget/Toast;
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 76
    .local v7, "height":I
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v13

    .line 78
    .local v13, "width":I
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 79
    .local v8, "res":Landroid/content/res/Resources;
    const-string v14, "toast_y_offset"

    const-string v15, "dimen"

    const-string v16, "android"

    move-object/from16 v0, v16

    invoke-virtual {v8, v14, v15, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 82
    .local v11, "toastHeightResId":I
    if-lez v11, :cond_1

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 85
    .local v10, "toastHeight":I
    :goto_0
    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 89
    .local v4, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v14, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v14, v1

    div-int/lit8 v15, v13, 0x2

    sub-int v5, v14, v15

    .line 93
    .local v5, "drawLocationX":I
    iget v14, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v6, v14, v2

    .line 97
    .local v6, "drawLocationY":I
    if-ge v2, v10, :cond_0

    .line 99
    add-int v14, v10, v7

    sub-int/2addr v6, v14

    .line 105
    :cond_0
    const/16 v14, 0x55

    invoke-virtual {v12, v14, v5, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 107
    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 108
    return-void

    .line 82
    .end local v4    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v5    # "drawLocationX":I
    .end local v6    # "drawLocationY":I
    .end local v10    # "toastHeight":I
    :cond_1
    const v14, 0x7f09018f

    invoke-virtual {v8, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    goto :goto_0
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ToolTipListener;->mToolTipsText:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/ToolTipListener;->mToolTipsText:Ljava/lang/CharSequence;

    .line 54
    .local v0, "title":Ljava/lang/CharSequence;
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/ToolTipListener;->mForActionBarItems:Z

    if-eqz v1, :cond_1

    .line 55
    invoke-static {p1, v0}, Lcom/google/android/apps/books/widget/ToolTipListener;->showActionItemToolTip(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 59
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 53
    .end local v0    # "title":Ljava/lang/CharSequence;
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 57
    .restart local v0    # "title":Ljava/lang/CharSequence;
    :cond_1
    invoke-static {p1, v0}, Lcom/google/android/apps/books/widget/ToolTipListener;->showToolTip(Landroid/view/View;Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
