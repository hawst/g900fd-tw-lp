.class public Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;
.super Ljava/lang/Object;
.source "AnnotationContextSearchImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationContextSearch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$1;,
        Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;
    }
.end annotation


# instance fields
.field private final mCharsToSave:I

.field private final mContentDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

.field private final mSegmentSource:Lcom/google/android/apps/books/model/SegmentSource;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/render/ReaderDataSource;Lcom/google/android/apps/books/model/SegmentSource;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "contentDataSource"    # Lcom/google/android/apps/books/render/ReaderDataSource;
    .param p3, "segmentSource"    # Lcom/google/android/apps/books/model/SegmentSource;

    .prologue
    .line 158
    const/16 v0, 0x12c

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/render/ReaderDataSource;Lcom/google/android/apps/books/model/SegmentSource;I)V

    .line 160
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/render/ReaderDataSource;Lcom/google/android/apps/books/model/SegmentSource;I)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "contentDataSource"    # Lcom/google/android/apps/books/render/ReaderDataSource;
    .param p3, "segmentSource"    # Lcom/google/android/apps/books/model/SegmentSource;
    .param p4, "charsToSave"    # I

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mVolumeId:Ljava/lang/String;

    .line 151
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mContentDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    .line 152
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mSegmentSource:Lcom/google/android/apps/books/model/SegmentSource;

    .line 153
    iput p4, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mCharsToSave:I

    .line 154
    return-void
.end method

.method private fetchContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "segmentId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x6

    .line 202
    const-string v0, ""

    .line 204
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mContentDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mVolumeId:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Lcom/google/android/apps/books/render/ReaderDataSource;->getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 214
    :cond_0
    :goto_0
    return-object v0

    .line 205
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "AnnotationCH"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    const-string v2, "AnnotationCH"

    const-string v3, "Error trying to retrieve content I\'m currently viewing"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 209
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 210
    .local v1, "e":Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
    const-string v2, "AnnotationCH"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    const-string v2, "AnnotationCH"

    const-string v3, "Content I\'m currently viewing is blocked"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public retrieveContext(Lcom/google/android/apps/books/annotations/TextLocationRange;Ljava/lang/String;Z)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    .locals 11
    .param p1, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p2, "selectedText"    # Ljava/lang/String;
    .param p3, "contextCanIncludeNearbyPages"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 165
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v4, v6, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    .line 166
    .local v4, "rp":Lcom/google/android/apps/books/common/Position;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mSegmentSource:Lcom/google/android/apps/books/model/SegmentSource;

    invoke-interface {v6, v4}, Lcom/google/android/apps/books/model/SegmentSource;->getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v5

    .line 167
    .local v5, "startingIndex":I
    new-instance v2, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;

    iget v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mCharsToSave:I

    invoke-direct {v2, p1, v6, p3, v7}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;-><init>(Lcom/google/android/apps/books/annotations/TextLocationRange;IZLcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$1;)V

    .line 169
    .local v2, "handler":Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;
    move v3, v5

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mSegmentSource:Lcom/google/android/apps/books/model/SegmentSource;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/SegmentSource;->getNumberOfSegments()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 170
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->mSegmentSource:Lcom/google/android/apps/books/model/SegmentSource;

    invoke-interface {v6, v3}, Lcom/google/android/apps/books/model/SegmentSource;->getSegmentIdAtIndex(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->fetchContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->parse(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :cond_0
    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->hasGatheredEnough()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 180
    invoke-virtual {p0, v10, v10}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->setRetrieveStatus(ZZ)V

    .line 181
    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->constructContext(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v6

    .line 191
    .end local v0    # "content":Ljava/lang/String;
    :goto_2
    return-object v6

    .line 173
    .restart local v0    # "content":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
    const-string v6, "AnnotationCH"

    const/4 v8, 0x6

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 175
    const-string v6, "AnnotationCH"

    const-string v8, "Error trying to parse content"

    invoke-static {v6, v8, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 169
    .end local v1    # "e":Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 185
    .end local v0    # "content":Ljava/lang/String;
    :cond_2
    # invokes: Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->hasBeforeText()Z
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->access$100(Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 186
    invoke-virtual {p0, v9, v10}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->setRetrieveStatus(ZZ)V

    .line 187
    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl$ContextGatheringHandler;->constructContext(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v6

    goto :goto_2

    .line 190
    :cond_3
    invoke-virtual {p0, v9, v9}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;->setRetrieveStatus(ZZ)V

    move-object v6, v7

    .line 191
    goto :goto_2
.end method

.method protected setRetrieveStatus(ZZ)V
    .locals 2
    .param p1, "gatheredEnough"    # Z
    .param p2, "hasBeforeText"    # Z

    .prologue
    .line 196
    if-nez p1, :cond_0

    const-string v0, "AnnotationCH"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "AnnotationCH"

    const-string v1, "Could not gather enought context"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_0
    return-void
.end method
