.class public Lcom/google/android/apps/books/data/ResourcePolicies;
.super Ljava/lang/Object;
.source "ResourcePolicies.java"


# static fields
.field public static final FROYO:Lcom/google/android/apps/books/data/ResourcePolicy;

.field public static final HONEYCOMB:Lcom/google/android/apps/books/data/ResourcePolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/books/data/ResourcePolicies$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/ResourcePolicies$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/ResourcePolicies;->FROYO:Lcom/google/android/apps/books/data/ResourcePolicy;

    .line 47
    new-instance v0, Lcom/google/android/apps/books/data/ResourcePolicies$2;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/ResourcePolicies$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/ResourcePolicies;->HONEYCOMB:Lcom/google/android/apps/books/data/ResourcePolicy;

    return-void
.end method

.method public static get()Lcom/google/android/apps/books/data/ResourcePolicy;
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnHoneycombOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    sget-object v0, Lcom/google/android/apps/books/data/ResourcePolicies;->HONEYCOMB:Lcom/google/android/apps/books/data/ResourcePolicy;

    .line 22
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/data/ResourcePolicies;->FROYO:Lcom/google/android/apps/books/data/ResourcePolicy;

    goto :goto_0
.end method
