.class public Lcom/google/android/apps/books/app/HomeSideDrawer;
.super Ljava/lang/Object;
.source "HomeSideDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;,
        Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

.field private final mCreatingShortcut:Z

.field private mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

.field private final mDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

.field private final mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

.field private final mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;Z)V
    .locals 10
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "parent"    # Landroid/view/View;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    .param p4, "creatingShortcut"    # Z

    .prologue
    const/4 v4, 0x1

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Lcom/google/android/apps/books/app/HomeSideDrawer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$1;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    .line 141
    new-instance v0, Lcom/google/android/apps/books/app/HomeSideDrawer$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$2;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    .line 166
    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    .line 167
    iput-boolean p4, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCreatingShortcut:Z

    .line 168
    check-cast p2, Lcom/google/android/play/drawer/PlayDrawerLayout;

    .end local p2    # "parent":Landroid/view/View;
    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 179
    const/16 v8, 0x28

    .line 181
    .local v8, "barHeight":I
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeSideDrawer;->initDownloadedOnlyTextSwitcher(Landroid/app/Activity;)V

    .line 183
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getPlayCommonNetworkStack(Landroid/content/Context;)Lcom/google/android/play/utils/PlayCommonNetworkStack;

    move-result-object v9

    .line 185
    .local v9, "networkStack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    const/4 v2, 0x0

    const/16 v3, 0x28

    invoke-virtual {v9}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout;->configure(Landroid/app/Activity;ZIZLcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDrawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDrawerContent()V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->syncActionBarIconState()V

    .line 193
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/HomeSideDrawer;)Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeSideDrawer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/HomeSideDrawer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeSideDrawer;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDownloadedOnlyViews()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/HomeSideDrawer;)Landroid/widget/TextSwitcher;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeSideDrawer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    return-object v0
.end method

.method private initDownloadedOnlyTextSwitcher(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 196
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    const v3, 0x7f0e0106

    invoke-virtual {v2, v3}, Lcom/google/android/play/drawer/PlayDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextSwitcher;

    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    if-eqz v2, :cond_0

    .line 199
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    new-instance v3, Lcom/google/android/apps/books/app/HomeSideDrawer$3;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/books/app/HomeSideDrawer$3;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;Landroid/app/Activity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 220
    const v2, 0x7f050017

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 222
    .local v0, "in":Landroid/view/animation/Animation;
    const v2, 0x7f050018

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 227
    .local v1, "out":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 228
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 229
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    new-instance v3, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$OnDownloadedOnlyClickListener;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDownloadStripVisibility()V

    .line 232
    .end local v0    # "in":Landroid/view/animation/Animation;
    .end local v1    # "out":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method private populateDrawerActions(ZLjava/util/List;Ljava/util/List;)V
    .locals 12
    .param p1, "creatingShortcut"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    .local p3, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    const v4, 0x7f0b00bf

    const/4 v11, -0x1

    const/4 v6, 0x1

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v8

    .line 340
    .local v8, "activity":Landroid/app/Activity;
    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 341
    .local v10, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getLibraryMode()Ljava/lang/String;

    move-result-object v9

    .line 344
    .local v9, "libraryMode":Ljava/lang/String;
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v1, 0x7f0f019f

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020094

    const v3, 0x7f020095

    const-string v5, "readnow"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    new-instance v7, Lcom/google/android/apps/books/app/HomeSideDrawer$4;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$4;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v1, 0x7f0f019e

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020092

    const v3, 0x7f020093

    const-string v5, "mylibrary"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    new-instance v7, Lcom/google/android/apps/books/app/HomeSideDrawer$5;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$5;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v1, 0x7f0f0062

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020096

    const/4 v5, 0x0

    new-instance v7, Lcom/google/android/apps/books/app/HomeSideDrawer$6;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$6;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    move v3, v11

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v1, 0x7f0f007b

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/HomeSideDrawer$7;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$7;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    if-nez p1, :cond_0

    .line 400
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v1, 0x7f0f006b

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/HomeSideDrawer$8;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/HomeSideDrawer$8;-><init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    :cond_0
    return-void
.end method

.method private updateDownloadStripVisibility()V
    .locals 4

    .prologue
    .line 245
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 248
    .local v0, "activity":Landroid/app/Activity;
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->getDownloadedOnlyMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 252
    .local v1, "resources":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v2}, Landroid/widget/TextSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0b0105

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 254
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    const v3, 0x7f0f01a1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    .line 256
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setVisibility(I)V

    .line 260
    .end local v1    # "resources":Landroid/content/res/Resources;
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mDownloadedOnlyTextSwitcher:Landroid/widget/TextSwitcher;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateDownloadedOnlyViews()V
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDownloadStripVisibility()V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDrawerContent()V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->refreshCurrentHomeView()V

    .line 242
    return-void
.end method


# virtual methods
.method public getSelectedItemTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 432
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getLibraryMode()Ljava/lang/String;

    move-result-object v1

    .line 433
    .local v1, "libraryMode":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 434
    .local v0, "activity":Landroid/app/Activity;
    const-string v2, "readnow"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 435
    const v2, 0x7f0f019f

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 439
    :goto_0
    return-object v2

    .line 436
    :cond_0
    const-string v2, "mylibrary"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 437
    const v2, 0x7f0f019e

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 439
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method public isSideDrawerOpen()Z
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public syncActionBarIconState()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->syncDrawerIndicator()V

    .line 418
    return-void
.end method

.method public updateActionBarTitle()V
    .locals 2

    .prologue
    .line 301
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 303
    .local v0, "activity":Landroid/app/Activity;
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0f0058

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 305
    return-void

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->getSelectedItemTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public updateDrawerContent()V
    .locals 12

    .prologue
    .line 313
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v8

    .line 314
    .local v8, "activity":Landroid/app/Activity;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 315
    .local v10, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 316
    .local v6, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCreatingShortcut:Z

    invoke-direct {p0, v1, v10, v6}, Lcom/google/android/apps/books/app/HomeSideDrawer;->populateDrawerActions(ZLjava/util/List;Ljava/util/List;)V

    .line 318
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    .line 319
    .local v9, "currentAccount":Landroid/accounts/Account;
    invoke-static {v8}, Lcom/google/android/apps/books/util/AccountUtils;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v7

    .line 320
    .local v7, "accounts":[Landroid/accounts/Account;
    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 322
    .local v11, "res":Landroid/content/res/Resources;
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    const v1, 0x7f0f01a0

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b00bf

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f020047

    const v4, 0x7f020046

    new-instance v5, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v5, v8}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Lcom/google/android/apps/books/preference/LocalPreferences;->getDownloadedOnlyMode()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;-><init>(Ljava/lang/String;IIIZ)V

    .line 331
    .local v0, "downloadSwitchConfig":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer;->mSideDrawerContainer:Lcom/google/android/play/drawer/PlayDrawerLayout;

    iget-object v2, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v3, v7

    move-object v4, v10

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/play/drawer/PlayDrawerLayout;->updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V

    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateActionBarTitle()V

    .line 334
    return-void
.end method
