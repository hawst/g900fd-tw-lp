.class public Lcom/google/android/apps/books/util/MirrorInterpolator;
.super Ljava/lang/Object;
.source "MirrorInterpolator.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private final mInterpolator:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(Landroid/animation/TimeInterpolator;)V
    .locals 0
    .param p1, "interpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/util/MirrorInterpolator;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 17
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3
    .param p1, "input"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/util/MirrorInterpolator;->mInterpolator:Landroid/animation/TimeInterpolator;

    sub-float v1, v2, p1

    invoke-interface {v0, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    sub-float v0, v2, v0

    return v0
.end method
