.class public final enum Lcom/google/android/apps/books/util/ConfigValue;
.super Ljava/lang/Enum;
.source "ConfigValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/ConfigValue$Constants;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/util/ConfigValue;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ALWAYS_AUTO_START_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ALWAYS_FORCE_ANNOTATION_REFRESH:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ALWAYS_SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ALWAYS_SHOW_ONBOARDING_CARD:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum APIARY:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum DICTIONARY_METADATA_SYNC_THROTTLE_SECONDS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum EMULATE_OFFERS_DEVICE:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ENABLE_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ENABLE_ONBOARD_EXISTING:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ENABLE_SEARCH_ON_UPLOADED_PDF:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum ENABLE_STUB_DICTIONARY_METADATA:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum FAKE_SLOW_IS_SCHOOL_OWNED:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum GEO_LAYER:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum GOOGLE_ANALYTICS_SAMPLE_RATE:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum GRID_TOC:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum LEGACY_PLAY_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum LOAD_TEXT_FOR_UI_AUTOMATOR:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum LOG_TO_GOOGLE_ANALYTICS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum NASTY_DENY_DOWNLOAD_LICENSE:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum NASTY_PROXY_SERVER:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum PERFORMANCE_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum PRETEND_OFFERS_ALWAYS_SUCCEED:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum SENTENCE_BEFORE_AND_AFTER:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum SHOW_DEBUG_WORD_BOXES:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum SHOW_EMPTY_UPLOADED:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum SHOW_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum TESTING_OFFERS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum TESTING_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum UPLOAD_URL:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum USE_DOGFOOD_BEHAVIOR:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum USE_TTS_CONTENT_FROM_STORAGE:Lcom/google/android/apps/books/util/ConfigValue;

.field public static final enum WEBVIEW_HARDWARE_RENDERING:Lcom/google/android/apps/books/util/ConfigValue;


# instance fields
.field private final mDefaultValue:Ljava/lang/String;

.field private final mGservicesKey:Ljava/lang/String;

.field private final mSharedPreferencesKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 24
    new-instance v0, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v1, "GEO_LAYER"

    const-string v3, "geoLayer"

    const-string v4, "books_geo_layer_2"

    const-string v5, "true"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/util/ConfigValue;->GEO_LAYER:Lcom/google/android/apps/books/util/ConfigValue;

    .line 30
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "PERFORMANCE_LOGGING"

    const-string v6, "performanceLogging"

    const-string v7, "books_performance"

    const-string v8, "false"

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->PERFORMANCE_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    .line 38
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ALWAYS_FORCE_ANNOTATION_REFRESH"

    const-string v6, "alwaysForceAnnotationRefresh"

    const-string v7, "books_full_annotation_refresh"

    const-string v8, "false"

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_FORCE_ANNOTATION_REFRESH:Lcom/google/android/apps/books/util/ConfigValue;

    .line 44
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "SHOW_RECOMMENDATIONS"

    const-string v6, "homeScreenRecommendations"

    const-string v7, "books_home_recommendations"

    const-string v8, "true"

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 47
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "CONTENT_API"

    const/4 v5, 0x4

    const-string v6, "contentApi"

    const-string v7, "books_content_api"

    const-string v8, "https://encrypted.google.com/"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    .line 50
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "APIARY"

    const/4 v5, 0x5

    const-string v6, "apiary"

    const-string v7, "books_apiary"

    const-string v8, "https://www.googleapis.com/books/v1"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    .line 53
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "UPLOAD_URL"

    const/4 v5, 0x6

    const-string v6, "upload_url"

    const-string v7, "books:upload_url"

    const-string v8, "https://play.google.com/books/library/upload_h"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->UPLOAD_URL:Lcom/google/android/apps/books/util/ConfigValue;

    .line 64
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "LEGACY_PLAY_LOGGING"

    const/4 v5, 0x7

    const-string v6, "playLogging"

    const-string v7, "books:playlog_enabled"

    const-string v8, "false"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->LEGACY_PLAY_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    .line 68
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "LOG_TO_GOOGLE_ANALYTICS"

    const/16 v5, 0x8

    const-string v6, "logToGoogleAnalytics"

    const-string v7, "books:ga_enabled"

    const-string v8, "true"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->LOG_TO_GOOGLE_ANALYTICS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 75
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "GOOGLE_ANALYTICS_SAMPLE_RATE"

    const/16 v5, 0x9

    const-string v6, "googleAnalyticsSampleRate"

    const-string v7, "books:ga_sample_rate_times_1000"

    const-string v8, "1000"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->GOOGLE_ANALYTICS_SAMPLE_RATE:Lcom/google/android/apps/books/util/ConfigValue;

    .line 82
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "SHOW_EMPTY_UPLOADED"

    const/16 v5, 0xa

    const-string v6, "showEmptyUploaded"

    const-string v7, "books:show_empty_uploaded"

    const-string v8, "true"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_EMPTY_UPLOADED:Lcom/google/android/apps/books/util/ConfigValue;

    .line 89
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ENABLE_SEARCH_ON_UPLOADED_PDF"

    const/16 v5, 0xb

    const-string v6, "searchUploadedPdf"

    const-string v7, "books:search_uploaded_pdf"

    const-string v8, "false"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_SEARCH_ON_UPLOADED_PDF:Lcom/google/android/apps/books/util/ConfigValue;

    .line 94
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "EMULATE_OFFERS_DEVICE"

    const/16 v5, 0xc

    const-string v6, "emulateOffersDevice"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->EMULATE_OFFERS_DEVICE:Lcom/google/android/apps/books/util/ConfigValue;

    .line 97
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "WEBVIEW_HARDWARE_RENDERING"

    const/16 v5, 0xd

    const-string v6, "webviewHardwareRendering"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->WEBVIEW_HARDWARE_RENDERING:Lcom/google/android/apps/books/util/ConfigValue;

    .line 100
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "TESTING_RECOMMENDATIONS"

    const/16 v5, 0xe

    const-string v6, "testingRecommendations"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 103
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "TESTING_OFFERS"

    const/16 v5, 0xf

    const-string v6, "testingOffers"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_OFFERS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 106
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "PRETEND_OFFERS_ALWAYS_SUCCEED"

    const/16 v5, 0x10

    const-string v6, "offersAlwaysSucceed"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->PRETEND_OFFERS_ALWAYS_SUCCEED:Lcom/google/android/apps/books/util/ConfigValue;

    .line 109
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ENABLE_STUB_DICTIONARY_METADATA"

    const/16 v5, 0x11

    const-string v6, "enable_stub_dictionary_metadata"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_STUB_DICTIONARY_METADATA:Lcom/google/android/apps/books/util/ConfigValue;

    .line 112
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ENABLE_OFFLINE_DICTIONARY"

    const/16 v5, 0x12

    const-string v6, "enable_offline_dictionary"

    const-string v7, "books:offline_dictionary_enabled"

    const-string v8, "true"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

    .line 115
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "DICTIONARY_METADATA_SYNC_THROTTLE_SECONDS"

    const/16 v5, 0x13

    const-string v6, "dictionaryMetadataSyncThrottleSeconds"

    const-string v7, "books:dictionary_metadata_sync_throttle_seconds"

    const v0, 0x93a80

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->DICTIONARY_METADATA_SYNC_THROTTLE_SECONDS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 120
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "SHOW_DEBUG_WORD_BOXES"

    const/16 v5, 0x14

    const-string v6, "showDebugWordBoxes"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_DEBUG_WORD_BOXES:Lcom/google/android/apps/books/util/ConfigValue;

    .line 123
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "LOAD_TEXT_FOR_UI_AUTOMATOR"

    const/16 v5, 0x15

    const-string v6, "loadTextForUIAutomator"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->LOAD_TEXT_FOR_UI_AUTOMATOR:Lcom/google/android/apps/books/util/ConfigValue;

    .line 129
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "COMPILE_JS"

    const/16 v5, 0x16

    const-string v6, "compileJS"

    sget-object v8, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_COMPILED_CONFIG_VALUE:Ljava/lang/String;

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 135
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ALWAYS_SHOW_ONBOARDING_CARD"

    const/16 v5, 0x17

    const-string v6, "onboardFakeData"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_ONBOARDING_CARD:Lcom/google/android/apps/books/util/ConfigValue;

    .line 142
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "USE_DOGFOOD_BEHAVIOR"

    const/16 v5, 0x18

    const-string v6, "useDogfoodBehavior"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->USE_DOGFOOD_BEHAVIOR:Lcom/google/android/apps/books/util/ConfigValue;

    .line 145
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "USE_TTS_CONTENT_FROM_STORAGE"

    const/16 v5, 0x19

    const-string v6, "useTtsContentFromStorage"

    const-string v8, "true"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->USE_TTS_CONTENT_FROM_STORAGE:Lcom/google/android/apps/books/util/ConfigValue;

    .line 148
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "NASTY_PROXY_SERVER"

    const/16 v5, 0x1a

    const-string v6, "nastyProxyServer"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_PROXY_SERVER:Lcom/google/android/apps/books/util/ConfigValue;

    .line 149
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "NASTY_DENY_DOWNLOAD_LICENSE"

    const/16 v5, 0x1b

    const-string v6, "nastyDenyDownloadLicense"

    const-string v8, ""

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_DENY_DOWNLOAD_LICENSE:Lcom/google/android/apps/books/util/ConfigValue;

    .line 151
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "GRID_TOC"

    const/16 v5, 0x1c

    const-string v6, "gridToc"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->GRID_TOC:Lcom/google/android/apps/books/util/ConfigValue;

    .line 153
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "SENTENCE_BEFORE_AND_AFTER"

    const/16 v5, 0x1d

    const-string v6, "sentenceBeforeAndAfter"

    const-string v8, "true"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->SENTENCE_BEFORE_AND_AFTER:Lcom/google/android/apps/books/util/ConfigValue;

    .line 156
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "SHOW_HATS_SURVEYS"

    const/16 v5, 0x1e

    const-string v6, "showHatsSurveys"

    const-string v7, "books:show_hats_surveys"

    const-string v8, "false"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 161
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ALWAYS_SHOW_HATS_SURVEYS"

    const/16 v5, 0x1f

    const-string v6, "alwaysShowHatsSurveys"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    .line 164
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ENABLE_ONBOARDING"

    const/16 v5, 0x20

    const-string v6, "enableOnboarding"

    const-string v7, "books:enable_onboarding"

    const-string v8, "false"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    .line 167
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ENABLE_ONBOARD_EXISTING"

    const/16 v5, 0x21

    const-string v6, "enableOnboardExisting"

    const-string v7, "books:enable_onboard_existing"

    const-string v8, "false"

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARD_EXISTING:Lcom/google/android/apps/books/util/ConfigValue;

    .line 170
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "ALWAYS_AUTO_START_ONBOARDING"

    const/16 v5, 0x22

    const-string v6, "alwaysAutoStartOnboarding"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_AUTO_START_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    .line 177
    new-instance v3, Lcom/google/android/apps/books/util/ConfigValue;

    const-string v4, "FAKE_SLOW_IS_SCHOOL_OWNED"

    const/16 v5, 0x23

    const-string v6, "fakeSlowIsSchoolOwned"

    const-string v8, "false"

    move-object v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/util/ConfigValue;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/util/ConfigValue;->FAKE_SLOW_IS_SCHOOL_OWNED:Lcom/google/android/apps/books/util/ConfigValue;

    .line 20
    const/16 v0, 0x24

    new-array v0, v0, [Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->GEO_LAYER:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->PERFORMANCE_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_FORCE_ANNOTATION_REFRESH:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->UPLOAD_URL:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->LEGACY_PLAY_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->LOG_TO_GOOGLE_ANALYTICS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->GOOGLE_ANALYTICS_SAMPLE_RATE:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_EMPTY_UPLOADED:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_SEARCH_ON_UPLOADED_PDF:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->EMULATE_OFFERS_DEVICE:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->WEBVIEW_HARDWARE_RENDERING:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_OFFERS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->PRETEND_OFFERS_ALWAYS_SUCCEED:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_STUB_DICTIONARY_METADATA:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->DICTIONARY_METADATA_SYNC_THROTTLE_SECONDS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_DEBUG_WORD_BOXES:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->LOAD_TEXT_FOR_UI_AUTOMATOR:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_ONBOARDING_CARD:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->USE_DOGFOOD_BEHAVIOR:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->USE_TTS_CONTENT_FROM_STORAGE:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_PROXY_SERVER:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_DENY_DOWNLOAD_LICENSE:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->GRID_TOC:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->SENTENCE_BEFORE_AND_AFTER:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARD_EXISTING:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_AUTO_START_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->FAKE_SLOW_IS_SCHOOL_OWNED:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/util/ConfigValue;->$VALUES:[Lcom/google/android/apps/books/util/ConfigValue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "sharedPreferencesKey"    # Ljava/lang/String;
    .param p4, "gservicesKey"    # Ljava/lang/String;
    .param p5, "defaultValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 190
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 191
    iput-object p3, p0, Lcom/google/android/apps/books/util/ConfigValue;->mSharedPreferencesKey:Ljava/lang/String;

    .line 192
    iput-object p4, p0, Lcom/google/android/apps/books/util/ConfigValue;->mGservicesKey:Ljava/lang/String;

    .line 193
    iput-object p5, p0, Lcom/google/android/apps/books/util/ConfigValue;->mDefaultValue:Ljava/lang/String;

    .line 194
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/util/ConfigValue;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/books/util/ConfigValue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ConfigValue;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/util/ConfigValue;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->$VALUES:[Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/util/ConfigValue;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/util/ConfigValue;

    return-object v0
.end method


# virtual methods
.method public canOverrideViaGservices()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConfigValue;->mGservicesKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBoolean(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 232
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->stringToBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDefaultValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConfigValue;->mDefaultValue:Ljava/lang/String;

    return-object v0
.end method

.method public getInt(Landroid/content/Context;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 252
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "valString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 255
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 260
    :cond_0
    :goto_0
    return v2

    .line 256
    :catch_0
    move-exception v0

    .line 257
    .local v0, "ignored":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public getLong(Landroid/content/Context;)J
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v2, 0x0

    .line 268
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 269
    .local v1, "valString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 271
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 276
    :cond_0
    :goto_0
    return-wide v2

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "ignored":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public getRawBoolean(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getRawString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "rawValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 242
    const/4 v1, 0x0

    .line 244
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->stringToBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method public getRawString(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 214
    if-eqz p1, :cond_1

    .line 215
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/util/ConfigValue;->mSharedPreferencesKey:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "fromPrefs":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 224
    .end local v0    # "fromPrefs":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 220
    .restart local v0    # "fromPrefs":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/ConfigValue;->mGservicesKey:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/books/util/ConfigValue;->mGservicesKey:Ljava/lang/String;

    invoke-static {p1, v2, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .end local v0    # "fromPrefs":Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .line 224
    goto :goto_0
.end method

.method public getString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getRawString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "result":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 205
    .end local v0    # "result":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "result":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/ConfigValue;->mDefaultValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public overrideViaGServices(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/ConfigValue;->canOverrideViaGservices()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 300
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/books/util/ConfigValue;->mGservicesKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 303
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public overrideViaSharedPreferences(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 283
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/util/ConfigValue;->mSharedPreferencesKey:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 285
    return-void
.end method
