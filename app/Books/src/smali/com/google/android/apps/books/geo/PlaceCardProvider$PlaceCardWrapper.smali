.class Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
.super Ljava/lang/Object;
.source "PlaceCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/geo/PlaceCardProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaceCardWrapper"
.end annotation


# instance fields
.field private final mContainer:Landroid/view/ViewGroup;

.field final synthetic this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/geo/PlaceCardProvider;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 59
    .local v0, "parent":Landroid/view/ViewGroup;
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040056

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->mContainer:Landroid/view/ViewGroup;

    .line 61
    return-void
.end method


# virtual methods
.method final getCardView()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->mContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method final getGeoAnnotationView()Lcom/google/android/apps/books/widget/GeoAnnotationView;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->mContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0e0121

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/GeoAnnotationView;

    return-object v0
.end method
