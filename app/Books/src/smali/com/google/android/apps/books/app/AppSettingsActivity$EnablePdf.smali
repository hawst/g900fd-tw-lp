.class Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;
.super Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnablePdf"
.end annotation


# instance fields
.field private final mComponentName:Landroid/content/ComponentName;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 348
    const v0, 0x7f0f015d

    const v1, 0x7f0f015e

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(II)V

    .line 349
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/apps/books/app/UploadsActivityAliasPdf;

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->mComponentName:Landroid/content/ComponentName;

    .line 350
    return-void
.end method


# virtual methods
.method protected getValue()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 354
    iget-object v6, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 355
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    iget-object v6, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v2, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    .line 356
    .local v3, "setting":I
    if-ne v3, v4, :cond_0

    .line 376
    :goto_0
    return v4

    .line 359
    :cond_0
    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    move v4, v5

    .line 360
    goto :goto_0

    .line 362
    :cond_1
    if-nez v3, :cond_2

    .line 365
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->mComponentName:Landroid/content/ComponentName;

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 371
    .local v1, "info":Landroid/content/pm/ActivityInfo;
    iget-boolean v4, v1, Landroid/content/pm/ActivityInfo;->enabled:Z

    goto :goto_0

    .line 366
    .end local v1    # "info":Landroid/content/pm/ActivityInfo;
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    move v4, v5

    .line 369
    goto :goto_0

    .line 373
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    const-string v4, "AppSettings"

    const/4 v6, 0x6

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 374
    const-string v4, "AppSettings"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unexpected value from getComponentEnabledSetting: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v4, v5

    .line 376
    goto :goto_0
.end method

.method protected setValue(Z)V
    .locals 4
    .param p1, "isChecked"    # Z

    .prologue
    const/4 v2, 0x1

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 382
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$EnablePdf;->mComponentName:Landroid/content/ComponentName;

    if-eqz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v3, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 386
    return-void

    .line 382
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method
