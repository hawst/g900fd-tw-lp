.class public Lcom/google/android/apps/books/preference/TextZoomPreference;
.super Lcom/google/android/apps/books/preference/PlusMinusPreference;
.source "TextZoomPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/TextZoomPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/preference/PlusMinusPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public static computeDisplayPercent(F)I
    .locals 2
    .param p0, "value"    # F

    .prologue
    .line 16
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoom()F

    move-result v0

    div-float v0, p0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static setToNearestIncrement(F)F
    .locals 3
    .param p0, "textZoom"    # F

    .prologue
    .line 20
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoomIncrement()F

    move-result v0

    .line 21
    .local v0, "zoomIncrement":F
    const/high16 v1, 0x3f800000    # 1.0f

    div-float v2, p0, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v0

    return v1
.end method


# virtual methods
.method protected canBeLarger(F)Z
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method protected canBeSmaller(F)Z
    .locals 6
    .param p1, "value"    # F

    .prologue
    .line 34
    float-to-double v0, p1

    const-wide v2, 0x3fffae147ae147aeL    # 1.98

    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoomIncrement()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constrainValue(F)F
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 53
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoomIncrement()F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method protected getSettingTextString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/TextZoomPreference;->getValue()F

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/books/preference/TextZoomPreference;->computeDisplayPercent(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected stepValue(FLcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;)F
    .locals 3
    .param p1, "value"    # F
    .param p2, "step"    # Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    .prologue
    .line 44
    sget-object v2, Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;->UP:Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    if-ne p2, v2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 45
    .local v0, "stepAmount":F
    :goto_0
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoomIncrement()F

    move-result v2

    mul-float/2addr v2, v0

    add-float v1, p1, v2

    .line 47
    .local v1, "textZoom":F
    invoke-static {v1}, Lcom/google/android/apps/books/preference/TextZoomPreference;->setToNearestIncrement(F)F

    move-result v2

    return v2

    .line 44
    .end local v0    # "stepAmount":F
    .end local v1    # "textZoom":F
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method
