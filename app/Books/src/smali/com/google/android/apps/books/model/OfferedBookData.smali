.class public Lcom/google/android/apps/books/model/OfferedBookData;
.super Ljava/lang/Object;
.source "OfferedBookData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/OfferedBookData$1;,
        Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    }
.end annotation


# instance fields
.field private final mAuthor:Ljava/lang/String;

.field private final mCanonicalVolumeLink:Ljava/lang/String;

.field private final mCoverUri:Landroid/net/Uri;

.field private final mDescription:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    # getter for: Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->access$100(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mTitle:Ljava/lang/String;

    .line 101
    # getter for: Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mAuthor:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->access$200(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mAuthor:Ljava/lang/String;

    .line 102
    # getter for: Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mDescription:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->access$300(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mDescription:Ljava/lang/String;

    .line 103
    # getter for: Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mVolumeId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->access$400(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mVolumeId:Ljava/lang/String;

    .line 104
    # getter for: Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCoverUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->access$500(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mCoverUri:Landroid/net/Uri;

    .line 105
    # getter for: Lcom/google/android/apps/books/model/OfferedBookData$Builder;->mCanonicalVolumeLink:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->access$600(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mCanonicalVolumeLink:Ljava/lang/String;

    .line 106
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/OfferedBookData$Builder;Lcom/google/android/apps/books/model/OfferedBookData$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/OfferedBookData$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/OfferedBookData;-><init>(Lcom/google/android/apps/books/model/OfferedBookData$Builder;)V

    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/OfferedBookData$Builder;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;-><init>()V

    return-object v0
.end method

.method public static fromApiaryOfferedBook(Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;)Lcom/google/android/apps/books/model/OfferedBookData;
    .locals 2
    .param p0, "book"    # Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;

    .prologue
    .line 48
    invoke-static {}, Lcom/google/android/apps/books/model/OfferedBookData;->builder()Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setDescription(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;->coverUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setCoverUri(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;->canonicalVolumeLink:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setCanonicalVolumeLink(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/api/data/ApiaryOffer$OfferedBook;->volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/model/OfferedBookData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferedBookData$Builder;->build()Lcom/google/android/apps/books/model/OfferedBookData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mAuthor:Ljava/lang/String;

    return-object v0
.end method

.method public getCanonicalVolumeLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mCanonicalVolumeLink:Ljava/lang/String;

    return-object v0
.end method

.method public getCoverUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mCoverUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/model/OfferedBookData;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method
