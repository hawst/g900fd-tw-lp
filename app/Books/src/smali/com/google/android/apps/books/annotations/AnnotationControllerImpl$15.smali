.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->updateSyncedCopyAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

.field final synthetic val$localId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;->val$localId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;->val$characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V
    .locals 2
    .param p1, "cache"    # Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    .prologue
    .line 811
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;->val$localId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->expunge(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;->val$characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->publishCharacterQuota(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V

    .line 814
    return-void
.end method
