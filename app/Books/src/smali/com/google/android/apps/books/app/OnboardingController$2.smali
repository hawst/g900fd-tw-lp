.class Lcom/google/android/apps/books/app/OnboardingController$2;
.super Ljava/lang/Object;
.source "OnboardingController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OnboardingController;->getSampleVolumes(Ljava/util/List;ILjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OnboardingController;

.field final synthetic val$categoryIds:Ljava/util/List;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$nextPageToken:Ljava/lang/String;

.field final synthetic val$pageSize:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OnboardingController;Ljava/util/List;ILjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$categoryIds:Ljava/util/List;

    iput p3, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$pageSize:I

    iput-object p4, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$nextPageToken:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 123
    const/4 v3, 0x0

    .line 124
    .local v3, "volumes":Lcom/google/android/apps/books/api/data/SampleVolumes;
    const/4 v1, 0x0

    .line 126
    .local v1, "exception":Ljava/io/IOException;
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    # getter for: Lcom/google/android/apps/books/app/OnboardingController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {v4}, Lcom/google/android/apps/books/app/OnboardingController;->access$000(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$categoryIds:Ljava/util/List;

    iget v7, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$pageSize:I

    iget-object v8, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->val$nextPageToken:Ljava/lang/String;

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/google/android/apps/books/net/BooksServer;->getSampleVolumes(Ljava/util/Locale;Ljava/util/List;ILjava/lang/String;)Lcom/google/android/apps/books/api/data/SampleVolumes;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 133
    :goto_0
    if-eqz v1, :cond_0

    .line 134
    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .line 139
    .local v2, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/SampleVolumes;>;"
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/OnboardingController$2;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    # getter for: Lcom/google/android/apps/books/app/OnboardingController;->mUiExecutor:Lcom/google/android/apps/books/util/HandlerExecutor;
    invoke-static {v4}, Lcom/google/android/apps/books/app/OnboardingController;->access$100(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/app/OnboardingController$2$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/apps/books/app/OnboardingController$2$1;-><init>(Lcom/google/android/apps/books/app/OnboardingController$2;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 157
    return-void

    .line 128
    .end local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/SampleVolumes;>;"
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/io/IOException;
    move-object v1, v0

    goto :goto_0

    .line 136
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .restart local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/SampleVolumes;>;"
    goto :goto_1
.end method
