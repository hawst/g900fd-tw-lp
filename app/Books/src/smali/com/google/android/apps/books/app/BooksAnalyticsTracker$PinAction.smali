.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PinAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum CANCEL_UNPIN_ANIMATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum PIN_LOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum PIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum PIN_WHILE_OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum PIN_WHILE_ON_COSTLY_CONNECTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum START_UNPIN_COUNTDOWN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum UNPIN_ANIMATION_COMPLETED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum UNPIN_SAW_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum UNPIN_SUPPRESS_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

.field public static final enum UNPIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "CANCEL_UNPIN_ANIMATION"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->CANCEL_UNPIN_ANIMATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 64
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "PIN_WHILE_OFFLINE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_WHILE_OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 68
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "PIN_WHILE_ON_COSTLY_CONNECTION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_WHILE_ON_COSTLY_CONNECTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 72
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "PIN_UNLOADED_VOLUME"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 76
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "START_UNPIN_COUNTDOWN"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->START_UNPIN_COUNTDOWN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 80
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "PIN_LOADED_VOLUME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_LOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 84
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "UNPIN_UNLOADED_VOLUME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 88
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "UNPIN_ANIMATION_COMPLETED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_ANIMATION_COMPLETED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 92
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "UNPIN_SAW_DIALOG"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_SAW_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 96
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    const-string v1, "UNPIN_SUPPRESS_DIALOG"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_SUPPRESS_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .line 56
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->CANCEL_UNPIN_ANIMATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_WHILE_OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_WHILE_ON_COSTLY_CONNECTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->START_UNPIN_COUNTDOWN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_LOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_ANIMATION_COMPLETED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_SAW_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_SUPPRESS_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    return-object v0
.end method
