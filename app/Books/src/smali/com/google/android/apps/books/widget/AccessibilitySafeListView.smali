.class public Lcom/google/android/apps/books/widget/AccessibilitySafeListView;
.super Landroid/widget/ListView;
.source "AccessibilitySafeListView.java"


# virtual methods
.method public getPositionForView(Landroid/view/View;)I
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x1

    .line 40
    invoke-super {p0, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 41
    .local v0, "position":I
    if-ne v0, v1, :cond_1

    .line 51
    .end local v0    # "position":I
    :cond_0
    :goto_0
    return v0

    .line 43
    .restart local v0    # "position":I
    :cond_1
    if-ltz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AccessibilitySafeListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    :cond_2
    move v0, v1

    .line 49
    goto :goto_0
.end method
