.class Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;
.super Ljava/lang/Object;
.source "SequenceBimap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpanInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field length:I

.field offsetToOther:I

.field value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ILjava/lang/Object;I)V
    .locals 0
    .param p1, "length"    # I
    .param p3, "offsetToOther"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;I)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo<TT;>;"
    .local p2, "value":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->length:I

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->value:Ljava/lang/Object;

    .line 44
    iput p3, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->offsetToOther:I

    .line 45
    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/Object;ILcom/google/android/apps/books/render/prepaginated/SequenceBimap$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$1;

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo<TT;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;-><init>(ILjava/lang/Object;I)V

    return-void
.end method
