.class Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$4;
.super Landroid/webkit/WebViewClient;
.source "FootnoteCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->loadContent(Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$4;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 230
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "bookcontent"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$4;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    # getter for: Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->access$400(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/common/Position;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;->showInBook(Lcom/google/android/apps/books/common/Position;)V

    .line 232
    const/4 v1, 0x1

    .line 234
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
