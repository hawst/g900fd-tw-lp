.class public Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;
.super Ljava/lang/Object;
.source "RecommendationsClusterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;
    }
.end annotation


# static fields
.field public static final COLUMN_RECOMMENDATION_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field public static final ROW_RECOMMENDATION_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const v4, 0x3fb872b0    # 1.441f

    const/4 v2, 0x1

    .line 27
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f0400b9

    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    sput-object v0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->ROW_RECOMMENDATION_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 31
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040048

    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    sput-object v0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->COLUMN_RECOMMENDATION_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public static buildCluster(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;IZ)Lcom/google/android/ublib/cardlib/PlayCardClusterView;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p3, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p4, "menuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "moreHandler"    # Landroid/view/View$OnClickListener;
    .param p7, "imageProviderFactory"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;
    .param p8, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p9, "columns"    # I
    .param p10, "useRows"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            "IZ)",
            "Lcom/google/android/ublib/cardlib/PlayCardClusterView",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    .local p2, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .local p6, "clickCallback":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 66
    .local v12, "inflater":Landroid/view/LayoutInflater;
    move/from16 v0, p10

    invoke-static {v12, p1, v0}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    move-result-object v2

    .local v2, "cluster":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    move-object v1, p0

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    .line 68
    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->populateCluster(Landroid/content/Context;Lcom/google/android/ublib/cardlib/PlayCardClusterView;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;IZ)V

    .line 70
    return-object v2
.end method

.method private static buildEmptyCluster(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/google/android/ublib/cardlib/PlayCardClusterView;
    .locals 9
    .param p0, "inflater"    # Landroid/view/LayoutInflater;
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "useRows"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Landroid/view/ViewGroup;",
            "Z)",
            "Lcom/google/android/ublib/cardlib/PlayCardClusterView",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 75
    if-eqz p2, :cond_0

    const v8, 0x7f0400ba

    .line 78
    .local v8, "clusterResId":I
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, v8, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    .line 81
    .local v0, "cluster":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    new-instance v1, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-direct {v1, v4, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .local v1, "clusterMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 82
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 83
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 84
    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->hideHeader()V

    .line 85
    return-object v0

    .line 75
    .end local v0    # "cluster":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .end local v1    # "clusterMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    .end local v8    # "clusterResId":I
    :cond_0
    const v8, 0x7f040049

    goto :goto_0
.end method

.method public static getCardMetadata(IIZ)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 2
    .param p0, "docListSize"    # I
    .param p1, "columns"    # I
    .param p2, "useRows"    # Z

    .prologue
    .line 96
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->getRecommendationInfo(IIZ)Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;

    move-result-object v0

    .line 97
    .local v0, "info":Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->getCardType()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    return-object v1
.end method

.method public static getRecommendationInfo(IIZ)Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;
    .locals 2
    .param p0, "numberOfRecommendations"    # I
    .param p1, "columns"    # I
    .param p2, "useRows"    # Z

    .prologue
    .line 90
    new-instance v1, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->ROW_RECOMMENDATION_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    :goto_0
    if-eqz p2, :cond_1

    .end local p0    # "numberOfRecommendations":I
    :goto_1
    invoke-direct {v1, v0, p1, p0}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;-><init>(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)V

    return-object v1

    .restart local p0    # "numberOfRecommendations":I
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->COLUMN_RECOMMENDATION_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    goto :goto_1
.end method

.method private static populateCluster(Landroid/content/Context;Lcom/google/android/ublib/cardlib/PlayCardClusterView;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;IZ)V
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p3, "cardHeap"    # Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .param p4, "menuDelegate"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .param p5, "moreHandler"    # Landroid/view/View$OnClickListener;
    .param p7, "imageProviderFactory"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;
    .param p8, "actionCallback"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;
    .param p9, "columns"    # I
    .param p10, "useRows"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/ublib/cardlib/PlayCardClusterView",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;",
            "Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;",
            "Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "cluster":Lcom/google/android/ublib/cardlib/PlayCardClusterView;, "Lcom/google/android/ublib/cardlib/PlayCardClusterView<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .local p2, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .local p6, "clickCallback":Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;, "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v17

    .line 107
    .local v17, "numberOfRecs":I
    move/from16 v0, v17

    move/from16 v1, p9

    move/from16 v2, p10

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->getRecommendationInfo(IIZ)Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;

    move-result-object v14

    .line 108
    .local v14, "info":Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;
    invoke-virtual {v14}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->getColumns()I

    move-result v15

    .line 109
    .local v15, "nbColumns":I
    invoke-virtual {v14}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->getRows()I

    move-result v16

    .line 110
    .local v16, "nbRows":I
    invoke-virtual {v14}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->getCardType()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v11

    .line 111
    .local v11, "cardType":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    new-instance v4, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    invoke-virtual {v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v3

    mul-int/2addr v3, v15

    invoke-virtual {v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v5

    mul-int v5, v5, v16

    invoke-direct {v4, v3, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 114
    .local v4, "clusterMetadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    mul-int v18, v15, v16

    .line 115
    .local v18, "numberToShow":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move/from16 v0, v18

    if-ge v13, v0, :cond_0

    .line 116
    div-int v3, v13, v15

    invoke-virtual {v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v5

    mul-int v20, v3, v5

    .line 117
    .local v20, "row":I
    rem-int v3, v13, v15

    invoke-virtual {v11}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v5

    mul-int v12, v3, v5

    .line 118
    .local v12, "column":I
    move/from16 v0, v20

    invoke-virtual {v4, v11, v12, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    .line 115
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 120
    .end local v12    # "column":I
    .end local v20    # "row":I
    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setColumnCount(I)V

    .line 122
    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->hasCards()Z

    move-result v3

    if-nez v3, :cond_1

    .line 126
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V

    .line 129
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->createContent()V

    .line 131
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 132
    .local v19, "res":Landroid/content/res/Resources;
    const v3, 0x7f0f01a6

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const v6, 0x7f0f01a7

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5, v6}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->showHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const v3, 0x7f0f01a8

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setHeaderContentDescription(Ljava/lang/String;)V

    .line 135
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setMoreClickHandler(Landroid/view/View$OnClickListener;)V

    .line 136
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 137
    return-void
.end method
