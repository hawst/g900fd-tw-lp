.class public Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
.super Ljava/lang/Object;
.source "ImmutableChapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutableChapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mDepth:I

.field private mId:Ljava/lang/String;

.field private mReadingPosition:Ljava/lang/String;

.field private mStartPageIndex:I

.field private mStartSegmentIndex:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/Chapter;
    .locals 7

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableChapter;

    iget-object v1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mTitle:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mStartSegmentIndex:I

    iget v4, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mStartPageIndex:I

    iget v5, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mDepth:I

    iget-object v6, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mReadingPosition:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/model/ImmutableChapter;-><init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V

    return-object v0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 70
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mId:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mTitle:Ljava/lang/String;

    .line 72
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mStartSegmentIndex:I

    .line 73
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mStartPageIndex:I

    .line 74
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mDepth:I

    .line 75
    return-void
.end method

.method public setDepth(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 0
    .param p1, "depth"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mDepth:I

    .line 61
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mId:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 0
    .param p1, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mReadingPosition:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public setStartPageIndex(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 0
    .param p1, "startPageIndex"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mStartPageIndex:I

    .line 56
    return-object p0
.end method

.method public setStartSegmentIndex(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 0
    .param p1, "startSegmentIndex"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mStartSegmentIndex:I

    .line 51
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->mTitle:Ljava/lang/String;

    .line 46
    return-object p0
.end method
