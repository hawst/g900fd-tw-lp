.class final Lcom/google/android/apps/books/app/ReaderFragment$11;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->createAnnouncementTask(Ljava/lang/String;)Lcom/google/android/ublib/utils/Consumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$message:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1851
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$11;->val$message:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 3
    .param p1, "fragment"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 1854
    const/4 v0, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3802(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 1856
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1857
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$11;->val$message:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/util/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 1860
    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V
    invoke-static {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 1862
    :cond_0
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1851
    check-cast p1, Lcom/google/android/apps/books/app/ReaderFragment;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$11;->take(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method
