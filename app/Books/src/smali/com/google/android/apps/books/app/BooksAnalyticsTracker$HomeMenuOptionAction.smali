.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HomeMenuOptionAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_HELP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_LAUNCH_APP_SETTINGS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_RECENT_HEADER_SEE_ALL:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_REFRESH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_SORT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_SORT_BY_AUTHOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_SORT_BY_RECENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_SORT_BY_TITLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

.field public static final enum HOME_MENU_SORT_NO_CHANGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 354
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_HELP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_HELP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 358
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_LAUNCH_APP_SETTINGS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_LAUNCH_APP_SETTINGS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 362
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_REFRESH"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_REFRESH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 366
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_SORT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 370
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_SORT_BY_RECENT"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_RECENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 374
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_SORT_BY_TITLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_TITLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 378
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_SORT_BY_AUTHOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_AUTHOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 382
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_SORT_NO_CHANGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_NO_CHANGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 386
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    const-string v1, "HOME_MENU_RECENT_HEADER_SEE_ALL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_RECENT_HEADER_SEE_ALL:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .line 350
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_HELP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_LAUNCH_APP_SETTINGS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_REFRESH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_RECENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_TITLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_BY_AUTHOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT_NO_CHANGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_RECENT_HEADER_SEE_ALL:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 350
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 350
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;
    .locals 1

    .prologue
    .line 350
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    return-object v0
.end method
