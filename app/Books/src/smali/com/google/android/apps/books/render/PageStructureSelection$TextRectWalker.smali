.class Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;
.super Ljava/lang/Object;
.source "PageStructureSelection.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/PageStructureSelection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextRectWalker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field private final mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

.field final synthetic this$0:Lcom/google/android/apps/books/render/PageStructureSelection;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/PageStructureSelection;)V
    .locals 1

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->this$0:Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/PageStructureSelection;Lcom/google/android/apps/books/render/PageStructureSelection$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/PageStructureSelection;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/PageStructureSelection$1;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;-><init>(Lcom/google/android/apps/books/render/PageStructureSelection;)V

    return-void
.end method


# virtual methods
.method public next(Landroid/graphics/Rect;)Z
    .locals 2
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->isInvalid()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->this$0:Lcom/google/android/apps/books/render/PageStructureSelection;

    # getter for: Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;
    invoke-static {v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->access$100(Lcom/google/android/apps/books/render/PageStructureSelection;)Lcom/google/android/apps/books/render/PageStructureHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->isAfter(Lcom/google/android/apps/books/render/PageStructureHandle;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->inPageBounds()Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    :cond_0
    const/4 v0, 0x0

    .line 58
    :goto_0
    return v0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->getRect(Landroid/graphics/Rect;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->increment()V

    .line 58
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Landroid/graphics/Rect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->next(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->mIter:Lcom/google/android/apps/books/render/PageStructureHandle;

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$TextRectWalker;->this$0:Lcom/google/android/apps/books/render/PageStructureSelection;

    # getter for: Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;
    invoke-static {v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->access$000(Lcom/google/android/apps/books/render/PageStructureSelection;)Lcom/google/android/apps/books/render/PageStructureHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V

    .line 49
    return-void
.end method
