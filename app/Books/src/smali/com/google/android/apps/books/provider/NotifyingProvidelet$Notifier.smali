.class public Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
.super Ljava/lang/Object;
.source "NotifyingProvidelet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/NotifyingProvidelet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Notifier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;
    }
.end annotation


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mContentResolver:Landroid/content/ContentResolver;

    .line 48
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    .line 49
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->notify(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method private getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private notify(Landroid/net/Uri;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 87
    iget-object v1, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;

    .line 88
    .local v0, "state":Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;
    if-eqz v0, :cond_0

    .line 89
    iget-object v1, v0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;->mPendingUris:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_0
.end method


# virtual methods
.method public beginBatch()V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "NotifyingProvidelet"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const-string v0, "NotifyingProvidelet"

    const-string v1, "Previous batch mode is never finished, discarding notifications"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    new-instance v1, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$1;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public endBatch()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;

    .line 73
    .local v1, "state":Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;
    if-nez v1, :cond_1

    .line 74
    const-string v3, "NotifyingProvidelet"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    const-string v3, "NotifyingProvidelet"

    const-string v4, "Finish batch mode with no matching start"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v3, v5}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 83
    return-void

    .line 77
    :cond_1
    iget-boolean v3, v1, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;->mBatchSuccessful:Z

    if-eqz v3, :cond_0

    .line 78
    iget-object v3, v1, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;->mPendingUris:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 79
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v5, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_0
.end method

.method public setBatchSuccessful()V
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->mThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;

    .line 66
    .local v0, "state":Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;
    if-eqz v0, :cond_0

    .line 67
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;->mBatchSuccessful:Z

    .line 69
    :cond_0
    return-void
.end method
