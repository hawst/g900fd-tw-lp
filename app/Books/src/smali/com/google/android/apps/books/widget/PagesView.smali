.class public interface abstract Lcom/google/android/apps/books/widget/PagesView;
.super Ljava/lang/Object;
.source "PagesView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesDisplay;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PagesView$Callbacks;,
        Lcom/google/android/apps/books/widget/PagesView$TouchZone;,
        Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    }
.end annotation


# virtual methods
.method public abstract dxToFraction(FLcom/google/android/apps/books/util/ScreenDirection;)F
.end method

.method public abstract finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V
.end method

.method public abstract getBitmapConfig()Landroid/graphics/Bitmap$Config;
.end method

.method public abstract getOnePageHeight()I
.end method

.method public abstract getOnePageWidth()I
.end method

.method public abstract inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
.end method

.method public abstract isCached(I)Z
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZI)V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
.end method

.method public abstract resetZoom()V
.end method

.method public abstract setCallbacks(Lcom/google/android/apps/books/widget/PagesView$Callbacks;)V
.end method

.method public abstract setDisplayTwoPages(ZZ)V
.end method

.method public abstract setExecutingInitialLoadTransition(Z)V
.end method

.method public abstract setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;FI)V
.end method

.method public abstract setPageBackgroundColor(I)V
.end method

.method public abstract setPagesLoading()V
.end method

.method public abstract setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V
.end method

.method public abstract supportsPageTurnMode(Ljava/lang/String;)Z
.end method
