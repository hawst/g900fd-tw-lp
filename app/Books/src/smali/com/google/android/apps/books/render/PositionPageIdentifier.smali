.class public Lcom/google/android/apps/books/render/PositionPageIdentifier;
.super Ljava/lang/Object;
.source "PositionPageIdentifier.java"


# instance fields
.field private final mPageOffsetFromPosition:I

.field private final mPosition:Lcom/google/android/apps/books/common/Position;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/Position;I)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "pageOffsetFromPosition"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/Position;

    iput-object v0, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPosition:Lcom/google/android/apps/books/common/Position;

    .line 20
    iput p2, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPageOffsetFromPosition:I

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p0, p1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 45
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/android/apps/books/render/PositionPageIdentifier;

    .line 50
    .local v0, "that":Lcom/google/android/apps/books/render/PositionPageIdentifier;
    iget v3, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPageOffsetFromPosition:I

    iget v4, v0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPageOffsetFromPosition:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 51
    goto :goto_0

    .line 53
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPosition:Lcom/google/android/apps/books/common/Position;

    iget-object v4, v0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 54
    goto :goto_0
.end method

.method public getPageOffsetFromPosition()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPageOffsetFromPosition:I

    return v0
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v1}, Lcom/google/android/apps/books/common/Position;->hashCode()I

    move-result v0

    .line 64
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPageOffsetFromPosition:I

    add-int v0, v1, v2

    .line 65
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 69
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "position"

    iget-object v2, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPosition:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "pageOffset"

    iget v2, p0, Lcom/google/android/apps/books/render/PositionPageIdentifier;->mPageOffsetFromPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
