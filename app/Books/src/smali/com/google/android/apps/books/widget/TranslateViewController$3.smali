.class Lcom/google/android/apps/books/widget/TranslateViewController$3;
.super Ljava/lang/Object;
.source "TranslateViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/TranslateViewController;->askServerToTranslateText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

.field final synthetic val$lastUsedTargetLanguageCode:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->val$lastUsedTargetLanguageCode:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "detectedLanguage"    # Ljava/lang/String;
    .param p2, "translation"    # Ljava/lang/CharSequence;

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->getVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 491
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/common/base/StringUtil;->unescapeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->setTranslationText(Ljava/lang/CharSequence;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1300(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/lang/CharSequence;)V

    .line 485
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1100(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->setSpinnerToLanguageCode(Landroid/widget/Spinner;Ljava/lang/String;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$800(Lcom/google/android/apps/books/widget/TranslateViewController;Landroid/widget/Spinner;Ljava/lang/String;)V

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$3;->val$lastUsedTargetLanguageCode:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->setLastUsedTargetLanguageCode(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1400(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 467
    const-string v0, "TranslateViewController"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const-string v0, "TranslateViewController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error loading translations: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    :cond_0
    return-void
.end method
