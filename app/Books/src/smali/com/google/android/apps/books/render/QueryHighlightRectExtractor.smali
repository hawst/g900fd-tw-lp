.class public Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;
.super Ljava/lang/Object;
.source "QueryHighlightRectExtractor.java"


# instance fields
.field private mMatcher:Ljava/util/regex/Matcher;

.field private mPageIndex:I

.field private mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

.field private mQuery:Ljava/lang/String;

.field private mStartOffsetToRect:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mWordString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIdToRects(Ljava/lang/String;ILcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)Lcom/google/common/collect/Multimap;
    .locals 11
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "pageIndex"    # I
    .param p3, "page"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;",
            ")",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mQuery:Ljava/lang/String;

    .line 39
    iput p2, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mPageIndex:I

    .line 40
    iget v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mPageIndex:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 41
    .local v5, "pageIndexString":Ljava/lang/String;
    iput-object p3, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 42
    new-instance v1, Lcom/google/android/apps/books/render/PageStructureHandle;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/PageStructureHandle;-><init>()V

    .line 43
    .local v1, "handle":Lcom/google/android/apps/books/render/PageStructureHandle;
    iget-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/books/render/PageStructureHandle;->setBeforeFirstWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V

    .line 45
    new-instance v8, Ljava/util/TreeMap;

    invoke-direct {v8}, Ljava/util/TreeMap;-><init>()V

    iput-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mStartOffsetToRect:Ljava/util/TreeMap;

    .line 46
    iget-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mStartOffsetToRect:Ljava/util/TreeMap;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWordString(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/books/render/PageStructureHandle;->normalizeMatchString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mWordString:Ljava/lang/String;

    .line 48
    invoke-static {p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->normalizeMatchString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "normalizedQuery":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-static {v4, v8}, Lcom/google/android/apps/books/util/StringUtils;->getQueryPattern(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v8, v9}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 52
    .local v6, "pattern":Ljava/util/regex/Pattern;
    iget-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mWordString:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mMatcher:Ljava/util/regex/Matcher;

    .line 53
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v3

    .line 54
    .local v3, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mMatcher:Ljava/util/regex/Matcher;

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 55
    iget-object v8, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mStartOffsetToRect:Ljava/util/TreeMap;

    iget-object v9, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mMatcher:Ljava/util/regex/Matcher;

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->start()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/render/QueryHighlightRectExtractor;->mMatcher:Ljava/util/regex/Matcher;

    invoke-virtual {v10}, Ljava/util/regex/Matcher;->end()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v7

    .line 57
    .local v7, "subMap":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-interface {v7}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 58
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v3, v5, v8}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v7    # "subMap":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    :cond_1
    return-object v3
.end method
