.class Lcom/google/android/apps/books/app/HomeMenuImpl$2;
.super Ljava/lang/Object;
.source "HomeMenuImpl.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnSuggestionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeMenuImpl;->configureSearchView(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

.field final synthetic val$adapter:Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;

.field final synthetic val$searchView:Landroid/support/v7/widget/SearchView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeMenuImpl;Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;Landroid/support/v7/widget/SearchView;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->val$adapter:Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;

    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->val$searchView:Landroid/support/v7/widget/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionClick(I)Z
    .locals 11
    .param p1, "position"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 166
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->val$adapter:Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne p1, v6, :cond_1

    move v2, v4

    .line 167
    .local v2, "lastItem":Z
    :goto_0
    const/4 v0, 0x0

    .line 169
    .local v0, "collapseSearch":Z
    if-eqz v2, :cond_2

    .line 170
    sget-object v6, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_STORE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    int-to-long v8, p1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;Ljava/lang/Long;)V

    .line 172
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
    invoke-static {v6}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$000(Lcom/google/android/apps/books/app/HomeMenuImpl;)Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mLastQuery:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$100(Lcom/google/android/apps/books/app/HomeMenuImpl;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->startSearch(Ljava/lang/String;)V

    .line 186
    :goto_1
    if-eqz v0, :cond_0

    .line 187
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->val$searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v6, v10, v5}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 188
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/HomeMenuImpl;->setSearchViewExpanded(Z)V
    invoke-static {v6, v5}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$200(Lcom/google/android/apps/books/app/HomeMenuImpl;Z)V

    .line 192
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # setter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mLastQuery:Ljava/lang/String;
    invoke-static {v5, v10}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$102(Lcom/google/android/apps/books/app/HomeMenuImpl;Ljava/lang/String;)Ljava/lang/String;

    .line 193
    return v4

    .end local v0    # "collapseSearch":Z
    .end local v2    # "lastItem":Z
    :cond_1
    move v2, v5

    .line 166
    goto :goto_0

    .line 176
    .restart local v0    # "collapseSearch":Z
    .restart local v2    # "lastItem":Z
    :cond_2
    sget-object v6, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_SELECT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    int-to-long v8, p1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;Ljava/lang/Long;)V

    .line 178
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->val$adapter:Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 179
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x2

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 180
    .local v3, "volumeId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$2;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
    invoke-static {v6}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$000(Lcom/google/android/apps/books/app/HomeMenuImpl;)Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    move-result-object v6

    invoke-interface {v6, v3}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->onSearchSuggestionSelected(Ljava/lang/String;)V

    .line 183
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method
