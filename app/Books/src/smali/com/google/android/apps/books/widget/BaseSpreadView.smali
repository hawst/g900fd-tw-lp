.class public abstract Lcom/google/android/apps/books/widget/BaseSpreadView;
.super Ljava/lang/Object;
.source "BaseSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/BaseSpreadView$1;
    }
.end annotation


# instance fields
.field protected final mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BaseSpreadView;->mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    .line 33
    return-void
.end method

.method static create(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Landroid/content/Context;ZI)Lcom/google/android/apps/books/widget/BaseSpreadView;
    .locals 4
    .param p0, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayTwoPages"    # Z
    .param p3, "backgroundColor"    # I

    .prologue
    .line 69
    sget-object v1, Lcom/google/android/apps/books/widget/BaseSpreadView$1;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$Mode:[I

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 79
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported reader mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 71
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/books/widget/ImageSpreadView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/ImageSpreadView;-><init>(Landroid/content/Context;ZI)V

    .line 81
    .local v0, "result":Lcom/google/android/apps/books/widget/BaseSpreadView;
    :goto_0
    return-object v0

    .line 76
    .end local v0    # "result":Lcom/google/android/apps/books/widget/BaseSpreadView;
    :pswitch_1
    new-instance v0, Lcom/google/android/apps/books/widget/HtmlSpreadView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/HtmlSpreadView;-><init>(Landroid/content/Context;ZI)V

    .line 77
    .restart local v0    # "result":Lcom/google/android/apps/books/widget/BaseSpreadView;
    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract clearContent()V
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BaseSpreadView;->mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    return-object v0
.end method

.method public abstract invalidatePageContent()V
.end method

.method public abstract onBookmarkChanged(I)V
.end method

.method public abstract scaleAndScroll(FFF)V
.end method

.method public abstract setPageContent(ILcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
.end method

.method public abstract setPageLoading(ILandroid/graphics/Point;)V
.end method

.method public abstract setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V
.end method
