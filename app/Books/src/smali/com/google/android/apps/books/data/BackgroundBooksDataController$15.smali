.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifestOnMainThread(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15$1;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 719
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V

    .line 720
    return-void
.end method
