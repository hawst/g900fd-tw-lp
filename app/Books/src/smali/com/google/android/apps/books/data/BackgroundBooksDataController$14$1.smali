.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->ensureSharedResourcesOnControlThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeManifest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/util/Nothing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

.field final synthetic val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 0

    .prologue
    .line 603
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 606
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

    iget-object v0, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifestOnMainThread(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 611
    :goto_0
    return-void

    .line 609
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

    iget-object v0, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->val$volumeId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishManifestException(Ljava/lang/String;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 603
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
