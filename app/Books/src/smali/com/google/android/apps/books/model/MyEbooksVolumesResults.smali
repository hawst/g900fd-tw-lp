.class public Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
.super Ljava/lang/Object;
.source "MyEbooksVolumesResults.java"


# instance fields
.field public downloadProgress:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;"
        }
    .end annotation
.end field

.field public localVolumeData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;"
        }
    .end annotation
.end field

.field public myEbooksVolumes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "myEbooksVolumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    .local p2, "localVolumeData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    .local p3, "downloadProgress":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->localVolumeData:Ljava/util/Map;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->downloadProgress:Ljava/util/Map;

    .line 35
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 39
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "myEbooksVolumes"

    iget-object v2, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "localVolumeData"

    iget-object v2, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->localVolumeData:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "downloadProgress"

    iget-object v2, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->downloadProgress:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
