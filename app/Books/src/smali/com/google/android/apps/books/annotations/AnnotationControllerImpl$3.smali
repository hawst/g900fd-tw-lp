.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->startServerSync(Ljava/util/List;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$layers:Ljava/util/Collection;

.field final synthetic val$onComplete:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$vvs:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$vvs:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$layers:Ljava/util/Collection;

    iput-object p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$onComplete:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 211
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$vvs:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$layers:Ljava/util/Collection;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->syncOnBackgroundThread(Ljava/util/List;Ljava/util/Collection;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$000(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Ljava/util/Collection;)V

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$onComplete:Lcom/google/android/ublib/utils/Consumer;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;->val$onComplete:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method
