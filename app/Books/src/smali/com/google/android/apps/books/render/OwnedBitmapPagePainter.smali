.class public Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;
.super Ljava/lang/Object;
.source "OwnedBitmapPagePainter.java"

# interfaces
.implements Lcom/google/android/apps/books/render/PagePainter;


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private mExposedBitmap:Z

.field private mReleasedBitmap:Z


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    .line 20
    return-void
.end method

.method private throwIfReleased()V
    .locals 2

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mReleasedBitmap:Z

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to access released bitmap"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public canStillDraw()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mReleasedBitmap:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 32
    return-void
.end method

.method public getOwnedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "create"    # Z

    .prologue
    .line 43
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mExposedBitmap:Z

    if-eqz v1, :cond_0

    .line 44
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Attempt to acquire released bitmap"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 46
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->getSharedBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 47
    .local v0, "result":Landroid/graphics/Bitmap;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mReleasedBitmap:Z

    .line 48
    return-object v0
.end method

.method public getSharedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->throwIfReleased()V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mExposedBitmap:Z

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->throwIfReleased()V

    .line 25
    iget-object v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 26
    return-object p1
.end method

.method public getViewLayerType()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public isMutableBitmap()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    return v0
.end method

.method public isUpdatable()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public recycle()V
    .locals 2

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mExposedBitmap:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mReleasedBitmap:Z

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to recycle released bitmap"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 57
    return-void
.end method
