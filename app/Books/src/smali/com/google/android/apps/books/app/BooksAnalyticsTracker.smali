.class public Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.super Ljava/lang/Object;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;,
        Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;
    }
.end annotation


# static fields
.field private static sExtraDebugLogging:Z

.field private static sFirstTimeInitialized:Z

.field private static sHomeDimensions:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sLastPageTurnLogTime:J

.field private static sOverflowDimensions:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPagesFlippedSinceLastLog:J

.field private static sPlayLogger:Lcom/google/android/apps/books/app/BooksEventLogger;

.field private static sShouldLogAnalytics:Z

.field private static sVolumeDimensions:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 869
    sput-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sFirstTimeInitialized:Z

    .line 870
    sput-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    .line 871
    sput-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sExtraDebugLogging:Z

    .line 872
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sLastPageTurnLogTime:J

    .line 873
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    .line 875
    sput-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sVolumeDimensions:Landroid/util/SparseArray;

    .line 876
    sput-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    .line 877
    sput-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sOverflowDimensions:Landroid/util/SparseArray;

    .line 878
    sput-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPlayLogger:Lcom/google/android/apps/books/app/BooksEventLogger;

    return-void
.end method

.method public static activityStart(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 940
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 941
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "activityStart: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeExtraDebugLog(Ljava/lang/String;)V

    .line 942
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getInstance()Lcom/google/analytics/tracking/android/EasyTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/analytics/tracking/android/EasyTracker;->activityStart(Landroid/app/Activity;)V

    .line 944
    :cond_0
    return-void
.end method

.method public static activityStop(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 951
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 952
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "activityStop: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeExtraDebugLog(Ljava/lang/String;)V

    .line 953
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getInstance()Lcom/google/analytics/tracking/android/EasyTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/analytics/tracking/android/EasyTracker;->activityStop(Landroid/app/Activity;)V

    .line 955
    :cond_0
    return-void
.end method

.method private static addAccessModeDimension(Landroid/util/SparseArray;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 2
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1101
    .local p0, "dims":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x4

    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeAccessMode(Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1102
    return-void
.end method

.method private static addIsOnlineDimension(Landroid/content/Context;Landroid/util/SparseArray;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1089
    .local p1, "dims":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/16 v0, 0x13

    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getConnectedString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1090
    return-void
.end method

.method private static annotationAction(ZI)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;
    .locals 1
    .param p0, "edit"    # Z
    .param p1, "colorChoice"    # I

    .prologue
    .line 1401
    if-nez p0, :cond_0

    .line 1402
    packed-switch p1, :pswitch_data_0

    .line 1416
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1403
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1404
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1405
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1406
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_HIGHLIGHT_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1409
    :cond_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 1410
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_1:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1411
    :pswitch_5
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_2:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1412
    :pswitch_6
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_3:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1413
    :pswitch_7
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->CHANGE_HIGHLIGHT_TO_COLOR_4:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_1

    .line 1402
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1409
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static bookmarkAction(ZZ)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;
    .locals 1
    .param p0, "fromMenu"    # Z
    .param p1, "gainingBookmark"    # Z

    .prologue
    .line 1378
    if-eqz p0, :cond_1

    .line 1379
    if-eqz p1, :cond_0

    .line 1380
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    .line 1388
    :goto_0
    return-object v0

    .line 1382
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->REMOVE_BOOKMARK_FROM_MENU:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_0

    .line 1385
    :cond_1
    if-eqz p1, :cond_2

    .line 1386
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_0

    .line 1388
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->REMOVE_BOOKMARK_FROM_PAGE_TAP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_0
.end method

.method private static changeVolumeDimensions(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V
    .locals 0
    .param p0, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p2, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p3, "displayTwoPages"    # Ljava/lang/Boolean;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 1025
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->flushPageFlips()V

    .line 1026
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeVolumeDimensions(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 1027
    return-void
.end method

.method private static checkForDebug(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 905
    invoke-static {p0}, Lcom/google/analytics/tracking/android/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/analytics/tracking/android/GoogleAnalytics;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/analytics/tracking/android/GoogleAnalytics;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    const-string v0, "BooksTracker"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sExtraDebugLogging:Z

    .line 907
    invoke-static {}, Lcom/google/analytics/tracking/android/GAServiceManager;->getInstance()Lcom/google/analytics/tracking/android/GAServiceManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/analytics/tracking/android/GAServiceManager;->setDispatchPeriod(I)V

    .line 909
    :cond_0
    return-void
.end method

.method private static checkForGaEnabled(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 900
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->LOG_TO_GOOGLE_ANALYTICS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    .line 901
    return-void
.end method

.method private static checkForPlayLogEnabled(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 912
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->LEGACY_PLAY_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 913
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sExtraDebugLogging:Z

    invoke-static {p0, v0}, Lcom/google/android/apps/books/app/BooksEventLogger;->getInstance(Landroid/content/Context;Z)Lcom/google/android/apps/books/app/BooksEventLogger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPlayLogger:Lcom/google/android/apps/books/app/BooksEventLogger;

    .line 915
    :cond_0
    return-void
.end method

.method private static checkForSampling(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 918
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldSubsampleGoogleAnalytics(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 919
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getGoogleAnalyticsSamplePercent(Landroid/content/Context;)D

    move-result-wide v0

    .line 920
    .local v0, "sampleRate":D
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/analytics/tracking/android/Tracker;->setSampleRate(D)V

    .line 922
    .end local v0    # "sampleRate":D
    :cond_0
    return-void
.end method

.method private static computeAccessMode(Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;
    .locals 1
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 1153
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1154
    const-string v0, "UPLOAD"

    .line 1162
    :goto_0
    return-object v0

    .line 1155
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/util/RentalUtils;->isRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1156
    invoke-static {p0}, Lcom/google/android/apps/books/util/RentalUtils;->isExpired(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1157
    const-string v0, "RENTAL_EXPIRED"

    goto :goto_0

    .line 1159
    :cond_1
    const-string v0, "RENTAL_ACTIVE"

    goto :goto_0

    .line 1162
    :cond_2
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeData$Access;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static computeAvailableViews(Lcom/google/android/apps/books/model/VolumeMetadata;)Ljava/lang/String;
    .locals 7
    .param p0, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 1167
    const-string v4, ""

    .line 1168
    .local v4, "views":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->values()[Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 1169
    .local v3, "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-interface {p0, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1170
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getAnalyticsKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1168
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1174
    .end local v3    # "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .end local v4    # "views":Ljava/lang/String;
    :cond_2
    return-object v4
.end method

.method private static computeBrightnessString(I)Ljava/lang/String;
    .locals 1
    .param p0, "brightness"    # I

    .prologue
    .line 1120
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 1121
    const-string v0, "match_system"

    .line 1123
    :goto_0
    return-object v0

    :cond_0
    div-int/lit8 v0, p0, 0xa

    mul-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static computeHasEobb(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Z
    .locals 1
    .param p0, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 1182
    invoke-interface {p0, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getEndOfBookBody(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static computeHomeDimensions(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "homeScreenMode"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1030
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 1031
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    .line 1033
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1034
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->addIsOnlineDimension(Landroid/content/Context;Landroid/util/SparseArray;)V

    .line 1035
    return-void
.end method

.method private static computeHomeOverflowDimensions(Lcom/google/android/apps/books/model/VolumeData;Landroid/content/Context;)V
    .locals 1
    .param p0, "data"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1038
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sOverflowDimensions:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 1039
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sOverflowDimensions:Landroid/util/SparseArray;

    .line 1041
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sOverflowDimensions:Landroid/util/SparseArray;

    invoke-static {v0, p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->addAccessModeDimension(Landroid/util/SparseArray;Lcom/google/android/apps/books/model/VolumeData;)V

    .line 1042
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sOverflowDimensions:Landroid/util/SparseArray;

    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->addIsOnlineDimension(Landroid/content/Context;Landroid/util/SparseArray;)V

    .line 1043
    return-void
.end method

.method private static computeLineHeightString(F)Ljava/lang/String;
    .locals 1
    .param p0, "lineHeight"    # F

    .prologue
    .line 1133
    invoke-static {p0}, Lcom/google/android/apps/books/preference/LineHeightPreference;->setToNearestIncrement(F)F

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/preference/LineHeightPreference;->computeDisplayPercent(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static computeOrientationString(Ljava/lang/Boolean;Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "displayTwoPages"    # Ljava/lang/Boolean;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1138
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v0, v2, Landroid/content/res/Configuration;->orientation:I

    .line 1139
    .local v0, "orientation":I
    if-eqz p0, :cond_3

    .line 1140
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1141
    .local v1, "twoPages":Z
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 1142
    if-eqz v1, :cond_0

    const-string v2, "portrait_2up"

    .line 1149
    .end local v1    # "twoPages":Z
    :goto_0
    return-object v2

    .line 1142
    .restart local v1    # "twoPages":Z
    :cond_0
    const-string v2, "portrait"

    goto :goto_0

    .line 1144
    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 1145
    if-eqz v1, :cond_2

    const-string v2, "landscape_2up"

    goto :goto_0

    :cond_2
    const-string v2, "landscape_1up"

    goto :goto_0

    .line 1149
    .end local v1    # "twoPages":Z
    :cond_3
    const-string v2, "unknown"

    goto :goto_0
.end method

.method private static computeTextZoomString(F)Ljava/lang/String;
    .locals 1
    .param p0, "textZoom"    # F

    .prologue
    .line 1128
    invoke-static {p0}, Lcom/google/android/apps/books/preference/TextZoomPreference;->setToNearestIncrement(F)F

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/preference/TextZoomPreference;->computeDisplayPercent(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static computeThemeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "readerTheme"    # Ljava/lang/String;

    .prologue
    .line 1105
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106
    const-string v0, "day"

    .line 1112
    :goto_0
    return-object v0

    .line 1107
    :cond_0
    const-string v0, "1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1108
    const-string v0, "night"

    goto :goto_0

    .line 1109
    :cond_1
    const-string v0, "2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1110
    const-string v0, "sepia"

    goto :goto_0

    .line 1112
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private static computeTtsEnablement(Lcom/google/android/apps/books/model/VolumeMetadata;)Ljava/lang/String;
    .locals 1
    .param p0, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 1178
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static computeVolumeDimensions(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V
    .locals 6
    .param p0, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p2, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p3, "displayTwoPages"    # Ljava/lang/Boolean;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 1047
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sVolumeDimensions:Landroid/util/SparseArray;

    if-nez v3, :cond_0

    .line 1048
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    sput-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sVolumeDimensions:Landroid/util/SparseArray;

    .line 1050
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sVolumeDimensions:Landroid/util/SparseArray;

    .line 1051
    .local v0, "dims":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-eqz p0, :cond_1

    .line 1052
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    .line 1053
    .local v2, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-static {v0, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->addAccessModeDimension(Landroid/util/SparseArray;Lcom/google/android/apps/books/model/VolumeData;)V

    .line 1054
    const/4 v3, 0x5

    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeAvailableViews(Lcom/google/android/apps/books/model/VolumeMetadata;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1055
    const/4 v3, 0x6

    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeTtsEnablement(Lcom/google/android/apps/books/model/VolumeMetadata;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1057
    .end local v2    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_1
    if-eqz p1, :cond_2

    .line 1058
    const/4 v3, 0x7

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeThemeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1060
    const/16 v3, 0x8

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->defaultForNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1061
    const/16 v3, 0x9

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->defaultForNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1063
    const/16 v3, 0xb

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/ReaderSettings;->getTextZoom()F

    move-result v5

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeTextZoomString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1065
    const/16 v3, 0xc

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/ReaderSettings;->getLineHeight()F

    move-result v5

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeLineHeightString(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1068
    :cond_2
    if-eqz p4, :cond_3

    .line 1069
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, p4}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 1070
    .local v1, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    const/16 v3, 0xa

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getBrightness()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeBrightnessString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1072
    const/16 v3, 0xe

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getPageTurnMode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1073
    const/16 v3, 0xf

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getVolumeKeyPageTurn()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1075
    const/16 v3, 0x10

    invoke-static {p3, p4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeOrientationString(Ljava/lang/Boolean;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1077
    invoke-static {p4, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->addIsOnlineDimension(Landroid/content/Context;Landroid/util/SparseArray;)V

    .line 1079
    .end local v1    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_3
    if-eqz p2, :cond_4

    .line 1080
    const/16 v3, 0xd

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getAnalyticsKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1081
    if-eqz p0, :cond_4

    .line 1082
    const/16 v3, 0x12

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0, p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeHasEobb(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1086
    :cond_4
    return-void
.end method

.method private static defaultForNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 1116
    if-nez p0, :cond_0

    const-string p0, "default"

    .end local p0    # "string":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static flushPageFlips()V
    .locals 2

    .prologue
    .line 1203
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldLogPageTurns()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1204
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPageFlips(J)V

    .line 1206
    :cond_0
    return-void
.end method

.method private static getConnectedString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1093
    if-nez p0, :cond_0

    .line 1095
    const-string v0, "context_missing"

    .line 1097
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getDimensionedTracker(Landroid/util/SparseArray;)Lcom/google/analytics/tracking/android/Tracker;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/analytics/tracking/android/Tracker;"
        }
    .end annotation

    .prologue
    .line 1244
    .local p0, "dimensions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v3

    .line 1245
    .local v3, "tracker":Lcom/google/analytics/tracking/android/Tracker;
    if-eqz p0, :cond_0

    .line 1246
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 1247
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1248
    .local v0, "dimensionKey":I
    invoke-virtual {p0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1249
    .local v1, "dimensionValue":Ljava/lang/String;
    invoke-virtual {v3, v0, v1}, Lcom/google/analytics/tracking/android/Tracker;->setCustomDimension(ILjava/lang/String;)V

    .line 1250
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dimension: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeExtraDebugLog(Ljava/lang/String;)V

    .line 1246
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1253
    .end local v0    # "dimensionKey":I
    .end local v1    # "dimensionValue":Ljava/lang/String;
    .end local v2    # "i":I
    :cond_0
    return-object v3
.end method

.method private static getExceptionTypeName(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 981
    if-nez p0, :cond_0

    .line 982
    const-string v0, ""

    .line 987
    :goto_0
    return-object v0

    .line 984
    :cond_0
    instance-of v0, p0, Lcom/google/android/ublib/utils/WrappedIoException;

    if-eqz v0, :cond_1

    .line 985
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getExceptionTypeName(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 987
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getGoogleAnalyticsSamplePercent(Landroid/content/Context;)D
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 932
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->GOOGLE_ANALYTICS_SAMPLE_RATE:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getInt(Landroid/content/Context;)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private static getTocAnnotationAction(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;
    .locals 1
    .param p0, "layerId"    # Ljava/lang/String;

    .prologue
    .line 1307
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1308
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_BOOKMARK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .line 1313
    :goto_0
    return-object v0

    .line 1309
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1310
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SELECT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    goto :goto_0

    .line 1313
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SHOULD_NEVER_HAPPEN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    goto :goto_0
.end method

.method private static homeDrawerPlaylogExtras(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p0, "label"    # Ljava/lang/String;

    .prologue
    .line 1459
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldPlayLog()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1460
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1462
    .local v0, "mode":Ljava/lang/String;
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "label"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string v3, "homeViewMode"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v0, v1, v2

    .line 1466
    .end local v0    # "mode":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 889
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getInstance()Lcom/google/analytics/tracking/android/EasyTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/analytics/tracking/android/EasyTracker;->setContext(Landroid/content/Context;)V

    .line 890
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sFirstTimeInitialized:Z

    if-nez v0, :cond_0

    .line 891
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->checkForGaEnabled(Landroid/content/Context;)V

    .line 892
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->checkForDebug(Landroid/content/Context;)V

    .line 893
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->checkForPlayLogEnabled(Landroid/content/Context;)V

    .line 894
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->checkForSampling(Landroid/content/Context;)V

    .line 895
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sFirstTimeInitialized:Z

    .line 897
    :cond_0
    return-void
.end method

.method public static logBookDownloadFailed(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;Ljava/lang/Throwable;)V
    .locals 6
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 975
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 976
    const-string v0, "download"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getExceptionTypeName(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 978
    :cond_0
    return-void
.end method

.method public static logBookLoaded()V
    .locals 4

    .prologue
    .line 964
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 965
    const-string v0, "load"

    const-string v1, "loaded_metadata"

    const-string v2, "loaded_metadata"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendVolumeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 968
    :cond_0
    const-string v0, "loaded_metadata"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybePlayLog(Ljava/lang/String;[Ljava/lang/String;)V

    .line 969
    return-void
.end method

.method public static logBookmarkAction(ZZI)V
    .locals 4
    .param p0, "fromMenu"    # Z
    .param p1, "gainingBookmark"    # Z
    .param p2, "numBookmarks"    # I

    .prologue
    .line 1371
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1372
    const-string v0, "annotation_action"

    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->bookmarkAction(ZZ)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    move-result-object v1

    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1375
    :cond_0
    return-void
.end method

.method public static logClosedBookUsingBackButton()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1613
    const-string v0, "close_book_via_back_button"

    invoke-static {v0, v1, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1614
    return-void
.end method

.method public static logClosedBookUsingUpButton()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1617
    const-string v0, "close_book_via_up_button"

    invoke-static {v0, v1, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1618
    return-void
.end method

.method public static logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V
    .locals 3
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .prologue
    .line 1303
    const-string v0, "dictionary_action"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1304
    return-void
.end method

.method public static logDisplayFirstMainPageTime(JZ)V
    .locals 4
    .param p0, "elapsedTime"    # J
    .param p2, "includingManifestDownload"    # Z

    .prologue
    .line 1529
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1530
    const-string v1, "timing_ui_reading"

    const-string v2, "display_first_main_page"

    if-eqz p2, :cond_1

    const-string v0, "including_manifest_download"

    :goto_0
    invoke-static {v1, p0, p1, v2, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1535
    :cond_0
    return-void

    .line 1530
    :cond_1
    const-string v0, "not_including_manifest_download"

    goto :goto_0
.end method

.method public static logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p3, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p4, "displayTwoPages"    # Ljava/lang/Boolean;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 1350
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1351
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->changeVolumeDimensions(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 1352
    const-string v0, "display_options_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1354
    :cond_0
    return-void
.end method

.method public static logDisplayedLoadingPage(J)V
    .locals 4
    .param p0, "elapsedTime"    # J

    .prologue
    .line 1541
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1542
    const-string v0, "timing_ui_reading"

    const-string v1, "display_loading_page"

    const/4 v2, 0x0

    invoke-static {v0, p0, p1, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1545
    :cond_0
    return-void
.end method

.method public static logDownloadedManifest(J)V
    .locals 6
    .param p0, "elapsedTime"    # J

    .prologue
    .line 1551
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1552
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    const-string v1, "timing_background"

    const-string v4, "download_manifest"

    const/4 v5, 0x0

    move-wide v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1555
    :cond_0
    return-void
.end method

.method public static logForceClosedBook(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V
    .locals 5
    .param p0, "reason"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 1622
    sget-boolean v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-nez v2, :cond_0

    .line 1636
    :goto_0
    return-void

    .line 1625
    :cond_0
    if-nez p0, :cond_1

    .line 1626
    sget-object p0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OTHER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 1629
    :cond_1
    if-eqz p1, :cond_2

    .line 1630
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 1631
    .local v0, "className":Ljava/lang/String;
    const-string v2, "Exception\\z"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1635
    .end local v0    # "className":Ljava/lang/String;
    .local v1, "label":Ljava/lang/String;
    :goto_1
    const-string v2, "force_close_book"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 1633
    .end local v1    # "label":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "label":Ljava/lang/String;
    goto :goto_1
.end method

.method public static logGeoAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    .prologue
    .line 1287
    const-string v0, "geo_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1288
    return-void
.end method

.method public static logHelp()V
    .locals 4

    .prologue
    .line 1357
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1358
    const-string v0, "user_action"

    const-string v1, "help_action"

    const-string v2, "help_action"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1360
    :cond_0
    return-void
.end method

.method public static logHighlightAddOrEdit(ZII)V
    .locals 4
    .param p0, "edit"    # Z
    .param p1, "colorChoice"    # I
    .param p2, "numCharacters"    # I

    .prologue
    .line 1394
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1395
    const-string v0, "annotation_action"

    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->annotationAction(ZI)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    move-result-object v1

    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1398
    :cond_0
    return-void
.end method

.method public static logHighlightDeletion(Z)V
    .locals 4
    .param p0, "hadNote"    # Z

    .prologue
    .line 1420
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1421
    const-string v1, "annotation_action"

    if-eqz p0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->DELETE_HIGHLIGHT_WITH_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    :goto_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1426
    :cond_0
    return-void

    .line 1421
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->DELETE_HIGHLIGHT_NO_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_0
.end method

.method public static logHomeAccountAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    .prologue
    .line 1490
    const-string v0, "home_settings_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendHomeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1491
    return-void
.end method

.method private static logHomeDrawerAction(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "label"    # Ljava/lang/String;
    .param p1, "homeScreenMode"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1453
    invoke-static {p1, p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeHomeDimensions(Ljava/lang/String;Landroid/content/Context;)V

    .line 1454
    const-string v0, "home_drawer_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendHomeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1455
    const-string v0, "home_drawer_action"

    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->homeDrawerPlaylogExtras(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybePlayLog(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1456
    return-void
.end method

.method public static logHomeDrawerModeSwitch(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p0, "homeScreenMode"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1449
    const-string v0, "change_drawer_selection"

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeDrawerAction(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1450
    return-void
.end method

.method public static logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .prologue
    .line 1471
    const-string v0, "home_menu_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendHomeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1472
    return-void
.end method

.method public static logHomeOverflowAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/Context;)V
    .locals 5
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1476
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_1

    .line 1477
    if-eqz p1, :cond_0

    .line 1478
    invoke-static {p1, p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->computeHomeOverflowDimensions(Lcom/google/android/apps/books/model/VolumeData;Landroid/content/Context;)V

    .line 1480
    :cond_0
    const-string v0, "user_action"

    const-string v1, "home_overflow_action"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sOverflowDimensions:Landroid/util/SparseArray;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendDimensionedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/util/SparseArray;)V

    .line 1483
    :cond_1
    return-void
.end method

.method public static logHomeSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;Ljava/lang/Long;)V
    .locals 1
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 1486
    const-string v0, "home_search_action"

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendHomeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1487
    return-void
.end method

.method public static logHomeSettingChoice(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;Ljava/lang/String;)V
    .locals 3
    .param p0, "option"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1496
    const-string v0, "home_settings_action"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "__"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendHomeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1497
    return-void
.end method

.method public static logHomeStart(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "homeScreenMode"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1438
    const-string v0, "start_with_drawer_selection"

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeDrawerAction(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1439
    const-string v0, "home_opened"

    sget-object v1, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->HOME_OPENED:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybePlaylogBooksEvent(Ljava/lang/String;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;Landroid/content/Context;)V

    .line 1441
    return-void
.end method

.method public static logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V
    .locals 1
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 1268
    const-string v0, "in_the_book_search_action"

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1269
    return-void
.end method

.method public static logJsPerformance(Lcom/google/android/apps/books/util/JsPerformanceMetrics;)V
    .locals 5
    .param p0, "metrics"    # Lcom/google/android/apps/books/util/JsPerformanceMetrics;

    .prologue
    .line 1584
    sget-boolean v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v1, :cond_1

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->isValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1585
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->wasBackgrounded()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "background"

    .line 1586
    .local v0, "label":Ljava/lang/String;
    :goto_0
    const-string v1, "timing_ui_reading"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->firstPageTime()I

    move-result v2

    int-to-long v2, v2

    const-string v4, "js_first_page"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1588
    const-string v1, "timing_ui_reading"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->lastPageTime()I

    move-result v2

    int-to-long v2, v2

    const-string v4, "js_last_page"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1590
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->hasFonts()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1591
    const-string v1, "timing_ui_reading"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->fontsTime()I

    move-result v2

    int-to-long v2, v2

    const-string v4, "js_fonts"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1594
    :cond_0
    const-string v1, "timing_ui_reading"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->overHeadTime()I

    move-result v2

    int-to-long v2, v2

    const-string v4, "js_overhead"

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1597
    .end local v0    # "label":Ljava/lang/String;
    :cond_1
    return-void

    .line 1585
    :cond_2
    const-string v0, "foreground"

    goto :goto_0
.end method

.method public static logLoadedManifest(J)V
    .locals 6
    .param p0, "elapsedTime"    # J

    .prologue
    .line 1571
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1572
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    const-string v1, "timing_background"

    const-string v4, "load_manifest"

    const/4 v5, 0x0

    move-wide v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1575
    :cond_0
    return-void
.end method

.method public static logNoteAddOrEdit(ZI)V
    .locals 4
    .param p0, "edit"    # Z
    .param p1, "numNoteCharacters"    # I

    .prologue
    .line 1429
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1430
    const-string v1, "annotation_action"

    if-eqz p0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->EDIT_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    :goto_0
    int-to-long v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1435
    :cond_0
    return-void

    .line 1430
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;->ADD_NOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$AnnotationAction;

    goto :goto_0
.end method

.method public static logOnboardingAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;Ljava/lang/Long;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 1299
    const-string v0, "onboarding_action"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1300
    return-void
.end method

.method private static logPageFlips(J)V
    .locals 10
    .param p0, "now"    # J

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 1213
    sget-wide v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_1

    .line 1214
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1215
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    sget-wide v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/google/analytics/tracking/android/Tracker;->setCustomMetric(ILjava/lang/Long;)V

    .line 1217
    const-string v0, "accumulated"

    const-string v1, "page_turn"

    const-string v2, "page_turns"

    sget-wide v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendVolumeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1220
    :cond_0
    const-string v0, "page_turn"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "pageTurns"

    aput-object v3, v1, v2

    sget-wide v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybePlayLog(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1222
    sput-wide v8, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    .line 1224
    :cond_1
    sput-wide p0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sLastPageTurnLogTime:J

    .line 1225
    return-void
.end method

.method public static logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V
    .locals 3
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    .prologue
    .line 1260
    const-string v0, "pin_action"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1261
    return-void
.end method

.method public static logPlaybackAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;Ljava/lang/Long;)V
    .locals 1
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;
    .param p1, "timeElapsed"    # Ljava/lang/Long;

    .prologue
    .line 1295
    const-string v0, "playback_action"

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1296
    return-void
.end method

.method public static logReadStart(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1444
    const-string v0, "read_opened"

    sget-object v1, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->READ_OPENED:Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybePlaylogBooksEvent(Ljava/lang/String;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;Landroid/content/Context;)V

    .line 1446
    return-void
.end method

.method public static logSavedManifest(J)V
    .locals 6
    .param p0, "elapsedTime"    # J

    .prologue
    .line 1561
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1562
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    const-string v1, "timing_background"

    const-string v4, "save_manifest"

    const/4 v5, 0x0

    move-wide v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1565
    :cond_0
    return-void
.end method

.method public static logScrubberAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    .prologue
    .line 1276
    const-string v0, "scrubber_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1277
    return-void
.end method

.method public static logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V
    .locals 3
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .prologue
    .line 1264
    const-string v0, "store_action"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1265
    return-void
.end method

.method public static logTextSelectionAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    .prologue
    .line 1291
    const-string v0, "text_selection_action"

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1292
    return-void
.end method

.method public static logTocAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;Ljava/lang/Long;)V
    .locals 1
    .param p0, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 1272
    const-string v0, "toc_action"

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V

    .line 1273
    return-void
.end method

.method public static logTocAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;I)V
    .locals 4
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p1, "position"    # I

    .prologue
    .line 1281
    if-eqz p0, :cond_0

    .line 1282
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getTocAnnotationAction(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    move-result-object v0

    int-to-long v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTocAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;Ljava/lang/Long;)V

    .line 1284
    :cond_0
    return-void
.end method

.method public static logVolumeOom(Ljava/lang/String;)V
    .locals 3
    .param p0, "debugString"    # Ljava/lang/String;

    .prologue
    .line 1578
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1579
    const-string v0, "oom"

    const-string v1, "oom_action"

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendVolumeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1581
    :cond_0
    return-void
.end method

.method private static maybeExtraDebugLog(Ljava/lang/String;)V
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 1336
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sExtraDebugLogging:Z

    if-eqz v0, :cond_0

    .line 1337
    const-string v0, "BooksTracker"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    :cond_0
    return-void
.end method

.method public static maybeLogHomeDisplayTime(Landroid/content/Context;JZ)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "loadTime"    # J
    .param p3, "includingSync"    # Z

    .prologue
    .line 1507
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1508
    if-eqz p0, :cond_0

    .line 1509
    new-instance v6, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 1510
    .local v6, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    if-eqz p3, :cond_1

    const-string v5, "including_sync"

    .line 1512
    .local v5, "label":Ljava/lang/String;
    :goto_0
    invoke-virtual {v6}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHasLoadedLibraryBefore()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1513
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    const-string v1, "timing_ui_home"

    const-string v4, "display_books_first_time"

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1515
    invoke-virtual {v6}, Lcom/google/android/apps/books/preference/LocalPreferences;->setHasLoadedLibraryBefore()V

    .line 1522
    .end local v5    # "label":Ljava/lang/String;
    .end local v6    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_0
    :goto_1
    return-void

    .line 1510
    .restart local v6    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_1
    const-string v5, "not_including_sync"

    goto :goto_0

    .line 1517
    .restart local v5    # "label":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    const-string v1, "timing_ui_home"

    const-string v4, "display_books_not_first_time"

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static varargs maybePlayLog(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "extras"    # [Ljava/lang/String;

    .prologue
    .line 991
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldPlayLog()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 992
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sExtraDebugLogging:Z

    if-eqz v0, :cond_0

    .line 993
    const-string v0, "BooksTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Play logging: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPlayLogger:Lcom/google/android/apps/books/app/BooksEventLogger;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/books/app/BooksEventLogger;->trackEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 997
    :cond_1
    return-void
.end method

.method private static maybePlaylogBooksEvent(Ljava/lang/String;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;Landroid/content/Context;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "type"    # Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1005
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldPlayLog()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1006
    sget-boolean v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sExtraDebugLogging:Z

    if-eqz v2, :cond_0

    .line 1007
    const-string v2, "BooksTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Play logging: PlaylogBooksEvent type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    :cond_0
    if-eqz p2, :cond_1

    .line 1010
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 1011
    .local v1, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;->newBuilder()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    move-result-object v0

    .line 1012
    .local v0, "builder":Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getPlaylogStickyId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->setClientId(J)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    .line 1013
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->setType(Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$EventType;)Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;

    .line 1014
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPlayLogger:Lcom/google/android/apps/books/app/BooksEventLogger;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;->build()Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/books/app/BooksEventLogger;->trackEvent(Ljava/lang/String;Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent;)V

    .line 1017
    .end local v0    # "builder":Lcom/google/android/apps/books/app/proto/Playlog$PlaylogBooksEvent$Builder;
    .end local v1    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_1
    return-void
.end method

.method private static maybeSendHomeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "label"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Long;

    .prologue
    .line 1500
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1501
    const-string v0, "user_action"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1, p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendHomeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1503
    :cond_0
    return-void
.end method

.method private static maybeSendUserAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Long;

    .prologue
    .line 1319
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1320
    const-string v0, "user_action"

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1322
    :cond_0
    return-void
.end method

.method private static maybeSendVolumeUserAction(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Long;)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "label"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Long;

    .prologue
    .line 1325
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-eqz v0, :cond_0

    .line 1326
    const-string v0, "user_action"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1, p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendVolumeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1328
    :cond_0
    return-void
.end method

.method public static pageFlipped()V
    .locals 6

    .prologue
    .line 1190
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldLogPageTurns()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1191
    sget-wide v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    sput-wide v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPagesFlippedSinceLastLog:J

    .line 1192
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1193
    .local v0, "now":J
    sget-wide v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sLastPageTurnLogTime:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1194
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPageFlips(J)V

    .line 1197
    :cond_0
    return-void
.end method

.method public static sendDimensionedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/util/SparseArray;)V
    .locals 1
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1237
    .local p4, "dimensions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-static {p4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getDimensionedTracker(Landroid/util/SparseArray;)Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/analytics/tracking/android/Tracker;->sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1238
    return-void
.end method

.method private static sendDimensionedTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Landroid/util/SparseArray;)V
    .locals 7
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "intervalInMilliseconds"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1609
    .local p5, "dimensions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-static {p5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->getDimensionedTracker(Landroid/util/SparseArray;)Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/analytics/tracking/android/Tracker;->sendTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1610
    return-void
.end method

.method private static sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Long;

    .prologue
    .line 1331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeExtraDebugLog(Ljava/lang/String;)V

    .line 1332
    invoke-static {}, Lcom/google/analytics/tracking/android/EasyTracker;->getTracker()Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/analytics/tracking/android/Tracker;->sendEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1333
    return-void
.end method

.method private static sendHomeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Long;

    .prologue
    .line 1232
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sHomeDimensions:Landroid/util/SparseArray;

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendDimensionedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/util/SparseArray;)V

    .line 1233
    return-void
.end method

.method private static sendReadingTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "intervalInMilliseconds"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;

    .prologue
    .line 1601
    sget-object v6, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sVolumeDimensions:Landroid/util/SparseArray;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendDimensionedTiming(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 1602
    return-void
.end method

.method private static sendVolumeEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Long;

    .prologue
    .line 1228
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sVolumeDimensions:Landroid/util/SparseArray;

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sendDimensionedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/util/SparseArray;)V

    .line 1229
    return-void
.end method

.method private static shouldLogPageTurns()Z
    .locals 1

    .prologue
    .line 1209
    sget-boolean v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sShouldLogAnalytics:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->shouldPlayLog()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static shouldPlayLog()Z
    .locals 1

    .prologue
    .line 1020
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->sPlayLogger:Lcom/google/android/apps/books/app/BooksEventLogger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static shouldSubsampleGoogleAnalytics(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 928
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->GOOGLE_ANALYTICS_SAMPLE_RATE:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getInt(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
