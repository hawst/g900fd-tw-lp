.class public interface abstract Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;
.super Ljava/lang/Object;
.source "TextModeSearchMatchRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RenderCallbacks"
.end annotation


# virtual methods
.method public abstract getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;
.end method

.method public abstract getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;
.end method

.method public abstract getTextSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/TextPageHandle;",
            ">;"
        }
    .end annotation
.end method
