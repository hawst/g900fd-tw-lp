.class public Lcom/google/android/apps/books/tts/SegmentedText;
.super Ljava/lang/Object;
.source "SegmentedText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/tts/SegmentedText$1;
    }
.end annotation


# instance fields
.field private final mLocale:Ljava/util/Locale;

.field private final mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

.field private final mText:Ljava/lang/String;

.field private final mUnitToSegmentation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/tts/TtsUnit;",
            "Lcom/google/android/apps/books/tts/Segmentation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mUnitToSegmentation:Ljava/util/Map;

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mLocale:Ljava/util/Locale;

    .line 43
    return-void
.end method

.method private getSegmentation(Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/Segmentation;
    .locals 5
    .param p1, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;

    .prologue
    .line 70
    iget-object v2, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mUnitToSegmentation:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/tts/Segmentation;

    .line 71
    .local v0, "result":Lcom/google/android/apps/books/tts/Segmentation;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 86
    .end local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    .local v1, "result":Lcom/google/android/apps/books/tts/Segmentation;
    :goto_0
    return-object v1

    .line 74
    .end local v1    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    .restart local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/tts/SegmentedText$1;->$SwitchMap$com$google$android$apps$books$tts$TtsUnit:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/tts/TtsUnit;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 85
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mUnitToSegmentation:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 86
    .end local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    .restart local v1    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    goto :goto_0

    .line 76
    .end local v1    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    .restart local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    :pswitch_0
    new-instance v0, Lcom/google/android/apps/books/tts/CharacterSegmentation;

    .end local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    iget-object v2, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/tts/CharacterSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 77
    .restart local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    goto :goto_1

    .line 79
    :pswitch_1
    new-instance v0, Lcom/google/android/apps/books/tts/WordSegmentation;

    .end local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    iget-object v2, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    iget-object v4, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mLocale:Ljava/util/Locale;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/books/tts/WordSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V

    .line 80
    .restart local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    goto :goto_1

    .line 82
    :pswitch_2
    new-instance v0, Lcom/google/android/apps/books/tts/SentenceSegmentation;

    .end local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    iget-object v2, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    iget-object v4, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mLocale:Ljava/util/Locale;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/books/tts/SentenceSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V

    .restart local v0    # "result":Lcom/google/android/apps/books/tts/Segmentation;
    goto :goto_1

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getCharacterOffsetAtPosition(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LabelMap;->getCharacterOffsetAt(I)I

    move-result v0

    return v0
.end method

.method public getItemCount(Lcom/google/android/apps/books/tts/TtsUnit;)I
    .locals 1
    .param p1, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/SegmentedText;->getSegmentation(Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/Segmentation;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/tts/Segmentation;->getItemCount()I

    move-result v0

    return v0
.end method

.method public getItemRange(Lcom/google/android/apps/books/tts/TtsUnit;I)Lcom/google/android/apps/books/util/Range;
    .locals 1
    .param p1, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;
    .param p2, "index"    # I

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/SegmentedText;->getSegmentation(Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/Segmentation;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/tts/Segmentation;->getItemRange(I)Lcom/google/android/apps/books/util/Range;

    move-result-object v0

    return-object v0
.end method

.method public getItemText(Lcom/google/android/apps/books/tts/TtsUnit;I)Ljava/lang/String;
    .locals 1
    .param p1, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;
    .param p2, "index"    # I

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/SegmentedText;->getSegmentation(Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/Segmentation;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/tts/Segmentation;->getItemText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getNearestItemIndex(Lcom/google/android/apps/books/tts/TtsUnit;I)I
    .locals 1
    .param p1, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;
    .param p2, "characterIndex"    # I

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/SegmentedText;->getSegmentation(Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/Segmentation;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/tts/Segmentation;->getNearestItemIndex(I)I

    move-result v0

    return v0
.end method

.method public getNearestItemIndex(Lcom/google/android/apps/books/tts/TtsUnit;Lcom/google/android/apps/books/common/Position;)Landroid/util/Pair;
    .locals 1
    .param p1, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/tts/TtsUnit;",
            "Lcom/google/android/apps/books/common/Position;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/SegmentedText;->getSegmentation(Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/Segmentation;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/tts/Segmentation;->getNearestItemIndex(Lcom/google/android/apps/books/common/Position;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public getNormalizedRange(Lcom/google/android/apps/books/util/Range;I)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 17
    .param p1, "phraseRange"    # Lcom/google/android/apps/books/util/Range;
    .param p2, "passageIndex"    # I

    .prologue
    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/tts/SegmentedText;->getNumberOfPositions()I

    move-result v7

    .line 127
    .local v7, "numPositions":I
    if-nez v7, :cond_1

    .line 128
    const-string v14, "BooksSegmentedText"

    const/4 v15, 0x5

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 129
    const-string v14, "BooksSegmentedText"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "No reading positions found in passage "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "?! Will not highlight text."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_0
    const/4 v14, 0x0

    .line 172
    :goto_0
    return-object v14

    .line 135
    :cond_1
    const/4 v11, 0x0

    .line 136
    .local v11, "startPositionIndex":I
    const/4 v5, 0x0

    .line 140
    .local v5, "endPositionIndex":I
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_1
    if-ge v6, v7, :cond_4

    .line 141
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/tts/SegmentedText;->getCharacterOffsetAtPosition(I)I

    move-result v1

    .line 143
    .local v1, "charPos":I
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/books/util/Range;->start:I

    if-gt v1, v14, :cond_2

    move v11, v6

    .line 145
    :cond_2
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/books/util/Range;->end:I

    if-ge v1, v14, :cond_3

    move v5, v6

    .line 146
    :cond_3
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/books/util/Range;->end:I

    if-lt v1, v14, :cond_6

    .line 149
    .end local v1    # "charPos":I
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/tts/SegmentedText;->getCharacterOffsetAtPosition(I)I

    move-result v10

    .line 150
    .local v10, "startPositionCharOffset":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/tts/SegmentedText;->getCharacterOffsetAtPosition(I)I

    move-result v4

    .line 152
    .local v4, "endPositionCharOffset":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v15, v0, Lcom/google/android/apps/books/util/Range;->start:I

    invoke-static {v14, v10, v15}, Lcom/google/android/apps/books/util/ReaderUtils;->countWhiteSpaceChars(Ljava/lang/String;II)I

    move-result v13

    .line 154
    .local v13, "whiteCharsBeforeStart":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v15, v0, Lcom/google/android/apps/books/util/Range;->end:I

    invoke-static {v14, v4, v15}, Lcom/google/android/apps/books/util/ReaderUtils;->countWhiteSpaceChars(Ljava/lang/String;II)I

    move-result v12

    .line 157
    .local v12, "whiteCharsBeforeEnd":I
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/books/util/Range;->start:I

    sub-int/2addr v14, v10

    sub-int v8, v14, v13

    .line 158
    .local v8, "startOffset":I
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/books/util/Range;->end:I

    sub-int/2addr v14, v4

    sub-int v2, v14, v12

    .line 160
    .local v2, "endOffset":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/tts/SegmentedText;->getReadingPositionByIndex(I)Ljava/lang/String;

    move-result-object v9

    .line 161
    .local v9, "startPosition":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/tts/SegmentedText;->getReadingPositionByIndex(I)Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "endPosition":Ljava/lang/String;
    const-string v14, "BooksSegmentedText"

    const/4 v15, 0x2

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 164
    const-string v14, "BooksSegmentedText"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getNormalizedRange range: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/apps/books/util/Range;->start:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/apps/books/util/Range;->end:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const-string v14, "BooksSegmentedText"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getNormalizedRange map: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/model/LabelMap;->debugString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v14, "BooksSegmentedText"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getNormalizedRange startPositionIndex: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " endPositionIndex: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " startPosition: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " endPosition: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " startOffset: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " endOffset: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_5
    new-instance v14, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v14, v9, v8, v3, v2}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_0

    .line 140
    .end local v2    # "endOffset":I
    .end local v3    # "endPosition":Ljava/lang/String;
    .end local v4    # "endPositionCharOffset":I
    .end local v8    # "startOffset":I
    .end local v9    # "startPosition":Ljava/lang/String;
    .end local v10    # "startPositionCharOffset":I
    .end local v12    # "whiteCharsBeforeEnd":I
    .end local v13    # "whiteCharsBeforeStart":I
    .restart local v1    # "charPos":I
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1
.end method

.method public getNumberOfPositions()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/LabelMap;->getNumberOfLabels()I

    move-result v0

    return v0
.end method

.method getPositionOffsets()Lcom/google/android/apps/books/model/LabelMap;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    return-object v0
.end method

.method public getReadingPositionByIndex(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mPositionOffsets:Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LabelMap;->getLabelAt(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/tts/SegmentedText;->mText:Ljava/lang/String;

    return-object v0
.end method
