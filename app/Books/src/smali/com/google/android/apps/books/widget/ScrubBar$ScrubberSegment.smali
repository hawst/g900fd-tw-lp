.class Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;
.super Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
.source "ScrubBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ScrubBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScrubberSegment"
.end annotation


# instance fields
.field private mTop:I

.field private mView:Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/ScrubBar;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/ScrubBar;IIILandroid/view/View;)V
    .locals 0
    .param p2, "center"    # I
    .param p3, "width"    # I
    .param p4, "top"    # I
    .param p5, "view"    # Landroid/view/View;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    .line 147
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;-><init>(II)V

    .line 148
    iput p4, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mTop:I

    .line 149
    iput-object p5, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mView:Landroid/view/View;

    .line 150
    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mView:Landroid/view/View;

    return-object v0
.end method

.method private realViewLeft()I
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->center:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public layoutAndShow()V
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->this$0:Lcom/google/android/apps/books/widget/ScrubBar;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mView:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->realViewLeft()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mTop:I

    # invokes: Lcom/google/android/apps/books/widget/ScrubBar;->layoutChild(Landroid/view/View;II)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->access$000(Lcom/google/android/apps/books/widget/ScrubBar;Landroid/view/View;II)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    return-void
.end method
