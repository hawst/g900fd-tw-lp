.class Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/libraries/happiness/HatsSurveyClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HatsCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 2922
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/HomeFragment$1;

    .prologue
    .line 2922
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    return-void
.end method


# virtual methods
.method public onSurveyCanceled()V
    .locals 1

    .prologue
    .line 2953
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->dismissHatsSurvey()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$5200(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 2954
    return-void
.end method

.method public onSurveyComplete(ZZ)V
    .locals 3
    .param p1, "justAnswered"    # Z
    .param p2, "unused"    # Z

    .prologue
    .line 2936
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->dismissHatsSurvey()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$5200(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 2937
    if-eqz p1, :cond_0

    .line 2938
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2939
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 2940
    const v1, 0x7f0f0215

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2944
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    return-void
.end method

.method public onSurveyReady()V
    .locals 1

    .prologue
    .line 2931
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->maybeShowHatsPrompt()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$5100(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 2932
    return-void
.end method

.method public onSurveyResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "surveyId"    # Ljava/lang/String;

    .prologue
    .line 2949
    return-void
.end method

.method public onWindowError()V
    .locals 0

    .prologue
    .line 2927
    return-void
.end method
