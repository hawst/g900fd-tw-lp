.class public interface abstract Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
.super Ljava/lang/Object;
.source "WebViewRendererModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRendererModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract canDrawPages()Z
.end method

.method public abstract dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V
.end method

.method public abstract dispatchRenderError(Ljava/lang/Exception;)V
.end method

.method public abstract drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V
.end method

.method public abstract getPageSize(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Point;
.end method
