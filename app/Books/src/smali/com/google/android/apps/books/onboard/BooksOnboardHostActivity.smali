.class public Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;
.super Lcom/google/android/apps/books/app/BaseBooksActivity;
.source "BooksOnboardHostActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/BooksFragmentCallbacks;",
        ">;"
    }
.end annotation


# static fields
.field private static final FRAGMENT_TAG:Ljava/lang/String;


# instance fields
.field private mAddedFragments:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;-><init>()V

    return-void
.end method

.method private hideStatusBar()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 53
    .local v0, "decorView":Landroid/view/View;
    const/16 v1, 0x404

    .line 55
    .local v1, "uiOptions":I
    const/16 v2, 0x404

    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 56
    return-void
.end method


# virtual methods
.method public getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getThemeId(Z)I
    .locals 1
    .param p1, "dark"    # Z

    .prologue
    .line 60
    const v0, 0x7f0a01e8

    return v0
.end method

.method protected handleIntent()V
    .locals 4

    .prologue
    .line 103
    const/4 v1, 0x0

    .line 104
    .local v1, "sequenceType":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 105
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 106
    const-string v2, "OnboardIntentBuilder_sequenceType"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 110
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->initializeFragment(I)V

    .line 111
    return-void
.end method

.method protected hostFragment()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    return-object v0
.end method

.method protected initializeFragment(I)V
    .locals 6
    .param p1, "sequenceType"    # I

    .prologue
    const/4 v5, 0x1

    .line 89
    iget-boolean v3, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->mAddedFragments:Z

    if-nez v3, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 91
    .local v0, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 92
    .local v2, "transaction":Landroid/support/v4/app/FragmentTransaction;
    new-instance v1, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    invoke-direct {v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;-><init>()V

    .line 93
    .local v1, "newFragment":Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->setSequenceType(I)V

    .line 94
    invoke-virtual {v1, v5}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->setRetainInstance(Z)V

    .line 96
    const v3, 0x1020002

    sget-object v4, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 97
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 98
    iput-boolean v5, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->mAddedFragments:Z

    .line 100
    .end local v0    # "manager":Landroid/support/v4/app/FragmentManager;
    .end local v1    # "newFragment":Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;
    .end local v2    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->hostFragment()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onBackPressed()V

    .line 123
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->hideStatusBar()V

    .line 41
    :cond_0
    if-eqz p1, :cond_1

    .line 42
    const-string v0, "OnboardActivity.addedFragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->mAddedFragments:Z

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->handleIntent()V

    .line 48
    return-void
.end method

.method public onFinishedOnboardFlow()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->setResult(I)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->finish()V

    .line 131
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 71
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 76
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 73
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->finish()V

    .line 74
    const/4 v0, 0x1

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 66
    const-string v0, "OnboardActivity.addedFragments"

    iget-boolean v1, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->mAddedFragments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 67
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method protected onSelectedAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->mAddedFragments:Z

    if-nez v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;->handleIntent()V

    .line 84
    :cond_0
    return-void
.end method
