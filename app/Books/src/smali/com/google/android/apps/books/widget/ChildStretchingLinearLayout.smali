.class public Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;
.super Landroid/widget/LinearLayout;
.source "ChildStretchingLinearLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method private getChildWidthSpec(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;I)I
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "params"    # Landroid/widget/LinearLayout$LayoutParams;
    .param p3, "maxWidth"    # I

    .prologue
    .line 99
    iget v0, p2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    if-lez v0, :cond_0

    .line 100
    iget v0, p2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 102
    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x80000000

    invoke-static {p3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private isHorizontallyStretching(Landroid/widget/LinearLayout$LayoutParams;)Z
    .locals 2
    .param p1, "lp"    # Landroid/widget/LinearLayout$LayoutParams;

    .prologue
    .line 91
    iget v0, p1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    if-nez v0, :cond_0

    iget v0, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 38
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    .line 40
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->getChildCount()I

    move-result v1

    .line 43
    .local v1, "childCount":I
    const/4 v7, 0x0

    .line 44
    .local v7, "maxHeight":I
    move v8, v10

    .line 45
    .local v8, "remainingWidth":I
    const/4 v5, 0x0

    .line 46
    .local v5, "horizontallyStretchingChildren":I
    const/4 v2, 0x0

    .local v2, "childIndex":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 47
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 50
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 52
    .local v6, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-direct {p0, v0, v6, v8}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->getChildWidthSpec(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;I)I

    move-result v3

    .line 54
    .local v3, "childWidthSpec":I
    invoke-virtual {v0, v3, p2}, Landroid/view/View;->measure(II)V

    .line 55
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-static {v7, v11}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 58
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->isHorizontallyStretching(Landroid/widget/LinearLayout$LayoutParams;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 59
    add-int/lit8 v5, v5, 0x1

    .line 46
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    sub-int/2addr v8, v11

    goto :goto_1

    .line 67
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "childWidthSpec":I
    .end local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    if-lez v5, :cond_2

    div-int v9, v8, v5

    .line 70
    .local v9, "stretchingChildWidth":I
    :goto_2
    move v8, v10

    .line 71
    invoke-static {v7, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 72
    .local v4, "fixedHeightSpec":I
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_4

    .line 73
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 74
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 77
    .restart local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->isHorizontallyStretching(Landroid/widget/LinearLayout$LayoutParams;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 78
    invoke-static {v9, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 83
    .restart local v3    # "childWidthSpec":I
    :goto_4
    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 84
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    sub-int/2addr v8, v11

    .line 72
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 67
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "childWidthSpec":I
    .end local v4    # "fixedHeightSpec":I
    .end local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v9    # "stretchingChildWidth":I
    :cond_2
    const/4 v9, -0x1

    goto :goto_2

    .line 81
    .restart local v0    # "child":Landroid/view/View;
    .restart local v4    # "fixedHeightSpec":I
    .restart local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v9    # "stretchingChildWidth":I
    :cond_3
    invoke-direct {p0, v0, v6, v8}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->getChildWidthSpec(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;I)I

    move-result v3

    .restart local v3    # "childWidthSpec":I
    goto :goto_4

    .line 87
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "childWidthSpec":I
    .end local v6    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    invoke-virtual {p0, v10, v7}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->setMeasuredDimension(II)V

    .line 88
    return-void
.end method
