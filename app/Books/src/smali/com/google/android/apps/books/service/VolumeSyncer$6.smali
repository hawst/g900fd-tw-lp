.class Lcom/google/android/apps/books/service/VolumeSyncer$6;
.super Ljava/lang/Object;
.source "VolumeSyncer.java"

# interfaces
.implements Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/service/VolumeSyncer;->planSegment(Lcom/google/android/apps/books/model/Segment;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

.field final synthetic val$ignoreResources:Z

.field final synthetic val$segment:Lcom/google/android/apps/books/model/Segment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/model/Segment;Z)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    iput-object p2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iput-boolean p3, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->val$ignoreResources:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ensure()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # getter for: Lcom/google/android/apps/books/service/VolumeSyncer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v2}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$400(Lcom/google/android/apps/books/service/VolumeSyncer;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # invokes: Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$500(Lcom/google/android/apps/books/service/VolumeSyncer;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iget-boolean v5, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->val$ignoreResources:Z

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureSegmentContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Z)Ljava/util/List;

    move-result-object v1

    .line 310
    .local v1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$6;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # invokes: Lcom/google/android/apps/books/service/VolumeSyncer;->fetchNewResources(Ljava/util/List;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$600(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    return-void

    .line 311
    .end local v1    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v2

    throw v2
.end method
