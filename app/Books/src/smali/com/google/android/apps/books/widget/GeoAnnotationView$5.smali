.class Lcom/google/android/apps/books/widget/GeoAnnotationView$5;
.super Ljava/lang/Object;
.source "GeoAnnotationView.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/GeoAnnotationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/annotations/AnnotationData;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 354
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # setter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$602(Lcom/google/android/apps/books/widget/GeoAnnotationView;Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$600(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$900(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$600(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationData;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    invoke-interface {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataPayload;->getCommon()Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->previewImageUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # invokes: Lcom/google/android/apps/books/widget/GeoAnnotationView;->getCachePolicy()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$700(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mBitmapConsumer:Lcom/google/android/ublib/utils/Consumer;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$800(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->fetchMapPreviewImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V

    .line 362
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$500(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # getter for: Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$600(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;->onFinishedLoading(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 351
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
