.class public Lcom/google/android/apps/books/widget/HeaderGridViewController;
.super Ljava/lang/Object;
.source "HeaderGridViewController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;,
        Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderViewHolder;
    }
.end annotation


# instance fields
.field private mAccessibilitySkipCount:I

.field private mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v7/widget/RecyclerView$Adapter",
            "<",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;

.field private final mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

.field private final mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

.field private final mRecyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    .line 38
    new-instance v0, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;-><init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    .line 107
    new-instance v0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;-><init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 146
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 148
    new-instance v0, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/widget/HeaderGridViewController$2;-><init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;

    new-instance v1, Lcom/google/android/apps/books/widget/HeaderGridViewController$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$3;-><init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/GridLayoutManager;->setSpanSizeLookup(Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 176
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/RecyclerView$Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getAdapterCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    iget v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAccessibilitySkipCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/GridLayoutManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HeaderGridViewController;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;

    return-object v0
.end method

.method private getAdapterCount()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHeaderCount()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public addHeaderView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderViewsCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->addHeaderView(Landroid/view/View;I)V

    .line 218
    return-void
.end method

.method public addHeaderView(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->notifyItemInserted(I)V

    .line 211
    return-void
.end method

.method public getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v7/widget/RecyclerView$Adapter",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    return-object v0
.end method

.method public getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mRecyclerView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public removeHeaderView(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 224
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 225
    .local v0, "pos":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaders:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->notifyItemRemoved(I)V

    .line 228
    const/4 v1, 0x1

    .line 230
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAccessibilitySkipCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 246
    iput p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAccessibilitySkipCount:I

    .line 247
    return-void
.end method

.method public setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eq v0, p1, :cond_2

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->unregisterAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 192
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    .line 193
    if-eqz p1, :cond_1

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->notifyDataSetChanged()V

    .line 198
    :cond_2
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "columnCount"    # I

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController;->mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/GridLayoutManager;->setSpanCount(I)V

    .line 239
    return-void
.end method
