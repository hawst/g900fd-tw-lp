.class public Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
.super Ljava/lang/Object;
.source "SelectionPopupImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SelectionPopupImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SelectionBoundsInfo"
.end annotation


# instance fields
.field private final mBounds:Landroid/graphics/Rect;

.field private final mHandleAnchorPoint:Landroid/graphics/Point;

.field private final mHandleLongerAxis:I


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;ILandroid/graphics/Point;)V
    .locals 0
    .param p1, "bounds"    # Landroid/graphics/Rect;
    .param p2, "handleMajor"    # I
    .param p3, "handleAnchorPoint"    # Landroid/graphics/Point;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mBounds:Landroid/graphics/Rect;

    .line 34
    iput p2, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mHandleLongerAxis:I

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mHandleAnchorPoint:Landroid/graphics/Point;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mHandleAnchorPoint:Landroid/graphics/Point;

    return-object v0
.end method


# virtual methods
.method public getBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getHandleLongerAxis()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mHandleLongerAxis:I

    return v0
.end method
