.class public Lcom/google/android/apps/books/render/ImageModeRenderer;
.super Lcom/google/android/apps/books/render/ReaderRenderer;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/ImageModeRenderer$6;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$StreamOpeningConsumer;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;,
        Lcom/google/android/apps/books/render/ImageModeRenderer$Config;
    }
.end annotation


# static fields
.field private static final mStaticTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;


# instance fields
.field private final mBitmapDecodeExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final mCanFitWidth:Z

.field private final mConfig:Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mDesatContrastPaint:Landroid/graphics/Paint;

.field private final mDisplayTwoPages:Z

.field private final mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

.field private final mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

.field private final mHighlightsByColorWalker:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mMakePagesAccessible:Z

.field private final mMarginNoteIconWalker:Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;

.field private mMaxImageConfig:Landroid/graphics/Bitmap$Config;

.field private mNextRequestId:I

.field private final mNightPaint:Landroid/graphics/Paint;

.field private final mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageDimensions:Landroid/graphics/Point;

.field private final mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRendererCoordinatesToPagePointsMatrix:Landroid/graphics/Matrix;

.field private final mSelectionStateCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

.field private final mSepiaTonePaint:Landroid/graphics/Paint;

.field private final mShouldFitWidth:Z

.field private final mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

.field private final mTempPageFit:Lcom/google/android/apps/books/util/PageFit;

.field private final mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

.field private final mTempPointF:Landroid/graphics/PointF;

.field private volatile mTheme:Ljava/lang/String;

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1458
    new-instance v0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mStaticTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Landroid/graphics/Point;ZLcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)V
    .locals 8
    .param p1, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p4, "screenSize"    # Landroid/graphics/Point;
    .param p5, "makePagesAccessible"    # Z
    .param p6, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p7, "config"    # Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 238
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/render/ReaderRenderer;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 109
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    .line 112
    const-string v1, "ImageModeRenderer"

    invoke-static {v7}, Lcom/google/android/apps/books/render/ImageModeRenderer;->computeMaxDecodeThreads(I)I

    move-result v4

    const/16 v5, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v4, v5, v6}, Lcom/google/android/apps/books/util/ExecutorUtils;->createThreadPoolExecutor(Ljava/lang/String;IILjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mBitmapDecodeExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 115
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPaint:Landroid/graphics/Paint;

    .line 116
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mNightPaint:Landroid/graphics/Paint;

    .line 117
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDesatContrastPaint:Landroid/graphics/Paint;

    .line 118
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mSepiaTonePaint:Landroid/graphics/Paint;

    .line 125
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPointF:Landroid/graphics/PointF;

    .line 126
    new-instance v1, Lcom/google/android/apps/books/util/PageFit;

    invoke-direct {v1}, Lcom/google/android/apps/books/util/PageFit;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageFit:Lcom/google/android/apps/books/util/PageFit;

    .line 127
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;

    .line 129
    iput v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mNextRequestId:I

    .line 412
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMaxImageConfig:Landroid/graphics/Bitmap$Config;

    .line 458
    new-instance v1, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    .line 1079
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mRendererCoordinatesToPagePointsMatrix:Landroid/graphics/Matrix;

    .line 1159
    new-instance v1, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMarginNoteIconWalker:Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;

    .line 1160
    new-instance v1, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mHighlightsByColorWalker:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .line 1163
    new-instance v1, Lcom/google/android/apps/books/render/ImageModeRenderer$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$5;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;)V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mSelectionStateCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

    .line 1435
    new-array v1, v7, [Lcom/google/android/apps/books/render/PageIdentifier;

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    .line 239
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/BooksDataController;

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 240
    invoke-static {p7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mConfig:Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    .line 249
    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getLandscape()Z
    invoke-static {p7}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->access$000(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)Z

    move-result v0

    .line 250
    .local v0, "isLandscape":Z
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mCanFitWidth:Z

    .line 251
    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isFitWidth()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mShouldFitWidth:Z

    .line 252
    if-eqz v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isFitWidth()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    .line 255
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mNightPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-static {}, Lcom/google/android/apps/books/render/ImageModeRenderer;->buildDesaturateAndInvert()Landroid/graphics/ColorMatrix;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDesatContrastPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-static {}, Lcom/google/android/apps/books/render/ImageModeRenderer;->buildDesaturateAndContrast()Landroid/graphics/ColorMatrix;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 262
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mSepiaTonePaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-static {}, Lcom/google/android/apps/books/render/ImageModeRenderer;->buildSepia()Landroid/graphics/ColorMatrix;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 264
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 266
    new-instance v2, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->displayTwoPages()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p4, Landroid/graphics/Point;->x:I

    div-int/lit8 v1, v1, 0x2

    :goto_2
    iget v3, p4, Landroid/graphics/Point;->y:I

    invoke-direct {v2, v1, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageDimensions:Landroid/graphics/Point;

    .line 268
    new-instance v1, Lcom/google/android/apps/books/render/HighlightPaintBox;

    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getDefaultDrawingColor()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/render/HighlightPaintBox;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

    .line 270
    iput-boolean p5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMakePagesAccessible:Z

    .line 271
    iput-object p6, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 273
    new-instance v1, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;-><init>(Ljava/util/List;ZZ)V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    .line 276
    new-instance v1, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;-><init>(Ljava/util/List;I)V

    iput-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    .line 278
    return-void

    :cond_0
    move v1, v3

    .line 251
    goto/16 :goto_0

    :cond_1
    move v2, v3

    .line 252
    goto :goto_1

    .line 266
    :cond_2
    iget v1, p4, Landroid/graphics/Point;->x:I

    goto :goto_2
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverErrorOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/io/InputStream;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p2, "x2"    # Ljava/io/InputStream;
    .param p3, "x3"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->handleRequestLoadResults(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/io/InputStream;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mBitmapDecodeExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverResult(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getTheme()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/HighlightPaintBox;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/PageStructureSelection;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/PageStructureSelection;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/ImageModeRenderer;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageDimensions:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageStructure(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->canAbandon(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z

    move-result v0

    return v0
.end method

.method private allocateRequestId()I
    .locals 2

    .prologue
    .line 1421
    iget v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mNextRequestId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mNextRequestId:I

    return v0
.end method

.method public static buildDesaturateAndContrast()Landroid/graphics/ColorMatrix;
    .locals 5

    .prologue
    .line 1032
    new-instance v1, Landroid/graphics/ColorMatrix;

    invoke-direct {v1}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 1033
    .local v1, "desaturate":Landroid/graphics/ColorMatrix;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 1036
    const/high16 v0, 0x3fa00000    # 1.25f

    .line 1037
    .local v0, "contrast":F
    const/high16 v2, -0x3e010000    # -31.875f

    .line 1038
    .local v2, "offset":F
    new-instance v3, Landroid/graphics/ColorMatrix;

    const/16 v4, 0x14

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-direct {v3, v4}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-virtual {v1, v3}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 1045
    return-object v1

    .line 1038
    nop

    :array_0
    .array-data 4
        0x3fa00000    # 1.25f
        0x0
        0x0
        0x0
        -0x3e010000    # -31.875f
        0x3fa00000    # 1.25f
        0x0
        0x0
        0x0
        -0x3e010000    # -31.875f
        0x3fa00000    # 1.25f
        0x0
        0x0
        0x0
        -0x3e010000    # -31.875f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static buildDesaturateAndInvert()Landroid/graphics/ColorMatrix;
    .locals 2

    .prologue
    .line 1013
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 1014
    .local v0, "desaturate":Landroid/graphics/ColorMatrix;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 1015
    invoke-static {}, Lcom/google/android/apps/books/render/ImageModeRenderer;->buildInvert()Landroid/graphics/ColorMatrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 1016
    return-object v0
.end method

.method public static buildInvert()Landroid/graphics/ColorMatrix;
    .locals 2

    .prologue
    .line 1020
    new-instance v0, Landroid/graphics/ColorMatrix;

    const/16 v1, 0x14

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 1028
    .local v0, "invert":Landroid/graphics/ColorMatrix;
    return-object v0

    .line 1020
    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static buildSepia()Landroid/graphics/ColorMatrix;
    .locals 2

    .prologue
    .line 1050
    const-string v0, "2"

    invoke-static {v0}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedForegroundColor(Ljava/lang/String;)I

    move-result v0

    const-string v1, "2"

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->generateLinearInterpolationMatrix(II)Landroid/graphics/ColorMatrix;

    move-result-object v0

    return-object v0
.end method

.method private calculateFirstPositionOnNextPage(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/common/Position;
    .locals 4
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "requestPosition"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1002
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v0

    .line 1003
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    invoke-virtual {p2}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v1

    .line 1005
    .local v1, "thisPageIndex":I
    add-int/lit8 v2, v1, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 1006
    const/4 v2, 0x0

    .line 1008
    :goto_0
    return-object v2

    :cond_0
    new-instance v3, Lcom/google/android/apps/books/common/Position;

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_0
.end method

.method private canAbandon(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .prologue
    .line 727
    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->isCanceled()Z
    invoke-static {p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->access$800(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private computeBitmapFactoryOptions(Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;
    .locals 3
    .param p1, "requestedConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 721
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMaxImageConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->smallerConfig(Landroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 722
    .local v0, "config":Landroid/graphics/Bitmap$Config;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->makeBitmapFactoryOptions(Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    .line 723
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    return-object v1
.end method

.method private static computeMaxDecodeThreads(I)I
    .locals 2
    .param p0, "memoryConstrainedMax"    # I

    .prologue
    .line 453
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 455
    .local v0, "spareOneCore":I
    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public static computeRightMargin(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;D)I
    .locals 11
    .param p0, "jsonPage"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p1, "minMarginRatio"    # D

    .prologue
    .line 1217
    const/4 v4, 0x0

    .line 1218
    .local v4, "rightmostBlock":I
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v5

    .line 1219
    .local v5, "structure":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v5}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockCount()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 1220
    invoke-virtual {v5, v2}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlock(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    .line 1221
    .local v0, "blockBox":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v8

    add-int v1, v7, v8

    .line 1222
    .local v1, "blockRight":I
    if-le v1, v4, :cond_0

    .line 1223
    move v4, v1

    .line 1219
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1226
    .end local v0    # "blockBox":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .end local v1    # "blockRight":I
    :cond_1
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getW()I

    move-result v6

    .line 1227
    .local v6, "width":I
    sub-int v3, v6, v4

    .line 1228
    .local v3, "marginNextToBlock":I
    int-to-double v8, v6

    mul-double/2addr v8, p1

    double-to-int v7, v8

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    return v7
.end method

.method public static decodeUnencryptedBitmap(Landroid/graphics/BitmapFactory$Options;Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/BitmapDecodeException;
        }
    .end annotation

    .prologue
    .line 1389
    :try_start_0
    const-string v2, "ImageModeRenderer"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1390
    const-string v2, "ImageModeRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Allocating page bitmap with config "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1394
    :cond_0
    :try_start_1
    const-string v2, "BooksContentFetcher#decodeBitmap"

    const/4 v3, 0x0

    invoke-static {v2, p1, v3, p0}, Lcom/google/android/apps/books/util/BitmapUtils;->decodeStreamInReader(Ljava/lang/String;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Lcom/google/android/apps/books/util/BitmapDecodeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1413
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    :try_start_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1416
    :goto_1
    return-object v0

    .line 1396
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 1399
    .local v1, "e":Lcom/google/android/apps/books/util/BitmapDecodeException;
    :try_start_3
    iget-object v2, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v3, :cond_2

    .line 1400
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1401
    const-string v2, "ImageModeRenderer"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1402
    const-string v2, "ImageModeRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Allocating page bitmap with config "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1404
    :cond_1
    const-string v2, "BooksContentFetcher#decodeBitmap"

    const/4 v3, 0x0

    invoke-static {v2, p1, v3, p0}, Lcom/google/android/apps/books/util/BitmapUtils;->decodeStreamInReader(Ljava/lang/String;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 1407
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1412
    .end local v1    # "e":Lcom/google/android/apps/books/util/BitmapDecodeException;
    :catchall_0
    move-exception v2

    .line 1413
    :try_start_4
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1416
    :goto_2
    throw v2

    .line 1414
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v1

    .line 1415
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "ImageModeRenderer"

    const-string v3, "Error when closing"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1414
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 1415
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v3, "ImageModeRenderer"

    const-string v4, "Error when closing"

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private deliverErrorOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p2, "error"    # Ljava/lang/Exception;

    .prologue
    .line 858
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->makeErrorResult(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverResultOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V

    .line 859
    return-void
.end method

.method private deliverPageOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Landroid/graphics/Bitmap;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 11
    .param p1, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    .param p4, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 825
    iget-object v1, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/RenderPosition;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v8

    .line 826
    .local v8, "normalizedPosition":Lcom/google/android/apps/books/common/Position;
    invoke-direct {p0, p4, v8}, Lcom/google/android/apps/books/render/ImageModeRenderer;->calculateFirstPositionOnNextPage(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/common/Position;

    move-result-object v6

    .line 830
    .local v6, "firstPositionOnNextPage":Lcom/google/android/apps/books/common/Position;
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMakePagesAccessible:Z

    if-eqz v1, :cond_0

    .line 831
    new-instance v7, Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {v7}, Lcom/google/android/apps/books/render/PageStructureSelection;-><init>()V

    .line 832
    .local v7, "helper":Lcom/google/android/apps/books/render/PageStructureSelection;
    invoke-virtual {p3}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v9

    .line 833
    .local v9, "pageStructure":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-virtual {v7, v9}, Lcom/google/android/apps/books/render/PageStructureSelection;->selectAll(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V

    .line 834
    invoke-virtual {v7}, Lcom/google/android/apps/books/render/PageStructureSelection;->getSelectedText()Ljava/lang/String;

    move-result-object v10

    .line 839
    .end local v7    # "helper":Lcom/google/android/apps/books/render/PageStructureSelection;
    .end local v9    # "pageStructure":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .local v10, "pageText":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/google/android/apps/books/widget/DevicePageRendering;->newBuilder()Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/RenderPosition;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setIndices(Lcom/google/android/apps/books/render/PageIndices;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/books/common/Position;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setViewablePositions([Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setFirstPositionOnNextPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setPageText(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setPageStructure(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->build()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v2

    .line 847
    .local v2, "devicePageRendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Landroid/graphics/Bitmap;Ljava/lang/Exception;)V

    .line 848
    .local v0, "result":Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverResultOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V

    .line 849
    return-void

    .line 836
    .end local v0    # "result":Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;
    .end local v2    # "devicePageRendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    .end local v10    # "pageText":Ljava/lang/String;
    :cond_0
    const/4 v10, 0x0

    .restart local v10    # "pageText":Ljava/lang/String;
    goto :goto_0
.end method

.method private deliverResult(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V
    .locals 14
    .param p1, "result"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    .prologue
    const/4 v13, 0x0

    .line 872
    iget-object v7, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->bitmap:Landroid/graphics/Bitmap;

    .line 874
    .local v7, "resultBitmap":Landroid/graphics/Bitmap;
    const-string v10, "BooksTextureDebug"

    const/4 v11, 0x2

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    .line 875
    .local v4, "paintDebug":Z
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getTheme()Ljava/lang/String;

    move-result-object v6

    .line 877
    .local v6, "readerTheme":Ljava/lang/String;
    iget-object v10, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->error:Ljava/lang/Exception;

    if-eqz v10, :cond_4

    const/4 v1, 0x1

    .line 879
    .local v1, "hasError":Z
    :goto_0
    iget-object v10, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->request:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    iget-object v8, v10, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    .line 881
    .local v8, "rp":Lcom/google/android/apps/books/render/RenderPosition;
    if-nez v1, :cond_3

    const-string v10, "0"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v4, :cond_3

    .line 882
    :cond_0
    const-string v10, "ImageModeRenderer"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 883
    const-string v10, "ImageModeRenderer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Allocating page bitmap with config "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    :cond_1
    const-string v10, "ImageModeRenderer#handleMessage"

    invoke-static {v7, v10}, Lcom/google/android/apps/books/util/PagesViewUtils;->createMutableBitmapOfSameSize(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 887
    .local v2, "modifiedBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 890
    .local v0, "canvas":Landroid/graphics/Canvas;
    const-string v10, "0"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 891
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPaint:Landroid/graphics/Paint;

    .line 911
    .local v3, "paint":Landroid/graphics/Paint;
    :goto_1
    invoke-virtual {v0, v7, v13, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 913
    if-eqz v4, :cond_2

    .line 914
    iget-object v10, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->request:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->getConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;
    invoke-static {v10}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->access$1400(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Lcom/google/android/apps/books/render/RenderResponseConsumer;

    move-result-object v10

    invoke-static {v0, v8, v8, v10, v6}, Lcom/google/android/apps/books/render/ImageModeRenderer;->paintDebugInfo(Landroid/graphics/Canvas;Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderPosition;Ljava/lang/Object;Ljava/lang/String;)V

    .line 919
    :cond_2
    if-eq v7, v2, :cond_3

    .line 920
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 921
    move-object v7, v2

    .line 927
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "modifiedBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "paint":Landroid/graphics/Paint;
    :cond_3
    if-eqz v1, :cond_8

    .line 928
    iget-object v10, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->error:Ljava/lang/Exception;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchRenderError(Ljava/lang/Exception;)V

    .line 937
    :goto_2
    return-void

    .line 877
    .end local v1    # "hasError":Z
    .end local v8    # "rp":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 892
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    .restart local v1    # "hasError":Z
    .restart local v2    # "modifiedBitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "rp":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_5
    const-string v10, "1"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 893
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mNightPaint:Landroid/graphics/Paint;

    .restart local v3    # "paint":Landroid/graphics/Paint;
    goto :goto_1

    .line 894
    .end local v3    # "paint":Landroid/graphics/Paint;
    :cond_6
    const-string v10, "2"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 896
    iget-object v10, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDesatContrastPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v7, v13, v13, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 899
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mSepiaTonePaint:Landroid/graphics/Paint;

    .line 902
    .restart local v3    # "paint":Landroid/graphics/Paint;
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 903
    move-object v7, v2

    goto :goto_1

    .line 905
    .end local v3    # "paint":Landroid/graphics/Paint;
    :cond_7
    const-string v10, "ImageModeRenderer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is not a valid reader theme."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPaint:Landroid/graphics/Paint;

    .restart local v3    # "paint":Landroid/graphics/Paint;
    goto :goto_1

    .line 930
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "modifiedBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "paint":Landroid/graphics/Paint;
    :cond_8
    new-instance v5, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;

    invoke-direct {v5, v7}, Lcom/google/android/apps/books/render/OwnedBitmapPagePainter;-><init>(Landroid/graphics/Bitmap;)V

    .line 931
    .local v5, "painter":Lcom/google/android/apps/books/render/PagePainter;
    iget-object v10, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v10}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v9

    .line 932
    .local v9, "structure":Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;
    if-eqz v9, :cond_9

    iget-object v10, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mConfig:Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getPaintDebugWordBoxes()Z
    invoke-static {v10}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->access$1500(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 933
    invoke-direct {p0, v5, v9}, Lcom/google/android/apps/books/render/ImageModeRenderer;->overlayDebugStructurePainter(Lcom/google/android/apps/books/render/PagePainter;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/android/apps/books/render/PagePainter;

    move-result-object v5

    .line 935
    :cond_9
    iget-object v10, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v11, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;->request:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->getConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;
    invoke-static {v11}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->access$1400(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Lcom/google/android/apps/books/render/RenderResponseConsumer;

    move-result-object v11

    invoke-virtual {p0, v10, v11, v5}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/PagePainter;)V

    goto :goto_2
.end method

.method private deliverResultOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ImageModeRenderer$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$3;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 868
    return-void
.end method

.method private dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V
    .locals 3
    .param p1, "range"    # Lcom/google/android/apps/books/render/PageStructureSelection;

    .prologue
    .line 1297
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageStructureSelection;->handleRects()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageStructureSelection;->textRects()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageStructureSelection;->getRequestId()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V

    .line 1299
    return-void
.end method

.method private dispatchSelectionStateChanged(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PageStructureSelection;)V
    .locals 7
    .param p1, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "selection"    # Lcom/google/android/apps/books/render/PageStructureSelection;

    .prologue
    .line 547
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering;->indices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getVolumePageIndexForImageMode(Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/model/VolumeMetadata;)I

    move-result v2

    .line 549
    .local v2, "volumePageIndex":I
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeSelectionState;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x12c

    iget-object v5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mSelectionStateCallbacks:Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/ImageModeSelectionState;-><init>(Lcom/google/android/apps/books/render/PageStructureSelection;ILjava/lang/String;ILcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    .end local v2    # "volumePageIndex":I
    :cond_0
    :goto_0
    return-void

    .line 553
    :catch_0
    move-exception v6

    .line 554
    .local v6, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v0, "ImageModeRenderer"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    const-string v0, "ImageModeRenderer"

    const-string v1, "Exception trying to begin selection"

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static generateLinearInterpolationMatrix(II)Landroid/graphics/ColorMatrix;
    .locals 10
    .param p0, "blackColor"    # I
    .param p1, "whiteColor"    # I

    .prologue
    const v9, 0x3b808081

    const/4 v8, 0x0

    .line 1066
    const v3, 0x3b808081

    .line 1067
    .local v3, "scale":F
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 1068
    .local v2, "bgRed":I
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 1069
    .local v1, "bgGreen":I
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 1070
    .local v0, "bgBlue":I
    new-instance v4, Landroid/graphics/ColorMatrix;

    const/16 v5, 0x14

    new-array v5, v5, [F

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v7

    sub-int/2addr v7, v2

    int-to-float v7, v7

    mul-float/2addr v7, v9

    aput v7, v5, v6

    const/4 v6, 0x1

    aput v8, v5, v6

    const/4 v6, 0x2

    aput v8, v5, v6

    const/4 v6, 0x3

    aput v8, v5, v6

    const/4 v6, 0x4

    int-to-float v7, v2

    aput v7, v5, v6

    const/4 v6, 0x5

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v7

    sub-int/2addr v7, v1

    int-to-float v7, v7

    mul-float/2addr v7, v9

    aput v7, v5, v6

    const/4 v6, 0x6

    aput v8, v5, v6

    const/4 v6, 0x7

    aput v8, v5, v6

    const/16 v6, 0x8

    aput v8, v5, v6

    const/16 v6, 0x9

    int-to-float v7, v1

    aput v7, v5, v6

    const/16 v6, 0xa

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    sub-int/2addr v7, v0

    int-to-float v7, v7

    mul-float/2addr v7, v9

    aput v7, v5, v6

    const/16 v6, 0xb

    aput v8, v5, v6

    const/16 v6, 0xc

    aput v8, v5, v6

    const/16 v6, 0xd

    aput v8, v5, v6

    const/16 v6, 0xe

    int-to-float v7, v0

    aput v7, v5, v6

    const/16 v6, 0xf

    aput v8, v5, v6

    const/16 v6, 0x10

    aput v8, v5, v6

    const/16 v6, 0x11

    aput v8, v5, v6

    const/16 v6, 0x12

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    const/16 v6, 0x13

    aput v8, v5, v6

    invoke-direct {v4, v5}, Landroid/graphics/ColorMatrix;-><init>([F)V

    return-object v4
.end method

.method private static getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I
    .locals 2
    .param p0, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 695
    sget-object v0, Lcom/google/android/apps/books/render/ImageModeRenderer$6;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {p0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 699
    const/16 v0, 0x10

    :goto_0
    return v0

    .line 697
    :pswitch_0
    const/16 v0, 0x20

    goto :goto_0

    .line 695
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private getBookPageIndex(Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 5
    .param p1, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 357
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v0

    .line 358
    .local v0, "bookPageIndex":I
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToContent(IILcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget-object v2, v2, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-nez v2, :cond_0

    .line 360
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget v2, v2, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->bookPageIndex:I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    .end local v0    # "bookPageIndex":I
    :goto_0
    return v2

    .line 362
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, "ImageModeRenderer"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 364
    const-string v2, "ImageModeRenderer"

    const-string v3, "Error looking up book page index"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 367
    .end local v1    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;
    .locals 4
    .param p0, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .param p1, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "visibleUserLayers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x6

    .line 1315
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1316
    const-string v1, "ImageModeRenderer"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1317
    const-string v1, "ImageModeRenderer"

    const-string v2, "Empty visible layer set"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1319
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 1327
    :goto_0
    return-object v1

    .line 1321
    :cond_1
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 1322
    const-string v1, "ImageModeRenderer"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1323
    const-string v1, "ImageModeRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected number of visible layers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    :cond_2
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1327
    .local v0, "layerId":Ljava/lang/String;
    invoke-interface {p0, p1, v0}, Lcom/google/android/apps/books/widget/AnnotationIndex;->getAnnotationsForImagePage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private getHighlightRectsTask(ILjava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/os/AsyncTask;
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ")",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 574
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$2;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer$2;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;Ljava/lang/String;I)V

    return-object v0
.end method

.method private getMinMarginRatio()D
    .locals 4

    .prologue
    .line 1212
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mConfig:Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getSideMarginPercent()I
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->access$1900(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private getPageFit(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Lcom/google/android/apps/books/util/PageFit;
    .locals 2
    .param p1, "contentDims"    # Landroid/graphics/Point;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageFit:Lcom/google/android/apps/books/util/PageFit;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageDimensions:Landroid/graphics/Point;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/books/util/PageFit;->compute(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 1303
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageFit:Lcom/google/android/apps/books/util/PageFit;

    return-object v0
.end method

.method private getPageStructure(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 1341
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1342
    :cond_0
    const/4 v0, 0x0

    .line 1344
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;->getStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v0

    goto :goto_0
.end method

.method private getTheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1425
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTheme:Ljava/lang/String;

    return-object v0
.end method

.method public static getVolumePageIndexForImageMode(Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/model/VolumeMetadata;)I
    .locals 2
    .param p0, "indices"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1293
    iget v0, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getStartPageIndex(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    add-int/2addr v0, v1

    return v0
.end method

.method private handleRequestLoadResults(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/io/InputStream;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V
    .locals 5
    .param p1, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p2, "pageContent"    # Ljava/io/InputStream;
    .param p3, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 803
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    .line 804
    .local v2, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    if-nez v2, :cond_0

    .line 805
    invoke-static {p2}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 820
    .end local v2    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :goto_0
    return-void

    .line 809
    .restart local v2    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :cond_0
    iget-object v4, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    iget-object v4, v4, Lcom/google/android/apps/books/render/RenderPosition;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/render/ImageModeRenderer;->computeBitmapFactoryOptions(Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    .line 812
    .local v3, "options":Landroid/graphics/BitmapFactory$Options;
    iget-object v4, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    iget v4, v4, Lcom/google/android/apps/books/render/RenderPosition;->sampleSize:I

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 814
    invoke-static {v3, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->decodeUnencryptedBitmap(Landroid/graphics/BitmapFactory$Options;Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 816
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, p1, v0, p3, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverPageOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Landroid/graphics/Bitmap;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;Lcom/google/android/apps/books/model/VolumeMetadata;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/books/util/BitmapDecodeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 817
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    .end local v3    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v1

    .line 818
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverErrorOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)V

    goto :goto_0

    .line 817
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static makeBitmapFactoryOptions(Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .param p0, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 714
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 715
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput-object p0, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 716
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 717
    return-object v0
.end method

.method private makeErrorResult(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;
    .locals 6
    .param p1, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    .param p2, "error"    # Ljava/lang/Exception;

    .prologue
    .line 852
    invoke-static {}, Lcom/google/android/apps/books/widget/DevicePageRendering;->newBuilder()Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/books/common/Position;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/RenderPosition;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setViewablePositions([Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->build()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v2

    .line 854
    .local v2, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Landroid/graphics/Bitmap;Ljava/lang/Exception;)V

    return-object v0
.end method

.method private makeNormalizedPageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1
    .param p1, "bookPageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1529
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->makeNormalizedPageIdentifier(Lcom/google/android/apps/books/model/VolumeMetadata;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public static makeNormalizedPageIdentifier(Lcom/google/android/apps/books/model/VolumeMetadata;I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 9
    .param p0, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p1, "bookPageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1535
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    .line 1536
    .local v2, "page":Lcom/google/android/apps/books/model/Page;
    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    .line 1537
    .local v3, "pageId":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v5

    .line 1539
    .local v5, "position":Lcom/google/android/apps/books/common/Position;
    invoke-interface {p0, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPageId(Ljava/lang/String;)I

    move-result v0

    .line 1540
    .local v0, "chapterIndex":I
    invoke-interface {p0, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getStartPageIndex(I)I

    move-result v1

    .line 1541
    .local v1, "chapterStartPageIndex":I
    sub-int v4, p1, v1

    .line 1542
    .local v4, "pageIndexInChapter":I
    new-instance v6, Lcom/google/android/apps/books/render/PageIdentifier;

    new-instance v7, Lcom/google/android/apps/books/render/PageIndices;

    invoke-direct {v7, v0, v4}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    invoke-direct {v6, v5, v8, v7, v8}, Lcom/google/android/apps/books/render/PageIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;ILcom/google/android/apps/books/render/PageIndices;I)V

    return-object v6
.end method

.method public static normalizePageIdentifier(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    .locals 7
    .param p0, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p1, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p2, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 1498
    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v5

    invoke-static {p0, p1, v5}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/common/Position;)I

    move-result v1

    .line 1500
    .local v1, "positionScreenPageIndex":I
    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v5

    add-int v2, v1, v5

    .line 1502
    .local v2, "screenPageIndex":I
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getScreenPageIndexOfFirstBookPage()I

    move-result v3

    .line 1503
    .local v3, "screenPageIndexOfFirstBookPage":I
    if-ge v2, v3, :cond_0

    .line 1504
    sub-int v0, v2, v3

    .line 1505
    .local v0, "margin":I
    invoke-static {v0}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->makeSpecialPage(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    move-result-object v5

    .line 1524
    .end local v0    # "margin":I
    :goto_0
    return-object v5

    .line 1508
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getScreenPageIndexOfLastViewableBookPage()I

    move-result v4

    .line 1510
    .local v4, "screenPageIndexOfLastBookPage":I
    if-le v2, v4, :cond_1

    .line 1513
    sub-int v5, v2, v4

    add-int/lit8 v0, v5, -0x1

    .line 1514
    .restart local v0    # "margin":I
    invoke-static {v0}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->makeSpecialPage(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    move-result-object v5

    goto :goto_0

    .line 1517
    .end local v0    # "margin":I
    :cond_1
    sget-object v5, Lcom/google/android/apps/books/render/ImageModeRenderer;->mStaticTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    invoke-virtual {p1, v2, v5}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->screenPageIndexToContent(ILcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;)Z

    .line 1519
    sget-object v5, Lcom/google/android/apps/books/render/ImageModeRenderer;->mStaticTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget-object v5, v5, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    sget-object v6, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_1
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 1521
    sget-object v5, Lcom/google/android/apps/books/render/ImageModeRenderer;->mStaticTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget-object v5, v5, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    sget-object v6, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-ne v5, v6, :cond_3

    .line 1522
    invoke-static {}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makePreviewGap()Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->makeSpecialPage(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    move-result-object v5

    goto :goto_0

    .line 1519
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 1524
    :cond_3
    sget-object v5, Lcom/google/android/apps/books/render/ImageModeRenderer;->mStaticTempContent:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    iget v5, v5, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->bookPageIndex:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->makeNormalPage(Ljava/lang/Integer;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    move-result-object v5

    goto :goto_0
.end method

.method private overlayDebugStructurePainter(Lcom/google/android/apps/books/render/PagePainter;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)Lcom/google/android/apps/books/render/PagePainter;
    .locals 6
    .param p1, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p2, "page"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    .prologue
    .line 941
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$4;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/ImageModeRenderer$4;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/util/Logger;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V

    return-object v0
.end method

.method private static smallerConfig(Landroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap$Config;
    .locals 2
    .param p0, "config1"    # Landroid/graphics/Bitmap$Config;
    .param p1, "config2"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 707
    invoke-static {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I

    move-result v0

    invoke-static {p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 710
    .end local p0    # "config1":Landroid/graphics/Bitmap$Config;
    :goto_0
    return-object p0

    .restart local p0    # "config1":Landroid/graphics/Bitmap$Config;
    :cond_0
    move-object p0, p1

    goto :goto_0
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)I
    .locals 1
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 1234
    const/4 v0, 0x0

    return v0
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 6
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    const/4 v4, 0x1

    .line 430
    iget-object v3, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    const-string v5, "0"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 431
    .local v0, "dayTheme":Z
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mConfig:Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->getMemoryClass()I
    invoke-static {v3}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->access$400(Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)I

    move-result v3

    const/16 v5, 0x40

    if-lt v3, v5, :cond_1

    move v2, v4

    .line 433
    .local v2, "use8888":Z
    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_1
    iput-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMaxImageConfig:Landroid/graphics/Bitmap$Config;

    .line 434
    iget-object v3, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTheme:Ljava/lang/String;

    .line 441
    if-eqz v0, :cond_3

    const/4 v3, 0x2

    :goto_2
    invoke-static {v3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->computeMaxDecodeThreads(I)I

    move-result v1

    .line 442
    .local v1, "maxDecodeThreads":I
    const-string v3, "ImageModeRenderer"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 443
    const-string v3, "ImageModeRenderer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Using "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " page image decode thread[s]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mBitmapDecodeExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->setCorePoolSize(I)V

    .line 446
    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mBitmapDecodeExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->setMaximumPoolSize(I)V

    .line 447
    return-void

    .line 431
    .end local v1    # "maxDecodeThreads":I
    .end local v2    # "use8888":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 433
    .restart local v2    # "use8888":Z
    :cond_2
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_1

    :cond_3
    move v3, v4

    .line 441
    goto :goto_2
.end method

.method public beginSelection(FFLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p4, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 1266
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    .line 1267
    .local v1, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    if-nez v1, :cond_1

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1272
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructureDimensions()Landroid/graphics/Point;

    move-result-object v5

    .line 1273
    .local v5, "structureDims":Landroid/graphics/Point;
    if-eqz v5, :cond_0

    .line 1276
    invoke-direct {p0, v5, p4}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageFit(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Lcom/google/android/apps/books/util/PageFit;

    move-result-object v0

    .line 1277
    .local v0, "fit":Lcom/google/android/apps/books/util/PageFit;
    new-instance v3, Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {v3}, Lcom/google/android/apps/books/render/PageStructureSelection;-><init>()V

    .line 1278
    .local v3, "selection":Lcom/google/android/apps/books/render/PageStructureSelection;
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageStructure(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v4

    .line 1279
    .local v4, "structure":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1280
    .local v2, "point":Landroid/graphics/PointF;
    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/util/PageFit;->mapDisplayToContent(Landroid/graphics/PointF;)V

    .line 1281
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/android/apps/books/render/PageStructureSelection;->initToClosestWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;FF)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1282
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchNewSelectionBegins()V

    .line 1283
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V

    .line 1284
    invoke-direct {p0, p3, v3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionStateChanged(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PageStructureSelection;)V

    goto :goto_0
.end method

.method public canFitWidth()Z
    .locals 1

    .prologue
    .line 1375
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mCanFitWidth:Z

    return v0
.end method

.method public canImmediatelyRedrawHighlights()Z
    .locals 1

    .prologue
    .line 1365
    const/4 v0, 0x1

    return v0
.end method

.method public canProvideText()Z
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x0

    return v0
.end method

.method public clearAnnotationCaches()V
    .locals 0

    .prologue
    .line 1359
    return-void
.end method

.method public convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z
    .locals 5
    .param p1, "p"    # Landroid/graphics/Point;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 660
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructureDimensions()Landroid/graphics/Point;

    move-result-object v0

    .line 661
    .local v0, "dims":Landroid/graphics/Point;
    if-nez v0, :cond_0

    .line 662
    const/4 v2, 0x0

    .line 668
    :goto_0
    return v2

    .line 664
    :cond_0
    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageFit(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Lcom/google/android/apps/books/util/PageFit;

    move-result-object v1

    .line 665
    .local v1, "pageFit":Lcom/google/android/apps/books/util/PageFit;
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPointF:Landroid/graphics/PointF;

    iget v3, p1, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, p1, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 666
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPointF:Landroid/graphics/PointF;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/PageFit;->mapDisplayToContent(Landroid/graphics/PointF;)V

    .line 667
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPointF:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPointF:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 668
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public createSearchMatchRectsCache(Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ILcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
    .locals 7
    .param p1, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p2, "searchHighlightColor"    # I
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "I",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            ")",
            "Lcom/google/android/apps/books/widget/SearchMatchRectsCache",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1431
    new-instance v0, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->displayTwoPages()Z

    move-result v5

    move-object v1, p0

    move v2, p2

    move-object v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/widget/ImageModeSearchMatchRectsCache;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;ILcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ZLcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    return-object v0
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 282
    invoke-super {p0}, Lcom/google/android/apps/books/render/ReaderRenderer;->destroy()V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->cancelPendingRequests()V

    .line 284
    return-void
.end method

.method public displayTwoPages()Z
    .locals 1

    .prologue
    .line 1370
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    return v0
.end method

.method public findTouchableItem(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;I)Lcom/google/android/apps/books/render/TouchableItem;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "rendererTouchPoint"    # Landroid/graphics/Point;
    .param p3, "type"    # I

    .prologue
    .line 654
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Ljava/util/List;
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1309
    .local p3, "visibleUserLayers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0, p3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDecorationInsetFromPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering;)I
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 1204
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructure()Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMinMarginRatio()D

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->computeRightMargin(Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;D)I

    move-result v0

    return v0
.end method

.method public getFirstPageAfterLastViewablePage()Lcom/google/android/apps/books/render/PageHandle;
    .locals 1

    .prologue
    .line 385
    new-instance v0, Lcom/google/android/apps/books/render/ImageModeRenderer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$1;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;)V

    return-object v0
.end method

.method public getGridRowCount()I
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->getGridRowCount()I

    move-result v0

    return v0
.end method

.method public getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;
    .locals 4
    .param p1, "gridRowIndex"    # I

    .prologue
    .line 377
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->gridRowIndexToStartBookPageIndex(I)I

    move-result v1

    .line 379
    .local v1, "startBookPageIndex":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Page;

    .line 380
    .local v0, "page":Lcom/google/android/apps/books/model/Page;
    new-instance v2, Lcom/google/android/apps/books/common/Position;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    return-object v2
.end method

.method public getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "annotationIndex"    # Lcom/google/android/apps/books/widget/AnnotationIndex;
    .param p3, "selectedAnnotationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Lcom/google/android/apps/books/widget/AnnotationIndex;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1333
    sget-object v1, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 1336
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mHighlightsByColorWalker:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageStructure(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->setup(Ljava/util/List;Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Ljava/lang/String;)V

    .line 1337
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mHighlightsByColorWalker:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    return-object v1
.end method

.method public getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "marginIconTapSize"    # I
    .param p4, "includePlainHighlights"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "IZ)",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/MarginNote;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1351
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMarginNoteIconWalker:Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;->setup(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)V

    .line 1352
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mMarginNoteIconWalker:Lcom/google/android/apps/books/render/ImageModeRenderer$MarginNoteIconWalker;

    return-object v0
.end method

.method public getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;
    .locals 3
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getBookPageIndex(Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    .line 319
    .local v0, "bookPageIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 320
    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    .line 322
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer$ImagePageHandle;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;I)V

    goto :goto_0
.end method

.method public getPageToViewMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Matrix;)Z
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "result"    # Landroid/graphics/Matrix;

    .prologue
    .line 641
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructureDimensions()Landroid/graphics/Point;

    move-result-object v1

    .line 642
    .local v1, "structureDims":Landroid/graphics/Point;
    if-eqz v1, :cond_0

    .line 643
    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageFit(Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Lcom/google/android/apps/books/util/PageFit;

    move-result-object v0

    .line 644
    .local v0, "fit":Lcom/google/android/apps/books/util/PageFit;
    invoke-virtual {v0, p3}, Lcom/google/android/apps/books/util/PageFit;->getContentToDisplayMatrix(Landroid/graphics/Matrix;)V

    .line 645
    const/4 v2, 0x1

    .line 647
    .end local v0    # "fit":Lcom/google/android/apps/books/util/PageFit;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 7
    .param p1, "p1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "p2"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    .line 292
    .local v1, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v2

    .line 293
    .local v2, "p1BookPageIndex":I
    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v3

    .line 294
    .local v3, "p2BookPageIndex":I
    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v6

    invoke-static {v4, v2, v5, v3, v6}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenPageDifference(Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;IIII)I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 297
    .end local v2    # "p1BookPageIndex":I
    .end local v3    # "p2BookPageIndex":I
    :goto_0
    return v4

    .line 296
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const v4, 0x7fffffff

    goto :goto_0
.end method

.method public getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I
    .locals 8
    .param p1, "s1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "s2"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v7

    .line 307
    .local v7, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v1

    .line 308
    .local v1, "p1BookPageIndex":I
    iget-object v0, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v3

    .line 309
    .local v3, "p2BookPageIndex":I
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget v2, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget v4, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    iget-boolean v5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;IIIIZ)I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 312
    .end local v1    # "p1BookPageIndex":I
    .end local v3    # "p2BookPageIndex":I
    :goto_0
    return v0

    .line 311
    :catch_0
    move-exception v6

    .line 312
    .local v6, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .locals 5
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1441
    .local p2, "result":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/render/PageHandle;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    iget-boolean v3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    iget-object v4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-static {v1, v2, p1, v3, v4}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->spreadIdentifierToPageIdentifiers(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/SpreadIdentifier;Z[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;

    .line 1443
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    if-eqz v1, :cond_0

    .line 1444
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1455
    :goto_0
    return-object p2

    .line 1447
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1449
    :catch_0
    move-exception v0

    .line 1450
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, "ImageModeRenderer"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1451
    const-string v1, "ImageModeRenderer"

    const-string v2, "Error in getTextSpreadPageHandles"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1453
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    iget-boolean v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    invoke-virtual {p2, v1, v2}, Lcom/google/android/apps/books/render/SpreadItems;->fill(Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public isPassageForbidden(I)Z
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    .line 532
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "no text in image mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isPositionEnabled(Lcom/google/android/apps/books/common/Position;)Z
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v0

    return v0
.end method

.method public loadHighlightRectsForQuery(Ljava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)I
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 626
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->allocateRequestId()I

    move-result v0

    .line 627
    .local v0, "requestId":I
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getHighlightRectsTask(ILjava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/os/AsyncTask;

    move-result-object v1

    .line 629
    .local v1, "walkerTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;>;>;"
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v1, v2}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 630
    return v0
.end method

.method public loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p3, "needSelectionData"    # Z
    .param p4, "handleIndex"    # I
    .param p5, "deltaX"    # I
    .param p6, "deltaY"    # I
    .param p7, "prevRequest"    # I
    .param p8, "startSelection"    # Z

    .prologue
    .line 564
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ImageModeRenderer#loadRangeData"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public loadRangeDataBulk(ILjava/util/Map;)I
    .locals 2
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 569
    .local p2, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ImageModeRenderer#loadRangeDataBulk"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public needsBackgroundAnnotationLoad()Z
    .locals 1

    .prologue
    .line 635
    const/4 v0, 0x0

    return v0
.end method

.method public normalizedPageCoordinatesToContentCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "in"    # Landroid/graphics/PointF;
    .param p3, "out"    # Landroid/graphics/PointF;

    .prologue
    .line 403
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructureDimensions()Landroid/graphics/Point;

    move-result-object v0

    .line 404
    .local v0, "structureDims":Landroid/graphics/Point;
    if-eqz v0, :cond_0

    .line 405
    iget v1, p2, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/PointF;->y:F

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {p3, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 406
    const/4 v1, 0x1

    .line 408
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onAccessibleSelectionChanged(IILcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 537
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/PageStructureSelection;-><init>()V

    .line 538
    .local v0, "selection":Lcom/google/android/apps/books/render/PageStructureSelection;
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getPageStructure(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/books/render/PageStructureSelection;->setToCharacterRange(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;II)V

    .line 540
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V

    .line 541
    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSelectionStateChanged(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PageStructureSelection;)V

    .line 542
    return-void
.end method

.method public pageBoundsInRendererCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/graphics/Rect;
    .locals 5
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    const/4 v4, 0x0

    .line 1194
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructureDimensions()Landroid/graphics/Point;

    move-result-object v0

    .line 1195
    .local v0, "dims":Landroid/graphics/Point;
    if-nez v0, :cond_0

    .line 1196
    const/4 v1, 0x0

    .line 1198
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z
    .locals 7
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1239
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    .line 1240
    .local v1, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    if-nez v1, :cond_1

    .line 1259
    :cond_0
    :goto_0
    return v4

    .line 1247
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    invoke-static {v1, v5, p1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->normalizePageIdentifier(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    move-result-object v2

    .line 1249
    .local v2, "normalizedPosition":Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->isBookPage()Z

    move-result v5

    if-eqz v5, :cond_2

    move v4, v3

    .line 1250
    goto :goto_0

    .line 1253
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->getSpecialPage()Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    sget-object v6, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v5, v6, :cond_3

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1

    .line 1255
    .end local v2    # "normalizedPosition":Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    :catch_0
    move-exception v0

    .line 1256
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v3, "ImageModeRenderer"

    const/4 v5, 0x6

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1257
    const-string v3, "ImageModeRenderer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error checking pageExists: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public rendererCoordinatesToPagePointsMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Matrix;
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "canvasDims"    # Landroid/graphics/Point;

    .prologue
    .line 1182
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageStructureDimensions()Landroid/graphics/Point;

    move-result-object v0

    .line 1183
    .local v0, "dims":Landroid/graphics/Point;
    if-nez v0, :cond_0

    .line 1185
    const/4 v1, 0x0

    .line 1189
    :goto_0
    return-object v1

    .line 1187
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageFit:Lcom/google/android/apps/books/util/PageFit;

    sget-object v2, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-virtual {v1, p2, v0, v2}, Lcom/google/android/apps/books/util/PageFit;->compute(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 1188
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mTempPageFit:Lcom/google/android/apps/books/util/PageFit;

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mRendererCoordinatesToPagePointsMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/PageFit;->getContentToDisplayMatrix(Landroid/graphics/Matrix;)V

    .line 1189
    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mRendererCoordinatesToPagePointsMatrix:Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 2
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 526
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t get text in image mode"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 18
    .param p1, "request"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .prologue
    .line 463
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v12

    .line 465
    .local v12, "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    if-nez v12, :cond_1

    .line 516
    .end local v12    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :cond_0
    :goto_0
    return-void

    .line 469
    .restart local v12    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    :cond_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    move-object/from16 v16, v0

    .line 471
    .local v16, "rawPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v16, :cond_2

    .line 472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDisplayTwoPages:Z

    invoke-static {v12, v2, v5, v6}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->spreadPageIdentifierToPageIdentifier(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v16

    .line 476
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    move-object/from16 v0, v16

    invoke-static {v12, v2, v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->normalizePageIdentifier(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;

    move-result-object v13

    .line 478
    .local v13, "normalizedPage":Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    const-string v2, "ImageModeRenderer"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 479
    const-string v2, "ImageModeRenderer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Position normalized to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_3
    invoke-virtual {v13}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->isBookPage()Z

    move-result v2

    if-nez v2, :cond_4

    .line 483
    invoke-virtual {v13}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->getSpecialPage()Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 510
    .end local v12    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    .end local v13    # "normalizedPage":Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    .end local v16    # "rawPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    :catch_0
    move-exception v9

    .line 512
    .local v9, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, "ImageModeRenderer"

    const/4 v5, 0x5

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 513
    const-string v2, "ImageModeRenderer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to normalize requested page "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 487
    .end local v9    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .restart local v12    # "metadata":Lcom/google/android/apps/books/model/VolumeMetadata;
    .restart local v13    # "normalizedPage":Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;
    .restart local v16    # "rawPageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_4
    :try_start_1
    new-instance v14, Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v13}, Lcom/google/android/apps/books/render/ImageModeRenderer$NormalizedPosition;->getBookPageIndex()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->makeNormalizedPageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v2

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/books/render/RenderPosition;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/books/render/RenderPosition;->sampleSize:I

    invoke-direct {v14, v2, v5, v6, v7}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V

    .line 490
    .local v14, "normalizedPosition":Lcom/google/android/apps/books/render/RenderPosition;
    new-instance v10, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getSequenceNumber()I

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v10, v0, v14, v1, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;I)V

    .line 493
    .local v10, "internalRequest":Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
    invoke-virtual {v14}, Lcom/google/android/apps/books/render/RenderPosition;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v17

    .line 494
    .local v17, "requestPosition":Lcom/google/android/apps/books/common/Position;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v15

    .line 496
    .local v15, "pageId":Ljava/lang/String;
    :try_start_2
    invoke-interface {v12, v15}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPage(Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;

    move-result-object v4

    .line 498
    .local v4, "page":Lcom/google/android/apps/books/model/Page;
    new-instance v11, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v10}, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)V

    .line 501
    .local v11, "joiner":Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;
    invoke-interface {v12}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    .line 503
    .local v3, "volumeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    new-instance v5, Lcom/google/android/apps/books/render/ImageModeRenderer$StreamOpeningConsumer;

    iget-object v6, v11, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->firstConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/render/ImageModeRenderer$StreamOpeningConsumer;-><init>(Lcom/google/android/ublib/utils/Consumer;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-interface/range {v2 .. v8}, Lcom/google/android/apps/books/data/BooksDataController;->getPageContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v5, v11, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->secondConsumer:Lcom/google/android/ublib/utils/Consumer;

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-interface/range {v2 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getPageStructure(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    :try_end_2
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 507
    .end local v3    # "volumeId":Ljava/lang/String;
    .end local v4    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v11    # "joiner":Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;
    :catch_1
    move-exception v9

    .line 508
    .restart local v9    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/google/android/apps/books/render/ImageModeRenderer;->makeErrorResult(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverResult(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderResult;)V
    :try_end_3
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method public shouldFitWidth()Z
    .locals 1

    .prologue
    .line 1380
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mShouldFitWidth:Z

    return v0
.end method

.method public weaklyAddWebLoadListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .prologue
    .line 621
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 622
    return-void
.end method
