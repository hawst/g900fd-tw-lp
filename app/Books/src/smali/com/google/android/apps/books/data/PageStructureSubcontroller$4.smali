.class Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "PageStructureSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iput-object p4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p5, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p6, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    iput-object p7, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 12
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    const/4 v11, 0x6

    .line 194
    const/4 v8, 0x0

    .line 196
    .local v8, "response":Lcom/google/android/apps/books/net/EncryptedContentResponse;
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v10

    .line 197
    .local v10, "server":Lcom/google/android/apps/books/net/BooksServer;
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v0, v0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-interface {v10, v0, v1, v2}, Lcom/google/android/apps/books/net/BooksServer;->getPageStructure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/net/EncryptedContentResponse;

    move-result-object v8

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    invoke-interface {v0, v8}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v7

    .line 200
    .local v7, "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    new-instance v6, Lcom/google/android/apps/books/data/LargeBlobFromServer;

    invoke-direct {v6, v7}, Lcom/google/android/apps/books/data/LargeBlobFromServer;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V

    .line 201
    .local v6, "blob":Lcom/google/android/apps/books/data/LargeBlobFromServer;
    new-instance v9, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    const/4 v1, 0x0

    invoke-direct {v9, v6, v0, v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/PageStructureSubcontroller$1;)V

    .line 202
    .local v9, "result":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;)V
    invoke-static {v0, p1, v1, v9}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$400(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    if-eqz v8, :cond_0

    .line 211
    :try_start_1
    invoke-interface {v8}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 219
    .end local v6    # "blob":Lcom/google/android/apps/books/data/LargeBlobFromServer;
    .end local v7    # "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .end local v9    # "result":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    .end local v10    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :cond_0
    :goto_0
    return-void

    .line 213
    .restart local v6    # "blob":Lcom/google/android/apps/books/data/LargeBlobFromServer;
    .restart local v7    # "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .restart local v9    # "result":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    .restart local v10    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :catch_0
    move-exception v4

    .line 214
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "SCSC"

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "SCSC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error closing page structure response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 203
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "blob":Lcom/google/android/apps/books/data/LargeBlobFromServer;
    .end local v7    # "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .end local v9    # "result":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    .end local v10    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :catch_1
    move-exception v4

    .line 204
    .local v4, "e":Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v5, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    move-object v1, p1

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$500(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    if-eqz v8, :cond_0

    .line 211
    :try_start_3
    invoke-interface {v8}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 213
    :catch_2
    move-exception v4

    .line 214
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "SCSC"

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "SCSC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error closing page structure response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 206
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 207
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_4
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$4;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, p1, v1, v4}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$600(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 210
    if-eqz v8, :cond_0

    .line 211
    :try_start_5
    invoke-interface {v8}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 213
    :catch_4
    move-exception v4

    .line 214
    const-string v0, "SCSC"

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "SCSC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error closing page structure response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 209
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    .line 210
    if-eqz v8, :cond_1

    .line 211
    :try_start_6
    invoke-interface {v8}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 217
    :cond_1
    :goto_1
    throw v0

    .line 213
    :catch_5
    move-exception v4

    .line 214
    .restart local v4    # "e":Ljava/io/IOException;
    const-string v1, "SCSC"

    invoke-static {v1, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    const-string v1, "SCSC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing page structure response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
