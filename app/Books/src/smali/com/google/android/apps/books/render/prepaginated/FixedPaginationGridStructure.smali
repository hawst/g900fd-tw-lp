.class public Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;
.super Ljava/lang/Object;
.source "FixedPaginationGridStructure.java"


# instance fields
.field private final mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 10
    .param p2, "pageCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    const/4 v9, 0x1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v6, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-direct {v6}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    .line 16
    const/4 v3, 0x0

    .line 17
    .local v3, "lastStartBookPageIndex":I
    const/4 v1, 0x0

    .line 19
    .local v1, "chapterIndex":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 21
    .local v2, "chaptersCount":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 22
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    .line 23
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v5

    .line 24
    .local v5, "startBookPageIndex":I
    if-eq v5, v3, :cond_0

    .line 25
    sub-int v4, v5, v3

    .line 26
    .local v4, "pagesInRow":I
    iget-object v6, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v7, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    sget-object v8, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-virtual {v6, v7, v9, v8, v4}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 28
    move v3, v5

    .line 21
    .end local v4    # "pagesInRow":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v5    # "startBookPageIndex":I
    :cond_1
    sub-int v4, p2, v3

    .line 35
    .restart local v4    # "pagesInRow":I
    iget-object v6, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v7, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    sget-object v8, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-virtual {v6, v7, v9, v8, v4}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public bookPageIndexToGridRowIndex(I)I
    .locals 1
    .param p1, "bookPageIndex"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->rightIndexToLeftIndex(I)I

    move-result v0

    return v0
.end method

.method public getGridRowCount()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getLeftSize()I

    move-result v0

    return v0
.end method

.method public gridRowIndexToStartBookPageIndex(I)I
    .locals 1
    .param p1, "gridRowIndex"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->mRowIndexToStartBookPageIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v0

    return v0
.end method
