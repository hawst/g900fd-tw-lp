.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->loadDismissedRecommendations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 349
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$000(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getDismissedRecommendations()Ljava/util/Set;

    move-result-object v0

    .line 350
    .local v0, "dismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$300(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6$1;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;Ljava/util/Set;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    .end local v0    # "dismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v1

    .line 361
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "FgDataController"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    const-string v2, "FgDataController"

    const-string v3, "loadDismissedRecommendations Failed"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
