.class public final enum Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
.super Ljava/lang/Enum;
.source "TransientInfoCardsLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CardsState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

.field public static final enum FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

.field public static final enum HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

.field public static final enum PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

.field public static final enum TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;


# instance fields
.field final backgroundAlpha:F

.field final backgroundVisible:Z

.field final cardBackgroundAlpha:F

.field final cardsVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    const-string v1, "HIDDEN"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;-><init>(Ljava/lang/String;IZZFF)V

    sput-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 59
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    const-string v1, "PEEKING"

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;-><init>(Ljava/lang/String;IZZFF)V

    sput-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 65
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    const-string v1, "TAPPED"

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;-><init>(Ljava/lang/String;IZZFF)V

    sput-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 75
    new-instance v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    const-string v1, "FOREGROUND"

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const v6, 0x3f733333    # 0.95f

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;-><init>(Ljava/lang/String;IZZFF)V

    sput-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->HIDDEN:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->$VALUES:[Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZFF)V
    .locals 0
    .param p3, "cardsVisible"    # Z
    .param p4, "backgroundVisible"    # Z
    .param p5, "cardBackgroundAlpha"    # F
    .param p6, "backgroundAlpha"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZFF)V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->cardsVisible:Z

    .line 94
    iput-boolean p4, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->backgroundVisible:Z

    .line 95
    iput p5, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->cardBackgroundAlpha:F

    .line 96
    iput p6, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->backgroundAlpha:F

    .line 97
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->$VALUES:[Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    return-object v0
.end method
