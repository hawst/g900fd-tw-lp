.class Lcom/google/android/apps/books/util/MediaUrlFetcher$1;
.super Ljava/lang/Object;
.source "MediaUrlFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFromNetwork()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

.field final synthetic val$targetWidth:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    iput p2, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->val$targetWidth:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const v9, 0x7f0f0148

    const/4 v8, 0x5

    .line 129
    iget-object v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # getter for: Lcom/google/android/apps/books/util/MediaUrlFetcher;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$100(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/BooksContext;

    .line 130
    .local v0, "booksContext":Lcom/google/android/apps/books/common/BooksContext;
    new-instance v4, Lcom/google/android/apps/books/util/MediaUrls;

    invoke-direct {v4}, Lcom/google/android/apps/books/util/MediaUrls;-><init>()V

    .line 131
    .local v4, "mediaUrls":Lcom/google/android/apps/books/util/MediaUrls;
    if-nez v0, :cond_1

    .line 132
    const-string v5, "MediaUrlFetcher"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 133
    const-string v5, "MediaUrlFetcher"

    const-string v6, "app context is null"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    const/4 v2, 0x0

    .line 140
    .local v2, "gotMediaUrls":Z
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/apps/books/common/BooksContext;->getResponseGetter()Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # getter for: Lcom/google/android/apps/books/util/MediaUrlFetcher;->mAccount:Landroid/accounts/Account;
    invoke-static {v6}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$200(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Landroid/accounts/Account;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # getter for: Lcom/google/android/apps/books/util/MediaUrlFetcher;->mTouchableItem:Lcom/google/android/apps/books/render/TouchableItem;
    invoke-static {v7}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$300(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Lcom/google/android/apps/books/render/TouchableItem;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/books/util/MediaUrls;->fetchUrls(Lcom/google/android/apps/books/net/ResponseGetter;Landroid/accounts/Account;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 156
    :cond_2
    :goto_1
    if-nez v2, :cond_4

    .line 157
    iget-object v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # invokes: Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFailed(I)V
    invoke-static {v5, v9}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$400(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V

    goto :goto_0

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    const-string v5, "MediaUrlFetcher"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 145
    const-string v5, "MediaUrlFetcher"

    const-string v6, "can\'t retrieve clip URL: no net connection"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    const v6, 0x7f0f0149

    # invokes: Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFailed(I)V
    invoke-static {v5, v6}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$400(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V

    goto :goto_0

    .line 150
    .end local v1    # "e":Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    :catch_1
    move-exception v1

    .line 151
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "MediaUrlFetcher"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 152
    const-string v5, "MediaUrlFetcher"

    const-string v6, "error retrieving clip URL"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 161
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    iget v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->val$targetWidth:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/util/MediaUrls;->getBestMediaUrl(I)Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "mediaUrl":Ljava/lang/String;
    if-nez v3, :cond_5

    .line 163
    iget-object v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # invokes: Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFailed(I)V
    invoke-static {v5, v9}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$400(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V

    goto :goto_0

    .line 166
    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$1;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # invokes: Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchSucceeded(Ljava/lang/String;)V
    invoke-static {v5, v3}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$500(Lcom/google/android/apps/books/util/MediaUrlFetcher;Ljava/lang/String;)V

    goto :goto_0
.end method
