.class public interface abstract Lcom/google/android/apps/books/data/EncryptionScheme;
.super Ljava/lang/Object;
.source "EncryptionScheme.java"


# virtual methods
.method public abstract decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method
