.class public Lcom/google/android/apps/books/annotations/PaginatedResponse;
.super Ljava/lang/Object;
.source "PaginatedResponse.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final continuation:Ljava/lang/String;

.field public final done:Z

.field public final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;ZLjava/util/List;)V
    .locals 1
    .param p1, "continuation"    # Ljava/lang/String;
    .param p2, "done"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/apps/books/annotations/PaginatedResponse;, "Lcom/google/android/apps/books/annotations/PaginatedResponse<TT;>;"
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/PaginatedResponse;->continuation:Ljava/lang/String;

    .line 25
    iput-boolean p2, p0, Lcom/google/android/apps/books/annotations/PaginatedResponse;->done:Z

    .line 29
    if-nez p3, :cond_0

    .line 30
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/PaginatedResponse;->items:Ljava/util/List;

    .line 35
    :goto_0
    return-void

    .line 32
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/PaginatedResponse;->items:Ljava/util/List;

    goto :goto_0
.end method

.method public static finalResponse(Ljava/util/List;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TU;>;)",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "items":Ljava/util/List;, "Ljava/util/List<TU;>;"
    new-instance v0, Lcom/google/android/apps/books/annotations/PaginatedResponse;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/books/annotations/PaginatedResponse;-><init>(Ljava/lang/String;ZLjava/util/List;)V

    return-object v0
.end method

.method public static partialResponse(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 2
    .param p0, "continuation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TU;>;)",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TU;>;"
    new-instance v0, Lcom/google/android/apps/books/annotations/PaginatedResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/books/annotations/PaginatedResponse;-><init>(Ljava/lang/String;ZLjava/util/List;)V

    return-object v0
.end method
