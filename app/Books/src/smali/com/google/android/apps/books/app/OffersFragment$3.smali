.class Lcom/google/android/apps/books/app/OffersFragment$3;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$3;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 123
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$3;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->getDocumentForView(Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/app/OffersFragment;->access$200(Lcom/google/android/apps/books/app/OffersFragment;Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;

    move-result-object v0

    .line 124
    .local v0, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    if-eqz v0, :cond_1

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$3;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->showRedeemOfferDialog(Lcom/google/android/apps/books/playcards/BookDocument;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/OffersFragment;->access$100(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/playcards/BookDocument;)V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    const-string v1, "OffersFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    const-string v1, "OffersFragment"

    const-string v2, "Can\'t find document for redeem button!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
