.class Lcom/google/android/apps/books/view/pages/PageTurnJava$1;
.super Ljava/lang/Object;
.source "PageTurnJava.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/view/pages/PageTurnJava;->setLoadingBitmap(Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

.field final synthetic val$previous:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    iput-object p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->val$previous:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private uploadLoadingTexture()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    # getter for: Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;
    invoke-static {v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$000(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnScene;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mLoading:I

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    # getter for: Lcom/google/android/apps/books/view/pages/PageTurnJava;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;
    invoke-static {v1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$100(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;)V

    .line 144
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->val$previous:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->val$previous:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 138
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnJava$1;->uploadLoadingTexture()V

    .line 139
    return-void
.end method
