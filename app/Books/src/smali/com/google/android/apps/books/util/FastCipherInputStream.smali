.class public Lcom/google/android/apps/books/util/FastCipherInputStream;
.super Ljava/io/InputStream;
.source "FastCipherInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/FastCipherInputStream$DiscoverBufferInputStream;
    }
.end annotation


# static fields
.field private static final SYSTEM_BUFSIZE:I


# instance fields
.field private final mBlockSize:I

.field private final mCipher:Ljavax/crypto/Cipher;

.field private mEof:Z

.field private final mInputBuf:[B

.field private final mInputStream:Ljava/io/InputStream;

.field private mProcessedBuf:[B

.field private mProcessedLen:I

.field private mProcessedOff:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/apps/books/util/FastCipherInputStream$DiscoverBufferInputStream;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/FastCipherInputStream$DiscoverBufferInputStream;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/FastCipherInputStream$DiscoverBufferInputStream;->getBufferSize()I

    move-result v0

    sput v0, Lcom/google/android/apps/books/util/FastCipherInputStream;->SYSTEM_BUFSIZE:I

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "cipher"    # Ljavax/crypto/Cipher;

    .prologue
    .line 93
    sget v0, Lcom/google/android/apps/books/util/FastCipherInputStream;->SYSTEM_BUFSIZE:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/util/FastCipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;I)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;I)V
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "cipher"    # Ljavax/crypto/Cipher;
    .param p3, "bufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputStream:Ljava/io/InputStream;

    .line 69
    iput-object p2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mCipher:Ljavax/crypto/Cipher;

    .line 70
    invoke-virtual {p2}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mBlockSize:I

    .line 72
    if-gtz p3, :cond_0

    .line 73
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Buffer size must be positive, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_0
    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mBlockSize:I

    add-int/2addr v1, p3

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mBlockSize:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mBlockSize:I

    mul-int v0, v1, v2

    .line 86
    .local v0, "bufSize":I
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    .line 87
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    .line 88
    return-void
.end method

.method private availableData()I
    .locals 2

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedLen:I

    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private fillBufferIfEmpty()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mEof:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedLen:I

    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    if-le v0, v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const/4 v3, 0x0

    .line 109
    .local v3, "readLen":I
    :goto_1
    iget v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mBlockSize:I

    if-ge v3, v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputStream:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    iget-object v2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    array-length v2, v2

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    .line 111
    .local v7, "len":I
    const/4 v0, -0x1

    if-ne v7, v0, :cond_3

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mEof:Z

    .line 118
    .end local v7    # "len":I
    :cond_2
    iput v4, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    .line 122
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mEof:Z

    if-eqz v0, :cond_4

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedLen:I
    :try_end_0
    .catch Ljavax/crypto/ShortBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 127
    :catch_0
    move-exception v6

    .line 130
    .local v6, "e":Ljavax/crypto/ShortBufferException;
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mEof:Z

    if-eqz v0, :cond_5

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    .line 135
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    array-length v0, v0

    iput v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedLen:I
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 137
    .end local v6    # "e":Ljavax/crypto/ShortBufferException;
    :catch_1
    move-exception v6

    .line 138
    .local v6, "e":Ljava/security/GeneralSecurityException;
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cipher error"

    invoke-direct {v0, v1, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 115
    .end local v6    # "e":Ljava/security/GeneralSecurityException;
    .restart local v7    # "len":I
    :cond_3
    add-int/2addr v3, v7

    .line 116
    goto :goto_1

    .line 125
    .end local v7    # "len":I
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->update([BII[BI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedLen:I
    :try_end_2
    .catch Ljavax/crypto/ShortBufferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 133
    .local v6, "e":Ljavax/crypto/ShortBufferException;
    :cond_5
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputBuf:[B

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B
    :try_end_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/google/android/apps/books/util/FastCipherInputStream;->availableData()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mCipher:Ljavax/crypto/Cipher;

    iget-object v2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v2

    const v3, 0x3fffffff    # 1.9999999f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v1, v2}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/apps/books/util/FastCipherInputStream;->fillBufferIfEmpty()V

    .line 167
    iget v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedLen:I

    if-ge v0, v1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    if-gtz p3, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    .line 152
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/util/FastCipherInputStream;->fillBufferIfEmpty()V

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/books/util/FastCipherInputStream;->availableData()I

    move-result v1

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 155
    .local v0, "readLen":I
    if-lez v0, :cond_1

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedBuf:[B

    iget v2, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    goto :goto_0

    .line 161
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public skip(J)J
    .locals 5
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 175
    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    .line 183
    :goto_0
    return-wide v2

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/util/FastCipherInputStream;->fillBufferIfEmpty()V

    .line 180
    invoke-direct {p0}, Lcom/google/android/apps/books/util/FastCipherInputStream;->availableData()I

    move-result v1

    int-to-long v2, v1

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    .line 182
    .local v0, "skipLen":I
    iget v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/books/util/FastCipherInputStream;->mProcessedOff:I

    .line 183
    int-to-long v2, v0

    goto :goto_0
.end method
