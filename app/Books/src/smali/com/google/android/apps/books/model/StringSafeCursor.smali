.class public Lcom/google/android/apps/books/model/StringSafeCursor;
.super Ljava/lang/Object;
.source "StringSafeCursor.java"


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private final mProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "projectionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    .line 28
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 36
    return-void
.end method

.method public getBlob(Ljava/lang/String;)[B
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/books/util/CursorUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getDoubleObject(Ljava/lang/String;)Ljava/lang/Double;
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/books/util/CursorUtils;->getDoubleObject(Landroid/database/Cursor;I)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(Ljava/lang/String;)F
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getInteger(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 76
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 77
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLongObject(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 68
    .local v0, "columnIndex":I
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isNull(Ljava/lang/String;)Z
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mProjectionMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/model/StringSafeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 48
    return-void
.end method
