.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$2;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$2;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 293
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->SKIPPED_FROM_SAMPLES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logOnboardingAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;Ljava/lang/Long;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$2;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->exitOnboardFlow()V

    .line 297
    return-void
.end method
