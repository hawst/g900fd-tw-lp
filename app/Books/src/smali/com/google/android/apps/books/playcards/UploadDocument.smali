.class public Lcom/google/android/apps/books/playcards/UploadDocument;
.super Lcom/google/android/ublib/cardlib/model/Document;
.source "UploadDocument.java"


# instance fields
.field private mUploadId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/model/Document;-><init>()V

    return-void
.end method


# virtual methods
.method public getUploadId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/UploadDocument;->mUploadId:Ljava/lang/String;

    return-object v0
.end method

.method public setUploadId(Ljava/lang/String;)V
    .locals 0
    .param p1, "uploadId"    # Ljava/lang/String;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/UploadDocument;->mUploadId:Ljava/lang/String;

    .line 17
    return-void
.end method
