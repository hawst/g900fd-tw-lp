.class Lcom/google/android/apps/books/app/AddVolumeTask;
.super Landroid/os/AsyncTask;
.source "AddVolumeTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p3, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/app/AddVolumeTask;->mVolumeId:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/books/app/AddVolumeTask;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/books/app/AddVolumeTask;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 35
    return-void
.end method

.method public static addVolume(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/apps/books/app/AddVolumeTask;

    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/apps/books/app/AddVolumeTask;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/AddVolumeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 22
    return-void
.end method

.method private static handleException(Ljava/lang/Exception;)V
    .locals 3
    .param p0, "e"    # Ljava/lang/Exception;

    .prologue
    .line 50
    const-string v0, "AddVolumeTask"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "AddVolumeTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error occurred adding volume to collection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AddVolumeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/apps/books/model/CollectionVolumesEditor;

    iget-object v2, p0, Lcom/google/android/apps/books/app/AddVolumeTask;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    iget-object v3, p0, Lcom/google/android/apps/books/app/AddVolumeTask;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/model/CollectionVolumesEditor;-><init>(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;)V

    .line 42
    .local v0, "adder":Lcom/google/android/apps/books/model/CollectionVolumesEditor;
    const-wide/16 v2, 0x7

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/AddVolumeTask;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->synchronousAdd(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 43
    :catch_0
    move-exception v1

    .line 44
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AddVolumeTask;->handleException(Ljava/lang/Exception;)V

    goto :goto_0
.end method
