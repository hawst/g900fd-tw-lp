.class public Lcom/google/android/apps/books/app/RecommendUninstallActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "RecommendUninstallActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/RecommendUninstallActivity$RecommendUninstallFragment;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/books/app/RecommendUninstallActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/RecommendUninstallActivity$1;-><init>(Lcom/google/android/apps/books/app/RecommendUninstallActivity;)V

    .line 64
    .local v0, "settingsClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0146

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0147

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStart()V

    .line 47
    new-instance v0, Lcom/google/android/apps/books/app/RecommendUninstallActivity$RecommendUninstallFragment;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/RecommendUninstallActivity$RecommendUninstallFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/RecommendUninstallActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "recommendUninstall"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/RecommendUninstallActivity$RecommendUninstallFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 48
    return-void
.end method
