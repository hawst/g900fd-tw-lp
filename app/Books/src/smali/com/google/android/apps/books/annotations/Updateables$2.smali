.class final Lcom/google/android/apps/books/annotations/Updateables$2;
.super Ljava/lang/Object;
.source "Updateables.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/Updateable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/Updateables;->forPlaceholder(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;)Lcom/google/android/apps/books/annotations/Updateable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/annotations/Updateable",
        "<",
        "Lcom/google/android/apps/books/annotations/Annotation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$annotationContextSearch:Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

.field final synthetic val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$annotationContextSearch:Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canBeUpdated()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public currentBestValue()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;

    return-object v0
.end method

.method public bridge synthetic currentBestValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Updateables$2;->currentBestValue()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public update()Lcom/google/android/apps/books/annotations/Updateable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/annotations/Updateable",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v4}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    .line 43
    .local v2, "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v4}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/annotations/LayerId;->fromString(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/LayerId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/annotations/LayerId;->contextCanIncludeNearbyPages()Z

    move-result v1

    .line 45
    .local v1, "contextCanIncludeNearbyPages":Z
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$annotationContextSearch:Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v5}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v2, v5, v1}, Lcom/google/android/apps/books/annotations/AnnotationContextSearch;->retrieveContext(Lcom/google/android/apps/books/annotations/TextLocationRange;Ljava/lang/String;Z)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v0

    .line 48
    .local v0, "context":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/Updateables$2;->val$placeholder:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/books/annotations/Annotation;->withNewContext(Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v3

    .line 49
    .local v3, "replacement":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-static {v3}, Lcom/google/android/apps/books/annotations/Updateables;->finished(Ljava/lang/Object;)Lcom/google/android/apps/books/annotations/Updateable;

    move-result-object v4

    return-object v4
.end method
