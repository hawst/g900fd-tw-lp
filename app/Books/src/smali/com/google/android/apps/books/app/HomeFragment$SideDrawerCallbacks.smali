.class Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SideDrawerCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/HomeFragment$1;

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    return-void
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getLibraryMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getStartingViewMode()Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$400(Lcom/google/android/apps/books/app/HomeFragment;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onSideDrawerClosed()V
    .locals 2

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 278
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->wasSidePanelClosedOnce()Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setSidePanelWasClosedOnce(Z)V

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateMenu()V

    .line 282
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->updateActionBarTitle()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$000(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 283
    return-void
.end method

.method public onSideDrawerOpened()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateMenu()V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->updateActionBarTitle()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$000(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 289
    return-void
.end method

.method public refreshCurrentHomeView()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->showCurrentViewMode(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$300(Lcom/google/android/apps/books/app/HomeFragment;Z)V

    .line 313
    return-void
.end method

.method public showHelpAndFeedback()V
    .locals 6

    .prologue
    .line 336
    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_HELP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 339
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/HomeFragment;->access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/BooksHomeView;->getSnapshottableView()Landroid/view/View;

    move-result-object v3

    .line 340
    .local v3, "snapshotableView":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/HomeFragment;->access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/BooksHomeView;->getHelpContext()Ljava/lang/String;

    move-result-object v2

    .line 341
    .local v2, "helpContext":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 342
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v2, :cond_0

    .line 343
    invoke-static {v3, v0}, Lcom/google/android/apps/books/app/HelpUtils;->getScreenshot(Landroid/view/View;Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 344
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v4}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-interface {v4, v2, v5, v0, v1}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V

    .line 349
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 347
    :cond_0
    const-string v4, "HomeFragment"

    const-string v5, "Could not determine help context."

    invoke-static {v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public showMyLibrary()V
    .locals 3

    .prologue
    .line 305
    const-string v0, "mylibrary"

    .line 306
    .local v0, "mode":Ljava/lang/String;
    const-string v1, "mylibrary"

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeDrawerModeSwitch(Ljava/lang/String;Landroid/content/Context;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const-string v2, "mylibrary"

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->onUserSelectedViewMode(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    .line 308
    return-void
.end method

.method public showReadNow()V
    .locals 3

    .prologue
    .line 298
    const-string v0, "readnow"

    .line 299
    .local v0, "mode":Ljava/lang/String;
    const-string v1, "readnow"

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeDrawerModeSwitch(Ljava/lang/String;Landroid/content/Context;)V

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const-string v2, "readnow"

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->onUserSelectedViewMode(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public showSettings()V
    .locals 2

    .prologue
    .line 329
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_LAUNCH_APP_SETTINGS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->showAppSettingsActivity(Landroid/app/Activity;Z)V

    .line 332
    return-void
.end method

.method public showShop()V
    .locals 2

    .prologue
    .line 322
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->HOME_SIDE_DRAWER_SHOP:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    const-string v1, "books_inapp_home_options_shop"

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startShop(Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method public switchAccountTo(Ljava/lang/String;)V
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 267
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_CHANGE_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeAccountAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;)V

    .line 269
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 270
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, p1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 272
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksApplication;->restart(Landroid/app/Activity;)V

    .line 273
    return-void
.end method
