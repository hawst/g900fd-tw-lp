.class public Lcom/google/android/apps/books/render/LruPassagePurger;
.super Ljava/lang/Object;
.source "LruPassagePurger.java"


# instance fields
.field public final mPassages:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/util/PassagePages;",
            ">;"
        }
    .end annotation
.end field

.field private final mPurgeStartThreshold:I


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "purgeStartThreshold"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPurgeStartThreshold:I

    .line 36
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x16

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    .line 37
    return-void
.end method


# virtual methods
.method public clearPassages()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 44
    return-void
.end method

.method public getPossiblyPurgeablePassages()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v5, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 68
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v4

    .line 69
    .local v4, "purgeablePassages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 70
    .local v2, "pageCount":I
    iget-object v5, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newLinkedList(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v1

    .line 71
    .local v1, "linkedPassages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/apps/books/util/PassagePages;>;"
    invoke-virtual {v1}, Ljava/util/LinkedList;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    .line 77
    .local v0, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/util/PassagePages;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 78
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/util/PassagePages;

    .line 79
    .local v3, "passage":Lcom/google/android/apps/books/util/PassagePages;
    invoke-virtual {v3}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v5

    add-int/2addr v2, v5

    .line 80
    iget v5, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPurgeStartThreshold:I

    if-le v2, v5, :cond_0

    .line 81
    invoke-virtual {v3}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    .end local v3    # "passage":Lcom/google/android/apps/books/util/PassagePages;
    :cond_1
    const-string v5, "LruPassagePurger"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 85
    const-string v5, "LruPassagePurger"

    const-string v6, "getPossiblyPurgeablePassages pageCount=%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    .end local v0    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/util/PassagePages;>;"
    .end local v1    # "linkedPassages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/apps/books/util/PassagePages;>;"
    .end local v2    # "pageCount":I
    .end local v4    # "purgeablePassages":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    :cond_2
    :goto_1
    return-object v4

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    goto :goto_1
.end method

.method public markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V
    .locals 3
    .param p1, "passage"    # Lcom/google/android/apps/books/util/PassagePages;

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/PassagePages;

    .line 54
    .local v0, "oldPassage":Lcom/google/android/apps/books/util/PassagePages;
    if-nez v0, :cond_0

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_0
    return-void
.end method

.method public remove(Lcom/google/android/apps/books/util/PassagePages;)V
    .locals 2
    .param p1, "passage"    # Lcom/google/android/apps/books/util/PassagePages;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/books/render/LruPassagePurger;->mPassages:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/PassagePages;->getPassageIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    return-void
.end method
