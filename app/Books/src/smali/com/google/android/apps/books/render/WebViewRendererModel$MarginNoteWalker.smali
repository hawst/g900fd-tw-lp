.class Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;
.super Ljava/lang/Object;
.source "WebViewRendererModel.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRendererModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MarginNoteWalker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Lcom/google/android/apps/books/render/MarginNote;",
        ">;"
    }
.end annotation


# instance fields
.field mAnnotationIds:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAnnotations:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private mIncludePlainHighlights:Z

.field private mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/WebViewRendererModel;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/render/WebViewRendererModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/WebViewRendererModel$1;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel;)V

    return-void
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/render/MarginNote;)Z
    .locals 8
    .param p1, "note"    # Lcom/google/android/apps/books/render/MarginNote;

    .prologue
    const/4 v4, 0x0

    .line 91
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mAnnotationIds:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 92
    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mAnnotationIds:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 94
    .local v1, "annotationId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mAnnotations:Ljava/util/Collection;

    invoke-static {v5, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getAnnotationWithId(Ljava/util/Collection;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 96
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_0

    iget-boolean v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mIncludePlainHighlights:Z

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 97
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$000(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .line 98
    .local v2, "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    if-eqz v2, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget-object v6, v2, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->rectIntersectsPage(Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    invoke-static {v5, v6, v7}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$100(Lcom/google/android/apps/books/render/WebViewRendererModel;Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v3

    .line 100
    .local v3, "intersects":Z
    :goto_0
    if-eqz v3, :cond_0

    .line 101
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v4

    invoke-virtual {p1, v1, v4, v2}, Lcom/google/android/apps/books/render/MarginNote;->set(Ljava/lang/String;ILcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;)V

    .line 102
    const/4 v4, 0x1

    .line 106
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v1    # "annotationId":Ljava/lang/String;
    .end local v2    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    .end local v3    # "intersects":Z
    :cond_2
    return v4

    .restart local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .restart local v1    # "annotationId":Ljava/lang/String;
    .restart local v2    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    :cond_3
    move v3, v4

    .line 98
    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 70
    check-cast p1, Lcom/google/android/apps/books/render/MarginNote;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->next(Lcom/google/android/apps/books/render/MarginNote;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mMarginNoteIcons:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$000(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mAnnotationIds:Ljava/util/Iterator;

    .line 87
    return-void
.end method

.method public setup(Ljava/util/Collection;Lcom/google/android/apps/books/widget/DevicePageRendering;Z)V
    .locals 0
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "includePlainHighlights"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mAnnotations:Ljava/util/Collection;

    .line 80
    iput-object p2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 81
    iput-boolean p3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$MarginNoteWalker;->mIncludePlainHighlights:Z

    .line 82
    return-void
.end method
