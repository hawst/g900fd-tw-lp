.class Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;
.super Ljava/lang/Object;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NavigationCommand"
.end annotation


# instance fields
.field moveType:Lcom/google/android/apps/books/app/MoveType;

.field spread:Lcom/google/android/apps/books/render/SpreadIdentifier;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 0
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;->spread:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 634
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;->moveType:Lcom/google/android/apps/books/app/MoveType;

    .line 635
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/MoveType;
    .param p3, "x2"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 628
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;)V

    return-void
.end method
