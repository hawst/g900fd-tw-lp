.class final Lcom/google/android/apps/books/annotations/Annotation$1;
.super Ljava/lang/Object;
.source "Annotation.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/Annotation;->comparator(Ljava/util/Comparator;)Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/annotations/AnnotationSortingKey;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$locationComparator:Ljava/util/Comparator;


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/Annotation$1;->val$locationComparator:Ljava/util/Comparator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/annotations/AnnotationSortingKey;Lcom/google/android/apps/books/annotations/AnnotationSortingKey;)I
    .locals 4
    .param p1, "lhs"    # Lcom/google/android/apps/books/annotations/AnnotationSortingKey;
    .param p2, "rhs"    # Lcom/google/android/apps/books/annotations/AnnotationSortingKey;

    .prologue
    .line 373
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/Annotation$1;->val$locationComparator:Ljava/util/Comparator;

    invoke-interface {p1}, Lcom/google/android/apps/books/annotations/AnnotationSortingKey;->getSortingLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/apps/books/annotations/AnnotationSortingKey;->getSortingLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 375
    .local v0, "locationCompare":I
    if-eqz v0, :cond_0

    .line 378
    .end local v0    # "locationCompare":I
    :goto_0
    return v0

    .restart local v0    # "locationCompare":I
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/annotations/AnnotationSortingKey;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/apps/books/annotations/AnnotationSortingKey;->getLocalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 370
    check-cast p1, Lcom/google/android/apps/books/annotations/AnnotationSortingKey;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/annotations/AnnotationSortingKey;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/Annotation$1;->compare(Lcom/google/android/apps/books/annotations/AnnotationSortingKey;Lcom/google/android/apps/books/annotations/AnnotationSortingKey;)I

    move-result v0

    return v0
.end method
