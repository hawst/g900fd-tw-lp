.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;
.super Ljava/lang/Object;
.source "TextureEndOfBookView.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageManager$Ensurer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ensure()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;->val$activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;->val$account:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    .line 175
    .local v0, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    iget-object v2, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$2;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$100(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    return-void

    .line 177
    .end local v0    # "dc":Lcom/google/android/apps/books/data/BooksDataController;
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "TextureEndOfBookView"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    const-string v2, "TextureEndOfBookView"

    const-string v3, "Could not download cover for EOB"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    :cond_0
    invoke-static {v1}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v2

    throw v2
.end method
