.class Lcom/google/android/apps/books/app/ReaderFragment$29;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->handleVideoItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$item:Lcom/google/android/apps/books/render/TouchableItem;

.field final synthetic val$outerContext:Landroid/content/Context;

.field final synthetic val$progressBarContainer:Landroid/widget/FrameLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;Landroid/content/Context;Landroid/widget/FrameLayout;)V
    .locals 0

    .prologue
    .line 5679
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$outerContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$progressBarContainer:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private removeProgressBar()V
    .locals 3

    .prologue
    .line 5723
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$300(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 5724
    .local v0, "bookView":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 5725
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$progressBarContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 5728
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13000(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$progressBarContainer:Landroid/widget/FrameLayout;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5729
    return-void
.end method


# virtual methods
.method public onFileFound(Ljava/io/FileDescriptor;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 5716
    const-string v0, "ReaderFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5717
    const-string v0, "ReaderFragment"

    const-string v1, "onFileFound is not yet supported for video"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5719
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$29;->removeProgressBar()V

    .line 5720
    return-void
.end method

.method public onUrlFetchSucceeded(Ljava/lang/String;)V
    .locals 5
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 5682
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5683
    const-string v1, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUrlFetchSucceeded "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5685
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5703
    :goto_0
    return-void

    .line 5691
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$29;->removeProgressBar()V

    .line 5693
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$outerContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/books/app/FullScreenVideoActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 5695
    .local v0, "videoIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5696
    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTitle()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1500(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5697
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12900(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5698
    const-string v1, "expiration"

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 5702
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onUrlFetchedError()V
    .locals 3

    .prologue
    .line 5707
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5708
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUrlFetchedError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$29;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5710
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$29;->removeProgressBar()V

    .line 5712
    return-void
.end method
