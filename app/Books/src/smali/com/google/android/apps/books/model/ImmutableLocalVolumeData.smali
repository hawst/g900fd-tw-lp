.class public Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
.super Ljava/lang/Object;
.source "ImmutableLocalVolumeData.java"

# interfaces
.implements Lcom/google/android/apps/books/model/LocalVolumeData;


# static fields
.field public static final DEFAULT:Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;


# instance fields
.field private final mFitWidth:Z

.field private final mForceDownload:Z

.field private final mHasOfflineLicense:Z

.field private final mLastLocalAccess:J

.field private final mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field private final mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

.field private final mLineHeight:F

.field private final mPinned:Z

.field private final mTextZoom:F

.field private final mTimestamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 26
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    const/4 v6, 0x0

    const/high16 v7, 0x3fc00000    # 1.5f

    move v3, v2

    move-object v5, v4

    move-wide v10, v8

    move v12, v2

    move v13, v2

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    sput-object v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->DEFAULT:Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    return-void
.end method

.method public constructor <init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V
    .locals 1
    .param p1, "pinned"    # Z
    .param p2, "forceDownload"    # Z
    .param p3, "licenseAction"    # Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    .param p4, "lastMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p5, "textZoom"    # F
    .param p6, "lineHeight"    # F
    .param p7, "timestamp"    # J
    .param p9, "lastLocalAccess"    # J
    .param p11, "hasOfflineLicense"    # Z
    .param p12, "fitWidth"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mPinned:Z

    .line 34
    iput-boolean p2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mForceDownload:Z

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .line 36
    iput-object p4, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 37
    iput p5, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTextZoom:F

    .line 38
    iput p6, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLineHeight:F

    .line 39
    iput-wide p7, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    .line 40
    iput-wide p9, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    .line 41
    iput-boolean p11, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mHasOfflineLicense:Z

    .line 42
    iput-boolean p12, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mFitWidth:Z

    .line 43
    return-void
.end method

.method public static withFitWidth(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "fitWidth"    # Z

    .prologue
    .line 183
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    move v13, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withForceDownload(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "forceDownload"    # Z

    .prologue
    .line 118
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move v3, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withLastLocalAccess(Lcom/google/android/apps/books/model/LocalVolumeData;J)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 15
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "lastLocalAccess"    # J

    .prologue
    .line 159
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move-wide/from16 v10, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withLastMode(Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "lastMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 175
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move-object v5, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withLicenseAction(Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "licenseAction"    # Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .prologue
    .line 126
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move-object v4, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withLineHeight(Lcom/google/android/apps/books/model/LocalVolumeData;F)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "lineHeight"    # F

    .prologue
    .line 142
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move v7, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withOfflineLicense(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "hasOfflineLicense"    # Z

    .prologue
    .line 167
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move v12, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withPinned(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "pinned"    # Z

    .prologue
    .line 97
    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 99
    .local v10, "lastLocalAccess":J
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move v2, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1

    .line 97
    .end local v10    # "lastLocalAccess":J
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    goto :goto_0
.end method

.method public static withPinnedAndOfflineLicense(Lcom/google/android/apps/books/model/LocalVolumeData;ZZ)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "pinned"    # Z
    .param p2, "hasOfflineLicense"    # Z

    .prologue
    .line 108
    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 110
    .local v10, "lastLocalAccess":J
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move v2, p1

    move/from16 v12, p2

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1

    .line 108
    .end local v10    # "lastLocalAccess":J
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    goto :goto_0
.end method

.method public static withTextZoom(Lcom/google/android/apps/books/model/LocalVolumeData;F)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 14
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "textZoom"    # F

    .prologue
    .line 134
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTimestamp()J

    move-result-wide v8

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move v6, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method

.method public static withTimestamp(Lcom/google/android/apps/books/model/LocalVolumeData;J)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 15
    .param p0, "other"    # Lcom/google/android/apps/books/model/LocalVolumeData;
    .param p1, "timestamp"    # J

    .prologue
    .line 151
    new-instance v1, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v3

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getTextZoom()F

    move-result v6

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLineHeight()F

    move-result v7

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastLocalAccess()J

    move-result-wide v10

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v12

    invoke-interface {p0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getFitWidth()Z

    move-result v13

    move-wide/from16 v8, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    if-ne p0, p1, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v1

    .line 223
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 224
    goto :goto_0

    .line 225
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 226
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 227
    check-cast v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    .line 228
    .local v0, "other":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-boolean v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mFitWidth:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mFitWidth:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 229
    goto :goto_0

    .line 230
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mForceDownload:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mForceDownload:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 231
    goto :goto_0

    .line 232
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mHasOfflineLicense:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mHasOfflineLicense:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 233
    goto :goto_0

    .line 234
    :cond_6
    iget-wide v4, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    iget-wide v6, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    .line 235
    goto :goto_0

    .line 236
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    iget-object v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 237
    goto :goto_0

    .line 238
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    iget-object v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 239
    goto :goto_0

    .line 240
    :cond_9
    iget v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLineHeight:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLineHeight:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 241
    goto :goto_0

    .line 242
    :cond_a
    iget-boolean v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mPinned:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mPinned:Z

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 243
    goto :goto_0

    .line 244
    :cond_b
    iget v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTextZoom:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTextZoom:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 245
    goto :goto_0

    .line 246
    :cond_c
    iget-wide v4, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    iget-wide v6, v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 247
    goto :goto_0
.end method

.method public getFitWidth()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mFitWidth:Z

    return v0
.end method

.method public getForceDownload()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mForceDownload:Z

    return v0
.end method

.method public getLastLocalAccess()J
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    return-wide v0
.end method

.method public getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-object v0
.end method

.method public getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    return-object v0
.end method

.method public getLineHeight()F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLineHeight:F

    return v0
.end method

.method public getPinned()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mPinned:Z

    return v0
.end method

.method public getTextZoom()F
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTextZoom:F

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    return-wide v0
.end method

.method public hasOfflineLicense()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mHasOfflineLicense:Z

    return v0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v5, 0x0

    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    .line 203
    const/16 v0, 0x1f

    .line 204
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 205
    .local v1, "result":I
    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mFitWidth:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 206
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mForceDownload:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 207
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mHasOfflineLicense:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 208
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    iget-wide v8, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    .line 209
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-nez v2, :cond_3

    move v2, v5

    :goto_3
    add-int v1, v6, v2

    .line 210
    mul-int/lit8 v2, v1, 0x1f

    iget-object v6, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-nez v6, :cond_4

    :goto_4
    add-int v1, v2, v5

    .line 211
    mul-int/lit8 v2, v1, 0x1f

    iget v5, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLineHeight:F

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    add-int v1, v2, v5

    .line 212
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v5, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mPinned:Z

    if-eqz v5, :cond_5

    :goto_5
    add-int v1, v2, v3

    .line 213
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTextZoom:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 214
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    iget-wide v6, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    ushr-long/2addr v6, v10

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v1, v2, v3

    .line 215
    return v1

    :cond_0
    move v2, v4

    .line 205
    goto :goto_0

    :cond_1
    move v2, v4

    .line 206
    goto :goto_1

    :cond_2
    move v2, v4

    .line 207
    goto :goto_2

    .line 209
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->hashCode()I

    move-result v2

    goto :goto_3

    .line 210
    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-virtual {v5}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->hashCode()I

    move-result v5

    goto :goto_4

    :cond_5
    move v3, v4

    .line 212
    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 191
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "pinned"

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mPinned:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "forceDownload"

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mForceDownload:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "licenseAction"

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLicenseAction:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastMode"

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "textZoom"

    iget v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTextZoom:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;F)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lineHeight"

    iget v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLineHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;F)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "timestamp"

    iget-wide v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mTimestamp:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "hasOfflineLicense"

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mHasOfflineLicense:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastLocalAccess"

    iget-wide v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mLastLocalAccess:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "fitWidth"

    iget-boolean v2, p0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->mFitWidth:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
