.class final Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor$1;
.super Ljava/lang/Object;
.source "HandlerPeriodicTaskExecutor.java"

# interfaces
.implements Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createExecutor(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;I)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
    .locals 4
    .param p1, "callbacks"    # Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;
    .param p2, "delay"    # I

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    int-to-long v2, p2

    invoke-direct {v0, p1, v2, v3}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    return-object v0
.end method
