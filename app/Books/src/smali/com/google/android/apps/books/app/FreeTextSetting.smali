.class public Lcom/google/android/apps/books/app/FreeTextSetting;
.super Lcom/google/android/apps/books/app/AbstractSetting;
.source "FreeTextSetting.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;)V
    .locals 2
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "key"    # Lcom/google/android/apps/books/util/ConfigValue;

    .prologue
    .line 15
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/books/util/ConfigValue;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/app/AbstractSetting;-><init>(Ljava/lang/String;[Lcom/google/android/apps/books/util/ConfigValue;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected createValidView(Landroid/app/Activity;Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)Landroid/view/View;
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "res"    # Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;
    .param p3, "listener"    # Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;

    .prologue
    .line 21
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    iget v5, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->freetextViewLayoutId:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 23
    .local v3, "view":Landroid/view/View;
    iget v4, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionViewLabelViewId:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 24
    .local v2, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/FreeTextSetting;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    iget v4, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->freetextInputViewId:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 27
    .local v1, "edit":Landroid/widget/EditText;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/FreeTextSetting;->computeCurrentValues(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 29
    iget v4, p2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->freetextSubmitButtonViewId:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 30
    .local v0, "changeButton":Landroid/widget/Button;
    const-string v4, "Change"

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 31
    new-instance v4, Lcom/google/android/apps/books/app/FreeTextSetting$1;

    invoke-direct {v4, p0, p3, v1}, Lcom/google/android/apps/books/app/FreeTextSetting$1;-><init>(Lcom/google/android/apps/books/app/FreeTextSetting;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;Landroid/widget/EditText;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    return-object v3
.end method
