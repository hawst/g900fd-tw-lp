.class public interface abstract Lcom/google/android/apps/books/app/SearchResultMap;
.super Ljava/lang/Object;
.source "SearchResultMap.java"


# virtual methods
.method public abstract addSearchResult(Lcom/google/android/apps/books/model/SearchResult;)V
.end method

.method public abstract clear()V
.end method

.method public abstract findNextLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;
.end method

.method public abstract findNextPassageWithSearchResults(I)I
.end method

.method public abstract findPreviousLocationWithSearchResults(Lcom/google/android/apps/books/annotations/TextLocation;)Lcom/google/android/apps/books/annotations/TextLocation;
.end method

.method public abstract findPreviousPassageWithSearchResults(I)I
.end method

.method public abstract getFirstTextLocationForPassage(I)Lcom/google/android/apps/books/annotations/TextLocation;
.end method

.method public abstract getNumMatches()I
.end method

.method public abstract getNumMatchesBeforeTextLocation(Lcom/google/android/apps/books/annotations/TextLocation;)I
.end method

.method public abstract getTextLocationToSearchMatch(I)Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            "Lcom/google/android/apps/books/model/SearchMatchTextRange;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method
