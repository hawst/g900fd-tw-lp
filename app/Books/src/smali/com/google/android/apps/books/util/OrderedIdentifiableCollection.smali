.class public Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
.super Ljava/lang/Object;
.source "OrderedIdentifiableCollection.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/util/Identifiable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mIdToIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<TT;>;"
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p2, "idToIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mList:Ljava/util/List;

    .line 24
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mIdToIndex:Ljava/util/Map;

    .line 25
    return-void

    .line 24
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public static withEagerMap(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/apps/books/util/Identifiable;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "idToIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;-><init>(Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method

.method public static withLazyMap(Ljava/util/List;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/apps/books/util/Identifiable;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;-><init>(Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getIdToIndex()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mIdToIndex:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mList:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/books/util/Identifiables;->buildIdToIndex(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mIdToIndex:Ljava/util/Map;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mIdToIndex:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mList:Ljava/util/List;

    return-object v0
.end method

.method public getValueWithId(Ljava/lang/String;)Lcom/google/android/apps/books/util/Identifiable;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getIdToIndex()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 67
    .local v0, "index":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/Identifiable;

    .line 70
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 74
    .local p0, "this":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;, "Lcom/google/android/apps/books/util/OrderedIdentifiableCollection<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
