.class public abstract Lcom/google/android/apps/books/app/AbstractSetting;
.super Ljava/lang/Object;
.source "AbstractSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;,
        Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;
    }
.end annotation


# instance fields
.field private final mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

.field private final mLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lcom/google/android/apps/books/util/ConfigValue;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "keys"    # [Lcom/google/android/apps/books/util/ConfigValue;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mLabel:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/AbstractSetting;[Ljava/lang/String;Landroid/content/Context;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AbstractSetting;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Landroid/content/Context;
    .param p3, "x3"    # Z

    .prologue
    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/AbstractSetting;->applyValues([Ljava/lang/String;Landroid/content/Context;Z)Z

    move-result v0

    return v0
.end method

.method private applyValues([Ljava/lang/String;Landroid/content/Context;Z)Z
    .locals 4
    .param p1, "values"    # [Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "useGServices"    # Z

    .prologue
    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/AbstractSetting;->areCurrentValues([Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 89
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 90
    aget-object v2, p1, v0

    .line 91
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

    aget-object v1, v3, v0

    .line 92
    .local v1, "key":Lcom/google/android/apps/books/util/ConfigValue;
    invoke-direct {p0, v1, v2, p3, p2}, Lcom/google/android/apps/books/app/AbstractSetting;->override(Lcom/google/android/apps/books/util/ConfigValue;Ljava/lang/String;ZLandroid/content/Context;)V

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    .end local v1    # "key":Lcom/google/android/apps/books/util/ConfigValue;
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x1

    .line 96
    .end local v0    # "i":I
    :goto_1
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private canOverrideViaGservices()Z
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

    .local v0, "arr$":[Lcom/google/android/apps/books/util/ConfigValue;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 109
    .local v3, "value":Lcom/google/android/apps/books/util/ConfigValue;
    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ConfigValue;->canOverrideViaGservices()Z

    move-result v4

    if-nez v4, :cond_0

    .line 110
    const/4 v4, 0x0

    .line 113
    .end local v3    # "value":Lcom/google/android/apps/books/util/ConfigValue;
    :goto_1
    return v4

    .line 108
    .restart local v3    # "value":Lcom/google/android/apps/books/util/ConfigValue;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    .end local v3    # "value":Lcom/google/android/apps/books/util/ConfigValue;
    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private override(Lcom/google/android/apps/books/util/ConfigValue;Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 0
    .param p1, "key"    # Lcom/google/android/apps/books/util/ConfigValue;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "useGServices"    # Z
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    if-eqz p3, :cond_0

    .line 101
    invoke-virtual {p1, p4, p2}, Lcom/google/android/apps/books/util/ConfigValue;->overrideViaGServices(Landroid/content/Context;Ljava/lang/String;)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-virtual {p1, p4, p2}, Lcom/google/android/apps/books/util/ConfigValue;->overrideViaSharedPreferences(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected areCurrentValues([Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3
    .param p1, "values"    # [Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/app/AbstractSetting;->computeCurrentValues(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "currentValues":[Ljava/lang/String;
    array-length v1, v0

    array-length v2, p1

    if-eq v1, v2, :cond_0

    .line 74
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Each choice must have a value for each key."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_0
    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method protected computeCurrentValues(Landroid/content/Context;)[Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    iget-object v3, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

    array-length v3, v3

    new-array v2, v3, [Ljava/lang/String;

    .line 61
    .local v2, "values":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 62
    iget-object v3, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mKeys:[Lcom/google/android/apps/books/util/ConfigValue;

    aget-object v1, v3, v0

    .line 63
    .local v1, "key":Lcom/google/android/apps/books/util/ConfigValue;
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getRawString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    .end local v1    # "key":Lcom/google/android/apps/books/util/ConfigValue;
    :cond_0
    return-object v2
.end method

.method protected abstract createValidView(Landroid/app/Activity;Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)Landroid/view/View;
.end method

.method public final createView(Landroid/app/Activity;ZLcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;)Landroid/view/View;
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "overridesViaGservices"    # Z
    .param p3, "res"    # Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;
    .param p4, "listener"    # Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;

    .prologue
    .line 33
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/AbstractSetting;->canOverrideViaGservices()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/app/AbstractSetting$1;

    invoke-direct {v0, p0, p1, p2, p4}, Lcom/google/android/apps/books/app/AbstractSetting$1;-><init>(Lcom/google/android/apps/books/app/AbstractSetting;Landroid/app/Activity;ZLcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;)V

    invoke-virtual {p0, p1, p3, v0}, Lcom/google/android/apps/books/app/AbstractSetting;->createValidView(Landroid/app/Activity;Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected getLabel()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/books/app/AbstractSetting;->mLabel:Ljava/lang/String;

    return-object v0
.end method
