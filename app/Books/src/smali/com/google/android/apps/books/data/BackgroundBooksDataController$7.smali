.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->setPinned(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$pinned:Z

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->val$volumeId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->val$pinned:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->val$volumeId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->val$pinned:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->setPinned(Ljava/lang/String;Z)V

    .line 335
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->val$pinned:Z

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkTasks:Lcom/google/android/apps/books/data/NetworkTaskQueue;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/NetworkTaskQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->boostVolumePriority(Ljava/lang/String;)V

    .line 338
    :cond_0
    return-void
.end method
