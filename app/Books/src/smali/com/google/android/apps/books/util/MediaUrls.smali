.class public Lcom/google/android/apps/books/util/MediaUrls;
.super Ljava/lang/Object;
.source "MediaUrls.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;
    }
.end annotation


# instance fields
.field final mMediaFormats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    .line 257
    return-void
.end method

.method private getMediaFormats(Ljava/util/List;)Z
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "server_response":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    const/4 v15, 0x0

    .line 135
    .local v15, "mediaStreamList":[Ljava/lang/String;
    const-string v5, ","

    .line 136
    .local v5, "commaSeparator":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->clear()V

    .line 137
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/http/NameValuePair;

    .line 139
    .local v16, "nvPair":Lorg/apache/http/NameValuePair;
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "status"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 140
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v20

    const-string v21, "ok"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 141
    const-string v20, "MediaUrls"

    const/16 v21, 0x5

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 142
    const-string v20, "MediaUrls"

    const-string v21, "media URL exchange status=%s"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_1
    const/16 v20, 0x0

    .line 207
    .end local v16    # "nvPair":Lorg/apache/http/NameValuePair;
    :goto_1
    return v20

    .line 150
    .restart local v16    # "nvPair":Lorg/apache/http/NameValuePair;
    :cond_2
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "fmt_list"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 152
    :try_start_0
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v20

    const-string v21, "UTF-8"

    invoke-static/range {v20 .. v21}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 153
    .local v8, "fmt":Ljava/lang/String;
    const-string v20, ","

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 154
    .local v10, "fmtList":[Ljava/lang/String;
    move-object v4, v10

    .local v4, "arr$":[Ljava/lang/String;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_2
    if-ge v13, v14, :cond_0

    aget-object v9, v4, v13

    .line 156
    .local v9, "fmtElement":Ljava/lang/String;
    const-string v20, "/"

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 158
    .local v7, "fields":[Ljava/lang/String;
    const/16 v20, 0x1

    :try_start_1
    aget-object v20, v7, v20

    const-string v21, "x"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 159
    .local v17, "sizeFields":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v21, v7, v21

    new-instance v22, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;

    const/16 v23, 0x0

    aget-object v23, v17, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    const/16 v24, 0x1

    aget-object v24, v17, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;-><init>(Lcom/google/android/apps/books/util/MediaUrls;II)V

    invoke-virtual/range {v20 .. v22}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const-string v20, "MediaUrls"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 163
    const-string v20, "MediaUrls"

    const-string v21, " format %s is %s x %s"

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    aget-object v24, v7, v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    const/16 v24, 0x0

    aget-object v24, v17, v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    const/16 v24, 0x1

    aget-object v24, v17, v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 154
    .end local v17    # "sizeFields":[Ljava/lang/String;
    :cond_3
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 166
    :catch_0
    move-exception v6

    .line 167
    .local v6, "e":Ljava/lang/NumberFormatException;
    :try_start_2
    const-string v20, "MediaUrls"

    const/16 v21, 0x5

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 168
    const-string v20, "MediaUrls"

    const-string v21, "media URL exchange fmt_list parse failure"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 173
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    .end local v7    # "fields":[Ljava/lang/String;
    .end local v8    # "fmt":Ljava/lang/String;
    .end local v9    # "fmtElement":Ljava/lang/String;
    .end local v10    # "fmtList":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    :catch_1
    move-exception v6

    .line 174
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    const-string v20, "MediaUrls"

    const/16 v21, 0x5

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 175
    const-string v20, "MediaUrls"

    const-string v21, "media URL exchange response fmt_list decode failure"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    :cond_4
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 182
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_5
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "fmt_stream_map"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 183
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    .line 186
    .end local v16    # "nvPair":Lorg/apache/http/NameValuePair;
    :cond_6
    if-nez v15, :cond_8

    .line 187
    const-string v20, "MediaUrls"

    const/16 v21, 0x5

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 188
    const-string v20, "MediaUrls"

    const-string v21, "fmt_stream_map is missing"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_7
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 192
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->isEmpty()Z

    move-result v20

    if-eqz v20, :cond_a

    .line 193
    const-string v20, "MediaUrls"

    const/16 v21, 0x5

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 194
    const-string v20, "MediaUrls"

    const-string v21, "no media formats found"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_9
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 199
    :cond_a
    move-object v4, v15

    .restart local v4    # "arr$":[Ljava/lang/String;
    array-length v14, v4

    .restart local v14    # "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_4
    if-ge v12, v14, :cond_c

    aget-object v18, v4, v12

    .line 200
    .local v18, "stream":Ljava/lang/String;
    const-string v20, "\\|"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 201
    .local v19, "streamElements":[Ljava/lang/String;
    const/16 v20, 0x0

    aget-object v11, v19, v20

    .line 202
    .local v11, "format":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;

    const/16 v21, 0x1

    aget-object v21, v19, v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;->url:Ljava/lang/String;

    .line 199
    :cond_b
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 207
    .end local v11    # "format":Ljava/lang/String;
    .end local v18    # "stream":Ljava/lang/String;
    .end local v19    # "streamElements":[Ljava/lang/String;
    :cond_c
    const/16 v20, 0x1

    goto/16 :goto_1
.end method

.method public static restrictFormatToMp3Mp4(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0, "mediaServerUriBuilder"    # Landroid/net/Uri$Builder;

    .prologue
    .line 95
    const-string v0, "container"

    const-string v1, "mpg"

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public fetchUrls(Lcom/google/android/apps/books/net/ResponseGetter;Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 6
    .param p1, "responseGetter"    # Lcom/google/android/apps/books/net/ResponseGetter;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "mediaServerUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "container"

    const-string v5, "mpg"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 111
    .local v1, "mediaServerUrlBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v3, p2, v4}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 114
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Lorg/apache/http/HttpEntity;)Ljava/util/List;

    move-result-object v2

    .line 116
    .local v2, "server_response":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/util/MediaUrls;->getMediaFormats(Ljava/util/List;)Z

    move-result v3

    return v3
.end method

.method public getBestMediaUrl(I)Ljava/lang/String;
    .locals 12
    .param p1, "targetWidth"    # I

    .prologue
    .line 222
    const v0, 0x7fffffff

    .line 223
    .local v0, "bestDeltaWidth":I
    const/4 v2, 0x0

    .line 224
    .local v2, "bestMediaSource":Ljava/lang/String;
    const-string v1, "???"

    .line 227
    .local v1, "bestFormat":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/util/MediaUrls;->mMediaFormats:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 228
    .local v6, "mediaFormatEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;

    .line 229
    .local v5, "mediaFormat":Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;
    iget v7, v5, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;->width:I

    sub-int/2addr v7, p1

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 231
    .local v3, "deltaWidth":I
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/16 v8, 0x32

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 234
    iget-object v7, v5, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;->url:Ljava/lang/String;

    if-eqz v7, :cond_0

    if-ge v3, v0, :cond_0

    .line 235
    iget-object v2, v5, Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;->url:Ljava/lang/String;

    .line 236
    move v0, v3

    .line 237
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "bestFormat":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "bestFormat":Ljava/lang/String;
    goto :goto_0

    .line 241
    .end local v3    # "deltaWidth":I
    .end local v5    # "mediaFormat":Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;
    .end local v6    # "mediaFormatEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/util/MediaUrls$MediaFormat;>;"
    :cond_1
    if-nez v2, :cond_3

    .line 242
    const-string v7, "MediaUrls"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 243
    const-string v7, "MediaUrls"

    const-string v8, "No compatible media stream!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_2
    :goto_1
    return-object v2

    .line 245
    :cond_3
    const-string v7, "MediaUrls"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 246
    const-string v7, "MediaUrls"

    const-string v8, " selected format %s as best match to width %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    const/4 v10, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public loadMediaServerResponse(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "s://a/?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 130
    .local v0, "server_response":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/MediaUrls;->getMediaFormats(Ljava/util/List;)Z

    move-result v1

    return v1
.end method
