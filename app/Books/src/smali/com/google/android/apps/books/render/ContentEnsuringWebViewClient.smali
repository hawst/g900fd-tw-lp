.class public Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "ContentEnsuringWebViewClient.java"


# instance fields
.field public mCallback:Lcom/google/android/apps/books/render/ReaderListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->mCallback:Lcom/google/android/apps/books/render/ReaderListener;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->mCallback:Lcom/google/android/apps/books/render/ReaderListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderListener;->onReaderReady()V

    .line 36
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/google/android/apps/books/render/ReaderListener;)V
    .locals 0
    .param p1, "callback"    # Lcom/google/android/apps/books/render/ReaderListener;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->mCallback:Lcom/google/android/apps/books/render/ReaderListener;

    .line 23
    return-void
.end method
