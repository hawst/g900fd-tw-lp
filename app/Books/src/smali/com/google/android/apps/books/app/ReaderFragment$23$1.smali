.class Lcom/google/android/apps/books/app/ReaderFragment$23$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$23;->publishSearchResultBatch(ZILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$23;

.field final synthetic val$chapterHeading:Ljava/lang/String;

.field final synthetic val$isDone:Z

.field final synthetic val$newResults:Ljava/util/List;

.field final synthetic val$thisSearchSequenceNumber:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$23;ILjava/util/List;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 4541
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$23;

    iput p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$thisSearchSequenceNumber:I

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$newResults:Ljava/util/List;

    iput-boolean p4, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$isDone:Z

    iput-object p5, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->val$chapterHeading:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V
    .locals 2
    .param p1, "transientInfoCardsLayout"    # Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .prologue
    .line 4544
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$23;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$23;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReaderFragment$23$1$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$23$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->post(Ljava/lang/Runnable;)Z

    .line 4553
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 4541
    check-cast p1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$23$1;->take(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V

    return-void
.end method
