.class interface abstract Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;
.super Ljava/lang/Object;
.source "AnnotationServerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationServerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PaginatedResponseSource"
.end annotation


# virtual methods
.method public abstract getPage(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation
.end method
