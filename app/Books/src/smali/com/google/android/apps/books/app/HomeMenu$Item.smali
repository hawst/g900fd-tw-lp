.class public final enum Lcom/google/android/apps/books/app/HomeMenu$Item;
.super Ljava/lang/Enum;
.source "HomeMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/HomeMenu$Item;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/HomeMenu$Item;

.field public static final enum REFRESH:Lcom/google/android/apps/books/app/HomeMenu$Item;

.field public static final enum SEARCH:Lcom/google/android/apps/books/app/HomeMenu$Item;

.field public static final enum SORT:Lcom/google/android/apps/books/app/HomeMenu$Item;

.field public static final enum TESTING_OPTIONS:Lcom/google/android/apps/books/app/HomeMenu$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/google/android/apps/books/app/HomeMenu$Item;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/HomeMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    .line 26
    new-instance v0, Lcom/google/android/apps/books/app/HomeMenu$Item;

    const-string v1, "SORT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/HomeMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenu$Item;->SORT:Lcom/google/android/apps/books/app/HomeMenu$Item;

    .line 27
    new-instance v0, Lcom/google/android/apps/books/app/HomeMenu$Item;

    const-string v1, "REFRESH"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/HomeMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenu$Item;->REFRESH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    .line 28
    new-instance v0, Lcom/google/android/apps/books/app/HomeMenu$Item;

    const-string v1, "TESTING_OPTIONS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/HomeMenu$Item;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenu$Item;->TESTING_OPTIONS:Lcom/google/android/apps/books/app/HomeMenu$Item;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/app/HomeMenu$Item;

    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->SORT:Lcom/google/android/apps/books/app/HomeMenu$Item;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->REFRESH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->TESTING_OPTIONS:Lcom/google/android/apps/books/app/HomeMenu$Item;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenu$Item;->$VALUES:[Lcom/google/android/apps/books/app/HomeMenu$Item;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/HomeMenu$Item;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/books/app/HomeMenu$Item;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/HomeMenu$Item;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/HomeMenu$Item;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/books/app/HomeMenu$Item;->$VALUES:[Lcom/google/android/apps/books/app/HomeMenu$Item;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/HomeMenu$Item;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/HomeMenu$Item;

    return-object v0
.end method
