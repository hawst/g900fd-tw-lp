.class Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PaintableRectsCacheCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 4548
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "x1"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 4548
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    return-void
.end method


# virtual methods
.method public onCacheUpdated()V
    .locals 1

    .prologue
    .line 4552
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 4555
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigationDelayed()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8900(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 4556
    return-void
.end method

.method public onSearchBarNavigationStateChanged()V
    .locals 2

    .prologue
    .line 4560
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigationDelayed()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$8900(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 4561
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$9002(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 4562
    return-void
.end method
