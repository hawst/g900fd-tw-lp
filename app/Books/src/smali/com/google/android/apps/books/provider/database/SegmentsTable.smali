.class public Lcom/google/android/apps/books/provider/database/SegmentsTable;
.super Ljava/lang/Object;
.source "SegmentsTable.java"


# direct methods
.method public static getCreationSql()Ljava/lang/String;
    .locals 3

    .prologue
    .line 32
    const-string v0, "REFERENCES chapters(chapter_id)"

    .line 34
    .local v0, "refChapterId":Ljava/lang/String;
    const-string v1, "REFERENCES session_keys(_id)"

    .line 36
    .local v1, "refSessionKeyId":Ljava/lang/String;
    const-string v2, "CREATE TABLE segments (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL, segment_id TEXT NOT NULL, title TEXT NOT NULL, segment_order INTEGER NOT NULL, chapter_id TEXT NOT NULL REFERENCES chapters(chapter_id), page_count INTEGER NOT NULL DEFAULT 0, start_position TEXT NOT NULL, remote_url TEXT NOT NULL, content_status INTEGER NOT NULL DEFAULT 0, session_key_id INTEGER REFERENCES session_keys(_id), fixed_layout_version INTEGER NOT NULL DEFAULT 0, fixed_viewport_width INTEGER NOT NULL DEFAULT 0, fixed_viewport_height INTEGER NOT NULL DEFAULT 0, storage_format INTEGER, FOREIGN KEY(account_name, volume_id) REFERENCES volumes(account_name, volume_id), UNIQUE (account_name, volume_id, segment_id) ON CONFLICT REPLACE)"

    return-object v2
.end method

.method public static getViewSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "CREATE VIEW view_segments AS SELECT segments.*, session_keys.session_key_version AS session_key_version, session_keys.root_key_version AS root_key_version, session_keys.session_key_blob AS session_key_blob FROM segments LEFT OUTER JOIN session_keys ON session_keys._id=segments.session_key_id"

    return-object v0
.end method
