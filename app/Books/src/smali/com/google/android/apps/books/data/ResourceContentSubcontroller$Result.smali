.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Result"
.end annotation


# instance fields
.field final blob:Lcom/google/android/apps/books/data/DataControllerBlob;

.field final resourceRetriever:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

.field final sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;)V
    .locals 0
    .param p1, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p3, "resourceRetriever"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/DataControllerBlob;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    .line 90
    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 91
    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->resourceRetriever:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x2"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    .param p4, "x3"    # Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;)V

    return-void
.end method
