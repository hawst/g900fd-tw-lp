.class Lcom/google/android/apps/books/app/ReaderFragment$19;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/app/BookmarkController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;

.field final synthetic val$readerMenu:Lcom/google/android/apps/books/app/ReaderMenu;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderMenu;)V
    .locals 0

    .prologue
    .line 3880
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$19;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$19;->val$readerMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/app/BookmarkController;)V
    .locals 3
    .param p1, "bc"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 3883
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$19;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->createBookmarkListener()Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    move-result-object v0

    .line 3884
    .local v0, "listener":Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$19;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$19;->val$readerMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->keepAliveAsLong(Ljava/lang/Object;Ljava/lang/Object;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7200(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3885
    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/app/BookmarkController;->addBookmarkListener(Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;)V

    .line 3886
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 3880
    check-cast p1, Lcom/google/android/apps/books/app/BookmarkController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$19;->take(Lcom/google/android/apps/books/app/BookmarkController;)V

    return-void
.end method
