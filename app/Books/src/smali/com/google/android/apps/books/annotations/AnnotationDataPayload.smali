.class public interface abstract Lcom/google/android/apps/books/annotations/AnnotationDataPayload;
.super Ljava/lang/Object;
.source "AnnotationDataPayload.java"


# virtual methods
.method public abstract getCommon()Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
.end method

.method public abstract getDictionary()Lcom/google/android/apps/books/annotations/DictionaryEntry;
.end method

.method public abstract getGeo()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
.end method
