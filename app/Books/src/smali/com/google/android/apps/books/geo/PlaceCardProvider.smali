.class public Lcom/google/android/apps/books/geo/PlaceCardProvider;
.super Lcom/google/android/apps/books/app/InfoCardProvider;
.source "PlaceCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;,
        Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    }
.end annotation


# instance fields
.field private final mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

.field mConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mLoadingViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestSequenceNumber:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/annotations/VolumeAnnotationController;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "annotationController"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardProvider;-><init>()V

    .line 75
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;

    .line 78
    iput-object p1, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mContext:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/geo/PlaceCardProvider;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/geo/PlaceCardProvider;

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mRequestSequenceNumber:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/geo/PlaceCardProvider;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/geo/PlaceCardProvider;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public getSelectableLayerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->GEO:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 5
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/annotations/LayerId;->GEO:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v4}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 91
    iget v3, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mRequestSequenceNumber:I

    add-int/lit8 v1, v3, 0x1

    iput v1, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mRequestSequenceNumber:I

    .line 92
    .local v1, "thisRequest":I
    iput-object p2, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 93
    iget-object v3, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 95
    new-instance v0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;-><init>(Lcom/google/android/apps/books/geo/PlaceCardProvider;I)V

    .line 97
    .local v0, "callbacks":Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;
    new-instance v2, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;

    iget-object v3, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;-><init>(Lcom/google/android/apps/books/geo/PlaceCardProvider;Landroid/content/Context;)V

    .line 98
    .local v2, "wrapper":Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    invoke-virtual {v2}, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->getGeoAnnotationView()Lcom/google/android/apps/books/widget/GeoAnnotationView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    invoke-virtual {v3, p1, v4, v0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->setup(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/VolumeAnnotationController;Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;)V

    .line 99
    iget-object v3, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 101
    .end local v0    # "callbacks":Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;
    .end local v1    # "thisRequest":I
    .end local v2    # "wrapper":Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    :cond_0
    return-void
.end method
