.class Lcom/google/android/apps/books/app/ReaderFragment$15;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/annotations/VolumeAnnotationController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 3127
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$15;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/annotations/VolumeAnnotationController;)V
    .locals 3
    .param p1, "t"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .prologue
    .line 3130
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/books/annotations/AnnotationListener;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$15;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->getAnnotationListener()Lcom/google/android/apps/books/annotations/AnnotationListener;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->weaklyAddListeners([Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 3131
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 3127
    check-cast p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$15;->take(Lcom/google/android/apps/books/annotations/VolumeAnnotationController;)V

    return-void
.end method
