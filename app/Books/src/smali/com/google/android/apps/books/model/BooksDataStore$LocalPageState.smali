.class public interface abstract Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
.super Ljava/lang/Object;
.source "BooksDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LocalPageState"
.end annotation


# virtual methods
.method public abstract getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;
.end method

.method public abstract imageIsLocal()Z
.end method

.method public abstract structureIsDownloaded()Z
.end method
