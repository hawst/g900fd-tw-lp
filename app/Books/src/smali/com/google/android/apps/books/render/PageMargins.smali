.class public Lcom/google/android/apps/books/render/PageMargins;
.super Ljava/lang/Object;
.source "PageMargins.java"


# instance fields
.field public final horizontal:I

.field public final vertical:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "horizontalMargin"    # I
    .param p2, "verticalMargin"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/google/android/apps/books/render/PageMargins;->horizontal:I

    .line 12
    iput p2, p0, Lcom/google/android/apps/books/render/PageMargins;->vertical:I

    .line 13
    return-void
.end method
