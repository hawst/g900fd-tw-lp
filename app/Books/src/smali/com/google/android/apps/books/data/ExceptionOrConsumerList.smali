.class Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
.super Lcom/google/android/apps/books/data/ConsumerList;
.source "ExceptionOrConsumerList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/books/data/ConsumerList",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<TV;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerList;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerList<TV;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ConsumerList;-><init>()V

    return-void
.end method

.method public static createExceptionOrList()Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerList",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;-><init>()V

    return-object v0
.end method


# virtual methods
.method public publishFailure(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 44
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerList;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerList<TV;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishFailure(Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V

    .line 45
    return-void
.end method

.method public publishFailure(Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerList;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerList<TV;>;"
    .local p2, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TV;>;>;"
    invoke-static {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 40
    .local v0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TV;>;"
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 41
    return-void
.end method

.method public publishSuccess(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerList;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerList<TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    .local p2, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TV;>;>;"
    invoke-static {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 25
    .local v0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TV;>;"
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 26
    return-void
.end method
