.class public Lcom/google/android/apps/books/annotations/PageStructureLocation;
.super Ljava/lang/Object;
.source "PageStructureLocation.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/annotations/PageStructureLocation;",
        ">;"
    }
.end annotation


# static fields
.field private static final CFI_PATTERN:Ljava/util/regex/Pattern;

.field public static final NATURAL_ORDERING:Lcom/google/common/collect/Ordering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Ordering",
            "<",
            "Lcom/google/android/apps/books/annotations/PageStructureLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBlockIndex:I

.field private final mPageId:Ljava/lang/String;

.field private final mPageIndex:I

.field private final mParaIndex:I

.field private final mWordIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "/([0-9]+)\\[(.*)\\]/([0-9]+)[^/]*/([0-9]+)[^/]*/([0-9]+).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->CFI_PATTERN:Ljava/util/regex/Pattern;

    .line 45
    invoke-static {}, Lcom/google/common/collect/Ordering;->natural()Lcom/google/common/collect/Ordering;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->NATURAL_ORDERING:Lcom/google/common/collect/Ordering;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;III)V
    .locals 0
    .param p1, "pageIndex"    # I
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "blockIndex"    # I
    .param p4, "paraIndex"    # I
    .param p5, "wordIndex"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    .line 52
    iput p3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    .line 53
    iput p4, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    .line 54
    iput p5, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    .line 55
    return-void
.end method

.method private static cfiIndexToMortalIndex(Ljava/lang/String;)I
    .locals 1
    .param p0, "cfiIndex"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private static mortalIndexToCfiIndex(I)I
    .locals 1
    .param p0, "mortalIndex"    # I

    .prologue
    .line 23
    add-int/lit8 v0, p0, 0x1

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static parseCfi(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;
    .locals 7
    .param p0, "cfi"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;
        }
    .end annotation

    .prologue
    .line 58
    if-nez p0, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0

    .line 61
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->CFI_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 62
    .local v6, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    new-instance v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->cfiIndexToMortalIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v6, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->cfiIndexToMortalIndex(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x4

    invoke-virtual {v6, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->cfiIndexToMortalIndex(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x5

    invoke-virtual {v6, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->cfiIndexToMortalIndex(Ljava/lang/String;)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/PageStructureLocation;-><init>(ILjava/lang/String;III)V

    goto :goto_0

    .line 71
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/annotations/PageStructureLocation;)I
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/annotations/PageStructureLocation;

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    if-ne v0, v1, :cond_2

    .line 103
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    if-ne v0, v1, :cond_1

    .line 104
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    if-ne v0, v1, :cond_0

    .line 105
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    sub-int/2addr v0, v1

    .line 111
    :goto_0
    return v0

    .line 107
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 109
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 111
    :cond_2
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    iget v1, p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->compareTo(Lcom/google/android/apps/books/annotations/PageStructureLocation;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    if-ne p0, p1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v1

    .line 133
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 134
    goto :goto_0

    .line 136
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 137
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 139
    check-cast v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    .line 140
    .local v0, "other":Lcom/google/android/apps/books/annotations/PageStructureLocation;
    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    iget v4, v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 141
    goto :goto_0

    .line 143
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 144
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 145
    goto :goto_0

    .line 147
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 148
    goto :goto_0

    .line 150
    :cond_6
    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    iget v4, v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 151
    goto :goto_0

    .line 153
    :cond_7
    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    iget v4, v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 154
    goto :goto_0

    .line 156
    :cond_8
    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    iget v4, v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 157
    goto :goto_0
.end method

.method public getBlockIndex()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    return v0
.end method

.method public getPageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    return-object v0
.end method

.method public getParaIndex()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    return v0
.end method

.method public getWordIndex()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 118
    const/16 v0, 0x1f

    .line 119
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 120
    .local v1, "result":I
    iget v2, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    add-int/lit8 v1, v2, 0x1f

    .line 121
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 122
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    add-int v1, v2, v3

    .line 123
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    add-int v1, v2, v3

    .line 124
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    add-int v1, v2, v3

    .line 125
    return v1

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public pageAsTextLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    return-object v0
.end method

.method public toCfiString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageIndex:I

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mortalIndexToCfiIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mPageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mBlockIndex:I

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mortalIndexToCfiIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mParaIndex:I

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mortalIndexToCfiIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mWordIndex:I

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->mortalIndexToCfiIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
