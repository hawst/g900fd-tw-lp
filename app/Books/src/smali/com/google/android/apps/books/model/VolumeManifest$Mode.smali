.class public final enum Lcom/google/android/apps/books/model/VolumeManifest$Mode;
.super Ljava/lang/Enum;
.source "VolumeManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/VolumeManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/model/VolumeManifest$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field public static final enum AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field public static final enum FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field public static final enum IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;


# instance fields
.field private final mAnalyticsKey:Ljava/lang/String;

.field private final mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

.field private final mIntegerValue:I

.field private final mLayoutStyle:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v11, 0x3

    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v5, 0x1

    .line 62
    new-instance v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    const-string v1, "IMAGE"

    sget-object v3, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;->FIXED:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    const-string v6, "image"

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 65
    new-instance v3, Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    const-string v4, "FLOWING_TEXT"

    sget-object v6, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    sget-object v7, Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;->FLOWING:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    const-string v9, "flowing"

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;ILjava/lang/String;)V

    sput-object v3, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 69
    new-instance v6, Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    const-string v7, "AFL_TEXT"

    sget-object v9, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    sget-object v10, Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;->FIXED:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    const-string v12, "fixed"

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;ILjava/lang/String;)V

    sput-object v6, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 60
    new-array v0, v11, [Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->$VALUES:[Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/books/model/VolumeManifest$ContentFormat;Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;ILjava/lang/String;)V
    .locals 0
    .param p3, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .param p4, "layoutStyle"    # Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;
    .param p5, "integerValue"    # I
    .param p6, "analyticsKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;",
            "Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 90
    iput-object p4, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mLayoutStyle:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    .line 91
    iput p5, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mIntegerValue:I

    .line 92
    iput-object p6, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mAnalyticsKey:Ljava/lang/String;

    .line 93
    return-void
.end method

.method static fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/StringSafeCursor;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromInteger(I)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    goto :goto_0
.end method

.method public static fromInteger(I)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 5
    .param p0, "value"    # I

    .prologue
    .line 115
    invoke-static {}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->values()[Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 116
    .local v3, "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-virtual {v3}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getIntegerValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 120
    .end local v3    # "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :goto_1
    return-object v3

    .line 115
    .restart local v3    # "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    .end local v3    # "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static toInteger(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)I
    .locals 1
    .param p0, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 128
    if-eqz p0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getIntegerValue()I

    move-result v0

    .line 131
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    const-class v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->$VALUES:[Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/model/VolumeManifest$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-object v0
.end method


# virtual methods
.method public getAnalyticsKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mAnalyticsKey:Ljava/lang/String;

    return-object v0
.end method

.method public getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mContentFormat:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    return-object v0
.end method

.method public getIntegerValue()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mIntegerValue:I

    return v0
.end method

.method public getLayoutStyle()Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->mLayoutStyle:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    return-object v0
.end method
