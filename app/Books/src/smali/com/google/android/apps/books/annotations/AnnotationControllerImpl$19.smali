.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyEditFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$annotation:Lcom/google/android/apps/books/annotations/ServerAnnotation;

.field final synthetic val$layer:Ljava/lang/String;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/String;Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 0

    .prologue
    .line 1016
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$annotation:Lcom/google/android/apps/books/annotations/ServerAnnotation;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$layer:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V
    .locals 5
    .param p1, "cache"    # Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    .prologue
    .line 1019
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$annotation:Lcom/google/android/apps/books/annotations/ServerAnnotation;

    iget-object v2, v3, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 1020
    .local v2, "serverValue":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$layer:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getAnnotationMarkedForDeletion(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 1022
    .local v0, "beingDeleted":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_1

    .line 1029
    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/annotations/Annotation;->isUserChange(Lcom/google/android/apps/books/annotations/Annotation;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1037
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getForegroundAnnotationEditor(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v1

    .line 1038
    .local v1, "editor":Lcom/google/android/apps/books/annotations/UserChangesEditor;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;->val$layer:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->withFreshId()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/annotations/Updateables;->finished(Ljava/lang/Object;)Lcom/google/android/apps/books/annotations/Updateable;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Updateable;)V

    .line 1043
    .end local v1    # "editor":Lcom/google/android/apps/books/annotations/UserChangesEditor;
    :cond_0
    :goto_0
    return-void

    .line 1041
    :cond_1
    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->markEdited(Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0
.end method
