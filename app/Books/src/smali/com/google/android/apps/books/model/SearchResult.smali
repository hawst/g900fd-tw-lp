.class public Lcom/google/android/apps/books/model/SearchResult;
.super Ljava/lang/Object;
.source "SearchResult.java"


# instance fields
.field private mColor:I

.field private mHighlightInText:Z

.field private mItemType:I

.field private final mMatchRanges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;"
        }
    .end annotation
.end field

.field private final mSnippet:Lcom/google/android/apps/books/model/Snippet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/Snippet;Ljava/util/List;ZI)V
    .locals 1
    .param p1, "snippet"    # Lcom/google/android/apps/books/model/Snippet;
    .param p3, "highlightInText"    # Z
    .param p4, "color"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/Snippet;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p2, "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkArgument(Z)V

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/books/model/SearchResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/books/model/SearchResult;->mMatchRanges:Ljava/util/List;

    .line 50
    iput-boolean p3, p0, Lcom/google/android/apps/books/model/SearchResult;->mHighlightInText:Z

    .line 51
    iput p4, p0, Lcom/google/android/apps/books/model/SearchResult;->mColor:I

    .line 53
    return-void

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAnchorLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchResult;->mMatchRanges:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    return-object v0
.end method

.method public getItemType()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/apps/books/model/SearchResult;->mItemType:I

    return v0
.end method

.method public getMatchRanges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchResult;->mMatchRanges:Ljava/util/List;

    return-object v0
.end method

.method public getSnippet()Lcom/google/android/apps/books/model/Snippet;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/model/SearchResult;->mSnippet:Lcom/google/android/apps/books/model/Snippet;

    return-object v0
.end method

.method public setItemType(I)V
    .locals 0
    .param p1, "itemType"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/google/android/apps/books/model/SearchResult;->mItemType:I

    .line 92
    return-void
.end method
