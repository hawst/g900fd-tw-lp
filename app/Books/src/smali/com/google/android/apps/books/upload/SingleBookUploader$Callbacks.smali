.class public interface abstract Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
.super Ljava/lang/Object;
.source "SingleBookUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/SingleBookUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract abortUpload(Ljava/lang/String;Ljava/lang/Exception;Z)V
.end method

.method public abstract deleteUpload(Ljava/lang/String;)V
.end method

.method public abstract scheduleBackgroundTask(Ljava/lang/Runnable;)V
.end method

.method public abstract updateUploadProgress(Ljava/lang/String;I)V
.end method

.method public abstract updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V
.end method

.method public abstract updateUploadTransferHandle(Ljava/lang/String;Lcom/google/uploader/client/ClientProto$TransferHandle;)V
.end method

.method public abstract updateVolumeId(Ljava/lang/String;Ljava/lang/String;)V
.end method
