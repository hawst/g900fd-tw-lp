.class public Lcom/google/android/apps/books/net/HttpHelper;
.super Ljava/lang/Object;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;,
        Lcom/google/android/apps/books/net/HttpHelper$TokenExpiredException;,
        Lcom/google/android/apps/books/net/HttpHelper$AuthIoException;,
        Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;,
        Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;,
        Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;,
        Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
    }
.end annotation


# instance fields
.field private final mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

.field private final mConnectivityManager:Lcom/google/android/apps/books/net/BooksConnectivityManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/net/BooksConnectivityManager;Lcom/google/android/apps/books/net/BooksAccountManager;)V
    .locals 0
    .param p1, "connectivityManager"    # Lcom/google/android/apps/books/net/BooksConnectivityManager;
    .param p2, "accountManager"    # Lcom/google/android/apps/books/net/BooksAccountManager;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/books/net/HttpHelper;->mConnectivityManager:Lcom/google/android/apps/books/net/BooksConnectivityManager;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/books/net/HttpHelper;->mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

    .line 62
    return-void
.end method

.method private blockingGetAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/books/net/HttpHelper;->mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksAccountManager;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static consumeContentAndException(Lorg/apache/http/HttpEntity;)V
    .locals 3
    .param p0, "entity"    # Lorg/apache/http/HttpEntity;

    .prologue
    .line 387
    if-eqz p0, :cond_0

    .line 389
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "HttpHelper"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    const-string v1, "HttpHelper"

    const-string v2, "exception during consumeContent"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static wrapException(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;
    .locals 2
    .param p0, "e"    # Ljava/io/IOException;
    .param p1, "requestString"    # Ljava/lang/String;

    .prologue
    .line 155
    instance-of v0, p0, Ljava/net/UnknownHostException;

    if-eqz v0, :cond_0

    .line 156
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 169
    :goto_0
    return-object v0

    .line 157
    :cond_0
    instance-of v0, p0, Ljava/net/NoRouteToHostException;

    if-eqz v0, :cond_1

    .line 158
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 159
    :cond_1
    instance-of v0, p0, Ljava/net/ConnectException;

    if-eqz v0, :cond_2

    .line 160
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 161
    :cond_2
    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_3

    .line 163
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 164
    :cond_3
    instance-of v0, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_4

    .line 165
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 166
    :cond_4
    instance-of v0, p0, Lorg/apache/http/client/ClientProtocolException;

    if-eqz v0, :cond_5

    .line 167
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 169
    :cond_5
    new-instance v0, Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static wrapExceptionBasedOnStatus(ILjava/lang/String;Ljava/lang/Exception;)Ljava/io/IOException;
    .locals 3
    .param p0, "status"    # I
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Exception;

    .prologue
    .line 185
    packed-switch p0, :pswitch_data_0

    .line 198
    :pswitch_0
    div-int/lit8 v0, p0, 0x64

    .line 199
    .local v0, "statusClass":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 204
    const-string v1, "HttpHelper"

    invoke-static {v1, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 205
    const/4 v1, 0x0

    .line 237
    .end local v0    # "statusClass":I
    :goto_0
    return-object v1

    .line 192
    :pswitch_1
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 209
    .restart local v0    # "statusClass":I
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 224
    packed-switch v0, :pswitch_data_1

    .line 237
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 212
    :sswitch_0
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$TokenExpiredException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$TokenExpiredException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 214
    :sswitch_1
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$AuthIoException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$AuthIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 216
    :sswitch_2
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 218
    :sswitch_3
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 233
    :pswitch_2
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 235
    :pswitch_3
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 209
    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_0
        0x193 -> :sswitch_1
        0x1a1 -> :sswitch_2
        0x1f7 -> :sswitch_3
    .end sparse-switch

    .line 224
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 128
    if-nez p1, :cond_1

    move-object v0, v1

    .line 139
    :cond_0
    :goto_0
    return-object v0

    .line 131
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/net/HttpHelper;->blockingGetAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "authToken":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 136
    const-string v2, "HttpHelper"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    const-string v2, "HttpHelper"

    const-string v3, "null auth token"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v0, v1

    .line 139
    goto :goto_0
.end method

.method public invalidateAuthToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/net/HttpHelper;->mAccountManager:Lcom/google/android/apps/books/net/BooksAccountManager;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/net/BooksAccountManager;->invalidateAuthToken(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/net/HttpHelper;->mConnectivityManager:Lcom/google/android/apps/books/net/BooksConnectivityManager;

    invoke-interface {v0}, Lcom/google/android/apps/books/net/BooksConnectivityManager;->isDeviceConnected()Z

    move-result v0

    return v0
.end method

.method public setAuthToken(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)V
    .locals 2
    .param p1, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GoogleLogin auth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpHeaders;->setAuthorization(Ljava/lang/String;)Lcom/google/api/client/http/HttpHeaders;

    .line 400
    return-void
.end method

.method public shouldSkipRetry(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "requestString"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x3

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/books/net/HttpHelper;->isConnected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device offline, skipping "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "HttpHelper"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const-string v1, "HttpHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v1

    .line 97
    .end local v0    # "msg":Ljava/lang/String;
    .end local p1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    return-object p1

    .line 90
    .restart local p1    # "e":Ljava/io/IOException;
    :cond_2
    instance-of v1, p1, Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;

    if-nez v1, :cond_3

    instance-of v1, p1, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;

    if-eqz v1, :cond_4

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;

    invoke-virtual {v1}, Lcom/google/android/apps/books/net/HttpHelper$ClientIoException;->shouldRetry()Z

    move-result v1

    if-nez v1, :cond_4

    .line 92
    :cond_3
    const-string v1, "HttpHelper"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    const-string v1, "HttpHelper"

    const-string v2, "Skipping retry for"

    invoke-static {v1, v2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 97
    :cond_4
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public sleep()V
    .locals 4

    .prologue
    .line 106
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "ie":Ljava/lang/InterruptedException;
    const-string v1, "HttpHelper"

    const-string v2, "Interrupted while waiting to retry"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
