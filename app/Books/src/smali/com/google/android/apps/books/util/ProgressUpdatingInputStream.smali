.class public Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;
.super Ljava/io/FilterInputStream;
.source "ProgressUpdatingInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;
    }
.end annotation


# instance fields
.field private mCancelled:Z

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalBytesRead:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 21
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mListeners:Ljava/util/List;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mTotalBytesRead:I

    .line 27
    return-void
.end method

.method private notifyListeners()V
    .locals 3

    .prologue
    .line 74
    iget-object v2, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;

    .line 75
    .local v1, "listener":Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;
    iget v2, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mTotalBytesRead:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;->onProgress(I)V

    goto :goto_0

    .line 77
    .end local v1    # "listener":Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mCancelled:Z

    .line 44
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mTotalBytesRead:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mTotalBytesRead:I

    .line 64
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->notifyListeners()V

    .line 65
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-boolean v1, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mCancelled:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_0
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 51
    .local v0, "bytesRead":I
    iget v1, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mTotalBytesRead:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mTotalBytesRead:I

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->notifyListeners()V

    .line 53
    return v0
.end method

.method public registerProgressListener(Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method
