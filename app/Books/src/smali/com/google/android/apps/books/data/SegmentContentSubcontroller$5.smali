.class Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;
.super Ljava/lang/Object;
.source "SegmentContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$result:Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$result:Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 310
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->copyListeners()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 311
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, v2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, v3, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$result:Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    iget-object v4, v4, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->resources:Ljava/util/List;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/books/model/BooksDataListener;->onNewSegmentResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 315
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;->val$result:Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    const/4 v5, 0x1

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V
    invoke-static {v2, p1, v3, v4, v5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$900(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V

    .line 316
    return-void
.end method
