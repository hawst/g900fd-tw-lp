.class Lcom/google/android/apps/books/data/PageContentSubcontroller$10;
.super Ljava/lang/Object;
.source "PageContentSubcontroller.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageContentSubcontroller;->localCcBoxSupplier(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/android/apps/books/model/CcBox;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/android/apps/books/model/CcBox;
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, v2, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getCcBox(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/CcBox;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/PageContentSubcontroller$10;->get()Lcom/google/android/apps/books/model/CcBox;

    move-result-object v0

    return-object v0
.end method
