.class public Lcom/google/android/apps/books/view/pages/Texture;
.super Ljava/lang/Object;
.source "Texture.java"


# static fields
.field private static final sTextureIds:[I


# instance fields
.field private mHeight:I

.field private mTextureId:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/books/view/pages/Texture;->sTextureIds:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deleteTextureId()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    if-eqz v0, :cond_0

    .line 33
    sget-object v0, Lcom/google/android/apps/books/view/pages/Texture;->sTextureIds:[I

    iget v1, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    aput v1, v0, v2

    .line 34
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/apps/books/view/pages/Texture;->sTextureIds:[I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 35
    iput v2, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    .line 37
    :cond_0
    return-void
.end method

.method public genTextureId()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already have valid id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/apps/books/view/pages/Texture;->sTextureIds:[I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 28
    sget-object v0, Lcom/google/android/apps/books/view/pages/Texture;->sTextureIds:[I

    aget v0, v0, v2

    iput v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    .line 29
    return-void
.end method

.method public getTextureDimensions(Landroid/graphics/Point;)Z
    .locals 1
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mWidth:I

    iput v0, p1, Landroid/graphics/Point;->x:I

    .line 49
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mHeight:I

    iput v0, p1, Landroid/graphics/Point;->y:I

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/Texture;->isTextureIdAllocated()Z

    move-result v0

    return v0
.end method

.method public getTextureId()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    return v0
.end method

.method public isTextureIdAllocated()Z
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/Texture;->deleteTextureId()V

    .line 68
    return-void
.end method

.method public setExtent(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/google/android/apps/books/view/pages/Texture;->mWidth:I

    .line 55
    iput p2, p0, Lcom/google/android/apps/books/view/pages/Texture;->mHeight:I

    .line 56
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mTextureId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/view/pages/Texture;->mTextureId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
