.class Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$1;
.super Ljava/lang/Object;
.source "LocalDictionarySelectorDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;

.field final synthetic val$dictionaryItemsAdapter:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$1;->this$0:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;

    iput-object p2, p0, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$1;->val$dictionaryItemsAdapter:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$1;->val$dictionaryItemsAdapter:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;

    .line 64
    .local v0, "item":Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->onClick(Landroid/view/View;)V

    .line 65
    return-void
.end method
