.class Lcom/google/android/apps/books/app/ReaderFragment$4;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 1040
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$4;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1043
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$4;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    .line 1044
    .local v0, "fragment":Lcom/google/android/apps/books/app/ReaderFragment;
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1045
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1046
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$4;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowRecentSearchesPopup(Ljava/util/List;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1800(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;)V

    .line 1049
    :cond_0
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1040
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$4;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
