.class Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;
.super Ljava/lang/Object;
.source "NotesExportDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/NotesExportDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/NotesExportDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/NotesExportDialogFragment;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    iget-object v0, v0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mListener:Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    iget-object v0, v0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mListener:Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;

    iget-object v1, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    # invokes: Lcom/google/android/apps/books/app/NotesExportDialogFragment;->currentCheckbox()Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->access$000(Lcom/google/android/apps/books/app/NotesExportDialogFragment;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;->this$0:Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    # invokes: Lcom/google/android/apps/books/app/NotesExportDialogFragment;->currentFolder()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->access$100(Lcom/google/android/apps/books/app/NotesExportDialogFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;->onFinishNotesExportDialog(ZLjava/lang/String;)V

    .line 87
    :cond_0
    return-void
.end method
