.class Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;
.super Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;
.source "OffersActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/OffersFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OffersCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/OffersFragment$Callbacks;",
        ">.FragmentCallbacks;",
        "Lcom/google/android/apps/books/app/OffersFragment$Callbacks;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/OffersActivity;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$FragmentCallbacks;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/OffersActivity;Lcom/google/android/apps/books/app/OffersActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/OffersActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/OffersActivity$1;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;-><init>(Lcom/google/android/apps/books/app/OffersActivity;)V

    return-void
.end method

.method private exitActivity()V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    # getter for: Lcom/google/android/apps/books/app/OffersActivity;->mIsFromWidget:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/OffersActivity;->access$000(Lcom/google/android/apps/books/app/OffersActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    iget-object v2, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/OffersActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalHomeIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/OffersActivity;->startActivity(Landroid/content/Intent;)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/OffersActivity;->finish()V

    .line 69
    return-void
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "onResult":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->this$0:Lcom/google/android/apps/books/app/OffersActivity;

    const-string v2, "OffersActivity.offers"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/OffersActivity;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/OffersFragment;

    .line 75
    .local v0, "fragment":Lcom/google/android/apps/books/app/OffersFragment;
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/app/OffersFragment;->acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 78
    :cond_0
    return-void
.end method

.method public getSystemUi()Lcom/google/android/ublib/view/SystemUi;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "coverView"    # Landroid/view/View;

    .prologue
    .line 82
    return-void
.end method

.method public onOfferAccepted()V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->startForcedSync()V

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->exitActivity()V

    .line 56
    return-void
.end method

.method public onOfferDismissed(Ljava/lang/String;)V
    .locals 0
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->exitActivity()V

    .line 49
    return-void
.end method

.method public onUserAcknowledgesFailure()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;->exitActivity()V

    .line 61
    return-void
.end method

.method public startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "helpContext"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "screenshot"    # Landroid/graphics/Bitmap;

    .prologue
    .line 92
    return-void
.end method
