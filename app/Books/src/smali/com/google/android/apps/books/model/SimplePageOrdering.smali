.class public Lcom/google/android/apps/books/model/SimplePageOrdering;
.super Ljava/lang/Object;
.source "SimplePageOrdering.java"

# interfaces
.implements Lcom/google/android/apps/books/common/Position$PageOrdering;


# instance fields
.field private final mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/model/SimplePageOrdering;->mMap:Ljava/util/Map;

    .line 19
    return-void
.end method


# virtual methods
.method public getPageIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 23
    iget-object v1, p0, Lcom/google/android/apps/books/model/SimplePageOrdering;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 24
    .local v0, "pageIndex":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 27
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No page matching ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
