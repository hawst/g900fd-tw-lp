.class Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;
.super Ljava/lang/Object;
.source "SingleBookUploaderImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->sendApiaryRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 273
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->getScottyContentId()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$800(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$100(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mAccount:Landroid/accounts/Account;
    invoke-static {v5}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$200(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Landroid/accounts/Account;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v3

    .line 276
    .local v3, "server":Lcom/google/android/apps/books/net/BooksServer;
    invoke-interface {v3, v0}, Lcom/google/android/apps/books/net/BooksServer;->addCloudloadingVolume(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;

    move-result-object v2

    .line 278
    .local v2, "response":Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->handleApiaryResponse(Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;)V
    invoke-static {v4, v2}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$900(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    .end local v0    # "contentId":Ljava/lang/String;
    .end local v2    # "response":Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    .end local v3    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :goto_0
    return-void

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "SingleBookUploaderImpl"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 281
    const-string v4, "SingleBookUploaderImpl"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    new-instance v5, Ljava/lang/Exception;

    const-string v6, "Failed to execute apiary request"

    invoke-direct {v5, v6, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v6, 0x0

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$400(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Ljava/lang/Exception;Z)V

    goto :goto_0
.end method
