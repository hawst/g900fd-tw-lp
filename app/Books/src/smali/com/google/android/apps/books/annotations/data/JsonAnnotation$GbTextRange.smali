.class public Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;
.super Ljava/lang/Object;
.source "JsonAnnotation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GbTextRange"
.end annotation


# instance fields
.field public endOffset:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "endOffset"
    .end annotation
.end field

.field public endPosition:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "endPosition"
    .end annotation
.end field

.field public startOffset:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "startOffset"
    .end annotation
.end field

.field public startPosition:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "startPosition"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
