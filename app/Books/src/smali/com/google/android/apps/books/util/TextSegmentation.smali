.class public Lcom/google/android/apps/books/util/TextSegmentation;
.super Ljava/lang/Object;
.source "TextSegmentation.java"


# instance fields
.field public final offsets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final text:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "offsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/util/TextSegmentation;->text:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    .line 29
    return-void
.end method

.method public static findNearestWord(Ljava/lang/String;IILjava/util/Locale;)Landroid/graphics/Point;
    .locals 10
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "searchDistance"    # I
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v9, 0x0

    .line 72
    invoke-static {p0, p3}, Lcom/google/android/apps/books/util/TextSegmentation;->findWords(Ljava/lang/String;Ljava/util/Locale;)Lcom/google/android/apps/books/util/TextSegmentation;

    move-result-object v3

    .line 73
    .local v3, "ss":Lcom/google/android/apps/books/util/TextSegmentation;
    const/4 v2, 0x0

    .line 76
    .local v2, "result":Landroid/graphics/Point;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_2

    .line 77
    iget-object v6, v3, Lcom/google/android/apps/books/util/TextSegmentation;->text:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v8, v1, 0x1

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "thisWord":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/util/TextSegmentation;->isWord(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 76
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_1
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-gt v5, p1, :cond_3

    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v5, p1, :cond_3

    .line 86
    new-instance v2, Landroid/graphics/Point;

    .end local v2    # "result":Landroid/graphics/Point;
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v2, v6, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 105
    .end local v4    # "thisWord":Ljava/lang/String;
    :cond_2
    return-object v2

    .line 87
    .restart local v2    # "result":Landroid/graphics/Point;
    .restart local v4    # "thisWord":Ljava/lang/String;
    :cond_3
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-gt v5, p1, :cond_5

    .line 89
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int v5, p1, v5

    add-int/lit8 v0, v5, 0x1

    .line 90
    .local v0, "distance":I
    if-gt v0, p2, :cond_0

    .line 91
    if-nez v2, :cond_4

    new-instance v2, Landroid/graphics/Point;

    .end local v2    # "result":Landroid/graphics/Point;
    invoke-direct {v2, v9, v9}, Landroid/graphics/Point;-><init>(II)V

    .line 92
    .restart local v2    # "result":Landroid/graphics/Point;
    :cond_4
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v6, v5}, Landroid/graphics/Point;->set(II)V

    .line 93
    move p2, v0

    goto/16 :goto_1

    .line 97
    .end local v0    # "distance":I
    :cond_5
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int v0, v5, p1

    .line 98
    .restart local v0    # "distance":I
    if-gt v0, p2, :cond_0

    .line 99
    if-nez v2, :cond_6

    new-instance v2, Landroid/graphics/Point;

    .end local v2    # "result":Landroid/graphics/Point;
    invoke-direct {v2, v9, v9}, Landroid/graphics/Point;-><init>(II)V

    .line 100
    .restart local v2    # "result":Landroid/graphics/Point;
    :cond_6
    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v5, v3, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v6, v5}, Landroid/graphics/Point;->set(II)V

    .line 101
    move p2, v0

    goto/16 :goto_1
.end method

.method public static findWords(Ljava/lang/String;Ljava/util/Locale;)Lcom/google/android/apps/books/util/TextSegmentation;
    .locals 4
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 43
    if-eqz p1, :cond_1

    invoke-static {p1}, Ljava/text/BreakIterator;->getWordInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    .line 45
    .local v0, "boundary":Ljava/text/BreakIterator;
    :goto_0
    invoke-virtual {v0, p0}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v1, "offsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    invoke-virtual {v0}, Ljava/text/BreakIterator;->current()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-virtual {v0}, Ljava/text/BreakIterator;->next()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 53
    new-instance v2, Lcom/google/android/apps/books/util/TextSegmentation;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/books/util/TextSegmentation;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v2

    .line 43
    .end local v0    # "boundary":Ljava/text/BreakIterator;
    .end local v1    # "offsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    invoke-static {}, Ljava/text/BreakIterator;->getWordInstance()Ljava/text/BreakIterator;

    move-result-object v0

    goto :goto_0
.end method

.method private static isWord(Ljava/lang/String;)Z
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 115
    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    .line 116
    .local v1, "letter":I
    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    const/4 v2, 0x1

    .line 120
    .end local v1    # "letter":I
    :goto_1
    return v2

    .line 114
    .restart local v1    # "letter":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    .end local v1    # "letter":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
