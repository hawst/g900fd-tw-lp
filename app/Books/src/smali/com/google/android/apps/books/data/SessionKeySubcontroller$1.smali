.class Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;->getValidAccountSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

.field final synthetic val$callbacks:Lcom/google/android/apps/books/data/ControlTaskServices;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$fromStore:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->val$fromStore:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p4, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->val$callbacks:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 94
    iget-object v1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$100(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;
    invoke-static {v2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$000(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v0

    .line 96
    .local v0, "startNewTask":Z
    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->val$fromStore:Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;->val$callbacks:Lcom/google/android/apps/books/data/ControlTaskServices;

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->upgradeKeyOnNetworkThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$200(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 99
    :cond_0
    return-void
.end method
