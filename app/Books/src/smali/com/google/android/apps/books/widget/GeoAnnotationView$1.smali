.class Lcom/google/android/apps/books/widget/GeoAnnotationView$1;
.super Ljava/lang/Object;
.source "GeoAnnotationView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/GeoAnnotationView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$1;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 138
    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$1;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    # invokes: Lcom/google/android/apps/books/widget/GeoAnnotationView;->createMapIntent()Landroid/content/Intent;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->access$000(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Landroid/content/Intent;

    move-result-object v1

    .line 139
    .local v1, "mapIntent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 140
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_MAP_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logGeoAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;)V

    .line 142
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView$1;->this$0:Lcom/google/android/apps/books/widget/GeoAnnotationView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "GeoAnnotationView"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    const-string v2, "GeoAnnotationView"

    const-string v3, "User doesn\'t have a map app installed"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
