.class public abstract Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;
.super Ljava/lang/Object;
.source "AppSettingsMultipleOptionsItem.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/apps/books/app/AppSettingsItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mLabel:I

.field private final mListener:Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;

.field private final mOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/util/List;Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;)V
    .locals 0
    .param p1, "label"    # I
    .param p3, "listener"    # Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p2, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mLabel:I

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mOptions:Ljava/util/List;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mListener:Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;

    .line 35
    return-void
.end method


# virtual methods
.method protected abstract getValue()Ljava/lang/CharSequence;
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mContext:Landroid/content/Context;

    .line 40
    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 41
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040027

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 42
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0e00af

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 43
    .local v1, "tv":Landroid/widget/TextView;
    iget v4, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mLabel:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 45
    const v4, 0x7f0e00b0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 46
    .local v2, "tv_summary":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->getValue()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    return-object v3
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichItemClicked"    # I

    .prologue
    .line 64
    const/4 v1, -0x2

    if-eq p2, v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mOptions:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 66
    .local v0, "selected":Ljava/lang/CharSequence;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->setValue(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mListener:Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;->notifySettingChanged()V

    .line 68
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 70
    .end local v0    # "selected":Ljava/lang/CharSequence;
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 53
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v3, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mLabel:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mOptions:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mOptions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->mOptions:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;->getValue()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v2, v4, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 58
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 59
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 60
    return-void
.end method

.method protected abstract setValue(Ljava/lang/CharSequence;)V
.end method
