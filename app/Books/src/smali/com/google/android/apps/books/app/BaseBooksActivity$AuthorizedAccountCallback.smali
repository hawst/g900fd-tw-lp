.class Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;
.super Ljava/lang/Object;
.source "BaseBooksActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AuthorizedAccountCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 664
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.AuthorizedAccountCallback;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/BaseBooksActivity$1;

    .prologue
    .line 664
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.AuthorizedAccountCallback;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.AuthorizedAccountCallback;"
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/4 v7, 0x3

    .line 668
    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 701
    :goto_0
    return-void

    .line 673
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    sget-object v5, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    # setter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccountState:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;
    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$602(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;)Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 676
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 677
    .local v1, "bundle":Landroid/os/Bundle;
    new-instance v0, Landroid/accounts/Account;

    const-string v4, "authAccount"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "accountType"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    .local v0, "account":Landroid/accounts/Account;
    const-string v4, "authtoken"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    .line 682
    .local v3, "validAccount":Z
    if-eqz v3, :cond_2

    .line 683
    const-string v4, "BooksActivity"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 684
    const-string v4, "BooksActivity"

    const-string v5, "Successfully authorized account"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v5, 0x1

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->setAccount(Landroid/accounts/Account;Z)V
    invoke-static {v4, v0, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$700(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/accounts/Account;Z)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 690
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "validAccount":Z
    :catch_0
    move-exception v2

    .line 691
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    const-string v4, "BooksActivity"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 692
    const-string v4, "BooksActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "problem authorizing account: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v4, "BooksActivity"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 698
    const-string v4, "BooksActivity"

    const-string v5, "Restarting account picking after authorization failure"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$AuthorizedAccountCallback;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->showAccountPicker()V
    invoke-static {v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$800(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    goto :goto_0

    .line 690
    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    goto :goto_1
.end method
