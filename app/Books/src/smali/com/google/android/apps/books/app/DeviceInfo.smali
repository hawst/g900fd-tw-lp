.class public Lcom/google/android/apps/books/app/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/DeviceInfo$1;,
        Lcom/google/android/apps/books/app/DeviceInfo$Builder;
    }
.end annotation


# instance fields
.field private final mAndroidId:Ljava/lang/String;

.field private final mDeviceName:Ljava/lang/String;

.field private final mManufacturer:Ljava/lang/String;

.field private final mModel:Ljava/lang/String;

.field private final mProductName:Ljava/lang/String;

.field private final mSerialNumber:Ljava/lang/String;

.field private final mUseFakeOffersData:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "androidId"    # Ljava/lang/String;
    .param p2, "serialNumber"    # Ljava/lang/String;
    .param p3, "model"    # Ljava/lang/String;
    .param p4, "manufacturer"    # Ljava/lang/String;
    .param p5, "deviceName"    # Ljava/lang/String;
    .param p6, "productName"    # Ljava/lang/String;
    .param p7, "useFakeOffersData"    # Z

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mAndroidId:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mSerialNumber:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mModel:Ljava/lang/String;

    .line 79
    iput-object p4, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mManufacturer:Ljava/lang/String;

    .line 80
    iput-object p5, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 81
    iput-object p6, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mProductName:Ljava/lang/String;

    .line 82
    iput-boolean p7, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mUseFakeOffersData:Z

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/DeviceInfo$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Ljava/lang/String;
    .param p7, "x6"    # Z
    .param p8, "x7"    # Lcom/google/android/apps/books/app/DeviceInfo$1;

    .prologue
    .line 9
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/books/app/DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public getAndroidId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mAndroidId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mProductName:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public useFakeOffersData()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/DeviceInfo;->mUseFakeOffersData:Z

    return v0
.end method
