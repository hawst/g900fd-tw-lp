.class public final Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Blobs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;",
        "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->buildParsed()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 1

    .prologue
    .line 436
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 487
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->buildPartial()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 3

    .prologue
    .line 445
    new-instance v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;-><init>()V

    .line 446
    .local v0, "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    new-instance v1, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;-><init>(Lcom/google/android/apps/books/annotations/proto/Blobs$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    .line 447
    return-object v0
.end method


# virtual methods
.method public addInfo(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .prologue
    .line 563
    if-nez p1, :cond_0

    .line 564
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1002(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;Ljava/util/List;)Ljava/util/List;

    .line 569
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    return-object p0
.end method

.method public build()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 478
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->buildPartial()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .locals 3

    .prologue
    .line 491
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    if-nez v1, :cond_0

    .line 492
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 496
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1002(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;Ljava/util/List;)Ljava/util/List;

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    .line 500
    .local v0, "returnMe":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    .line 501
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 2

    .prologue
    .line 464
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->mergeFrom(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->clone()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    .prologue
    .line 505
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->getDefaultInstance()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-object p0

    .line 506
    :cond_1
    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1002(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;Ljava/util/List;)Ljava/util/List;

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->result:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->info_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->access$1000(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 520
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 521
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 525
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 526
    :sswitch_0
    return-object p0

    .line 531
    :sswitch_1
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->newBuilder()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    .line 532
    .local v0, "subBuilder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 533
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->buildPartial()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->addInfo(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    goto :goto_0

    .line 521
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    return-object v0
.end method
