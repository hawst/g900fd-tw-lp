.class Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;
.super Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;
.source "DataControllerUtils.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InputStreamOpeningConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/InputStreamSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mGetCalled:Z

.field result:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;-><init>()V

    .line 189
    return-void
.end method


# virtual methods
.method public getResult()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->mGetCalled:Z

    if-eqz v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "get() called more than once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->mGetCalled:Z

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->block()V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->throwIfFailure()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    return-object v0
.end method

.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-interface {v1}, Lcom/google/android/apps/books/data/InputStreamSource;->openInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->unblock()V

    .line 203
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    goto :goto_0

    .line 200
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 182
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
