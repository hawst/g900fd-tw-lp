.class public Lcom/google/android/apps/books/dictionary/LocalDictionaryUtils;
.super Ljava/lang/Object;
.source "LocalDictionaryUtils.java"


# direct methods
.method public static getDisplayLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 51
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUpdateLists(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)",
            "Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "serverDictList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p1, "localDictList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {p0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v7

    .line 21
    .local v7, "serverList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {p1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 23
    .local v2, "localList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 24
    .local v4, "newestVersionServerOnly":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 25
    .local v3, "newestVersionLocal":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 27
    .local v5, "olderVersionLocal":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "li":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 28
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 29
    .local v1, "localDict":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "si":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 30
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 31
    .local v6, "serverDict":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v6}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 32
    invoke-virtual {v6}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getVersion()J

    move-result-wide v10

    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getVersion()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 33
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    .line 39
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 35
    :cond_2
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 44
    .end local v1    # "localDict":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .end local v6    # "serverDict":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .end local v8    # "si":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    :cond_3
    invoke-interface {v4, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 45
    invoke-interface {v5, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 46
    new-instance v9, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;

    invoke-direct {v9, v4, v3, v5}, Lcom/google/android/apps/books/dictionary/DictionaryUpdateLists;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v9
.end method
