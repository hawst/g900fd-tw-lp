.class Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;
.super Ljava/lang/Object;
.source "BooksSafeFetchController.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/BooksSafeFetchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncContext"
.end annotation


# instance fields
.field private final mStorageSequenceNumber:Ljava/lang/Integer;

.field final synthetic this$0:Lcom/google/android/apps/books/service/BooksSafeFetchController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/BooksSafeFetchController;)V
    .locals 1

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->this$0:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    # getter for: Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;
    invoke-static {p1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->access$000(Lcom/google/android/apps/books/service/BooksSafeFetchController;)Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/common/FileStorageManager;->getSequenceNumber()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->mStorageSequenceNumber:Ljava/lang/Integer;

    .line 44
    return-void
.end method

.method private throwChangedException(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/service/SyncContextChangedException;
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/apps/books/service/SyncContextChangedException;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/service/SyncContextChangedException;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, "exception":Lcom/google/android/apps/books/service/SyncContextChangedException;
    const-string v1, "BooksSyncEnsurer"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    const-string v1, "BooksSyncEnsurer"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 63
    :cond_0
    throw v0
.end method


# virtual methods
.method public check()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/service/SyncContextChangedException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->mStorageSequenceNumber:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->this$0:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    # getter for: Lcom/google/android/apps/books/service/BooksSafeFetchController;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;
    invoke-static {v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->access$000(Lcom/google/android/apps/books/service/BooksSafeFetchController;)Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/common/FileStorageManager;->getSequenceNumber()I

    move-result v0

    .line 51
    .local v0, "currentSequenceNumber":I
    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->mStorageSequenceNumber:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 52
    const-string v1, "File storage sequence number changed"

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController$SyncContext;->throwChangedException(Ljava/lang/String;)V

    .line 55
    .end local v0    # "currentSequenceNumber":I
    :cond_0
    return-void
.end method
