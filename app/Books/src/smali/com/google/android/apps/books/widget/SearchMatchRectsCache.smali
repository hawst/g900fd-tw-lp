.class public abstract Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
.super Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;
.source "SearchMatchRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;,
        Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final mDisplayTwoPages:Z

.field protected final mIsRightToLeft:Z

.field protected mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

.field protected final mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ZZLcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V
    .locals 1
    .param p1, "loader"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
    .param p2, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p3, "displayTwoPages"    # Z
    .param p4, "isRightToLeft"    # Z
    .param p5, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/apps/books/widget/SearchMatchRectsCache;, "Lcom/google/android/apps/books/widget/SearchMatchRectsCache<TT;>;"
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    .line 34
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mDisplayTwoPages:Z

    .line 35
    iput-boolean p4, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mIsRightToLeft:Z

    .line 36
    invoke-interface {p2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getSearchResultMap()Lcom/google/android/apps/books/app/SearchResultMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    .line 37
    return-void
.end method


# virtual methods
.method protected finishProcessRequestResult()V
    .locals 1

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/apps/books/widget/SearchMatchRectsCache;, "Lcom/google/android/apps/books/widget/SearchMatchRectsCache<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onCacheUpdated()V

    .line 55
    return-void
.end method

.method protected abstract getHighlightRectsWalker(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ")",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMatchIndexForSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z
.end method

.method public getNumMatches()I
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/google/android/apps/books/widget/SearchMatchRectsCache;, "Lcom/google/android/apps/books/widget/SearchMatchRectsCache<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SearchResultMap;->getNumMatches()I

    move-result v0

    return v0
.end method

.method protected getPaintables(I)Ljava/util/List;
    .locals 3
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lcom/google/android/apps/books/widget/SearchMatchRectsCache;, "Lcom/google/android/apps/books/widget/SearchMatchRectsCache<TT;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 85
    .local v1, "resultSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/app/SearchResultMap;->getTextLocationToSearchMatch(I)Ljava/util/SortedMap;

    move-result-object v0

    .line 87
    .local v0, "map":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/model/SearchMatchTextRange;>;"
    if-eqz v0, :cond_0

    .line 88
    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 90
    :cond_0
    return-object v1
.end method

.method public abstract hasNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
.end method

.method public abstract hasPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
.end method

.method public abstract maybeMoveToPendingMatch()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method protected abstract moveToPrevOrNextMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public setPendingSearchMatch(Lcom/google/android/apps/books/render/PendingSearchMatchData;)V
    .locals 0
    .param p1, "pendingSearchMatch"    # Lcom/google/android/apps/books/render/PendingSearchMatchData;

    .prologue
    .line 97
    .local p0, "this":Lcom/google/android/apps/books/widget/SearchMatchRectsCache;, "Lcom/google/android/apps/books/widget/SearchMatchRectsCache<TT;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->mPendingSearchMatch:Lcom/google/android/apps/books/render/PendingSearchMatchData;

    .line 98
    return-void
.end method
