.class Lcom/google/android/apps/books/widget/PassageSearchMatchData;
.super Lcom/google/android/apps/books/widget/PassageHighlightRects;
.source "PassageSearchMatchData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;
    }
.end annotation


# instance fields
.field public final localIdToRect:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field public final pageIndexToMatchRange:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/TreeMap;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "localIdToRect":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/render/LabeledRect;>;"
    .local p2, "pageIndexToMatchRange":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PassageSearchMatchData$MatchRange;>;"
    .local p3, "paintableTextRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    .local p4, "rects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p3, p4, v0}, Lcom/google/android/apps/books/widget/PassageHighlightRects;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->localIdToRect:Ljava/util/Map;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PassageSearchMatchData;->pageIndexToMatchRange:Ljava/util/TreeMap;

    .line 25
    return-void
.end method
