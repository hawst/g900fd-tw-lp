.class public Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;
.super Ljava/lang/Object;
.source "LocalAnnotationDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnsyncedEdit"
.end annotation


# instance fields
.field public final annotation:Lcom/google/android/apps/books/annotations/Annotation;

.field public final serverId:Ljava/lang/String;

.field public final volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;Ljava/lang/String;)V
    .locals 0
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p3, "serverId"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 47
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;->serverId:Ljava/lang/String;

    .line 48
    return-void
.end method
