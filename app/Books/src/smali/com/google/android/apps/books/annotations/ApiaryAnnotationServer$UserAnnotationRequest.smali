.class public Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
.super Ljava/lang/Object;
.source "ApiaryAnnotationServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserAnnotationRequest"
.end annotation


# instance fields
.field public final lastSyncDate:J

.field public final layerId:Ljava/lang/String;

.field public final vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)V
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layerId"    # Ljava/lang/String;
    .param p3, "lastSyncDate"    # J

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->layerId:Ljava/lang/String;

    .line 78
    iput-wide p3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->lastSyncDate:J

    .line 79
    return-void
.end method


# virtual methods
.method public fullRefresh()Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
    .locals 6

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->layerId:Ljava/lang/String;

    const-wide/16 v4, -0x1

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;-><init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)V

    return-object v0
.end method
