.class public Lcom/google/android/apps/books/app/HomeMenuImpl;
.super Ljava/lang/Object;
.source "HomeMenuImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HomeMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/HomeMenuImpl$SearchResultColumns;,
        Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static final mItemResourceIds:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Lcom/google/android/apps/books/app/HomeMenu$Item;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

.field private mLastQuery:Ljava/lang/String;

.field private final mMenu:Landroid/view/Menu;

.field private final mSearchMenuItem:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "HomeMenuImpl"

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenuImpl;->TAG:Ljava/lang/String;

    .line 54
    invoke-static {}, Lcom/google/android/apps/books/app/HomeMenuImpl;->buildItemResourceIdMap()Lcom/google/common/collect/BiMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mItemResourceIds:Lcom/google/common/collect/BiMap;

    return-void
.end method

.method public constructor <init>(Landroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/apps/books/app/HomeMenu$Callbacks;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-string v0, "Null menu"

    invoke-static {p1, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const-string v0, "Null inflater"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-string v0, "Null callbacks"

    invoke-static {p3, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    .line 72
    const/high16 v0, 0x7f120000

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mMenu:Landroid/view/Menu;

    .line 75
    const v0, 0x7f0e020f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mSearchMenuItem:Landroid/view/MenuItem;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mSearchMenuItem:Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeMenuImpl;->createSearchView(Landroid/view/MenuItem;)Landroid/support/v7/widget/SearchView;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/HomeMenuImpl;)Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeMenuImpl;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/HomeMenuImpl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeMenuImpl;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mLastQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/app/HomeMenuImpl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeMenuImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mLastQuery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/HomeMenuImpl;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeMenuImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeMenuImpl;->setSearchViewExpanded(Z)V

    return-void
.end method

.method private static buildItemResourceIdMap()Lcom/google/common/collect/BiMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Lcom/google/android/apps/books/app/HomeMenu$Item;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Lcom/google/common/collect/HashBiMap;->create()Lcom/google/common/collect/HashBiMap;

    move-result-object v0

    .line 58
    .local v0, "result":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Lcom/google/android/apps/books/app/HomeMenu$Item;Ljava/lang/Integer;>;"
    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    const v2, 0x7f0e020f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->SORT:Lcom/google/android/apps/books/app/HomeMenu$Item;

    const v2, 0x7f0e0210

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->REFRESH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    const v2, 0x7f0e0005

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v1, Lcom/google/android/apps/books/app/HomeMenu$Item;->TESTING_OPTIONS:Lcom/google/android/apps/books/app/HomeMenu$Item;

    const v2, 0x7f0e0211

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-object v0
.end method

.method private configureSearchView(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchView"    # Landroid/support/v7/widget/SearchView;

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;-><init>(Landroid/content/Context;)V

    .line 121
    .local v0, "adapter":Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;
    new-instance v1, Lcom/google/android/apps/books/app/HomeMenuImpl$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/HomeMenuImpl$1;-><init>(Lcom/google/android/apps/books/app/HomeMenuImpl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;->setFilterQueryProvider(Landroid/widget/FilterQueryProvider;)V

    .line 157
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/SearchView;->setSuggestionsAdapter(Landroid/support/v4/widget/CursorAdapter;)V

    .line 158
    new-instance v1, Lcom/google/android/apps/books/app/HomeMenuImpl$2;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/apps/books/app/HomeMenuImpl$2;-><init>(Lcom/google/android/apps/books/app/HomeMenuImpl;Lcom/google/android/apps/books/app/HomeMenuImpl$LocalSearchAdapter;Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {p2, v1}, Landroid/support/v7/widget/SearchView;->setOnSuggestionListener(Landroid/support/v7/widget/SearchView$OnSuggestionListener;)V

    .line 197
    new-instance v1, Lcom/google/android/apps/books/app/HomeMenuImpl$3;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/books/app/HomeMenuImpl$3;-><init>(Lcom/google/android/apps/books/app/HomeMenuImpl;Landroid/support/v7/widget/SearchView;)V

    invoke-virtual {p2, v1}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 223
    const v1, 0x7f0901aa

    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/books/app/HomeMenuImpl;->configureSearchViewLayoutParams(Landroid/support/v7/widget/SearchView;I)V

    .line 224
    return-void
.end method

.method private configureSearchViewLayoutParams(Landroid/support/v7/widget/SearchView;I)V
    .locals 2
    .param p1, "searchView"    # Landroid/support/v7/widget/SearchView;
    .param p2, "searchWidth"    # I

    .prologue
    .line 227
    new-instance v0, Landroid/support/v7/app/ActionBar$LayoutParams;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/support/v7/app/ActionBar$LayoutParams;-><init>(I)V

    .line 228
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1}, Landroid/support/v7/widget/SearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 229
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/SearchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    return-void
.end method

.method private createSearchView(Landroid/view/MenuItem;)Landroid/support/v7/widget/SearchView;
    .locals 7
    .param p1, "searchMenu"    # Landroid/view/MenuItem;

    .prologue
    .line 107
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 108
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v4, 0x7f0a01f1

    invoke-direct {v1, v0, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 109
    .local v1, "darkContext":Landroid/content/Context;
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 110
    .local v3, "searchViewInflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0400bd

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 111
    .local v2, "newSearchView":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mSearchMenuItem:Landroid/view/MenuItem;

    invoke-static {v4, v2}, Landroid/support/v4/view/MenuItemCompat;->setActionView(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    move-object v4, v2

    .line 112
    check-cast v4, Landroid/support/v7/widget/SearchView;

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/books/app/HomeMenuImpl;->configureSearchView(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V

    .line 113
    check-cast v2, Landroid/support/v7/widget/SearchView;

    .end local v2    # "newSearchView":Landroid/view/View;
    return-object v2
.end method

.method private findMenuItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mMenu:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method private setItemVisible(IZ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeMenuImpl;->findMenuItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 87
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 88
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 90
    const v1, 0x7f0e0210

    if-ne p1, v1, :cond_0

    .line 91
    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v0, v1}, Landroid/support/v4/view/MenuItemCompat;->setShowAsAction(Landroid/view/MenuItem;I)V

    .line 95
    :cond_0
    return-void

    .line 91
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setSearchViewExpanded(Z)V
    .locals 1
    .param p1, "expanded"    # Z

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mSearchMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 279
    if-eqz p1, :cond_1

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mSearchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->expandActionView(Landroid/view/MenuItem;)Z

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mSearchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 234
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    move v2, v3

    .line 264
    :cond_0
    :goto_0
    return v2

    .line 236
    :sswitch_0
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_MENU_SEARCH_REQUESTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;Ljava/lang/Long;)V

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeMenuImpl;->onSearchRequested()Z

    goto :goto_0

    .line 242
    :sswitch_1
    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_REFRESH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 243
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 244
    .local v0, "activity":Landroid/app/Activity;
    const v4, 0x7f0f005e

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 246
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    invoke-interface {v3}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->startRefresh()V

    goto :goto_0

    .line 250
    .end local v0    # "activity":Landroid/app/Activity;
    :sswitch_2
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_LAUNCH_APP_SETTINGS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 252
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    invoke-interface {v3}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->showAppSettingsActivity(Landroid/app/Activity;Z)V

    goto :goto_0

    .line 256
    :sswitch_3
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    invoke-interface {v3}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 257
    .restart local v0    # "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 258
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/books/app/TestingOptionsActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 234
    :sswitch_data_0
    .sparse-switch
        0x7f0e0005 -> :sswitch_1
        0x7f0e020f -> :sswitch_0
        0x7f0e0211 -> :sswitch_3
        0x7f0e021c -> :sswitch_2
    .end sparse-switch
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 273
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeMenuImpl;->setSearchViewExpanded(Z)V

    .line 274
    return v0
.end method

.method public setItemVisible(Lcom/google/android/apps/books/app/HomeMenu$Item;Z)V
    .locals 1
    .param p1, "item"    # Lcom/google/android/apps/books/app/HomeMenu$Item;
    .param p2, "visible"    # Z

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/apps/books/app/HomeMenuImpl;->mItemResourceIds:Lcom/google/common/collect/BiMap;

    invoke-interface {v0, p1}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/books/app/HomeMenuImpl;->setItemVisible(IZ)V

    .line 83
    return-void
.end method

.method public tearDown()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeMenuImpl;->setSearchViewExpanded(Z)V

    .line 104
    return-void
.end method
