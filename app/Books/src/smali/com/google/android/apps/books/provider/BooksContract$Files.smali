.class public final Lcom/google/android/apps/books/provider/BooksContract$Files;
.super Ljava/lang/Object;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Files"
.end annotation


# static fields
.field public static final URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3357
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract;->BASE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "files"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$Files;->URI:Landroid/net/Uri;

    return-void
.end method

.method public static buildPageContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 3422
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 3423
    .local v0, "volumeDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "pages"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static buildPageContentFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;

    .prologue
    .line 3550
    new-instance v0, Ljava/io/File;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static buildPageStructureDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 3432
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 3433
    .local v0, "volumeDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "structure"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static buildPageStructureFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;

    .prologue
    .line 3558
    new-instance v0, Ljava/io/File;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageStructureDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static buildResContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 3412
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 3413
    .local v1, "volumeDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v2, "res2"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3414
    .local v0, "typeDir":Ljava/io/File;
    return-object v0
.end method

.method public static buildResContentFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "resId"    # Ljava/lang/String;

    .prologue
    .line 3541
    invoke-static {p3}, Lcom/google/android/apps/books/provider/BooksContract$Files;->resourceIdToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3542
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildResContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static buildSectionContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 3402
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 3403
    .local v1, "volumeDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v2, "segments"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3404
    .local v0, "typeDir":Ljava/io/File;
    return-object v0
.end method

.method public static buildSectionContentFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 3482
    new-instance v0, Ljava/io/File;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSectionContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static buildSharedResContentDir(Ljava/io/File;)Ljava/io/File;
    .locals 2
    .param p0, "baseDir"    # Ljava/io/File;

    .prologue
    .line 3441
    new-instance v0, Ljava/io/File;

    const-string v1, "shared_res"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3442
    .local v0, "typeDir":Ljava/io/File;
    return-object v0
.end method

.method public static buildSharedResContentFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 3565
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSharedResContentDir(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static buildSharedResMD5File(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 3572
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSharedResContentDir(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".md5"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static buildVolumeDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 3391
    invoke-static {p0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildVolumesDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 3392
    .local v1, "volumesDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3393
    .local v0, "volumeDir":Ljava/io/File;
    return-object v0
.end method

.method public static buildVolumesDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0, "baseDir"    # Ljava/io/File;
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 3381
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 3382
    .local v0, "acctDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "volumes"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3383
    .local v1, "volumesDir":Ljava/io/File;
    return-object v1
.end method

.method public static resourceIdToFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 3512
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Files;->resourceIdToUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3515
    .local v3, "resourceUrl":Ljava/lang/String;
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 3516
    .local v0, "digest":Ljava/security/MessageDigest;
    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 3519
    .local v1, "digestBytes":[B
    const/16 v4, 0xa

    invoke-static {v1, v4}, Lcom/google/android/apps/books/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    return-object v4

    .line 3520
    .end local v0    # "digest":Ljava/security/MessageDigest;
    .end local v1    # "digestBytes":[B
    .end local v3    # "resourceUrl":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 3521
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Missing SHA-1 digest"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3522
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 3523
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Missing UTF-8 charset"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static resourceIdToUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "resourceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 3532
    new-instance v0, Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lcom/google/android/apps/books/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public static urlToResourceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "resourceUrl"    # Ljava/lang/String;

    .prologue
    .line 3491
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 3493
    :catch_0
    move-exception v0

    .line 3494
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unsupported encoding UTF-8"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
