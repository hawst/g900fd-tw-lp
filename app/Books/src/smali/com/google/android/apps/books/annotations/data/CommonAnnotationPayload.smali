.class public Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
.super Ljava/lang/Object;
.source "CommonAnnotationPayload.java"


# instance fields
.field public final lang:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public final previewImageUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public final snippet:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public final snippetUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "snippet"    # Ljava/lang/String;
    .param p3, "snippetUrl"    # Ljava/lang/String;
    .param p4, "previewImageUrl"    # Ljava/lang/String;
    .param p5, "title"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->lang:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->snippet:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->snippetUrl:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->previewImageUrl:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->title:Ljava/lang/String;

    .line 27
    return-void
.end method
