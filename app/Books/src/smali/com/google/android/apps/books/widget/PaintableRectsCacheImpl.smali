.class public abstract Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;
.super Ljava/lang/Object;
.source "PaintableRectsCacheImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;
.implements Lcom/google/android/apps/books/widget/PaintableRectsCache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;",
        "Lcom/google/android/apps/books/widget/PaintableRectsCache",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final mCacheIndexToData:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mCacheIndexToLoadRequestId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected final mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

.field private final mKnownCompleteCacheIndices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoader:Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;

.field protected final mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V
    .locals 1
    .param p1, "loader"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
    .param p2, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    .prologue
    .line 40
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToData:Landroid/util/SparseArray;

    .line 76
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mKnownCompleteCacheIndices:Ljava/util/Set;

    .line 83
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mLoader:Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mLoader:Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;

    invoke-interface {v0, p0}, Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;->weaklyAddWebLoadListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V

    .line 43
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    .line 45
    return-void
.end method

.method private updateDrawableAnnotationsCache(Lcom/google/common/collect/Multimap;IZ)V
    .locals 8
    .param p2, "cacheIndex"    # I
    .param p3, "shouldInvalidate"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 215
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    .local p1, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v5, "rects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/apps/books/render/LabeledRect;>;"
    if-eqz p1, :cond_0

    .line 217
    invoke-interface {p1}, Lcom/google/common/collect/Multimap;->entries()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 218
    .local v4, "rectEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/graphics/Rect;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 219
    .local v0, "annotationId":Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/books/render/LabeledRect;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    invoke-direct {v1, v6, v0}, Lcom/google/android/apps/books/render/LabeledRect;-><init>(Landroid/graphics/Rect;Ljava/lang/String;)V

    .line 221
    .local v1, "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 225
    .end local v0    # "annotationId":Ljava/lang/String;
    .end local v1    # "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "rectEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/graphics/Rect;>;"
    :cond_0
    invoke-virtual {p0, p2, v5}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->makeCachedData(ILjava/util/List;)Ljava/lang/Object;

    move-result-object v2

    .line 226
    .local v2, "data":Ljava/lang/Object;, "TT;"
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v6, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 227
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mKnownCompleteCacheIndices:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 228
    if-eqz p3, :cond_1

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->finishProcessRequestResult()V

    .line 231
    :cond_1
    return-void
.end method


# virtual methods
.method protected alreadyLoadingOrLoaded(I)Z
    .locals 2
    .param p1, "cacheIndex"    # I

    .prologue
    .line 131
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mKnownCompleteCacheIndices:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearData()V
    .locals 1

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 95
    return-void
.end method

.method public clearData(I)V
    .locals 1
    .param p1, "cacheIndex"    # I

    .prologue
    .line 114
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 115
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->clearRangeDataRequestAndMarkIncomplete(I)V

    .line 116
    return-void
.end method

.method public clearRangeDataRequestAndMarkIncomplete(I)V
    .locals 2
    .param p1, "cacheIndex"    # I

    .prologue
    .line 123
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mKnownCompleteCacheIndices:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method

.method public clearRequests()V
    .locals 1

    .prologue
    .line 106
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 107
    return-void
.end method

.method public clearRequestsAndCacheIndices()V
    .locals 1

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mKnownCompleteCacheIndices:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 103
    return-void
.end method

.method protected finishProcessRequestResult()V
    .locals 0

    .prologue
    .line 237
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    return-void
.end method

.method public getCachedValue(I)Ljava/lang/Object;
    .locals 1
    .param p1, "cacheIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToData:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getPaintables(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method protected abstract makeCachedData(ILjava/util/List;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public maybeLoadDataForPage(Ljava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "pageIndex"    # I
    .param p4, "refreshPagesOnChange"    # Z

    .prologue
    .line 171
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->alreadyLoadingOrLoaded(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    :goto_0
    return-void

    .line 174
    :cond_0
    if-eqz p1, :cond_1

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mLoader:Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;->loadHighlightRectsForQuery(Ljava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)I

    move-result v1

    .line 176
    .local v1, "requestId":I
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 180
    .end local v1    # "requestId":I
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v2, p3, p4}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->updateDrawableAnnotationsCache(Lcom/google/common/collect/Multimap;IZ)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public maybeLoadDataForPassage(IZ)V
    .locals 8
    .param p1, "passageIndex"    # I
    .param p2, "refreshPagesOnChange"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->alreadyLoadingOrLoaded(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 160
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 148
    .local v2, "paintableRanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->getPaintables(I)Ljava/util/List;

    move-result-object v3

    .line 149
    .local v3, "paintables":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 150
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/PaintableTextRange;

    .line 151
    .local v1, "paintable":Lcom/google/android/apps/books/model/PaintableTextRange;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/PaintableTextRange;->getPaintableRangeId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/android/apps/books/model/PaintableTextRange;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 154
    .end local v1    # "paintable":Lcom/google/android/apps/books/model/PaintableTextRange;
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mLoader:Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;

    invoke-interface {v5, p1, v2}, Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;->loadRangeDataBulk(ILjava/util/Map;)I

    move-result v4

    .line 155
    .local v4, "requestId":I
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 158
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "requestId":I
    :cond_2
    const/4 v5, 0x0

    invoke-direct {p0, v5, p1, p2}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->updateDrawableAnnotationsCache(Lcom/google/common/collect/Multimap;IZ)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Exception;

    .prologue
    .line 241
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    .line 242
    return-void
.end method

.method public onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V
    .locals 5
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    .local p2, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mCacheIndexToLoadRequestId:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 192
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 193
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 194
    .local v3, "requestIdEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 195
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 196
    .local v0, "cacheIndex":I
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 198
    const/4 v4, 0x1

    :try_start_0
    invoke-direct {p0, p2, v0, v4}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->updateDrawableAnnotationsCache(Lcom/google/common/collect/Multimap;IZ)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->onLoadedRangeDataBulkSuccess()V

    .line 207
    .end local v0    # "cacheIndex":I
    .end local v3    # "requestIdEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_1
    return-void

    .line 199
    .restart local v0    # "cacheIndex":I
    .restart local v3    # "requestIdEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v4, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onLoadedRangeDataBulkSuccess()V
    .locals 0

    .prologue
    .line 210
    .local p0, "this":Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;, "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl<TT;>;"
    return-void
.end method
