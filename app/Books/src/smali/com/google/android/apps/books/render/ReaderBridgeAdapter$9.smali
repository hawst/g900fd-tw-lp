.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onPageData(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$bounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

.field final synthetic val$debugText:Ljava/lang/String;

.field final synthetic val$firstPositionOnNextPageInPassage:Ljava/lang/String;

.field final synthetic val$pageIndex:I

.field final synthetic val$pageText:Ljava/lang/String;

.field final synthetic val$passageIndex:I

.field final synthetic val$positions:Ljava/util/List;

.field final synthetic val$readingPosition:Ljava/lang/String;

.field final synthetic val$touchableItems:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$passageIndex:I

    iput p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$pageIndex:I

    iput-object p4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$readingPosition:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$positions:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$touchableItems:Ljava/util/List;

    iput-object p7, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$bounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    iput-object p8, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$firstPositionOnNextPageInPassage:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$pageText:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$debugText:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$passageIndex:I

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$pageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$readingPosition:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$positions:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$touchableItems:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$bounds:Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    iget-object v7, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$firstPositionOnNextPageInPassage:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/books/common/Position;->createPositionOrNull(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$pageText:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;->val$debugText:Ljava/lang/String;

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/books/render/ReaderListener;->onPageData(IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-void
.end method
