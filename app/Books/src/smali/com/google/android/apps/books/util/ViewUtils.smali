.class public Lcom/google/android/apps/books/util/ViewUtils;
.super Ljava/lang/Object;
.source "ViewUtils.java"


# static fields
.field private static final mDstPoint:[F

.field private static final mSrcPoint:[F

.field private static final mTempMatrix:Landroid/graphics/Matrix;

.field private static final sTmpLoc:[I

.field private static final sTypedValue:Landroid/util/TypedValue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 51
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/apps/books/util/ViewUtils;->sTmpLoc:[I

    .line 345
    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/apps/books/util/ViewUtils;->mSrcPoint:[F

    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    .line 346
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/ViewUtils;->mTempMatrix:Landroid/graphics/Matrix;

    .line 461
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/ViewUtils;->sTypedValue:Landroid/util/TypedValue;

    return-void
.end method

.method public static dropDownFromAnchor(Landroid/widget/PopupWindow;Landroid/view/View;Z)V
    .locals 4
    .param p0, "popup"    # Landroid/widget/PopupWindow;
    .param p1, "anchorView"    # Landroid/view/View;
    .param p2, "shouldBeFlush"    # Z

    .prologue
    .line 124
    if-eqz p2, :cond_1

    .line 125
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 126
    .local v1, "backgroundPadding":Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 127
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 130
    :cond_0
    iget v3, v1, Landroid/graphics/Rect;->top:I

    neg-int v2, v3

    .line 135
    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    .end local v1    # "backgroundPadding":Landroid/graphics/Rect;
    .local v2, "topOffset":I
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 136
    return-void

    .line 132
    .end local v2    # "topOffset":I
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "topOffset":I
    goto :goto_0
.end method

.method public static enableSoftInput(Landroid/view/View;Z)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "toEnable"    # Z

    .prologue
    const/4 v3, 0x2

    .line 441
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 443
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 444
    if-eqz p1, :cond_0

    .line 445
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInputFromWindow(Landroid/os/IBinder;II)V

    .line 449
    :cond_0
    return-void
.end method

.method public static finish2DPageTurn(Landroid/view/View;ZFFZLcom/google/android/apps/books/widget/PagesView$Callbacks;Ljava/lang/String;)V
    .locals 14
    .param p0, "view"    # Landroid/view/View;
    .param p1, "flinging"    # Z
    .param p2, "velocity"    # F
    .param p3, "newTranslation"    # F
    .param p4, "addListener"    # Z
    .param p5, "callbacks"    # Lcom/google/android/apps/books/widget/PagesView$Callbacks;
    .param p6, "tag"    # Ljava/lang/String;

    .prologue
    .line 279
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v10

    .line 280
    .local v10, "translationHelper":Lcom/google/android/ublib/view/TranslationHelper;
    invoke-virtual {v10, p0}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslatable(Landroid/view/View;)Lcom/google/android/ublib/view/TranslationHelper$Translatable;

    move-result-object v9

    .line 281
    .local v9, "translatable":Lcom/google/android/ublib/view/TranslationHelper$Translatable;
    invoke-virtual {v10, p0}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationX(Landroid/view/View;)F

    move-result v4

    .line 282
    .local v4, "currentTranslation":F
    const-string v11, "translationX"

    const/4 v12, 0x1

    new-array v12, v12, [F

    const/4 v13, 0x0

    aput p3, v12, v13

    invoke-static {v9, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 285
    .local v3, "animator":Landroid/animation/Animator;
    if-eqz p1, :cond_2

    .line 286
    new-instance v11, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v11}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v3, v11}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 287
    sub-float v8, p3, v4

    .line 289
    .local v8, "remainingDistance":F
    const/high16 v11, 0x45700000    # 3840.0f

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    mul-float v5, v11, v12

    .line 292
    .local v5, "minVelocity":F
    move/from16 v0, p2

    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 294
    .local v2, "adjustedVelocity":F
    const/high16 v11, 0x447a0000    # 1000.0f

    mul-float/2addr v11, v8

    div-float/2addr v11, v2

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-long v6, v11

    .line 296
    .local v6, "duration":J
    invoke-virtual {v3, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 297
    const/4 v11, 0x3

    move-object/from16 v0, p6

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 298
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Starting a linear page turn animation from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10, p0}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationX(Landroid/view/View;)F

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " in "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "ms"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-static {v0, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    .end local v2    # "adjustedVelocity":F
    .end local v5    # "minVelocity":F
    .end local v6    # "duration":J
    .end local v8    # "remainingDistance":F
    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    .line 311
    new-instance v11, Lcom/google/android/apps/books/util/ViewUtils$1;

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    invoke-direct {v11, p0, v0, v1}, Lcom/google/android/apps/books/util/ViewUtils$1;-><init>(Landroid/view/View;Ljava/lang/String;Lcom/google/android/apps/books/widget/PagesView$Callbacks;)V

    invoke-virtual {v3, v11}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 330
    :cond_1
    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    .line 331
    return-void

    .line 304
    :cond_2
    const/4 v11, 0x3

    move-object/from16 v0, p6

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 305
    const-string v11, "Starting a normal page turn animation"

    move-object/from16 v0, p6

    invoke-static {v0, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getActivityFromView(Landroid/view/View;)Landroid/app/Activity;
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 480
    move-object v0, p0

    .line 481
    .local v0, "current":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    .line 482
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Landroid/app/Activity;

    if-eqz v3, :cond_1

    .line 483
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 488
    :cond_0
    return-object v2

    .line 485
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 486
    .local v1, "parent":Landroid/view/ViewParent;
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_2

    check-cast v1, Landroid/view/View;

    .end local v1    # "parent":Landroid/view/ViewParent;
    move-object v0, v1

    .line 487
    :goto_1
    goto :goto_0

    .restart local v1    # "parent":Landroid/view/ViewParent;
    :cond_2
    move-object v0, v2

    .line 486
    goto :goto_1
.end method

.method public static getBounds(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "out"    # Landroid/graphics/Rect;

    .prologue
    const/4 v2, 0x0

    .line 432
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 433
    return-object p1
.end method

.method public static getChildView(Ljava/lang/Class;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "layoutId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "I)TT;"
        }
    .end annotation

    .prologue
    .line 338
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 339
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 340
    .local v0, "infl":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    invoke-virtual {v0, p3, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 342
    .end local v0    # "infl":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    return-object v1
.end method

.method public static getEffectiveScale(Landroid/view/View;)J
    .locals 6
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 171
    invoke-virtual {p0}, Landroid/view/View;->getScaleX()F

    move-result v2

    .local v2, "scaleX":F
    invoke-virtual {p0}, Landroid/view/View;->getScaleY()F

    move-result v3

    .line 172
    .local v3, "scaleY":F
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 174
    .local v0, "parent":Landroid/view/ViewParent;
    :goto_0
    instance-of v4, v0, Landroid/view/View;

    if-eqz v4, :cond_0

    move-object v1, v0

    .line 175
    check-cast v1, Landroid/view/View;

    .line 176
    .local v1, "parentView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    move-result v4

    mul-float/2addr v2, v4

    .line 177
    invoke-virtual {v1}, Landroid/view/View;->getScaleY()F

    move-result v4

    mul-float/2addr v3, v4

    .line 178
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 179
    goto :goto_0

    .line 181
    .end local v1    # "parentView":Landroid/view/View;
    :cond_0
    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/LongPointF;->with(FF)J

    move-result-wide v4

    return-wide v4
.end method

.method public static getLocationOnScreen(Landroid/view/View;)J
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/apps/books/util/ViewUtils;->sTmpLoc:[I

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 189
    sget-object v0, Lcom/google/android/apps/books/util/ViewUtils;->sTmpLoc:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    sget-object v1, Lcom/google/android/apps/books/util/ViewUtils;->sTmpLoc:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/LongPoint;->with(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getThemeColor(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrId"    # I

    .prologue
    .line 467
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/util/ViewUtils;->sTypedValue:Landroid/util/TypedValue;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    const/4 v0, 0x0

    .line 470
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/ViewUtils;->sTypedValue:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static immediateLayoutAtCurrentSize(Landroid/view/View;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 456
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 458
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 459
    return-void
.end method

.method public static inflateWithContext(ILandroid/content/Context;)Landroid/view/ViewGroup;
    .locals 3
    .param p0, "layoutId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 152
    .local v0, "fakeParent":Landroid/view/ViewGroup;
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 153
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    return-object v2
.end method

.method public static isParentPointInChildBounds(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "childPoint"    # Landroid/graphics/PointF;

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 361
    .local v0, "matrix":Landroid/graphics/Matrix;
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mSrcPoint:[F

    iget v4, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    aput v4, v3, v2

    .line 362
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mSrcPoint:[F

    iget v4, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    aput v4, v3, v1

    .line 364
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 365
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mTempMatrix:Landroid/graphics/Matrix;

    sget-object v4, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    sget-object v5, Lcom/google/android/apps/books/util/ViewUtils;->mSrcPoint:[F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 371
    :goto_0
    if-eqz p2, :cond_0

    .line 372
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    aget v3, v3, v2

    sget-object v4, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    aget v4, v4, v1

    invoke-virtual {p2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 375
    :cond_0
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    aget v3, v3, v2

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_2

    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    aget v3, v3, v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    aget v3, v3, v1

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_2

    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    aget v3, v3, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    :goto_1
    return v1

    .line 367
    :cond_1
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    sget-object v4, Lcom/google/android/apps/books/util/ViewUtils;->mSrcPoint:[F

    aget v4, v4, v2

    aput v4, v3, v2

    .line 368
    sget-object v3, Lcom/google/android/apps/books/util/ViewUtils;->mDstPoint:[F

    sget-object v4, Lcom/google/android/apps/books/util/ViewUtils;->mSrcPoint:[F

    aget v4, v4, v1

    aput v4, v3, v1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 375
    goto :goto_1
.end method

.method public static maybeInflateAdapterItemView(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 2
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "layoutId"    # I

    .prologue
    .line 385
    if-nez p1, :cond_0

    .line 386
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 388
    .end local p1    # "convertView":Landroid/view/View;
    :cond_0
    return-object p1
.end method

.method public static newCenterLayout()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newWrapContentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    .line 74
    .local v0, "result":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 75
    return-object v0
.end method

.method public static newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 57
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public static newWrapContentLayout()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 65
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public static placeOverRectangle(Landroid/view/View;Landroid/view/View;FFFFZ)V
    .locals 10
    .param p0, "view"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "screenX"    # F
    .param p3, "screenY"    # F
    .param p4, "width"    # F
    .param p5, "height"    # F
    .param p6, "excludePadding"    # Z

    .prologue
    .line 199
    if-nez p0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 203
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 204
    .local v5, "viewWidth":I
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 206
    .local v4, "viewHeight":I
    if-eqz p6, :cond_1

    .line 207
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    add-int/2addr v8, v9

    sub-int/2addr v5, v8

    .line 208
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v8

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    add-int/2addr v8, v9

    sub-int/2addr v4, v8

    .line 211
    :cond_1
    int-to-float v8, v5

    div-float v0, p4, v8

    .local v0, "scaleX":F
    int-to-float v8, v4

    div-float v1, p5, v8

    .line 213
    .local v1, "scaleY":F
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 214
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    .line 215
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotation(F)V

    .line 217
    invoke-static {p0}, Lcom/google/android/apps/books/util/ViewUtils;->getLocationOnScreen(Landroid/view/View;)J

    move-result-wide v6

    .line 219
    .local v6, "viewLocation":J
    invoke-virtual {p0}, Landroid/view/View;->getTranslationX()F

    move-result v8

    add-float/2addr v8, p2

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/LongPoint;->getX(J)I

    move-result v9

    int-to-float v9, v9

    sub-float v2, v8, v9

    .line 220
    .local v2, "translationX":F
    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v8

    add-float/2addr v8, p3

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/LongPoint;->getY(J)I

    move-result v9

    int-to-float v9, v9

    sub-float v3, v8, v9

    .line 222
    .local v3, "translationY":F
    if-eqz p6, :cond_2

    .line 223
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v0

    sub-float/2addr v2, v8

    .line 224
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v1

    sub-float/2addr v3, v8

    .line 227
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 228
    invoke-virtual {p1, v3}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method public static placeOverView(Landroid/view/View;Landroid/view/View;Landroid/view/View;Z)V
    .locals 15
    .param p0, "view"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "otherView"    # Landroid/view/View;
    .param p3, "excludePadding"    # Z

    .prologue
    .line 242
    if-nez p2, :cond_0

    .line 243
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 265
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/books/util/ViewUtils;->getLocationOnScreen(Landroid/view/View;)J

    move-result-wide v8

    .line 249
    .local v8, "otherLocation":J
    invoke-static {v8, v9}, Lcom/google/android/apps/books/util/LongPoint;->getX(J)I

    move-result v0

    int-to-float v2, v0

    .local v2, "screenX":F
    invoke-static {v8, v9}, Lcom/google/android/apps/books/util/LongPoint;->getY(J)I

    move-result v0

    int-to-float v3, v0

    .line 250
    .local v3, "screenY":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getWidth()I

    move-result v14

    .local v14, "width":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 252
    .local v7, "height":I
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/books/util/ViewUtils;->getEffectiveScale(Landroid/view/View;)J

    move-result-wide v10

    .line 253
    .local v10, "otherScale":J
    invoke-static {v10, v11}, Lcom/google/android/apps/books/util/LongPointF;->getX(J)F

    move-result v12

    .line 254
    .local v12, "otherScaleX":F
    invoke-static {v10, v11}, Lcom/google/android/apps/books/util/LongPointF;->getY(J)F

    move-result v13

    .line 256
    .local v13, "otherScaleY":F
    if-eqz p3, :cond_1

    .line 257
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v12

    add-float/2addr v2, v0

    .line 258
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v13

    add-float/2addr v3, v0

    .line 259
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v14, v0

    .line 260
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v7, v0

    .line 263
    :cond_1
    int-to-float v0, v14

    mul-float v4, v0, v12

    int-to-float v0, v7

    mul-float v5, v0, v13

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/books/util/ViewUtils;->placeOverRectangle(Landroid/view/View;Landroid/view/View;FFFFZ)V

    goto :goto_0
.end method

.method public static setVisibility(Landroid/view/View;I)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visibility"    # I

    .prologue
    .line 142
    if-eqz p0, :cond_0

    .line 143
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    :cond_0
    return-void
.end method

.method public static varargs useLayerWhenAnimating(Landroid/animation/Animator;[Landroid/view/View;)V
    .locals 1
    .param p0, "animator"    # Landroid/animation/Animator;
    .param p1, "views"    # [Landroid/view/View;

    .prologue
    .line 398
    new-instance v0, Lcom/google/android/apps/books/util/ViewUtils$2;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/util/ViewUtils$2;-><init>([Landroid/view/View;)V

    invoke-virtual {p0, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 414
    return-void
.end method
