.class public Lcom/google/android/apps/books/annotations/LayersTable;
.super Ljava/lang/Object;
.source "LayersTable.java"


# direct methods
.method public static addCharacterQuotaFields(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ALTER TABLE layers ADD COLUMN limit_type TEXT DEFAULT \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$LimitType;->LIMITED:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Layer$LimitType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 64
    const-string v0, "ALTER TABLE layers ADD COLUMN remaining_allowed_characters INTEGER DEFAULT 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    const-string v0, "ALTER TABLE layers ADD COLUMN max_allowed_characters INTEGER DEFAULT 0"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public static getCreationSql()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE TABLE layers (volume_id TEXT, layer_id TEXT, type TEXT, content_version TEXT, layer_version TEXT, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "limit_type TEXT DEFAULT \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$LimitType;->LIMITED:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "remaining_allowed_characters INTEGER DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "max_allowed_characters INTEGER DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " UNIQUE (volume_id, layer_id, type)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLayer(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 13
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;

    .prologue
    const/4 v11, 0x0

    const/4 v12, 0x1

    .line 92
    const/4 v8, 0x0

    .line 94
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x5

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "content_version"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "layer_version"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "limit_type"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "remaining_allowed_characters"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "max_allowed_characters"

    aput-object v1, v2, v0

    .line 96
    .local v2, "columns":[Ljava/lang/String;
    const-string v10, "volume_id=? AND type=? AND layer_id=?"

    .line 97
    .local v10, "where":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Layer$Type;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 98
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string v1, "layers"

    const-string v3, "volume_id=? AND type=? AND layer_id=?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 99
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v12, :cond_0

    const-string v0, "LayersTable"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "LayersTable"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected count "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for layer "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    new-instance v9, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/Layer$LimitType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer$LimitType;

    move-result-object v0

    const/4 v1, 0x3

    const v3, 0x7fffffff

    invoke-static {v8, v1, v3}, Lcom/google/android/apps/books/util/CursorUtils;->getIntOrFallbackIfNull(Landroid/database/Cursor;II)I

    move-result v1

    const/4 v3, 0x4

    const v5, 0x7fffffff

    invoke-static {v8, v3, v5}, Lcom/google/android/apps/books/util/CursorUtils;->getIntOrFallbackIfNull(Landroid/database/Cursor;II)I

    move-result v3

    invoke-direct {v9, v0, v1, v3}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;-><init>(Lcom/google/android/apps/books/annotations/Layer$LimitType;II)V

    .line 107
    .local v9, "quota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer;

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p1, v1, v3, v9}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    if-eqz v8, :cond_1

    .line 111
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 114
    .end local v9    # "quota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    :cond_1
    :goto_0
    return-object v0

    .line 110
    :cond_2
    if-eqz v8, :cond_3

    .line 111
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v11

    .line 114
    goto :goto_0

    .line 110
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    .end local v10    # "where":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 111
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static removeLayersForVolume(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 118
    const-string v0, "volume_id=?"

    .line 119
    .local v0, "where":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 120
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string v2, "layers"

    const-string v3, "volume_id=?"

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 121
    return-void
.end method

.method public static updateLayer(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/books/annotations/Layer;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;

    .prologue
    .line 75
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 76
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "volume_id"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v3, v3, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v2, "layer_id"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v3, v3, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v2, "type"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v3, v3, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-virtual {v3}, Lcom/google/android/apps/books/annotations/Layer$Type;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v2, "content_version"

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v2, "layer_version"

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getCharacterLimitType()Lcom/google/android/apps/books/annotations/Layer$LimitType;

    move-result-object v0

    .line 82
    .local v0, "limitType":Lcom/google/android/apps/books/annotations/Layer$LimitType;
    const-string v2, "limit_type"

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer$LimitType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v2, Lcom/google/android/apps/books/annotations/Layer$LimitType;->LIMITED:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    if-ne v0, v2, :cond_0

    .line 85
    const-string v2, "remaining_allowed_characters"

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getRemainingCharacterCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 86
    const-string v2, "max_allowed_characters"

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Layer;->getAllowedCharacterCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 88
    :cond_0
    const-string v2, "layers"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {p0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 89
    return-void
.end method
