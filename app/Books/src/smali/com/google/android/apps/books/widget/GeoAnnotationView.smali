.class public Lcom/google/android/apps/books/widget/GeoAnnotationView;
.super Landroid/widget/RelativeLayout;
.source "GeoAnnotationView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParserError"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;
    }
.end annotation


# instance fields
.field private mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

.field private mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

.field private mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;"
        }
    .end annotation
.end field

.field private final mAnnotationDataConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mBitmapConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private mLocaleString:Ljava/lang/String;

.field private mMapPreviewImage:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mMapSize:Landroid/graphics/Point;

.field private mMapTrigger:Landroid/view/View;

.field private mMapView:Landroid/widget/ImageView;

.field private mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

.field private mSearchOnGoogleView:Landroid/widget/TextView;

.field private mSnippetView:Landroid/widget/TextView;

.field private mStartedDataFetch:Z

.field private mTitleView:Landroid/widget/TextView;

.field private mWikiIntent:Landroid/content/Intent;

.field private mWikiLinkView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 335
    new-instance v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mBitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 350
    new-instance v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationDataConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->init()V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 335
    new-instance v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mBitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 350
    new-instance v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationDataConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->init()V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 335
    new-instance v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$4;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mBitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 350
    new-instance v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$5;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationDataConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 90
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->init()V

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->createMapIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->createGoogleSearchIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/widget/GeoAnnotationView;Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapPreviewImage:Lcom/google/android/apps/books/util/ExceptionOr;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->maybeFinishedLoading()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/widget/GeoAnnotationView;Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getCachePolicy()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mBitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/GeoAnnotationView;)Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    return-object v0
.end method

.method private createGoogleSearchIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 289
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 290
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    return-object v0
.end method

.method private createMapIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 302
    iget-object v4, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 303
    iget-object v4, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/AnnotationData;

    iget-object v4, v4, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    invoke-interface {v4}, Lcom/google/android/apps/books/annotations/AnnotationDataPayload;->getGeo()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;

    move-result-object v0

    .line 304
    .local v0, "geoData":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    if-eqz v0, :cond_2

    .line 307
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    .line 309
    .local v2, "paramsBuilder":Landroid/net/Uri$Builder;
    iget v4, v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->zoom:I

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    iget v3, v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->zoom:I

    .line 310
    .local v3, "zoom":I
    :goto_0
    const-string v4, "z"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 311
    const-string v4, "q"

    iget-object v5, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v5}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 313
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "geo:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->latitude:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->longitude:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 316
    .local v1, "geoUri":Landroid/net/Uri;
    const-string v4, "GeoAnnotationView"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 317
    const-string v4, "GeoAnnotationView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "map intent uri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 322
    .end local v0    # "geoData":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .end local v1    # "geoUri":Landroid/net/Uri;
    .end local v2    # "paramsBuilder":Landroid/net/Uri$Builder;
    .end local v3    # "zoom":I
    :goto_1
    return-object v4

    .line 309
    .restart local v0    # "geoData":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .restart local v2    # "paramsBuilder":Landroid/net/Uri$Builder;
    :cond_1
    const/4 v3, 0x2

    goto :goto_0

    .line 322
    .end local v0    # "geoData":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .end local v2    # "paramsBuilder":Landroid/net/Uri$Builder;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private createWikiIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "wikilink"    # Ljava/lang/String;

    .prologue
    .line 295
    if-nez p1, :cond_0

    .line 296
    const/4 v0, 0x0

    .line 298
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private getCachePolicy()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .locals 2

    .prologue
    .line 326
    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/AnnotationData;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    invoke-interface {v1}, Lcom/google/android/apps/books/annotations/AnnotationDataPayload;->getGeo()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;

    move-result-object v0

    .line 328
    .local v0, "geo":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .line 332
    .end local v0    # "geo":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNKNOWN:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    goto :goto_0
.end method

.method private getEntityKey()Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getDataId()Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "entityId":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 120
    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mLocaleString:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    return-object v1

    .end local v0    # "entityId":Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .line 118
    goto :goto_0
.end method

.method private getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/AnnotationData;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    invoke-interface {v1}, Lcom/google/android/apps/books/annotations/AnnotationDataPayload;->getCommon()Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    move-result-object v0

    .line 247
    .local v0, "common":Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->title:Ljava/lang/String;

    .line 250
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 94
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mLocaleString:Ljava/lang/String;

    .line 95
    return-void
.end method

.method private maybeFetchAnnotationData()V
    .locals 5

    .prologue
    .line 107
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mStartedDataFetch:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    if-eqz v1, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getEntityKey()Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    move-result-object v0

    .line 109
    .local v0, "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    if-eqz v0, :cond_0

    .line 110
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mStartedDataFetch:Z

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget-object v4, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationDataConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v4}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->fetchAnnotationData(Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V

    .line 115
    .end local v0    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :cond_0
    return-void
.end method

.method private maybeFinishedLoading()V
    .locals 5

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getLoadResult()Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .line 218
    .local v2, "loadResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 219
    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;->onFinishedLoading(Ljava/lang/Exception;)V

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    iget-object v4, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapPreviewImage:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 225
    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/AnnotationData;

    .line 226
    .local v1, "entity":Lcom/google/android/apps/books/annotations/AnnotationData;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mTitleView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v3, v1, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    invoke-interface {v3}, Lcom/google/android/apps/books/annotations/AnnotationDataPayload;->getCommon()Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    move-result-object v0

    .line 229
    .local v0, "commonData":Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    if-eqz v0, :cond_3

    .line 230
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->snippet:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->snippetUrl:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->setWikiInformation(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;->onFinishedLoading(Ljava/lang/Exception;)V

    goto :goto_0

    .line 233
    :cond_3
    const-string v3, "GeoAnnotationView"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 234
    const-string v3, "GeoAnnotationView"

    const-string v4, "Geo annotation data doesn\'t have common data"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public getLoadResult()Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 258
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapPreviewImage:Lcom/google/android/apps/books/util/ExceptionOr;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->firstFailure([Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 260
    .local v0, "firstFailure":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    if-eqz v0, :cond_0

    .line 266
    .end local v0    # "firstFailure":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    :goto_0
    return-object v0

    .line 263
    .restart local v0    # "firstFailure":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapPreviewImage:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationData:Lcom/google/android/apps/books/util/ExceptionOr;

    goto :goto_0

    .line 266
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMapSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapSize:Landroid/graphics/Point;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 13

    .prologue
    .line 130
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 132
    const v11, 0x7f0e0124

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mTitleView:Landroid/widget/TextView;

    .line 133
    const v11, 0x7f0e0129

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapView:Landroid/widget/ImageView;

    .line 134
    const v11, 0x7f0e012a

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapTrigger:Landroid/view/View;

    .line 135
    iget-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapTrigger:Landroid/view/View;

    new-instance v12, Lcom/google/android/apps/books/widget/GeoAnnotationView$1;

    invoke-direct {v12, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$1;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    invoke-virtual {v11, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const v11, 0x7f0e0126

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiLinkView:Landroid/view/View;

    .line 152
    iget-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiLinkView:Landroid/view/View;

    new-instance v12, Lcom/google/android/apps/books/widget/GeoAnnotationView$2;

    invoke-direct {v12, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$2;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    invoke-virtual {v11, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    const v11, 0x7f0e0127

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSearchOnGoogleView:Landroid/widget/TextView;

    .line 163
    iget-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSearchOnGoogleView:Landroid/widget/TextView;

    new-instance v12, Lcom/google/android/apps/books/widget/GeoAnnotationView$3;

    invoke-direct {v12, p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView$3;-><init>(Lcom/google/android/apps/books/widget/GeoAnnotationView;)V

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    const v11, 0x7f0e0125

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSnippetView:Landroid/widget/TextView;

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 185
    .local v10, "res":Landroid/content/res/Resources;
    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 186
    .local v1, "display":Landroid/util/DisplayMetrics;
    iget v11, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v12, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 192
    .local v8, "maxDimension":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getPaddingLeft()I

    move-result v3

    .line 193
    .local v3, "horizontalPadding":I
    const v11, 0x7f090128

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 194
    .local v4, "layoutHorizontalMargin":I
    add-int v2, v3, v4

    .line 201
    .local v2, "horizontalMargin":I
    const v11, 0x7f090125

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 202
    .local v7, "maxCardWidth":I
    mul-int/lit8 v11, v2, 0x2

    sub-int v11, v8, v11

    mul-int/lit8 v12, v3, 0x2

    sub-int v12, v7, v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 204
    .local v0, "cardWidth":I
    div-int/lit8 v6, v0, 0x2

    .line 205
    .local v6, "mapWidth":I
    const v11, 0x7f090126

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 206
    .local v9, "minMapHeight":I
    int-to-float v11, v6

    const/high16 v12, 0x3fc00000    # 1.5f

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-static {v11, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 207
    .local v5, "mapHeight":I
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11, v6, v5}, Landroid/graphics/Point;-><init>(II)V

    iput-object v11, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mMapSize:Landroid/graphics/Point;

    .line 208
    return-void
.end method

.method protected setWikiInformation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "snippet"    # Ljava/lang/String;
    .param p2, "snippetUrl"    # Ljava/lang/String;

    .prologue
    .line 270
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->createWikiIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiIntent:Landroid/content/Intent;

    .line 271
    if-eqz p1, :cond_2

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSnippetView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSnippetView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mWikiLinkView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 286
    :cond_1
    return-void

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSnippetView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mSnippetView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHeight(I)V

    goto :goto_0
.end method

.method public setup(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/VolumeAnnotationController;Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;)V
    .locals 1
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "controller"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    .param p3, "listener"    # Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    .prologue
    .line 99
    const-string v0, "null annotation"

    invoke-static {p1, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 100
    const-string v0, "null owner"

    invoke-static {p3, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mOwner:Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;

    .line 101
    const-string v0, "null controller"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/GeoAnnotationView;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->maybeFetchAnnotationData()V

    .line 104
    return-void
.end method
