.class Lcom/google/android/apps/books/navigation/PageRangeLoader$4;
.super Ljava/lang/Object;
.source "PageRangeLoader.java"

# interfaces
.implements Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/PageRangeLoader;->importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$4;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .prologue
    .line 830
    const/4 v0, 0x1

    return v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$4;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # getter for: Lcom/google/android/apps/books/navigation/PageRangeLoader;->mImportedPages:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$2000(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 836
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader$4;->this$0:Lcom/google/android/apps/books/navigation/PageRangeLoader;

    # invokes: Lcom/google/android/apps/books/navigation/PageRangeLoader;->doRequestPages()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->access$1900(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    .line 837
    return-void
.end method
