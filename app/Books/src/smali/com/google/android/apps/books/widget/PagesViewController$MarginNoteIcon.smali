.class public Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MarginNoteIcon"
.end annotation


# instance fields
.field public final anchorRect:Landroid/graphics/Rect;

.field private final mMarginNoteIconTapSize:I

.field private mPaintRect:Landroid/graphics/Rect;

.field private mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

.field private mTapAreaRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;I)V
    .locals 1
    .param p1, "anchorRect"    # Landroid/graphics/Rect;
    .param p2, "marginNoteIconTapSize"    # I

    .prologue
    .line 632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    .line 635
    iput p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mMarginNoteIconTapSize:I

    .line 636
    return-void
.end method

.method private computePaintRect(Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;ILandroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer;)V
    .locals 5
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "edge"    # I
    .param p4, "minIconSize"    # Landroid/graphics/Point;
    .param p5, "renderer"    # Lcom/google/android/apps/books/render/Renderer;

    .prologue
    .line 720
    invoke-interface {p5, p2}, Lcom/google/android/apps/books/render/Renderer;->pageBoundsInRendererCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;)Landroid/graphics/Rect;

    move-result-object v1

    .line 724
    .local v1, "pageBounds":Landroid/graphics/Rect;
    invoke-interface {p5, p2}, Lcom/google/android/apps/books/render/Renderer;->getDecorationInsetFromPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering;)I

    move-result v0

    .line 726
    .local v0, "margin":I
    packed-switch p3, :pswitch_data_0

    .line 744
    const-string v2, "PagesViewHelper"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 745
    const-string v2, "PagesViewHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown edge: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 728
    :pswitch_0
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 729
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p4, Landroid/graphics/Point;->y:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 732
    :pswitch_1
    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 733
    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p4, Landroid/graphics/Point;->x:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 736
    :pswitch_2
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 737
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iget v3, p4, Landroid/graphics/Point;->y:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 740
    :pswitch_3
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 741
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p4, Landroid/graphics/Point;->x:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 726
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private computeTapAreaRect()Landroid/graphics/Rect;
    .locals 8

    .prologue
    .line 751
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    if-nez v4, :cond_0

    .line 752
    const/4 v3, 0x0

    .line 760
    :goto_0
    return-object v3

    .line 753
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    .line 754
    .local v0, "centerX":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    .line 756
    .local v1, "centerY":I
    iget v4, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mMarginNoteIconTapSize:I

    div-int/lit8 v2, v4, 0x2

    .line 757
    .local v2, "halfTapSize":I
    new-instance v3, Landroid/graphics/Rect;

    sub-int v4, v0, v2

    sub-int v5, v1, v2

    add-int v6, v0, v2

    add-int v7, v1, v2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 760
    .local v3, "tapArea":Landroid/graphics/Rect;
    goto :goto_0
.end method

.method private shouldReplaceAnchorRect(Landroid/graphics/Rect;Z)Z
    .locals 4
    .param p1, "newAnchorRect"    # Landroid/graphics/Rect;
    .param p2, "isVertical"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 692
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 700
    :cond_0
    :goto_0
    return v0

    .line 695
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 697
    goto :goto_0

    .line 700
    :cond_2
    if-eqz p2, :cond_3

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    if-gt v2, v3, :cond_0

    :cond_3
    if-nez p2, :cond_4

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-lt v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method getPaintRect(Lcom/google/android/apps/books/widget/DevicePageRendering;ZLcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "isVertical"    # Z
    .param p3, "decorationPosition"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "minIconSize"    # Landroid/graphics/Point;
    .param p5, "renderer"    # Lcom/google/android/apps/books/render/Renderer;

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-eq v0, p3, :cond_1

    .line 662
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    .line 663
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 667
    if-eqz p2, :cond_2

    .line 668
    const/4 v3, 0x2

    .line 675
    .local v3, "edge":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    move-object v0, p0

    move-object v2, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->computePaintRect(Landroid/graphics/Rect;Lcom/google/android/apps/books/widget/DevicePageRendering;ILandroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer;)V

    .line 677
    .end local v3    # "edge":I
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mPaintRect:Landroid/graphics/Rect;

    return-object v0

    .line 669
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne p3, v0, :cond_3

    .line 670
    const/4 v3, 0x1

    .restart local v3    # "edge":I
    goto :goto_0

    .line 672
    .end local v3    # "edge":I
    :cond_3
    const/4 v3, 0x3

    .restart local v3    # "edge":I
    goto :goto_0
.end method

.method getTapAreaRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mTapAreaRect:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 654
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->computeTapAreaRect()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mTapAreaRect:Landroid/graphics/Rect;

    .line 656
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mTapAreaRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public maybeUpdateAnchorRect(Landroid/graphics/Rect;Z)V
    .locals 1
    .param p1, "newAnchorRect"    # Landroid/graphics/Rect;
    .param p2, "isVertical"    # Z

    .prologue
    .line 639
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->shouldReplaceAnchorRect(Landroid/graphics/Rect;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 643
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->mTapAreaRect:Landroid/graphics/Rect;

    .line 645
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 765
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MarginNoteIcon [anchorRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->anchorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTapAreaRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->getTapAreaRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
