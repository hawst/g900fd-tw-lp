.class public final enum Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
.super Ljava/lang/Enum;
.source "VolumeManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/VolumeManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

.field public static final enum EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

.field public static final enum IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    const-string v1, "EPUB"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 29
    new-instance v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->$VALUES:[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 1
    .param p0, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 32
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->$VALUES:[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    return-object v0
.end method
