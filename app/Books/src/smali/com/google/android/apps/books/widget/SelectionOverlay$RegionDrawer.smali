.class Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;
.super Ljava/lang/Object;
.source "SelectionOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SelectionOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RegionDrawer"
.end annotation


# instance fields
.field private final mPreallocatedRect:Landroid/graphics/Rect;

.field private final mPreallocatedRegion:Landroid/graphics/Region;

.field private mRegionIterator:Landroid/graphics/RegionIterator;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRegion:Landroid/graphics/Region;

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRect:Landroid/graphics/Rect;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/SelectionOverlay$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/SelectionOverlay$1;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;-><init>()V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 45
    new-instance v0, Landroid/graphics/RegionIterator;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRegion:Landroid/graphics/Region;

    invoke-direct {v0, v1}, Landroid/graphics/RegionIterator;-><init>(Landroid/graphics/Region;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mRegionIterator:Landroid/graphics/RegionIterator;

    .line 46
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mRegionIterator:Landroid/graphics/RegionIterator;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RegionIterator;->next(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method

.method public getBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRegion:Landroid/graphics/Region;

    invoke-virtual {v0}, Landroid/graphics/Region;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public setRects(Lcom/google/android/apps/books/widget/Walker;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "rects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRegion:Landroid/graphics/Region;

    invoke-virtual {v0}, Landroid/graphics/Region;->setEmpty()V

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRect:Landroid/graphics/Rect;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRegion:Landroid/graphics/Region;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->mPreallocatedRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method
