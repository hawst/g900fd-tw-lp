.class public Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;
.super Ljava/lang/Object;
.source "HighlightsByColorWalkerFromList.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnnotations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private mColorIndex:I

.field private final mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field private mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

.field private mSelectedAnnotationColor:I

.field private mSelectedAnnotationId:Ljava/lang/String;

.field private final mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

.field private final mUnselectedAnnotationColors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    .line 66
    new-instance v0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;-><init>(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;

    .line 149
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {v0}, Lcom/google/android/apps/books/render/PageStructureSelection;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mAnnotations:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Lcom/google/android/apps/books/render/PageStructureSelection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    .prologue
    .line 17
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationColor:I

    return v0
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Z
    .locals 4
    .param p1, "hsc"    # Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 179
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    .line 180
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    iget v3, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->set(IZLcom/google/android/apps/books/widget/Walker;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    move v0, v1

    .line 190
    :goto_0
    return v0

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 187
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationColor:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->set(IZLcom/google/android/apps/books/widget/Walker;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    move v0, v1

    .line 188
    goto :goto_0

    :cond_1
    move v0, v2

    .line 190
    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->next(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mLabeledRectWalker:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 175
    return-void
.end method

.method public setup(Ljava/util/List;Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Ljava/lang/String;)V
    .locals 4
    .param p2, "page"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p3, "selectedAnnotationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mAnnotations:Ljava/util/List;

    .line 154
    iput-object p2, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 155
    iput-object p3, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationId:Ljava/lang/String;

    .line 157
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 158
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationColor:I

    goto :goto_0

    .line 162
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 163
    iget-object v2, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->reset()V

    .line 169
    return-void
.end method
