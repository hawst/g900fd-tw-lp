.class Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;
.super Landroid/os/AsyncTask;
.source "DictionaryCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LookupTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/annotations/DictionaryEntry;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final mWord:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p2, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 121
    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->mWord:Ljava/lang/String;

    .line 122
    iput-object p3, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 123
    return-void
.end method

.method private addAttributionView(Landroid/view/ViewGroup;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V
    .locals 4
    .param p1, "wordContentView"    # Landroid/view/ViewGroup;
    .param p2, "attribution"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .prologue
    const/4 v3, 0x0

    .line 271
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 274
    .local v0, "lastSenseView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 275
    const v2, 0x7f0e0209

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 277
    const v2, 0x7f0e020a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 279
    .local v1, "linkTextView":Landroid/widget/Button;
    iget-object v2, p2, Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 280
    new-instance v2, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask$1;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask$1;-><init>(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    return-void
.end method

.method private addSenseView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "sense"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    .param p4, "fallbackSyllabification"    # Ljava/lang/String;
    .param p5, "fallbackPronunciation"    # Ljava/lang/String;
    .param p6, "isFirst"    # Z

    .prologue
    .line 298
    const v0, 0x7f0400d9

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 300
    .local v1, "senseView":Landroid/view/View;
    const v2, 0x7f0e0205

    iget-object v3, p3, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->syllabification:Ljava/lang/String;

    move-object v0, p0

    move-object/from16 v4, p4

    move/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->configureOptionalTextView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 302
    const v2, 0x7f0e0206

    iget-object v3, p3, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->pronunciation:Ljava/lang/String;

    move-object v0, p0

    move-object/from16 v4, p5

    move/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->configureOptionalTextView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 305
    const v0, 0x7f0e0207

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 307
    .local v10, "partOfSpeechView":Landroid/widget/TextView;
    iget-object v0, p3, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->partOfSpeech:Ljava/lang/String;

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    if-nez p6, :cond_0

    .line 310
    invoke-virtual {v10}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {v10}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v10}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v10, v0, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 314
    :cond_0
    const v0, 0x7f0e0208

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 317
    .local v8, "definitionsContainer":Landroid/view/ViewGroup;
    iget-object v0, p3, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->definitions:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 318
    const/4 v7, 0x1

    .line 319
    .local v7, "definitionIndex":I
    iget-object v0, p3, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->definitions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;

    .line 320
    .local v6, "definition":Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$100(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04004a

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 323
    .local v11, "textView":Landroid/widget/TextView;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v6, Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;->text:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    invoke-virtual {v8, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 325
    add-int/lit8 v7, v7, 0x1

    .line 326
    goto :goto_0

    .line 329
    .end local v6    # "definition":Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;
    .end local v7    # "definitionIndex":I
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "textView":Landroid/widget/TextView;
    :cond_1
    if-nez p6, :cond_2

    .line 331
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 334
    :cond_2
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 335
    return-void
.end method

.method private configureOptionalTextView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "childId"    # I
    .param p3, "itemValue"    # Ljava/lang/String;
    .param p4, "fallbackValue"    # Ljava/lang/String;
    .param p5, "isFirst"    # Z

    .prologue
    .line 350
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 352
    .local v1, "view":Landroid/widget/TextView;
    if-nez p5, :cond_0

    if-eqz p3, :cond_2

    invoke-static {p3, p4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 354
    :cond_0
    invoke-static {p3}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 355
    move-object v0, p3

    .line 362
    .local v0, "value":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_3

    .line 363
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    :goto_1
    return-void

    .line 357
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    move-object v0, p4

    .restart local v0    # "value":Ljava/lang/String;
    goto :goto_0

    .line 360
    .end local v0    # "value":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "value":Ljava/lang/String;
    goto :goto_0

    .line 365
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private getAttribution(Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    .locals 3
    .param p1, "word"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    .param p2, "fallback"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .prologue
    .line 259
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 260
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    .line 261
    .local v1, "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    if-eqz v2, :cond_0

    .line 262
    iget-object p2, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .line 266
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    .end local p2    # "fallback":Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    :cond_1
    return-object p2
.end method

.method private getPronunciation(Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "word"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    .param p2, "fallback"    # Ljava/lang/String;

    .prologue
    .line 245
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 246
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    .line 247
    .local v1, "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->pronunciation:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 248
    iget-object p2, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->pronunciation:Ljava/lang/String;

    .line 252
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    .end local p2    # "fallback":Ljava/lang/String;
    :cond_1
    return-object p2
.end method

.method private getTitle(Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "word"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    .param p2, "fallback"    # Ljava/lang/String;

    .prologue
    .line 231
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 232
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    .line 233
    .local v1, "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->syllabification:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 234
    iget-object p2, v1, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;->syllabification:Ljava/lang/String;

    .line 238
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    .end local p2    # "fallback":Ljava/lang/String;
    :cond_1
    return-object p2
.end method

.method private publishException(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 371
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 129
    :try_start_0
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBookLocale:Ljava/util/Locale;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$000(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBookLocale:Ljava/util/Locale;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$000(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "bookLocale":Ljava/lang/String;
    :goto_0
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$100(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 131
    new-instance v5, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->mWord:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mUserLocale:Ljava/util/Locale;
    invoke-static {v10}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$200(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v9, v10, v0}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 134
    .local v5, "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :try_start_1
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mAnnotationServer:Lcom/google/android/apps/books/annotations/AnnotationServer;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$400(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Lcom/google/android/apps/books/annotations/AnnotationServer;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;
    invoke-static {v10}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$300(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v10

    invoke-interface {v9, v10, v5}, Lcom/google/android/apps/books/annotations/AnnotationServer;->getAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;)Lcom/google/android/apps/books/annotations/AnnotationData;

    move-result-object v3

    .line 137
    .local v3, "entity":Lcom/google/android/apps/books/annotations/AnnotationData;
    if-eqz v3, :cond_1

    iget-object v9, v3, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    if-eqz v9, :cond_1

    .line 138
    iget-object v9, v3, Lcom/google/android/apps/books/annotations/AnnotationData;->payload:Lcom/google/android/apps/books/annotations/AnnotationDataPayload;

    invoke-interface {v9}, Lcom/google/android/apps/books/annotations/AnnotationDataPayload;->getDictionary()Lcom/google/android/apps/books/annotations/DictionaryEntry;

    move-result-object v8

    .line 139
    .local v8, "word":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    invoke-static {v8}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v7

    .line 140
    .local v7, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v9}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V

    .line 181
    .end local v0    # "bookLocale":Ljava/lang/String;
    .end local v3    # "entity":Lcom/google/android/apps/books/annotations/AnnotationData;
    .end local v5    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .end local v8    # "word":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    :goto_1
    return-object v7

    .end local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    :cond_0
    move-object v0, v6

    .line 129
    goto :goto_0

    .line 144
    .restart local v0    # "bookLocale":Ljava/lang/String;
    .restart local v3    # "entity":Lcom/google/android/apps/books/annotations/AnnotationData;
    .restart local v5    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :cond_1
    new-instance v9, Ljava/lang/Exception;

    const-string v10, "Missing dictionary data in response"

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v7

    .line 146
    .restart local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v9}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 150
    .end local v3    # "entity":Lcom/google/android/apps/books/annotations/AnnotationData;
    .end local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    :catch_0
    move-exception v2

    .line 152
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v9}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V

    .line 155
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 160
    .end local v0    # "bookLocale":Ljava/lang/String;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    :catch_1
    move-exception v2

    .line 162
    .restart local v2    # "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mOfflineDictionaryEnabled:Z
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$500(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 163
    new-instance v9, Ljava/lang/Exception;

    const-string v10, "online lookup failed"

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v7

    .restart local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    goto :goto_1

    .line 158
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    .restart local v0    # "bookLocale":Ljava/lang/String;
    :cond_2
    :try_start_3
    new-instance v9, Ljava/net/ConnectException;

    invoke-direct {v9}, Ljava/net/ConnectException;-><init>()V

    throw v9
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 167
    .end local v0    # "bookLocale":Ljava/lang/String;
    .restart local v2    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_4
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v1

    .line 169
    .local v1, "dictEntryConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBookLocale:Ljava/util/Locale;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$000(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBookLocale:Ljava/util/Locale;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$000(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    .line 171
    .local v6, "language":Ljava/lang/String;
    :cond_4
    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$600(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->mWord:Ljava/lang/String;

    invoke-interface {v9, v10, v6, v1}, Lcom/google/android/apps/books/data/BooksDataController;->lookup(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 172
    invoke-virtual {v1}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/util/ExceptionOr;

    .restart local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    goto :goto_1

    .line 169
    .end local v6    # "language":Ljava/lang/String;
    .end local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    :cond_5
    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mUserLocale:Ljava/util/Locale;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$200(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mUserLocale:Ljava/util/Locale;
    invoke-static {v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$200(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v6

    goto :goto_2

    .line 173
    .end local v1    # "dictEntryConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    :catch_2
    move-exception v4

    .line 174
    .local v4, "ex":Ljava/lang/InterruptedException;
    const-string v9, "DictCard"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 175
    const-string v9, "DictCard"

    const-string v10, "Offline lookup failed"

    invoke-static {v9, v10, v4}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    :cond_6
    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v7

    .restart local v7    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 115
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/annotations/DictionaryEntry;

    .line 188
    .local v8, "entry":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 189
    .local v10, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz v8, :cond_3

    iget-object v0, v8, Lcom/google/android/apps/books/annotations/DictionaryEntry;->words:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 191
    iget-object v13, v8, Lcom/google/android/apps/books/annotations/DictionaryEntry;->words:Ljava/util/List;

    .line 192
    .local v13, "words":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;>;"
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 193
    const/4 v0, 0x0

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;

    .line 196
    .local v11, "word":Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    invoke-virtual {v8}, Lcom/google/android/apps/books/annotations/DictionaryEntry;->getFallBackTitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->getTitle(Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "title":Ljava/lang/String;
    const/4 v0, 0x0

    invoke-direct {p0, v11, v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->getPronunciation(Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 198
    .local v5, "pronunciation":Ljava/lang/String;
    iget-object v0, v11, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->source:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    invoke-direct {p0, v11, v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->getAttribution(Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    move-result-object v7

    .line 200
    .local v7, "attribution":Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    const v0, 0x7f0400d8

    iget-object v14, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mContext:Landroid/content/Context;
    invoke-static {v14}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$100(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Landroid/content/Context;

    move-result-object v14

    invoke-static {v0, v14}, Lcom/google/android/apps/books/util/ViewUtils;->inflateWithContext(ILandroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v12

    .line 202
    .local v12, "wordView":Landroid/view/ViewGroup;
    const v0, 0x7f0e0204

    invoke-virtual {v12, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 205
    .local v2, "wordContentView":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->this$0:Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->access$100(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 206
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v6, 0x1

    .line 207
    .local v6, "isFirst":Z
    iget-object v0, v11, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;->senses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    .local v3, "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    move-object v0, p0

    .line 208
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->addSenseView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 210
    const/4 v6, 0x0

    .line 211
    goto :goto_0

    .line 213
    .end local v3    # "sense":Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    :cond_0
    if-eqz v7, :cond_1

    .line 214
    invoke-direct {p0, v2, v7}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->addAttributionView(Landroid/view/ViewGroup;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    .line 216
    :cond_1
    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "wordContentView":Landroid/view/ViewGroup;
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "pronunciation":Ljava/lang/String;
    .end local v6    # "isFirst":Z
    .end local v7    # "attribution":Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "word":Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    .end local v12    # "wordView":Landroid/view/ViewGroup;
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v10}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v14

    invoke-interface {v0, v14}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 225
    .end local v8    # "entry":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .end local v10    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v13    # "words":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;>;"
    :goto_1
    return-void

    .line 220
    .restart local v8    # "entry":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .restart local v10    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_3
    new-instance v0, Ljava/lang/Exception;

    const-string v14, "Bad dictionary result"

    invoke-direct {v0, v14}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->publishException(Ljava/lang/Exception;)V

    goto :goto_1

    .line 223
    .end local v8    # "entry":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .end local v10    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->publishException(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 115
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;->onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
