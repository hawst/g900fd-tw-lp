.class public Lcom/google/android/apps/books/data/DataControllerUtils;
.super Ljava/lang/Object;
.source "DataControllerUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;,
        Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;,
        Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;,
        Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;,
        Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;
    }
.end annotation


# static fields
.field public static final DUMMY_EXCEPTION_OR_NOTHING_CONSUMER:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/books/data/DataControllerUtils$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/DataControllerUtils;->DUMMY_EXCEPTION_OR_NOTHING_CONSUMER:Lcom/google/android/ublib/utils/Consumer;

    return-void
.end method

.method public static buildContentStreamSource(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/data/InputStreamSource;
    .locals 1
    .param p0, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p1, "scheme"    # Lcom/google/android/apps/books/data/EncryptionScheme;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/DataControllerBlob;",
            "Lcom/google/android/apps/books/data/EncryptionScheme;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/data/InputStreamSource;"
        }
    .end annotation

    .prologue
    .line 439
    .local p2, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-eqz p2, :cond_0

    .line 440
    new-instance v0, Lcom/google/android/apps/books/data/DataControllerUtils$2;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/apps/books/data/DataControllerUtils$2;-><init>(Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    move-object p0, v0

    .line 462
    .end local p0    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    :cond_0
    return-object p0
.end method

.method private static createSaveConsumer(Z)Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;
    .locals 1
    .param p0, "waitForSave"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 340
    if-eqz p0, :cond_0

    .line 341
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v0

    .line 343
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ensureMyEbooksVolumes(Lcom/google/android/apps/books/data/BooksDataController;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .locals 3
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "fromServer"    # Z
    .param p2, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 329
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v0

    .line 331
    .local v0, "recordingConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;"
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v1

    .line 333
    .local v1, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    invoke-interface {p0, p1, v0, v1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 334
    invoke-virtual {v1}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 335
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    return-object v2
.end method

.method public static ensurePageImage(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 277
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v5

    .local v5, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v6, p3

    .line 279
    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/data/BooksDataController;->getPageContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 280
    invoke-virtual {v5}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 281
    return-void
.end method

.method public static ensurePageStructure(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 288
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v4

    .line 290
    .local v4, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/data/BooksDataController;->getPageStructure(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 291
    invoke-virtual {v4}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 292
    return-void
.end method

.method public static ensureResourceContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/BooksDataController$Priority;)Ljava/util/List;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v4

    .line 124
    .local v4, "resourcesConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;"
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v5

    .line 126
    .local v5, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    const/4 v3, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 128
    invoke-virtual {v5}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 129
    invoke-virtual {v4}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static ensureSegmentContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Z)Ljava/util/List;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p3, "ignoreResources"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Segment;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v4

    .line 108
    .local v4, "resourcesConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;"
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v5

    .line 110
    .local v5, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    const/4 v3, 0x0

    sget-object v7, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, p3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getSegmentContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 112
    invoke-virtual {v5}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 113
    invoke-virtual {v4}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static ensureVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 301
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v0

    .line 303
    .local v0, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    const/4 v1, 0x0

    invoke-interface {p0, p1, v1, v0, p2}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeCover(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 304
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 305
    return-void
.end method

.method public static ensureVolumeThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 315
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v0

    .line 317
    .local v0, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    const/4 v1, 0x0

    invoke-interface {p0, p1, v1, v0, p2}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeThumbnail(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 318
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 319
    return-void
.end method

.method public static getNotificationThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/res/Resources;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "res"    # Landroid/content/res/Resources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Landroid/content/res/Resources;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 486
    .local p3, "bitmapConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/DataControllerUtils$3;

    invoke-direct {v0, p2, p3}, Lcom/google/android/apps/books/data/DataControllerUtils$3;-><init>(Landroid/content/res/Resources;Lcom/google/android/ublib/utils/Consumer;)V

    .line 529
    .local v0, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-interface {p0, p1, v0, v1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeThumbnail(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 531
    return-void
.end method

.method public static getResourceContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 138
    new-instance v3, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;

    invoke-direct {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;-><init>()V

    .line 139
    .local v3, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;
    sget-object v6, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 140
    invoke-virtual {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$InputStreamOpeningConsumer;->getResult()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public static getResourceParcelFileDescriptor(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Landroid/os/ParcelFileDescriptor;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 149
    new-instance v3, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;

    invoke-direct {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;-><init>()V

    .line 151
    .local v3, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;
    sget-object v6, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 152
    invoke-virtual {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->getResult()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static getResourceParcelFileDescriptor(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 162
    new-instance v3, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;

    invoke-direct {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;-><init>()V

    .line 164
    .local v3, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;
    sget-object v6, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 166
    invoke-virtual {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->getResult()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static getResourceResources(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/BooksDataController$Priority;)Ljava/util/List;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 266
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v4

    .line 268
    .local v4, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;"
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v3

    move-object v6, p3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 269
    invoke-virtual {v4}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static getSegmentContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;)Ljava/lang/String;
    .locals 9
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 89
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v3

    .line 91
    .local v3, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getSegmentContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 92
    invoke-virtual {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getNonNullValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 93
    .local v8, "result":Ljava/lang/String;
    return-object v8
.end method

.method public static getVolumeData(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/model/VolumeData;
    .locals 7
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fromServer"    # Z
    .param p3, "waitForSave"    # Z
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v3

    .line 58
    .local v3, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeData;>;>;"
    invoke-static {p3}, Lcom/google/android/apps/books/data/DataControllerUtils;->createSaveConsumer(Z)Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v4

    .local v4, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p4

    .line 59
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeData(Ljava/lang/String;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 60
    invoke-virtual {v3}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getNonNullValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/VolumeData;

    .line 61
    .local v6, "result":Lcom/google/android/apps/books/model/VolumeData;
    invoke-static {v4}, Lcom/google/android/apps/books/data/DataControllerUtils;->maybeWait(Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;)V

    .line 62
    return-object v6
.end method

.method public static getVolumeManifest(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Ljava/util/Set;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    .locals 8
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "fromServer"    # Z
    .param p4, "waitForSave"    # Z
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v4

    .line 75
    .local v4, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    invoke-static {p4}, Lcom/google/android/apps/books/data/DataControllerUtils;->createSaveConsumer(Z)Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v5

    .local v5, "saveConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v6, p5

    .line 76
    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeManifest(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 78
    invoke-virtual {v4}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getNonNullValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    .line 79
    .local v7, "result":Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    invoke-static {v5}, Lcom/google/android/apps/books/data/DataControllerUtils;->maybeWait(Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;)V

    .line 80
    return-object v7
.end method

.method private static maybeWait(Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 348
    .local p0, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    if-eqz p0, :cond_0

    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->throwIfFailure()V

    .line 351
    :cond_0
    return-void
.end method

.method public static uploadCollectionMembershipChanges(Lcom/google/android/apps/books/data/BooksDataController;J)V
    .locals 1
    .param p0, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 472
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    move-result-object v0

    .line 474
    .local v0, "completionConsumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<Lcom/google/android/apps/books/util/Nothing;>;"
    invoke-interface {p0, p1, p2, v0}, Lcom/google/android/apps/books/data/BooksDataController;->uploadCollectionChanges(JLcom/google/android/ublib/utils/Consumer;)V

    .line 475
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->getOrThrow()Ljava/lang/Object;

    .line 476
    return-void
.end method
