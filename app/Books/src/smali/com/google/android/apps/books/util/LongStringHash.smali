.class public Lcom/google/android/apps/books/util/LongStringHash;
.super Ljava/lang/Object;
.source "LongStringHash.java"


# direct methods
.method public static from(Ljava/lang/CharSequence;)J
    .locals 14
    .param p0, "data"    # Ljava/lang/CharSequence;

    .prologue
    .line 19
    if-nez p0, :cond_0

    .line 20
    const-wide/16 v2, 0x0

    .line 59
    :goto_0
    return-wide v2

    .line 23
    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 24
    .local v1, "len":I
    add-int/lit8 v0, v1, -0x4

    .line 26
    .local v0, "alignedLen":I
    int-to-long v10, v1

    const-wide v12, -0x395b586ca42e166bL    # -2.0946245025644615E32

    mul-long v2, v10, v12

    .line 30
    .local v2, "h":J
    const/4 v6, 0x0

    .local v6, "off":I
    move v7, v6

    .end local v6    # "off":I
    .local v7, "off":I
    :goto_1
    if-gt v7, v0, :cond_1

    .line 32
    invoke-interface {p0, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    int-to-long v10, v9

    add-int/lit8 v9, v7, 0x1

    invoke-interface {p0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    int-to-long v12, v9

    const/16 v9, 0x10

    shl-long/2addr v12, v9

    or-long/2addr v10, v12

    add-int/lit8 v9, v7, 0x2

    invoke-interface {p0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    int-to-long v12, v9

    const/16 v9, 0x20

    shl-long/2addr v12, v9

    or-long/2addr v10, v12

    add-int/lit8 v9, v7, 0x3

    invoke-interface {p0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    int-to-long v12, v9

    const/16 v9, 0x30

    shl-long/2addr v12, v9

    or-long v4, v10, v12

    .line 37
    .local v4, "k":J
    const-wide v10, -0x395b586ca42e166bL    # -2.0946245025644615E32

    mul-long/2addr v4, v10

    .line 38
    const/16 v9, 0x2f

    ushr-long v10, v4, v9

    xor-long/2addr v4, v10

    .line 39
    const-wide v10, -0x395b586ca42e166bL    # -2.0946245025644615E32

    mul-long/2addr v4, v10

    .line 41
    xor-long/2addr v2, v4

    .line 42
    const-wide v10, -0x395b586ca42e166bL    # -2.0946245025644615E32

    mul-long/2addr v2, v10

    .line 30
    add-int/lit8 v6, v7, 0x4

    .end local v7    # "off":I
    .restart local v6    # "off":I
    move v7, v6

    .end local v6    # "off":I
    .restart local v7    # "off":I
    goto :goto_1

    .line 45
    .end local v4    # "k":J
    :cond_1
    if-ge v7, v1, :cond_3

    .line 46
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "off":I
    .restart local v6    # "off":I
    invoke-interface {p0, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    int-to-long v10, v9

    xor-long/2addr v2, v10

    .line 48
    const/16 v8, 0x10

    .local v8, "shift":I
    :goto_2
    if-ge v6, v1, :cond_2

    .line 49
    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    shl-int/2addr v9, v8

    int-to-long v10, v9

    xor-long/2addr v2, v10

    .line 48
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v8, v8, 0x10

    goto :goto_2

    .line 52
    :cond_2
    const-wide v10, -0x395b586ca42e166bL    # -2.0946245025644615E32

    mul-long/2addr v2, v10

    .line 55
    .end local v8    # "shift":I
    :goto_3
    const/16 v9, 0x2f

    ushr-long v10, v2, v9

    xor-long/2addr v2, v10

    .line 56
    const-wide v10, -0x395b586ca42e166bL    # -2.0946245025644615E32

    mul-long/2addr v2, v10

    .line 57
    const/16 v9, 0x2f

    ushr-long v10, v2, v9

    xor-long/2addr v2, v10

    .line 59
    goto/16 :goto_0

    .end local v6    # "off":I
    .restart local v7    # "off":I
    :cond_3
    move v6, v7

    .end local v7    # "off":I
    .restart local v6    # "off":I
    goto :goto_3
.end method
