.class public Lcom/google/android/apps/books/widget/StaggeredCoverView;
.super Landroid/widget/AdapterView;
.source "StaggeredCoverView.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayScrollableContentView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;,
        Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;,
        Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;,
        Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;",
        ">;",
        "Lcom/google/android/play/headerlist/PlayScrollableContentView;"
    }
.end annotation


# instance fields
.field private adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

.field private final adapterObserver:Landroid/database/DataSetObserver;

.field private final bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field protected final edgePadding:I

.field private final gestureDetector:Landroid/view/GestureDetector;

.field protected interiorPadding:I

.field private isInGesture:Z

.field private laidOut:Z

.field private mAnimatorSet:Landroid/animation/AnimatorSet;

.field private final mCoverViewLoader:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

.field private mFirstVisiblePosition:I

.field private final mHoldingPen:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLastVisiblePosition:I

.field private final mLayoutHelper:Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

.field private mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

.field private final mTemporaryHitRect:Landroid/graphics/Rect;

.field private mVisiblePositionsNeedComputing:Z

.field private overScroll:I

.field private final previousLocations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final scroller:Landroid/widget/Scroller;

.field private final topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private touchDownY:F

.field private final touchSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 445
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 446
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 449
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 450
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 467
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114
    new-instance v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$1;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapterObserver:Landroid/database/DataSetObserver;

    .line 163
    new-instance v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;Lcom/google/android/apps/books/widget/StaggeredCoverView$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mCoverViewLoader:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    .line 407
    new-instance v0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLayoutHelper:Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

    .line 414
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mHoldingPen:Ljava/util/Map;

    .line 417
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->laidOut:Z

    .line 426
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->previousLocations:Ljava/util/Map;

    .line 427
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    .line 439
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->isInGesture:Z

    .line 442
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mTemporaryHitRect:Landroid/graphics/Rect;

    .line 967
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$3;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->gestureDetector:Landroid/view/GestureDetector;

    .line 469
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setClickable(Z)V

    .line 470
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setFocusableInTouchMode(Z)V

    .line 472
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 473
    new-instance v0, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 474
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->touchSlop:I

    .line 479
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->edgePadding:I

    .line 480
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->resetState()V

    .line 484
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setWillNotDraw(Z)V

    .line 485
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->update()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->recordLocations()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->isInGesture:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/StaggeredCoverView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->overScroll:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Landroid/support/v4/widget/EdgeEffectCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/widget/StaggeredCoverView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollExtent()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/StaggeredCoverView;)Landroid/widget/Scroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method private addChildToGrid(Landroid/view/View;IZ)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "reusing"    # Z

    .prologue
    .line 568
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 570
    if-eqz p3, :cond_1

    .line 571
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    .line 572
    .local v1, "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 573
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setupExistingView(Landroid/view/View;I)V

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 581
    .end local v1    # "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    :cond_1
    const v2, 0x7f0e003a

    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v3, p2}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 582
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    .line 583
    .restart local v1    # "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 586
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getCoverContainerView(Landroid/view/View;)Lcom/google/android/apps/books/widget/BookCoverContainerView;

    move-result-object v0

    .line 587
    .local v0, "coverView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    if-eqz v0, :cond_0

    .line 588
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->loadCoverView(Z)V

    goto :goto_0
.end method

.method private cancelAnimationAndRecordLocations()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 764
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    .line 770
    :goto_0
    return-void

    .line 768
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->recordLocations()V

    goto :goto_0
.end method

.method private clearHoldingPen()V
    .locals 3

    .prologue
    .line 526
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mHoldingPen:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 527
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Landroid/view/View;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 528
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->removeDetachedView(Landroid/view/View;Z)V

    goto :goto_0

    .line 530
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mHoldingPen:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 531
    return-void
.end method

.method private static createAnimatorSet()Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 756
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    return-object v0
.end method

.method private getCardCount()I
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getCardCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCoverContainerView(Landroid/view/View;)Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 607
    instance-of v0, p1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    .end local p1    # "view":Landroid/view/View;
    :goto_0
    return-object p1

    .restart local p1    # "view":Landroid/view/View;
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private isReady(Landroid/view/View;)Z
    .locals 3
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 597
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 602
    :cond_0
    :goto_0
    return v1

    .line 600
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getCoverContainerView(Landroid/view/View;)Lcom/google/android/apps/books/widget/BookCoverContainerView;

    move-result-object v0

    .line 602
    .local v0, "coverContainerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->hasCoverImage()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isViewVisibleOnScreen(Landroid/view/View;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 854
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mTemporaryHitRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHitRect(Landroid/graphics/Rect;)V

    .line 856
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mTemporaryHitRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    .line 858
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeComputeVisiblePositions()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 863
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mVisiblePositionsNeedComputing:Z

    if-eqz v5, :cond_4

    .line 864
    iput v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    .line 865
    iput v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLastVisiblePosition:I

    .line 867
    const/4 v3, 0x0

    .line 868
    .local v3, "invisibleCardCount":I
    const/4 v4, 0x0

    .line 873
    .local v4, "visibleCardCount":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v1

    .line 874
    .local v1, "childCount":I
    const/4 v2, 0x0

    .line 875
    .local v2, "index":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 876
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 877
    .local v0, "child":Landroid/view/View;
    instance-of v5, v0, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    if-eqz v5, :cond_0

    .line 878
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->isViewVisibleOnScreen(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 879
    add-int/lit8 v4, v4, 0x1

    .line 880
    iget v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    if-ne v5, v6, :cond_0

    .line 881
    iput v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    .line 887
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 888
    goto :goto_0

    .line 884
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 890
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    iget v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    if-eq v5, v6, :cond_3

    .line 891
    iget v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    add-int/2addr v5, v4

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLastVisiblePosition:I

    .line 896
    :cond_3
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mVisiblePositionsNeedComputing:Z

    .line 898
    .end local v1    # "childCount":I
    .end local v2    # "index":I
    .end local v3    # "invisibleCardCount":I
    .end local v4    # "visibleCardCount":I
    :cond_4
    return-void
.end method

.method private maybeRunAnimation(Z)V
    .locals 23
    .param p1, "onInitialLayout"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 672
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 673
    .local v5, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    new-instance v9, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v9}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 674
    .local v9, "interp":Landroid/view/animation/Interpolator;
    const/4 v4, 0x0

    .line 675
    .local v4, "animationCount":I
    const/16 v14, 0x12c

    .line 676
    .local v14, "maxDelay":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_4

    .line 677
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 678
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v19

    if-eqz v19, :cond_1

    .line 676
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 682
    :cond_1
    const/16 v19, 0x2

    const/16 v20, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 684
    const v19, 0x7f0e003a

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 685
    .local v10, "id":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->previousLocations:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;

    .line 687
    .local v15, "prev":Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    .line 688
    .local v13, "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    if-eqz v15, :cond_0

    iget-boolean v0, v13, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;->fillWidth:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    .line 690
    iget-object v0, v15, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;->location:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v20

    sub-int v12, v19, v20

    .line 691
    .local v12, "leftDelta":I
    iget-object v0, v15, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;->location:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v20

    sub-int v16, v19, v20

    .line 692
    .local v16, "topDelta":I
    if-nez v12, :cond_2

    if-eqz v16, :cond_0

    .line 693
    :cond_2
    int-to-float v0, v12

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 694
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 698
    if-eqz p1, :cond_3

    const/16 v19, 0x12c

    mul-int/lit16 v0, v4, 0x12c

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x8

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 701
    .local v7, "delay":I
    :goto_2
    sget-object v19, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v6}, Landroid/view/View;->getTranslationX()F

    move-result v22

    aput v22, v20, v21

    const/16 v21, 0x1

    const/16 v22, 0x0

    aput v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v6, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v17

    .line 704
    .local v17, "translateAnimatorX":Landroid/animation/ObjectAnimator;
    const-wide/16 v20, 0x12c

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 705
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 706
    int-to-long v0, v7

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 708
    sget-object v19, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v6}, Landroid/view/View;->getTranslationY()F

    move-result v22

    aput v22, v20, v21

    const/16 v21, 0x1

    const/16 v22, 0x0

    aput v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v6, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v18

    .line 711
    .local v18, "translateAnimatorY":Landroid/animation/ObjectAnimator;
    const-wide/16 v20, 0x12c

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 712
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 713
    int-to-long v0, v7

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 715
    move-object/from16 v0, v17

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 716
    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 717
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 698
    .end local v7    # "delay":I
    .end local v17    # "translateAnimatorX":Landroid/animation/ObjectAnimator;
    .end local v18    # "translateAnimatorY":Landroid/animation/ObjectAnimator;
    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    .line 722
    .end local v6    # "child":Landroid/view/View;
    .end local v10    # "id":J
    .end local v12    # "leftDelta":I
    .end local v13    # "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    .end local v15    # "prev":Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;
    .end local v16    # "topDelta":I
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_5

    .line 723
    invoke-static {}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->createAnimatorSet()Landroid/animation/AnimatorSet;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    .line 724
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    new-instance v20, Lcom/google/android/apps/books/widget/StaggeredCoverView$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$2;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;)V

    invoke-virtual/range {v19 .. v20}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/animation/AnimatorSet;->start()V

    .line 753
    :goto_3
    return-void

    .line 751
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->recordLocations()V

    goto :goto_3
.end method

.method private moveChildrenToHoldingPen()V
    .locals 4

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 514
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 515
    .local v0, "child":Landroid/view/View;
    const v3, 0x7f0e003a

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 516
    .local v2, "itemId":Ljava/lang/Long;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->detachViewFromParent(Landroid/view/View;)V

    .line 517
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mHoldingPen:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 519
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "itemId":Ljava/lang/Long;
    :cond_0
    return-void
.end method

.method private recordLocations()V
    .locals 10

    .prologue
    .line 648
    iget-boolean v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->laidOut:Z

    if-eqz v6, :cond_3

    .line 649
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 650
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 653
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->isReady(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 654
    const v6, 0x7f0e003a

    invoke-virtual {v0, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 655
    .local v3, "itemId":Ljava/lang/Long;
    iget-object v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->previousLocations:Ljava/util/Map;

    new-instance v7, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;

    invoke-direct {v7, p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;-><init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;Landroid/view/View;)V

    invoke-interface {v6, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 649
    .end local v3    # "itemId":Ljava/lang/Long;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 658
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 659
    .local v4, "now":J
    iget-object v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->previousLocations:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 661
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;>;>;"
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 662
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;

    iget-wide v6, v6, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;->timestamp:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x7d0

    cmp-long v6, v6, v8

    if-lez v6, :cond_2

    .line 663
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 667
    .end local v1    # "i":I
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;>;>;"
    .end local v4    # "now":J
    :cond_3
    return-void
.end method

.method private resetState()V
    .locals 1

    .prologue
    .line 534
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->cancelAnimationAndRecordLocations()V

    .line 535
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->moveChildrenToHoldingPen()V

    .line 536
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->resetVisiblePositions()V

    .line 537
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->laidOut:Z

    .line 538
    return-void
.end method

.method private resetVisiblePositions()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 901
    iput v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    .line 902
    iput v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLastVisiblePosition:I

    .line 903
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mVisiblePositionsNeedComputing:Z

    .line 904
    return-void
.end method

.method private scrollExtent()I
    .locals 2

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLayoutHelper:Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->getLayoutBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private unsetAdapter()V
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapterObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 555
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .line 557
    :cond_0
    return-void
.end method

.method private update()V
    .locals 7

    .prologue
    .line 611
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->resetState()V

    .line 616
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v5, :cond_2

    .line 617
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 618
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getItemId(I)J

    move-result-wide v2

    .line 619
    .local v2, "id":J
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mHoldingPen:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 620
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    const/4 v4, 0x1

    .line 621
    .local v4, "reusing":Z
    :goto_1
    if-nez v4, :cond_0

    .line 622
    iget-object v5, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6, p0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 624
    :cond_0
    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->addChildToGrid(Landroid/view/View;IZ)V

    .line 617
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 620
    .end local v4    # "reusing":Z
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 632
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    .end local v2    # "id":J
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->clearHoldingPen()V

    .line 639
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->requestLayout()V

    .line 640
    return-void
.end method

.method private updateVisibilities(Z)V
    .locals 11
    .param p1, "inLayout"    # Z

    .prologue
    const/4 v9, 0x4

    const/4 v7, 0x0

    .line 823
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHeight()I

    move-result v2

    .line 824
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v6

    .line 825
    .local v6, "scrollY":I
    const/high16 v8, 0x3e800000    # 0.25f

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v8, v10

    float-to-int v5, v8

    .line 826
    .local v5, "margin":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v8

    if-ge v3, v8, :cond_6

    .line 827
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 828
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    sub-int v10, v6, v5

    if-lt v8, v10, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    add-int v10, v6, v2

    add-int/2addr v10, v5

    if-ge v8, v10, :cond_1

    const/4 v4, 0x1

    .line 831
    .local v4, "isVisible":Z
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getCoverContainerView(Landroid/view/View;)Lcom/google/android/apps/books/widget/BookCoverContainerView;

    move-result-object v1

    .line 832
    .local v1, "coverContainerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    if-nez v1, :cond_3

    .line 834
    if-eqz v4, :cond_2

    move v8, v7

    :goto_2
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 826
    :cond_0
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .end local v1    # "coverContainerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .end local v4    # "isVisible":Z
    :cond_1
    move v4, v7

    .line 828
    goto :goto_1

    .restart local v1    # "coverContainerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .restart local v4    # "isVisible":Z
    :cond_2
    move v8, v9

    .line 834
    goto :goto_2

    .line 835
    :cond_3
    if-eqz v4, :cond_5

    .line 836
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_0

    .line 837
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->hasCoverImage()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 838
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 840
    :cond_4
    iget-object v8, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mCoverViewLoader:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    invoke-virtual {v8, v1, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->loadCover(Lcom/google/android/apps/books/widget/BookCoverContainerView;Z)V

    goto :goto_3

    .line 844
    :cond_5
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 845
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->unloadCoverView(Z)V

    goto :goto_3

    .line 849
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "coverContainerView":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    .end local v4    # "isVisible":Z
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->resetVisiblePositions()V

    .line 850
    iget-object v7, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mCoverViewLoader:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->finishLoadingCovers()V

    .line 851
    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 950
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 951
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollTo(II)V

    .line 952
    iget v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->overScroll:I

    if-eqz v1, :cond_1

    .line 954
    iget v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->overScroll:I

    if-lez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 956
    .local v0, "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 957
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 959
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 961
    .end local v0    # "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    .line 962
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 965
    :cond_2
    return-void

    .line 954
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1013
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1014
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->isInGesture:Z

    .line 1015
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->touchDownY:F

    .line 1018
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1040
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->draw(Landroid/graphics/Canvas;)V

    .line 1043
    const/4 v0, 0x0

    .line 1044
    .local v0, "needsInvalidate":Z
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1045
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1046
    .local v1, "restoreCount":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1047
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    .line 1048
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1049
    const/4 v0, 0x1

    .line 1051
    .end local v1    # "restoreCount":I
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1052
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1053
    .restart local v1    # "restoreCount":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getWidth()I

    move-result v2

    .line 1054
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollX()I

    move-result v3

    sub-int/2addr v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1055
    const/high16 v3, 0x43340000    # 180.0f

    int-to-float v4, v2

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1056
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    .line 1057
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1058
    const/4 v0, 0x1

    .line 1060
    .end local v1    # "restoreCount":I
    .end local v2    # "width":I
    :cond_1
    if-eqz v0, :cond_2

    .line 1061
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 1063
    :cond_2
    return-void
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getAdapter()Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    return-object v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getFirstVisiblePosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getCardCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->maybeComputeVisiblePositions()V

    .line 490
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mFirstVisiblePosition:I

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->maybeComputeVisiblePositions()V

    .line 496
    iget v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLastVisiblePosition:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 459
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 561
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    .line 562
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->unsetAdapter()V

    .line 563
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1074
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1075
    const-class v0, Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1076
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1068
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1069
    const-class v0, Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1070
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 1024
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1025
    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 1029
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->touchDownY:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->touchSlop:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v3, 0x1

    .line 792
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->cancelAnimationAndRecordLocations()V

    .line 794
    sub-int v2, p4, p2

    .line 795
    .local v2, "width":I
    sub-int v0, p5, p3

    .line 797
    .local v0, "height":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLayoutHelper:Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

    invoke-virtual {v4, v2, v0}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->layoutViews(II)V

    .line 799
    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->topEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v2, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 800
    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->bottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v2, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 802
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->laidOut:Z

    .line 803
    .local v1, "wasLaidOut":Z
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->laidOut:Z

    .line 806
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollX()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollTo(II)V

    .line 808
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->updateVisibilities(Z)V

    .line 809
    if-nez v1, :cond_0

    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->maybeRunAnimation(Z)V

    .line 810
    return-void

    .line 809
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v9, 0x0

    .line 774
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onMeasure(II)V

    .line 775
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 776
    .local v4, "width":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLayoutHelper:Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->getColumnCount()I

    move-result v1

    .line 777
    .local v1, "columnCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildCount()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 778
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 779
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;

    .line 780
    .local v3, "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    iget v6, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->edgePadding:I

    mul-int/lit8 v6, v6, 0x2

    sub-int v6, v4, v6

    add-int/lit8 v7, v1, -0x1

    iget v8, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->interiorPadding:I

    mul-int/2addr v7, v8

    sub-int v5, v6, v7

    .line 781
    .local v5, "widthMinusPadding":I
    iget-boolean v6, v3, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;->fillWidth:Z

    if-eqz v6, :cond_0

    move v6, v4

    :goto_1
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/view/View;->measure(II)V

    .line 777
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 781
    :cond_0
    div-int v6, v5, v1

    goto :goto_1

    .line 786
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "lp":Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutParams;
    .end local v5    # "widthMinusPadding":I
    :cond_1
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 2
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    const/4 v1, 0x0

    .line 814
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->onScrollChanged(IIII)V

    .line 815
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->updateVisibilities(Z)V

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    invoke-interface {v0, p0, v1}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrollStateChanged(Lcom/google/android/play/headerlist/PlayScrollableContentView;I)V

    .line 820
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 1034
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->isInGesture:Z

    .line 1035
    return v0
.end method

.method public scrollBy(II)V
    .locals 6
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 935
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v3

    .line 936
    .local v3, "oldY":I
    add-int v0, v3, p2

    .line 937
    .local v0, "attemptedY":I
    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollExtent()I

    move-result v5

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/books/util/MathUtils;->constrain(III)I

    move-result v2

    .line 938
    .local v2, "clampedY":I
    sub-int v1, v2, v3

    .line 939
    .local v1, "clampedDy":I
    sub-int v4, v2, v3

    invoke-super {p0, p1, v4}, Landroid/widget/AdapterView;->scrollBy(II)V

    .line 940
    sub-int v4, v0, v2

    iput v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->overScroll:I

    .line 942
    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    if-eqz v4, :cond_0

    .line 943
    iget-object v4, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    invoke-interface {v4, p0, p1, v1}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrolled(Lcom/google/android/play/headerlist/PlayScrollableContentView;II)V

    .line 945
    :cond_0
    return-void
.end method

.method public scrollTo(II)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 912
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollX()I

    move-result v1

    .line 913
    .local v1, "oldX":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->getScrollY()I

    move-result v2

    .line 914
    .local v2, "oldY":I
    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->scrollExtent()I

    move-result v4

    invoke-static {p2, v3, v4}, Lcom/google/android/apps/books/util/MathUtils;->constrain(III)I

    move-result v0

    .line 915
    .local v0, "clampedY":I
    invoke-super {p0, p1, v0}, Landroid/widget/AdapterView;->scrollTo(II)V

    .line 916
    sub-int v3, p2, v0

    iput v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->overScroll:I

    .line 918
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    if-eqz v3, :cond_0

    .line 919
    iget-object v3, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    sub-int v4, p1, v1

    sub-int v5, v0, v2

    invoke-interface {v3, p0, v4, v5}, Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;->onScrolled(Lcom/google/android/play/headerlist/PlayScrollableContentView;II)V

    .line 921
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 81
    check-cast p1, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setAdapter(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    return-void
.end method

.method public setAdapter(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .prologue
    .line 546
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->unsetAdapter()V

    .line 547
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->adapterObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 549
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->update()V

    .line 550
    return-void
.end method

.method public setColumnCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mLayoutHelper:Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/StaggeredCoverView$LayoutHelper;->setColumnCount(I)V

    .line 542
    return-void
.end method

.method public setOnScrollListener(Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    .prologue
    .line 930
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView;->mListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    .line 931
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 464
    return-void
.end method
